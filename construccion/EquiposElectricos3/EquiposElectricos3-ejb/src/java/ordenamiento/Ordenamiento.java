/*
 * Ordenamiento.java
 *
 * Created on 22 de septiembre de 2008, 12:25
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package ordenamiento;

import java.util.Comparator;
import java.util.List;
import mac.ee.pla.equipo.clase.Equipo;

/**
 *
 * @author dbarra
 */
public class Ordenamiento implements Comparator {
    private byte verificar;
    private byte estado;
    public Ordenamiento(byte verificar,byte estado){
        this.verificar = verificar;
        this.estado = estado;
    }
    /*public int compare(Object o1, Object o2) {
        EstructuraPorEquipo persona1 = (EstructuraPorEquipo)o1;
        EstructuraPorEquipo persona2 = (EstructuraPorEquipo)o2; 
        if((byte)verificar == 1){
          return persona1.getCodigoMatPrima().compareTo(persona2.getCodigoMatPrima());
        }else if((byte)verificar == 2){
          return persona1.getDescripcion().compareTo(persona2.getDescripcion());  
        }else if((byte)verificar == 3){
          return (int)persona1.getStockMinimo() - (int)persona2.getStockMinimo();  
        }else if((byte)verificar == 4){
          return (int)persona1.getStockMaximo() - (int)persona2.getStockMaximo();  
        }else if((byte)verificar == 5){
          return persona1.getLeadTime() - persona2.getLeadTime();    
        } else{}   
      return -1;        
    }*/
    public int compare(Object o1, Object o2) {
      if(estado == 1){  
        List persona1 = (List)o1;
        List persona2 = (List)o2; 
        if((byte)verificar == 1){
          return ((String)persona1.get(0)).compareTo((String)persona2.get(0));   
        }else if((byte)verificar == 2){
          return ((String)persona1.get(1)).compareTo((String)persona2.get(1));   
        }else{} 
      }else if(estado == 2){
          Equipo equipo1 = (Equipo)o1;
          Equipo equipo2 = (Equipo)o2;
          if((byte)verificar == 1){
              return equipo1.getFechaCliente().compareTo(equipo2.getFechaCliente());
          }else if((byte)verificar == 2){
              return equipo1.getReadTime().compareTo(equipo2.getReadTime());
          }else if((byte)verificar == 3){
              return equipo1.getCliente().compareTo(equipo2.getCliente());
          }else if((byte)verificar == 4){
              return equipo1.getStatus().compareTo(equipo2.getStatus());
          }
          
      }  
      return -1;        
    }
}

