/*
 * PrmDatos.java
 *
 * Created on 19 de marzo de 2011, 01:53 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.datos;

/**
 *
 * @author dbarra
 */
public class PrmDatos {
    
    /** Creates a new instance of PrmDatos */
    public PrmDatos() {
    }
    
    public static String devolverString(String descripcion){
        if(descripcion.length() > 25){
            return descripcion.substring(0,25).toLowerCase();
        }else{
            return descripcion;
        }
    }
    
}
