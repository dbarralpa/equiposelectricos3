package mac.excel;

import java.util.ArrayList;

public class ModAuxiliar implements Cloneable {

    private int ID;
    private String Nombre0;
    private String Nombre1;
    private String Nombre2;
    private String Nombre3;    
    private double valor0;
    private double valor1;
    private boolean Estado;
    private ArrayList detalle;

    public ModAuxiliar() {
        ID = 0;
        Nombre0 = "";
        Nombre1 = "";
        Nombre2 = "";
        Nombre3 = "";        
        valor0 = 0.0;
        valor1 = 0.0;
        Estado = true;
        detalle = new ArrayList();
    }

    public void addObjeto(ModAuxiliar mod) {
        this.detalle.add(mod);
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNombre0() {
        return Nombre0;
    }

    public void setNombre0(String Nombre0) {
        this.Nombre0 = Nombre0;
    }

    public ArrayList getDetalle() {
        return detalle;
    }

    public void setDetalle(ArrayList Detalle) {
        this.detalle = Detalle;
    }

    public boolean isEstado() {
        return Estado;
    }

    public void setEstado(boolean Estado) {
        this.Estado = Estado;
    }

    public String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(String Nombre1) {
        this.Nombre1 = Nombre1;
    }

    public String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(String Nombre2) {
        this.Nombre2 = Nombre2;
    }

    public String getNombre3() {
        return Nombre3;
    }

    public void setNombre3(String Nombre3) {
        this.Nombre3 = Nombre3;
    }

    public double getValor0() {
        return valor0;
    }

    public void setValor0(double valor0) {
        this.valor0 = valor0;
    }

    public double getValor1() {
        return valor1;
    }

    public void setValor1(double valor1) {
        this.valor1 = valor1;
    }
    
    public static double redondear(double numero, int decimales) {

        return Math.round(numero * Math.pow(10, decimales)) / Math.pow(10, decimales);

    }



    public ModAuxiliar copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModAuxiliar) obj;
    }
}
