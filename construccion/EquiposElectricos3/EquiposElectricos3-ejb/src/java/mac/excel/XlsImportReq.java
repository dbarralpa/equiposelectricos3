/*
 * XlsImportReq.java
 *
 * Created on 11 de febrero de 2011, 12:15 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.excel;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class XlsImportReq {
    
    /** Creates a new instance of XlsImportReq */
    public XlsImportReq(String ddd) {
      try{
        AsExcel excel = new AsExcel();
            excel.crearExcelSinEstilos(ddd);
            ArrayList datos = excel.leerDatosHoja(0);
            String html = "";
            for (int i = 0; i < datos.size(); i++) {
                ModAuxiliar fila = (ModAuxiliar) datos.get(i);
                NumberFormat nf = new DecimalFormat(" ");
                if (fila.getNombre0().equals("")) {
                    html += nf.format(ModAuxiliar.redondear(fila.getValor0(), 0));
                } else {
                    html += fila.getNombre0();
                }
                html += nf.format(ModAuxiliar.redondear(fila.getValor1(), 0)) + "\n";
                String ccc = "campo1, campo2";
                String vvv = "";
                fila = null;
                nf = null;
            }
            System.out.println(html+"\n");
            excel.cerrarPlantilla();
    }catch (Exception ex) {
          //  PrmApp.addError(ex, true);
        }
    }
    
  /*  public static void main(String[] arg){
        new XlsImportReq();
    }*/
}
