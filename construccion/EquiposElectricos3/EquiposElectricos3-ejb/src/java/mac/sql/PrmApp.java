/*
 * PrmApp.java
 *
 * Created on 16 de noviembre de 2011, 17:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.sql;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrmApp {

    public static String FORMATO_YMD = "yyyy-MM-dd";
    public static String FORMATO_YMD1 = "yyyyMMdd";
    public static String FORMATO_HMS = "HH:mm:ss";
    //public static String FILE_ERROR = "/home/trafonet/logErrorGestionTrafonetJar.txt";
    //public static String FILE_ERROR = "/home/dbarra/logErrorGestionTrafonetJar.txt";
    public static String FILE_ERROR = "E:/logErrorPedidoMateriales.txt";

    public static String verOverTip(String Contenido) {
        return verOverLib("Info. Tip", Contenido, "#E1E1FF", "#000066");
    }

    public static String verOverLib(String Contenido) {
        return verOverLib("Gesti�n Trafonet", Contenido, "#FFFFCC", "#FF9900");
    }

    public static String verOverLib(String Titulo, String Contenido, String fg, String bg) {
        String tmp = "'";
        Contenido = Contenido.replace(tmp.charAt(0), '�');
        Contenido = Contenido.replace('"', '�');
        Contenido = Contenido.replaceAll("�", "<br>");
        Contenido = Contenido.replaceAll("\n", "<br>");
        Contenido = Contenido.replaceAll("\r", "");
        return "onMouseOver=" + '"' + "showOverLib('<center>" + Titulo + "</center>','" + Contenido + "', '" + fg + "', '" + bg + "');" + '"' + " OnMouseOut=" + '"' + "return nd();" + '"';
    }

    public static String getFechaActual() {
        return verFecha(new java.util.Date(), FORMATO_YMD);
    }
     public static String getFechaActual1() {
        return verFecha(new java.util.Date(), FORMATO_YMD1);
    }

    public static String getHoraActual() {
        return verFecha(new java.util.Date(), FORMATO_HMS);
    }

    private static String verFecha(Date fecha, String formato) {
        SimpleDateFormat f = new SimpleDateFormat(formato);
        if (fecha != null) {
            return f.format(fecha);
        } else {
            return "";
        }
    }

    public static String verHtmlNull(String dato, int largo) {
        String tmp = verHtmlNull(dato);
        if (tmp.length() > largo) {
            tmp = tmp.substring(0, (largo - 3)) + "...";
        }
        return tmp;
    }

    public static String verHtmlNull(String dato) {
        if (dato != null) {
            if (dato.trim().equals("")) {
                return "&nbsp;";
            } else {
                return dato.trim();
            }
        } else {
            return "&nbsp;";
        }
    }

    public static String verHtmlEstado(String dato) {
        if (dato == null) {
            return "NULO";
        } else if (dato.equals("VIGENT")) {
            return "VIGENTE";
        } else if (dato.equals("NOVIGE")) {
            return "NO VIGENTE";
        } else {
            return "ELIMINADO";
        }
    }

    public static String llenaCaracter(String val, int largo, char caracter) {
        return llenaCaracter(val, largo, caracter, false);
    }

    public static String llenaCaracter(String val, int largo, char caracter, boolean left) {
        int l = val.trim().length();
        String txt = "";
        if (largo < l) {
            txt = val.trim();
        } else {
            for (int i = 0; i < (largo - l); i++) {
                txt = txt + caracter;
            }
            if (left) {
                txt = val + txt.trim();
            } else {
                txt = txt + val.trim();
            }
        }
        return txt.toUpperCase();
    }

    

    public static void addError(Exception ex) {
        addError(ex, false);
    }

    public static void addError(Exception ex, boolean conTraza) {
        String Err = "";
        if (conTraza) {
            StackTraceElement[] nn = ex.getStackTrace();
            for (int i = 0; i < nn.length; i++) {
                Err += nn[i].toString() + " ";
            }
        }
        FileWriter fichero = null;
        PrintWriter pw = null;
        try {
            fichero = new FileWriter(FILE_ERROR, true);
            pw = new PrintWriter(fichero);
            pw.println(PrmApp.getFechaActual() + " " + PrmApp.getHoraActual());
            pw.println(ex.toString());
            pw.println(Err);
            pw.println("");
        } catch (Exception e) {
        } finally {
            if (null != fichero) {
                try {
                    fichero.close();
                } catch (Exception e1) {
                }
            }
            try {
            } catch (Exception e2) {
            }
        }
    }
}
