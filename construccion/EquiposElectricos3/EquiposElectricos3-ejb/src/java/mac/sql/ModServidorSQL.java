/*
 * ModServidorSQL.java
 *
 * Created on 16 de noviembre de 2011, 17:21
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.sql;
import com.microsoft.sqlserver.jdbc.SQLServerDriver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ModServidorSQL {

    private boolean activa;
    private String Svr;
    private int port;
    private String Bdd;
    private String Usr;
    private String Pwd;
    private String prefix;
    private final String selectMethod = "cursor";

    public ModServidorSQL() {
        activa = false;
        Svr = "";
        port = 0;
        Bdd = "";
        Usr = "";
        Pwd = "";
        prefix = "";
    }

    public String getBdd() {
        return Bdd;
    }

    public void setBdd(String Bdd) {
        this.Bdd = Bdd;
    }

    public String getPwd() {
        return Pwd;
    }

    public void setPwd(String Pwd) {
        this.Pwd = Pwd;
    }

    public String getSvr() {
        return Svr;
    }

    public void setSvr(String Svr) {
        this.Svr = Svr;
    }

    public String getUsr() {
        return Usr;
    }

    public void setUsr(String Usr) {
        this.Usr = Usr;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    
//    public java.sql.Connection ConexionMYSQL() throws SQLException{
//        java.sql.Connection tmp	= null;
//        DriverManager.registerDriver(new org.gjt.mm.mysql.Driver());
//        tmp = java.sql.DriverManager.getConnection("jdbc:mysql://" + getSvr() + ":" + getPort() + "/" + getBdd(), getUsr(), getPwd());
//        return tmp;
//    }

    public java.sql.Connection ConexionMSSQL() throws SQLException{
        java.sql.Connection tmp	= null;
        DriverManager.registerDriver(new SQLServerDriver());
        tmp = java.sql.DriverManager.getConnection("jdbc:sqlserver://" +  getSvr() + ";DatabaseName=" + getBdd(), getUsr(), getPwd());
        //tmp = java.sql.DriverManager.getConnection("jdbc:microsoft:sqlserver://" +  getSvr() + ";DatabaseName=" + getBdd()+";selectMethod="+selectMethod, getUsr(), getPwd());
        //tmp = java.sql.DriverManager.getConnection("jdbc:microsoft:sqlserver://" +  getSvr() + ";DatabaseName=" + getBdd(), getUsr(), getPwd());
        return tmp;
    } 
}
