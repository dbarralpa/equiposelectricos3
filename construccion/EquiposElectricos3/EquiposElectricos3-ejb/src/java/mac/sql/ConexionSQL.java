/*
 * ConexionSQL.java
 *
 * Created on 16 de noviembre de 2011, 17:20
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.sql;


import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class ConexionSQL {

    private ModServidorSQL MSSQL;
    private Connection con;
    public final static int MS_LOCAL = 1;

    public ConexionSQL() {
        MSSQL = new ModServidorSQL();
        con = null;
        Properties prop = null;
        try {
            prop = loadProperties("Application.properties", this.getClass());
            MSSQL.setSvr("SCH06");
            MSSQL.setPort(1433);
            MSSQL.setBdd("MAC");
            MSSQL.setUsr("i_fase");
            MSSQL.setPwd("if9632");
            MSSQL.setPrefix("");

        } catch (Exception ex) {
            PrmApp.addError(ex, true);
        }
    }

    public static Properties loadProperties(String resourceName, Class cl) {
        Properties properties = new Properties();
        ClassLoader loader = cl.getClassLoader();
        try {
            InputStream in = loader.getResourceAsStream(resourceName);
            if (in != null) {
                properties.load(in);
            }

        } catch (IOException ex) {
            PrmApp.addError(ex, true);
        }
        return properties;
    }

    public boolean openSQL(int model) {
        boolean estado = false;
        try {
            if (model == 1) {
                con = MSSQL.ConexionMSSQL();

                MSSQL.setActiva(true);
            } else {
                con = null;
            }

            if (con != null) {
                con.setAutoCommit(false);
                estado = true;
            } else {
                estado = false;
            }
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
        return estado;
    }

    public void closeSQL() {
        try {
            con.close();
            con = null;
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
    }

    public void commitSQL(boolean estado) {
        try {
            if (con != null) {
                if (estado) {
                    con.commit();
                } else {
                    con.rollback();
                }
            }
        } catch (SQLException ex) {
            PrmApp.addError(ex, true);
        }
    }

    private ModServidorSQL getConActiva() {
        if (MSSQL.isActiva()) {
            return MSSQL;
        } else {
            return new ModServidorSQL();
        }
    }

    public PreparedStatement getPrepareStatement(String query) throws SQLException {
        return con.prepareStatement(query);
    }

    public ResultSet SelectReg(String tabla) throws SQLException {
        return SelectReg(tabla, "*", null);
    }

    public ResultSet SelectReg(String tabla, String condicion) throws SQLException {
        return SelectReg(tabla, "*", condicion, "");
    }

    public ResultSet SelectReg(String tabla, String campos, String condicion) throws SQLException {
        return SelectReg(tabla, campos, condicion, "");
    }

    public ResultSet SelectReg(String tabla, String campos, String condicion, String orden) throws SQLException {
        String sSQL = "";
        ResultSet rs = null;
        String tTabla = "";
        for (int i = 0; i < tabla.split(",").length; i++) {
            tTabla += getConActiva().getPrefix().concat(tabla.split(",")[i].trim());
            if (tabla.split(",").length > 1 && (tabla.split(",").length - 1 > i)) {
                tTabla = tTabla + ", ";
            }
        }
        if (con != null) {
            sSQL = (new StringBuilder()).append("SELECT ").append(campos).append(" FROM ").append(tTabla).toString();
            if (condicion != null) {
                sSQL = (new StringBuilder()).append(sSQL).append(" WHERE ").append(condicion).toString();
            }
            if (!orden.equals("")) {
                sSQL = (new StringBuilder()).append(sSQL).append(" ORDER BY ").append(orden).toString();
            }
            rs = con.createStatement().executeQuery(sSQL);
        }
        return rs;
    }

    public ResultSet ExecuteQuery(String sql) throws SQLException {
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery(sql);
        }
        return rs;
    }

    public String CallFunction(String funcion, String parametros) throws SQLException {
        return CallFunction(funcion, parametros, true);
    }

    public String CallFunction(String funcion, String parametros, boolean nulo) throws SQLException {
        String aux = "";
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery((new StringBuilder()).append("SELECT ").append(getConActiva().getPrefix()).append(funcion).append("(").append(parametros).append(")").toString());
            if (rs.next()) {
                if (!nulo) {
                    if (rs.getString(1) != null) {
                        aux = rs.getString(1);
                    } else {
                        aux = "";
                    }
                } else {
                    aux = rs.getString(1);
                }
            } else {
                aux = "";
            }
        }
        return aux;
    }

    public double CallFunction_Dbl(String funcion, String parametros) throws SQLException {
        double aux = 0.0;
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery((new StringBuilder()).append("SELECT ").append(getConActiva().getPrefix()).append(funcion).append("(").append(parametros).append(")").toString());
            if (rs.next()) {
                if (rs.getString(1) != null) {
                    aux = rs.getDouble(1);
                } else {
                    aux = 0.0;
                }
            } else {
                aux = 0.0;
            }
        }
        return aux;
    }

    public boolean DeleteReg(String tabla, String condicion) throws SQLException {
        boolean estado = false;
        if (con.createStatement().executeUpdate((new StringBuilder()).append("DELETE FROM ").append(getConActiva().getPrefix()).append(tabla).append(" WHERE ").append(condicion).toString()) > 0) {
            estado = true;
        }
        return estado;
    }

    public boolean InsertReg(String tabla, String campos, String valores) throws SQLException {
        boolean estado = false;
        valores = valores.replace(',', '.');
        valores = valores.replaceAll("'", "\264");
        valores = valores.replaceAll("\n", "");
        valores = valores.replaceAll("\r", "");
        valores = (new StringBuilder()).append("'").append(valores.replaceAll("\267", "','")).append("'").toString();
        valores = valores.replaceAll("'SYSDATE'", "SYSDATE");
        valores = valores.replaceAll("'NULL'", "Null");
        valores = valores.replaceAll("'null'", "Null");
        String val = (new StringBuilder()).append("INSERT INTO ").append(getConActiva().getPrefix()).append(tabla).append("(").append(campos).append(") VALUES(").append(valores).append(")").toString();
        if (con.createStatement().executeUpdate(val) > 0) {
            estado = true;
        }

        return estado;
    }

    public boolean UpdateReg(String tabla, String campos, String valores, String condicion) throws SQLException {
        boolean estado = false;
        String sSQL = "";
        String cc[] = null;
        String vv[] = null;
        valores = valores.replace(',', '.');
        valores = valores.replaceAll("'", "\264");
        valores = valores.replaceAll("\n", "");
        valores = valores.replaceAll("\r", "");
        cc = campos.split(",");
        vv = valores.split("\267");
        for (int i = 0; i < cc.length; i++) {
            if (!sSQL.equals("")) {
                sSQL = (new StringBuilder()).append(sSQL).append(", ").toString();
            }
            if (vv[i].toLowerCase().equals("null")) {
                sSQL = (new StringBuilder()).append(sSQL).append(cc[i]).append("=Null").toString();
            } else {
                sSQL = (new StringBuilder()).append(sSQL).append(cc[i]).append("='").append(vv[i]).append("'").toString();
            }
        }

        sSQL = (new StringBuilder()).append("UPDATE ").append(getConActiva().getPrefix()).append(tabla).append(" SET ").append(sSQL).toString();
        sSQL = (new StringBuilder()).append(sSQL).append(" WHERE ").append(condicion).toString();

        if (con.createStatement().executeUpdate(sSQL) > 0) {
            estado = true;
        }

        return estado;
    }

    public double getDouble_Function(String func, String param) throws SQLException {
        String sSQL = "";
        ResultSet rx = null;
        double tmp = 0.0;
        if (con != null) {
            sSQL = "SELECT " + getConActiva().getPrefix() + "." + func + "(" + param + ")";
            rx = con.createStatement().executeQuery(sSQL);
            if (rx.next()) {
                tmp = rx.getDouble(1);
            }
            rx.close();
        }
        return tmp;
    }

    public int getInt_Function(String func, String param) throws SQLException {
        String sSQL = "";
        ResultSet rx = null;
        int tmp = 0;
        if (con != null) {
            sSQL = (new StringBuilder()).append("SELECT ").append(getConActiva().getPrefix()).append(func).append("(").append(param).append(")").toString();
            rx = con.createStatement().executeQuery(sSQL);
            if (rx.next()) {
                tmp = rx.getInt(1);
            }
            rx.close();
        }
        return tmp;
    }

    public ResultSet ExecuteProcedure(String proced, String args) throws SQLException {
        ResultSet rs = null;
        if (con != null) {
            rs = con.createStatement().executeQuery((new StringBuilder()).append(proced).append(" ").append(args).toString());
        }
        return rs;
    }
}

