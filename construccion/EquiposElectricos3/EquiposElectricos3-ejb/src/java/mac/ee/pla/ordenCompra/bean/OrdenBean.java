package mac.ee.pla.ordenCompra.bean;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Vector;
import javax.ejb.*;
import mac.ee.pla.ordeCompra.clase.ArchivoAdjunto;
import mac.ee.pla.ordeCompra.clase.OrdenDeCompra;
import mac.ee.pla.ordeCompra.clase.Sap;
import sis.conexion.Conexion;
import sis.logger.Loger;


public class OrdenBean implements SessionBean, OrdenRemoteBusiness {
    private SessionContext context;
    
    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    
    
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")
    
    private Statement st=null;
    private ResultSet rs = null;
    private Statement stm=null;
    private ResultSet rst = null;
    private Conexion myConexion= new Conexion();
    private Conexion mConexion= new Conexion();
    private Connection conMAC=null;
    private Connection conENS=null;
    java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
    String date = formateador.format(new java.util.Date());
    private  OrdenDeCompra ordenCompra =  new OrdenDeCompra();
              
//-------------------------------------------------------------------------------------------------------------------------------     
     // M�todo que trae todas las ordenes de compra inv�lidas
    public Vector ordenDeCompraInvalidas(String fechaActual, int identificador){
        Vector vec =new Vector();  
        String query = null;
        try{
            if(identificador == 1 || identificador == 2 || identificador == 3 || identificador == 4 || identificador == 5 || identificador == 6){
                /*query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "
                        +" FROM MAC.dbo.TOCompra TOC "
                        +" LEFT OUTER JOIN MAC.dbo.MArticulo MA " 
                        +" ON TOC.COD_PRO = MA.CODIGO "
                        +" WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND "
                        +" FEC_ENT < '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.NUMERO";
                 */
                  query = "   select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega < '"+fechaActual+"' "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         //+"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD ";
            }
            
            /*if(identificador == 2){
                query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "
                        +" FROM MAC.dbo.TOCompra TOC "
                        +" LEFT OUTER JOIN MAC.dbo.MArticulo MA " 
                        +" ON TOC.COD_PRO = MA.CODIGO "
                        +" WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND "
                        +" FEC_ENT < '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.RUT";
            }if(identificador == 3){
                 query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "
                        +" FROM MAC.dbo.TOCompra TOC "
                        +" LEFT OUTER JOIN MAC.dbo.MArticulo MA " 
                        +" ON TOC.COD_PRO = MA.CODIGO "
                        +" WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND "
                        +" FEC_ENT < '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.FECHA";
            }if(identificador == 4){
                 query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "
                        +" FROM MAC.dbo.TOCompra TOC "
                        +" LEFT OUTER JOIN MAC.dbo.MArticulo MA " 
                        +" ON TOC.COD_PRO = MA.CODIGO "
                        +" WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND "
                        +" FEC_ENT < '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.FEC_ENT";
            }if(identificador == 5){
                 query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "
                        +" FROM MAC.dbo.TOCompra TOC "
                        +" LEFT OUTER JOIN MAC.dbo.MArticulo MA " 
                        +" ON TOC.COD_PRO = MA.CODIGO "
                        +" WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND "
                        +" FEC_ENT < '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.COD_PRO";
            }if(identificador == 6){
                 query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "
                        +" FROM MAC.dbo.TOCompra TOC "
                        +" LEFT OUTER JOIN MAC.dbo.MArticulo MA " 
                        +" ON TOC.COD_PRO = MA.CODIGO "
                        +" WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND "
                        +" FEC_ENT < '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY MA.DESCRIPCION";
            }*/
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getLong(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             orden.setDescripcion(rs.getString(9));
             vec.addElement(orden);
          
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraInvalidas(3) " + e.getMessage(),0);
       }
        return vec;
   }
   
//--------------------------------------------------------------------------------------------------------------------------------
     // M�todo que trae todas las ordenes de compra inv�lidas por material 
     public Vector ordenDeCompraInvalidas(String material){
        Vector vec =new Vector(); 
       try{           
         /*String query =   " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR,TOC.MONEDA,TOC.PRECIO"
                        + " FROM MAC.dbo.TOCompra TOC"
                        + " LEFT OUTER JOIN MAC.dbo.MArticulo MA" 
                        + " ON TOC.COD_PRO = MA.CODIGO"
                        + " WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' and FEC_ENT < '"+date+"' AND COD_PRO = '" + material + "' AND CAN_PEN > 0"
                        + " ORDER BY TOC.FEC_ENT ";
          */
           String query = "    select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC,per.PER_NOMBRE,enc.MVE_Moneda,tc.MVD_PrecioAjustado "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega < '"+date+"' "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         //+"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO "
                         +"   and tc.MVD_CodProd = '" + material + "' ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query);
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getLong(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             orden.setDescripcion(rs.getString(9));
             orden.setProveedor(rs.getString(10));
             orden.setMoneda(rs.getFloat(11));
             orden.setPrecio(rs.getFloat(12));
             vec.addElement(orden);
          
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraInvalidas(2) " + e.getMessage(),0);
       }
        return vec;
   }
  //--------------------------------------------------------------------------------------------------------------------------------
     // M�todo que trae todas las ordenes de compra inv�lidas por material 
     public Vector ordenDeCompraInvalidas(String material ,int top,String nada){
        Vector vec =new Vector(); 
        int restar = 10;
       try{           
         if(top < restar){
           top = restar - top; 
         }
         /*String query =   " SELECT TOP " + top + " TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR,TOC.MONEDA,TOC.PRECIO"
                        + " FROM MAC.dbo.TOCompra TOC"
                        + " LEFT OUTER JOIN MAC.dbo.MArticulo MA" 
                        + " ON TOC.COD_PRO = MA.CODIGO"
                        + " WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' and FEC_ENT < '"+date+"' AND COD_PRO = '" + material + "' "
                        + " ORDER BY TOC.CAN_PEN DESC";
          */
         //System.out.println("TOP: " + top);
          String query = " select TOP " + top + " enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha) as FechaEntrada,tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                        +" convert(datetime,tc.MVD_FechaEntrega) as FechaEntrega,pro.PRO_DESC,per.PER_NOMBRE,enc.MVE_Moneda,tc.MVD_PrecioAjustado, "
                        +" (select TAB_Desc from SiaSchaffner.dbo.SYS_TABLAS where TAB_CodTabla = 9 and TAB_Codigo = enc.MVE_Moneda) as Desc_Moneda "
                        +" from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                        +" where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega < '"+date+"' "
                        +" and enc.MVE_CodSistema=7  "
                        +" and enc.MVE_CodClase =1  "
                        +" and enc.MVE_TipoDoc = 1 "
                        +" and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                        //+" and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                        +" and tc.MVD_CodProd = pro.PRO_CODPROD "
                        +" and enc.MVE_CodPersona = per.PER_CODIGO "
                        +" and tc.MVD_CodProd = '" + material + "' "
                        +" order by FechaEntrada DESC";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query);
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getLong(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             orden.setDescripcion(rs.getString(9));
             orden.setProveedor(rs.getString(10));
             orden.setMoneda(rs.getFloat(11));
             orden.setPrecio(rs.getFloat(12));
             orden.setDescMoneda(rs.getString(13));
             vec.addElement(orden);
          
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraInvalidas(1) " + e.getMessage(),0);
       }
        return vec;
   }
//--------------------------------------------------------------------------------------------------------------------------------     
       public Vector ordenDeCompraValidas(String codPro){
        Vector vec =new Vector();
         
       try{
           
        
        /* String query =   " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR,TOC.PRECIO,TOC.MONEDA"
                        + " FROM MAC.dbo.TOCompra TOC"
                        + " LEFT OUTER JOIN MAC.dbo.MArticulo MA" 
                        + " ON TOC.COD_PRO = MA.CODIGO"
                        + " WHERE TIP_REG = 1 AND COD_PRO = '" + codPro + "' AND CAN_PEN > 0 AND TIP_COMPRA <>'G' AND FEC_ENT >= '"+date+"'";
         */
           String query = "   select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC,per.PER_NOMBRE,enc.MVE_Moneda,tc.MVD_PrecioAjustado "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega >= '"+date+"' "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         //+"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO "
                         +"   and tc.MVD_CodProd = '" + codPro + "' ";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query);
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getLong(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             orden.setDescripcion(rs.getString(9));
             orden.setProveedor(rs.getString(10));
             orden.setPrecio(rs.getFloat(11));
             orden.setMoneda(rs.getFloat(12));
             vec.addElement(orden);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraValidas(String codPro) " + e.getMessage(),0);
       }
        return vec;
   }
 //--------------------------------------------------------------------------------------------------------------------------------     
       public float ordenDeCompraPendientes(String codPro){
        float cant = 0;
         
       try{
           
        
           
           String query = "   select (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) from  SiaSchaffner.dbo.STO_MOVDET tcc "
                         +"   where (tcc.MVD_CodProd = '" + codPro + "' COLLATE SQL_Latin1_General_CP850_CS_AS and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 "
                         +"   and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0)  ";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query);
             
             while(rs.next()){
             cant = rs.getFloat(1);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , " ordenDeCompraPendientes " + e.getMessage(),0);
       }
        return cant;
   }      
//--------------------------------------------------------------------------------------------------------------------------------     
       public Vector ordenDeCompraValidasComprasRequeridas(String codPro){
        Vector vec =new Vector();
         
       try{
           
        
         /*String query =   " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR,TOC.PRECIO,TOC.MONEDA"
                        + " FROM MAC.dbo.TOCompra TOC"
                        + " LEFT OUTER JOIN MAC.dbo.MArticulo MA" 
                        + " ON TOC.COD_PRO = MA.CODIGO"
                        + " WHERE TIP_REG = 1 AND COD_PRO = '" + codPro + "' AND TIP_COMPRA <>'G' AND FEC_ENT >= '"+date+"'";
          */
           String query = "   select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC,per.PER_NOMBRE,enc.MVE_Moneda,tc.MVD_PrecioAjustado, "
                         +"   (select TAB_Desc from SiaSchaffner.dbo.SYS_TABLAS where TAB_CodTabla = 9 and TAB_Codigo = enc.MVE_Moneda) as Desc_Moneda "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega >= '"+date+"' "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                        // +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO "
                         +"   and tc.MVD_CodProd = '" + codPro + "'  ";
             conMAC = myConexion.getMACConnector();
             st = conMAC.createStatement();
             rs = st.executeQuery(query);
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getLong(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             orden.setDescripcion(rs.getString(9));
             orden.setProveedor(rs.getString(10));
             orden.setMoneda(rs.getFloat(11));
             orden.setPrecio(rs.getFloat(12));
             orden.setDescMoneda(rs.getString(13));
             vec.addElement(orden);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , " ordenDeCompraValidasComprasRequeridas() " + e.getMessage(),0);
       }
        return vec;
   }       
//--------------------------------------------------------------------------------------------------------------------------------     
      public Vector ordenDeCompraValidas(String fechaActual, int identificador ){
        Vector vec =new Vector();
        String query = null;
        try{
            if(identificador == 7 || identificador == 8 || identificador == 9 || identificador == 10 || identificador == 11 || identificador == 12){
            /*query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "
                    +" FROM MAC.dbo.TOCompra TOC "
                    +" LEFT OUTER JOIN MAC.dbo.MArticulo MA "
                    +" ON TOC.COD_PRO = MA.CODIGO "
                    +" WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 AND FEC_ENT >=  '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.NUMERO";
             */
                 query = "    select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC,per.PER_NOMBRE "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega >= '"+fechaActual+"' "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         //+"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO ";
                
            }
            
            /*if(identificador == 8){
            query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "
                    +" FROM MAC.dbo.TOCompra TOC "
                    +" LEFT OUTER JOIN MAC.dbo.MArticulo MA "
                    +" ON TOC.COD_PRO = MA.CODIGO "
                    +" WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 AND FEC_ENT >=  '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.RUT";
            }if(identificador == 9){
            query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "
                    +" FROM MAC.dbo.TOCompra TOC "
                    +" LEFT OUTER JOIN MAC.dbo.MArticulo MA "
                    +" ON TOC.COD_PRO = MA.CODIGO "
                    +" WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 AND FEC_ENT >=  '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.FECHA";
            }if(identificador == 10){
            query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "
                    +" FROM MAC.dbo.TOCompra TOC "
                    +" LEFT OUTER JOIN MAC.dbo.MArticulo MA "
                    +" ON TOC.COD_PRO = MA.CODIGO "
                    +" WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 AND FEC_ENT >=  '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.FEC_ENT";
            }if(identificador == 11){
            query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "
                    +" FROM MAC.dbo.TOCompra TOC "
                    +" LEFT OUTER JOIN MAC.dbo.MArticulo MA "
                    +" ON TOC.COD_PRO = MA.CODIGO "
                    +" WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 AND FEC_ENT >=  '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY TOC.COD_PRO";
            }if(identificador == 12){
            query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "
                    +" FROM MAC.dbo.TOCompra TOC "
                    +" LEFT OUTER JOIN MAC.dbo.MArticulo MA "
                    +" ON TOC.COD_PRO = MA.CODIGO "
                    +" WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 AND FEC_ENT >=  '"+fechaActual+"' AND COD_PRO not like 'M%' ORDER BY MA.DESCRIPCION";
            }*/
             conMAC = myConexion.getMACConnector();
             st = conMAC.createStatement();
             rs = st.executeQuery(query);
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getLong(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             orden.setDescripcion(rs.getString(9));
             orden.setProveedor(rs.getString(10));
             vec.addElement(orden);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraValidas() " + e.getMessage(),0);
       }
        return vec;
   }   
       
//------------------------------------------------------------------------------------------------------------------------------
                                        
                                        
  public Vector ordenDeCompraValidas(String codPro,String dat){
        Vector vec =new Vector();
        //dat = dat.substring(6,10) + dat.substring(3,5) + dat.substring(0,2);
       try{
           
        
         //String query = "SELECT CAN_PEN,FEC_ENT FROM MAC.dbo.TOCompra WHERE TIP_REG = 1 AND COD_PRO = '" + codPro + "' AND TIP_COMPRA <>'G'   AND CAN_PEN != 0 AND FEC_ENT >= '"+dat+"'";
           String query = "   select (mo.MVD_Cant-mo.MVD_CantAsignada) as CAN_PEN,convert(datetime,mo.MVD_FechaEntrega) "
                         +"   from SiaSchaffner.dbo.STO_MOVDET mo "
                         +"   where mo.MVD_CodSistema = 7 and mo.MVD_CodClase = 1 AND mo.MVD_TipoDoc = 1 "
                         +"   and mo.MVD_FechaEntrega = '"+dat+"'  "
                         +"   and mo.MVD_CodProd  = '" + codPro + "' ";
                         //+"   and mo.MVD_TipoProd in(1,2,5,6,8) ";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setCan_pedida(rs.getFloat(1));
             orden.setFecha_entrega(rs.getDate(2));
             vec.addElement(orden);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraValidas(String codPro) " + e.getMessage(),0);
       }
        return vec;
   }
//--------------------------------------------------------------------------------------------------------------------------------     
       public Vector ordenDeCompraPorNumero(String numero){
        Vector vec =new Vector();
         
       try{
           
        
         /*String query =   " select toc.COD_PRO from MAC.dbo.TOCompra toc "
                         +" left outer join  MAC.dbo.ComprasRequeridas com "
                         +" on(toc.COD_PRO = com.Codigo and com.verificar <> 3) "
                         +" where toc.TIP_REG = 1 and  toc.NUMERO = " + numero + " "
                         +" and com.Codigo is not null ";
          */
          String query =" select tc.MVD_CodProd "
                       +" from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, MAC.dbo.ComprasRequeridas com "
                       +" where tc.MVD_CodProd = com.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS and com.verificar <> 3 "
                       +" and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                       +" and enc.MVE_CodSistema=7  "
                       +" and enc.MVE_CodClase =1  "
                       +" and enc.MVE_TipoDoc = 1 "
                       +" and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                       +" and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                       +" and tc.MVD_TipoProd in(1,2,5,6,8) "
                       +" and enc.MVE_FolioFisico = "+ numero +" " ;
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query);
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setCod_producto(rs.getString(1));
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  ordenDeCompraPorNumero() " + e.getMessage(),0);
       }
        return vec;
   }
//--------------------------------------------------------------------------------------------------------------------------------     
   /*  public float sumarOrdenDeCompraValidas(String codPro){
       float canPedida=0;
       try{
           
        
         String query = "SELECT SUM(cantidad) FROM MAC.dbo.ordenesDeCompra WHERE codMaterial = '" + codPro + "' and fechaLlegada >= '"+date+"'";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
             
             while(rs.next()){
             canPedida = rs.getFloat(1);

             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  sumarOrdenDeCompraValidas() " + e.getMessage(),0);
       }
        return canPedida;
   } */    
//--------------------------------------------------------------------------------------------------------------------------------     
     public float sumarOrdenDesVigentes(String codPro){
       float canPedida=0;
       try{
           
        
         String query = " select sum(mov.MVD_Cant-mov.MVD_CantAsignada) "
                       +" from SiaSchaffner.dbo.STO_MOVDET mov,SiaSchaffner.dbo.STO_MOVENC enc "
                       +" where mov.MVD_CodSistema=7 and mov.MVD_CodClase =1 AND mov.MVD_TipoDoc = 1 "
                       +" and enc.MVE_CodSistema=mov.MVD_CodSistema and enc.MVE_CodClase = mov.MVD_CodClase AND enc.MVE_TipoDoc = mov.MVD_TipoDoc "
                       +" and  enc.MVE_NumeroDoc = mov.MVD_NumeroDoc "
                       +" and MVD_FechaEntrega >= '"+date+"' "
                       +" and MVD_CodProd = '" + codPro + "' "
                       +" and (mov.MVD_Cant-mov.MVD_CantAsignada) > 0 ";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
             
             while(rs.next()){
             canPedida = rs.getFloat(1);

             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , " sumarOrdenDesValidas() " + e.getMessage(),0);
       }
        return canPedida;
   }          
//--------------------------------------------------------------------------------------------------------------------------------     
     public float sumarOrdenDesVencidas(String codPro){
       float canPedida=0;
       try{
           
        
         String query = " select sum(mov.MVD_Cant-mov.MVD_CantAsignada) "
                       +" from SiaSchaffner.dbo.STO_MOVDET mov,SiaSchaffner.dbo.STO_MOVENC enc "
                       +" where mov.MVD_CodSistema=7 and mov.MVD_CodClase =1 AND mov.MVD_TipoDoc = 1 "
                       +" and enc.MVE_CodSistema=mov.MVD_CodSistema and enc.MVE_CodClase = mov.MVD_CodClase AND enc.MVE_TipoDoc = mov.MVD_TipoDoc "
                       +" and  enc.MVE_NumeroDoc = mov.MVD_NumeroDoc "
                       +" and MVD_FechaEntrega < '"+date+"' "
                       +" and MVD_CodProd = '" + codPro + "' "
                       +" and (mov.MVD_Cant-mov.MVD_CantAsignada) > 0 ";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
             
             while(rs.next()){
             canPedida = rs.getFloat(1);

             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , " sumarOrdenDesVencidas() " + e.getMessage(),0);
       }
        return canPedida;
   }               
//-------------------------------------------------------------------------------------------------------------------------------- 

  public float stock(String codPro){
    float stock = 0;
         
       try{
           
         
        // String query = "SELECT STOCK FROM MAC.dbo.MArticulo WHERE CODIGO = '" + codPro + "'";
           String query = "SELECT sum(ml.PSL_Saldo) FROM SiaSchaffner.dbo.STO_PRODSAL ml WHERE ml.PSL_CodProd  = '" + codPro + "' and PSL_CodBodega <> 7";
             conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
             
             while(rs.next()){
             stock = rs.getFloat(1);
          
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR stock() " + e.getMessage(),0);
       }
        return stock;
   }
//--------------------------------------------------------------------------------------------------------------------------------
     public float stockPlanta(String codMaterial){
       byte contador = 0;
       float cantidadLoad=0;
       try{
         conENS = mConexion.getENSConnector();
         stm = conENS.createStatement();
         String query4 = "SELECT Cantidad FROM ENSchaffner.dbo.StockPlanta WHERE CodigoArticulo = '" + codMaterial + "'";
         rst = stm.executeQuery(query4);
         while(rst.next()){
          cantidadLoad = rst.getFloat(1);
         }
                 rst.close();
                 stm.close();
                 conENS.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR stockPlanta(): " + e.getMessage(),0);
           return 0;
       }
       return cantidadLoad;
   }
//-------------------------------------------------------------------------------------------------------------------------------- 
   
   public float buscarStockPLanta(String codigo){
       float contador = 0;
        try{
         String query4 = "SELECT cantidad FROM MAC.dbo.stockPlanta WHERE codMaterial = '" + codigo + "'";
         conMAC = myConexion.getMACConnector();
         st = conMAC.createStatement();
         rs = st.executeQuery(query4);
         while(rs.next()){
           
               contador =  rs.getFloat(1);  
           }
         st.close();
         rs.close();
         conMAC.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , "ERROR buscarStockPLanta(): " + e.getMessage(),0);
        }
       return contador;
   }
 //--------------------------------------------------------------------------------------------------------------------------------     
       //inserta una nueva orden de compra
       public int insertarOrdenDeCompra(int numOrden , int linea , String codMaterial , String fleje , String fechaLlegada , float cantidad){
       try{
       
         
         String query ="INSERT INTO MAC.dbo.ordenesDeCompra (numOrden,linea,codMaterial,fleje,fechaLlegada,cantidad) VALUES(" + numOrden + "," + linea + ",'" + codMaterial + "','" + fleje + "','" + fechaLlegada + "' , " + cantidad + ")";
         //System.out.println(query);
         conMAC = myConexion.getMACConnector();
         st = conMAC.createStatement();
         st.execute(query); 
         st.close();
         conMAC.close(); 
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR insertarOrdenDeCompra(): " + e.getMessage(),0);
           return 0;
       }
               return 1;
       
   }
       
//--------------------------------------------------------------------------------------------------------------------------------     
       //inserta una nueva orden de compra
       public int borrarOrdenDeCompra(int numOrden , int linea){
       try{
       
         
         String query ="DELETE FROM MAC.dbo.ordenesDeCompra WHERE numOrden = " + numOrden + " AND linea = " + linea + "";
         //System.out.println(query);
         conMAC = myConexion.getMACConnector();
         st = conMAC.createStatement();
         st.execute(query); 
         st.close();
         conMAC.close(); 
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR borrarOrdenDeCompra(): " + e.getMessage(),0);
           return 0;
       }
               return 1;
       
   }

//-------------------------------------------------------------------------------------------------------------------------------- 
       

// Este m�todo se usa para cambiar la fecha de llegada 
     // atarsada por una desp�es de la fecha actual 
      public Vector cambiarFechaLlegada(int numero){
       Vector fecha =new Vector();
       try{
         
         //String query = "SELECT NUMERO,LINEA,RUT,FECHA,COD_PRO,CAN_PED,CAN_PEN,FEC_ENT from dbo.TOCompra where TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' and FEC_ENT < '"+date+"' and NUMERO = "+ numero;
           String query = "    select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN "
                         +"   ,convert(datetime,tc.MVD_FechaEntrega) "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         +"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and enc.MVE_FolioFisico = "+ numero + " "
                         +"   and tc.MVD_FechaEntrega = '"+date+"'";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
             
             while(rs.next()){
             OrdenDeCompra orden =  new OrdenDeCompra();
             orden.setNum_orden_compra(rs.getInt(1));
             orden.setLinea(rs.getInt(2));
             orden.setRut(rs.getFloat(3));
             orden.setFecha_pedido(rs.getDate(4));
             orden.setCod_producto(rs.getString(5));
             orden.setCan_pedida(rs.getFloat(6));
             orden.setCan_pendiente(rs.getFloat(7));
             orden.setFecha_entrega(rs.getDate(8));
             fecha.addElement(orden);        
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR cambiarFechaLlegada(): " + e.getMessage(),0);
       }
        return fecha;
   }
    
 //--------------------------------------------------------------------------------------------------------------------------------     
       // se inserta el registro con la nueva fecha de entrega de materiales
       public void insertarRegistro(int numero,int linea,float rut,String fechaPedido,String codProducto,float canPedida,float canPendiente,String fechaEntrega,String fechaNueva){
       Vector fecha =new Vector();
       try{   
         String query ="INSERT INTO OrdenesCompraVencidas (NUM_ORDEN,LINEA,RUT,FEC_PEDIDO,COD_PRODUCTO,CAN_PEDIDA,CAN_PENDIENTE,FEC_ENTREGA,FEC_NUEVA) VALUES('" + numero + "','" + linea + "','" + rut + "','" + fechaPedido + "' , '" + codProducto + "' , '" + canPedida + "' , '" + canPendiente + "' , '" + fechaEntrega + "', '" + fechaNueva + "')";
       
         conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              st.execute(query); 
         st.close();
         conMAC.close(); 
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR insertarRegistro(): " + e.getMessage(),0);
       }
  
   }
//--------------------------------------------------------------------------------------------------------------------------------//
 
 public Vector buscarPorRutCliente(float rut){      
       Vector rutCliente2 = new Vector();       
       try{                  
        /* String query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION,TOC.PROVEEDOR "+
                        " FROM MAC.dbo.TOCompra TOC"+
                        " LEFT OUTER JOIN MAC.dbo.MArticulo MA"+
                        " ON TOC.COD_PRO = MA.CODIGO"+
                        " WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0 "+
                        " AND COD_PRO not like 'M%' AND RUT = '"+rut+"'";
         */
           String query = " select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC,per.PER_NOMBRE "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1  "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         +"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO "
                         +"   and enc.MVE_CodPersona = '"+rut+"'  ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra datosPorRut2 =  new OrdenDeCompra();                    
                     datosPorRut2.setNum_orden_compra(rs.getLong(1));
                     datosPorRut2.setLinea(rs.getInt(2));
                     datosPorRut2.setRut(rs.getFloat(3));
                     datosPorRut2.setFecha_pedido(rs.getDate(4));
                     datosPorRut2.setCod_producto(rs.getString(5));
                     datosPorRut2.setCan_pedida(rs.getFloat(6));
                     datosPorRut2.setCan_pendiente(rs.getFloat(7));
                     datosPorRut2.setFecha_entrega(rs.getDate(8));
                     datosPorRut2.setDescripcion(rs.getString(9));
                     datosPorRut2.setProveedor(rs.getString(10));
                     rutCliente2.addElement(datosPorRut2);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorRutCliente() " + e.getMessage(),0);
       }
        return rutCliente2;
  }
//------------------------------------------------------------------------------------------------------------------------------//
 public Vector buscarPorNumeroDeOrden(){      
     
     Vector numeroOrden = new Vector();       
       try{                  
         //String query = "  select DISTINCT(NUMERO) FROM MAC.dbo.TOCompra TOC ORDER BY NUMERO ";
           String query = "   select distinct(enc.MVE_FolioFisico) "
                         +"   from  SiaSchaffner.dbo.STO_MOVENC enc "
                         +"   where "
                         +"   enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1 "  
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   order by enc.MVE_FolioFisico ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra numeroDeLaOrden =  new OrdenDeCompra();
                     numeroDeLaOrden.setNum_orden_compra(rs.getLong(1));                    
                     numeroOrden.addElement(numeroDeLaOrden);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorNumeroDeOrden() " + e.getMessage(),0);
       }
        return numeroOrden;
  }
 //---------------------------------------------------------------------------------------------------------------------------//
  public Vector buscarPorRutCliente(){      
       Vector rutCliente = new Vector();       
       try{                  
         //String query = " SELECT DISTINCT(RUT) FROM MAC.dbo.TOCompra ORDER BY RUT ";
           String query = "   select distinct(enc.MVE_CodPersona) "
                         +"   from  SiaSchaffner.dbo.STO_MOVENC enc "
                         +"   where "
                         +"   enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1 "  
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   order by enc.MVE_CodPersona ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra datosPorRut =  new OrdenDeCompra();
                     datosPorRut.setRut(rs.getFloat(1));                    
                     rutCliente.addElement(datosPorRut);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorRutCliente() " + e.getMessage(),0);
       }
        return rutCliente;
  } 
 //---------------------------------------------------------------------------------------------------------------------------//
 public Vector buscarPorProveedor(){      
       Vector proveedor = new Vector();       
       try{                  
         //String query = " SELECT DISTINCT(PROVEEDOR) FROM MAC.dbo.TOCompra ORDER BY PROVEEDOR";
           String query = "   select distinct(per.PER_NOMBRE) "
                         +"   from  SiaSchaffner.dbo.STO_MOVENC enc,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where "
                         +"   enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1 "  
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO "
                         +"   order by per.PER_NOMBRE ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){
                  
                     OrdenDeCompra datosDelProveedor =  new OrdenDeCompra();
                     datosDelProveedor.setProveedor(rs.getString(1));                    
                     proveedor.addElement(datosDelProveedor);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorProveedor() " + e.getMessage(),0);
       }
        return proveedor;
  }
 //----------------------------------------------------------------------------------------------------------------------------//
 public Vector buscarPorDescripcion(){      
       Vector descripcion = new Vector();       
       try{                  
         //String query = " SELECT DISTINCT(DESCRIPCION) FROM MAC.dbo.MArticulo ORDER BY DESCRIPCION ";
           String query = "   select distinct(PRO_DESC) "
                         +"   from SiaSchaffner.dbo.STO_PRODUCTO ml "
                         +"   where ml.PRO_CODTIPO in (1,5,8)   order by PRO_DESC ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){
                  
                     OrdenDeCompra datosDelProducto =  new OrdenDeCompra();
                     datosDelProducto.setDescripcion(rs.getString(1));                    
                     descripcion.addElement(datosDelProducto);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorDescripcion() " + e.getMessage(),0);
       }
        return descripcion;
  } 
//-----------------------------------------------------------------------------------------------------------------------------//
  public Vector buscarPorCodigo(){      
       Vector codigo = new Vector();       
       try{                  
         //String query = " SELECT DISTINCT(CODIGO) FROM MAC.dbo.MArticulo ORDER BY CODIGO";
           String query = "   select distinct(PRO_CODPROD) "
                         +"   from SiaSchaffner.dbo.STO_PRODUCTO ml "
                         +"   where ml.PRO_CODTIPO in (1,5,8)   order by ml.PRO_CODPROD  ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){
                  
                     OrdenDeCompra datosPorCodigo =  new OrdenDeCompra();
                     datosPorCodigo.setCod_producto(rs.getString(1));                    
                     codigo.addElement(datosPorCodigo);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorCodigo() " + e.getMessage(),0);
       }
        return codigo;
  } 

//----------------------------------------------------------------------------------------------------------------------------//
   public Vector mostrarDatosPorProveedor(String  proveedor){      
       Vector datosProveedor = new Vector();       
       try{                  
         /*String query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "+
                        " FROM MAC.dbo.TOCompra TOC "+
                        " LEFT OUTER JOIN MAC.dbo.MArticulo MA"+
                        " ON TOC.COD_PRO = MA.CODIGO "+
                        " WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND COD_PRO not like 'M%' "+
                        " AND TOC.PROVEEDOR = '"+proveedor+"'"+
                        " ORDER BY TOC.FECHA";
          */
           String query = " select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro,SiaSchaffner.dbo.SYS_PERSONA per "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1  "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         +"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and enc.MVE_CodPersona = per.PER_CODIGO "
                         +"   and per.PER_NOMBRE = '"+proveedor+"' "
                         +"   order by tc.MVD_Fecha  ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra verDatosProveedor =  new OrdenDeCompra();                 
                     verDatosProveedor.setNum_orden_compra(rs.getLong(1));
                     verDatosProveedor.setLinea(rs.getInt(2));
                     verDatosProveedor.setRut(rs.getFloat(3));
                     verDatosProveedor.setFecha_pedido(rs.getDate(4));
                     verDatosProveedor.setCod_producto(rs.getString(5));
                     verDatosProveedor.setCan_pedida(rs.getFloat(6));
                     verDatosProveedor.setCan_pendiente(rs.getFloat(7));                       
                     verDatosProveedor.setFecha_entrega(rs.getDate(8));              
                     verDatosProveedor.setDescripcion(rs.getString(9));
                     datosProveedor.addElement(verDatosProveedor);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  buscarPorRutCliente() " + e.getMessage(),0);
       }
        return datosProveedor;
 }
   
 //--------------------------------------------------------------------------------------------------------------------------//
    
    public Vector mostrarDatosPorCodigo(String  codigo){      
       Vector verPorCodigo = new Vector();       
       try{                  
         /*String query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "+
                        " FROM MAC.dbo.TOCompra TOC "+
                        " LEFT OUTER JOIN MAC.dbo.MArticulo MA"+
                        " ON TOC.COD_PRO = MA.CODIGO "+
                        " WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND COD_PRO not like 'M%' "+
                        " AND TOC.COD_PRO = '" + codigo + "' "+
                        " ORDER BY TOC.FECHA";
          */
           String query = "select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         //+"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and tc.MVD_CodProd = '" + codigo + "' "
                         +"   order by tc.MVD_Fecha  ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra verDatosPorCodigo =  new OrdenDeCompra();                    
                     verDatosPorCodigo.setNum_orden_compra(rs.getLong(1));
                     verDatosPorCodigo.setLinea(rs.getInt(2));
                     verDatosPorCodigo.setRut(rs.getFloat(3));
                     verDatosPorCodigo.setFecha_pedido(rs.getDate(4));
                     verDatosPorCodigo.setCod_producto(rs.getString(5));
                     verDatosPorCodigo.setCan_pedida(rs.getFloat(6));
                     verDatosPorCodigo.setCan_pendiente(rs.getFloat(7));                       
                     verDatosPorCodigo.setFecha_entrega(rs.getDate(8));              
                     verDatosPorCodigo.setDescripcion(rs.getString(9));
                     verPorCodigo.addElement(verDatosPorCodigo);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  mostrarDatosPorCodigo() " + e.getMessage(),0);
       }
        return verPorCodigo;
 }
  //---------------------------------------------------------------------------------------------------------------------------//  
    public Vector mostrarDatosPorDescripcionMateriaPrima(String descripcion){      
       Vector verPorDescripcion = new Vector();       
       try{                  
         /*String query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION "+
                        " FROM MAC.dbo.TOCompra TOC "+
                        " LEFT OUTER JOIN MAC.dbo.MArticulo MA"+
                        " ON TOC.COD_PRO = MA.CODIGO "+
                        " WHERE  TIP_REG = 1 AND CAN_PEN >0  AND TIP_COMPRA <>'G' AND COD_PRO not like 'M%' AND MA.DESCRIPCION IS NOT NULL"+
                        " AND MA.DESCRIPCION  = '"+descripcion+"'"+
                        " ORDER BY TOC.FECHA";
          */
           String query = " select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         +"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   and pro.PRO_DESC = '"+descripcion+"' "
                         +"   order by tc.MVD_Fecha"; 
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra verDatosPorDescripcion =  new OrdenDeCompra();                    
                     verDatosPorDescripcion.setNum_orden_compra(rs.getLong(1));
                     verDatosPorDescripcion.setLinea(rs.getInt(2));
                     verDatosPorDescripcion.setRut(rs.getFloat(3));
                     verDatosPorDescripcion.setFecha_pedido(rs.getDate(4));
                     verDatosPorDescripcion.setCod_producto(rs.getString(5));
                     verDatosPorDescripcion.setCan_pedida(rs.getFloat(6));
                     verDatosPorDescripcion.setCan_pendiente(rs.getFloat(7));                       
                     verDatosPorDescripcion.setFecha_entrega(rs.getDate(8));              
                     verDatosPorDescripcion.setDescripcion(rs.getString(9));
                     verPorDescripcion.addElement(verDatosPorDescripcion);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  mostrarDatosPorDescripcionMateriaPrima() " + e.getMessage(),0);
       }
        return verPorDescripcion;
 }
 //----------------------------------------------------------------------------------------------------------------------------//   
     public Vector listarOrdenes(){  
         
       Vector ordenes = new Vector();       
       try{                  
         String query = " SELECT id, orden FROM ordenesMRP  ORDER BY id ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra verOrdenes =  new OrdenDeCompra();
                     verOrdenes.setIdOrden(rs.getInt(1));   
                     verOrdenes.setNombreOrden(rs.getString(2));
                     ordenes.addElement(verOrdenes);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  listarOrdenes() " + e.getMessage(),0);
       }
        return ordenes;
  } 
     
 //-----------------------------------------------------------------------------------------------------------------------------// 
       public Vector listarMotorDeBusqueda(){           
       Vector motor = new Vector();       
       try{                  
         String query = " SELECT id , nombreBusqueda FROM motorBusquedaMRP ORDER BY id";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra verMotor =  new OrdenDeCompra();
                     verMotor.setIdOrden(rs.getInt(1));   
                     verMotor.setNombreOrden(rs.getString(2));
                     motor.addElement(verMotor);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  listarMotorDeBusqueda() " + e.getMessage(),0);
       }
        return motor;
  } 
//-------------------------------------------------------------------------------------------------------------------------------//     
    public Vector listarTodasLasOrdenes(){      
       Vector listarTodas = new Vector();       
       try{                  
         /*String query = " SELECT TOC.NUMERO,TOC.LINEA,TOC.RUT,TOC.FECHA,TOC.COD_PRO,TOC.CAN_PED,TOC.CAN_PEN,TOC.FEC_ENT,MA.DESCRIPCION" +
                        " FROM MAC.dbo.TOCompra TOC" +
                        " LEFT OUTER JOIN MAC.dbo.MArticulo MA" +
                        " ON TOC.COD_PRO = MA.CODIGO" +
                        " WHERE TIP_REG = 1 AND TIP_COMPRA <>'G' AND CAN_PEN > 0" +
                        " ORDER BY TOC.NUMERO,TOC.LINEA" ;
          */
           String query = "select enc.MVE_FolioFisico,tc.MVD_Linea,enc.MVE_CodPersona,convert(datetime,tc.MVD_Fecha),tc.MVD_CodProd,tc.MVD_cant,(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN, "
                         +"   convert(datetime,tc.MVD_FechaEntrega),pro.PRO_DESC "
                         +"   from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc, SiaSchaffner.dbo.STO_PRODUCTO pro "
                         +"   where  tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 "
                         +"   and enc.MVE_CodSistema=7  "
                         +"   and enc.MVE_CodClase =1  "
                         +"   and enc.MVE_TipoDoc = 1 "
                         +"   and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                         +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0 "
                         //+"   and tc.MVD_TipoProd in(1,2,5,6,8) "
                         +"   and tc.MVD_CodProd = pro.PRO_CODPROD "
                         +"   order by enc.MVE_FolioFisico,tc.MVD_Linea  ";
              conMAC = myConexion.getMACConnector();
              st = conMAC.createStatement();
              rs = st.executeQuery(query); 
              while(rs.next()){                  
                     OrdenDeCompra verDatosPorDescripcion =  new OrdenDeCompra();                    
                     verDatosPorDescripcion.setNum_orden_compra(rs.getLong(1));
                     verDatosPorDescripcion.setLinea(rs.getInt(2));  
                     verDatosPorDescripcion.setRut(rs.getFloat(3));
                     verDatosPorDescripcion.setFecha_pedido(rs.getDate(4));
                     verDatosPorDescripcion.setCod_producto(rs.getString(5));
                     verDatosPorDescripcion.setCan_pedida(rs.getFloat(6));
                     verDatosPorDescripcion.setCan_pendiente(rs.getFloat(7));                       
                     verDatosPorDescripcion.setFecha_entrega(rs.getDate(8));              
                     verDatosPorDescripcion.setDescripcion(rs.getString(9));
                     listarTodas.addElement(verDatosPorDescripcion);
             }
             st.close();
             rs.close();
             conMAC.close();
       }catch(Exception e){
           new Loger().logger("OrdenBean.class " , "ERROR  mostrarDatosPorDescripcionMateriaPrima() " + e.getMessage(),0);
       }
        return listarTodas;
 }
 //--------------------------------------------------------------------------------------------------------------------------------
    public void actualizarEstadoOC(long numOrden, String usuario){
        SimpleDateFormat forma = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        try{
            conMAC = myConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            String update = " update SiaSchaffner.dbo.STO_MOVENC set MVE_Aprobacion = '1' where " 
                            +"  MVE_CodSistema=7 and "
                            +"  MVE_CodClase =1 AND "
                            +"  MVE_TipoDoc = 1 AND  "
                            +"  MVE_FolioFisico= '" + numOrden + "'";
            st.execute(update);
             String query10 = " insert into MAC.dbo.ordenesDeCompra (numOrden,fechaValidacion,usuario,validacion,estado) " 
                              +" values(" + numOrden + ",'" + forma.format(new java.util.Date()) + "','" + usuario.trim() + "','validarSapGL','valida') ";
                             st.executeUpdate(query10);
            st.execute(query10);  
            conMAC.setAutoCommit(true);
            conMAC.commit();
            st.close();
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("OrdenBean.class " , " actualizarEstadoOC() " + e.getMessage(),0);
        }
    }
    public Vector buscarOCNumero(String numero){
        String query4 = "";
        
        Vector detalleOrdenCompra= new Vector();
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            query4 =  " select MVE_FolioFisico as NumOrden,MVD_CodProd,MVD_UMAlternativa,(MVD_Cant - MVD_CantAsignada) as Saldo,MVD_PrecioAjustado, "
                    +"  (select TAB_Desc from SiaSchaffner.dbo.SYS_TABLAS where TAB_CodTabla = 15 and TAB_Codigo = MVD_Comprador) as Comprador, "
                    +"  convert(datetime,MVD_Fecha) as FechaDeEmision,convert(datetime,MVD_FechaEntrega) as FechaEntrega,MVD_NumeroDocOrigen, "
                    +"  (select MAX(lower(CTA_RAZONSOCIAL)) from SiaSchaffner.dbo.STO_CTACTE where CTA_CODIGO = MVE_CodPersona)  as Proveedor "
                    +"  ,MVD_Cant,MVD_CantAsignada,isnull(MVE_Aprobacion,'0') as Aprobacion,MVD_Linea,replace(convert(varchar,PRO_DESC),'\"\',' ') as Descripcion "
                    +"  ,MVE_CodPersona,enc.Pmp,MVE_GlosaDoc, ISNULL(CONVERT(varchar(50),Convert(datetime ,MVD_Analisis5,113), 20), '0')AS FechaConfirmada "
                    //+"  MVE_CodPersona,enc.Pmp,MVE_GlosaDoc,MVD_Analisis5  "                    
                    +"  from SiaSchaffner.dbo.STO_MOVDET ,SiaSchaffner.dbo.STO_MOVENC enc,SiaSchaffner.dbo.STO_PRODUCTO "
                    +"  where MVD_CodSistema=7 and MVD_CodClase =1 AND MVD_TipoDoc = 1 and "
                    +"  MVE_CodSistema=7 and "
                    +"  MVE_CodClase =1 AND "
                    +"  MVE_TipoDoc = 1 AND  "
                    +"  MVD_CodProd = PRO_CODPROD and "
                    +"  MVD_NumeroDoc = MVE_NumeroDoc and MVE_FolioFisico = '" + numero + "'";
   
            rs = st.executeQuery(query4);
            while(rs.next()){
                OrdenDeCompra orden= new OrdenDeCompra();
                orden.setNum_orden_compra(rs.getLong(1));
                orden.setCod_producto(rs.getString(2));
                orden.setUm(rs.getString(3));
                orden.setSaldo(rs.getFloat(4));
                orden.setPrecio(rs.getFloat(5));
                if(rs.getString(6) != null){
                    orden.setComprador(rs.getString(6));
                }else{
                    orden.setComprador("SC");
                }
                orden.setFechaEntrada(rs.getDate(7));
                orden.setFecha_entrega(rs.getDate(8));
                orden.setNumeroDocOrigen(rs.getFloat(9));
                orden.setNumDocOrigen(rs.getLong(9));
                orden.setProveedor(rs.getString(10));
                orden.setCan_pedida(rs.getFloat(11));
                orden.setCan_pendiente(rs.getFloat(12));
                orden.setAprobacion(rs.getString(13));
                orden.setLinea(rs.getInt(14));
                orden.setDescripcion(rs.getString(15));
                orden.setIdOrden(rs.getInt(16));
                orden.setAccion(rs.getString(17));
                
                orden.setComentario(rs.getString(18));
                orden.setFechaConfirmada(rs.getString(19));
                detalleOrdenCompra.addElement(orden);
            }
            rs.close();
            st.close();
             myConexion.cerrarConMAC();
        }catch(Exception e){
             myConexion.cerrarConMAC();
            new Loger().logger("OrdenBean.class " , " buscarOCNumero() "+e.getMessage(),0);
        }
        return detalleOrdenCompra;
    }   
     public Vector devolverEstadosOC(long numero){
        
        Vector devolver = new Vector();
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =" select numOrden,fechaValidacion,usuario,validacion from MAC.dbo.ordenesDeCompra "
                          +"  where numOrden = '" + numero + "' "
                          +"  order by fechaValidacion ";
            rs = st.executeQuery(query4);
            while(rs.next()){
                OrdenDeCompra orden = new OrdenDeCompra();
                orden.setNum_orden_compra(rs.getLong(1));
                orden.setFechaEntrada(rs.getTimestamp(2));
                orden.setPersona(rs.getString(3));
                orden.setDescripcion(rs.getString(4));
                devolver.addElement(orden);
            }
            
            rs.close();
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " devolverEstadosOC() "+e.getMessage(),0);
        }
        return devolver;
    }   
     //---------------------------------------------------------------------------------------------------------------------------
    public Vector bitacoraOc(long numOrden){
        Vector buscar = new Vector();
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
            String query = "select numOrden,bitacora,id,fecha,usuario from MAC.dbo.LG_bitacora_oc where numOrden = " + numOrden + "";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                OrdenDeCompra orden = new OrdenDeCompra();
                orden.setNum_orden_compra(rs.getLong("numOrden"));
                orden.setDescripcion(rs.getString("bitacora"));
                orden.setIdOrden(rs.getInt("id"));
                orden.setFechaIni(rs.getTimestamp("fecha"));
                orden.setPersona(rs.getString("usuario"));
                buscar.addElement(orden);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " bitacoraOc() "+e.getMessage(),0);
        }
        return buscar;
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------------------
    
    public String nombreUsuario1(String usuario){
        String nombre = "";
        Statement stml = null;
        ResultSet result = null;
        
        try{
            conENS = myConexion.getENSConnector();
            stml = conENS.createStatement();
            String query4 =  " select nombre + ' ' + apellido from MAC.dbo.TUsuario "
                    +" where login = '" + usuario.trim() + "' ";
            result = stml.executeQuery(query4);
            while(result.next()){
                nombre = result.getString(1);
            }
            result.close();
            stml.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " nombreUsuario1() "+e.getMessage(),0);
        }
        return nombre;
    }
    
      public Vector traerSapPorNumero(long numSap){
        
        Vector traerSapPorNumero = new Vector();
        String area = "";
        String query4 = "";
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
           
                query4 = " select convert(varchar(1000),MVD_ComentarioLinea),MVD_CodProd,MVD_UMAlternativa,MVD_Cant, enc.UsCreacion "
                        +",mve_analisisadic4,convert(datetime,MVD_Fecha),convert(int,Convert(float,mve_analisisadic2)) as Pedido,Validar,NumSap,convert(varchar(8000),MVE_GlosaDoc),Anular,MVD_Linea,MVE_CentroCosto,MVD_CantAsignada "
                        +" ,Comprador,(select USE_NombreCompleto from SiaSchaffner.dbo.SYS_USER  where USE_Codigo = enc.UsCreacion) as Usuario"
                        +" ,FechaValidacion,FechaValidacionSGC,usuValArea,usuValLogi,usuAnular"
                        +"   from SiaSchaffner.dbo.STO_MOVDET "
                        +"   left outer join SiaSchaffner.dbo.STO_MOVENC enc "
                        +"   on(MVD_NumeroDoc = MVE_NumeroDoc and MVE_CodSistema=7 and "
                        +"   MVE_CodClase =1 AND "
                        +"   MVE_TipoDoc = 20) "
                        +"   inner join MAC.dbo.LG_sap "
                        +"   on MVE_FolioFisico = NumSap "
                        +"   where MVD_CodSistema=7 and "
                        +"   MVD_CodClase =1 AND "
                        +"   MVD_TipoDoc = 20 and "
                        +"   MVE_FolioFisico = '" + numSap + "' ";
                        rs = st.executeQuery(query4);
            String caracter = "";
            while(rs.next()){
                Sap sap = new Sap();
                
                caracter = rs.getString(1).replace('"',' ');
                
                sap.setDescripcion(caracter);
                sap.setCodigo(rs.getString(2));
                sap.setUm(rs.getString(3));
                sap.setCantidad(rs.getFloat(4));
                sap.setUsuario(rs.getString(5));
                sap.setArea(rs.getString(6));
                sap.setFechaEntrada(rs.getDate(7));
                sap.setNp(rs.getInt(8));
                sap.setValidar(rs.getByte(9));
                sap.setNumSap(rs.getLong(10));
                sap.setObservacion(rs.getString(11));
                sap.setAnular(rs.getByte(12));
                sap.setLinea(rs.getInt(13));
                sap.setCentroCosto(rs.getString(14));
                sap.setCantAsignada(rs.getFloat(15));
                sap.setComprador(rs.getString(16));
                sap.setNomPersona(rs.getString(17));
                sap.setFechaVal(rs.getDate(18));
                sap.setFechaValSgc(rs.getDate(19));
                sap.setUsuValArea(rs.getString(20));
                sap.setUsuValLogi(rs.getString(21));
                sap.setUsuAnular(rs.getString(22));
                
                traerSapPorNumero.addElement(sap);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " traerSapPorNumero() "+e.getMessage(),0);
        }
        return traerSapPorNumero;
    }
      public Vector traerArchivosAdjuntosPorSap(long saps){
        String query4 = "";
        Vector traerArchivosAdjuntosPorSap = new Vector();
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            query4 =  " select NombreArchivo from MAC.dbo.LG_archivos_adjuntos_sgc where NumSap = '" + saps + "'";
            
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                ArchivoAdjunto arch = new ArchivoAdjunto();
                arch.setNombreArchivo(rs.getString(1));
                traerArchivosAdjuntosPorSap.addElement(arch);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " traerArchivosAdjuntosPorSap() "+e.getMessage(),0);
        }
        return traerArchivosAdjuntosPorSap;
    }
      
      //-----------------------------------------------------------------------------------------------------------------------------------------------------
    public String nombreArea(String area){
        String nombre = "";
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            String query4 =  " select AreaNegocio from MAC.dbo.LG_permisos_sap where id = '" + area.trim() + "' ";
            rs = st.executeQuery(query4);
            while(rs.next()){
                nombre = rs.getString(1);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " nombreArea() "+e.getMessage(),0);
        }
        return nombre;
    }
    
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------
    public String nombreCentroDeCosto(String centroCosto){
        String query4 = "";
        String nombreCentroCosto = "";
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            query4 =  " select TAB_Desc from SiaSchaffner.dbo.SYS_TABLAS where TAB_CodTabla = 10 and TAB_Codigo = '" + centroCosto.trim() + "' ";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Sap sap= new Sap();
                nombreCentroCosto = rs.getString(1);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " nombreCentroDeCosto() "+e.getMessage(),0);
        }
        return nombreCentroCosto;
    }
     public String descripcionPorCodigo(String codigo){
        String descripcion = "";
        String query4 = "";
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
            query4 =  " select PRO_DESC from SiaSchaffner.dbo.STO_PRODUCTO  where PRO_CODPROD = '" + codigo.trim() + "'";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                descripcion = rs.getString(1).replace('"',' ');
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " descripcionPorCodigo() " + e.getMessage(),0);
        }
        return descripcion;
    }
     
      public Vector observacionAsap(long numSap){
        Vector observacion = new Vector();
        String query4 = "";
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            query4 =  " select tipo,fechaIngreso,Observacion,usuario,accion from MAC.dbo.LG_bitacoraSap "
                    +" where NumSap = " + numSap + " "
                    +" order by fechaIngreso ";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Sap sap = new Sap();
                sap.setDescripcion(rs.getString(1));
                sap.setFechaIni(rs.getTimestamp(2));
                sap.setObservacion(rs.getString(3));
                sap.setUsuario(rs.getString(4));
                sap.setAccion(rs.getString(5));
                observacion.addElement(sap);
            }
            rs.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " observacionAsap() " + e.getMessage(),0);
        }
        return observacion;
    }
      public Object devolverObject(Object objeto){
        if(objeto == null){
            objeto = "";
        }
        return objeto;
    }
       public String usuarioSia(String usuario){
        String query4 = "";
        String usuarioSia = "";
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            query4 =  " select USE_Codigo from SiaSchaffner.dbo.SYS_USER  where USE_Email = "
                    +" convert(varchar,(select idUsuario from MAC.dbo.TUsuario where login = '" + usuario.trim() + "')) ";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Sap sap= new Sap();
                usuarioSia = rs.getString(1);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("OrdenBean.class " , " usuarioSia() " + e.getMessage(),0);
        }
        return usuarioSia;
    }

}
