package mac.ee.pla.calendarioWeb.bean;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.ejb.*;
import javax.swing.JLabel;
import mac.ee.pla.calendario.clase.CalendarioDeMaquina;
import mac.ee.pla.calendario.clase.Maquina;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 * This is the bean class for the CalendarioWebBean enterprise bean.
 * Created 08-feb-2008 14:41:41
 * @author dbarra
 */
public class CalendarioWebBean implements SessionBean, CalendarioWebRemoteBusiness {
    private SessionContext context;
    private String identi;
    CalendarioDeMaquina calendario = new CalendarioDeMaquina();
    Maquina maquina = new Maquina();
    CalendarioDeMaquina calendar = new CalendarioDeMaquina(maquina.getMaquinas().size(),calendario.largoCalendario());

    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
//-----------------------------------------------------------------------------------------------------------------------    
 // método que carga en el arreglo hora todos los domingos festivos,turnos,etc
 public void setearArregloCalendario(){
    
    calendar.domingosPorAño();
    calendar.agregarDomingo();
    calendar.traerHorasSeteadas();
    calendar.setearTurnoNormal();
}
//-----------------------------------------------------------------------------------------------------------------------
 public short[][] retornarArregloCalendario(){
     return calendar.retornarArregloCalendario();
 }
//-----------------------------------------------------------------------------------------------------------------------
public Vector calendarioMes(int month){
       return calendario.calendarioMes(month);
    }
//-----------------------------------------------------------------------------------------------------------------------
public int transformarFechaAhora(String fech ,short identificador,short idMaquina){
   return calendario.transformarFechaAhora(fech,identificador,idMaquina);
}
//-----------------------------------------------------------------------------------------------------------------------    
    public void setIdentificador(String identi){
        this.identi = identi;
    }
//-----------------------------------------------------------------------------------------------------------------------   
    public String getIdentificador(){
        return identi;
    }
//-----------------------------------------------------------------------------------------------------------------------
    public Vector getMaquinas(){
       return maquina.getMaquinas();
    }
    
//-----------------------------------------------------------------------------------------------------------------------
    public int largoCalendarioDeMaquina(){
        return calendario.largoCalendario();
    }
    
//-----------------------------------------------------------------------------------------------------------------------
}
