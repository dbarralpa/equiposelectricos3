package mac.ee.pla.mrp.bean;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Vector;
import javax.ejb.*;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 * This is the bean class for the PedidoMaterialesBean enterprise bean.
 * Created 20-08-2010 08:54:47 AM
 * @author dbarra
 */
public class PedidoMaterialesBean implements SessionBean, PedidoMaterialesRemoteBusiness {
    private SessionContext context;
    private Connection conMAC = null;
    private SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
    String fechaM = formateador.format(new java.util.Date());
    private Conexion mConexion= new Conexion();
    private Statement st;
    private ResultSet rs;
    Vector materialesPorProcesos = new Vector();
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    
    
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")
    //-------------------------------------------------------------------------------------------------------------------------------
    public void materialesPorEquipo(int np,int linea,String proceso,String familia){
        String query4 = "";
        
        if(familia.equals("CELDA")){
            
          query4 ="  SELECT  me.CantidadMP, PRO_UMPRINCIPAL, me.CodigoMatPrima,PRO_DESC "
                    +"  ,(select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega not in(7,10)) "
                    +"  ,(select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega = 10) "
                    +"  ,PRO_CODTIPO "
                    +"  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ,SiaSchaffner.dbo.STO_PRODUCTO "
                    +"  WHERE a.codProducto = me.CodigoTerminado  "
                    +"  AND a.np = " + np + " AND a.linea =" + linea + " "
                    +"  and me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = PRO_CODPROD and a.proceso = 'MON' "
                    +"  ORDER BY a.readTime ";
            
         
        }else{
            
            query4 = "  SELECT me.CantidadMP, PRO_UMPRINCIPAL, me.CodigoMatPrima,PRO_DESC, "
                    +"  (select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega not in(7,10)) "
                    +"  ,(select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega = 10) "
                    +"  ,PRO_CODTIPO "
                    +"  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ,SiaSchaffner.dbo.STO_PRODUCTO  "
                    +"  WHERE a.codProducto = me.CodigoTerminado "
                    +"  AND a.proceso = me.CodigoSemiElaborado "
                    //+"  AND a.proceso = '" + proceso + "' AND a.np = " + np + "  AND a.linea = " + linea + " "
                    +"  AND a.np = " + np + "  AND a.linea = " + linea + " "
                    +"  and me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = PRO_CODPROD "
                    +"  ORDER BY a.readTime  ";
           
        }
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCantidad(rs.getFloat(1));
                estructura.setUnidadMedida(rs.getString(2));
                estructura.setCodigoMatPrima(rs.getString(3));
                estructura.setDescripcion(rs.getString(4));
                estructura.setObservacion("");
                estructura.setStockBodega(rs.getFloat(5));
                estructura.setStockBodega1(rs.getFloat(6));
                estructura.setStockPlanta(0f);
                estructura.setTipoProducto(rs.getInt(7));
                materialesPorProcesos.addElement(estructura);
            }
            rs.close();
            this.desconectarMac();
        }catch(Exception e){
            this.desconectarMac();
            new Loger().logger("MrpBean.class " , " materialesPorEquipo(); "+e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------    
    public Vector retornarMaterialesPorProcesos(){
        return materialesPorProcesos;
    }
     public void retornarMaterialesPorProcesos(Vector mater){
       materialesPorProcesos = mater;
    }
//--------------------------------------------------------------------------------------------------------------------
    public void desconectarMac(){
        try{
            st.close();
            conMAC.close();
        }catch(Exception e){}
    }
    
    
}
