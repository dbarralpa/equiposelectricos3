
package mac.ee.pla.crp2.bean;

import java.rmi.RemoteException;
import java.sql.Date;
import java.util.Vector;


/**
 * This is the business interface for Crp2 enterprise bean.
 */
public interface Crp2RemoteBusiness {
    void getNotValidTimming() throws RemoteException;
    Vector actualizarEquipos(int np, int linea, int identificador) throws RemoteException;
    Vector retornarEquipos() throws RemoteException;
    void createMapa(Vector vEquiposParaMapear) throws RemoteException;
    Vector equiposConProblemas() throws RemoteException;
    void actualizarStockPlanta() throws RemoteException;
    void eliminarMaterialesRepetidos(int np, int linea) throws RemoteException;
    Vector retornarEliminarMaterialesRepetidos() throws RemoteException;
    Vector retornarEliminarMaterialesRepetidos(Vector vacio) throws RemoteException;
    Vector procesosSinTerminar(String proceso) throws RemoteException;
    Vector traerEquipo(int np, int linea) throws RemoteException;
    void recuperarEquipo(Vector recuperarEquipos) throws RemoteException;
    Vector recuperarEquipos() throws RemoteException;
    String updateTeam(int np, int linea, Date fechaInicio, String date, int nodo, int identificador, String proceso) throws RemoteException;
    long diferenciaFecha(Date fechaInicio, Date fechaTermino) throws RemoteException;
    Vector retornarEquiposConProblemas(String material,String descripcion) throws RemoteException;
    Vector mapaAsBuildConEstructura(String proceso,String np,String linea) throws RemoteException;
    Vector equiposPorProcesos(String proceso,String equipo1,byte value) throws RemoteException;
    Vector lineaPorEquipo(String proceso, int np) throws RemoteException;
    int contarEquipos(String proceso) throws RemoteException;
    Vector retornarVectorDetalles() throws RemoteException;
    Vector equiposPorPedido(int np, int linea,String proceso,String numero) throws RemoteException;
    Vector equiposPorItem(long numero) throws RemoteException;
    byte totalEquiposPorPedido(int np, int linea, long numero, byte grupo, byte estado, String descriEstado) throws RemoteException;
    Vector mapaAsBuildConEstructura(String material, String fechaMenor, String fechaMayor, int compara) throws RemoteException;
    Vector mapaAsBuildConEstructura1(String material, String fechaMenor, String fechaMayor, int compara) throws RemoteException;
    Vector retornarMapaAsBuildConMateriales() throws RemoteException;
    Vector recalcularEstadoMaterialParaActualizar() throws RemoteException;
    boolean aprobarPedidos(String pedido, String prioridad, String usuario) throws RemoteException;    
    Vector controlMaterialesPorPedido(String pedido,String linea) throws RemoteException;
    Vector analisisMaterialesPorPedido(String familia, String avance, String fechaIni, String fechaFin) throws RemoteException;
    boolean equipoIfExist(String pedido, String linea) throws RemoteException;
   
    
}
