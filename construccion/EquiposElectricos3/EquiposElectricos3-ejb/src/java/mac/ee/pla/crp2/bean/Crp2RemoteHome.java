
package mac.ee.pla.crp2.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for Crp2 enterprise bean.
 */
public interface Crp2RemoteHome extends EJBHome {
    
    Crp2Remote create()  throws CreateException, RemoteException;
    
    
}
