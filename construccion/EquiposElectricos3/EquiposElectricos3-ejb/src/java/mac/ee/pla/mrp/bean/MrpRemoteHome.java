
package mac.ee.pla.mrp.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for Mrp enterprise bean.
 */
public interface MrpRemoteHome extends EJBHome {
    
    MrpRemote create()  throws CreateException, RemoteException;
    
    
}
