/*
 * Calendario.java
 *
 * Created on 12 de febrero de 2008, 11:25 por David Barra
 *
 * Clase que setea un calendario para saber que d�as y que horas los equipos pueden fabricarse
 */

package mac.ee.pla.calendario.clase;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.ejb.*;
import javax.swing.JLabel;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 *
 * @author dbarra
 */
public class CalendarioDeMaquina {
      private int diainicial; //Primer dia del mes
      private int diafinal;   //Ultimo dia del mes
      private int mes;        //Mes actual 
      private int ano;        //A�o actual       
      private GregorianCalendar calendar = new GregorianCalendar();
      private JLabel dias[][];
      String[] dia = new String[5];
      int day = 0;
      private int mes1;
      int[] meses={0,1,2,3,4,5,6,7,8,9,10,11};
      private String a�o[] = new String[52];
      private int correlativo=0;
      private short calendarioDeMaquina[][] = null;
      private Conexion mConexion= new Conexion();
      private Connection conMAC=null;
      private Statement st;
      private ResultSet rs;
      private String identi;
      private static final short TARDE = 1;
      private static final short NOCHE = 2;
      private static final short MANANATARDE= 3;
      private static final short NOCHEMANANA = 4;
      //private static final short TARDENOCHE = 5;
      private static final short TRESTURNOS = 6;
      private static final short FESTIVO = 7;
      private static final short MANTENCION = 8;
      private static final short DOMINGO = 9;
      private static final short MANANA = 10;
      private static final short DIACTUAL = 11;
      private Maquina maquina = new Maquina();
      private short largoCalendario=0;
      private Dia days = new Dia();
      
//----------------------------------------------------------------------------------------------------------------------------
      /** Creates a new instance of Calendario */
      public CalendarioDeMaquina(){
          dias = new JLabel[7][7];
          for(int i=0; i<7; i++ )
           for(int j=0; j<7; j++ ){
              dias[i][j]=new JLabel(" ",JLabel.RIGHT);
      }  
          ano = calendar.get(Calendar.YEAR);
          
      }
              
        public CalendarioDeMaquina(int largoMaquina , int largoCalendario){
            dias = new JLabel[7][7];
              for(int i=0; i<7; i++ )
               for(int j=0; j<7; j++ ){
                  dias[i][j]=new JLabel(" ",JLabel.RIGHT);
          }
            ano = calendar.get(Calendar.YEAR);
            calendarioDeMaquina = new short[largoMaquina][largoCalendario];
        }   
//----------------------------------------------------------------------------------------------------------------------------        
    
    
                                   ///////////////////////////////////////////////
                                   //     esta es la parte para guardar         //
                                   //     los seteos a las fechas que           //
                                   //     quedar�n como horas                   //
                                   ///////////////////////////////////////////////
    
    
//----------------------------------------------------------------------------------------------------------------------------            
    
    
    public Vector calendarioMes(int month){
       Vector months = new Vector();
       //ano = 2008;
       mes = month;
       update();
       for(int i=0; i<7; i++){
	   for(int j=0; j<7; j++){
                Dia da = new Dia();
                da.setDomingo(dias[i][0].getText());
                da.setLunes(dias[i][1].getText());
                da.setMartes(dias[i][2].getText());
                da.setMiercoles(dias[i][3].getText());
                da.setJueves(dias[i][4].getText());
                da.setViernes(dias[i][5].getText());
                da.setSabado(dias[i][6].getText());
                months.addElement(da);
            break;
          
          }
      }
       return months;
    }
//----------------------------------------------------------------------------------------------------------------------------    
     private void update(){
   	
     for(int i=0;i<7; i++ )
	  for(int j=0; j<7; j++ )
	       	 dias[i][j].setText(" ");
                 calendar.set( Calendar.DATE, 1 );
                 calendar.set( Calendar.MONTH, mes+Calendar.JANUARY );
                 calendar.set( Calendar.YEAR, ano+1900 ); //1900 a�o inicial
                 diainicial = calendar.get(Calendar.DAY_OF_WEEK)-Calendar.SUNDAY-1;
                 if(diainicial==-1) diainicial=diainicial+7;
                 diafinal = calendar.getActualMaximum(Calendar.DATE);
                 for(int i=0; i<diafinal; i++)
                 {	  	
                  dias[(i+diainicial)/7+1][(i+diainicial)%7].setText( String.valueOf(i+1));
                 }
           }
    
//----------------------------------------------------------------------------------------------------------------------------
 private int grabarCalendario(short horaIni , short horaFin , short identificador , short idMaquina){
       int result = 0;
       try{
       conMAC = mConexion.getMACConnector();
       st = conMAC.createStatement();
       String query = "insert into MAC.dbo.Calendario (horaInicial,horaFinal,identificador,idMaquina) values (" + horaIni + "," +  horaFin+ "," + identificador + "," + idMaquina + ")"; 
       st.executeUpdate(query);
       result = 1;
       st.close();
       conMAC.close();
       }catch(Exception e){
           new Loger().logger("CalendarioWeb.class " , "ERROR: grabarCalendario() " + e.getMessage(),0);
           result = 0;
       }
        return result;
   }
//----------------------------------------------------------------------------------------------------------------------------
     public int transformarFechaAhora(String fech ,short identificador , short idMaquina){
        Timestamp fecha1 = null;
        Timestamp fecha = null;
        long hour = 0; 
        long hour1 = 0;
        String hora="";
     
        if(identificador == TARDE){
            hora = "17:00:00.0";
        }
        if(identificador == NOCHE || identificador == NOCHEMANANA || identificador == TRESTURNOS
                                  || identificador == FESTIVO || identificador == MANTENCION){
            hora = "01:00:00.0";
        }
        if(identificador == MANANATARDE){
          hora = "09:00:00.0";
        }
        long fechaHora1 = fecha1.valueOf(fech+" "+hora).getTime();
        long fechaHora  = fecha.valueOf(ano+"-01-01 00:00:00.0").getTime();
        hour = (fechaHora1 - fechaHora) /3600000;
               
        if(identificador == TARDE || identificador == NOCHE){
          hour1 = hour + 7;     
        }
        if(identificador == MANANATARDE || identificador == NOCHEMANANA){ //|| identificador == TARDENOCHE ){
          hour1 = hour + 15;
        }
        if(identificador == FESTIVO || identificador == MANTENCION || identificador == TRESTURNOS){
          hour1 = hour + 23;
        }
        return this.grabarCalendario((short)hour,(short)hour1,identificador,idMaquina);
    }
     
//----------------------------------------------------------------------------------------------------------------------------
     
                                   ///////////////////////////////////////////////
                                   //     aqu� viene la parte para              //
                                   //     cargar los domingos en el             //
                                   //     arreglo calendario                    //
                                   ///////////////////////////////////////////////

//----------------------------------------------------------------------------------------------------------------------------
 // rescata todos los domingos del a�o
    public void domingosPorA�o(){
       //mes1 = mes+1;
       //ano = calendar.get(Calendar.YEAR);
       int w = 0;
       for(int u=0;u<meses.length;u++){
           mes = meses[u];
         update();
        mes1 = mes+1;
        for(int i=0; i<7; i++){
	   for(int j=0; j<7; j++){
            if(!dias[i][0].getText().equals(" ")){
               a�o[w] = ano + "-" + mes1 + "-" + dias[i][0].getText();
               w++;
               break;
            }
        }
     }	
   }
}   
//----------------------------------------------------------------------------------------------------------------------------
 // agrega todos los domingos al arreglo calendario
  public void agregarDomingo(){
        for(int i=0;i<a�o.length;i++){
            horasDeUnDia(a�o[i]);
        }
    }
//----------------------------------------------------------------------------------------------------------------------------    
     private void horasDeUnDia(String fech){
        Timestamp fecha = null;
        Timestamp fecha1 = null;
        long hour = 0; 
        long hour1 = 0;
        long fechaHora2 = fecha1.valueOf(fech+" 24:00:00.0").getTime();
        long fechaHora1 = fecha1.valueOf(fech+" 01:00:00.0").getTime();
        long fechaHora  = fecha.valueOf(ano+"-01-01 00:00:00.0").getTime();
        hour = (fechaHora1 - fechaHora) /3600000;
        hour1 = (fechaHora2 - fechaHora) /3600000;
        //System.out.println(fech);
        vHoras(hour,hour1);
  }
//----------------------------------------------------------------------------------------------------------------------------
     //24 horas
     private void vHoras(long fechaIni , long fechaFin){
       for(int u=0;u<maquina.getMaquinas().size();u++){
         for(int a = (int)fechaIni;a<=(int)fechaFin;a++){
             calendarioDeMaquina[u][a] = DOMINGO;
         }
     }
}
//----------------------------------------------------------------------------------------------------------------------------
   public short[][] retornarArregloCalendario(){
       return calendarioDeMaquina;
   }     
//----------------------------------------------------------------------------------------------------------------------------
                                   ///////////////////////////////////////////////
                                   //                     fin                   //
                                   ///////////////////////////////////////////////
//----------------------------------------------------------------------------------------------------------------------------
   // m�todo que setea un dia  normal de trabajo en el arreglo calendario
   public void setearTurnoNormal(){
       short contador = 0;
       short verificador = 0;
       short diaNormal = 0;
       for(int u=0;u<maquina.getMaquinas().size();u++){
         for(int a = 0;a<this.largoCalendario();a++){
             if(calendarioDeMaquina[u][a] > verificador){
                 verificador = calendarioDeMaquina[u][a];
             }
             if(contador == 24){
                 if(verificador == 0){
                     diaNormal = 1;
                 }
             } 
              contador++;
             if(contador == 25){
                 verificador = 0;
                 contador=1;
             }
              if(diaNormal == 1){
                 int menor = a-15;
                 int mayor = a-8;
                 for(int b = menor;b<=mayor;b++){
                    calendarioDeMaquina[u][b] = MANANA;
                 }
                 diaNormal = 0;
             }
             if(a == this.largoCalendario()-1){
                 contador = 0;
             }
         }
      }    
   }
//----------------------------------------------------------------------------------------------------------------------------
// carga en el vector calendario las fechas que fueron seteadas y guardadas en la base de datos
   public void traerHorasSeteadas(){
       try{
       conMAC = mConexion.getMACConnector();
       st = conMAC.createStatement();
       String query = "SELECT horaInicial,horaFinal,identificador,idMaquina FROM MAC.dbo.Calendario"; 
       rs = st.executeQuery(query);
       while(rs.next()){
           for(short i=rs.getShort(1);i<=rs.getShort(2);i++){
               calendarioDeMaquina[rs.getShort(4)][i] = rs.getShort(3);
           }
  }
       st.close();
       conMAC.close();
       }catch(Exception e){
           new Loger().logger("CalendarioWeb.class " , "ERROR: traerHorasSeteadas() " + e.getMessage(),0); 
       }
   }
   
//----------------------------------------------------------------------------------------------------------------------------
   public short largoCalendario(){
       if(ano%4==0){
               largoCalendario = 8785;
           }else{
               largoCalendario = 8761;
           }
       return largoCalendario;
   }

//----------------------------------------------------------------------------------------------------------------------------
// Se encarga de setear el dia que se puede empezar a fabricar en cada m�quina
   public void setearDiaActual(){
        Timestamp fecha = null;
        Timestamp fecha1 = null;
        long hour = 0; 
        long hour1 = 0;
        long fechaHora2 = fecha1.valueOf(days.diaActual()+" 24:00:00.0").getTime();
        long fechaHora1 = fecha1.valueOf(days.diaActual()+" 01:00:00.0").getTime();
        long fechaHora  = fecha.valueOf(ano+"-01-01 00:00:00.0").getTime();
        hour = (fechaHora1 - fechaHora) /3600000;
        hour1 = (fechaHora2 - fechaHora) /3600000;
        System.out.println(hour + " " + hour1);
        for(int u=0;u<maquina.getMaquinas().size();u++){
        for(int a=(int)hour;a<=(int)hour1;a++){
            calendarioDeMaquina[u][a] +=5;
            //System.out.println(a + "---" +calendarioDeMaquina[0][a]);
        }
     }
 }
   public static void main (String [] args){
       CalendarioDeMaquina cale = new CalendarioDeMaquina(3,9000);
       cale.domingosPorA�o();
       cale.agregarDomingo();
       cale.traerHorasSeteadas();
       cale.setearDiaActual();
   }
//----------------------------------------------------------------------------------------------------------------------------

}// fin       
