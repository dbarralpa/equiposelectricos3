package mac.ee.pla.mrp.bean;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.ejb.*;
import mac.ee.pla.crp2.bean.Crp2Bean;
import mac.ee.pla.equipo.clase.Equipo;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import mac.ee.pla.mrp.clase.BitacoraVales;
import mac.ee.pla.mrp.clase.ModOrdenCompra;
import mac.ee.pla.mrp.clase.ModParteEntrada;
import mac.ee.pla.mrp.clase.ModReclamo;
import mac.ee.pla.mrp.clase.Usuario;
import mac.ee.pla.ordeCompra.clase.OrdenDeCompra;
import mac.ee.pla.ordenCompra.bean.OrdenBean;
import mac.excel.ModAuxiliar;
import mac.sql.ConexionSQL;
import mac.sql.PrmApp;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 * This is the bean class for the MrpBean enterprise bean.
 * Created 22-ago-2007 12:27:12
 * @author dbarra
 */
public class MrpBean implements SessionBean, MrpRemoteBusiness {
    private SessionContext context;
    java.util.Calendar fecha = java.util.Calendar.getInstance();
    SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd");
    public static String FORMATO_DMY = "yyyyMMdd";
    SimpleDateFormat formatea = new SimpleDateFormat("yyyyMM");
    String fechaM = formateador.format(new java.util.Date());
    String periodoLibro = formatea.format(new java.util.Date());
    Vector controlMateriales = new Vector();
    Vector controlMateriales1 = new Vector();
    private Conexion mConexion= new Conexion();
    private Conexion myConexion= new Conexion();
    private Conexion dConexion= new Conexion();
    private Conexion MacConexion= new Conexion();
    private Connection conMAC=null;
    private Connection conMAC1=null;
    private Connection conMAC2=null;
    private Connection conMAC3=null;
    private Connection conENS=null;
    private Statement st;
    private Statement st1;
    private Statement st2;
    private Statement st3;
    private Statement stm;
    private ResultSet rs;
    private ResultSet rst;
    private long numPedido = 0l;
    Crp2Bean crp = new Crp2Bean();
    EstructuraPorEquipo estructuraEquipo = new EstructuraPorEquipo();
    Vector materiales = new Vector();
    boolean retornarValor;
    int retornarValor1;
    Vector codigoMaterial = new Vector();
    
    Vector recuperarEquipos = new Vector();
    Vector balance = new Vector();
    SimpleDateFormat forma = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
    int incrementar;
    Vector materialesPorProcesos = new Vector();
    
    
    
    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    
    
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")
    
    
    public Vector statusProduccion(Vector crp, Vector ordenCompra , String codPro, String uMedida , String descri , float stock , int compara ,short leadTime,int comprasRequeridas,float stockReal,String comprador){
        
        Date fecha = new Date(0);
        Date fecha1 = new Date(0);
        float sumarCantidad = 0;
        float sumarOC = 0;
        int contador = 0;
        int contador1 = 0;
        float verificarStock = 0;
        float verificarStock1 = 0;
        int verificador = 0;
        int verificador1 = 0;
        String nuevaFecha = "";
        String nuevaFecha1 = "1970-01-01";
        int stockPositivo = 0;
        int stockPositivo1 = 0;
        int valor = 0;
        float promedio12 = 0;
        float promedio3 = 0;
        float peak = 0;
        int mesPeak = 0;
        int anoPeak = 0;
        float stockNegativo=0;
        float stockMinimo = 0;
        float stockMaximo = 0;
        OrdenBean ordenBean = new OrdenBean();
        int incrementar = 0;
        short lead=0;
        float diferenciaStock = 0;
        
        try{
            Date fechaMayor = ((Date)((Vector)crp.elementAt(0)).elementAt(0));
            for(int i = 0;i<crp.size();i++){
                if(((Date)((Vector)crp.elementAt(i)).elementAt(0)).compareTo(fechaMayor) > 0){
                    fechaMayor = ((Date)((Vector)crp.elementAt(i)).elementAt(0));
                }
            }
            Date fechaMenor = new Date(0).valueOf(forma.format(new java.util.Date()));
            long fechaInicio = fechaMenor.getTime()/86400000;
            long fechaFin = fechaMayor.getTime()/86400000;
            // System.out.println("------------------------------LISTO-----------------------------------------------------------------");
            Vector controlMaterial = new Vector();
            Vector controlMaterial1 = new Vector();
            if(compara == 1){
                for(int i=0;i<balance.size();i++){
                    if(((String)((Vector)balance.elementAt(i)).elementAt(0)).equals(codPro)){
                        balance.remove(i);
                        i-=1;
                    }
                }
            }
            
            for(int f = (int)fechaInicio;f <= (int)fechaFin;f++){
                Vector balancePorMaterial = new Vector();
                for(int i=0;i<crp.size();i++){
                    Equipo equipo = new Equipo();
                    if(((Date)((Vector)crp.elementAt(i)).elementAt(0)).getTime() / 86400000 == f ){
                        sumarCantidad += ((Float)((Vector)crp.elementAt(i)).elementAt(4)).floatValue();
                    }
                }
                
                for(int i=0;i<ordenCompra.size();i++){
                    if(((OrdenDeCompra)ordenCompra.elementAt(i)).getFecha_entrega().getTime() / 86400000 == f ){
                        sumarOC += ((OrdenDeCompra)ordenCompra.elementAt(i)).getCan_pendiente();
                    }
                }
                
                Date dates = new Date(0);
                dates.setTime(((long)f + 1) * 86400000);
                balancePorMaterial.addElement(codPro);
                balancePorMaterial.addElement(new Float(stock));
                balancePorMaterial.addElement(new Float(sumarOC));
                balancePorMaterial.addElement(new Float(sumarCantidad));
                float sumar = sumarOC + stock;
                float restar = sumar - sumarCantidad;
                stock = restar;
                balancePorMaterial.addElement(new Float(stock));
                balancePorMaterial.addElement(format.format(dates));
                balance.addElement(balancePorMaterial);
                
                if(stock >= 0 && stockPositivo == 1 ){
                    stockPositivo = 2;
                }
                
                if(stock < 0 ){
                    if(stockPositivo == 1 || stockPositivo == 0 || stockPositivo == 2){
                        if(stockPositivo == 2){
                            controlMaterial1.addElement(format.format(new Date(0).valueOf(nuevaFecha)));
                            controlMaterial1.addElement(new Float(verificarStock));
                            controlMaterial1.addElement(new Integer(contador));
                            stockPositivo  = 1;
                            verificarStock =0;
                            contador = 0;
                            verificador = 0;
                        }
                        contador++;
                        //if(stock < verificarStock){
                        verificarStock = stock;
                        //}
                        verificador++;
                        if(verificador == 1){
                            fecha.setTime(((long)f + 1) * 86400000);
                            nuevaFecha = String.valueOf(fecha);
                        }
                        stockPositivo = 1;
                    }
                }
                
                
                if(f == fechaFin && nuevaFecha != ""){
                    
                    stockMinimo =  new EstructuraPorEquipo().retornarStockMin(codPro);
                    stockMaximo =  new EstructuraPorEquipo().retornarStockMax(codPro);
                    for(int i=0;i<ordenBean.ordenDeCompraInvalidas(codPro).size();i++){
                        incrementar++;
                    }
                    if(leadTime > 0){
                        lead = estructuraEquipo.diferenciaDiasLeadTime(format.format(new Date(0).valueOf(nuevaFecha)),leadTime);
                    }
                    
                    
                    
                    
                    if(compara == 0){
                        
                        
                        controlMaterial.addElement(codPro);
                        controlMaterial.addElement(descri);
                        controlMaterial.addElement(format.format(new Date(0).valueOf(nuevaFecha)));
                        controlMaterial.addElement(new Float(verificarStock));
                        controlMaterial.addElement(new Integer(contador));
                        controlMaterial.addElement(new Integer(stockPositivo));
                        controlMaterial.addElement(controlMaterial1);
                        controlMaterial.addElement(new Float(stockReal));
                        controlMaterial.addElement(uMedida);
                        controlMaterial.addElement(new Float(stockMinimo));
                        controlMaterial.addElement(new Float(stockMaximo));
                        
                        if(incrementar==0){
                            controlMaterial.addElement("NO");
                        }else{
                            controlMaterial.addElement("SI");
                        }
                        controlMaterial.addElement(new Short(leadTime));
                        controlMaterial.addElement(new Short(lead));
                        controlMaterial.addElement(new Integer(comprasRequeridas));
                        controlMaterial.addElement(comprador);
                        controlMateriales.addElement(controlMaterial);
                    }else{
                        int a = 0;
                        for(int y=0;y<controlMateriales.size();y++){
                            if(codPro.equals((String)((Vector)controlMateriales.elementAt(y)).elementAt(0))){
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(format.format(new Date(0).valueOf(nuevaFecha)),2);
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(new Float(verificarStock),3);
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(new Integer(contador),4);
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(new Integer(stockPositivo),5);
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(controlMaterial1,6);
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(new Float(stockReal),7);
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(new Float(stockReal),7);
                                if(incrementar==0){
                                    ((Vector)controlMateriales.elementAt(y)).setElementAt("NO",11);
                                }else{
                                    ((Vector)controlMateriales.elementAt(y)).setElementAt("SI",11);
                                }
                                ((Vector)controlMateriales.elementAt(y)).setElementAt(new Integer(comprasRequeridas),14);
                                a = 1;
                            }
                        }
                        
                        if(a == 0){
                            controlMaterial.addElement(codPro);
                            controlMaterial.addElement(descri);
                            controlMaterial.addElement(format.format(new Date(0).valueOf(nuevaFecha)));
                            controlMaterial.addElement(new Float(verificarStock));
                            controlMaterial.addElement(new Integer(contador));
                            controlMaterial.addElement(new Integer(stockPositivo));
                            controlMaterial.addElement(controlMaterial1);
                            controlMaterial.addElement(new Float(stockReal));
                            controlMaterial.addElement(uMedida);
                            controlMaterial.addElement(new Float(stockMinimo));
                            controlMaterial.addElement(new Float(stockMaximo));
                            
                            if(incrementar==0){
                                controlMaterial.addElement("NO");
                            }else{
                                controlMaterial.addElement("SI");
                            }
                            controlMaterial.addElement(new Short(leadTime));
                            controlMaterial.addElement(new Short(lead));
                            controlMaterial.addElement(new Integer(comprasRequeridas));
                            controlMaterial.addElement(comprador);
                            controlMateriales.addElement(controlMaterial);
                        }
                    }
                }if(f == fechaFin && nuevaFecha.equals("")){
                    for(int y=0;y<controlMateriales.size();y++){
                        if(codPro.equals((String)((Vector)controlMateriales.elementAt(y)).elementAt(0))){
                            controlMateriales.remove(y);
                        }
                    }
                }
                sumar = 0;
                sumarOC = 0;
                restar = 0;
                sumarCantidad = 0;
                //}
            }
            
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: statusProduccion() " + e.getMessage(),0);
        }
        
        
        //System.out.println("-----------------------------------------fin------------------------------------------------------");
        
        // return controlMateriales;
        return  null;
    }
//----------------------------------------------------------------------------------------------------------------------------------
    public Vector controlDeMateriales(){
        return controlMateriales;
    }
    public Vector controlDeMateriales1(){
        return controlMateriales1;
    }
    public Vector balancePorMaterial(){
        return balance;
    }
//----------------------------------------------------------------------------------------------------------------------------------
    
    public Vector controlDeMateriales(Vector controlMaterialesReturn){
        //controlMateriales.clear();
        controlMateriales = controlMaterialesReturn;
        return controlMateriales;
    }
    public Vector controlDeMateriales1(Vector controlMaterialesReturn){
        controlMateriales1 = controlMaterialesReturn;
        return controlMateriales1;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public void actualizarMapaAsBuild(Date readTime1 , Date completionTime1 , Integer np , Integer linea , String proceso){
        try{
            conMAC1 = myConexion.getMACConnector();
            stm = conMAC1.createStatement();
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
            String update = "UPDATE MAC.dbo.MapaAsBuild SET readTime = '" + formateador.format(readTime1)+ "', completionTime='" + formateador.format(completionTime1) + "'"
                    +" WHERE np = " + np + " and linea = " + linea + " and proceso = '" + proceso + "'";
            // System.out.println(update);
            stm.execute(update);
            stm.close();
            conMAC1.close();
        }catch(Exception e){
            System.out.println("ERROR: actualizarMapaAsBuild() " + e.getMessage());
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
    // M�todo que se encarga de llamar al M�todo que hace el trabajo de ver que equipos est�n con materiales negativos
    // y le pasa todos los datos para que statusProduccion pueda hacer el trabajo
    
    public void materialesProyectados(String codigo , String descripcion , int compara , String fechaMenor , String fechaMayor ,int validar ,String uMedida){
        Vector materiales = new Vector();
        Vector equi = new Vector();
        Vector orden = new Vector();
        Vector controlMateriales = new Vector();
        Vector estructura = new Vector();
        //Vector stockMinMax = new Vector();
        materiales = estructuraEquipo.materiales();
        Vector promedios = new Vector();
        OrdenBean ordenCompra = new OrdenBean();
        float stock = 0;
        float stockReal = 0;
        short leadTime = 0;
        int comprasRequeridas = 0;
        float stockMax = 0;
        String comprador = "";
        try{
            
            if(!codigo.equals("all")){
                crp.retornarMapaAsBuildConMateriales().clear();
                if(estructuraEquipo.verificarMaterial(codigo) == false){
                    equi =  crp.mapaAsBuildConEstructura(codigo,fechaMenor,fechaMayor,validar);
                    if(equi.size() != 0){
                        //stockPlanta = ordenCompra.stockPlanta(codigo);
                        stock = ordenCompra.stock(codigo);
                        comprasRequeridas = retornarCantidadComprasRequeridas(codigo);
                        stockMax = estructuraEquipo.retornarStockMax(codigo);
                        //stock = (stockPlanta + stock) - estructuraEquipo.retornarStockMin(codigo);
                        stockReal = stock;
                        stock -= stockMax;
                        orden = ordenCompra.ordenDeCompraValidas(codigo);
                        leadTime = estructuraEquipo.leadTime(codigo);
                        comprador = estructuraEquipo.buscarComprador(codigo);
                        this.statusProduccion(equi,orden,codigo,uMedida,descripcion,stock,compara,leadTime,comprasRequeridas,stockReal,comprador);
                        crp.retornarMapaAsBuildConMateriales().clear();
                    }}
            }else{
                for(int i=0;i<materiales.size();i++){
                    crp.retornarMapaAsBuildConMateriales().clear();
                    equi =  crp.mapaAsBuildConEstructura(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima(),fechaMenor,fechaMayor,validar);
                    if(equi.size() != 0){
                        //stockPlanta = ordenCompra.stockPlanta(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        stock = ordenCompra.stock(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        comprasRequeridas = retornarCantidadComprasRequeridas(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        stockMax = estructuraEquipo.retornarStockMax(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        //stock = (stockPlanta + stock) - estructuraEquipo.retornarStockMin(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        stockReal = stock;
                        stock -= stockMax;
                        orden = ordenCompra.ordenDeCompraValidas(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        leadTime = estructuraEquipo.leadTime(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        comprador = estructuraEquipo.buscarComprador(((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima());
                        this.statusProduccion(equi,orden,((EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima(),((EstructuraPorEquipo)materiales.elementAt(i)).getUnidadMedida(),((EstructuraPorEquipo)materiales.elementAt(i)).getDescripcion().replace('"',' '),stock,compara,leadTime,comprasRequeridas,stockReal,comprador);
                        crp.retornarMapaAsBuildConMateriales().clear();
                    }
                }
            }
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: materialesProyectados() " + e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
    // M�todos para devolver un valor que sirve para verificar si es necesario actualizar Mrp o no
    public void setRetornarValor(boolean valor){
        this.retornarValor = valor;
    }
    public boolean getRetornarValor(){
        return this.retornarValor;
    }
//--------------------------------------------------------------------------------------------------------------------------------
// M�todo para retornar la �ltima actualizaci�n del vector materiales proyectados en memoria
    public Timestamp getFecha(){
        Timestamp fecha = null;
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = "select fecha from MAC.dbo.tiempoTranscurrido";
            rs = st.executeQuery(query);
            while(rs.next()){
                fecha = rs.getTimestamp(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: getFecha() " + e.getMessage(),0);
        }
        return fecha;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public void eliminarOcargarMaterial(String codigo , String descripcion , int identificador , String uMedida){
        
        try{
            Vector cargarPorMateriales = new Vector();
            cargarPorMateriales.addElement(codigo);
            cargarPorMateriales.addElement(descripcion);
            cargarPorMateriales.addElement(new Integer(identificador));
            cargarPorMateriales.addElement(uMedida);
            codigoMaterial.addElement(cargarPorMateriales);
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR eliminarOcargarMaterial(): " + e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public Vector retornarMaterialesEliminadosOcargados(){
        return codigoMaterial;
    }
    public Vector retornarMaterialesEliminadosOcargadosVacio(Vector vacio){
        codigoMaterial = vacio;
        return codigoMaterial;
    }
    
//--------------------------------------------------------------------------------------------------------------------------------
    // estos dos m�todos eran para actualizar el stock de planta el primero de enero
/*public Vector actualizarStockPlanta (){ // LUEGO BORRAR
          Vector stockPlanta = new Vector();
 
         String query4 = " SELECT d.CodigoMatPrima,d.CantidadMP"
                        +" FROM ENSchaffner.dbo.TPMP a"
                        +" LEFT OUTER JOIN ENSchaffner.dbo.TAvance b"
                        +" ON (a.Pedido = b.Pedido and a.Linea = b.Linea)"
                        +" LEFT OUTER JOIN  ENSchaffner.dbo.MEstructura d"
                        +" ON (d.CodigoTerminado = a.Codpro and d.CodigoSemiElaborado = b.Semielaborado)"
                        +" WHERE b.FechaTermino >= '20080101' AND d.CodigoMatPrima is not null";
     try{
         conENS = myConexion.getENSConnector();
         st = conENS.createStatement();
         stm = conENS.createStatement();
         rs = st.executeQuery(query4);
 
 
         while(rs.next()){
         /*Equipo equipo = new Equipo();
         equipo.setCodPro(rs.getString(1)); // material 0
         equipo.setCantidad(rs.getFloat(2)); // cantidad
         stockPlanta.addElement(equipo);
             this.actualizarStockPlanta1(rs.getString(1),rs.getFloat(2));
           }
         //System.out.println("equiposConMateriales: "  + equiposConMateriales);
         rs.close();
         st.close();
         stm.close();
         conENS.close();
 
 
     }catch(Exception e){
         new Loger().logger("MrpBean.class " , " ERROR: actualizarStockPlanta (): "+e.getMessage(),0);
     }
        return stockPlanta;
  }
 
  public void actualizarStockPlanta1 (String material ,float cantidad){ // LUEGO BORRAR
 
         int count =0;
         float canti = 0;
 
     try{
         //conENS = myConexion.getENSConnector();
         //stm = conENS.createStatement();
         String query4 = "SELECT COUNT(Cantidad) FROM ENSchaffner.dbo.StockPlanta where CodigoArticulo = '" + material + "'";
         rst = stm.executeQuery(query4);
 
         while(rst.next()){
            count = rst.getInt(1); // material 4
           }
         rst.close();
 
         if(count > 0){
             String query2 = "SELECT SUM(Cantidad) FROM ENSchaffner.dbo.StockPlanta where CodigoArticulo = '" + material + "'";
             rst = stm.executeQuery(query2);
              while(rst.next()){
                    canti = rst.getFloat(1); // material 4
           }
 
           canti -=cantidad;
           String query3 = "UPDATE ENSchaffner.dbo.StockPlanta set Cantidad = " + canti + " where CodigoArticulo = '" + material + "'";
           stm.execute(query3);
         }else{
             //"insert into MapaAsBuild (np,linea,proceso,processTime,readTime,completionTime,timeToBegin,timeToLead,codProducto,familia,vigencia) values ('"+ pedido +"','"+ linea +"','"+ proceso +"' ,'"+ processTime +"','"+ readTime +"','"+ completionTime +"','"+ timeToBegin +"','"+ timeToLead +"','"+ codPro +"','"+ familia +"','"+ vigencia +"')";
             String query = "insert into ENSchaffner.dbo.StockPlanta (CodigoArticulo,Cantidad) values ('" + material + "'," + -cantidad + ")";
             stm.execute(query);
         }
         count = 0;
         canti = 0;
         cantidad = 0;
         rst.close();
         //stm.close();
         //conENS.close();
 
 
     }catch(Exception e){
         new Loger().logger("MrpBean.class " , " ERROR: actualizarStockPlanta1 (): "+e.getMessage(),0);
     }
 
  }*/
//--------------------------------------------------------------------------------------------------------------------------------
 /* public int stockPlanta(String codMaterial , float cantidad , int estado){
       int contador = 0;
       float cantidadLoad=0;
       int verificar=0;
       try{
  
         String query4 = "SELECT COUNT(Cantidad) FROM ENSchaffner.dbo.StockPlanta where CodigoArticulo = '" + codMaterial + "'";
         rst = stm.executeQuery(query4);
         while(rst.next()){
           contador = rst.getInt(1);
           }
         rst.close();
         if (contador == 0){
             String query1="";
             if(estado == 4){
                   query1 = "insert into ENSchaffner.dbo.StockPlanta (CodigoArticulo,Cantidad) values ('"+ codMaterial +"',"+ cantidad +")";
                   stm.execute(query1);
                   verificar = 1;
           }
             if(estado == 2){
                   query1 = "insert into ENSchaffner.dbo.StockPlanta  (CodigoArticulo,Cantidad) values ('"+ codMaterial +"',"+ -cantidad +")";
                   stm.execute(query1);
                   verificar = 1;
             }
  
       }else{
                 String query6 = "";
                 String query5 = "SELECT Cantidad FROM ENSchaffner.dbo.StockPlanta  WHERE CodigoArticulo = '" + codMaterial + "'";
                 //stm = conENS.createStatement();
                 rst = stm.executeQuery(query5);
                 while(rst.next()){
                       cantidadLoad =  rst.getFloat(1);
                }
                 rst.close();
                  if(estado == 4){
                     cantidadLoad += cantidad;
                     query6 = "UPDATE  ENSchaffner.dbo.StockPlanta SET  Cantidad = "+ cantidadLoad +" WHERE CodigoArticulo = '" + codMaterial + "'";
                     stm.execute(query6);
                     verificar = 1;
                 }
  
                  if(estado == 2){
                     cantidadLoad -= cantidad;
                     query6 = "UPDATE  ENSchaffner.dbo.StockPlanta SET  Cantidad = "+ cantidadLoad +" WHERE CodigoArticulo = '" + codMaterial + "'";
                     stm.execute(query6);
                     verificar = 1;
                 }
            }
  
            if(verificar == 1){
             this.deleteAll(codMaterial);
            }
  
       }catch(Exception e){
         new Loger().logger("OrdenBean.class "," M�todo stockPlanta() " +  e.getMessage(),0);
           verificar = 0;
       }
  
       return verificar;
   }*/
//--------------------------------------------------------------------------------------------------------------------------------
    public void deleteAll(){
        try{
            String query ="DELETE FROM ENSchaffner.dbo.TMovimientos WHERE Tipo in (0,2,4)";
            stm.execute(query);
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR deleteAll(): " + e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public void deleteOC(){
        try{
            String query ="DELETE FROM ENSchaffner.dbo.TMovimientos WHERE Tipo = 0 ";
            stm.execute(query);
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR deleteOC(): " + e.getMessage(),0);
        }
    }
    
//--------------------------------------------------------------------------------------------------------------------------------
    public void recalcularEstadoMaterial(){
        
        try{
            this.retornarMaterialesEliminadosOcargados().clear();
            conENS = mConexion.getENSConnector();
            stm = conENS.createStatement();
            String query3 = "SELECT DISTINCT(Codigo),Descripcion,Unimed FROM ENSchaffner.dbo.TMovimientos WHERE Tipo in(0,2,4) ";
            rst = stm.executeQuery(query3);
            while(rst.next()){
                String codigo = rst.getString(1);
                String descri = rst.getString(2);
                String unimed = rst.getString(3);
                this.eliminarOcargarMaterial(codigo,descri,1,unimed);
            }
            this.deleteAll();
            rst.close();
            stm.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR recalcularEstadoMaterial(): " + e.getMessage(),0);
        }
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public boolean comprasRequeridas(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int verificar,byte estado){
        boolean valor=false;
        int count = 0;
        //fechaQuiebre = fechaQuiebre.substring(6,10) + fechaQuiebre.substring(3,5) + fechaQuiebre.substring(0,2);
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
        /* String query4 = "SELECT COUNT(Codigo) FROM MAC.dbo.ComprasRequeridas where Codigo = '" + codigo + "' AND verificar <> 3";
         rs = st.executeQuery(query4);
         while(rs.next()){
           count = rs.getInt(1);
           }*/
            //if(count == 0){
            String query1 = "insert into MAC.dbo.ComprasRequeridas(Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',"+ verificar +","+ estado +")";
            st.execute(query1);
         /*}else{
            String query = "UPDATE MAC.dbo.ComprasRequeridas SET verificar = 0 ,contador = '" + fechaM + "' WHERE Codigo = '" + codigo + "' " ;
            st.execute(query);
         }*/
            valor = true;
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR comprasRequeridas(): " + e.getMessage(),0);
        }
        return valor;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
    public float retornarTotalQuiebre(String codigo){
        float cantidad  = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "SELECT SUM(TotalQuiebre) FROM MAC.dbo.ComprasRequeridas WHERE Codigo = '" + codigo + "'";
            rs = st.executeQuery(query1);
            while(rs.next()){
                if(rs.getFloat(1) > 0){
                    cantidad = rs.getFloat(1);
                    //System.out.println(rs.getFloat(1));
                }
                
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR retornarTotalQuiebre(String material): " + e.getMessage(),0);
        }
        return cantidad;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public int retornarCantidadComprasRequeridas(String codigo){
        int cantidad  = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "SELECT COUNT(Codigo) FROM MAC.dbo.ComprasRequeridas WHERE Codigo = '" + codigo + "' AND verificar <> 3";
            rs = st.executeQuery(query1);
            while(rs.next()){
                cantidad = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR retornarCantidadComprasRequeridas(String material): " + e.getMessage(),0);
        }
        return cantidad;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public float retornarStockPlanta(String codigo){
        float cantidad  = 0f;
        ResultSet rs = null;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1  = " select nb_np,nb_linea,MVD_CodProd,MVD_CantAsignada-nb_asignado_a_piso ";
            query1 += " from MAC2BETA.dbo.VW_equipos_pedidos ";
            query1 += " left outer join  ENSchaffner.dbo.TPMP tp";
            query1 += " on(nb_np = tp.Pedido and nb_linea = tp.Linea)";
            query1 += " left outer join  ENSchaffner.dbo.TAvance ta";
            query1 += " on(nb_np = ta.Pedido and nb_linea = ta.Linea and ta.FechaTermino is not null and Semielaborado COLLATE SQL_Latin1_General_CP850_CS_AS = case when tp.Familia ='CELDA' then 'MON' when tp.Familia ='REP' then 'LTX' else vc_proceso end)";
            query1 += " where  ta.FechaTermino is null and MVD_Cant > 0 and MVD_CantAsignada > 0 and MVD_CodProd = '"+codigo+"'";
            rs = st.executeQuery(query1);
            while(rs.next()){
                if(rs.getFloat(1) >= rs.getFloat(2) ){
                    cantidad += rs.getFloat(2);
                }else{
                    cantidad += rs.getFloat(1);
                }
            }
            rs.close();
            rs = st.executeQuery( "SELECT  nb_cantidad FROM MAC2BETA.dbo.TBL_SIS_PISO where vc_codigo = '"+codigo+"'");
            
            while(rs.next()){
                cantidad += rs.getFloat(1);
            }
            rs.close();
            st.close();
            conMAC.close();
            
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR retornarStockPlanta(): " + e.getMessage(),0);
        }
        return cantidad;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public Vector retornarComprasRequeridas(int verificar,String ordenar){
        Vector comprasRequeridas = new Vector();
        String query1 = "";
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(verificar == 1){
                query1 = " SELECT cr.Codigo,cr.Descripcion,cr.cantRequerida,cr.UniMed,pa.Comprador, "
                        +"  cr.verificar, "
                        +"  PRO_INVMIN,PRO_INVMAX ,Cantidad as ordenesPendientes, "
                        +"  (SELECT sum(me.CantidadMP) "
                        +"  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +"  WHERE a.codProducto = me.CodigoTerminado "
                        +"  AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +"  AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                        +"  (SELECT SUM(me.CantidadMP) "
                        +"   FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +"  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        //+"  AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA'"
                        +"  AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                        +"  AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                        +"  ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +"  WHERE ma1.Codigo = cr.Codigo ) "
                        +"  - "
                        +"  (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +"  WHERE de1.Codigo = cr.Codigo) "
                        +"  )  AS MediaCantidad,     "
                        +"  ml.Saldo as Stock,cr.estado, "
                        +"  cr.numeroSap, "
                        +"  (pa.MonedaCompra) as NI, "
                        +"  (pa.Stokeable) as stokeable, cr.fechaEntrega,cr.id,cr.leadTime,isnull(pp.CantAsignada,0) as Planta,pa.analisis,pa.Estado,pa.anaGen,pa.asociado, "
                        +"  (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = cr.Codigo) as obser,pt.Saldo as BPT,ml9.Saldo as Stock9,"
                        
                        + " isnull(("
                        + " select sum(isnull(Cantidad_mat_prima,0)) "
                        + " from  ENSchaffner.dbo.EE_repro_rechazo "
                        + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                        + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                        + " inner join ENSchaffner.dbo.EE_repro_falla"
                        + " on(rep_codigo = rpr_codigo_falla)"
                        + " inner join ENSchaffner.dbo.EE_repro_est "
                        + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                        + " where av_fecha_termino is null and rep_analisis =1 "
                        + " and Codigo_mat_prima = cr.Codigo ),0) as reprocesos"
                        
                        +"  FROM MAC.dbo.ComprasRequeridas  cr "
                        +"  left outer join MAC.dbo.ProductoAnexo pa "
                        +"  on (pa.Codigo = cr.Codigo) "
                        +"  left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +"  on(ml.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +"  left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +"  on(ml9.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +"  left outer join SiaSchaffner.dbo.VW_ordenes_sumadas os "
                        +"  on(os.Codigo = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +"  left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +"  on(pp.MVD_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)"
                        +"  left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +"  on(pt.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)  "
                        /*+"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                        +"  left outer join  SiaSchaffner.dbo.STO_PRODUCTO "
                        +"  on(PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS = cr.Codigo) "
                        +"  WHERE verificar in(1,2,4) "
                        +"  order by "+ordenar+"";
                rs = st.executeQuery(query1);
                int a = 0;
                while(rs.next()){
                    Vector compras = new Vector();
                    compras.addElement(rs.getString(1));
                    compras.addElement(rs.getString(2).replace('"',' '));
                    compras.addElement(new Float(rs.getFloat(3)));
                    compras.addElement(rs.getString(4));
                    compras.addElement(rs.getString(5));
                    //compras.addElement(new Short(rs.getShort(6)));
                    //compras.addElement(new Integer(rs.getInt(7)));
                    compras.addElement(new Byte(rs.getByte(6)));
                    compras.addElement(new Float(rs.getFloat(7)));
                    compras.addElement(new Float(rs.getFloat(8)));
                    //compras.addElement(rs.getDate(11));
                    //compras.addElement(new Float(rs.getFloat(12)));
                    compras.addElement(new Float(rs.getFloat(9)));
                    //compras.addElement(new Float(rs.getFloat(10) + rs.getFloat(11) + rs.getFloat(29)  + rs.getFloat(30)  + rs.getFloat(31)));
                    compras.addElement(new Float(rs.getFloat(10) + rs.getFloat(11) + rs.getFloat(29)));
                    if(rs.getFloat(12) == 0){
                        compras.addElement(new Float(rs.getFloat(12)));
                    }else{
                        compras.addElement(new Float(rs.getFloat(12)/12));
                    }
                    compras.addElement(new Float(rs.getFloat(13)));
                    compras.addElement(new Byte(rs.getByte(14)));
                    compras.addElement(new Integer(rs.getInt(15)));
                    //compras.addElement(new Integer(rs.getInt(21)));
                    compras.addElement(new Integer(rs.getInt(16)));
                    compras.addElement(new Integer(rs.getInt(17)));
                    compras.addElement(rs.getDate(18));
                    compras.addElement(new Integer(rs.getInt(19)));
                    compras.addElement(new Short(rs.getShort(20)));
                    compras.addElement(new Float(rs.getFloat(21)));
                    compras.addElement(new Byte(rs.getByte(22)));
                    compras.addElement(new Byte(rs.getByte(23)));
                    compras.addElement(new Byte(rs.getByte(24)));
                    compras.addElement(rs.getString(25));
                    compras.addElement(new Integer(rs.getInt(26)));
                    compras.addElement(new Float(rs.getFloat(27)));
                    compras.addElement(new Float(rs.getFloat(27)+rs.getFloat(13)));
                    compras.addElement(new Float(rs.getFloat(28)));
                    comprasRequeridas.addElement(compras);
                }
            }else if(verificar == 0){
                /*query1 = " SELECT Codigo,Descripcion,UniMed,comprador,leadTime,stockMin, "
                        +" stockMax FROM MAC.dbo.ComprasRequeridas  "
                        +" WHERE verificar = 0 ORDER BY Codigo";*/
                query1 = " SELECT cr.Codigo,Descripcion,UniMed,pa.Comprador,leadTime,stockMin, "
                        +" stockMax "
                        +" FROM MAC.dbo.ComprasRequeridas cr "
                        +" left outer join MAC.dbo.ProductoAnexo pa   "
                        +" on(cr.Codigo = pa.Codigo) "
                        +" WHERE verificar = 0 ORDER BY cr.Codigo";
                rs = st.executeQuery(query1);
                while(rs.next()){
                    Vector compras = new Vector();
                    compras.addElement(rs.getString(1));
                    compras.addElement(rs.getString(2));
                    compras.addElement(rs.getString(3));
                    compras.addElement(rs.getString(4));
                    compras.addElement(new Short(rs.getShort(5)));
                    compras.addElement(new Float(rs.getFloat(6)));
                    compras.addElement(new Float(rs.getFloat(7)));
                    comprasRequeridas.addElement(compras);
                }
            }else{
                query1 =  " SELECT Codigo ,fechaEntrega "
                        +" FROM MAC.dbo.ComprasRequeridas "
                        +" WHERE verificar  = 1  AND numeroSap IS NULL "
                        +" ORDER BY Codigo ";
                rs = st.executeQuery(query1);
                while(rs.next()){
                    Vector compras = new Vector();
                    compras.addElement(rs.getString(1));
                    compras.addElement(rs.getDate(2));
                    comprasRequeridas.addElement(compras);
                }
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR retornarComprasRequeridas(): " + e.getMessage(),0);
        }
        return comprasRequeridas;
    }
    
    public Vector retornarComprasRequeridasPorOrdenCompras(int numero,byte verificar){
        Vector comprasRequeridas = new Vector();
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(verificar == 1){
                
                
                /*query1 = " SELECT cr.Codigo,cr.Descripcion,cr.cantRequerida,cr.UniMed,pa.Comprador,cr.verificar, "
                        +" stockMin,stockMax , "
                        +"  (select (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) from  SiaSchaffner.dbo.STO_MOVDET tcc "
                        +" where (tcc.MVD_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 "
                        +"  and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0)) as ordenesPendientes, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                        +" AND a.proceso = CodigoSemiElaborado "
                        +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                        +" AND ma.readTime >= '" + fechaM + "'  "
                        +" AND ma.vigencia != 1 "
                        +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                 
                        //+" (SELECT SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial WHERE codigo = cr.Codigo)  AS MediaCantidad, "
                        +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1 "
                        +" WHERE ma1.Codigo = cr.Codigo ) "
                        +" - "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = cr.Codigo) "
                        +" )  AS MediaCantidad,     "
                        +" ml.Saldo as Stock1,cr.estado, "
                        +" cr.numeroSap, "
                        +" pa.MonedaCompra  as NI, "
                        +" pa.Stokeable as stokeable "
                        +"  ,cr.fechaEntrega,cr.id,cr.leadTime,isnull(sp.cantidad,0),pa.analisis,pa.Estado,pa.anaGen,pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = cr.Codigo) as obser "
                        +" FROM MAC.dbo.ComprasRequeridas  cr "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" left outer join MAC.dbo.LG_stock_planta sp "
                        +" on(cr.Codigo = sp.codigo) "
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on (pa.Codigo = cr.Codigo) "
                        +" WHERE  cr.numeroSap =   "+ numero +" "
                        +" order by cr.Codigo ";*/
                query1  = " SELECT cr.Codigo,cr.Descripcion,cr.cantRequerida,cr.UniMed,pa.Comprador,cr.verificar, ";
                query1 += " stockMin,stockMax , ";
                query1 += " (select (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) from  SiaSchaffner.dbo.STO_MOVDET tcc ";
                query1 += " where (tcc.MVD_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 ";
                query1 += "  and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0)) as ordenesPendientes, ";
                query1 += " (SELECT sum(me.CantidadMP) ";
                query1 += " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
                query1 += " WHERE a.codProducto = me.CodigoTerminado ";
                query1 += " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA' ";
                query1 += " AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, ";
                query1 += " (SELECT SUM(me.CantidadMP) ";
                query1 += " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma ";
                query1 += " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
                query1 += " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA' ";
                query1 += " AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, ";
                query1 += " ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1 ";
                query1 += " WHERE ma1.Codigo = cr.Codigo ) ";
                query1 += " - ";
                query1 += " (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 ";
                query1 += " WHERE de1.Codigo = cr.Codigo) ";
                query1 += " )  AS MediaCantidad,     ";
                query1 += " ml.Saldo as Stock1,cr.estado,";
                query1 += " cr.numeroSap, ";
                query1 += " pa.MonedaCompra  as NI, ";
                query1 += " pa.Stokeable as stokeable ";
                query1 += " ,cr.fechaEntrega,cr.id,cr.leadTime,isnull(pp.CantAsignada,0),pa.analisis,pa.Estado,pa.anaGen,pa.asociado, ";
                query1 += " (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = cr.Codigo) as obser,pt.Saldo as BPT ";
                query1 += " FROM MAC.dbo.ComprasRequeridas  cr ";
                query1 += " left outer join SiaSchaffner.dbo.VW_stock_materiales ml ";
                query1 += " on(ml.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) ";
                query1 += " left outer join MAC2BETA.dbo.VW_pedidos_planta pp";
                query1 += " on(pp.MVD_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
                query1 += " left outer join MAC.dbo.ProductoAnexo pa ";
                query1 += " on (pa.Codigo = cr.Codigo) ";
                query1 += " left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt ";
                query1 += " on(pt.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) ";
                query1 += " WHERE  cr.numeroSap =   "+ numero;
                query1 += " order by cr.Codigo ";
                
            }else{
                
                
                query1 = " SELECT cr.Codigo,cr.Descripcion,cr.cantRequerida,cr.UniMed,pa.Comprador,cr.verificar,cr.stockMin, "
                        +" cr.stockMax, "
                        +" os.Cantidad as ordenesPendientes, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me  "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA'"
                        +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                        
                        //+" (SELECT SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial WHERE codigo = cr.Codigo)  AS MediaCantidad, "
                        +" ((SELECT sum(Cantidad) FROM MAC.dbo.consumosPorMaterial1 ma1 "
                        +" WHERE ma1.Codigo = cr.Codigo ) "
                        +" - "
                        +" (SELECT sum(Cantidad) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = cr.Codigo) "
                        +" )  AS MediaCantidad,     "
                        +" ml.Saldo as Stock1,cr.estado, "
                        +" cr.numeroSap, "
                        +"  pa.MonedaCompra as procedencia, "
                        +" pa.Stokeable as stokeable "
                        +" ,cr.fechaEntrega,cr.id,cr.leadTime,isnull(pp.CantAsignada,0),pa.analisis,pa.Estado,pa.anaGen,pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = cr.Codigo) as obser,pt.Saldo as BPT "
                        +" FROM SiaSchaffner.dbo.VW_ordenes_compra toc "
                        +" left outer join  MAC.dbo.ComprasRequeridas cr "
                        +" on(toc.MVD_CodProd COLLATE Modern_Spanish_CI_AI = cr.Codigo) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = toc.MVD_CodProd) "
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on (pa.Codigo = cr.Codigo) "
                        +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +" on(pp.MVD_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)"
                        +" left outer join SiaSchaffner.dbo.VW_ordenes_sumadas os "
                        +" on(os.Codigo COLLATE Modern_Spanish_CI_AI = cr.Codigo) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" WHERE cr.verificar in(1,2,4)  "
                        +" and toc.MVE_FolioFisico =   " + numero + " "
                        +" order by cr.Codigo    ";
            }
            rs = st.executeQuery(query1);
            while(rs.next()){
                Vector compras = new Vector();
                compras.addElement(rs.getString(1));
                compras.addElement(rs.getString(2).replace('"',' '));
                compras.addElement(new Float(rs.getFloat(3)));
                compras.addElement(rs.getString(4));
                compras.addElement(rs.getString(5));
                //compras.addElement(new Short(rs.getShort(6)));
                //compras.addElement(new Integer(rs.getInt(7)));
                compras.addElement(new Byte(rs.getByte(6)));
                compras.addElement(new Float(rs.getFloat(7)));
                compras.addElement(new Float(rs.getFloat(8)));
                //compras.addElement(rs.getDate(11));
                //compras.addElement(new Float(rs.getFloat(12)));
                compras.addElement(new Float(rs.getFloat(9)));
                compras.addElement(new Float(rs.getFloat(10) + rs.getFloat(11)));
                if(rs.getFloat(12) == 0){
                    compras.addElement(new Float(rs.getFloat(12)));
                }else{
                    compras.addElement(new Float(rs.getFloat(12)/12));
                }
                compras.addElement(new Float(rs.getFloat(13)));
                compras.addElement(new Byte(rs.getByte(14)));
                compras.addElement(new Integer(rs.getInt(15)));
                //compras.addElement(new Integer(rs.getInt(21)));
                compras.addElement(new Integer(rs.getInt(16)));
                compras.addElement(new Integer(rs.getInt(17)));
                compras.addElement(rs.getDate(18));
                compras.addElement(new Integer(rs.getInt(19)));
                compras.addElement(new Short(rs.getShort(20)));
                compras.addElement(new Float(rs.getFloat(21)));
                compras.addElement(new Byte(rs.getByte(22)));
                compras.addElement(new Byte(rs.getByte(23)));
                compras.addElement(new Byte(rs.getByte(24)));
                compras.addElement(rs.getString(25));
                compras.addElement(new Integer(rs.getInt(26)));
                compras.addElement(new Float(rs.getFloat(27)));
                comprasRequeridas.addElement(compras);
            }
            
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR retornarComprasRequeridasPorOrdenCompras(): " + e.getMessage(),0);
        }
        return comprasRequeridas;
    }
    public Vector retornarComprasRequeridasAsociados(String numero){
        Vector comprasRequeridas = new Vector();
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            
            /*query1 = " SELECT pa.Codigo,PRO_DESC,cr.cantRequerida,PRO_UMPRINCIPAL,pa.Comprador,cr.verificar,PRO_INVMIN,PRO_INVMAX, "
                    +" os.Cantidad as ordenesPendientes, "
                    +"  (SELECT sum(me.CantidadMP) "
                    +"  FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me  "
                    +"  WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '20110303' "
                    +"  AND a.proceso = CodigoSemiElaborado "
                    +"  AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                    +"  (SELECT SUM(me.CantidadMP) "
                    +"  FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma "
                    +"  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    +"  AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    +"  AND ma.readTime >= '20110303' "
                    +"  AND ma.vigencia != 1 "
                    +"  AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                    +"  ((SELECT sum(Cantidad) FROM MAC.dbo.consumosPorMaterial1 ma1 "
                    +"  WHERE ma1.Codigo = cr.Codigo ) "
                    +"  - "
                    +"  (SELECT sum(Cantidad) FROM MAC.dbo.devolucionesPorMaterial de1 "
                    +"  WHERE de1.Codigo = cr.Codigo) "
                    +"  )/12  AS MediaCantidad,     "
                    +"  ml.Saldo as Stock1,cr.estado, "
                    +"  cr.numeroSap, "
                    +"   pa.MonedaCompra as procedencia, "
                    +"  pa.Stokeable as stokeable "
                    +"  ,cr.fechaEntrega,cr.id,cr.leadTime,isnull(sp.cantidad,0),pa.analisis,pa.Estado,pa.anaGen,pa.asociado, "
                    +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = cr.Codigo) as obser "
                    +" from MAC.dbo.ProductoAnexo pa "
                    +"  left outer join SiaSchaffner.dbo.VW_ordenes_compra toc "
                    +"  left outer join  MAC.dbo.ComprasRequeridas cr "
                    +"  on(toc.MVD_CodProd COLLATE Modern_Spanish_CI_AI = cr.Codigo) "
                    +"  left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                    +"  on(ml.PSL_CodProd = toc.MVD_CodProd) "
                    +"  on (pa.Codigo = cr.Codigo) "
                    +"  left outer join MAC.dbo.LG_stock_planta sp "
                    +"  on(toc.MVD_CodProd COLLATE Modern_Spanish_CI_AI = sp.codigo) "
                    +"  left outer join SiaSchaffner.dbo.VW_ordenes_sumadas os "
                    +"  on(os.Codigo COLLATE Modern_Spanish_CI_AI = cr.Codigo) "
                    +" LEFT OUTER JOIN SiaSchaffner.dbo.STO_PRODUCTO "
                    +" on(PRO_CODPROD COLLATE Modern_Spanish_CI_AI = pa.Codigo) ";
            query1 +="  WHERE pa.asociado = '-1' ";
            query1 +="  order by cr.Codigo   ";*/
            
            query1 = " SELECT pa.Codigo,PRO_DESC,cr.cantRequerida,PRO_UMPRINCIPAL,pa.Comprador,cr.verificar,PRO_INVMIN,PRO_INVMAX, "
                    +" os.Cantidad as ordenesPendientes, "
                    +"  (SELECT sum(me.CantidadMP) "
                    +"  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me  "
                    +"  WHERE a.codProducto = me.CodigoTerminado "
                    +"  AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA' "
                    +"  AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                    +"  (SELECT SUM(me.CantidadMP) "
                    +"  FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    +"  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    +"  AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA' "
                    +"  AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                    +"  ((SELECT sum(Cantidad) FROM MAC.dbo.consumosPorMaterial1 ma1 "
                    +"  WHERE ma1.Codigo = cr.Codigo ) "
                    +"  - "
                    +"  (SELECT sum(Cantidad) FROM MAC.dbo.devolucionesPorMaterial de1 "
                    +"  WHERE de1.Codigo = cr.Codigo) "
                    +"  )/12  AS MediaCantidad,     "
                    +"  ml.Saldo as Stock1,cr.estado, "
                    +"  cr.numeroSap, "
                    +"   pa.MonedaCompra as procedencia, "
                    +"  pa.Stokeable as stokeable "
                    +"  ,cr.fechaEntrega,cr.id,cr.leadTime,isnull(pp.CantAsignada,0),pa.analisis,pa.Estado,pa.anaGen,pa.asociado, "
                    +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = cr.Codigo) as obser,pt.Saldo as BPT "
                    +" from MAC.dbo.ProductoAnexo pa "
                    +"  left outer join SiaSchaffner.dbo.VW_ordenes_compra toc "
                    +"  left outer join  MAC.dbo.ComprasRequeridas cr "
                    +"  on(toc.MVD_CodProd COLLATE Modern_Spanish_CI_AI = cr.Codigo) "
                    +"  left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                    +"  on(ml.PSL_CodProd = toc.MVD_CodProd) "
                    +"  on (pa.Codigo = cr.Codigo) "
                    +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                    +" on(pp.MVD_CodProd = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)"
                    +"  left outer join SiaSchaffner.dbo.VW_ordenes_sumadas os "
                    +"  on(os.Codigo COLLATE Modern_Spanish_CI_AI = cr.Codigo) "
                    +" LEFT OUTER JOIN SiaSchaffner.dbo.STO_PRODUCTO "
                    +" on(PRO_CODPROD COLLATE Modern_Spanish_CI_AI = pa.Codigo) "
                    +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                    +" on(pt.PSL_CodProd = toc.MVD_CodProd) "
                    +"  WHERE pa.asociado = '-1' "
                    +"  order by cr.Codigo   ";
            
            rs = st.executeQuery(query1);
            while(rs.next()){
                Vector compras = new Vector();
                compras.addElement(rs.getString(1));
                compras.addElement(rs.getString(2).replace('"',' '));
                compras.addElement(new Float(rs.getFloat(3)));
                compras.addElement(rs.getString(4));
                compras.addElement(rs.getString(5));
                //compras.addElement(new Short(rs.getShort(6)));
                //compras.addElement(new Integer(rs.getInt(7)));
                compras.addElement(new Byte(rs.getByte(6)));
                compras.addElement(new Float(rs.getFloat(7)));
                compras.addElement(new Float(rs.getFloat(8)));
                //compras.addElement(rs.getDate(11));
                //compras.addElement(new Float(rs.getFloat(12)));
                compras.addElement(new Float(rs.getFloat(9)));
                compras.addElement(new Float(rs.getFloat(10) + rs.getFloat(11)));
                compras.addElement(new Float(rs.getFloat(12)));
                compras.addElement(new Float(rs.getFloat(13)));
                compras.addElement(new Byte(rs.getByte(14)));
                compras.addElement(new Integer(rs.getInt(15)));
                //compras.addElement(new Integer(rs.getInt(21)));
                compras.addElement(new Integer(rs.getInt(16)));
                compras.addElement(new Integer(rs.getInt(17)));
                compras.addElement(rs.getDate(18));
                compras.addElement(new Integer(rs.getInt(19)));
                compras.addElement(new Short(rs.getShort(20)));
                compras.addElement(new Float(rs.getFloat(21)));
                compras.addElement(new Byte(rs.getByte(22)));
                compras.addElement(new Byte(rs.getByte(23)));
                compras.addElement(new Byte(rs.getByte(24)));
                compras.addElement(rs.getString(25));
                compras.addElement(new Integer(rs.getInt(26)));
                comprasRequeridas.addElement(compras);
            }
            
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " retornarComprasRequeridasAsociados() " + e.getMessage(),0);
        }
        return comprasRequeridas;
    }
    
//----------------------------------------------------------------------------------------------------------------
    public Vector retornarComprasRequeridasHidroPack(){
        Vector comprasRequeridasHidroPack = new Vector();
        String query1 = "";
        GregorianCalendar calendario = new GregorianCalendar();
        int ano = Integer.parseInt(fechaM.substring(0,4));
        String mes = fechaM.substring(4,6);
        calendario.set( Calendar.MONTH, Integer.parseInt(mes)-1);
        String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            query1 = "select DISTINCT(me.CodigoMatPrima),ma.PRO_DESC,cr.cantRequerida,ma.PRO_UMPRINCIPAL,cr.comprador,cr.leadTime,cr.contador,cr.verificar, "
                    +"   stockMin,stockMax, "
                    +"   cr.fechaEntrega, "
                    +"   (select sum(tc.MVD_Cant-tc.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tc "
                    +"   where  tc.MVD_CodProd COLLATE Modern_Spanish_CI_AI = me.CodigoMatPrima and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega < '" + fechaM + "' "
                    +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0) as ordenVencida, "
                    +"   (select sum(tc.MVD_Cant-tc.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tc "
                    +"   where  tc.MVD_CodProd COLLATE Modern_Spanish_CI_AI = me.CodigoMatPrima and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega >= '" + fechaM + "' "
                    +"   and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0) as ordenFutura, "
                    +"   (SELECT sum(mt.CantidadMP) "
                    +"   FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura mt "
                    +"   WHERE a.codProducto = mt.CodigoTerminado "
                    +"   AND a.proceso = CodigoSemiElaborado "
                    +"   AND mt.CodigoMatPrima in(me.CodigoMatPrima)) as NecesidadPorEquipo_no_Celda, "
                    +"   (SELECT SUM(mt.CantidadMP) "
                    +"   FROM ENSchaffner.dbo.MEstructura mt,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    +"   WHERE mt.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    +"   AND ma.proceso = 'MON' and mt.CodigoTerminado like 'B%' "
                    +"   AND mt.CodigoMatPrima in(me.CodigoMatPrima)) as NecesidadPorEquipo_si_Celda, "
                    +"   ( "
                    +"   select sum(mo.MVD_Cant)/12 "
                    +"   from SiaSchaffner.dbo.STO_MOVDET mo "
                    +"   where mo.MVD_Fecha between '" + String.valueOf(ano-1)+mes+"01" + "' and '" + String.valueOf(ano)+mes+dia1 + "'  "
                    +"   and mo.MVD_CodProd  = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS "
                    +"   and mo.MVD_FactorInventario = -1 "
                    +"   ) as MediaCantidad, "
                    +"   (SELECT sum(ml.PSL_Saldo) FROM SiaSchaffner.dbo.STO_PRODSAL ml WHERE ml.PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega <> 7) as Stock,cr.estado, "
                    +"   cr.numeroSap,cr.id, "
                    +"   (SELECT pa.MonedaCompra from MAC.dbo.ProductoAnexo pa Where pa.Codigo = me.CodigoMatPrima) as NI, "
                    +"   (SELECT tc.Stokeable FROM MAC.dbo.ProductoAnexo tc WHERE  tc.Codigo = me.CodigoMatPrima) as stokeable "
                    +"   FROM  ENSchaffner.dbo.TPMP tp "
                    +"   left outer join ENSchaffner.dbo.MEstructura me "
                    +"   ON  tp.Codpro = me.CodigoTerminado  "
                    +"   left outer join MAC.dbo.ProductoAnexo pa "
                    +"   ON  me.CodigoMatPrima = pa.Codigo  "
                    +"  LEFT OUTER JOIN SiaSchaffner.dbo.STO_PRODUCTO ma "
                    +"  ON (me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS=  ma.PRO_CODPROD) "
                    +"  LEFT OUTER JOIN MAC.dbo.ComprasRequeridas cr "
                    +"  ON (me.CodigoMatPrima =  cr.Codigo and cr.verificar <> 3) "
                    +"  LEFT OUTER JOIN MAC.dbo.MaterialSeleccionado ms "
                    +"  ON (me.CodigoMatPrima =  ms.codigoMaterial) "
                    +"  WHERE tp.Status <> 'Bodega' AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 AND tp.Status <> 'Reproceso' "
                    +"  and ms.codigoMaterial IS NULL and cr.Codigo is not null and tp.Division = 3  ";
            rs = st.executeQuery(query1);
            while(rs.next()){
                Vector compras = new Vector();
                compras.addElement(rs.getString(1));
                String caracter = "";
                caracter = rs.getString(2);
                for(int i=0;i<rs.getString(2).length();i++){
                    if(rs.getString(2).charAt(i) == '"'){
                        caracter = rs.getString(2).replace('"',' ');
                    }
                }
                compras.addElement(caracter);
                compras.addElement(new Float(rs.getFloat(3)));
                compras.addElement(rs.getString(4));
                compras.addElement(rs.getString(5));
                compras.addElement(new Short(rs.getShort(6)));
                compras.addElement(rs.getDate(7));
                compras.addElement(new Byte(rs.getByte(8)));
                compras.addElement(new Float(rs.getFloat(9)));
                compras.addElement(new Float(rs.getFloat(10)));
                compras.addElement(rs.getDate(11));
                compras.addElement(new Float(rs.getFloat(12)));
                compras.addElement(new Float(rs.getFloat(13)));
                compras.addElement(new Float(rs.getFloat(14) + rs.getFloat(15)));
                compras.addElement(new Float(rs.getFloat(16)));
                compras.addElement(new Float(rs.getFloat(17)));
                compras.addElement(new Byte(rs.getByte(18)));
                compras.addElement(new Integer(rs.getInt(19)));
                compras.addElement(new Integer(rs.getInt(20)));
                compras.addElement(new Integer(rs.getInt(21)));
                compras.addElement(new Integer(rs.getInt(22)));
                comprasRequeridasHidroPack.addElement(compras);
            }
            
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " retornarComprasRequeridasHidroPack() " + e.getMessage(),0);
        }
        return comprasRequeridasHidroPack;
    }
//----------------------------------------------------------------------------------------------------------------
    public Vector  retornarEstadoMateriales(String codigoIni,String codigoFin,String rango,int valor,byte estado){
            Vector comprasRequeridas = new Vector();
        GregorianCalendar calendario = new GregorianCalendar();
        int ano = Integer.parseInt(fechaM.substring(0,4));
        String mes = fechaM.substring(4,6);
        calendario.set( Calendar.MONTH, Integer.parseInt(mes)-1);
        String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(valor == 1){
                if(codigoIni.equals("8011001147")){
                    
                    query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                            +" (SELECT sum(me.CantidadMP) "
                            +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                            +" WHERE a.codProducto = me.CodigoTerminado "
                            +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                            +" AND me.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_no_Celda, "
                            +" (SELECT SUM(me.CantidadMP) "
                            +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                            +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                            +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA' "
                            +" AND me.CodigoMatPrima  = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_si_Celda,   "
                            +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                            +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                            +" - "
                            +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                            +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS "
                            +" )  AS MediaCantidad,     "
                            +" tc.PRO_INVMIN, "
                            +" tc.PRO_INVMAX,  "
                            +"(ml.Saldo) as Stock, "
                            +" (0) as Stock1 "
                            +" ,tc.PRO_UMPRINCIPAL, "
                            +"  (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as LeadTime,      "
                            +" (select sum(tce.MVD_Cant-tce.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tce "
                            +" where tce.MVD_CodProd = tc.PRO_CODPROD  and tce.MVD_CodSistema=7 and tce.MVD_CodClase =1 AND tce.MVD_TipoDoc = 1 and tce.MVD_FechaEntrega < '" + fechaM + "' "
                            +" and tce.MVD_Cant-tce.MVD_CantAsignada > 0) as ordenVencida, "
                            +" (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) as ordenFutura, "
                            +" isnull(pp.CantAsignada,0), "
                            +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                            +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as obser,pt.Saldo as BPT,ml9.Saldo as Stock9,"
                            
                            + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos"
                            
                            +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                            +" left outer join SiaSchaffner.dbo.STO_MOVDET tcc "
                            +" on(tcc.MVD_CodProd = tc.PRO_CODPROD  and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 and tcc.MVD_FechaEntrega >= '" + fechaM + "' "
                            +" and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0) "
                            +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                            +" on(ml.PSL_CodProd = tc.PRO_CODPROD) "
                            +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                            +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                            +" left outer join MAC.dbo.ProductoAnexo pa "
                            +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                            +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                            +" on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                            +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                            +" on(pt.PSL_CodProd = tc.PRO_CODPROD)    "
                            
                            /*+"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                            +"  on(msf.vc_codigo = cr.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                            
                            +" WHERE tc.PRO_CODPROD >=  '" + codigoIni + "' "
                            //+" and (tc.PRO_CODTIPO in (1,5,8) or tc.PRO_CODPROD like '5041%') "
                            +" group by pa.asociado,tc.PRO_CODPROD,tc.PRO_DESC,pa.Comprador,ml9.Saldo,tc.PRO_INVMIN,isnull(pp.CantAsignada,0),tc.PRO_INVMAX,pt.Saldo,ml.Saldo,tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen "
                            +" ORDER BY tc.PRO_CODPROD ";
                }else{
                    
                    query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                            +" (SELECT sum(me.CantidadMP) "
                            +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                            +" WHERE a.codProducto = me.CodigoTerminado "
                            +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                            +" AND me.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_no_Celda, "
                            +" (SELECT SUM(me.CantidadMP) "
                            +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                            +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                            +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                            +" AND me.CodigoMatPrima  = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_si_Celda,   "
                            
                            //+" (select sum(cantidad)/12 from MAC.dbo.consumosPorMaterial where codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as MediaCantidad, "
                            +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                            +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ) "
                            +" - "
                            +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                            +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                            +" )  AS MediaCantidad,     "
                            +" tc.PRO_INVMIN, "
                            +" tc.PRO_INVMAX,  "
                            +" (ml.Saldo) as Stock, "
                            //+" (pro.PSL_Saldo) as Stock1 "
                            +" (0) as Stock1 "
                            +" ,tc.PRO_UMPRINCIPAL, "
                            +"  (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as LeadTime,      "
                            +" (select sum(tce.MVD_Cant-tce.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tce "
                            +" where tce.MVD_CodProd = tc.PRO_CODPROD  and tce.MVD_CodSistema=7 and tce.MVD_CodClase =1 AND tce.MVD_TipoDoc = 1 and tce.MVD_FechaEntrega < '" + fechaM + "' "
                            +" and tce.MVD_Cant-tce.MVD_CantAsignada > 0) as ordenVencida, "
                            +"  (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) as ordenFutura, "
                            //+" isnull(sp.cantidad,0), "
                            +" isnull(pp.CantAsignada,0), "
                            +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                            +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as obser,pt.Saldo as BPT,ml9.Saldo as Stock9,"
                            
                            + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos, "
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=tc.PRO_CODPROD),0)as integer)as Cantidad "
                            
                            +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                            +" left outer join SiaSchaffner.dbo.STO_MOVDET tcc "
                            +" on(tcc.MVD_CodProd = tc.PRO_CODPROD  and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 and tcc.MVD_FechaEntrega >= '" + fechaM + "' "
                            +" and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0) "
                            +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                            +" on(ml.PSL_CodProd = tc.PRO_CODPROD) "
                            +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                            +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                            /*+" left outer join SiaSchaffner.dbo.STO_PRODSAL pro "
                            +" on(pro.PSL_CodProd =tc.PRO_CODPROD  and pro.PSL_CodBodega in(21)) "*/
                            +" left outer join MAC.dbo.ProductoAnexo pa "
                            +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                            +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                            +" on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                            +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                            +" on(pt.PSL_CodProd = tc.PRO_CODPROD)    "
                            
                           /* +"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                            +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                            
                            +" WHERE tc.PRO_CODPROD BETWEEN '" + codigoIni + "'  AND '" + codigoFin + "' "
                            //+" and (tc.PRO_CODTIPO in (1,5,8) or tc.PRO_CODPROD like '5041%') "
                            +" group by tc.PRO_CODPROD,pa.asociado,pa.Comprador,tc.PRO_DESC,ml9.Saldo,tc.PRO_INVMIN,tc.PRO_INVMAX,pt.Saldo,ml.Saldo,isnull(pp.CantAsignada,0),tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen "
                            +" ORDER BY tc.PRO_CODPROD ";
                    
                    
                }
            }else if(valor == 0){
                
                query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +" AND me.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                        +" AND me.CodigoMatPrima  = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_si_Celda,   "
                        
                        //+" (select sum(cantidad)/12 from MAC.dbo.consumosPorMaterial where codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as MediaCantidad, "
                        +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ) "
                        +" - "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" )  AS MediaCantidad,     "
                        +" tc.PRO_INVMIN, "
                        +" tc.PRO_INVMAX,  "
                        +" (ml.Saldo) as Stock, "
                        //+" (pro.PSL_Saldo) as Stock1 "
                        +" (0) as Stock1 "
                        +" ,tc.PRO_UMPRINCIPAL, "
                        +"  (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as LeadTime,      "
                        +" (select sum(tce.MVD_Cant-tce.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tce "
                        +" where tce.MVD_CodProd = tc.PRO_CODPROD  and tce.MVD_CodSistema=7 and tce.MVD_CodClase =1 AND tce.MVD_TipoDoc = 1 and tce.MVD_FechaEntrega < '" + fechaM + "' "
                        +" and tce.MVD_Cant-tce.MVD_CantAsignada > 0) as ordenVencida, "
                        +"  (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) as ordenFutura, "
                        //+" isnull(sp.cantidad,0), "
                        +" isnull(pp.CantAsignada,0), "
                        +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as obser,pt.Saldo,ml9.Saldo as Stock9, "
                        
                        + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos, "
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=tc.PRO_CODPROD),0)as integer)as Cantidad "
                        
                        +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                        +" left outer join SiaSchaffner.dbo.STO_MOVDET tcc "
                        +" on(tcc.MVD_CodProd = tc.PRO_CODPROD  and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 and tcc.MVD_FechaEntrega >= '" + fechaM + "' "
                        +" and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = tc.PRO_CODPROD ) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                        /*+" left outer join SiaSchaffner.dbo.STO_PRODSAL pro "
                        +" on(pro.PSL_CodProd =tc.PRO_CODPROD  and pro.PSL_CodBodega in(21)) "*/
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +" on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd = tc.PRO_CODPROD)    "
                        
                        
                       /* +"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                        
                        
                        +" WHERE  tc.PRO_DESC LIKE  '%"+ rango.toUpperCase() +"%' "
                        //+" and (tc.PRO_CODTIPO in (1,5,8) or tc.PRO_CODPROD like '5041%') "
                        +" group by tc.PRO_CODPROD,pa.asociado,pa.Comprador,tc.PRO_DESC,ml9.Saldo,tc.PRO_INVMIN,isnull(pp.CantAsignada,0),tc.PRO_INVMAX,pt.Saldo,ml.Saldo,tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen "
                        +" ORDER BY tc.PRO_CODPROD ";
                
                
                
                
            }else if(valor == 3){
                
                query1 = " select man.CodigoMatPrima,PRO_DESC, "
                        +" (SELECT sum(me.CantidadMP) "
                        +"  FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +"  WHERE a.codProducto = me.CodigoTerminado "
                        +"  AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +"  AND me.CodigoMatPrima = man.CodigoMatPrima) AS NecesidadPorEquipo_no_Celda, "
                        +"  (SELECT SUM(me.CantidadMP) "
                        +"  FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +"  WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +"  AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                        +"  AND me.CodigoMatPrima  = man.CodigoMatPrima) AS NecesidadPorEquipo_si_Celda,  "
                        
                        //+"  (select sum(cantidad)/12 from MAC.dbo.consumosPorMaterial where codigo = man.CodigoMatPrima) as MediaCantidad, "
                        +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1 "
                        +"  WHERE ma1.Codigo = man.CodigoMatPrima ) "
                        +"  - "
                        +"  (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +"  WHERE de1.Codigo = man.CodigoMatPrima) "
                        +"  )  AS MediaCantidad,     "
                        +"  PRO_INVMIN, "
                        +"  PRO_INVMAX, "
                        +"  ml.Saldo,0,PRO_UMPRINCIPAL,pa.LeadTime,osu.Cantidad,0,isnull(pp.CantAsignada,0), "
                        +"  pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado,  "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = man.CodigoMatPrima) as obser,pt.Saldo,ml9.Saldo as Stock9,"
                        
                        + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = man.CodigoMatPrima ),0) as reprocesos ,"                             
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=man.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS),0)as integer)as Cantidad "
                        
                        +"  from ENschaffner.dbo.TPMP "
                        +"  inner join ENSchaffner.dbo.MEstructura man "
                        +"  on(CodigoTerminado = Codpro) "
                        +"  inner join SiaSchaffner.dbo.STO_PRODUCTO  "
                        +"  on(man.CodigoMatPrima = PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +"  left outer join SiaSchaffner.dbo.VW_stock_materiales ml"
                        +"  on(ml.PSL_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS = man.CodigoMatPrima) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +" on(ml9.PSL_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS = man.CodigoMatPrima) "
                        +"  left outer join MAC.dbo.ProductoAnexo pa "
                        +"  on(pa.Codigo = man.CodigoMatPrima) "
                        +"  left outer join SiaSchaffner.dbo.VW_ordenes_sumadas osu "
                        +"  on(osu.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = man.CodigoMatPrima) "
                        +"  left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +"  on(pp.MVD_CodProd = man.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS)"
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS = man.CodigoMatPrima)    "
                        
                        /*+"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                        
                        +"  where Pedido = '" + rango + "' "
                        +"  group by CodigoMatPrima,PRO_DESC,PRO_INVMIN,ml9.Saldo,PRO_INVMAX,pt.Saldo,ml.Saldo,LeadTime,PRO_UMPRINCIPAL,osu.Cantidad,isnull(pp.CantAsignada,0), "
                        +"  pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,pa.Comprador,pa.asociado ";
                
                
            }else if(valor == 4){
                
                query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +" AND me.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                        +" AND me.CodigoMatPrima  = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_si_Celda,   "
                        
                        //+" (select sum(cantidad)/12 from MAC.dbo.consumosPorMaterial where codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as MediaCantidad, "
                        +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ) "
                        +" - "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" )  AS MediaCantidad,     "
                        +" tc.PRO_INVMIN, "
                        +" tc.PRO_INVMAX,  "
                        +" (ml.Saldo) as Stock, "
                        //+" (pro.PSL_Saldo) as Stock1 "
                        +" (0) as Stock1 "
                        +" ,tc.PRO_UMPRINCIPAL, "
                        +"  (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as LeadTime,      "
                        +" (select sum(tce.MVD_Cant-tce.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tce "
                        +" where tce.MVD_CodProd = tc.PRO_CODPROD  and tce.MVD_CodSistema=7 and tce.MVD_CodClase =1 AND tce.MVD_TipoDoc = 1 and tce.MVD_FechaEntrega < '" + fechaM + "' "
                        +" and tce.MVD_Cant-tce.MVD_CantAsignada > 0) as ordenVencida, "
                        +"  (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) as ordenFutura, "
                        //+" isnull(sp.cantidad,0), "
                        +" isnull(pp.CantAsignada,0), "
                        +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as obser,pt.Saldo,ml9.Saldo as Stock9,"
                        
                         + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos ,"
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=tc.PRO_CODPROD),0)as integer)as Cantidad "
                        
                        +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                        +" left outer join SiaSchaffner.dbo.STO_MOVDET tcc "
                        +" on(tcc.MVD_CodProd = tc.PRO_CODPROD  and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 and tcc.MVD_FechaEntrega >= '" + fechaM + "' "
                        +" and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = tc.PRO_CODPROD) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                        /*+" left outer join SiaSchaffner.dbo.STO_PRODSAL pro "
                        +" on(pro.PSL_CodProd =tc.PRO_CODPROD  and pro.PSL_CodBodega in(21)) "*/
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +"  left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +"  on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd = tc.PRO_CODPROD) ";
                
                       /* +"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) ";*/
                
                query1 += " WHERE  pa.asociado = '" + rango + "'"; 
                query1 +=" group by pa.asociado,tc.PRO_CODPROD,pa.Comprador,ml9.Saldo,tc.PRO_DESC,tc.PRO_INVMIN,isnull(pp.CantAsignada,0),tc.PRO_INVMAX,pt.Saldo,ml.Saldo,tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen "
                        +" ORDER BY tc.PRO_CODPROD ";
                
                
                
            }else if(valor == 5){
                
                query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +" AND me.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                        +" AND me.CodigoMatPrima  = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_si_Celda,   "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  "
                        +" - "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS "
                        +" )  AS MediaCantidad,     "
                        +" tc.PRO_INVMIN, "
                        +" tc.PRO_INVMAX,  "
                        +"(ml.Saldo) as Stock, "
                        //+" (pro.PSL_Saldo) as Stock1 "
                        +" (0) as Stock1 "
                        +" ,tc.PRO_UMPRINCIPAL, "
                        +"  (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as LeadTime,      "
                        +" (select sum(tce.MVD_Cant-tce.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tce "
                        +" where tce.MVD_CodProd = tc.PRO_CODPROD  and tce.MVD_CodSistema=7 and tce.MVD_CodClase =1 AND tce.MVD_TipoDoc = 1 and tce.MVD_FechaEntrega < '" + fechaM + "' "
                        +" and tce.MVD_Cant-tce.MVD_CantAsignada > 0) as ordenVencida, "
                        +" (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) as ordenFutura, "
                        //+" isnull(sp.cantidad,0), "
                        +" isnull(pp.CantAsignada,0), "
                        +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as obser,pt.Saldo,ml9.Saldo as Stock9,"
                        
                        + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos, "
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=tc.PRO_CODPROD),0)as integer)as Cantidad "
                        
                        
                        +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                        +" left outer join SiaSchaffner.dbo.STO_MOVDET tcc "
                        +" on(tcc.MVD_CodProd = tc.PRO_CODPROD  and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 and tcc.MVD_FechaEntrega >= '" + fechaM + "' "
                        +" and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = tc.PRO_CODPROD) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                            /*+" left outer join SiaSchaffner.dbo.STO_PRODSAL pro "
                            +" on(pro.PSL_CodProd =tc.PRO_CODPROD  and pro.PSL_CodBodega in(21)) "*/
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +"  left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +"  on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                        +" left outer join SiaSchaffner.dbo.VW_sap sap "
                        +" on(tc.PRO_CODPROD = sap.MVD_CodProd) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd = tc.PRO_CODPROD) "
                        
                       /* +"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                        
                        +" WHERE sap.MVE_FolioFisico =  "+rango+" "
                        //+" and (tc.PRO_CODTIPO in (1,5,8) or tc.PRO_CODPROD like '5041%') "
                        +" group by pa.asociado,tc.PRO_CODPROD,ml9.Saldo,tc.PRO_DESC,pa.Comprador,tc.PRO_INVMIN,isnull(pp.CantAsignada,0),tc.PRO_INVMAX,pt.Saldo,ml.Saldo,tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen "
                        +" ORDER BY tc.PRO_CODPROD ";
                
            }else if(valor == 6){
                query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +" AND me.CodigoMatPrima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        //+" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%'  and tp.Familia = 'CELDA'"
                        +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                        +" AND me.CodigoMatPrima  = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) AS NecesidadPorEquipo_si_Celda,   "
                        +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ) "
                        +" - "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" )  AS MediaCantidad,     "
                        +" tc.PRO_INVMIN, "
                        +" tc.PRO_INVMAX,  "
                        +" (ml.Saldo) as Stock, "
                        +" (0) as Stock1 "
                        +" ,tc.PRO_UMPRINCIPAL, "
                        +"  (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as LeadTime,      "
                        +" (select sum(tce.MVD_Cant-tce.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tce "
                        +" where tce.MVD_CodProd = tc.PRO_CODPROD  and tce.MVD_CodSistema=7 and tce.MVD_CodClase =1 AND tce.MVD_TipoDoc = 1 and tce.MVD_FechaEntrega < '" + fechaM + "' "
                        +" and tce.MVD_Cant-tce.MVD_CantAsignada > 0) as ordenVencida, "
                        +"  (sum(tcc.MVD_Cant-tcc.MVD_CantAsignada)) as ordenFutura, "
                        +" isnull(pp.CantAsignada,0), "
                        +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) as obser,pt.Saldo as BPT,ml9.Saldo as Stock9,"
                        
                        + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos ,"
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=tc.PRO_CODPROD),0)as integer)as Cantidad "
                        
                        +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                        +" left outer join SiaSchaffner.dbo.STO_MOVDET tcc "
                        +" on(tcc.MVD_CodProd = tc.PRO_CODPROD  and tcc.MVD_CodSistema=7 and tcc.MVD_CodClase =1 AND tcc.MVD_TipoDoc = 1 and tcc.MVD_FechaEntrega >= '" + fechaM + "' "
                        +" and (tcc.MVD_Cant-tcc.MVD_CantAsignada) > 0) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = tc.PRO_CODPROD) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +" on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd = tc.PRO_CODPROD)    "
                        
                       /* +"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                        
                        +" WHERE tc.PRO_CODPROD = '"+ rango + "' "
                        //+" and (tc.PRO_CODTIPO in (1,5,8) or tc.PRO_CODPROD like '5041%') "
                        +" group by tc.PRO_CODPROD,pa.asociado,pa.Comprador,ml9.Saldo,tc.PRO_DESC,tc.PRO_INVMIN,tc.PRO_INVMAX,pt.Saldo,ml.Saldo,isnull(pp.CantAsignada,0),tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen "
                        +" ORDER BY tc.PRO_CODPROD ";
            }else{
                
                
                query1 = "SELECT  tc.PRO_CODPROD, tc.PRO_DESC, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = me.CodigoSemiElaborado and a.familia <> 'CELDA'"
                        +" AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) AS NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and tp.Familia = 'CELDA' "
                        +" AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) AS NecesidadPorEquipo_si_Celda,  "
                        
                        //+"  (select sum(cantidad)/12 from MAC.dbo.consumosPorMaterial where codigo COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) as MediaCantidad, "
                        +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +" WHERE ma1.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD ) "
                        +" - "
                        +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) "
                        +" )  AS MediaCantidad,     "
                        +" tc.PRO_INVMIN as StockMinimo, "
                        +" tc.PRO_INVMAX as StockMaximo, "
                        +" (ml.Saldo) as Stock, "
                        //+" (pro.PSL_Saldo) as Stock1, "
                        +" (0) as Stock1, "
                        +" tc.PRO_UMPRINCIPAL as Unimed, "
                        +" (select pan.LeadTime from MAC.dbo.ProductoAnexo pan where pan.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) as LeadTime, "
                        +" (select sum(mov.MVD_Cant-mov.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET mov "
                        +" where  mov.MVD_CodProd  = tc.PRO_CODPROD  and mov.MVD_CodSistema=7 and mov.MVD_CodClase =1 AND mov.MVD_TipoDoc = 1 and mov.MVD_FechaEntrega < '" + fechaM + "' "
                        +" and (mov.MVD_Cant-mov.MVD_CantAsignada) > 0) as ordenVencida, "
                        +" (select sum(mov.MVD_Cant-mov.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET mov "
                        +" where  mov.MVD_CodProd = tc.PRO_CODPROD  and mov.MVD_CodSistema=7 and mov.MVD_CodClase =1 AND mov.MVD_TipoDoc = 1 and mov.MVD_FechaEntrega >= '" + fechaM + "' "
                        +" and (mov.MVD_Cant-mov.MVD_CantAsignada) > 0) as ordenFutura, "
                        //+" isnull(sp.cantidad,0),  "
                        +" isnull(pp.CantAsignada,0), "
                        +" pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,isnull(pa.Comprador,'null'),pa.asociado, "
                        +" (select count(Codigo) from ENSchaffner.dbo.SC_bitacora_materiales  where Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) as obser,pt.Saldo,ml9.Saldo as Stock9,"
                        
                        
                        + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos, "
                            + " cast(isnull((select SUM(vw.MVD_Cant)-SUM(vw.MVD_CAntAsignada) from SiaSchaffner.dbo.VW_sap vw where vw.MVD_Codprod=tc.PRO_CODPROD),0)as integer )as Cantidad "
                        
                        +" FROM SiaSchaffner.dbo.STO_PRODUCTO tc "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                        +" on(ml.PSL_CodProd = tc.PRO_CODPROD ) "
                        +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                        +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                        /*+" left outer join SiaSchaffner.dbo.STO_PRODSAL pro "
                        +" on(pro.PSL_CodProd =tc.PRO_CODPROD  and pro.PSL_CodBodega in(21)) "*/
                        +" left outer join MAC.dbo.materialesPorFamilia mp "
                        +" on(tc.PRO_CODPROD = mp.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                        +" left outer join MAC.dbo.ProductoAnexo pa "
                        +" on(pa.Codigo = mp.Codigo) "
                        +" left outer join MAC2BETA.dbo.VW_pedidos_planta pp"
                        +" on(pp.MVD_CodProd = tc.PRO_CODPROD)"
                        +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados pt "
                        +" on(pt.PSL_CodProd = tc.PRO_CODPROD) "
                        
                       /* +"  left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf "
                        +"  on(msf.vc_codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "*/
                        
                        +" WHERE mp.estado =  "+ estado +"  and mp.familia = '" + rango + "' and tc.PRO_DESC is not null and mp.Codigo is not null "
                        +" ORDER BY tc.PRO_CODPROD ";
            }
            rs = st.executeQuery(query1);
            while(rs.next()){
                String caracter = "";
                Vector compras = new Vector();
                compras.addElement(rs.getString(1));
                compras.addElement(rs.getString(2).replace('"',' '));
                //compras.addElement(new Float(rs.getFloat(3) + rs.getFloat(4) + rs.getFloat(24) + rs.getFloat(25) + rs.getFloat(26)));
                compras.addElement(new Float(rs.getFloat(3) + rs.getFloat(4) + rs.getFloat(24)));
                if(rs.getFloat(5) == 0){
                    compras.addElement(new Float(rs.getFloat(5)));
                }else{
                    compras.addElement(new Float(rs.getFloat(5)/12));
                }
                compras.addElement(new Float(rs.getFloat(6)));
                compras.addElement(new Float(rs.getFloat(7)));
                compras.addElement(new Float(rs.getFloat(8) + rs.getFloat(9)));
                compras.addElement(rs.getString(10));
                compras.addElement(new Integer(rs.getInt(11)));
                compras.addElement(new Float(rs.getFloat(12)));
                compras.addElement(new Float(rs.getFloat(13)));
                compras.addElement(new Float(rs.getFloat(14)));
                compras.addElement(new Byte(rs.getByte(15)));
                compras.addElement(new Byte(rs.getByte(16)));
                compras.addElement(new Byte(rs.getByte(17)));
                compras.addElement(new Byte(rs.getByte(18)));
                compras.addElement(new String(rs.getString(19)));
                compras.addElement(rs.getString(20));
                compras.addElement(new Integer(rs.getInt(21)));
                compras.addElement(new Float(rs.getFloat(22)));
                compras.addElement(new Float(rs.getFloat(22)+rs.getFloat(8)));
                compras.addElement(new Float(rs.getFloat(23)));                
                compras.addElement(new Integer(rs.getInt(25)));//numero 22 en pagina jsp
                comprasRequeridas.addElement(compras);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR retornarEstadoMateriales(): " + e.getMessage(),0);
        }
        return comprasRequeridas;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public byte actualizarComprasRequeridas(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int valor){
        byte veri = (byte)0;
        int count = 0;
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
       /*String query1 = "SELECT COUNT(Codigo) FROM MAC.dbo.ComprasRequeridas WHERE Codigo = '" + codigo + "'";
       rs = st.executeQuery(query1);
       while(rs.next()){
           count = rs.getInt(1);
       }*/
            if(valor == 1){
                String query = "UPDATE MAC.dbo.ComprasRequeridas SET cantRequerida = "+ cantRequerida +" ,verificar = 1,contador = '" + fechaM + "',fechaEntrega = '" + fechaEntrega + "' WHERE Codigo = '" + codigo + "' AND verificar = 0" ;
                st.execute(query);
                
            }if(valor == 3){
                String query2 = "insert into MAC.dbo.ComprasRequeridas(Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',1,1)";
                st.execute(query2);
            }
            veri = 1;
            //rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: actualizarComprasRequeridas() " + e.getMessage(),0);
        }
        return veri;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public byte actualizarComprador(String material,String comprador,String fecha){
        byte veri = (byte)0;
        try{
            conMAC = myConexion.getMACConnector();
            //conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            /*String query = "UPDATE MAC.dbo.ComprasRequeridas SET comprador = '"+ comprador +"'  WHERE Codigo = '" + material + "'  AND fechaEntrega = '" + fecha + "' " ;
            st.execute(query);*/
            String query1 = "UPDATE MAC.dbo.ProductoAnexo SET Comprador = '"+ comprador +"'  WHERE Codigo = '" + material + "' " ;
            st.execute(query1);
            //conMAC.commit();
            //conMAC.setAutoCommit(true);
            veri = (byte)1;
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , "actualizarComprador() " + e.getMessage(),0);
        }
        return veri;
    }
    
    public byte actualizarCompradorReclamo(String material,String comprador,long numero){
        byte veri = (byte)0;
        try{
            conMAC = myConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            String query = "UPDATE MAC2BETA.dbo.TBL_PRO_RECLAMOS SET vc_comprador = '"+ comprador +"'  WHERE vc_codigo = '" + material + "' and nb_numero =  "+ numero ;
            st.execute(query);
            String query1 = "UPDATE MAC.dbo.ProductoAnexo SET Comprador = '"+ comprador +"'  WHERE Codigo = '" + material + "' " ;
            st.execute(query1);
            conMAC.commit();
            conMAC.setAutoCommit(true);
            veri = (byte)1;
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , "actualizarComprador() " + e.getMessage(),0);
        }
        return veri;
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void agruparPedidosSap(String material,String fecha,int numSap){
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = "UPDATE MAC.dbo.ComprasRequeridas SET numeroSap = " + numSap + " WHERE Codigo = '" + material + "' AND fechaEntrega = '" + fecha + "' AND verificar <> 3 AND numeroSap IS NULL";
            st.execute(query);
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " agruparPedidosSap(): " + e.getMessage(),0);
        }
    }
    public int numeroSap(){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "SELECT max(numeroSap) FROM MAC.dbo.ComprasRequeridas";
            rs = st.executeQuery(query1);
            while(rs.next()){
                count = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR numeroSap(): " + e.getMessage(),0);
        }
        return count;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public void descartarCompra(String material,int id){
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = "UPDATE MAC.dbo.ComprasRequeridas SET verificar = 3  WHERE Codigo = '" + material + "' AND id = " + id + "";
            st.execute(query);
            st.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , "ERROR descartarCompra(): " + e.getMessage(),0);
        }
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public Vector SapPorUsuario(String comprador){
        Vector sap = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "SELECT DISTINCT(numeroSap) FROM MAC.dbo.ComprasRequeridas WHERE comprador = '" + comprador + "' AND numeroSap is not null";
            rs = st.executeQuery(query1);
            while(rs.next()){
                sap.addElement(new Integer(rs.getInt(1)));
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR numeroSap(): " + e.getMessage(),0);
        }
        return sap;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public int borrarCompra(String material,String fecha){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "DELETE FROM MAC.dbo.ComprasRequeridas WHERE Codigo = '" + material + "' AND fechaEntrega = '" + fecha + "' ";
            st.execute(query1);
            count = 1;
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR borrarCompra(): " + e.getMessage(),0);
        }
        return count;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int actualizarComprasRequeridas(String material,float cantidad ,String fecha,String fecha1){
        int count = 3;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query1 = "UPDATE MAC.dbo.ComprasRequeridas2 SET cantRequerida = " + cantidad + " WHERE Codigo = '" + material + "' AND fechaEntrega = '" + fecha + "'";
            String query1 = "UPDATE MAC.dbo.ComprasRequeridas SET cantRequerida = " + cantidad + ", fechaEntrega = '" + fecha1 + "'  WHERE Codigo = '" + material + "' AND fechaEntrega = '" + fecha + "' ";
            st.execute(query1);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR comprasRequeridas(): " + e.getMessage() , 0);
        }
        return count;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public void setIncrementar(int incrementar){
        this.incrementar = incrementar;
    }
    public int getIncrementar(){
        return incrementar;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public byte insertarPedidoMateriales(String codigo ,float canti,long numero,String usuario,String um,byte size,byte tipo,String usu){
        int verifi = 0;
        byte veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            long numDoc = this.traerNumDoc(numero);
            // String query4 =  "INSERT INTO MAC.dbo.detallePedido (codigo,descripcion,cantidadPedida,um,usuario,fecha,numero,status,descripcionStatus,area,stockPlanta,prioridad) values ('"+ codigo +"','"+ descri +"',"+ canti +",'"+ um +"','"+ usuario +"','"+ fechaM +"',"+ numero +","+ status +",'"+ descriEstado +"','"+ area +"'," + stockPlanta + ",'normal')";
            String query4 =  " insert into SiaSchaffner.dbo.STO_MOVDET (MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_CodProd,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_Zona,MVD_NumeroSerie,MVD_NumeroLote,MVD_ComentarioLinea,MVD_Estado,MVD_Costeo,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion,FecCreacion,HoraCreacion,UsModif,FecModif,HoraModif,mvd_expansion1,PMP) values(" + 8 + "," + 1 + "," + 8 + ","
                    + numDoc + "," + ++size + ",'" + codigo + "','" + tipo + "'," + canti + ",0,'" + um + "',1," +canti + ",0,0,0,'" + fechaM + "','N',0,0,'','V',0,0,0,0,0,1,'" + usuario + "','" + fechaM + "','000000','" + usu + "','" + fechaM + "','000000',0,'1')";
            
            st.execute(query4);
            String update1 = "";
            update1 = " update MAC.dbo.totalEquiposPorPedido set usuIngAgregar = '" + usu + "'  where numero = '" + numero + "'";
            st.execute(update1);
            veri = 1;
            conMAC.commit();
            conMAC.setAutoCommit(true);
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " insertarPedidoMateriales() "+e.getMessage(),0);
        }
        return veri;
    }
    //-----------------------------------------------------------------------------------------------------------------------------------------------------------
    public Vector centroDeCosto(){
        String query4 = "";
        Vector centroCosto = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            query4 =  " select TAB_Codigo,TAB_Desc from SiaSchaffner.dbo.SYS_TABLAS where TAB_CodTabla = 10 order by TAB_Desc ";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoProducto(rs.getString(1));
                estructura.setDescripcion(rs.getString(2));
                centroCosto.addElement(estructura);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " centroDeCosto() "+e.getMessage(),0);
        }
        return centroCosto;
    }
    public long traerNumDoc(long numero){
        
        long traerNumDoc =0l;
        
        try{
         /*conENS = mConexion.getENSConnector();
         st = conENS.createStatement();*/
            String query4 = " select distinct(mov.MVD_NumeroDoc) "
                    +"     from SiaSchaffner.dbo.STO_MOVDET mov ,SiaSchaffner.dbo.STO_MOVENC enc "
                    +"    where mov.MVD_CodSistema=8 and mov.MVD_CodClase =1 AND mov.MVD_TipoDoc = 8 and "
                    +"    enc.MVE_CodSistema=8 and "
                    +"    enc.MVE_CodClase =1 AND "
                    +"    enc.MVE_TipoDoc = 8 and "
                    +"    mov.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                    +"    and enc.MVE_FolioFisico =  " + numero + " ";
            
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                traerNumDoc = rs.getLong(1);
            }
            rs.close();
         /*st.close();
         conENS.close(); */
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " traerNumDoc() "+e.getMessage(),0);
        }
        return traerNumDoc;
    }
    private boolean insertarDetalle(Vector necesidades,String usu,long numeroDoc,int centroCosto, String fechaNormal, ConexionSQL cn) {
        boolean estadoDET = false;
        int increment = 1;
        try{
            if(necesidades.size() > 0){
                for(int i=0;i<necesidades.size();i++){
                    if(((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima() != null && !((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima().equals("") &&
                            ((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida() != null && !((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida().equals("") &&
                            ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() > 0 && ((EstructuraPorEquipo)necesidades.elementAt(i)).getTipoProducto() > 0 &&
                            numeroDoc > 0 && centroCosto > 0 && usu != null && !usu.equals("")
                            && fechaM != null && !fechaM.equals("")){
                        String vvv  = "MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_CodProd,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,";
                        vvv += "MVD_FactorUMAlternativa,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_Zona,MVD_NumeroSerie,";
                        vvv += "MVD_NumeroLote,MVD_ComentarioLinea,MVD_Estado,MVD_Costeo,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,";
                        vvv += "MVD_LineaOrigen,UsCreacion,FecCreacion,HoraCreacion,UsModif,FecModif,HoraModif,mvd_expansion1,PMP,MVD_CentroCosto";
                        // String query7 =  " insert into SiaSchaffner.dbo.STO_MOVDET (MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_CodProd,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_Zona,MVD_NumeroSerie,MVD_NumeroLote,MVD_ComentarioLinea,MVD_Estado,MVD_Costeo,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion,FecCreacion,HoraCreacion,UsModif,FecModif,HoraModif,mvd_expansion1,PMP,MVD_CentroCosto) values(" + 8 + "," + 1 + "," + 8 + ","
                        //        + numeroDoc + "," + increment + ",'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima() + "','" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getTipoProducto() + "'," + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + ",0,'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida() + "',1," + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + ",0,0,0,'" + fechaNormal + "','N',0,0,'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getObservacion() + "','V',0,0,0,0,0,0,'" + usu + "','" + fechaNormal + "','000000','" + usu + "','" + fechaNormal + "','000000',0,'0','" + centroCosto + "')";
                        String ccc = 8 + "�";
                        ccc += 1 + "�";
                        ccc += 8 + "�";
                        ccc += numeroDoc + "�";
                        ccc += increment + "�";
                        ccc += ((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima() + "�";
                        ccc += ((EstructuraPorEquipo)necesidades.elementAt(i)).getTipoProducto() + "�";
                        ccc += ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + "�";
                        ccc += 0 + "�";
                        ccc += ((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida() + "�";
                        ccc += 1 + "�";
                        ccc += ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + "�";
                        ccc += 0 + "�";
                        ccc += 0 + "�";
                        ccc += 0 + "�";
                        ccc += fechaNormal + "�";
                        ccc += "N" + "�";
                        ccc += 0 + "�";
                        ccc += 0 + "�";
                        ccc += ((EstructuraPorEquipo)necesidades.elementAt(i)).getObservacion() + "�";
                        ccc += "V" +"�";
                        ccc += 0 +"�";
                        ccc += 0 +"�";
                        ccc += 0 +"�";
                        ccc += 0 +"�";
                        ccc += 0 +"�";
                        ccc += 0 +"�";
                        ccc += usu + "�";
                        ccc += fechaNormal + "�";
                        ccc += "000000" + "�";
                        ccc += usu + "�";
                        ccc += fechaNormal + "�";
                        ccc += "000000" + "�";
                        ccc += 0 + "�";
                        ccc += 0 + "�";
                        ccc += centroCosto;
                        estadoDET = cn.InsertReg("SiaSchaffner.dbo.STO_MOVDET", vvv, ccc);
                        increment++;
                        if(!estadoDET){
                            break;
                            
                        }
                    }else{
                        estadoDET = false;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            estadoDET = false;
            PrmApp.addError(ex, true);
        }
        return estadoDET;
    }
    /* private boolean insertarDetalle(Vector necesidades,String usu,float numeroDoc,int centroCosto, String fechaNormal) throws SQLException{
        boolean estadoDET = false;
        int estadoDET1 = 0;
        int increment = 1;
     
        if(necesidades.size() > 0){
            for(int i=0;i<necesidades.size();i++){
                 if(((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima() != null && !((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima().equals("") &&
                        ((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida() != null && !((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida().equals("") &&
                        ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() > 0 && ((EstructuraPorEquipo)necesidades.elementAt(i)).getTipoProducto() > 0 &&
                        numeroDoc > 0 && centroCosto > 0 && usu != null && !usu.equals("")
                        && fechaM != null && !fechaM.equals("")){
                String query7 =  " insert into SiaSchaffner.dbo.STO_MOVDET (MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_CodProd,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_Zona,MVD_NumeroSerie,MVD_NumeroLote,MVD_ComentarioLinea,MVD_Estado,MVD_Costeo,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion,FecCreacion,HoraCreacion,UsModif,FecModif,HoraModif,mvd_expansion1,PMP,MVD_CentroCosto) values(" + 8 + "," + 1 + "," + 8 + ","
                        + numeroDoc + "," + increment + ",'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima() + "','" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getTipoProducto() + "'," + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + ",0,'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida() + "',1," + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + ",0,0,0,'" + fechaNormal + "','N',0,0,'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getObservacion() + "','V',0,0,0,0,0,0,'" + usu + "','" + fechaNormal + "','000000','" + usu + "','" + fechaNormal + "','000000',0,'0','" + centroCosto + "')";
                estadoDET1 = st3.executeUpdate(query7);
                increment++;
                if(estadoDET1 > 0){
                    estadoDET = true;
     
                }else{
                    estadoDET = false;
                    break;
     
                }
     
            }else{
                    estadoDET = false;
                    break;
               }
            }
        }else{
            estadoDET = false;
        }
     
        return estadoDET;
    }*/
    private boolean insertarPedido(Vector equipos,long folioFisico,byte grupo,String descriEstado,byte estado,String usu,String process,String prioridad,String supervisor,ConexionSQL cn){
        boolean estadoP = false;
        int increment = 1;
        try{
            
            if(equipos.size() > 0){
                for(int i=0;i<equipos.size();i++){
                    if(folioFisico > 0 && ((Equipo)equipos.elementAt(i)).getNp() > 0 && process != null && !process.equals("") && usu != null && !usu.equals("") && supervisor != null && !supervisor.equals("")){
                        String vvv = "np,linea,numero,grupo,status,descriEstado,usuario,fecha,area,prioridad,usuAprobacion";
                        String ccc = ((Equipo)equipos.elementAt(i)).getNp() +"�";
                        ccc += ((Equipo)equipos.elementAt(i)).getLinea() + "�";
                        ccc += folioFisico + "�";
                        ccc += grupo +"�";
                        ccc += estado + "�";
                        ccc += descriEstado + "�";
                        ccc += usu + "�";
                        ccc += PrmApp.getFechaActual1() +" "+PrmApp.getHoraActual()  + "�";
                        ccc += process + "�";
                        ccc += prioridad + "�";
                        ccc += supervisor;
                        estadoP = cn.InsertReg("MAC.dbo.totalEquiposPorPedido", vvv, ccc);
                        increment++;
                        if(!estadoP){
                            break;
                        }
                    }else{
                        estadoP = false;
                        break;
                    }
                }
            }
        } catch (Exception ex) {
            estadoP = false;
            PrmApp.addError(ex, true);
        }
        return estadoP;
    }
   /*  private boolean insertarPedido(Vector equipos,float folioFisico,byte grupo,String descriEstado,byte estado,String usu,String process,String prioridad,String supervisor,ConexionSQL cn) throws SQLException{
        boolean estadoP = false;
        int estadoP1 = 0;
        int increment = 1;
        // try{
        conMAC2 = myConexion.getMACConnector();
        conMAC2.setAutoCommit(false);
        st2 = conMAC2.createStatement();
        if(equipos.size() > 0){
            for(int i=0;i<equipos.size();i++){
                if(folioFisico > 0 && ((Equipo)equipos.elementAt(i)).getNp() > 0 && process != null && !process.equals("") && usu != null && !usu.equals("") && supervisor != null && !supervisor.equals("")){
                    String query4 =  " INSERT INTO MAC.dbo.totalEquiposPorPedido (np,linea,numero,grupo,status,descriEstado,usuario,fecha,area,prioridad,usuAprobacion) values "
                            +" ("+ ((Equipo)equipos.elementAt(i)).getNp() +","+ ((Equipo)equipos.elementAt(i)).getLinea() +","+  folioFisico  +","+ grupo +","+ estado +",'" + descriEstado + "','" + usu + "',getdate(),'" + process + "','" + prioridad + "','" + supervisor + "')";
                    estadoP1 = st2.executeUpdate(query4);
                    increment++;
                    if(estadoP1 > 0){
                        estadoP = true;
    
                    }else{
                        estadoP = false;
                        break;
    
                    }
                }else{
                    estadoP = false;
                    break;
    
                }
            }
        }else{
            estadoP = false;
        }
    
        return estadoP;
    }*/
    private boolean insertarEncabezado(Vector necesidades,Vector equipos,String usu,String process,
            byte grupo,String prioridad,int centroCosto,String supervisor,long numeroDoc,long folioFisico,ConexionSQL cn, String ordenMantencion, String tipoMantencion){
        boolean estadoP = false;
        boolean estadoENC1 = false;
        boolean estadoDET = false;
        boolean estadoE = false;
        SimpleDateFormat fo = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat forma = new SimpleDateFormat("yyyyMM");
        String fechaNormal = fo.format(new java.util.Date());
        String periLibro = forma.format(new java.util.Date());
        
        try{
            String ccc= "MVE_CodSistema, MVE_CodClase, MVE_TipoDoc, MVE_NumeroDoc, MVE_FechaDoc, ";
            ccc += " MVE_PeriodoLibro, MVE_FolioFisico, MVE_Zona, MVE_Estado, MVE_FechaDelEstado, MVE_EstadoImpresion, MVE_CentroCosto, UsCreacion, ";
            ccc += " mve_analisisadic2, FecCreacion, HoraCreacion, UsModif, FecModif, HoraModif, MVE_Moneda, MVE_Paridad, MVE_TipoCambioBimoneda,";
            ccc += " MVE_SubTotal, MVE_Afecto, MVE_Exento, MVE_Iva, MVE_Impuesto1, MVE_Impuesto2, MVE_Impuesto3, MVE_Impuesto4, MVE_Impuesto5,";
            ccc += " MVE_Impuesto6, MVE_Impuesto7, MVE_Impuesto8, MVE_Impuesto9, MVE_Impuesto10, MVE_Total, MVE_SubTotalMA, MVE_AfectoMA, MVE_ExentoMA,";
            ccc += " MVE_IvaMA, MVE_Impuesto1MA, MVE_Impuesto2MA, MVE_Impuesto3MA, MVE_Impuesto4MA, MVE_Impuesto5MA, MVE_Impuesto6MA, MVE_Impuesto7MA,";
            ccc += " MVE_Impuesto8MA, MVE_Impuesto9MA, MVE_Impuesto10MA, MVE_TotalMA, MVE_Aprobacion";
            String vvv = 8  + "�";
            vvv += 1  + "�";
            vvv += 8  + "�";
            vvv += numeroDoc   + "�";
            vvv += fechaNormal + "�";
            vvv += periLibro + "�";
            vvv += folioFisico + "�";
            vvv += "N" +"�";
            vvv += "V" +"�";
            vvv += fechaNormal + "�";
            vvv += "N" + "�";
            vvv += centroCosto + "�";
            vvv += usu + "�";
            vvv += 1 +"�";
            vvv += fechaNormal + "�";
            vvv += "000000" + "�";
            vvv += usu + "�";
            vvv += fechaNormal + "�";
            vvv += "000000" + "�";
            vvv += 1 +"�";
            vvv += 1 +"�";
            vvv += 1 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += 0 +"�";
            vvv += "N";
            
            if(numeroDoc > 0 && centroCosto > 0  && usu != null && !usu.trim().equals("") && folioFisico > 0
                    && fechaNormal != null && !fechaNormal.equals("") && periLibro != null && !periLibro.equals("")){
                //estadoENC = st3.executeUpdate(query5);
                estadoENC1 = cn.InsertReg("SiaSchaffner.dbo.STO_MOVENC", ccc, vvv);
            }
            if(estadoENC1){
                estadoDET =  this.insertarDetalle(necesidades,usu,numeroDoc,centroCosto,fechaNormal,cn);
            }
            
            
            
            if(estadoENC1 && estadoDET){
                estadoP = this.insertarPedido(equipos,folioFisico,grupo,"Aprobado",(byte)2,usu,process,"normal",supervisor,cn);
                
            }
            if(estadoENC1 && estadoDET && estadoP){
                if(centroCosto == 1101){
                    if(!ordenMantencion.equals("")){
                        estadoE = this.ordenMantencionSap(Long.parseLong(ordenMantencion),numeroDoc,tipoMantencion,cn);
                    }
                }else{
                    estadoE = true;
                }
            }
            
        } catch (Exception ex) {
            estadoP = false;
            PrmApp.addError(ex, true);
        }
        
        return estadoENC1 && estadoDET && estadoP && estadoE;
    }
    private boolean ordenMantencionSap(long ordenMantencion, long numSap , String tipoMantencion,ConexionSQL cn) throws SQLException{
        boolean estadoP = false;
        try{
            //String query6 =  "insert into MAC.dbo.PRO_gastos_mantencion(ordenMantencion,numSap,tipoMantencion,pedido) values(" + ordenMantencion + "," + numSap + ",'"+tipoMantencion+"','P')";
            //estadoP1 = st2.executeUpdate(query6);
            String vvv  = "ordenMantencion,numPedido,tipoMantencion,pedido";
            String ccc  = ordenMantencion+"�";
            ccc += numSap+"�";
            ccc += tipoMantencion+"�";
            ccc += "P";
            estadoP = cn.InsertReg("MAC.dbo.PRO_gastos_mantencion", vvv, ccc);
        }catch(Exception e){
            PrmApp.addError(e, true);
        }
        return estadoP;
    }
    /* private boolean insertarEncabezado(Vector necesidades,Vector equipos,String usu,String process,
            byte grupo,String prioridad,int centroCosto,String supervisor,float numeroDoc,float folioFisico,ConexionSQL cn) throws SQLException{
        boolean estadoP = false;
        int estadoENC = 0;
        boolean estadoENC1 = false;
        boolean estadoDET = false;
        boolean estadoGeneral = false;
        SimpleDateFormat fo = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat forma = new SimpleDateFormat("yyyyMM");
        String fechaNormal = fo.format(new java.util.Date());
        String periLibro = forma.format(new java.util.Date());
     
        //   try{
        conMAC3 = mConexion.getMACConnector();
        conMAC3.setAutoCommit(false);
        st3 = conMAC3.createStatement();
        String query5 =  " insert into SiaSchaffner.dbo.STO_MOVENC (MVE_CodSistema, MVE_CodClase, MVE_TipoDoc, MVE_NumeroDoc, MVE_FechaDoc, ";
        query5 += " MVE_PeriodoLibro, MVE_FolioFisico, MVE_Zona, MVE_Estado, MVE_FechaDelEstado, MVE_EstadoImpresion, MVE_CentroCosto, UsCreacion, ";
        query5 += " mve_analisisadic2, FecCreacion, HoraCreacion, UsModif, FecModif, HoraModif, MVE_Moneda, MVE_Paridad, MVE_TipoCambioBimoneda,";
        query5 += " MVE_SubTotal, MVE_Afecto, MVE_Exento, MVE_Iva, MVE_Impuesto1, MVE_Impuesto2, MVE_Impuesto3, MVE_Impuesto4, MVE_Impuesto5,";
        query5 += " MVE_Impuesto6, MVE_Impuesto7, MVE_Impuesto8, MVE_Impuesto9, MVE_Impuesto10, MVE_Total, MVE_SubTotalMA, MVE_AfectoMA, MVE_ExentoMA,";
        query5 += " MVE_IvaMA, MVE_Impuesto1MA, MVE_Impuesto2MA, MVE_Impuesto3MA, MVE_Impuesto4MA, MVE_Impuesto5MA, MVE_Impuesto6MA, MVE_Impuesto7MA,";
        query5 += " MVE_Impuesto8MA, MVE_Impuesto9MA, MVE_Impuesto10MA, MVE_TotalMA, MVE_Aprobacion) values(" + 8 + "," + 1 + "," + 8 + "," ;
        query5 += numeroDoc + ",'" + fechaNormal + "','" + periLibro + "'," + folioFisico + ",'N','V','" + fechaNormal + "','N','" + centroCosto + "','" + usu + "',1,";
        query5 += " '" + fechaNormal + "','000000','" + usu + "','" + fechaNormal + "','000000',1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'N')";
        if(numeroDoc > 0 && centroCosto > 0  && usu != null && !usu.trim().equals("") && folioFisico > 0
           && fechaNormal != null && !fechaNormal.equals("") && periLibro != null && !periLibro.equals("")){
        estadoENC = st3.executeUpdate(query5);
     
        }
        if(estadoENC > 0){
            estadoDET =  this.insertarDetalle(necesidades,usu,numeroDoc,centroCosto,fechaNormal);
            estadoENC1 = true;
        }
     
     
     
        if(estadoENC1 && estadoDET){
            estadoP = this.insertarPedido(equipos,folioFisico,grupo,"Aprobado",(byte)2,usu,process,"normal",supervisor);
     
        }
     
        if(estadoENC1 && estadoDET && estadoP){
            conMAC3.commit();
            conMAC2.commit();
            estadoGeneral = true;
        }else{
            conMAC3.rollback();
            conMAC2.rollback();
            estadoGeneral = false;
        }
     
     
     
        return estadoGeneral;
    }*/
//-----------------------------------------------------------------------------------------------------------------------------
    public long insertarPedidoMateriales(Vector necesidades,Vector equipos,String usu,String process,byte grupo,String prioridad,int centroCosto,String supervisor,String ordenMantencion,String tipoMantencion){
        boolean veri = false;
        long numeroDoc = 0l;
        long folioFisico = 0l;
        ConexionSQL cn = new ConexionSQL();
        String sql = null;
        ResultSet rs = null;
        long num = 0;
        try{
            if (cn.openSQL(ConexionSQL.MS_LOCAL)) {
                prioridad = "normal";
                numeroDoc = this.numeroDocVales1(cn);
                folioFisico = this.folioFisicoVales1(cn);
                //setNumpedido(0l);
                //setNumpedido(folioFisico);
                if(numeroDoc > 0 && folioFisico > 0){
                    veri = this.insertarEncabezado(necesidades,equipos,usu,process,grupo,prioridad,centroCosto,supervisor,numeroDoc,folioFisico,cn,ordenMantencion,tipoMantencion);
                    cn.commitSQL(veri);
                    if(veri){
                        num =  folioFisico;
                    }
                }
            }
        }catch(Exception e){
            PrmApp.addError(e, true);
        }
        return num;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
 /*   public boolean insertarPedidoMateriales(Vector necesidades,Vector equipos,String usu,String process,byte grupo,String prioridad,int centroCosto,String supervisor){
        boolean veri = false;
        float numeroDoc = 0f;
        float folioFisico = 0f;
        try{
            prioridad = "normal";
            numeroDoc = this.numeroDocVales1();
            folioFisico = this.folioFisicoVales1();
            setNumpedido(0f);
            setNumpedido(folioFisico);
            if(numeroDoc > 0 && folioFisico > 0){
            veri = this.insertarEncabezado(necesidades,equipos,usu,process,grupo,prioridad,centroCosto,supervisor,numeroDoc,folioFisico);
            }
        }catch(SQLException e){
            try {
                conMAC3.rollback();
                conMAC2.rollback();
  
            } catch (SQLException ex) {
                //new Loger().logger("MrpBean.class " , " insertarPedidoMateriales (): "+ex.getMessage(),0);
            }
  
            new Loger().logger("MrpBean.class " , " insertarPedidoMateriales (): "+e.getMessage(),0);
        }catch(Exception e){
            try {
                conMAC3.rollback();
                conMAC2.rollback();
  
            } catch (SQLException ex) {
                //new Loger().logger("MrpBean.class " , " insertarPedidoMateriales (): "+ex.getMessage(),0);
            }
  
            new Loger().logger("MrpBean.class " , " insertarPedidoMateriales (): "+e.getMessage(),0);
        }
        finally{
                 try {
                st2.close();
                st3.close();
                conMAC2.close();
                conMAC3.close();
  
            } catch (SQLException ex) {
                 new Loger().logger("MrpBean.class " , " insertarPedidoMateriales (): "+ex.getMessage(),0);
            }
          }
        return veri;
    }*/
    
//-----------------------------------------------------------------------------------------------------------------------------
    /*public byte insertarPedidoMateriales(Vector necesidades,Vector equipos,String usu,String process,byte grupo,String prioridad,int centroCosto,String supervisor){
        byte veri = 0;
        String descriEstado = "";
        byte estado = 0;
        float numeroDoc = 0f;
        float folioFisico = 0f;
        //String usuAprobarVales = "";
        try{*/
            /* if(validarUsuarioALink(usu,"supervisorVales")){
                descriEstado = "Aprobado";
                estado = 2;
            }else{
                descriEstado = "Pedir autorizaci�n"
                estado = 1;
                prioridad = "normal";
            }*/
         /*   descriEstado = "Aprobado";
            estado = 2;
            prioridad = "normal";
          
            conMAC = MacConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            //if(!usuAprobarVale(process).equals("")){
            //    usuAprobarVales = usuAprobarVale(usu,process);
          
            numeroDoc = this.numeroDocVales();
            folioFisico = this.folioFisicoVales();
            setNumpedido(folioFisico);
            int increment = 1;
            for(int i=0;i<necesidades.size();i++){
                if(i == 0){
                    String query5 =  "insert into SiaSchaffner.dbo.STO_MOVENC (MVE_CodSistema,MVE_CodClase,MVE_TipoDoc,MVE_NumeroDoc,MVE_FechaDoc,MVE_PeriodoLibro,MVE_FolioFisico,MVE_Zona,MVE_Estado,MVE_FechaDelEstado,MVE_EstadoImpresion,MVE_CentroCosto,UsCreacion,mve_analisisadic2,FecCreacion,HoraCreacion,UsModif,FecModif,HoraModif,MVE_Moneda,MVE_Paridad,MVE_TipoCambioBimoneda,MVE_SubTotal,MVE_Afecto,MVE_Exento,MVE_Iva,MVE_Impuesto1,MVE_Impuesto2,MVE_Impuesto3,MVE_Impuesto4,MVE_Impuesto5,MVE_Impuesto6,MVE_Impuesto7,MVE_Impuesto8,MVE_Impuesto9,MVE_Impuesto10,MVE_Total,MVE_SubTotalMA,MVE_AfectoMA,MVE_ExentoMA,MVE_IvaMA,MVE_Impuesto1MA,MVE_Impuesto2MA,MVE_Impuesto3MA,MVE_Impuesto4MA,MVE_Impuesto5MA,MVE_Impuesto6MA,MVE_Impuesto7MA,MVE_Impuesto8MA,MVE_Impuesto9MA,MVE_Impuesto10MA,MVE_TotalMA,MVE_Aprobacion) values(" + 8 + "," + 1 + "," + 8 + ","
                            + numeroDoc + ",'" + fechaM + "','" + periodoLibro + "'," + folioFisico + ",'N','V','" + fechaM + "','N','" + centroCosto + "','" + usu + "',1,'" + fechaM + "','000000','" + usu + "','" + fechaM + "','000000',1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'N')";
                    st.execute(query5);
          
                }
                String query7 =  " insert into SiaSchaffner.dbo.STO_MOVDET (MVD_CodSistema,MVD_CodClase,MVD_TipoDoc,MVD_NumeroDoc,MVD_Linea,MVD_CodProd,MVD_TipoProd,MVD_Cant,MVD_CantAsignada,MVD_UMAlternativa,MVD_FactorUMAlternativa,MVD_CantUMAlternativa,MVD_FactorInventario,MVD_PorcDRGlobal,MVD_PorcDRCosteo,MVD_Fecha,MVD_Zona,MVD_NumeroSerie,MVD_NumeroLote,MVD_ComentarioLinea,MVD_Estado,MVD_Costeo,MVD_TipoCambioConversion,MVD_CodClaseOrigen,MVD_TipoDocOrigen,MVD_NumeroDocOrigen,MVD_LineaOrigen,UsCreacion,FecCreacion,HoraCreacion,UsModif,FecModif,HoraModif,mvd_expansion1,PMP) values(" + 8 + "," + 1 + "," + 8 + ","
                        + numeroDoc + "," + increment + ",'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCodigoMatPrima() + "','" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getTipoProducto() + "'," + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + ",0,'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getUnidadMedida() + "',1," + ((EstructuraPorEquipo)necesidades.elementAt(i)).getCantidad() + ",0,0,0,'" + fechaM + "','N',0,0,'" + ((EstructuraPorEquipo)necesidades.elementAt(i)).getObservacion() + "','V',0,0,0,0,0,0,'" + usu + "','" + fechaM + "','000000','" + usu + "','" + fechaM + "','000000',0,'0')";
                st.execute(query7);
                increment++;
            }
            if(this.folioFisicoExist(folioFisico)){
          
                for(int i=0;i<equipos.size();i++){
          
                    String query4 =  " INSERT INTO MAC.dbo.totalEquiposPorPedido (np,linea,numero,grupo,status,descriEstado,usuario,fecha,area,prioridad,usuAprobacion) values "
                            +" ("+ ((Equipo)equipos.elementAt(i)).getNp() +","+ ((Equipo)equipos.elementAt(i)).getLinea() +","+  folioFisico  +","+ grupo +","+ estado +",'" + descriEstado + "','" + usu + "','" + fechaM + "','" + process + "','" + prioridad + "','" + supervisor + "')";
                    st.execute(query4);
          
                }
                if(equipos.size() > 0){
          
                    conMAC.commit();
                    conMAC.setAutoCommit(true);
                    veri = 1;
                }else{
                    this.deleteFolioFisico(folioFisico);
                    this.macRollback();
                    veri = 0;
                }
            }else{
                this.macRollback();
                veri = 0;
            }
          
            this.desconectarMac();
        }catch(Exception e){
            this.macRollback();
            this.desconectarMac();
            new Loger().logger("MrpBean.class " , " insertarPedidoMateriales (): "+e.getMessage(),0);
            veri = 0;
          
        }
        return veri;
    }*/
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
    public void setNumpedido(long numPedido){
        this.numPedido = numPedido;
    }
    public long getNumPedido(){
        return numPedido;
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
    private void macRollback( ) {
        try { conMAC.rollback();  } catch (SQLException e) {
            new Loger().logger("MrpBean.class " , " macRollback (): "+e.getMessage(),0);
        }
    }
    
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public long numeroDocVales1(ConexionSQL cn){
        String sql = null;
        ResultSet rs1 = null;
        long numeroDoc = 0;
        try{
            sql = " select max(MVE_NumeroDoc)+1 "
                    +" from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 " ;
            rs1 = cn.ExecuteQuery(sql);
            while(rs1.next()){
                numeroDoc = rs1.getLong(1);
            }
            
            rs1.close();
            
        }catch(Exception e){
            PrmApp.addError(e, true);
        }
        return numeroDoc;
    }
    public float numeroDocVales(){
        
        float numeroDoc = 0;
        try{
            conMAC = MacConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =" select max(MVE_NumeroDoc)+1 "
                    +" from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 ";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                numeroDoc = rs.getFloat(1);
            }
            
            rs.close();
            MacConexion.cerrarConMAC();
        }catch(Exception e){
            MacConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " numeroDocVales() "+e.getMessage(),0);
        }
        return numeroDoc;
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public String ubicacionBodMaterial(String codigo){
        
        String  ubi = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =" select AAP_Texto from SiaSchaffner.dbo.sto_prodadic where  AAP_CODANALISIS=1 and AAP_codprod = '" + codigo + "'";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                ubi = rs.getString(1);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " ubicacionBodMaterial() "+e.getMessage(),0);
        }
        return ubi;
    }
    //---------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public long folioFisicoVales1(ConexionSQL cn ){
        String sql = null;
        ResultSet rs1 = null;
        long numeroSap = 0;
        long numeroSap1 = 0;
        try{
            sql = " select max(MVE_FolioFisico)+1 "
                    +" from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 ";
            rs1 = cn.ExecuteQuery(sql);
            while(rs1.next()){
                numeroSap = rs1.getLong(1);
            }
            rs1.close();
            
            String query5 =" select max(fol_folfisico) from SiaSchaffner.dbo.sys_Foldoc "
                    +" where fol_codsistema = 8 and fol_codtipo = 8";
            rs1 = cn.ExecuteQuery(query5);
            while(rs1.next()){
                numeroSap1 = rs1.getLong(1);
            }
            rs1.close();
            if(numeroSap1 > numeroSap){
                numeroSap = numeroSap1;
            }
            
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " folioFisicoVales() "+e.getMessage(),0);
        }
        return numeroSap;
    }
    public float folioFisicoVales(){
        
        float numeroSap = 0;
        try{
            conMAC = MacConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =" select max(MVE_FolioFisico)+1 "
                    +" from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 ";
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                numeroSap = rs.getFloat(1);
            }
            
            rs.close();
            MacConexion.cerrarConMAC();
        }catch(Exception e){
            MacConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " folioFisicoVales() "+e.getMessage(),0);
        }
        return numeroSap;
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public boolean folioFisicoExist(float folioFisico){
        
        boolean folio = false;
        try{
            // conENS = mConexion.getENSConnector();
            // st = conENS.createStatement();
            /*String query4 =" select distinct(MVE_FolioFisico) "
                    +" from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and MVE_FolioFisico = " + folioFisico + "";
             */
            
            String query4 = " select distinct(MVD_NumeroDoc) "
                    +" from SiaSchaffner.dbo.STO_MOVENC,SiaSchaffner.dbo.STO_MOVDET "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and MVE_FolioFisico =  " + folioFisico + " "
                    +" and MVD_CodSistema=8 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 8 "
                    +" and MVD_NumeroDoc = MVE_NumeroDoc ";
            
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                folio = true;
            }
            
            rs.close();
            //st.close();
            //conENS.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " folioFisicoExist() "+e.getMessage(),0);
        }
        return folio;
    }
//----------------------------------------------------------------------------------------------------------------------------
    
    public boolean deleteFolioFisico(float folioFisico){
        
        boolean folio = false;
        try{
            // conENS = mConexion.getENSConnector();
            // st = conENS.createStatement();
            /*String query4 =" select distinct(MVE_FolioFisico) "
                    +" from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and MVE_FolioFisico = " + folioFisico + "";
             */
            
            String query4 = " delete from SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and MVE_FolioFisico =   " + folioFisico + " ";
            
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                folio = true;
            }
            
            rs.close();
            //st.close();
            //conENS.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " deleteFolioFisico() "+e.getMessage(),0);
        }
        return folio;
    }
    
//-----------------------------------------------------------------------------------------------------------------------------
    public void cambiarPrioridad(String prioridad){
        String estado = "";
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            
            String query4 =  "update MAC.dbo.totalEquiposPorPedido set prioridad = '" + prioridad.substring(0,prioridad.indexOf("-")) + "' where numero = '" + prioridad.substring(prioridad.indexOf("-")+1) + "'";
            st.execute(query4);
            
            
            
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " cambiarPrioridad() "+e.getMessage(),0);
        }
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public boolean estadoObs(int numero, String codigo){
        
        boolean estado = false;
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =  "SELECT count(codMaterial) from MAC.dbo.EE_bitacora_vales WHERE numPedido = " + numero +" and codMaterial = '" + codigo + "'";
            rs = st.executeQuery(query4);
            while(rs.next()){
                if(rs.getInt(1)>0){
                    estado = true;
                }
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " estadoObs (): "+e.getMessage(),0);
        }
        return estado;
    }
    
//------------------------------------------------------------------------------------------------------------------------------
    public float cantOtrosPedidos(String codigo){
        
        float cant = 0f;
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =  "select sum(cantidadPedida) from MAC.dbo.detallePedido where codigo = '" + codigo + "'  and status < 3 ";
            rs = st.executeQuery(query4);
            while(rs.next()){
                cant = rs.getFloat(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " cantOtrosPedidos() "+e.getMessage(),0);
        }
        return cant;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public void eliminarItem(String eliminar){
        float pedido = Float.parseFloat(eliminar.substring(0,eliminar.indexOf("-")));
        String codigo = eliminar.substring(eliminar.indexOf("-")+1);
     /*System.out.println(pedido);
     System.out.println(codigo);
      */
        
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            //String query4 = " delete from  MAC.dbo.detallePedido where codigo = '" + codigo + "' and numero = " + pedido + "";
            String query4 = " delete from SiaSchaffner.dbo.STO_MOVDET where MVD_CodSistema = 8 and MVD_CodClase= 1 and MVD_TipoDoc = 8 "
                    +" and MVD_NumeroDoc = " + pedido + " and MVD_CodProd = '" + codigo + "' ";
            st.executeQuery(query4);
            mConexion.cerrarConMAC();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " eliminarItem(): "+e.getMessage(),0);
            mConexion.cerrarConMAC();
        }
    }
    
//-----------------------------------------------------------------------------------------------------------------------------
    public byte insertarAFlex(int numPedido,int linea,String codigo ,float canti,String proceso){
        int verifi = 0;
        byte veri = 0;
        String query4 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query =  "SELECT COUNT(numPedido) FROM MAC.dbo.grabarAFlex WHERE numPedido = " + numPedido + " AND linea = '" + linea + "' ";
            rs = st.executeQuery(query);
            while(rs.next()){
                verifi = rs.getInt(1);
            }
            if(verifi == 0){
                query4 =  "INSERT INTO MAC.dbo.grabarAFlex(numPedido,linea,codigo,cantEntregada,proceso) values ("+ numPedido +","+ linea +",'"+ codigo +"',"+ canti +",'" + proceso + "')";
                veri = 1;
            }
            st.execute(query4);
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: insertarAFlex(): "+e.getMessage(),0);
        }
        return veri;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int cantidadGrabarFlex(int numPedido){
        int verifi = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query =  "select COUNT(numPedido) FROM MAC.dbo.grabarAFlex WHERE numPedido = " + numPedido + " ";
            rs = st.executeQuery(query);
            while(rs.next()){
                verifi = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: cantidadGrabarFlex(): "+e.getMessage(),0);
        }
        return verifi;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public byte deleteAFlex(int numPedido){
        int verifi = 0;
        byte veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =  "DELETE MAC.dbo.grabarAFlex WHERE numPedido = " + numPedido + "";
            st.execute(query4);
            veri = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: deleteAFlex(): "+e.getMessage(),0);
        }
        return veri;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
  /*  public int cantidadPedidoMateriales(){
        int verifi = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query =  "SELECT MAX(numero) FROM MAC.dbo.detallePedido";
            rs = st.executeQuery(query);
            while(rs.next()){
                verifi = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " cantidadPedidoMateriales(): "+e.getMessage(),0);
        }
        return verifi;
    }*/
    
//-----------------------------------------------------------------------------------------------------------------------------
    
    
    public Vector usuariosDetallePedido(){
        
        Vector lineasPorProcesos = new Vector();
        String query4 =  "SELECT Distinct(usuario) FROM MAC.dbo.detallePedido";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            while(rs.next()){
                lineasPorProcesos.addElement(rs.getString(1)); // usuario
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: usuariosDetallePedido (): "+e.getMessage(),0);
        }
        return lineasPorProcesos;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public Vector pedido(){
        
        Vector pedidosPorUsuario = new Vector();
        String query4 = "";
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            query4 = " SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,usuAprobacion,fecha " +
                    " FROM MAC.dbo.totalEquiposPorPedido where status between 2 and 4 and fecha >= '"+formateador.format(new java.util.Date())+"' " +
                    " order by prioridad desc ";
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setNumPedido(rs.getLong(1));
                estructura.setUsuario(rs.getString(2));
                estructura.setDescripcionEstado(rs.getString(3));
                estructura.setProceso(rs.getString(4));
                estructura.setPrioridad(rs.getString(5));
                estructura.setUsuAprobar(rs.getString(6));
                estructura.setFechaCreacion(rs.getDate(7));
                pedidosPorUsuario.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " pedido(): "+e.getMessage(),0);
        }
        return pedidosPorUsuario;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    public Vector todosPedidos(){
        
        Vector pedidosPorUsuario = new Vector();
        String query4 = "";
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            query4 = " SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,usuAprobacion,fecha " +
                    " FROM MAC.dbo.totalEquiposPorPedido where status between 2 and 4 order by prioridad desc ";
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setNumPedido(rs.getLong(1));
                estructura.setUsuario(rs.getString(2));
                estructura.setDescripcionEstado(rs.getString(3));
                estructura.setProceso(rs.getString(4));
                estructura.setPrioridad(rs.getString(5));
                estructura.setUsuAprobar(rs.getString(6));
                estructura.setFechaCreacion(rs.getDate(7));
                pedidosPorUsuario.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " pedido(): "+e.getMessage(),0);
        }
        return pedidosPorUsuario;
    }
    //------------------------------------------------------------------------------------------------------------------------------
    public Vector pedidoPorNumero(long numero){
        
        Vector pedidoPorNumero = new Vector();
        String query4 = "";
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            query4 = " SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,usuAprobacion,fecha " +
                    " FROM MAC.dbo.totalEquiposPorPedido where status between 2 and 4 and numero = '"+numero+"' " +
                    " order by prioridad desc ";
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setNumPedido(rs.getLong(1));
                estructura.setUsuario(rs.getString(2));
                estructura.setDescripcionEstado(rs.getString(3));
                estructura.setProceso(rs.getString(4));
                estructura.setPrioridad(rs.getString(5));
                estructura.setUsuAprobar(rs.getString(6));
                estructura.setFechaCreacion(rs.getDate(7));
                pedidoPorNumero.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " pedidoPorNumero(): "+e.getMessage(),0);
        }
        return pedidoPorNumero;
    }
    
    //------------------------------------------------------------------------------------------------------------------------------
    public Vector pedidoPorFechas(String min, String max){
        
        Vector pedidoPorNumero = new Vector();
        String query4 = "";
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            query4 = " SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,usuAprobacion,fecha " +
                    " FROM MAC.dbo.totalEquiposPorPedido " +
                    " where   status between 2 and 4 and  fecha between '"+min+"' and '"+max+"' " +
                    " order by prioridad desc ";
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setNumPedido(rs.getLong(1));
                estructura.setUsuario(rs.getString(2));
                estructura.setDescripcionEstado(rs.getString(3));
                estructura.setProceso(rs.getString(4));
                estructura.setPrioridad(rs.getString(5));
                estructura.setUsuAprobar(rs.getString(6));
                estructura.setFechaCreacion(rs.getDate(7));
                pedidoPorNumero.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " pedidoPorNumero(): "+e.getMessage(),0);
        }
        return pedidoPorNumero;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public Vector pedidoPorUsuario(String usuario, String numero){
        
        Vector pedidosPorUsuario = new Vector();
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =  "SELECT codigo,descripcion,cantidadPedida,um,usuario,numero,cantidadEntregada,status,np,linea FROM MAC.dbo.detallePedido WHERE usuario = '" + usuario + "' AND numero = '" + numero + "' order by np,linea";
            rs = st.executeQuery(query4);
            while(rs.next()){
                Vector pedidos = new Vector();
                pedidos.addElement(rs.getString(1));
                pedidos.addElement(rs.getString(2));
                pedidos.addElement(new Float(rs.getFloat(3)));
                pedidos.addElement(rs.getString(4));
                pedidos.addElement(rs.getString(5));
                pedidos.addElement(new Integer(rs.getInt(6)));
                //pedidos.addElement(rs.getString(7));
                pedidos.addElement(new Float(rs.getFloat(7)));
                pedidos.addElement(new Integer(rs.getInt(8)));
                pedidos.addElement("");
                pedidos.addElement(new Integer(rs.getInt(9)));
                pedidos.addElement(new Integer(rs.getInt(10)));
                pedidosPorUsuario.addElement(pedidos); // usuario
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: pedidoPorUsuario (): "+e.getMessage(),0);
        }
        return pedidosPorUsuario;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int actualizarCantidadRequeridas(int numero ,String codigo,float cantidad,int tipo){
        int count = 0;
        String query1 = "";
        float cant = 0f;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            float numeroDoc = this.traerNumDoc(numero);
            if(tipo == 1){
                //query1 = "UPDATE MAC.dbo.detallePedido SET cantidadPedida = " + cantidad + "  WHERE numero = '" + numero + "' AND codigo = '" + codigo + "'";
                query1 = " update SiaSchaffner.dbo.STO_MOVDET set MVD_Cant = " + cantidad + ",MVD_CantUMAlternativa = " + cantidad + " "
                        +" where MVD_CodSistema=8 and MVD_CodClase =1 AND MVD_TipoDoc = 8 and "
                        +" MVD_NumeroDoc = " + numeroDoc + " and MVD_CodProd = '" + codigo + "'";
                st.execute(query1);
                count = 1;
            }
            /*else if(tipo == 2){
                String query = " select sum(b.PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL b where b.PSL_CodProd = '" + codigo + "' and b.PSL_CodBodega in (11,21) ";
                rs = st.executeQuery(query);
                while(rs.next()){
                    cant = rs.getFloat(1);
                }
                if(cant > cantidad){
                    query1 = "UPDATE MAC.dbo.detallePedido SET cantidadEntregada = " + cantidad + "  WHERE numero = '" + numero + "' AND codigo = '" + codigo + "'";
                    st.execute(query1);
                    count = 1;
                }else{
                    count = -1;
                }
             
            }else if(tipo == 3){
                query1 = "UPDATE MAC.dbo.detallePedido SET stockPlanta = " + cantidad + "  WHERE numero = '" + numero + "' AND codigo = '" + codigo + "'";
                st.execute(query1);
                count = 1;
            }else{}*/
            
            
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " actualizarCantidadRequeridas(): " + e.getMessage(),0);
        }
        return count;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    
    public byte actualizarStockBodega(Vector cantiEntregada){
        byte count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            conMAC.setAutoCommit(false);
            for(int i=0;i<cantiEntregada.size();i++){
                String query1 = " update SiaSchaffner.dbo.STO_PRODSAL set PSL_Saldo = " + ((EstructuraPorEquipo)cantiEntregada.elementAt(i)).getCantidadEntregada() + " where PSL_CodProd = '" + ((EstructuraPorEquipo)cantiEntregada.elementAt(i)).getCodigoMatPrima() + "'";
                st.execute(query1);
                count++;
            }
            conMAC.commit();
            conMAC.setAutoCommit(true);
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " actualizarStockBodega(): " + e.getMessage(),0);
        }
        return count;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public int insertarObservacionDeVales(int numero ,String codigo,String observacion,byte estado,String usuario){
        int count = 0;
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            float numeroDoc = this.traerNumDoc(numero);
            
            // query1 = "INSERT INTO MAC.dbo.EE_bitacora_vales(codMaterial,observacion,usuario,estado,fecha,numPedido) values ('"+ codigo +"','"+ observacion +"','"+ usuario +"'," + estado + ",'" + fechaM + "'," + numero + ")";
            query1 = " update SiaSchaffner.dbo.STO_MOVDET set MVD_ComentarioLinea = '" + observacion + "' "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 8 and "
                    +" MVD_NumeroDoc =  " + numero + " "
                    +" and MVD_CodProd =  '" + codigo +"' ";
            
            st.execute(query1);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " actualizarObservacionRequeridas(): " + e.getMessage(),0);
        }
        return count;
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    public Vector bitacoraVales(int pedido, String codigo){
        
        Vector bitacoraVales = new Vector();
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            float numeroDoc = this.traerNumDoc(pedido);
            /*String query4 =  " select codMaterial,observacion,usuario,estado,fecha,numPedido "
                            +" from MAC.dbo.EE_bitacora_vales where numPedido = " + pedido + " and codMaterial = '" + codigo + "'  ";*/
            String query4 = " select MVD_CodProd,convert(varchar(300),MVD_ComentarioLinea),UsCreacion,convert(datetime,MVD_Fecha) "
                    +" from SiaSchaffner.dbo.STO_MOVDET "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 8 and "
                    +" MVD_NumeroDoc =  " + numeroDoc + " "
                    +" and MVD_CodProd = '" + codigo + "'";
            rs = st.executeQuery(query4);
            while(rs.next()){
                BitacoraVales vales = new BitacoraVales();
                if(!rs.getString(2).trim().equals("") || !rs.getString(2).equals("null")){
                    vales.setCodigo(rs.getString(1));
                    vales.setObservacion(rs.getString(2));
                    vales.setUsuario(rs.getString(3));
                    vales.setFecha(rs.getDate(4));
                }
                bitacoraVales.addElement(vales);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " bitacoraVales() "+e.getMessage(),0);
        }
        return bitacoraVales;
    }
    
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int actualizarPedidoMateriales(String usuario,int numero,float cantidadEntregada){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "UPDATE MAC.dbo.detallePedido SET status = 2 , cantidadEntregada = " + cantidadEntregada + "  WHERE usuario = '" + usuario + "' AND numero = '" + numero + "'";
            st.execute(query1);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR actualizarPedidoMateriales(): " + e.getMessage(),0);
        }
        return count;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public int actualizarPrioridadPedido(int numero){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "UPDATE MAC.dbo.detallePedido SET prioridad = 'prioritario'  WHERE  numero = '" + numero + "'";
            st.execute(query1);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR actualizarPrioridadPedido(): " + e.getMessage(),0);
        }
        return count;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public Vector pedidoConDeficit(String usuario, String numero){
        
        Vector pedidoConDeficit = new Vector();
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
         /*String query4 =  " select de.codigo,ma.STOCK-sum(de.cantidadPedida),ma.STOCK,sum(de.cantidadPedida) from MAC.dbo.detallePedido de ,MAC.dbo.MArticulo ma "
                         +" where de.usuario = '" + usuario + "' AND de.numero = '" + numero + "'"
                         +" AND de.codigo = ma.CODIGO "
                         +" group by de.codigo,ma.STOCK";
          */
            
            String query4 = "  select de.codigo,sum(ma.PSL_Saldo)-sum(de.cantidadPedida),sum(ma.PSL_Saldo),sum(de.cantidadPedida) "
                    +"  from MAC.dbo.detallePedido de ,SiaSchaffner.dbo.STO_PRODSAL ma "
                    +"  where  de.codigo COLLATE Modern_Spanish_CI_AI = ma.PSL_CodProd "
                    +"  and ma.PSL_CodBodega <> 7 and de.usuario = '" + usuario + "' AND de.numero = '" + numero + "' "
                    +"  group by de.codigo ";
            rs = st.executeQuery(query4);
            while(rs.next()){
                if(rs.getFloat(2) < 0){
                    Vector pedidos = new Vector();
                    pedidos.addElement(rs.getString(1));
                    pedidos.addElement(rs.getString(2));
                    pedidos.addElement(new Float(rs.getFloat(3)));
                    pedidos.addElement(rs.getString(4));
                    pedidoConDeficit.addElement(pedidos);
                }
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: pedidoConDeficit (): "+e.getMessage(),0);
        }
        return pedidoConDeficit;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int borrarPedido(int np,int linea,String proceso,String usuario){
        int count = 0;
        Vector pedidos = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            conMAC.setAutoCommit(false);
            
            
            
            String query = " SELECT distinct(numero) FROM MAC.dbo.totalEquiposPorPedido  "
                    +" WHERE  np = " + np + "  AND linea = " + linea + " AND status < 3 and area = '" + proceso + "' and (usuario  = '" + usuario + "' or usuAprobacion  = '" + usuario + "')";
            rs = st.executeQuery(query);
            while(rs.next()){
                pedidos.addElement(new Integer(rs.getInt(1)));
            }
            rs.close();
            for(int i=0;i<pedidos.size();i++){
                float numeroDoc = this.traerNumDoc(((Integer)pedidos.elementAt(i)).intValue());
                String query1 = " DELETE FROM MAC.dbo.totalEquiposPorPedido  WHERE numero = '" + ((Integer)pedidos.elementAt(i)).intValue() + "'";
                st.execute(query1);
                String delete = " delete SiaSchaffner.dbo.STO_MOVDET "
                        +" where MVD_CodSistema=8 and "
                        +" MVD_CodClase =1 AND "
                        +" MVD_TipoDoc = 8 and "
                        +" MVD_NumeroDoc =  '" + numeroDoc + "' " ;
                st.execute(delete);
                String delete1 = " delete SiaSchaffner.dbo.STO_MOVENC "
                        +" where MVE_CodSistema=8 and "
                        +" MVE_CodClase =1 AND "
                        +" MVE_TipoDoc = 8 and "
                        +" MVE_FolioFisico =  '" + ((Integer)pedidos.elementAt(i)).intValue() + "' " ;
                st.execute(delete1);
                
                
                
            }
            conMAC.commit();
            conMAC.setAutoCommit(true);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " borrarPedido() " + e.getMessage(),0);
        }
        return count;
    }
    
    //-----------------------------------------------------------------------------------------------------------------------------
    
    public int borrarPedido(long numero){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            conMAC.setAutoCommit(false);
            /*String query1 = " DELETE FROM MAC.dbo.detallePedido  WHERE numero = '" + numero + "'";
            st.execute(query1);*/
            long numeroDoc = this.traerNumDoc(numero);
            
            String delete = " delete SiaSchaffner.dbo.STO_MOVDET "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 8 and "
                    +" MVD_NumeroDoc =  '" + numeroDoc + "' " ;
            st.execute(delete);
            String delete1 = " delete SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and "
                    +" MVE_FolioFisico =  '" + numero + "' " ;
            st.execute(delete1);
            String query =  " DELETE FROM MAC.dbo.totalEquiposPorPedido  WHERE numero = '" + numero + "'";
            st.execute(query);
            conMAC.commit();
            conMAC.setAutoCommit(true);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " borrarPedido() " + e.getMessage(),0);
        }
        return count;
    }
    
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int mandarPedidoABodega(String usuario,int numero,String estado){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "UPDATE MAC.dbo.detallePedido SET status = 1 , descripcionStatus = '" + estado + "' WHERE usuario = '" + usuario + "' AND numero = '" + numero + "'";
            st.execute(query1);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR mandarPedidoABodega(): " + e.getMessage(),0);
        }
        return count;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    
    public int entregarPedidoDesdeBodega(String usuario,int numero,String estado,int veri){
        int count = 0;
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(veri == 1){
                query1 = "UPDATE MAC.dbo.detallePedido SET status = 2 , descripcionStatus = '" + estado + "' WHERE usuario = '" + usuario + "' AND numero = '" + numero + "'";
            }else{
                query1 = "UPDATE MAC.dbo.detallePedido SET status = 3 , descripcionStatus = '" + estado + "' WHERE usuario = '" + usuario + "' AND numero = '" + numero + "'";
            }
            
            st.execute(query1);
            count = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR entregarPedidoDesdeBodega(): " + e.getMessage(),0);
        }
        return count;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public Vector numeroDelPedido(String usuario,int tipo){
        
        Vector numeroDelPedido = new Vector();
        String query4 = "";
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(tipo == 1 || tipo == 2){
                query4 =  " SELECT DISTINCT(numero),status,descripcionStatus FROM MAC.dbo.detallePedido WHERE usuario = '" + usuario + "' AND status in(0,1,2)";
            }else{
                query4 =  " SELECT DISTINCT(numero),status,descripcionStatus FROM MAC.dbo.detallePedido WHERE usuario = '" + usuario + "' AND status in(1,2)";
            }
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                Vector pedidos = new Vector();
                pedidos.addElement(new Integer(rs.getInt(1)));
                pedidos.addElement(new Integer(rs.getInt(2)));
                pedidos.addElement(rs.getString(3));
                numeroDelPedido.addElement(pedidos);
                
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: numeroDelPedido (): "+e.getMessage(),0);
        }
        return numeroDelPedido;
    }
    
//-------------------------------------------------------------------------------------------------------------------------------
    
    public Vector usuarios(){
        
        Vector usuarios = new Vector();
        
        try{
            conENS = myConexion.getENSConnector();
            st = conENS.createStatement();
            String query4 =  "select Codigo,NombreCompleto from ENSchaffner.dbo.MPersonal order by NombreCompleto";
            rs = st.executeQuery(query4);
            while(rs.next()){
                Usuario usuario = new Usuario();
                usuario.setCodigo(rs.getInt(1));
                usuario.setNombre(rs.getString(2));
                usuarios.addElement(usuario);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR: usuarios(): "+e.getMessage(),0);
        }
        return usuarios;
    }
//-------------------------------------------------------------------------------------------------------------------------------
  /*  public void materialesPorEquipo(int np,int linea,String proceso,String familia){
        String query4 = "";
   
        if(familia.equals("CELDA")){
   
            query4 ="  SELECT  me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP "
                    +"  ,(select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega in(11,21)) "
                    +"  ,PRO_CODTIPO "
                    +"  FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ,SiaSchaffner.dbo.STO_PRODUCTO "
                    +"  WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "'  "
                    +"  AND a.np = " + np + " AND a.linea =" + linea + " "
                    +"  and me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = PRO_CODPROD "
                    +"  ORDER BY a.readTime ";
   
        }else{
   
            query4 = "  SELECT me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP, "
                    +"  (select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and PSL_CodBodega in(11,21)) "
                    +"  ,PRO_CODTIPO "
                    +"  FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ,SiaSchaffner.dbo.STO_PRODUCTO  "
                    +"  WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                    +"  AND a.proceso = me.CodigoSemiElaborado "
                    +"  AND a.proceso = '" + proceso + "' AND a.np = " + np + "  AND a.linea = " + linea + " "
                    +"  and me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = PRO_CODPROD "
                    +"  ORDER BY a.readTime  ";
        }
   
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo()
                estructura.setCantidad(rs.getFloat(1));
                estructura.setUnidadMedida(rs.getString(2));
                estructura.setCodigoMatPrima(rs.getString(3));
                estructura.setDescripcion(rs.getString(4));
                estructura.setObservacion("");
                estructura.setStockBodega(rs.getFloat(5));
                estructura.setStockPlanta(0f);
                estructura.setTipoProducto(rs.getInt(6));
                materialesPorProcesos.addElement(estructura);
            }
            rs.close();
            this.desconectarMac();
        }catch(Exception e){
            this.desconectarMac();
            new Loger().logger("MrpBean.class " , " materialesPorEquipo(); "+e.getMessage(),0);
        }
    }
   
    public Vector retornarMaterialesPorProcesos(){
        return materialesPorProcesos;
    }*/
    //-----------------------------------------------------------------------------------------------------------------------------
    public Vector materiales(){
        
        Vector materiales = new Vector();
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            //String query4 =  "select Codigo,descripcion,Unimed from MAC.dbo.TConsumoPromedio";
            String query4 = "select ml.PRO_CODPROD,ml.PRO_DESC,ml.PRO_UMPRINCIPAL,ml.PRO_CODTIPO from SiaSchaffner.dbo.STO_PRODUCTO ml where "
                    +" ml.PRO_CODTIPO in (1,2,5,8) ";
            rs = st.executeQuery(query4);
            String caracter = "";
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                /*caracter = rs.getString(2);
                for(int i=0;i<rs.getString(2).length();i++){
                    if(rs.getString(2).charAt(i) == '"'){
                        caracter = rs.getString(2).replace('"',' ');
                    }
                }*/
                estructura.setDescripcion(rs.getString(2).replace('"',' '));
                estructura.setUnidadMedida(rs.getString(3));
                estructura.setTipoProducto(rs.getInt(4));
                materiales.addElement(estructura);
            }
            rs.close();
            mConexion.cerrarConENS();
        }catch(Exception e){
            mConexion.cerrarConENS();
            new Loger().logger("MrpBean.class " , " materiales(): "+e.getMessage(),0);
        }
        return materiales;
    }
//  public Vector materiaPrima(){
//
//        Vector materiales = new Vector();
//
//        try{
//            conENS = mConexion.getENSConnector();
//            st = conENS.createStatement();
//            //String query4 =  "select Codigo,descripcion,Unimed from MAC.dbo.TConsumoPromedio";
//            String query4 = "select ml.PRO_CODPROD,ml.PRO_DESC,ml.PRO_UMPRINCIPAL,ml.PRO_CODTIPO from SiaSchaffner.dbo.STO_PRODUCTO ml where "
//                    +" ml.PRO_CODTIPO in (1,2,5,8) ";
//            rs = st.executeQuery(query4);
//            String caracter = "";
//            while(rs.next()){
//                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
//                estructura.setCodigoMatPrima(rs.getString(1));
//
//                estructura.setDescripcion(rs.getString(2).replace('"',' '));
//                estructura.setUnidadMedida(rs.getString(3));
//                estructura.setTipoProducto(rs.getInt(4));
//                materiales.addElement(estructura);
//            }
//            rs.close();
//            mConexion.cerrarConENS();
//        }catch(Exception e){
//            mConexion.cerrarConENS();
//            new Loger().logger("MrpBean.class " , " materiales(): "+e.getMessage(),0);
//        }
//        return materiales;
//    }
    public ArrayList materiaPrimaCodigo(String tmp){
        return materiaPrima(tmp, "PRO_CODPROD");
        
    }
    
    public ArrayList materiaPrimaDesc(String tmp){
        return materiaPrima(tmp, "PRO_DESC");
        
    }
    private  ArrayList materiaPrima(String codigo, String campo){
        ArrayList materiales = new ArrayList();
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            String sql  = "SELECT " + campo +" ";
            sql += "FROM SiaSchaffner.dbo.STO_PRODUCTO ";
            sql += "WHERE " + campo +" LIKE '%" + codigo + "%' and PRO_CODTIPO in (1) ";
            sql += "ORDER BY " + campo;
            rs = st.executeQuery(sql);
            while(rs.next()){
                String tmp = rs.getString(campo).toUpperCase().trim();
                tmp = tmp.replaceAll(codigo.toUpperCase(), "<b>" + codigo.toUpperCase() + "</b>");
                materiales.add(tmp);
                tmp = null;
            }
            rs.close();
            mConexion.cerrarConENS();
        }catch(Exception e){
            mConexion.cerrarConENS();
            new Loger().logger("MrpBean.class " , " materiaPrima: "+e.getMessage(),0);
        }
        return materiales;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public Vector pedidosPorItem(long numero){
        
        Vector pedidoPorItem = new Vector();
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            
            String query4 = " select distinct(tc.MVD_CodProd),PRO_DESC,tc.MVD_UMAlternativa,tc.MVD_Cant,tc.UsCreacion,descriEstado,tc.MVD_CantAsignada,numero, "
                    +" (select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = tc.MVD_CodProd and PSL_CodBodega <> 7) as Saldo, "
                    +" status,0,area,prioridad,convert(varchar(300),tc.MVD_ComentarioLinea),tc.MVD_Linea,tc.MVD_NumeroDoc "
                    +" from SiaSchaffner.dbo.STO_MOVDET tc ,SiaSchaffner.dbo.STO_MOVENC enc ,SiaSchaffner.dbo.STO_PRODUCTO,MAC.dbo.totalEquiposPorPedido "
                    +" where  tc.MVD_CodSistema=8 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 8 "
                    +" and enc.MVE_CodSistema=8  "
                    +" and enc.MVE_CodClase =1  "
                    +" and enc.MVE_TipoDoc = 8 "
                    +" and tc.MVD_NumeroDoc = enc.MVE_NumeroDoc "
                    +" and enc.MVE_FolioFisico = " + numero + " "
                    +" and tc.MVD_CodProd = PRO_CODPROD "
                    +" and numero = " + numero + " "
                    +" order by tc.MVD_Linea ";
            
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setDescripcion(rs.getString(2));
                estructura.setUnidadMedida(rs.getString(3));
                estructura.setCantidad(rs.getFloat(4));
                estructura.setUsuario(rs.getString(5));
                estructura.setDescripcionEstado(rs.getString(6));
                estructura.setCantidadEntregada(rs.getFloat(7));
                estructura.setNumPedido(rs.getLong(8));
                estructura.setStockBodega(rs.getFloat(9));
                estructura.setEstado(rs.getInt(10));
                estructura.setStockPlanta(rs.getFloat(11));
                estructura.setProceso(rs.getString(12));
                estructura.setPrioridad(rs.getString(13));
                estructura.setObservacion(rs.getString(14));
                estructura.setLeadTime(rs.getInt(15));
                estructura.setNumDoc(rs.getLong(16));
                pedidoPorItem.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " pedidoPorItem(): "+e.getMessage(),0);
        }
        return pedidoPorItem;
    }
    
//---------------------------------------------------------------------------------------------------------------------
    public float stockBodega(String codigo){
        float stock = 0;
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = "select STOCK from MAC.dbo.MArticulo where CODIGO = '" + codigo + "'";
            //String query = "select PSL_Saldo from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = '" + codigo + "' ";
            String query = "select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where PSL_CodProd = '" + codigo + "' and PSL_CodBodega <> 7";
            
            rs = st.executeQuery(query);
            while(rs.next()){
                stock = rs.getFloat(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","stockBodega() " +  e.getMessage(),0);
        }
        
        return stock;
    }
//---------------------------------------------------------------------------------------------------------------------
    public float[] stockBodega1(String codigo){
        float[] stock = new float[2];
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String sql = "select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where   PSL_CodBodega not in (7,10)  and PSL_CodProd = '"+codigo+"'";
            String sql1 = "select sum(PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL where   PSL_CodBodega = 10 and PSL_CodProd = '"+codigo+"'";
            
            rs = st.executeQuery(sql);
            while(rs.next()){
                stock[0] = rs.getFloat(1);
            }
            rs.close();
            
            rs = st.executeQuery(sql1);
            while(rs.next()){
                stock[1] = rs.getFloat(1);
            }
            rs.close();
            
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","stockBodega() " +  e.getMessage(),0);
        }
        
        return stock;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public byte actualizarEstado(int estado,long numero,String descri,String usuario){
        byte veri = (byte)0;
        try{
            conMAC = myConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            /*String query = "UPDATE MAC.dbo.detallePedido SET status = "+ estado +" , descripcionStatus = '" + descri + "'  WHERE numero = " + numero + "" ;
            st.execute(query);*/
            if(estado == 5){
                String query1 = "UPDATE MAC.dbo.totalEquiposPorPedido SET status = "+ estado +" ,descriEstado = '" + descri + "',fechaEntregado = getdate(),usuBodega = '" + usuario + "'  WHERE numero = " + numero + "" ;
                st.execute(query1);
                String query = " update SiaSchaffner.dbo.STO_MOVENC set "
                        +" MVE_Aprobacion = 'S',MVE_EstadoImpresion = 'S' where "
                        +" MVE_CodSistema=8 and "
                        +" MVE_CodClase =1 AND "
                        +" MVE_TipoDoc =8  and "
                        +" MVE_FolioFisico = " + numero + " ";
                st.executeUpdate(query);
            }else{
                String query1 = "UPDATE MAC.dbo.totalEquiposPorPedido SET status = "+ estado +" ,descriEstado = '" + descri + "' WHERE numero = " + numero + "" ;
                st.execute(query1);
            }
            
            conMAC.commit();
            conMAC.setAutoCommit(true);
            veri = (byte)1;
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " actualizarEstado() " + e.getMessage(),0);
        }
        return veri;
    }
    
//-------------------------------------------------------------------------------------------------------------------------------
    public byte devolverEstadoCantidadEntregada(Vector cantiEntregada){
        byte very = (byte)0;
        for(int i=0;i<cantiEntregada.size();i++){
            if(((EstructuraPorEquipo)cantiEntregada.elementAt(i)).getCantidadEntregada() == 0){
                very = (byte)1;
            }
        }
        return very;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public String devolverEstadoGrabacionAflex(char ver){
        String mostrar = "";
        switch (ver) {
            case '2':
                mostrar = "el documento a traspasar est� incompleto.. revise num.,lin.,cod.,Etc.";
                break;
            case '3':
                mostrar = "Campo proceso no se puede relacionar con Centro de costo";
                break;
            case '4':
                mostrar = "No se puede ingresar consumo, en Flex est� cerrado el mes ";
                break;
            case '5':
                mostrar = "N� de pedido ya existe  en Flex ";
                break;
            case '6':
                mostrar = "Codigo no existe en Flex ";
                break;
            case '7':
                mostrar = "No existe suficiente stock para este consumo ";
                break;
            default:
                mostrar = "Proceso no realizado";
                break;
        }
        return mostrar;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public void actualizarMaterialesTconsumoPromedio(){
        Vector mater = new Vector();
        byte total = 0;
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select ml.PRO_CODPROD,PRO_MONEDACOMPRA from SiaSchaffner.dbo.STO_PRODUCTO ml "
                    +" left outer join MAC.dbo.ProductoAnexo pa "
                    +" on(pa.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS =  ml.PRO_CODPROD) "
                    +" where "
                    +" (ml.PRO_CODTIPO in (1,2,5,8) or ml.PRO_CODPROD like '5041%') "
                    +" and len(ml.PRO_CODPROD) = 10 "
                    +" and pa.Codigo is null ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setEstado(rs.getInt(2));
                mater.addElement(estructura);
            }
            rs.close();
            for(int i=0;i<mater.size();i++){
                total += this.actualizarMaterialesTconsumoPromedio(((EstructuraPorEquipo)mater.elementAt(i)).getCodigoMatPrima()
                ,((EstructuraPorEquipo)mater.elementAt(i)).getEstado());
            }
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: ","actualizarMaterialesTconsumoPromedio() " +  e.getMessage(),0);
        }
    }
//------------------------------------------------------------------------------------------------------------------------------
    private byte actualizarMaterialesTconsumoPromedio(String codigo,int estado){
        int verifi = 0;
        byte veri = 0;
        try{
            String query4 =  "INSERT INTO MAC.dbo.ProductoAnexo(Codigo,LeadTime,Estado,Stokeable,MonedaCompra,Comprador) values ('"+ codigo +"',"+ 0 +","+ 0 +","+ 1 +","+ estado +",'null')";
            st.execute(query4);
            veri = 1;
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: actualizarMaterialesTconsumoPromedio(): "+e.getMessage(),0);
        }
        return veri;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    // actualiza los materiales de estructura que estan bajos el stock minimo
    public byte actualizarComprasRequeridasEE(String codigo,float cantRequerida){
        int count = 0;
        float cantReq = 0;
        float numSap = 0;
        byte veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "UPDATE MAC.dbo.ComprasRequeridas SET cantRequerida = '" + cantRequerida + "' ,verificar  = 1,fechaEntrega = '" + fechaM + "',contador = '" + fechaM + "'  WHERE Codigo = '" + codigo + "' AND verificar = 4 " ;
            st.execute(query);
            veri = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR actualizarComprasRequeridasEE(): " + e.getMessage() + " " + codigo,0);
        }
        return veri;
    }
//---------------------------------------------------------------------------------------------------------------------
    public Vector familiaPorMaterial(String usuario){
        Vector familiaPorMaterial = new Vector();
        String query = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            if(usuario.equals("rrosas")){
                query = "SELECT DISTINCT(familia) FROM MAC.dbo.materialesPorFamilia where estado = 2 order by familia";
            }else{
                query = "SELECT DISTINCT(familia) FROM MAC.dbo.materialesPorFamilia where estado = 1 order by familia";
            }
            
            
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setFamilia(rs.getString(1));
                familiaPorMaterial.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","familiaPorMaterial() " +  e.getMessage(),0);
        }
        return familiaPorMaterial;
    }
    //---------------------------------------------------------------------------------------------------------------------
    public Vector leadTimePorFamilia(String familia){
        Vector leadTimePorFamilia = new Vector();
        String query = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            
            query = "SELECT Codigo FROM MAC.dbo.materialesPorFamilia where familia = '" + familia +  "'";
            
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                leadTimePorFamilia.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","leadTimePorFamilia() " +  e.getMessage(),0);
        }
        return leadTimePorFamilia;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public void actualizarMaterialesPorFamilia(String codigo,String familia,byte estado){
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "SELECT COUNT(Codigo) FROM MAC.dbo.materialesPorFamilia WHERE Codigo = '" + codigo + "' AND familia = '" + familia + "' AND estado = " + estado + " ";
            rs = st.executeQuery(query1);
            while(rs.next()){
                count = rs.getInt(1);
            }
            if(count == 0){
                String query = "INSERT INTO MAC.dbo.materialesPorFamilia (Codigo,familia,estado) values('" + codigo + "','" + familia.toLowerCase() + "'," + estado + ")";
                new Loger().logger("MrpBean.class " , query ,1);
                st.execute(query);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR actualizarMaterialesPorFamilia(): " + e.getMessage() + " " + codigo,0);
        }
        
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public void borrarMaterialesPorFamilia(String codigo,String familia,byte estado){
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "DELETE FROM MAC.dbo.materialesPorFamilia WHERE Codigo = '" + codigo + "' AND familia = '" + familia + "' AND estado = " + estado + " ";
            st.execute(query);
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR borrarMaterialesPorFamilia(): " + e.getMessage() + " " + codigo,0);
        }
    }
//---------------------------------------------------------------------------------------------------------------------
    public int comprasRequeridasPorComprador(String comprador){
        int cantidad = 0;
        String query = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            if(comprador.equals("todos")){
                query = "SELECT COUNT(Codigo) FROM MAC.dbo.ComprasRequeridas WHERE verificar in(1,2,4)";
            }else{
                query = " SELECT COUNT(*) FROM MAC.dbo.ComprasRequeridas cr "
                        +" inner join MAC.dbo.ProductoAnexo pa "
                        +" on(cr.Codigo = pa.Codigo) "
                        +" WHERE pa.Comprador = '" + comprador + "' AND verificar in(1,2,4) ";
            }
            rs = st.executeQuery(query);
            while(rs.next()){
                cantidad = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","comprasRequeridasPorComprasdor() " +  e.getMessage(),0);
        }
        return cantidad;
    }
//---------------------------------------------------------------------------------------------------------------------
    public int comprasRequeridasConSapPorComprador(String comprador){
        int cantidad = 0;
        String query = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            if(comprador.equals("todos")){
                query = "select count(Codigo) from MAC.dbo.ComprasRequeridas where verificar <> 3 and  verificar <> 0 and numeroSap is not null";
            }else{
                query = "select count(Codigo) from MAC.dbo.ComprasRequeridas where comprador = '" + comprador + "' and verificar <> 3 and  verificar <> 0 and numeroSap is not null";
            }
            
            rs = st.executeQuery(query);
            while(rs.next()){
                cantidad = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","comprasRequeridasConSapPorComprador() " +  e.getMessage(),0);
        }
        return cantidad;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public void cambiarEstadoMaterial(String codigo,byte estado){
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            conMAC.setAutoCommit(false);
            //String query = "SELECT estado FROM MAC.dbo.TConsumoPromedio WHERE Codigo = '" + codigo + "'";
            /*String query = " SELECT Estado FROM MAC.dbo.ProductoAnexo WHERE Codigo = '" + codigo + "' ";
             
             
            rs = st.executeQuery(query);
            while(rs.next()){
                estado = rs.getByte(1);
            }*/
            if(estado == 1){
                String query1 = "UPDATE MAC.dbo.ProductoAnexo SET Estado = 1 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query1);
                String query2 = "INSERT INTO MAC.dbo.materialesPorFamilia (Codigo,familia,estado) values('" + codigo + "','descartados',1)";
                st.execute(query2);
                String query3 = "UPDATE MAC.dbo.ProductoAnexo SET analisis = 0 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query3);
                String query4 = "DELETE FROM MAC.dbo.materialesPorFamilia WHERE Codigo = '" + codigo + "' AND familia = 'solo minMax' ";
                st.execute(query4);
                String query5 = "UPDATE MAC.dbo.ProductoAnexo SET anaGen = 0 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query5);
                
                
            }
            if(estado == 2){
                String query1 = "UPDATE MAC.dbo.ProductoAnexo SET analisis = 1 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query1);
                String query2 = "INSERT INTO MAC.dbo.materialesPorFamilia (Codigo,familia,estado) values('" + codigo + "','solo minMax',1)";
                st.execute(query2);
                String query3 = "UPDATE MAC.dbo.ProductoAnexo SET Estado = 0 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query3);
                String query4 = "DELETE FROM MAC.dbo.materialesPorFamilia WHERE Codigo = '" + codigo + "' AND familia = 'descartados' ";
                st.execute(query4);
                String query5 = "UPDATE MAC.dbo.ProductoAnexo SET anaGen = 0 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query5);
            }
            if(estado == 3){
                String query1 = "UPDATE MAC.dbo.ProductoAnexo SET anaGen = 1 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query1);
                String query3 = "UPDATE MAC.dbo.ProductoAnexo SET Estado = 0 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query3);
                String query4 = "DELETE FROM MAC.dbo.materialesPorFamilia WHERE Codigo = '" + codigo + "' AND familia = 'descartados' ";
                st.execute(query4);
                String query5 = "UPDATE MAC.dbo.ProductoAnexo SET analisis = 0 WHERE Codigo = '" + codigo + "'" ;
                st.execute(query5);
                String query2 = "DELETE FROM MAC.dbo.materialesPorFamilia WHERE Codigo = '" + codigo + "' AND familia = 'solo minMax' ";
                st.execute(query2);
            }
            conMAC.commit();
            conMAC.setAutoCommit(true);
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " cambiarEstadoMaterial(): " + e.getMessage() + " " + codigo,0);
        }
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public boolean asociarMateriales(String codigo,String asociado, byte estado){
        boolean asociar = false;
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(estado == 1){
                query1 = "UPDATE MAC.dbo.ProductoAnexo SET asociado = '" + asociado + "'  WHERE Codigo = '" + codigo + "'" ;
            }
            if(estado == 2){
                query1 = "UPDATE MAC.dbo.ProductoAnexo SET asociado = " + null +  "  WHERE Codigo = '" + codigo + "'" ;
            }
            
            st.execute(query1);
            asociar = true;
            rs.close();
            st.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " asociarMateriales(): " + e.getMessage(),0);
        }
        return asociar;
    }
    public Vector detalleConsumoPromedio(String codigo){
        Vector consumoPromedios = new Vector();
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " SELECT Ano,Mes,Cantidad FROM MAC.dbo.consumosPorMaterial1 WHERE Codigo = '" + codigo + "' "
                    +" order by Ano,Mes ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo consumoProm = new EstructuraPorEquipo();
                consumoProm.setAno(rs.getInt(1));
                consumoProm.setMes(rs.getInt(2));
                consumoProm.setCantidad(rs.getFloat(3));
                consumoPromedios.add(consumoProm);
                
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","detalleConsumoPromedio() " +  e.getMessage(),0);
        }
        return consumoPromedios;
    }
    public Vector detalleDevolucionesPromedio(String codigo){
        Vector detalleDevolucionesPromedio = new Vector();
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " SELECT Ano,Mes,Cantidad FROM MAC.dbo.devolucionesPorMaterial WHERE Codigo = '" + codigo + "' "
                    +" order by Ano,Mes ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo consumoProm = new EstructuraPorEquipo();
                consumoProm.setAno(rs.getInt(1));
                consumoProm.setMes(rs.getInt(2));
                consumoProm.setCantidad(rs.getFloat(3));
                detalleDevolucionesPromedio.add(consumoProm);
                
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: "," detalleDevolucionesPromedio() " +  e.getMessage(),0);
        }
        return detalleDevolucionesPromedio;
    }
    
    //borrar
    public Vector fechasConsumoPromedio(){
        Vector fechasConsumoPromedio = new Vector();
        
        /*GregorianCalendar calendario = new GregorianCalendar();
        int ano = Integer.parseInt(fechaM.substring(0,4));
        String mes = fechaM.substring(4,6);
        calendario.set( Calendar.MONTH, Integer.parseInt(mes)-1);
        String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = " SELECT codigo, fecha, cantidad FROM MAC.dbo.consumosPorMaterial where codigo = '"+codigo+"' order by fecha";
            String query =  " select Distinct(YEAR(fecha)),MONTH(fecha) "
                    +" from   MAC.dbo.consumosPorMaterial  group by YEAR(fecha),MONTH(fecha) "
                    +" order by YEAR(fecha),MONTH(fecha) ";
            String query = " SELECT distinct(Ano),Mes FROM MAC.dbo.consumosPorMaterial1 "
                          +" order by Ano,Mes ";
         
         
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo consumoProm = new EstructuraPorEquipo();
                consumoProm.setAno(rs.getInt(1));
                consumoProm.setMes(rs.getInt(2));
                fechasConsumoPromedio.addElement(consumoProm);
            }
            rs.close();
            st.close();
            conMAC.close();*/
        try{
            String fechaM = formateador.format(new java.util.Date());
            GregorianCalendar calendario = new GregorianCalendar();
            int ano = Integer.parseInt(fechaM.substring(0,4));
            String mes = fechaM.substring(4,6);
            if(Integer.parseInt(mes)-2 == 0){
                calendario.set( Calendar.MONTH,12);
            }else{
                calendario.set( Calendar.MONTH, Integer.parseInt(mes)-2);
            }
            
            String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
            String mes1 = String.valueOf(Integer.parseInt(mes)-1);
            
            if(mes1.equals("00")){
                mes1 = "12";
                ano -= 1;
            }
            if(mes.substring(0,1).equals("0")){
                mes = mes.substring(1,2);
            }
            String fecIni = String.valueOf(ano-1);
            String fecFin = String.valueOf(ano);
            for(int i=Integer.parseInt(mes);i<13;i++){
                EstructuraPorEquipo consumoProm = new EstructuraPorEquipo();
                consumoProm.setAno(ano-1);
                consumoProm.setMes(i);
                fechasConsumoPromedio.addElement(consumoProm);
                //System.out.println(fecIni + i);
            }
            for(int i=1;i<=Integer.parseInt(mes1);i++){
                EstructuraPorEquipo consumoProm = new EstructuraPorEquipo();
                consumoProm.setAno(ano);
                consumoProm.setMes(i);
                fechasConsumoPromedio.addElement(consumoProm);
                //System.out.println(fecFin + i);
            }
        } catch(Exception e){
            new Loger().logger("MrpBean.class: "," fechasConsumoPromedio() " +  e.getMessage(),0);
        }
        return fechasConsumoPromedio;
    }
    
    public Vector generarSap(int verificar,int numeroSap){
        Vector comprasRequeridas = new Vector();
        String query1 = "";
        GregorianCalendar calendario = new GregorianCalendar();
        int ano = Integer.parseInt(fechaM.substring(0,4));
        String mes = fechaM.substring(4,6);
        calendario.set( Calendar.MONTH, Integer.parseInt(mes)-1);
        String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(verificar == 1){
           /*query1 = " SELECT cr.Codigo,cr.Descripcion,cr.cantRequerida,cr.UniMed,cr.comprador,cr.leadTime,cr.contador,cr.verificar,cr.stockMin, "
                   +" cr.stockMax,cr.fechaEntrega , "
                   +" (select SUM(tc.CAN_PEN) from  MAC.dbo.TOCompra tc  where tc.COD_PRO=cr.Codigo AND tc.TIP_REG = 1 AND tc.CAN_PEN > 0 AND tc.FEC_ENT < '" + fechaM + "') as ordenVencida, "
                   +" (select SUM(tc.CAN_PEN) from  MAC.dbo.TOCompra tc  where tc.COD_PRO=cr.Codigo AND tc.TIP_REG = 1 AND tc.CAN_PEN > 0 AND tc.FEC_ENT >= '" + fechaM + "') as ordenFutura, "
                   +" (SELECT sum(me.CantidadMP) "
                   +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                   +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                   +" AND a.proceso = CodigoSemiElaborado "
                   +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                   +" (SELECT SUM(me.CantidadMP) "
                   +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma "
                   +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                   +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                   +" AND ma.readTime >= '" + fechaM + "' "
                   +" AND ma.vigencia != 1 "
                   +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                   +" (SELECT SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial WHERE codigo = cr.Codigo)  AS MediaCantidad, "
                   +" (SELECT ml.STOCK FROM MAC.dbo.MArticulo ml WHERE ml.CODIGO = cr.Codigo) as Stock,cr.estado, "
                   +" cr.numeroSap,cr.id "
                   +" FROM MAC.dbo.ComprasRequeridas  cr "
                   +" WHERE verificar in(1,2,4) "
                   +" and cr.numeroSap = '"+numeroSap+"' ";
            */
                query1 = " SELECT cr.Codigo,cr.Descripcion,cr.cantRequerida,cr.UniMed,cr.comprador,cr.leadTime,cr.contador,cr.verificar,cr.stockMin, "
                        +" cr.stockMax,cr.fechaEntrega , "
                        +"(select sum(tc.MVD_Cant-tc.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tc "
                        +" where  tc.MVD_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS = cr.Codigo  and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega < '" + fechaM + "' "
                        +" and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0) as ordenVencida, "
                        +" (select sum(tc.MVD_Cant-tc.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tc "
                        +" where  tc.MVD_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS = cr.Codigo  and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1 and tc.MVD_FechaEntrega >= '" + fechaM + "' "
                        +" and (tc.MVD_Cant-tc.MVD_CantAsignada) > 0) as ordenFutura, "
                        +" (SELECT sum(me.CantidadMP) "
                        +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                        +" WHERE a.codProducto = me.CodigoTerminado "
                        +" AND a.proceso = CodigoSemiElaborado "
                        +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_no_Celda, "
                        +" (SELECT SUM(me.CantidadMP) "
                        +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                        +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                        +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                        +" AND me.CodigoMatPrima in(cr.Codigo)) as NecesidadPorEquipo_si_Celda, "
                        
                        //+" (SELECT SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial WHERE codigo = cr.Codigo)  AS MediaCantidad, "
                        +" ((SELECT sum(Cantidad) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                        +" WHERE ma1.Codigo = cr.Codigo ) "
                        +" - "
                        +" (SELECT sum(Cantidad) FROM MAC.dbo.devolucionesPorMaterial de1 "
                        +" WHERE de1.Codigo = cr.Codigo) "
                        +" )/12  AS MediaCantidad,     "
                        +" (SELECT sum(ml.PSL_Saldo) FROM SiaSchaffner.dbo.STO_PRODSAL ml WHERE ml.PSL_CodProd COLLATE SQL_Latin1_General_CP850_CS_AS = cr.Codigo  and PSL_CodBodega <> 7) as Stock,cr.estado, "
                        +" cr.numeroSap,cr.id "
                        +" FROM MAC.dbo.ComprasRequeridas  cr "
                        +" WHERE verificar in(1,2,4) "
                        +" and cr.numeroSap = '"+numeroSap+"' ";
                // +" ORDER BY cr." + ordenar + " ";
                rs = st.executeQuery(query1);
                while(rs.next()){
                    Vector compras = new Vector();
                    compras.addElement(rs.getString(1));
                    compras.addElement(rs.getString(2));
                    compras.addElement(new Float(rs.getFloat(3)));
                    compras.addElement(rs.getString(4));
                    compras.addElement(rs.getString(5));
                    compras.addElement(new Short(rs.getShort(6)));
                    compras.addElement(rs.getDate(7));
                    compras.addElement(new Byte(rs.getByte(8)));
                    compras.addElement(new Float(rs.getFloat(9)));
                    compras.addElement(new Float(rs.getFloat(10)));
                    compras.addElement(rs.getDate(11));
                    compras.addElement(new Float(rs.getFloat(12)));
                    compras.addElement(new Float(rs.getFloat(13)));
                    compras.addElement(new Float(rs.getFloat(14) + rs.getFloat(15)));
                    compras.addElement(new Float(rs.getFloat(16)));
                    compras.addElement(new Float(rs.getFloat(17)));
                    compras.addElement(new Byte(rs.getByte(18)));
                    compras.addElement(new Integer(rs.getInt(19)));
                    compras.addElement(new Integer(rs.getInt(20)));
                    comprasRequeridas.addElement(compras);
                }
            }else if(verificar == 0){
                query1 = " SELECT Codigo,Descripcion,UniMed,comprador,leadTime,stockMin, "
                        +" stockMax FROM MAC.dbo.ComprasRequeridas  "
                        +" WHERE verificar = 0 ORDER BY Codigo";
                rs = st.executeQuery(query1);
                while(rs.next()){
                    Vector compras = new Vector();
                    compras.addElement(rs.getString(1));
                    compras.addElement(rs.getString(2));
                    compras.addElement(rs.getString(3));
                    compras.addElement(rs.getString(4));
                    compras.addElement(new Short(rs.getShort(5)));
                    compras.addElement(new Float(rs.getFloat(6)));
                    compras.addElement(new Float(rs.getFloat(7)));
                    comprasRequeridas.addElement(compras);
                }
            }else{
                query1 =  " SELECT Codigo ,fechaEntrega "
                        +" FROM MAC.dbo.ComprasRequeridas "
                        +" WHERE verificar  = 1  AND numeroSap IS NULL "
                        +" ORDER BY Codigo ";
                rs = st.executeQuery(query1);
                while(rs.next()){
                    Vector compras = new Vector();
                    compras.addElement(rs.getString(1));
                    compras.addElement(rs.getDate(2));
                    comprasRequeridas.addElement(compras);
                }
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR generarSap(): " + e.getMessage(),0);
        }
        return comprasRequeridas;
    }
    
    public void cambiarStokeable(String codigo){
        byte valor = (byte)3;
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = " SELECT codigo, fecha, cantidad FROM MAC.dbo.consumosPorMaterial where codigo = '"+codigo+"' order by fecha";
            String query =  " select Stokeable from   MAC.dbo.ProductoAnexo where Codigo = '" + codigo + "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                valor = rs.getByte(1);
            }
            rs.close();
            if(valor == 1){
                String update = "update MAC.dbo.ProductoAnexo set Stokeable = 0 where Codigo = '" + codigo + "'";
                st.executeUpdate(update);
            }if(valor == 0){
                String update = "update MAC.dbo.ProductoAnexo set Stokeable = 1 where Codigo = '" + codigo + "'";
                st.executeUpdate(update);
            }
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: ","cambiarStokeable() " +  e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------------------
    
    public Vector cambiarStockMinMax(String familia,float e,float k1,float k2){
        Vector fechasConsumoPromedio = new Vector();
        float min = 0;
        float max = 0;
        GregorianCalendar calendario = new GregorianCalendar();
        int ano = Integer.parseInt(fechaM.substring(0,4));
        String mes = fechaM.substring(4,6);
        calendario.set( Calendar.MONTH, Integer.parseInt(mes)-1);
        String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = " SELECT codigo, fecha, cantidad FROM MAC.dbo.consumosPorMaterial where codigo = '"+codigo+"' order by fecha";
            /*String query =  " select mpf.Codigo,tc.StockMinimo,tc.StockMaximo,tc.leadTime,(SELECT SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial WHERE codigo = mpf.Codigo) as mediaCantidad from  MAC.dbo.materialesPorFamilia mpf "
                           +" left outer join MAC.dbo.TConsumoPromedio tc "
                           +" on(mpf.Codigo = tc.Codigo) "
                           +" where mpf.familia = '" + familia + "'";
             */
            String query = " select mpf.Codigo,st.PRO_INVMIN,st.PRO_INVMAX,tc.LeadTime, "
                    
                    //+" (SELECT SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial WHERE codigo = mpf.Codigo)  AS MediaCantidad "
                    +" ((SELECT sum(Cantidad) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                    +" WHERE ma1.Codigo = mpf.Codigo ) "
                    +" - "
                    +" (SELECT sum(Cantidad) FROM MAC.dbo.devolucionesPorMaterial de1 "
                    +" WHERE de1.Codigo = mpf.Codigo) "
                    +" )/12  AS MediaCantidad,     "
                    +" from  MAC.dbo.materialesPorFamilia mpf "
                    +" left outer join MAC.dbo.ProductoAnexo tc "
                    +" on(tc.Codigo = mpf.Codigo) "
                    +" left outer join SiaSchaffner.dbo.STO_PRODUCTO st "
                    +" on(st.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS = mpf.Codigo ) "
                    +" where mpf.familia = '" + familia + "' ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo consumoProm = new EstructuraPorEquipo();
                min = rs.getFloat(5)/30;
                min *= rs.getInt(4) + e;
                min *= k1;
                max = rs.getFloat(5) * k2;
                max += min;
                consumoProm.setCodigoMatPrima(rs.getString(1));
                consumoProm.setStockMinimo(min);
                consumoProm.setStockMaximo(max);
                consumoProm.setCantidad(rs.getFloat(2));
                consumoProm.setCantidadEntregada(rs.getFloat(3));
                consumoProm.setLeadTime(rs.getInt(4));
                consumoProm.setMediaCantidad(rs.getDouble(5));
                
                fechasConsumoPromedio.addElement(consumoProm);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception ex){
            new Loger().logger("MrpBean.class: ","cambiarStockMinMax() " +  ex.getMessage(),0);
        }
        return fechasConsumoPromedio;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public void insertarObservacionAmaterial(String codigo,String observacion,java.util.Date fecha){
        SimpleDateFormat formateador = new SimpleDateFormat("yyyyMMdd HH:mm:ss:SSS");
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
            String query1 = "insert into ENSchaffner.dbo.SC_bitacora_materiales(Codigo,Observacion,Fecha) values ('"+ codigo +"','"+ observacion +"','"+ formateador.format(fecha) +"')";
            st.execute(query1);
            
            
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , "ERROR insertarObservacionAmaterial(): " + e.getMessage(),0);
        }
    }
    
    public Vector bitacoraMateriales(String codigo){
        Vector bitacoraMateriales = new Vector();
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            String query = " SELECT Observacion,Fecha  FROM ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = '" + codigo + "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructuraPorEquipo = new EstructuraPorEquipo();
                estructuraPorEquipo.setBitacoraMaterial(rs.getString(1));
                estructuraPorEquipo.setFechaBitacora(rs.getTimestamp(2));
                bitacoraMateriales.addElement(estructuraPorEquipo);
            }
            rs.close();
            st.close();
            conENS.close();
        } catch(Exception ex){
            new Loger().logger("MrpBean.class: "," bitacoraMateriales() " +  ex.getMessage(),0);
        }
        return bitacoraMateriales;
    }
    
//-------------------------------------------------------------------------------------------------------------------------
    public StringBuffer bitacoraMaterialesUnidos(String codigo){
        StringBuffer bitacoraMateriales = new StringBuffer();
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            String query = " SELECT Observacion,Fecha FROM ENSchaffner.dbo.SC_bitacora_materiales  where Codigo = '" + codigo + "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                bitacoraMateriales.append(rs.getString(1) + "-" + rs.getTimestamp(2) + "\n");
            }
            if(bitacoraMateriales.length()==0){
                bitacoraMateriales.append("Sin Observacion");
            }
            // System.out.println( "bitacoraMateriales: " + bitacoraMateriales);
            rs.close();
            st.close();
            conENS.close();
        } catch(Exception ex){
            new Loger().logger("MrpBean.class: "," bitacoraMaterialesUnidos() " +  ex.getMessage(),0);
        }
        return bitacoraMateriales;
    }
    //------------------------------------------------------------------------------------------------------------------------------
    public Vector buscarPedido(String param,byte entrar, String estado1, String estado2, String estado3, String estado4, String estado5){
        
        Vector pedidosPorUsuario = new Vector();
        String query4 = "";
        
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(entrar == 1){
                query4 =  "SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) FROM MAC.dbo.totalEquiposPorPedido WHERE numero = '" + param + "' and descriEstado in('"+estado1+"','"+estado2+"','"+estado3+"','"+estado4+"','"+estado5+"') group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc ";
            }else if(entrar == 2){
                query4 =  "SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) FROM MAC.dbo.totalEquiposPorPedido where usuario = '" + param + "' and descriEstado in('"+estado1+"','"+estado2+"','"+estado3+"','"+estado4+"','"+estado5+"')  group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc";
            }else if(entrar == 3){
                String fechaIni =  param.substring(0,param.indexOf(" "));
                String fechaFin =  param.substring(param.indexOf(" ")+1);
                query4 =  "SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) FROM MAC.dbo.totalEquiposPorPedido where fecha between '" + fechaIni + "' and '" + fechaFin + "' and descriEstado in('"+estado1+"','"+estado2+"','"+estado3+"','"+estado4+"','"+estado5+"') group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc";
            }else if(entrar == 4){
                query4 =  "SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) FROM MAC.dbo.totalEquiposPorPedido where status < 5 group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc ";
            }else if(entrar == 5){
                query4 =  "SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) FROM MAC.dbo.totalEquiposPorPedido where status < 5 and usuario = '" + param + "' group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc";
            }else if(entrar == 6){
                query4 =  "SELECT DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) FROM MAC.dbo.totalEquiposPorPedido where np = '" + param + "' and descriEstado in('"+estado1+"','"+estado2+"','"+estado3+"','"+estado4+"','"+estado5+"') group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc";
            }else if(entrar == 7){
                query4 =  " select DISTINCT(numero),usuario,descriEstado,area,prioridad,fecha,usuAprobacion,max(np) "
                        +" from SiaSchaffner.dbo.STO_MOVDET  ,SiaSchaffner.dbo.STO_MOVENC ,MAC.dbo.totalEquiposPorPedido "
                        +" where MVD_CodSistema=8 and MVD_CodClase =1 AND MVD_TipoDoc = 8 and "
                        +" MVE_CodSistema=8 and "
                        +" MVE_CodClase =1 AND "
                        +" MVE_TipoDoc = 8 and "
                        +" MVD_NumeroDoc = MVE_NumeroDoc "
                        +" and MVE_FolioFisico = numero "
                        +" and MVD_CodProd = '" + param + "' and descriEstado in('"+estado1+"','"+estado2+"','"+estado3+"','"+estado4+"','"+estado5+"') group by numero,usuario,descriEstado,area,prioridad,fecha,usuAprobacion order by fecha desc";
            }else{
                
            }
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setNumPedido(rs.getLong(1));
                estructura.setUsuario(rs.getString(2));
                estructura.setDescripcionEstado(rs.getString(3));
                estructura.setProceso(rs.getString(4));
                estructura.setPrioridad(rs.getString(5));
                estructura.setFechaIn(rs.getString(6));
                estructura.setUsuAprobar(rs.getString(7));
                estructura.setEstado(rs.getInt(8));
                pedidosPorUsuario.addElement(estructura); // n�mero del pedido
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " buscarPedido(): "+e.getMessage(),0);
        }
        return pedidosPorUsuario;
    }
//---------------------------------------------------------------------------------------------------------------------------
    public boolean validarUsuarioALink(String usuario,String nombre){
        boolean valUsuario = false;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "  select Nombre from MAC.dbo.LG_permiso_a_link where Usuario = '" + usuario + "'";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                if(rs.getString(1).equals(nombre)){
                    valUsuario = true;
                    break;
                }
            }
            
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " validarUsuarioALink() "+e.getMessage(),0);
        }
        return valUsuario;
    }
//----------------------------------------------------------------------------------------------------------------------------
    public Vector usuAprobarVale(String proceso){
        Vector valUsuario = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            //por ahora se va a dejar que aparezcan todos los supervisores  no solamente por proceso
            // String query = "  select UsuAprobar from LG_permisos_vales where  proceso = '" + proceso + "' ";
            String query = "  select distinct(UsuAprobar) from LG_permisos_vales ";
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setUsuario(rs.getString(1));
                valUsuario.addElement(estruc);
            }
            
            
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " usuAprobarVale() "+e.getMessage(),0);
        }
        return valUsuario;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    public String imprimirPedidos(long numero){
        String imprimir = "";
        Crp2Bean  crp = new Crp2Bean();
        Vector pedidosPorItem = this.pedidosPorItem(numero);
        Vector equiposPorItem = crp.equiposPorItem(numero);
        
        imprimir += " <table> ";
        imprimir += "    <tr> ";
        imprimir += "      <td> ";
        imprimir += "   <table border=0 cellspacing=0 cellpadding=2 align=left> ";
        imprimir += "       <tr align=center> ";
        imprimir += "       <td align=center class=Estilo10 width=30>Np</td> ";
        imprimir += "       <td align=center class=Estilo10 width=30>Linea</td> ";
        imprimir += "       <td align=center class=Estilo10 width=80>N� Pedido</td> ";
        imprimir += "       </tr> ";
        imprimir += "   <tr> ";
        imprimir += "   <td colspan=3 valign=top> ";
        imprimir += "   <table  class=dos bgcolor=#DDDDDD> ";
        for(int h=0;h<equiposPorItem.size();h++){
            
            imprimir += "   <tr> ";
            imprimir += "     <td align=center class=Estilo10 width=30> " + ((Equipo)equiposPorItem.elementAt(h)).getNp() + "</td> ";
            imprimir += "     <td align=center class=Estilo10 width=30> " + ((Equipo)equiposPorItem.elementAt(h)).getLinea() + "</td>";
            imprimir += "     <td align=center class=Estilo10 width=80> " + numero + "</td>";
            imprimir += "   </tr> ";
            
        }
        imprimir += "   </table> ";
        imprimir += "   </td> ";
        imprimir += "   </tr> ";
        imprimir += "</table>";
        imprimir += "  </td> ";
        imprimir += "  </tr> ";
        imprimir += "  <tr>  ";
        imprimir += "  <td>  ";
        imprimir += "  <table border=1 cellspacing=0 cellpadding=2 bgcolor=#cccccc align=center width=90%> ";
        imprimir += "  <tr bgcolor=#B9DDFB align=center> ";
        imprimir += "  <td align=center class=Estilo10 width=80>C�digo</td> ";
        imprimir += "  <td align=center class=Estilo10 width=80>Descripci�n</td> ";
        imprimir += "  <td align=center class=Estilo10 width=80>Um</td> ";
        imprimir += "  <td align=center class=Estilo10 width=80>Pedida</td> ";
        imprimir += "  <td align=center class=Estilo10 width=80>Entregada</td> ";
        imprimir += "  <td align=center class=Estilo10 width=80>Observaciones</td> ";
        imprimir += "  </tr> ";
        for(int u=0;u<pedidosPorItem.size();u++){
            imprimir += "  <tr> ";
            imprimir += "  <td align=center><font class=Estilo9>" + ((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima() +" </font></td> ";
            imprimir +="   <td align=center><font class=Estilo9>" + this.devolverDescripcion(((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()) + " </font></td> ";
            imprimir +="   <td align=center><font class=Estilo9>" + ((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getUnidadMedida() + "</font></td> ";
            imprimir +="   <td align=center><font class=Estilo9>" + ((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad() + "</font></td> ";
            imprimir +="   <td align=center><font class=Estilo9>" + ((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidadEntregada() + "</font></td> ";
            imprimir +="   <td align=center><font class=Estilo9>" + this.devolverObservacion(((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getObservacion()) + " </font></td> ";
            imprimir += "  </tr> ";
        }
        imprimir +="  </table> ";
        imprimir +="  </td> ";
        imprimir +="  </tr> ";
        imprimir +="  </table> ";
        imprimir +="  <br><br> ";
        
        return imprimir;
    }
    
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public String devolverObservacion(String observacion){
        String devolver = "";
        if(observacion == null || observacion.trim().length() == 0){
            observacion = "Sin observaci�n";
        }
        devolver = observacion.replace("\n"," ");
        devolver = devolver.replace("\r"," ");
        devolver = devolver.replace("\t"," ");
        devolver = devolver.replace("/"," ");
        devolver = devolver.replace('"',' ');
        devolver = devolver.replace('�',' ');
        devolver = devolver.replace("'"," ");
        //devolver = devolver.replace("$"," ");
        return devolver;
    }
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public String devolverDescripcion(String observacion){
        String devolver = "";
        if(observacion == null || observacion.trim().length() == 0){
            observacion = "Sin descripci�n";
        }
        devolver = observacion.replace("\n"," ");
        devolver = devolver.replace("\r"," ");
        devolver = devolver.replace("\t"," ");
        devolver = devolver.replace("/"," ");
        devolver = devolver.replace('"',' ');
        devolver = devolver.replace('�',' ');
        devolver = devolver.replace("'"," ");
        //devolver = devolver.replace("$"," ");
        return devolver;
    }
    //----------------------------------------------------------------------------------------------------------------------
    public int[] paginacion(){
        
        
        int e = 0;
        int[] largo = null;
        //System.out.println(e/25);
        //System.out.println("%:" + e%25);
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "  select count(Codigo) from MAC.dbo.ComprasRequeridas where verificar <> 3 ";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                e = rs.getInt(1);
            }
            
            
            if(e%25 > 0){
                if(e >= 25){
                    largo = new int[(e/25)+1];
                    for(int i=0;i<(e/25)+1;i++){
                        // System.out.println(i);
                        if(i == (e/25)){
                            largo[i] += largo[i-1]+e%25;
                        }else{
                            largo[i] += 25*(i+1);
                        }
                    }
                }else{
                    largo = new int[1];
                    largo[0] = e;
                }
                if(largo.length > 0){
                    for(int i=0;i<largo.length;i++){
                        // System.out.println(largo[i]);
                    }
                }else{
                    // System.out.println(largo[0]);
                }
                
                
            }else{
                if(e >= 25){
                    largo = new int[(e/25)];
                    for(int i=0;i<(e/25);i++){
                        // System.out.println(i);
                        if(i == (e/25)){
                            largo[i] += largo[i-1]+e%25;
                        }else{
                            largo[i] += 25*(i+1);
                        }
                    }
                }else{
                    largo = new int[1];
                    largo[0] = e;
                }
                if(largo.length > 0){
                    for(int i=0;i<largo.length;i++){
                        //  System.out.println(largo[i]);
                    }
                }else{
                    // System.out.println(largo[0]);
                }
                
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception ex){
            try{
                rs.close();
                st.close();
                conMAC.close();
            }catch(Exception  exc){
                
            }
        }
        return largo;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public boolean actualizarPedidoMateriales(String codProd,String linea,String nuevoCodProd,String numDoc,String um,String comentario, String usus, String num, String tipo){
        boolean estado = false;
        try{
            conMAC = myConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
            String update = " update SiaSchaffner.dbo.STO_MOVDET  "
                    +" set  MVD_CodProd = '" + nuevoCodProd + "' ,PMP = '" + codProd + "', MVD_LineaOrigen = 2, MVD_UMAlternativa = '" + um + "', MVD_TipoProd = " + Integer.parseInt(tipo) +  " "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 8 and "
                    +" MVD_NumeroDoc =  '" + numDoc + "' and MVD_CodProd = '" + codProd + "' and MVD_Linea = '" + linea + "' ";
            st.execute(update);
            String update1 = "";
            update1 = " update MAC.dbo.totalEquiposPorPedido set usuIngModi = '" + usus + "'   where numero = '" + num + "'";
            st.execute(update1);
            estado = true;
            conMAC.commit();
            conMAC.setAutoCommit(true);
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " actualizarPedidoMateriales() "+e.getMessage(),0);
        }
        return estado;
    }
    
//---------------------------------------------------------------------------------------------------------------------------
   /* public Vector historialVales(String supervisor){
        // Vector historialVales = new Vector();
        Vector vales = new Vector();
        Vector devoluciones = new Vector();
        Vector equipos = new Vector();
        Vector estructura = new Vector();
        String  pedidos = "";
        try{
            conMAC = mConexion.getMACConnector()
            st = conMAC.createStatement();
    
            //trae los vales que se han emitido de los correspondientes pedidos
    
            String query1 = " select sum(MVD_CantAsignada),MVD_CodProd,mv.PMP "
                    +" ,(select PRO_DESC from SiaSchaffner.dbo.STO_PRODUCTO where PRO_CODPROD = MVD_CodProd) as descri "
                    +" from SiaSchaffner.dbo.STO_MOVDET mv  ,SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVD_CodSistema=8 and MVD_CodClase =1 AND MVD_TipoDoc = 8 and "
                    +" MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and "
                    +" MVD_NumeroDoc = MVE_NumeroDoc "
                    +" and MVE_FolioFisico in(select distinct(pp.numero) "
                    +"  from MAC.dbo.totalEquiposPorPedido pp "
                    +"  where usuAprobacion = '" + supervisor + "' and status >= 5 ) and MVD_NumeroLote = 0 "
                    +" group by MVD_CodProd,mv.PMP "
                    +" having sum(MVD_CantAsignada) > 0  ";
            rs = st.executeQuery(query1);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCantidad(rs.getFloat(1));
                estruc.setCodigoMatPrima(rs.getString(2));
                estruc.setCodigoProducto(rs.getString(3));
                estruc.setDescripcion(rs.getString(4));
                vales.addElement(estruc);
            }
            rs.close();
            //devoluciones
    
    
            String query2 = " select sum(MVD_Cant),MVD_CodProd "
                    +" from SiaSchaffner.dbo.STO_MOVENC ,SiaSchaffner.dbo.STO_MOVDET "
                    +" where MVD_CodSistema=8 and  MVD_CodClase =3 AND  MVD_TipoDoc = 3 and "
                    +" MVE_CodSistema=8 and  MVE_CodClase =3 AND  MVE_TipoDoc = 3 and "
                    +" MVD_NumeroDoc = MVE_NumeroDoc "
                    +" and mve_analisisadic8 = (select idUsuario from MAC.dbo.TUsuario where login = '" + supervisor + "') and MVD_NumeroLote = 0 "
                    +" group by MVD_CodProd ";
            rs = st.executeQuery(query2);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCantidad(rs.getFloat(1));
                estruc.setCodigoMatPrima(rs.getString(2));
                devoluciones.addElement(estruc);
            }
            rs.close();
            // equipos con marcaje para descontar materiales
            String query3 = " select distinct(pp.np),pp.linea,pp.area,tp.Codpro, "
                    +" (select count(ta.Pedido) from ENSchaffner.dbo.TAvance ta where tp.Pedido = ta.Pedido and tp.Linea = ta.Linea and pp.area = ta.Semielaborado) "
                    +" as Marcaje,tp.Familia "
                    +" from MAC.dbo.totalEquiposPorPedido pp,ENSchaffner.dbo.TPMP tp "
                    +" where usuAprobacion = '" + supervisor + "' and status >= 5 and pp.np = tp.Pedido and pp.linea = tp.Linea "
                    +" and (select count(ta.Pedido) from ENSchaffner.dbo.TAvance ta where tp.Pedido = ta.Pedido and tp.Linea = ta.Linea and pp.area = ta.Semielaborado)  > 0 ";
            rs = st.executeQuery(query3);
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setCodPro(rs.getString(4));
                equipo.setArea(rs.getString(3));
                equipo.setFamilia(rs.getString(6));
                equipos.addElement(equipo);
            }
            rs.close();
            // materiales de estructura para descontar a los procesos terminados
            for(int i=0;i<equipos.size();i++){
                if(((Equipo)equipos.elementAt(i)).getFamilia().equals("CELDA")){
                    String query4 = " select CantidadMP,CodigoMatPrima from ENSchaffner.dbo.MEstructura where CodigoTerminado = '" + ((Equipo)equipos.elementAt(i)).getCodPro() + "' ";
                    rs = st.executeQuery(query4);
                    while(rs.next()){
                        EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                        estruc.setCantidad(rs.getFloat(1));
                        estruc.setCodigoMatPrima(rs.getString(2));
                        estructura.addElement(estruc);
                        //System.out.println(rs.getFloat(1) + "---" + rs.getString(2));
                    }
                    rs.close();
                }else{
                    String query4 = " select CantidadMP,CodigoMatPrima from ENSchaffner.dbo.MEstructura where CodigoTerminado = '" + ((Equipo)equipos.elementAt(i)).getCodPro() + "' and CodigoSemiElaborado = '" + ((Equipo)equipos.elementAt(i)).getArea() + "'";
                    rs = st.executeQuery(query4);
                    while(rs.next()){
                        EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                        estruc.setCantidad(rs.getFloat(1));
                        estruc.setCodigoMatPrima(rs.getString(2));
                        estructura.addElement(estruc);
                        //System.out.println(rs.getFloat(1) + "---" + rs.getString(2));
                    }
                    rs.close();
                }
    
            }
    
            for(int i=0;i<vales.size();i++){
                for(int a=0;a<devoluciones.size();a++){
                    if(((EstructuraPorEquipo)vales.elementAt(i)).getCodigoMatPrima().equals(((EstructuraPorEquipo)devoluciones.elementAt(a)).getCodigoMatPrima())
                    || ((EstructuraPorEquipo)vales.elementAt(i)).getCodigoProducto().equals(((EstructuraPorEquipo)devoluciones.elementAt(a)).getCodigoMatPrima()
                    )){
                        ((EstructuraPorEquipo)vales.elementAt(i)).setCantidad(((EstructuraPorEquipo)vales.elementAt(i)).getCantidad() - ((EstructuraPorEquipo)devoluciones.elementAt(a)).getCantidad());
                    }
                }
            }
            for(int i=0;i<vales.size();i++){
                for(int a=0;a<estructura.size();a++){
                    if(((EstructuraPorEquipo)vales.elementAt(i)).getCodigoMatPrima().equals(((EstructuraPorEquipo)estructura.elementAt(a)).getCodigoMatPrima())
                    || ((EstructuraPorEquipo)vales.elementAt(i)).getCodigoProducto().equals(((EstructuraPorEquipo)estructura.elementAt(a)).getCodigoMatPrima()
                    )){
                        ((EstructuraPorEquipo)vales.elementAt(i)).setCantidad(((EstructuraPorEquipo)vales.elementAt(i)).getCantidad() - ((EstructuraPorEquipo)estructura.elementAt(a)).getCantidad());
                    }
                }
            }
    
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " historialVales() "+e.getMessage(),0);
        }
        return vales;
    }*/
    //---------------------------------------------------------------------------------------------------------------------------
    public Vector historialVales(String fecha){
        // Vector historialVales = new Vector();
        Vector vales = new Vector();
        Vector devoluciones = new Vector();
        Vector marcaje = new Vector();
        //Vector estructura = new Vector();
        String  pedidos = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            //trae los vales que se han emitido de los correspondientes pedidos
            
            String query1 = " select (sum(MVD_CantAsignada)+isnull(cantidad,0)),MVD_CodProd,isnull(mv.PMP,'0')  as PMP "
                    +" ,(select PRO_DESC from SiaSchaffner.dbo.STO_PRODUCTO where PRO_CODPROD = MVD_CodProd) as descri "
                    +"  from SiaSchaffner.dbo.STO_MOVDET mv "
                    +"  inner join SiaSchaffner.dbo.STO_MOVENC  "
                    +"  on(MVD_NumeroDoc = MVE_NumeroDoc ) "
                    +"  left outer join MAC.dbo.LG_stock_planta "
                    +"  on(MVD_CodProd = codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +"  where MVD_CodSistema=8 and MVD_CodClase =1 AND MVD_TipoDoc = 8 and "
                    +"  MVE_CodSistema=8 and "
                    +"  MVE_CodClase =1 AND "
                    +"  MVE_TipoDoc = 8 and "
                    +"  MVE_FolioFisico in(select distinct(pp.numero) "
                    +"  from MAC.dbo.totalEquiposPorPedido pp "
                    +"  where status >= 5 and fecha > '" + fecha + "') and MVD_NumeroLote = 0 "
                    +"  group by MVD_CodProd,mv.PMP,cantidad "
                    +"  having sum(MVD_CantAsignada) > 0 "
                    +"  order by MVD_CodProd   ";
            rs = st.executeQuery(query1);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCantidad(rs.getFloat(1));
                estruc.setCodigoMatPrima(rs.getString(2));
                estruc.setCodigoProducto(rs.getString(3));
                estruc.setDescripcion(rs.getString(4));
                vales.addElement(estruc);
            }
            rs.close();
            //devoluciones
            
            
            String query2 = " select sum(MVD_Cant),MVD_CodProd "
                    +" from SiaSchaffner.dbo.STO_MOVENC ,SiaSchaffner.dbo.STO_MOVDET "
                    +"  where MVD_CodSistema=8 and  MVD_CodClase =3 AND  MVD_TipoDoc = 3 and "
                    +"  MVE_CodSistema=8 and  MVE_CodClase =3 AND  MVE_TipoDoc = 3 and "
                    +"  MVD_NumeroDoc = MVE_NumeroDoc "
                    +"  and  MVD_NumeroLote = 0 and mve_analisisadic10 is not null "
                    +"  and MVE_FechaDoc > '" + fecha + "' "
                    +"  group by MVD_CodProd "
                    +"  order by MVD_CodProd";
            rs = st.executeQuery(query2);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCantidad(rs.getFloat(1));
                estruc.setCodigoMatPrima(rs.getString(2));
                devoluciones.addElement(estruc);
            }
            rs.close();
            // equipos con marcaje para descontar materiales
            String query3 = " select me.CodigoMatPrima,sum(CantidadMP) "
                    +" from ENSchaffner.dbo.TAvance ta "
                    +"  inner join ENSchaffner.dbo.TPMP tp "
                    +"  on (tp.Pedido = ta.Pedido and tp.Linea = ta.Linea) "
                    +"  inner join ENSchaffner.dbo.MEstructura me "
                    +"  on(tp.Codpro = me.CodigoTerminado) "
                    +"  where ta.FechaInicio > '" + fecha + "' and ta.FechaTermino > '" + fecha  + "' "
                    +"  and me.CodigoSemiElaborado = ta.Semielaborado "
                    +"  group by me.CodigoMatPrima ";
            rs = st.executeQuery(query3);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCodigoMatPrima(rs.getString(1));
                estruc.setCantidad(rs.getFloat(2));
                marcaje.addElement(estruc);
            }
            rs.close();
            
            
            for(int i=0;i<vales.size();i++){
                for(int a=0;a<devoluciones.size();a++){
                    if(((EstructuraPorEquipo)vales.elementAt(i)).getCodigoMatPrima().equals(((EstructuraPorEquipo)devoluciones.elementAt(a)).getCodigoMatPrima())
                    || ((EstructuraPorEquipo)vales.elementAt(i)).getCodigoProducto().equals(((EstructuraPorEquipo)devoluciones.elementAt(a)).getCodigoMatPrima()
                    )){
                        ((EstructuraPorEquipo)vales.elementAt(i)).setCantidad(((EstructuraPorEquipo)vales.elementAt(i)).getCantidad() - ((EstructuraPorEquipo)devoluciones.elementAt(a)).getCantidad());
                    }
                }
            }
            for(int i=0;i<vales.size();i++){
                for(int a=0;a<marcaje.size();a++){
                    if(((EstructuraPorEquipo)vales.elementAt(i)).getCodigoMatPrima().equals(((EstructuraPorEquipo)marcaje.elementAt(a)).getCodigoMatPrima())
                    || ((EstructuraPorEquipo)vales.elementAt(i)).getCodigoProducto().equals(((EstructuraPorEquipo)marcaje.elementAt(a)).getCodigoMatPrima()
                    )){
                        ((EstructuraPorEquipo)vales.elementAt(i)).setCantidad(((EstructuraPorEquipo)vales.elementAt(i)).getCantidad() - ((EstructuraPorEquipo)marcaje.elementAt(a)).getCantidad());
                    }
                }
            }
            
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " historialVales() "+e.getMessage(),0);
        }
        return vales;
    }
    
    public void stockPlanta(String fecha){
        String codigo = "";
        Vector stockPlanta = historialVales(fecha);
        int count = 0;
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            stm = conMAC.createStatement();
            for(int i=0;i<stockPlanta.size();i++){
                if(((EstructuraPorEquipo)stockPlanta.elementAt(i)).getCodigoProducto().length() == 10){
                    codigo = ((EstructuraPorEquipo)stockPlanta.elementAt(i)).getCodigoProducto();
                }else {
                    codigo = ((EstructuraPorEquipo)stockPlanta.elementAt(i)).getCodigoMatPrima();
                }
                boolean estado = codigoStockPlanta(codigo);
                if(estado){
                    byte valor = actualizarStockPlanta(((EstructuraPorEquipo)stockPlanta.elementAt(i)).getCantidad(),codigo);
                    if(valor == 0){
                        break;
                    }
                }else{
                    byte valor = insertarStockPlanta(((EstructuraPorEquipo)stockPlanta.elementAt(i)).getCantidad(),codigo);
                    if(valor == 0){
                        break;
                    }
                }
                count++;
            }
            
            if(count == stockPlanta.size()){
                short esta = updateFechaStockPlanta();
                if(esta > 0){
                    conMAC.setAutoCommit(true);
                    conMAC.commit();
                    new Loger().logger("MrpBean.class " , " registros stock de planta " + count + " registros vales " + stockPlanta.size(),1);
                }else{
                    conMAC.rollback();
                    new Loger().logger("MrpBean.class " , " no se actualizo la tabla ",1);
                }
                
            }else{
                conMAC.rollback();
                new Loger().logger("MrpBean.class " , " no se actualizo la tabla ",1);
            }
            //conMAC.setAutoCommit(true);
            //mConexion.cerrarConMAC();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " stockPlanta() "+e.getMessage(),0);
        } finally{
            try{
                conMAC.setAutoCommit(true);
                stm.close();
                mConexion.cerrarConMAC();
            }catch(Exception e){
                mConexion.cerrarConMAC();
                new Loger().logger("MrpBean.class " , " stockPlanta() "+e.getMessage(),0);
            }
        }
    }
    public byte actualizarStockPlanta(float cantidad,String codigo) throws SQLException {
        byte estado = 0;
        try{
            if(cantidad > 0){
                String update = "UPDATE MAC.dbo.LG_stock_planta SET cantidad = " + cantidad + " "
                        +" WHERE codigo = '" + codigo + "' ";
                estado = (byte)stm.executeUpdate(update);
            }else{
                String update = "UPDATE MAC.dbo.LG_stock_planta SET cantidad = 0 "
                        +" WHERE codigo = '" + codigo + "' ";
                estado = (byte)stm.executeUpdate(update);
            }
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " actualizarStockPlanta() "+e.getMessage(),0);
        }
        return estado;
    }
    public byte insertarStockPlanta(float cantidad,String codigo) throws SQLException {
        byte estado = 0;
        try{
            if(cantidad > 0){
                String update = "INSERT INTO MAC.dbo.LG_stock_planta (codigo,cantidad,fecha) values('" + codigo + "'," + cantidad + ",getdate()) ";
                estado = (byte)stm.executeUpdate(update);
            }else{
                String update = "INSERT INTO MAC.dbo.LG_stock_planta (codigo,cantidad,fecha) values('" + codigo + "',0,getdate()) ";
                estado = (byte)stm.executeUpdate(update);
            }
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " insertarStockPlanta() "+e.getMessage(),0);
        }
        return estado;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public boolean codigoStockPlanta(String codProd) throws SQLException {
        boolean estado = false;
        try{
            
            
            String query = "  select distinct(cantidad) from MAC.dbo.LG_stock_planta where  codigo = '" + codProd + "'";
            rs = stm.executeQuery(query);
            while(rs.next()){
                estado = true;
            }
            rs.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " codigoStockPlanta() " + e.getMessage(),0);
        }
        return estado;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public Vector detallePorMaterialVales(String codProd){
        Vector detallePorMaterialVales = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "  select distinct(MVD_CantAsignada),MVD_CodProd,MVE_FolioFisico,MVD_Linea,enc.UsCreacion "
                    +" ,(select PRO_DESC from SiaSchaffner.dbo.STO_PRODUCTO where PRO_CODPROD = MVD_CodProd) as descri "
                    +"  ,area,convert(datetime,MVD_Fecha) "
                    +"  from SiaSchaffner.dbo.STO_MOVDET det ,SiaSchaffner.dbo.STO_MOVENC enc,MAC.dbo.totalEquiposPorPedido "
                    +"  where MVD_CodSistema=8 and MVD_CodClase =1 AND MVD_TipoDoc = 8 and "
                    +"  MVE_CodSistema=8 and "
                    +"  MVE_CodClase =1 AND "
                    +"  MVE_TipoDoc = 8 and "
                    +"  MVD_NumeroDoc = MVE_NumeroDoc  "
                    +"  and MVE_FolioFisico = numero and vales = 0 and status >= 5 and MVD_CodProd = '" + codProd + "' ";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCantidad(rs.getFloat(1));
                estruc.setCodigoMatPrima(rs.getString(2));
                estruc.setNumPedido(rs.getLong(3));
                estruc.setLinea(rs.getInt(4));
                estruc.setUsuario(rs.getString(5));
                estruc.setDescripcion(rs.getString(6));
                estruc.setProceso(rs.getString(7));
                estruc.setFechaConsumo(rs.getDate(8));
                detallePorMaterialVales.addElement(estruc);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " detallePorMaterialVales() "+e.getMessage(),0);
        }
        return detallePorMaterialVales;
    }
    
    //---------------------------------------------------------------------------------------------------------------------------
    public Vector usuariosVales(){
        Vector usuariosVales = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "  select distinct(usuario) from  totalEquiposPorPedido ";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setUsuario(rs.getString(1));
                usuariosVales.addElement(estruc);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " usuariosVales() "+e.getMessage(),0);
        }
        return usuariosVales;
    }
    //---------------------------------------------------------------------------------------------------------------------------
    public Vector supervisorVales(){
        Vector supervisorVales = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " select Usuario from MAC.dbo.LG_permiso_a_link where Nombre = 'supervisorVales' ";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setUsuario(rs.getString(1));
                supervisorVales.addElement(estruc);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " supervisorVales() "+e.getMessage(),0);
        }
        return supervisorVales;
    }
//---------------------------------------------------------------------------------------------------------------------------
    public String nombreUsuario(String login){
        String nombreUsuario = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " select nombre + ' ' +apellido from MAC.dbo.TUsuario  where login = '" + login + "'";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                nombreUsuario = rs.getString(1);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " nombreUsuario() "+e.getMessage(),0);
        }
        return nombreUsuario;
    }
    
    
//--------------------------------------------------------------------------------------------------------------------------------
    public boolean descartarMaterialesValeConsumo(String codProd,String supervisor){
        boolean estado = false;
        try{
            conMAC = myConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
            //descartar materiales de devoluciones al analisis de vales de consumo
            String update = " update SiaSchaffner.dbo.STO_MOVDET  set MVD_NumeroLote = '1' "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =3 AND "
                    +" MVD_TipoDoc = 3 and "
                    +" MVD_NumeroDoc in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC  "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =3 AND "
                    +" MVE_TipoDoc = 3 and mve_analisisadic8 =  (select idUsuario from MAC.dbo.TUsuario where login = '" + supervisor + "')) "
                    +" and MVD_CodProd = '" + codProd + "' ";
            st.execute(update);
            // descartar materiales de pedido de materiales al analisis de vales de consumo
            String update1 = " update SiaSchaffner.dbo.STO_MOVDET  set MVD_NumeroLote = '1' "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 8 and "
                    +" MVD_NumeroDoc in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC  "
                    +" where MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and MVE_FolioFisico in(select numero from MAC.dbo.totalEquiposPorPedido where status >= 5 and usuAprobacion = '" + supervisor + "')) "
                    +" and MVD_CodProd = '" + codProd + "' ";
            st.execute(update1);
            estado = true;
            conMAC.commit();
            conMAC.setAutoCommit(true);
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " descartarMaterialesValeConsumo() "+e.getMessage(),0);
        }
        return estado;
    }
    //---------------------------------------------------------------------------------------------------------------------------
    public Vector materialesPorEquipo(String estructura,String equipo,String linea){
        Vector materialesPorEquipo = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
//            String query = "select me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS,PRO_DESC,me.UnimedMP COLLATE SQL_Latin1_General_CP850_CS_AS,sum(Costo),sum(me.CantidadMP) "
//                    +"  ,(select sum(MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET "
//                    +"  where(me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = MVD_CodProd and MVD_CodSistema = 8 and MVD_CodClase= 1 and MVD_TipoDoc = 8 "
//                    +"  and MVD_NumeroDoc  in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC where MVE_CodSistema = 8 and MVE_CodClase= 1 and MVE_TipoDoc = 8 "
//                    +"  and MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where  np = '" + equipo + "' and linea = '" + linea + "')))) , "
//                    +"  (isnull(Costo,0) * isnull(sum(me.CantidadMP),0)) as psptoMonto, "
//                    +"  (isnull(Costo,0) *(select sum(MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET "
//                    +"  where(me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = MVD_CodProd and MVD_CodSistema = 8 and MVD_CodClase= 1 and MVD_TipoDoc = 8 "
//                    +"  and MVD_NumeroDoc  in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC where MVE_CodSistema = 8 and MVE_CodClase= 1 and MVE_TipoDoc = 8 "
//                    +"  and MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where  np = '" + equipo + "' and linea = '" + linea + "'))))) as realMonto,Costo "
//                    +"  from  ENSchaffner.dbo.TPMP "
//                    +"  inner join ENSchaffner.dbo.MEstructura me "
//                    +"  on(Codpro = me.CodigoTerminado) "
//                    +"  inner join SiaSchaffner.dbo.STO_PRODUCTO "
//                    +"  on(PRO_CODPROD = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS) "
//                    +"  left outer join ENSchaffner.dbo.SCHCup cup "
//                    +"  on(cup.Codpro = me.CodigoMatPrima) "
//                    +"  where Pedido =  '" + equipo + "'  AND Linea = '" + linea + "' "
//                    +"  group by me.CodigoMatPrima,PRO_DESC,me.UnimedMP,Costo "
//                    +"  union all "
//                    +"  select MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,Costo,0 as CantPresu,MVD_CantAsignada as cantReal "
//                    +"  ,0 as realPresu,isnull(MVD_CantAsignada,0)*isnull(Costo,0) as realMonto,Costo "
//                    +"  from SiaSchaffner.dbo.STO_MOVDET "
//                    +"  left outer join ENSchaffner.dbo.MEstructura "
//                    +"  on  (CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS= MVD_CodProd  and CodigoTerminado = '" + estructura + "') "
//                    +"  left outer join ENSchaffner.dbo.SCHCup "
//                    +"  on(MVD_CodProd = Codpro COLLATE SQL_Latin1_General_CP850_CS_AS) "
//                    +"  inner join SiaSchaffner.dbo.STO_PRODUCTO "
//                    +"  on(PRO_CODPROD = MVD_CodProd) "
//                    +"  where  MVD_CodSistema=8 and "
//                    +"   MVD_CodClase =1 AND "
//                    +"   MVD_TipoDoc = 8 and "
//                    +"   MVD_NumeroDoc  in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC where MVE_CodSistema = 8 and MVE_CodClase= 1 and MVE_TipoDoc = 8 "
//                    +"   and MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where  np = '" + equipo + "' and linea = '" + linea + "')) "
//                    +"  and CodigoMatPrima is null and MVD_CantAsignada > 0";
            String query  = " select me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS,PRO_DESC,me.UnimedMP COLLATE SQL_Latin1_General_CP850_CS_AS,sum(Costo),sum(me.CantidadMP)  ";
            query += " ,(select sum(MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET ";
            query += " inner join SiaSchaffner.dbo.STO_MOVENC ";
            query += " on(MVD_NumeroDoc = MVE_NumeroDoc and MVE_CodSistema = 8 and MVE_CodClase= 1 and MVE_TipoDoc = 8) ";
            query += " where(me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = MVD_CodProd and MVD_CodSistema = 8 and MVD_CodClase= 1 and MVD_TipoDoc = 8  ";
            query += " and MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where  np = '" + equipo + "' and linea = '" + linea + "'))) , ";
            query += " (isnull(Costo,0) * isnull(sum(me.CantidadMP),0)) as psptoMonto, ";
            query += " (isnull(Costo,0) *( ";
            query += " select sum(MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET ";
            query += " inner join SiaSchaffner.dbo.STO_MOVENC ";
            query += " on(MVD_NumeroDoc = MVE_NumeroDoc and MVE_CodSistema = 8 and MVE_CodClase = 1 and MVE_TipoDoc = 8) ";
            query += " where(me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = MVD_CodProd and MVD_CodSistema = 8 and MVD_CodClase= 1 and MVD_TipoDoc = 8 ";
            query += " and MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where  np = '" + equipo + "' and linea = '" + linea + "')))) as realMonto ";
            query += " ,Costo ";
            query += " from  ENSchaffner.dbo.TPMP ";
            query += " inner join ENSchaffner.dbo.MEstructura me ";
            query += " on(Codpro = me.CodigoTerminado) ";
            query += " inner join SiaSchaffner.dbo.STO_PRODUCTO ";
            query += " on(PRO_CODPROD = me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS) ";
            query += " left outer join ENSchaffner.dbo.SCHCup cup ";
            query += " on(cup.Codpro = me.CodigoMatPrima) ";
            query += " where Pedido =  '" + equipo + "'  AND Linea = '" + linea + "' ";
            query += " group by me.CodigoMatPrima,PRO_DESC,me.UnimedMP,Costo ";
            query += " union all ";
            query += " select MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,Costo,0 as CantPresu,MVD_CantAsignada as cantReal ";
            query += " ,0 as realPresu,isnull(MVD_CantAsignada,0)*isnull(Costo,0) as realMonto,Costo ";
            query += " from SiaSchaffner.dbo.STO_MOVDET ";
            query += " inner join SiaSchaffner.dbo.STO_MOVENC ";
            query += " on(MVD_NumeroDoc = MVE_NumeroDoc and MVE_CodSistema = 8 and MVE_CodClase= 1 and MVE_TipoDoc = 8) ";
            query += " left outer join ENSchaffner.dbo.MEstructura ";
            query += " on  (CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS= MVD_CodProd  and CodigoTerminado = '" + estructura + "') ";
            query += " left outer join ENSchaffner.dbo.SCHCup ";
            query += " on(MVD_CodProd = Codpro COLLATE SQL_Latin1_General_CP850_CS_AS) ";
            query += " inner join SiaSchaffner.dbo.STO_PRODUCTO ";
            query += " on(PRO_CODPROD = MVD_CodProd) ";
            query += " where  MVD_CodSistema=8 and ";
            query += " MVD_CodClase =1 AND ";
            query += " MVD_TipoDoc = 8 and ";
            query += " MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where  np = '" + equipo + "' and linea = '" + linea + "') ";
            query += " and CodigoMatPrima is null and MVD_CantAsignada > 0  ";
            
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCodigoMatPrima(rs.getString(1));
                estruc.setDescripcion(rs.getString(2));
                estruc.setUnidadMedida(rs.getString(3));
                estruc.setCantidad(rs.getFloat(4));
                estruc.setPresupuesto(rs.getFloat(5));
                estruc.setReal(rs.getFloat(6));
                estruc.setDiferencia(rs.getFloat(5)-rs.getFloat(6));
                estruc.setPresupuesto1(rs.getFloat(7));
                estruc.setReal1(rs.getFloat(8));
                estruc.setDiferencia1((rs.getFloat(9)*rs.getFloat(5))-(rs.getFloat(9)*rs.getFloat(6)));
                materialesPorEquipo.addElement(estruc);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " materialesPorEquipo() "+e.getMessage(),0);
        }
        return materialesPorEquipo;
    }
//---------------------------------------------------------------------------------------------------------------------------
    public Vector controlDeCostoPorMateriales(String fechaIni,String fechaFin){
        Vector materialesPorEquipo = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            /*String query = " select MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,sum(MVD_CantAsignada) as cantidadReal, "
                          +"  (select sum(CantidadMP) "
                          +"  from ENSchaffner.dbo.MEstructura "
                          +"  inner join ENSchaffner.dbo.TPMP "
                          +"  on(Codpro = CodigoTerminado) "
                          +"  where convert(varchar,Pedido)+convert(varchar,Linea) in "
                          +"  (select distinct(convert(varchar,np)+convert(varchar,linea)) from MAC.dbo.totalEquiposPorPedido where fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "') "
                          +"  and CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = MVD_CodProd "
                          +"  ) as cantPresupuestada,Costo "
                          +"  from SiaSchaffner.dbo.STO_MOVDET "
                          +"  left outer join ENSchaffner.dbo.SCHCup "
                          +"  on(MVD_CodProd = Codpro COLLATE SQL_Latin1_General_CP850_CS_AS) "
                          +"  left outer join SiaSchaffner.dbo.STO_PRODUCTO "
                          +"  on(MVD_CodProd = PRO_CODPROD) "
                          +"       where MVD_CodSistema=8 and "
                          +"      MVD_CodClase =1 AND "
                          +"      MVD_TipoDoc = 8 and "
                          +"      MVD_CantAsignada > 0 and "
                          +"      MVD_NumeroDoc in (select MVE_NumeroDoc from   SiaSchaffner.dbo.STO_MOVENC "
                          +"     where MVE_CodSistema=8 and "
                          +"     MVE_CodClase =1 AND "
                          +"      MVE_TipoDoc = 8 and MVE_FolioFisico in (select distinct(numero) from MAC.dbo.totalEquiposPorPedido where fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "')) "
                          +"  group by MVD_CodProd,Costo,PRO_DESC,PRO_UMPRINCIPAL ";
             */
            String query = " select vw1.MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,sum(vw1.MVD_Cant) as cantidadReal "
                    +" ,(select sum(CantidadMP) from ENSchaffner.dbo.MEstructura  "
                    +"  where CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = vw1.MVD_CodProd and CodigoTerminado in ( "
                    +"  select distinct(Codpro) from SiaSchaffner.dbo.vw_pedMaterialesTPMP tp where tp.MVE_FolioFisico in(select distinct(enc.MVE_FolioFisico) "
                    +"   from SiaSchaffner.dbo.vw_pedidoDeMateriales enc "
                    +"  where MVD_CodProd = vw1.MVD_CodProd and  enc.MVE_FolioFisico in "
                    +"  (select distinct(numero) "
                    +"  from MAC.dbo.totalEquiposPorPedido  "
                    +"  where fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "'))) ) as cantPpda,cup.Costo as Costo "
                    +"  from SiaSchaffner.dbo.vw_valesDeConsumo vw1 "
                    +"  left outer join SiaSchaffner.dbo.STO_PRODUCTO "
                    +"  on(vw1.MVD_CodProd = PRO_CODPROD) "
                    +"  left outer join ENSchaffner.dbo.SCHCup cup "
                    +"  on(vw1.MVD_CodProd = cup.Codpro COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +"  where vw1.MVD_NumeroDocOrigen in "
                    +"  ( select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC "
                    +"  where MVE_CodSistema= 8 and "
                    +"  MVE_CodClase = 1 AND "
                    +"  MVE_TipoDoc = 8 and MVE_FolioFisico in "
                    +"  (select distinct(numero) "
                    +"  from MAC.dbo.totalEquiposPorPedido "
                    +"  where fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "')) "
                    +"  group by vw1.MVD_CodProd,PRO_DESC,PRO_UMPRINCIPAL,cup.Costo ";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCodigoMatPrima(rs.getString(1));
                estruc.setDescripcion(rs.getString(2));
                estruc.setUnidadMedida(rs.getString(3));
                estruc.setReal(rs.getFloat(4));
                estruc.setPresupuesto(rs.getFloat(5));
                estruc.setReal1(rs.getFloat(4)*rs.getFloat(6));
                estruc.setPresupuesto1(rs.getFloat(5)*rs.getFloat(6));
                estruc.setCantidad(rs.getFloat(6));
                materialesPorEquipo.addElement(estruc);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " controlDeCostoPorMateriales() "+e.getMessage(),0);
        }
        return materialesPorEquipo;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    public Vector detalleControlPorMaterial(String fechaIni,String fechaFin,String material){
        Vector materialesPorEquipo = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " select MVD_CantAsignada,np,linea,grupo,numero "
                    +"  from SiaSchaffner.dbo.STO_MOVDET,SiaSchaffner.dbo.STO_MOVENC "
                    +"  inner join MAC.dbo.totalEquiposPorPedido "
                    +"  on(MVE_FolioFisico = numero) "
                    +"       where MVD_CodSistema=8 and "
                    +"      MVD_CodClase =1 AND "
                    +"      MVD_TipoDoc = 8 and "
                    +"      MVD_CantAsignada > 0 and "
                    +"      MVD_NumeroDoc = MVE_NumeroDoc and "
                    +"      MVE_CodSistema=8 and "
                    +"      MVE_CodClase =1 AND "
                    +"      MVE_TipoDoc = 8 and MVE_FolioFisico in (select distinct(numero) from MAC.dbo.totalEquiposPorPedido where fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "') "
                    +"  and MVD_CodProd = '" + material + "' "
                    +"  order by MVD_CodProd ";
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setReal(rs.getFloat(1));
                estruc.setNumDoc(rs.getLong(2));
                estruc.setLinea(rs.getInt(3));
                if(rs.getInt(4) == 2){
                    estruc.setProceso("SI");
                }else{
                    estruc.setProceso("NO");
                }
                estruc.setNumPedido(rs.getLong(5));
                materialesPorEquipo.addElement(estruc);
            }
            
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " detalleControlPorMaterial() "+e.getMessage(),0);
        }
        return materialesPorEquipo;
    }
//---------------------------------------------------------------------------------------------------------------------------
    public Vector buscarPorDescripcion(String descri){
        Vector buscar = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = "select PRO_CODPROD,PRO_UMPRINCIPAL,PRO_CODTIPO,PRO_DESC from SiaSchaffner.dbo.STO_PRODUCTO where replace(PRO_DESC,'\"\',' ') = '" + descri.replace('"',' ') + "' or PRO_CODPROD = '" + descri + "'";
            //String query = "select PRO_CODPROD,PRO_UMPRINCIPAL,PRO_CODTIPO,PRO_DESC from SiaSchaffner.dbo.STO_PRODUCTO where PRO_DESC = '" + descri + "' or PRO_CODPROD = '" + descri + "'";
            rs = st.executeQuery(query);
            
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setCodigoMatPrima(rs.getString("PRO_CODPROD"));
                estruc.setUnidadMedida(rs.getString("PRO_UMPRINCIPAL"));
                estruc.setTipoProducto(rs.getInt("PRO_CODTIPO"));
                estruc.setDescripcion(rs.getString("PRO_DESC").replace('"',' ').trim());
                buscar.addElement(estruc);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " buscarPorDescripcion() "+e.getMessage(),0);
        }
        return buscar;
    }
//---------------------------------------------------------------------------------------------------------------------
    public byte updateStockPlanta(float cantidad,String codigo,String usuario,String observacion)  {
        
        byte estado = 0;
        
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            String update = "UPDATE MAC.dbo.LG_stock_planta SET cantidad = " + cantidad + " "
                    +" WHERE codigo = '" + codigo + "' ";
            estado = (byte)st.executeUpdate(update);
            if(estado > 0){
                String insert = "  insert into LG_bitacora_stock_planta (Codigo,Usuario,Fecha,Observacion) "
                        +"  values('" + codigo + "','" + usuario + "',getDate(),'" + observacion + "') ";
                estado = (byte)st.executeUpdate(insert);
            }
            if(estado > 0){
                conMAC.setAutoCommit(true);
                conMAC .commit();
            }else{
                conMAC.rollback();
            }
            mConexion.cerrarConMAC();
        }catch(Exception e){
            rollBackMac();
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " updateStockPlanta() "+e.getMessage(),0);
        }
        return estado;
    }
//---------------------------------------------------------------------------------------------------------------------
    public short updateFechaStockPlanta()  {
        short estado = 0;
        try{
            String update = "UPDATE MAC.dbo.LG_stock_planta SET fecha = getDate() ";
            estado = (short)stm.executeUpdate(update);
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " updateFechaStockPlanta() "+e.getMessage(),0);
        }
        return estado;
    }
//---------------------------------------------------------------------------------------------------------------------
   /* public byte updateFechaStockPlantaConn()  {
        byte estado = 0;
        try{
            String update = "UPDATE MAC.dbo.LG_stock_planta SET fecha = getDate() ";
            estado = (byte)st.executeUpdate(update);
            st.close();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " updateFechaStockPlanta() "+e.getMessage(),0);
        }
        return estado;
    }*/
//---------------------------------------------------------------------------------------------------------------------------
    public String buscarFechaStockPlanta(){
        String fecha = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " SELECT year(min(fecha)),month(min(fecha)),day(min(fecha)) "
                    +" FROM MAC.dbo.LG_stock_planta ";
            rs = st.executeQuery(query);
            
            while(rs.next()){
                fecha += rs.getString(1);
                if(rs.getInt(2) < 10){
                    fecha += "0"+rs.getString(2);
                }else{
                    fecha += rs.getString(2);
                }
                if(rs.getInt(3) < 10){
                    fecha += "0"+rs.getString(3);
                }else{
                    fecha += rs.getString(3);
                }
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " buscarFechaStockPlanta() "+e.getMessage(),0);
        }
        return fecha;
    }
    
    //---------------------------------------------------------------------------------------------------------------------------
    public String buscarCodigoStockPlanta(ArrayList datos,String todos,String usuario){
        String devolver = "No fueron actualizados todos los datos";
        int cant = 0;
        short valor1 = 0;
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            for (int i = 0; i < datos.size(); i++) {
                String codigo = "";
                int valor = 0;
                boolean estado1 = false;
                byte state = 0;
                ModAuxiliar fila = (ModAuxiliar) datos.get(i);
                NumberFormat nf = new DecimalFormat(" ");
                if (fila.getNombre0().equals("")) {
                    codigo = nf.format(ModAuxiliar.redondear(fila.getValor0(), 0));
                } else {
                    codigo = fila.getNombre0();
                }
                valor = Integer.parseInt(nf.format(ModAuxiliar.redondear(fila.getValor1(), 0)).trim());
                String query = " select codigo from MAC.dbo.LG_stock_planta where codigo = '" + codigo.trim() + "' ";
                rs = st.executeQuery(query);
                while(rs.next()){
                    estado1 = true;
                }
                rs.close();
                if(estado1){
                    String update = "UPDATE MAC.dbo.LG_stock_planta SET inventario = " + valor + ",estado=1,cantidad= " + valor +" WHERE codigo = '" + codigo.trim() + "' ";
                    state = (byte)st.executeUpdate(update);
                }else{
                    
                    String update = "INSERT INTO MAC.dbo.LG_stock_planta (codigo,inventario,estado,cantidad) values('" + codigo.trim() + "'," + valor +",1," + valor +")";
                    state = (byte)st.executeUpdate(update);
                }
                if(state > 0){
                    cant++;
                }
                nf = null;
            }
            
            if(cant == datos.size()){
                if(todos != null){
                    String update = "UPDATE MAC.dbo.LG_stock_planta SET cantidad = 0, inventario = 0 where estado = 0";
                    valor1= (short)st.executeUpdate(update);
                }
                String update = "UPDATE MAC.dbo.LG_stock_planta SET fecha = getDate(), estado = 0 ";
                valor1= (short)st.executeUpdate(update);
                String insert = "  insert into LG_bitacora_stock_planta (Codigo,Usuario,Fecha,Observacion) "
                        +"  values('Inventario','" + usuario + "',getDate(),'Actualizaci�n por inventario') ";
                valor1= (short)st.executeUpdate(insert);
            }
            if(valor1 > 0){
                conMAC.setAutoCommit(true);
                conMAC.commit();
                devolver  = "Se actualizaron todos los registros";
            }else{
                conMAC.rollback();
            }
        }catch(Exception e){
            rollBackMac();
            new Loger().logger("MrpBean.class " , " buscarCodigoStockPlanta() "+e.getMessage(),0);
        } finally{
            mConexion.cerrarConMAC();
        }
        return devolver;
    }
    public void rollBackMac(){
        try{
            conMAC.rollback();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " rollBackMac() "+e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------------------
    public Vector partesEntrada(String codProd,String fecha)  {
        Vector partesEntrada = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            /*String query = " select MVE_FechaDoc,MVD_Cant,MVD_FactorInventario from SiaSchaffner.dbo.STO_MOVDET  "
                    +" inner join SiaSchaffner.dbo.STO_MOVENC  "
                    +"  on(MVD_NumeroDoc = MVE_NumeroDoc and MVD_CodSistema = MVE_CodSistema and  "
                    +"  MVD_CodClase = MVE_CodClase and MVD_TipoDoc = MVE_TipoDoc) "
                    +"  where MVD_CodProd = '" + codProd + "' and MVD_FactorInventario <> 0 AND MVD_Bodega1 = 11 "
                    +"  order by MVE_FechaDoc,MVD_FactorInventario desc ";*/
            String query  = " select MVE_FolioFisico,MVE_FechaDoc,MVD_Cant,MVD_FactorInventario,DOC_Desc,USE_NombreCompleto,ms.vc_usuario,ms.nb_np,ms.nb_linea,ms.vc_proceso";
            query += " from SiaSchaffner.dbo.STO_MOVDET  mov";
            query += " left join SiaSchaffner.dbo.STO_MOVENC  ";
            query += " on(MVD_NumeroDoc = MVE_NumeroDoc and MVD_CodSistema = MVE_CodSistema and  ";
            query += " MVD_CodClase = MVE_CodClase and MVD_TipoDoc = MVE_TipoDoc) ";
            query += " left join SiaSchaffner.dbo.STO_DICDOC";
            query += " on(DOC_CodSistema = MVD_CodSistema and DOC_CodClase = MVD_CodClase and Doc_TipoDoc = MVD_TipoDoc)";
            query += " left join SiaSchaffner.dbo.SYS_USER";
            query += " on(USE_Codigo = mov.UsCreacion)";
            query += " left outer join MAC2BETA.dbo.VW_movimiento_stock ms";
            query += " on(mov.MVD_NumeroDoc = ms.MVD_NumeroDoc and mov.MVD_Linea = ms.MVD_Linea)";
            query += " where MVD_CodProd = '"+codProd+"' and MVD_FactorInventario <> 0 AND MVD_Bodega1 = 11 ";
            query += " order by MVE_FechaDoc,MVD_FactorInventario desc ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setFechaIn(rs.getString("MVE_FechaDoc"));
                estruc.setCantidad(rs.getFloat("MVD_Cant"));
                estruc.setEstado(rs.getInt("MVD_FactorInventario"));
                estruc.setNumDoc(rs.getLong("MVE_FolioFisico"));
                estruc.setDescripcion(rs.getString("DOC_Desc"));
                estruc.setComprador(rs.getString("USE_NombreCompleto"));
                estruc.setDescripcionEstado(rs.getString("vc_usuario"));
                estruc.setNumPedido(rs.getInt("nb_np"));
                estruc.setLinea(rs.getInt("nb_linea"));
                estruc.setProceso(rs.getString("vc_proceso"));
                partesEntrada.add(estruc);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " partesEntrada() " + e.getMessage(),0);
        }
        return partesEntrada;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public Vector valesDeConsumo(String codProd,String fecha)  {
        Vector valesDeConsumo = new Vector();
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select MVE_FechaDoc,MVD_CantAsignada "
                    +" from SiaSchaffner.dbo.STO_MOVDET mv ,SiaSchaffner.dbo.STO_MOVENC "
                    +" where MVD_CodSistema=8 and MVD_CodClase =1 AND MVD_TipoDoc = 8 and "
                    +" MVE_CodSistema=8 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 8 and "
                    +" MVD_NumeroDoc = MVE_NumeroDoc "
                    +" and MVE_FolioFisico in(select distinct(pp.numero) "
                    +" from MAC.dbo.totalEquiposPorPedido pp "
                    +" where status >= 5 and fecha >= '" + fecha  + "') and MVD_NumeroLote = 0 and (MVD_CodProd = '" + codProd + "' or mv.PMP = '" + codProd + "') and "
                    +" MVD_CantAsignada <> 0 "
                    +" order by MVE_FechaDoc desc";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setFechaIn(rs.getString(1));
                estruc.setCantidad(rs.getFloat(2));
                valesDeConsumo.add(estruc);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " valesDeConsumo() " + e.getMessage(),0);
        }
        return valesDeConsumo;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public Vector devoluciones(String codProd,String fecha)  {
        Vector devoluciones = new Vector();
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select MVE_FechaDoc,MVD_Cant "
                    +"  from SiaSchaffner.dbo.STO_MOVENC ,SiaSchaffner.dbo.STO_MOVDET "
                    +"  where MVD_CodSistema=8 and  MVD_CodClase =3 AND  MVD_TipoDoc = 3 and "
                    +"  MVE_CodSistema=8 and  MVE_CodClase =3 AND  MVE_TipoDoc = 3 and "
                    +"  MVD_NumeroDoc = MVE_NumeroDoc "
                    //+"  and  MVD_NumeroLote = 0 and mve_analisisadic10 is not null "
                    +"  and MVE_FechaDoc >= '" + fecha + "' and MVD_CodProd = '" + codProd + "' "
                    +"  order by MVE_FechaDoc desc";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setFechaIn(rs.getString(1));
                estruc.setCantidad(rs.getFloat(2));
                devoluciones.add(estruc);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " devoluciones() " + e.getMessage(),0);
        }
        return devoluciones;
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public Vector marcaje(String codProd,String fecha)  {
        Vector marcajes = new Vector();
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select ta.FechaTermino,CantidadMP "
                    +"  from ENSchaffner.dbo.TAvance ta "
                    +"  inner join ENSchaffner.dbo.TPMP tp "
                    +"  on (tp.Pedido = ta.Pedido and tp.Linea = ta.Linea) "
                    +"  inner join ENSchaffner.dbo.MEstructura me "
                    +"  on(tp.Codpro = me.CodigoTerminado) "
                    +"  where ta.FechaInicio >= '" + fecha + "' and ta.FechaTermino >= '" + fecha + "' "
                    +"  and me.CodigoSemiElaborado = ta.Semielaborado and me.CodigoMatPrima = '" + codProd + "' "
                    +"  order by ta.FechaTermino desc";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setFechaIn(formateador.format(rs.getDate(1)));
                estruc.setCantidad(rs.getFloat(2));
                marcajes.add(estruc);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " marcaje() " + e.getMessage(),0);
        }
        return marcajes;
    }
    
     /*public static void main(String [] arf){
        new MrpBean().evolucionStock();
    }*/
    
    public Vector evolucionStock(String codigo){
        Vector evolucionStock = new Vector();
        Vector partesEntrada = partesEntrada(codigo,"");
        float stock = 0;
        try{
            
            for(int i=0;i<partesEntrada.size();i++){
                EstructuraPorEquipo  estru = new EstructuraPorEquipo();
                estru.setFechaIn(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getFechaIn());
                estru.setNumDoc(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getNumDoc());
                estru.setDescripcion(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getDescripcion());
                estru.setComprador(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getComprador());
                estru.setDescripcionEstado(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getDescripcionEstado());
                estru.setNumPedido(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getNumPedido());
                estru.setLinea(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getLinea());
                estru.setProceso(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getProceso());
                if(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getEstado() == 1){
                    stock += ((EstructuraPorEquipo)partesEntrada.elementAt(i)).getCantidad();
                    estru.setCantidad(stock);
                    estru.setCantidadConsumo(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getCantidad());
                    //estru.setDescripcion("Entrada");
                }
                if(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getEstado() == -1){
                    stock -= ((EstructuraPorEquipo)partesEntrada.elementAt(i)).getCantidad();
                    estru.setCantidad(stock);
                    estru.setCantidadConsumo(((EstructuraPorEquipo)partesEntrada.elementAt(i)).getCantidad());
                    //estru.setDescripcion("Salida");
                }
                
                evolucionStock.add(estru);
            }
       /*for(int i=0;i<evolucionStock.size();i++){
           System.out.println("Fecha: " +((EstructuraPorEquipo)evolucionStock.elementAt(i)).getFechaIn()+ "---"+"Stock: " +  ((EstructuraPorEquipo)evolucionStock.elementAt(i)).getCantidad() + " " +((EstructuraPorEquipo)evolucionStock.elementAt(i)).getDescripcion()+" " + ((EstructuraPorEquipo)evolucionStock.elementAt(i)).getCantidadConsumo());
       }*/
        }catch(Exception e){
            
        }
        return evolucionStock;
    }
    public String getFechaProgramada(int dias, String fDesde) throws Exception {
        Calendar Chasta = Calendar.getInstance();
        try{
            Chasta = Calendar.getInstance();
            Chasta.set(Integer.parseInt(fDesde.substring(0, 4)),
                    Integer.parseInt(fDesde.substring(4, 6)) - 1,
                    Integer.parseInt(fDesde.substring(6, 8)));
            
            while (dias < 0) {
                Chasta.add(Calendar.DATE, -1);
                dias += 1;
            }
            //System.out.println(fDesde.substring(0, 4));
        }catch(Exception e){
        }
        return verFecha(Chasta.getTime(), "yyyyMMdd");
        //return Chasta.toString();
    }
    
    public String verFecha(java.util.Date fecha, String formato) {
        SimpleDateFormat f = new SimpleDateFormat(formato);
        if (fecha != null) {
            return f.format(fecha);
        } else {
            return "";
        }
    }
    //--------------------------------------------------------------------------------------------------------------------------------------------
    public String fechaEnDias(int dias)  {
        Date fecha = null;
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String select = "select getDate() - " + dias;
            rs = st.executeQuery(select);
            while(rs.next()){
                fecha = rs.getDate(1);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " fechaEnDias() " + e.getMessage(),0);
        }
        return formateador.format(fecha);
    }
//--------------------------------------------------------------------------------------------------------------------------------------------
    public Vector bitacoraStockPlanta(String codProd)  {
        Vector bitacora = new Vector();
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " select Usuario,Fecha,Observacion "
                    +" from MAC.dbo.LG_bitacora_stock_planta "
                    +" where Codigo = '" + codProd + "'  ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
                estruc.setUsuario(rs.getString(1));
                estruc.setFechaCreacion(rs.getDate(2));
                estruc.setObservacion(rs.getString(3));
                bitacora.add(estruc);
            }
            rs.close();
            mConexion.cerrarConMAC();
        }catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " bitacoraStockPlanta() " + e.getMessage(),0);
        }
        return bitacora;
    }
    
    public boolean consumosActualizados(String fechaIni)  {
        boolean consumos = false;
        String anoIni = "";
        String mesIni = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " select min(Ano),substring(convert(varchar,min(convert(int,convert(varchar,Ano)+convert(varchar,Mes)))),5,2) "
                    +" from MAC.dbo.consumosPorMaterial1 ";
            rs = st.executeQuery(query);
            while(rs.next()){
                anoIni = rs.getString(1);
                mesIni = rs.getString(2);
                
            }
            if(mesIni.length() == 1){
                mesIni ="0"+mesIni;
            }
            
            if(fechaIni.substring(0,6).equals(anoIni+mesIni)){
                consumos = true;
            }
            rs.close();
            myConexion.cerrarConMAC();
        }catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class " , " consumosActualizados() " + e.getMessage(),0);
        }
        return consumos;
    }
    
    public void truncarConsumosPromedio(){
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " truncate table MAC.dbo.consumosPorMaterial1";
            st.execute(query);
            String query1 = " truncate table MAC.dbo.devolucionesPorMaterial";
            st.execute(query1);
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," truncarConsumosPromedio " +  e.getMessage(),0);
        }
        
    }
    public Vector consumosPorAno(String fecIni,String fecFin){
        Vector usar = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            /*String query = " select sum(MVD_Cant),year(MVD_Fecha),month(MVD_Fecha),MVD_CodProd "
                    +" from SiaSchaffner.dbo.STO_MOVDET ,SiaSchaffner.dbo.STO_MOVENC enc "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =2 AND "
                    +" MVD_TipoDoc = MVE_TipoDoc and "
                    +" MVD_NumeroDoc = MVE_NumeroDoc and "
                    +" MVE_CodSistema=8 and "
                    +" MVE_CodClase =2 and "
                    +" MVD_Fecha between '" + fecIni + "'  and '" + fecFin + "' "
                    +" group by year(MVD_Fecha),month(MVD_Fecha),MVD_CodProd ";*/
            String query =  " select sum(MVD_Cant),year(MVD_Fecha),month(MVD_Fecha),MVD_CodProd ";
            query += " from SiaSchaffner.dbo.STO_MOVDET ";
            query += " where ";
            query += " MVD_TIPOPROD =1";
            query += " and mvd_FactorInventario = -1";
            query += " and mvd_CodClase <>7";
            query += " and MVD_Fecha between '"+fecIni+"'  and '"+fecFin+"'";
            query += " group by year(MVD_Fecha),month(MVD_Fecha),MVD_CodProd ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCantidad(rs.getFloat(1));
                estructura.setAno(rs.getInt(2));
                estructura.setMes(rs.getInt(3));
                estructura.setCodigoMatPrima(rs.getString(4));
                usar.addElement(estructura);
            }
            rs.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," consumosPorAno() " +  e.getMessage(),0);
        }
        
        return usar;
    }
    //--------------------------------------------------------------------------------------------------------------------
    public void desconectarMac(){
        try{
            conMAC.setAutoCommit(true);
            conMAC.commit();
            st.close();
            conMAC.close();
        }catch(Exception e){}
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    public void conectarMac(){
        try{
            
            conMAC = myConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
        }catch(Exception e){
            new Loger().logger("MrpBean.class: "," conectarMac() " +  e.getMessage(),0);
        }
    }
    
    public boolean insertarConsumosPromedio(float cantidad, int ano, int mes, String codigo ){
        boolean estado = false;
        try{
            String query = " insert into MAC.dbo.consumosPorMaterial1(Cantidad,Ano,Mes,Codigo) values (" + cantidad + "," + ano + "," + mes + ",'" + codigo + "')";
            if(st.executeUpdate(query) > 0){
                estado = true;
            }
        } catch(Exception e){
            new Loger().logger("MrpBean.class: "," insertarConsumosPromedio " +  e.getMessage(),0);
        }
        return estado;
    }
    
    public boolean insertarDevolucionesPromedio(float cantidad, int ano, int mes, String codigo ){
        boolean estado = false;
        try{
            String query = " insert into MAC.dbo.devolucionesPorMaterial(Cantidad,Ano,Mes,Codigo) values (" + cantidad + "," + ano + "," + mes + ",'" + codigo + "')";
            if(st.executeUpdate(query) > 0){
                estado = true;
            }
        } catch(Exception e){
            new Loger().logger("MrpBean.class: "," insertarDevolucionesPromedio " +  e.getMessage(),0);
        }
        return estado;
    }
    public Vector devolucionesPorAno(String fecIni,String fecFin){
        Vector usar = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            
            
            String query = " select sum(MVD_Cant),year(MVD_Fecha),month(MVD_Fecha),MVD_CodProd "
                    +" from SiaSchaffner.dbo.STO_MOVDET ,SiaSchaffner.dbo.STO_MOVENC enc "
                    +" where MVD_CodSistema=8 and "
                    +" MVD_CodClase =3 AND "
                    +" MVD_TipoDoc = 3 and "
                    +" MVD_NumeroDoc = MVE_NumeroDoc and "
                    +" MVE_CodSistema=8 and "
                    +" MVE_CodClase =3 and "
                    +" MVE_TipoDoc = 3 "
                    +" and MVD_Fecha between '" + fecIni + "'  and '" + fecFin + "' "
                    //+" and MVD_CodProd = '1051201017' "
                    +" group by year(MVD_Fecha),month(MVD_Fecha),MVD_CodProd "
                    +" order by year(MVD_Fecha),month(MVD_Fecha) ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCantidad(rs.getFloat(1));
                estructura.setAno(rs.getInt(2));
                estructura.setMes(rs.getInt(3));
                estructura.setCodigoMatPrima(rs.getString(4));
                usar.addElement(estructura);
            }
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("MrpBean.class: "," devolucionesPorAno() " +  e.getMessage(),0);
        }
        
        return usar;
    }
    public void eliminarAsociado(String codigo){
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " update MAC.dbo.ProductoAnexo set asociado = 0 where Codigo = '" + codigo + "' ";
            st.execute(query);
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," eliminarAsociado() " +  e.getMessage(),0);
        }
        
    }
    public Vector conjuntosAsociados(){
        Vector usar = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select distinct(asociado)  FROM MAC.dbo.ProductoAnexo ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                if(rs.getString(1) != null){
                    estructura.setConjunto(rs.getString(1));
                }else{
                    estructura.setConjunto("Sin conjunto");
                }
                
                usar.addElement(estructura);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," conjuntosAsociados() " +  e.getMessage(),0);
        }
        return usar;
    }
    //----------------------------------------------------------------------------------------------------------------
    public Vector retornarNenecidadesMes(){
        Vector comprasRequeridas = new Vector();
        SimpleDateFormat fo = new SimpleDateFormat("yyyy");
        String ano = fo.format(new java.util.Date());
        String query1 = "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String[] fec = format.format(new java.util.Date()).split("-");
            Calendar c1 = Calendar.getInstance();
            c1.set(Integer.parseInt(fec[0]), Integer.parseInt(fec[1])-1, 1);
            int an = c1.get(c1.YEAR);
            String me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            query1 = " SELECT tc.PRO_CODPROD,tc.PRO_DESC, "
                    
                    
                    
                  /*  + " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, "*/
                    
                    
                    + " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
                    
                    
                    
                    
                    +" ((SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.consumosPorMaterial1 ma1  "
                    +" WHERE ma1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ) "
                    +" - "
                    +" (SELECT isnull(sum(Cantidad),0) FROM MAC.dbo.devolucionesPorMaterial de1 "
                    +" WHERE de1.Codigo = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +" )  AS MediaCantidad,     "
                    +" tc.PRO_INVMIN, "
                    +" tc.PRO_INVMAX,  "
                    +"  isnull(ml.Saldo,0) as Stock,  "
                    +" (0) as Stock1 "
                    +" ,tc.PRO_UMPRINCIPAL, "
                    +" (0) as ordenVencida, "
                    +" (tcc.porLlegar) as ordenFutura, "
            
                    + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,";
            
            c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
            an = c1.get(c1.YEAR);
            me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            
            /*query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, ";*/
            
            query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
            
            
            
                  + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,";
            
            
            
            c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
            an = c1.get(c1.YEAR);
            me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            
           /* query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, ";*/
            
            query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
            
            
                    + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,";
            c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
            an = c1.get(c1.YEAR);
            me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            
            
            /*query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, ";*/
            
            query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
            
            
            
            + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,";
            
            
            c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
            an = c1.get(c1.YEAR);
            me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            
            
           /* query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, ";*/
            
            query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
            
            + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,";
            
            c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
            an = c1.get(c1.YEAR);
            me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            
           /* query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, ";*/
            
            query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
            
            
            
                   + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,";
            c1.set(c1.get(c1.YEAR), Integer.parseInt(me), 1);
            an = c1.get(c1.YEAR);
            me = String.valueOf(c1.get(c1.MONTH)+1);
            if(me.length() == 1){
                me = "0"+me;
            }
            
            
           /* query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND a.proceso = CodigoSemiElaborado "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0 ) as necesidadNoCelda"
                    + " , "
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    + " AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"' "
                    + " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(tc.PRO_CODPROD)),0) as necesidadSiCelda, "*/
            
            query1 += " isnull((SELECT sum(me.CantidadMP) "
                    + " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    + " WHERE a.codProducto = me.CodigoTerminado "
                    + " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime >= '"+an+me+"01'  and  a.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadNoCelda"
                    + " ,"
                    + " isnull((SELECT SUM(me.CantidadMP) "
                    + " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma"
                    + " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    + " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'"
                    + " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime >= '"+an+me+"01'  and  ma.readTime <= '"+an+me+c1.getActualMaximum(Calendar.DAY_OF_MONTH)+"'),0) as necesidadSiCelda,"
                    
                    
                    + " isnull(("
                            + " select sum(isnull(Cantidad_mat_prima,0)) "
                            + " from  ENSchaffner.dbo.EE_repro_rechazo "
                            + " INNER JOIN ENSchaffner.dbo.EE_repro_avance"
                            + " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)"
                            + " inner join ENSchaffner.dbo.EE_repro_falla"
                            + " on(rep_codigo = rpr_codigo_falla)"
                            + " inner join ENSchaffner.dbo.EE_repro_est "
                            + " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)"
                            + " where av_fecha_termino is null and rep_analisis =1 "
                            + " and Codigo_mat_prima = tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS ),0) as reprocesos,"
                    
                    +" sp.CantAsignada,isnull(ml1.Saldo,0) as BPT,pa.Comprador,pa.LeadTime,puc.MVD_PrecioAjustado,oa.MVE_FolioFisico,oa.MVD_FechaEntrega,oa.PER_NOMBRE,isnull(ml9.Saldo,0),puc.moneda,isnull(oa.fechaConfirmada,''), ";
            
            query1 += " isnull((SELECT sum(me.CantidadMP) ";
            query1 += " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
            query1 += " WHERE a.codProducto = me.CodigoTerminado ";
            query1 += " AND a.proceso = CodigoSemiElaborado and a.familia <> 'CELDA'";
            query1 += " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND a.readTime <= (SELECT CONVERT(char(8), dateadd(day,(7+pa.LeadTime),a.readTime) ,112))),0)";
            query1 += " +";
            query1 += " isnull((SELECT SUM(me.CantidadMP) ";
            query1 += " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma";
            query1 += " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
            query1 += " AND ma.proceso = 'MON' and tp.Familia = 'CELDA'";
            query1 += " AND me.CodigoMatPrima in(tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS)  AND ma.readTime <= (SELECT CONVERT(char(8), dateadd(day,(7+pa.LeadTime),ma.readTime) ,112))),0) as NecesidadPorEquipo";
            
            
            query1 += " FROM SiaSchaffner.dbo.STO_PRODUCTO tc  "
                    +" left outer join SiaSchaffner.dbo.VW_oc_atrasadas_sumadas tcc "
                    +" on(tcc.MVD_CodProd = tc.PRO_CODPROD) "
                    +" left outer join SiaSchaffner.dbo.VW_stock_materiales ml "
                    +" on(ml.PSL_CodProd = tc.PRO_CODPROD ) "
                    +" left outer join SiaSchaffner.dbo.VW_stock_productos_terminados ml1 "
                    +" on(ml1.PSL_CodProd = tc.PRO_CODPROD) "
                    +" left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 "
                    +" on(ml9.PSL_CodProd = tc.PRO_CODPROD) "
                    +" inner join MAC.dbo.ProductoAnexo pa "
                    +" on(pa.Codigo =tc.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +" left outer join MAC2BETA.dbo.VW_pedidos_planta sp "
                    +" on(tc.PRO_CODPROD = sp.MVD_CodProd)  "
                    +" left outer join SiaSchaffner.dbo.VW_precio_ultima_compra puc "
                    +" on(tc.PRO_CODPROD = puc.MVD_CodProd) "
                    +" left outer join SiaSchaffner.dbo.VW_oc_atrasadas  oa "
                    +" on(tc.PRO_CODPROD = oa.MVD_CodProd and MVD_FechaEntrega = "
                    +" (select min(MVD_FechaEntrega) "
                    +" from SiaSchaffner.dbo.VW_oc_atrasadas "
                    +" where MVD_CodProd = tc.PRO_CODPROD)) "
                    //+" WHERE (tc.PRO_CODTIPO in (1,5,8) or tc.PRO_CODPROD like '5041%') "
                    +" WHERE pa.Estado <> 1 and tc.PRO_VIGENCIA = 'S'  and pa.Comprador <> 'null' AND len( tc.PRO_CODPROD) >3  "
                    +" group by tc.PRO_CODPROD,isnull(oa.fechaConfirmada,''),puc.moneda,isnull(ml1.Saldo,0),isnull(ml.Saldo,0),isnull(ml9.Saldo,0),pa.asociado,pa.Comprador,tcc.porLlegar,sp.MVD_CodProd,tc.PRO_DESC,tc.PRO_INVMIN,tc.PRO_INVMAX,sp.CantAsignada,tc.PRO_UMPRINCIPAL,pa.Estado,pa.Stokeable,pa.analisis,pa.anaGen,pa.LeadTime,puc.MVD_PrecioAjustado,oa.MVE_FolioFisico,oa.MVD_FechaEntrega,oa.PER_NOMBRE "
                    +" ORDER BY tc.PRO_CODPROD ";
            
            rs = st.executeQuery(query1);
            while(rs.next()){
                /*String caracter = "";
                Vector compras = new Vector();
                compras.addElement(rs.getString(1));
                compras.addElement(rs.getString(2).replace('"',' '));
                compras.addElement(new Float(rs.getFloat(3) + rs.getFloat(4) + rs.getFloat(13) + rs.getFloat(14) + rs.getFloat(15)));
                if(rs.getFloat(5) == 0){
                    compras.addElement(new Float(rs.getFloat(5)));
                }else{
                    compras.addElement(new Float(rs.getFloat(5)/12));
                }
                compras.addElement(new Float(rs.getFloat(6)));
                compras.addElement(new Float(rs.getFloat(7)));
                compras.addElement(new Float(rs.getFloat(8) + rs.getFloat(9)));
                compras.addElement(rs.getString(10));
                compras.addElement(new Float(rs.getFloat(11) + rs.getFloat(12)));
                compras.addElement(new Float(rs.getFloat(16) + rs.getFloat(17) + rs.getFloat(18) + rs.getFloat(19) + rs.getFloat(20)));
                compras.addElement(new Float(rs.getFloat(21) + rs.getFloat(22) + rs.getFloat(23) + rs.getFloat(24) + rs.getFloat(25)));
                compras.addElement(new Float(rs.getFloat(26) + rs.getFloat(27) + rs.getFloat(28) + rs.getFloat(29) + rs.getFloat(30)));
                compras.addElement(new Float(rs.getFloat(31) + rs.getFloat(32) + rs.getFloat(33) + rs.getFloat(34) + rs.getFloat(35)));
                compras.addElement(new Float(rs.getFloat(36) + rs.getFloat(37) + rs.getFloat(38) + rs.getFloat(39) + rs.getFloat(40)));
                compras.addElement(new Float(rs.getFloat(41) + rs.getFloat(42) + rs.getFloat(43) + rs.getFloat(44) + rs.getFloat(45)));
                compras.addElement(new Float(rs.getFloat(46)));
                compras.addElement(new Float(rs.getFloat(47)));
                compras.addElement(rs.getString(48));
                compras.addElement(new Integer(rs.getInt(49)));
                compras.addElement(new Float(rs.getFloat(50)));
                compras.addElement(new Long(rs.getLong(51)));
                compras.addElement(rs.getString(52));
                compras.addElement(rs.getString(53));
                compras.addElement(new Float(rs.getFloat(54)));
                comprasRequeridas.addElement(compras);*/
                
                
                String caracter = "";
                Vector compras = new Vector();
                compras.addElement(rs.getString(1)); //0 c�digo materia prima
                compras.addElement(rs.getString(2).replace('"',' ')); //1 descripci�n
                compras.addElement(new Float(rs.getFloat(3) + rs.getFloat(4) + rs.getFloat(13))); //2 necesidad_no_celda + necesidad_si_celda + reproceso
                if(rs.getFloat(5) == 0){
                    compras.addElement(new Float(rs.getFloat(5))); //3 consumo promedio
                }else{
                    compras.addElement(new Float(rs.getFloat(5)/12));//3 consumo promedio
                }
                compras.addElement(new Float(rs.getFloat(6))); //4 stock m�nimo
                compras.addElement(new Float(rs.getFloat(7))); //5 stock m�ximo
                compras.addElement(new Float(rs.getFloat(8) + rs.getFloat(9))); //6 BMP
                compras.addElement(rs.getString(10)); //7 UM
                compras.addElement(new Float(rs.getFloat(11) + rs.getFloat(12))); //8 �rden compra
                
                compras.addElement(new Float(rs.getFloat(14) + rs.getFloat(15)  + rs.getFloat(16))); //9 mes 2
                compras.addElement(new Float(rs.getFloat(17) + rs.getFloat(18) + rs.getFloat(19))); //10 mes 3
                compras.addElement(new Float(rs.getFloat(20) + rs.getFloat(21) + rs.getFloat(22))); //11 mes 4
                compras.addElement(new Float(rs.getFloat(23) + rs.getFloat(24) + rs.getFloat(25))); //12 mes 5
                compras.addElement(new Float(rs.getFloat(26) + rs.getFloat(27) + rs.getFloat(28))); //13 mes 6
                compras.addElement(new Float(rs.getFloat(29) + rs.getFloat(30) + rs.getFloat(31))); //14 mes 7
                compras.addElement(new Float(rs.getFloat(32))); //15 stock planta
                compras.addElement(new Float(rs.getFloat(33))); //16 BPT
                compras.addElement(rs.getString(34)); //17 comprador
                compras.addElement(new Integer(rs.getInt(35))); //18 lead time
                compras.addElement(new Float(rs.getFloat(36))); //19 precio �ltima compra
                compras.addElement(new Long(rs.getLong(37))); //20 folio fisico oc atrasada
                compras.addElement(rs.getString(38)); //21 fecha entrega oc atrasada
                compras.addElement(rs.getString(39)); //22 proveedor oc atrasada
                compras.addElement(new Float(rs.getFloat(40))); //23 saldo bodega 9
                compras.addElement(rs.getString(41)); //24 moneda
                compras.addElement(rs.getString(42)); //25 fecha confirmada oc
                compras.addElement(new Float(rs.getFloat(43))); //26 necesidades =  LeadTime + 7 dias
                comprasRequeridas.addElement(compras);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR retornarNenecidadesMes(): " + e.getMessage(),0);
        }
        return comprasRequeridas;
    }
    
    public Vector compradores(){
        Vector comprador = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " SELECT Usuario FROM MAC.dbo.LG_permiso_a_link WHERE Nombre = 'comprador'";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setUsuario(rs.getString(1));
                comprador.addElement(estructura);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," compradores() " +  e.getMessage(),0);
        }
        return comprador;
    }
    
    public boolean parametros(int numero, int id){
        boolean est = false;
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select count(*) from MAC.dbo.SIS_parametros where nb_numero = "+numero+" and nb_id = "+id;
            rs = st.executeQuery(query);
            while(rs.next()){
                if(rs.getInt(1)  > 0){
                    est = true;
                }
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," parametros(int numero, int id) " +  e.getMessage(),0);
        }
        return est;
    }
    
    public Object[] ultimoPrecioCompra(String codigo){
        boolean est = false;
        Object[] valores = new Object[2];
        valores[0] = new Double(0d);
        valores[1] = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select  MVD_PrecioAjustado,moneda from SiaSchaffner.dbo.VW_precio_ultima_compra where MVD_COdProd = '"+codigo+"'";
            rs = st.executeQuery(query);
            while(rs.next()){
                valores[0] = new Double(rs.getDouble(1));
                valores[1] = rs.getString(2);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," ultimoPrecioCompra() " +  e.getMessage(),0);
        }
        return valores;
    }
    
    public int reclamos(String comprador, boolean est){
        int reclamo = 0;
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select count(*)";
            query += " from MAC2BETA.dbo.TBL_PRO_RECLAMOS";
            query += " inner join MAC.dbo.ProductoAnexo";
            query += " on(vc_codigo = Codigo)";
            if(est){
                query += " where Comprador = '"+comprador+"' and nb_estado = 1";
            }else{
                query += " where nb_estado = 1";
            }
            rs = st.executeQuery(query);
            while(rs.next()){
                reclamo = rs.getInt(1);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," reclamos() " +  e.getMessage(),0);
        }
        return reclamo;
    }
    
    public ModReclamo reclamoPorNumero(int numero){
        ModReclamo rec = new ModReclamo();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String sql = " select nb_numero, vc_codigo, nb_estado, vc_usuario, vc_comprador, vc_observacion, convert(datetime,dt_fecha_ingreso) as dt_fecha_ingreso, convert(datetime,dt_fecha_ajuste) as dt_fecha_ajuste, nb_cantidad,PRO_DESC,PRO_UMPRINCIPAL";
            sql += " from MAC2BETA.dbo.TBL_PRO_RECLAMOS ";
            sql += " inner join SiaSchaffner.dbo.STO_PRODUCTO";
            sql += " on(PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
            sql += " where nb_numero = " + numero;
            rs = st.executeQuery(sql);
            while(rs.next()){
                rec.setNumero(rs.getLong("nb_numero"));
                rec.setEstado(rs.getByte("nb_estado"));
                rec.setUsuario(rs.getString("vc_usuario"));
                rec.getMaterial().setMateriaPrima(rs.getString("vc_codigo"));
                rec.getMaterial().setComprador(rs.getString("vc_comprador"));
                rec.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                rec.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                rec.getMaterial().setCantidad(rs.getInt("nb_cantidad"));
                rec.setObservacion(rs.getString("vc_observacion"));
                rec.setFechaIngreso(rs.getDate("dt_fecha_ingreso"));
                rec.setFechaAjuste(rs.getDate("dt_fecha_ajuste"));
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," reclamoPorNumero() " +  e.getMessage(),0);
        }
        return rec;
    }
    
    public ArrayList grillaReclamosComprador(String comprador, boolean est){
        ArrayList reclamos = new ArrayList();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String sql = " select nb_numero, vc_codigo, nb_estado, vc_usuario, vc_comprador, vc_observacion, convert(datetime,dt_fecha_ingreso) as dt_fecha_ingreso, dt_fecha_ajuste, nb_cantidad,PRO_DESC,PRO_UMPRINCIPAL";
            sql += " ,isnull((select sum(MVD_Cant - MVD_CantAsignada) as MVD_Cant from MAC2BETA.dbo.VW_ordenes_cant_cantAsignada where Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = vc_codigo ";
            sql += " and  MVD_Fecha COLLATE SQL_Latin1_General_CP850_CS_AS <= case when dt_fecha_ajuste is null then getDate() else dt_fecha_ajuste end),0) as cantOrden,";
            sql += " isnull((select sum(MVD_CantAsignada) from MAC2BETA.dbo.VW_ordenes_cant_cantAsignada where Codigo COLLATE SQL_Latin1_General_CP850_CS_AS = vc_codigo ";
            sql += " and  MVD_Fecha COLLATE SQL_Latin1_General_CP850_CS_AS between dt_fecha_ingreso  and case when dt_fecha_ajuste is null then getDate() else dt_fecha_ajuste end),0) as cantParte,";
            sql += " case when dt_fecha_ajuste is null then (select DATEDIFF(day,dt_fecha_ingreso,getDate())) else (select DATEDIFF(day,dt_fecha_ingreso,dt_fecha_ajuste)) end as dias";
            sql += " from MAC2BETA.dbo.TBL_PRO_RECLAMOS ";
            sql += " inner join SiaSchaffner.dbo.STO_PRODUCTO";
            sql += " on(PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS)";
            sql += " inner join MAC.dbo.ProductoAnexo";
            sql += " on(vc_codigo = Codigo)";
            if(est){
                sql += " where Comprador = '"+comprador+"' and nb_estado = 1";
            }else{
                sql += " where nb_estado = 1";
            }
            sql += " order by nb_numero ";
            rs = st.executeQuery(sql);
            while(rs.next()){
                ModReclamo rec = new ModReclamo();
                rec.setNumero(rs.getLong("nb_numero"));
                rec.setEstado(rs.getByte("nb_estado"));
                rec.setUsuario(rs.getString("vc_usuario"));
                rec.getMaterial().setMateriaPrima(rs.getString("vc_codigo"));
                rec.getMaterial().setComprador(rs.getString("vc_comprador"));
                rec.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                rec.getMaterial().setUm(rs.getString("PRO_UMPRINCIPAL"));
                rec.getMaterial().setCantidad(rs.getInt("nb_cantidad"));
                rec.setObservacion(rs.getString("vc_observacion"));
                rec.setFechaIngreso(rs.getDate("dt_fecha_ingreso"));
                Date convertedDate = null;
                if(rs.getString("dt_fecha_ajuste") != null){
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyymmdd");
                    convertedDate = (Date) dateFormat.parse(rs.getString("dt_fecha_ajuste")); 
                }
                rec.setFechaAjuste(convertedDate);
                rec.setCantOc(rs.getDouble("cantOrden"));
                rec.setCantParte(rs.getDouble("cantParte"));
                rec.setAtraso(rs.getInt("dias"));
                reclamos.add(rec);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," grillaReclamosComprador() " +  e.getMessage(),0);
        }
        return reclamos;
    }
    
    public ArrayList ordenesDeCompra(String codigo, String fechaIni, String fechaFin){
        ArrayList ordenes = new ArrayList();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String sql = " select MVE_FolioFisico as NumOrden,MVD_Linea,(MVD_Cant-MVD_CantAsignada) as MVD_Cant,MVD_Fecha as FechaEntrada,isnull(mvd_Analisis5,'') as FechaConfirmada,";
            sql += " MVD_FechaEntrega as FechaEntrega,PER_NOMBRE";
            sql += " from SiaSchaffner.dbo.VW_ordenes_compra";
            sql += " left outer join SiaSchaffner.dbo.SYS_PERSONA ";
            sql += " on(MVE_CodPersona = PER_CODIGO) ";
            sql += " where MVD_CodProd = '" + codigo + "' and (MVD_Cant-MVD_CantAsignada) > 1";
            if (!fechaFin.trim().equals("")) {
                sql += "and MVD_FechaEntrega <= '" + fechaFin + "'";
            } else {
                sql += "and MVD_FechaEntrega >= '" + fechaIni + "'";
            }
            rs = st.executeQuery(sql);
            while(rs.next()){
                ModOrdenCompra orden = new ModOrdenCompra();
                orden.setNumero(rs.getLong("NumOrden"));
                orden.setLinea(rs.getInt("MVD_Linea"));
                orden.setCantidad(rs.getInt("MVD_Cant"));
                orden.setFechaIngreso(rs.getString("FechaEntrada"));
                orden.setFechaConfirmada(rs.getString("FechaConfirmada"));
                orden.setFechaEntrega(rs.getString("FechaEntrega"));
                orden.setNombreProveedor(rs.getString("PER_NOMBRE"));
                ordenes.add(orden);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," ordenesDeCompra() " +  e.getMessage(),0);
        }
        return ordenes;
    }
    
    public ArrayList partesEntrada(String codigo, String fechaIni, String fechaFin){
        ArrayList partes = new ArrayList();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String sql = " select MVE_FolioFisico as NumParte,MVD_Linea as Linea";
            sql += " ,MVD_Cant as Cant,MVD_Fecha as FechaEntrada ";
            sql += " ,(select CTA_RAZONSOCIAL from SiaSchaffner.dbo.STO_CTACTE where CTA_CODIGO = MVE_CodPersona and CTA_TIPOCTACORRIENTE = 2) as RazonSocial ";
            sql += " from SiaSchaffner.dbo.VW_partes_entrada";
            sql += " where MVD_CodProd = '" + codigo + "' ";
            if (!fechaFin.trim().equals("")) {
                sql += "and MVD_Fecha between '" + fechaIni + "' and '" + fechaFin + "'";
            } else {
                sql += "and MVD_Fecha >= '" + fechaIni + "'";
            }
            rs = st.executeQuery(sql);
            while(rs.next()){
                ModParteEntrada parte = new ModParteEntrada();
                parte.setNumero(rs.getLong("NumParte"));
                parte.setLinea(rs.getInt("Linea"));
                parte.setCantidad(rs.getDouble("Cant"));
                parte.setFechaIngreso(rs.getString("FechaEntrada"));
                parte.setNombreProveedor(rs.getString("RazonSocial"));
                partes.add(parte);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," partesEntrada() " +  e.getMessage(),0);
        }
        return partes;
    }
    
    public ArrayList grillaReclamos(String condicion){
        ArrayList reclamos = new ArrayList();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            /*String sql = " select ree.nb_np,ree.nb_linea,tp.Serie,tp.Cliente,tp.Descri,tp.Familia,tp.KVA,tp.Monto,PRO_CODPROD,PRO_DESC,re.vc_comprador,re.dt_fecha_ingreso, ";
            sql += " case when re.dt_fecha_ajuste is null then (select DATEDIFF(day,re.dt_fecha_ingreso,getDate())) else (select DATEDIFF(day,re.dt_fecha_ingreso,re.dt_fecha_ajuste)) end as dias , ";
            sql += " re.dt_fecha_ajuste,re.nb_cantidad,re.nb_nece_material,nb_stock_minimo,nb_stock_maximo,nb_stock_bodega,nb_lead_time,nb_oc_pendiente,gen_Desc ";
            sql += " from MAC2BETA.dbo.TBL_PRO_RECLAMOS re ";
            sql += " inner join MAC2BETA.dbo.TBL_PRO_RECLAMOS_EQUIPO ree ";
            sql += " on(re.nb_numero = ree.nb_numero) ";
            sql += " inner join SiaSchaffner.dbo.STO_PRODUCTO ";
            sql += " on(PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS) ";
            sql += " left outer join ENSchaffner.dbo.TPMP tp ";
            sql += " on(tp.Pedido = ree.nb_np and tp.Linea = ree.nb_linea) ";
            sql += " left outer join ENSchaffner.dbo.EE_tabla_generica ";
            sql += " on(gen_Tipo = 13 and gen_Codigo = re.vc_proceso) ";
            sql += " where " + condicion;
            sql += " order by re.nb_numero desc ";*/
            
            String sql = "select re.nb_numero,ree.nb_np,ree.nb_linea,tp.Serie,tp.Cliente,tp.Descri,tp.Familia,tp.KVA,tp.Monto,PRO_CODPROD,PRO_DESC,re.vc_comprador,convert(datetime,re.dt_fecha_ingreso) as dt_fecha_ingreso, ";
            sql += " case when re.dt_fecha_ajuste is null then (select DATEDIFF(day,convert(datetime,re.dt_fecha_ingreso),getDate())) else (select DATEDIFF(day,convert(datetime,re.dt_fecha_ingreso),convert(datetime,re.dt_fecha_ajuste))) end as dias , ";
            sql += " convert(datetime,re.dt_fecha_ajuste) as dt_fecha_ajuste,re.nb_cantidad,re.nb_nece_material,nb_stock_minimo,nb_stock_maximo,nb_stock_bodega,nb_lead_time,nb_oc_pendiente,re.vc_proceso,";
            sql += " re.vc_observacion, er.vc_nombre,re.vc_usuario";
            sql += " from MAC2BETA.dbo.TBL_PRO_RECLAMOS re ";
            sql += " inner join MAC2BETA.dbo.TBL_PRO_RECLAMOS_EQUIPO ree ";
            sql += " on(re.nb_numero = ree.nb_numero) ";
            sql += " inner join SiaSchaffner.dbo.STO_PRODUCTO ";
            sql += " on(PRO_CODPROD = vc_codigo COLLATE SQL_Latin1_General_CP850_CS_AS) ";
            sql += " left outer join ENSchaffner.dbo.TPMP tp ";
            sql += " on(tp.Pedido = ree.nb_np and tp.Linea = ree.nb_linea) ";
            //sql += " left outer join ENSchaffner.dbo.EE_tabla_generica ";
            //sql += " on(gen_Tipo = 13 and gen_Codigo = re.vc_proceso) ";
            sql += " inner join MAC2BETA.dbo.TBL_PRO_ESTADO_RECLAMO er";
            sql += " on(er.nb_id = re.nb_estado)";
            sql += " where " + condicion;
            sql += " order by re.nb_numero";
            rs = st.executeQuery(sql);
            while(rs.next()){
                ModReclamo rec = new ModReclamo();
                rec.setNumero(rs.getLong("nb_numero"));
                rec.getEquipo().setNp(rs.getInt("nb_np"));
                rec.getEquipo().setLinea(rs.getInt("nb_linea"));
                rec.getEquipo().setSerie(rs.getString("Serie"));
                rec.getEquipo().setCliente(rs.getString("Cliente"));
                rec.getEquipo().setFamilia(rs.getString("Familia"));
                rec.getEquipo().setKva(rs.getString("KVA"));
                rec.getEquipo().setDescripcion(rs.getString("Descri"));
                rec.getEquipo().setMonto(rs.getFloat("Monto"));
                rec.getMaterial().setMateriaPrima(rs.getString("PRO_CODPROD"));
                rec.getMaterial().setComprador(rs.getString("vc_comprador"));
                rec.getMaterial().setDescripcion(rs.getString("PRO_DESC"));
                rec.getMaterial().setCantidad(rs.getInt("nb_cantidad"));
                rec.getMaterial().setSolicitado(rs.getDouble("nb_nece_material"));
                rec.getMaterial().setStockMin(rs.getDouble("nb_stock_minimo"));
                rec.getMaterial().setStockMax(rs.getDouble("nb_stock_maximo"));
                rec.getMaterial().setSaldo(rs.getDouble("nb_stock_bodega"));
                rec.getMaterial().setLeadTime(rs.getInt("nb_lead_time"));
                rec.getMaterial().setOc(rs.getDouble("nb_oc_pendiente"));
                rec.setFechaIngreso(rs.getDate("dt_fecha_ingreso"));
                rec.setFechaAjuste(rs.getDate("dt_fecha_ajuste"));
                rec.setAtraso(rs.getInt("dias"));
                rec.setProceso(rs.getString("vc_proceso"));
                rec.setObservacion(rs.getString("vc_observacion"));
                rec.setRazon(rs.getString("vc_nombre"));
                rec.setUsuario(rs.getString("vc_usuario"));
                reclamos.add(rec);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," grillaReclamos() " +  e.getMessage(),0);
        }
        return reclamos;
    }
    
    
    public ArrayList grillaReclamosPorPlanos(String condicion){
        ArrayList reclamos = new ArrayList();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            
            
            String sql = " select re.nb_id,re.nb_np,re.nb_linea,tp.Serie,tp.Cliente,tp.Descri,tp.Familia,tp.KVA,tp.Monto,convert(datetime,re.dt_fecha_creacion) as dt_fecha_creacion, ";
            sql += " case when re.dt_fecha_solucion is null then (select DATEDIFF(day,convert(datetime,re.dt_fecha_creacion),getDate())) else (select DATEDIFF(day,convert(datetime,re.dt_fecha_creacion),convert(datetime,re.dt_fecha_solucion))) end as dias , ";
            sql += " convert(datetime,re.dt_fecha_solucion) as dt_fecha_solucion,re.vc_area,re.vc_observacion, er.vc_nombre, re.vc_proyectista,re.ch_detencion,re.vc_num_plano,re.vc_usuario_solucion,re.vc_observacion_solucion,re.ch_tipo_plano,re.vc_usuario_creacion";
            sql += " from MAC.dbo.TBL_PRO_RECLAMOS_PLANO re ";
            sql += " left outer join ENSchaffner.dbo.TPMP tp ";
            sql += " on(tp.Pedido = re.nb_np and tp.Linea = re.nb_linea)";
            sql += " inner join MAC.dbo.TBL_PRO_RECLAMOS_ESTADO er";
            sql += " on(er.nb_id = re.nb_id_estado)";
            sql += " where " + condicion;
            sql += " order by re.nb_id";
            rs = st.executeQuery(sql);
            while(rs.next()){
                ModReclamo rec = new ModReclamo();
                rec.setNumero(rs.getLong("nb_id"));
                rec.getEquipo().setNp(rs.getInt("nb_np"));
                rec.getEquipo().setLinea(rs.getInt("nb_linea"));
                rec.getEquipo().setSerie(rs.getString("Serie"));
                rec.getEquipo().setCliente(rs.getString("Cliente"));
                rec.getEquipo().setFamilia(rs.getString("Familia"));
                rec.getEquipo().setKva(rs.getString("KVA"));
                rec.getEquipo().setDescripcion(rs.getString("Descri"));
                rec.getEquipo().setMonto(rs.getFloat("Monto"));
                rec.setFechaIngreso(rs.getDate("dt_fecha_creacion"));
                rec.setFechaAjuste(rs.getDate("dt_fecha_solucion"));
                rec.setAtraso(rs.getInt("dias"));
                rec.setProceso(rs.getString("vc_area"));
                rec.setObservacion(rs.getString("vc_observacion"));
                rec.setRazon(rs.getString("vc_nombre"));
                rec.setProyectista(rs.getString("vc_proyectista"));
                rec.setDetencion(rs.getString("ch_detencion"));
                rec.setNumPlano(rs.getString("vc_num_plano"));
                rec.setUsuSolution(rs.getString("vc_usuario_solucion"));
                rec.setObserSolution(rs.getString("vc_observacion_solucion"));
                rec.setTipoPlano(rs.getString("ch_tipo_plano"));
                rec.setUsuario(rs.getString("vc_usuario_creacion"));
                reclamos.add(rec);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," grillaReclamosPorPlanos() " +  e.getMessage(),0);
        }
        return reclamos;
    }
    
    public ArrayList equiposPorNumero(long numero) {
        ResultSet rs;
        ArrayList equipos = new ArrayList();
        try {
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String sql = " select Pedido,Linea,Serie,Cliente,FechaCli,Expeditacion,Familia,KVA,Diseno,Status,Codpro";
            sql += " from MAC2BETA.dbo.TBL_PRO_RECLAMOS re";
            sql += " inner join MAC2BETA.dbo.TBL_PRO_RECLAMOS_EQUIPO ree";
            sql += " on(re.nb_numero = ree.nb_numero)";
            sql += " left outer join ENSchaffner.dbo.TPMP tp";
            sql += " on(tp.Pedido = ree.nb_np and tp.Linea = ree.nb_linea)";
            sql += " where re.nb_numero = " + numero;
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Equipo equipo = new Equipo();
                equipo.setNp(rs.getInt("Pedido"));
                equipo.setLinea(rs.getInt("Linea"));
                equipo.setSerie(rs.getString("Serie"));
                equipo.setNombreCliente(rs.getString("Cliente"));
                equipo.setFechaCliente(rs.getDate("FechaCli"));
                equipo.setExpeditacion(rs.getDate("Expeditacion"));
                equipo.setFamilia(rs.getString("Familia"));
                equipo.setKva(rs.getString("KVA"));
                equipo.setDiseno(rs.getString("Diseno"));
                equipo.setStatus(rs.getString("Status"));
                equipos.add(equipo);
            }
            rs.close();
            rs = null;
            
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," equiposPorNumero() " +  e.getMessage(),0);
        }
        return equipos;
    }
    
    
    
    /* public ArrayList DetalleCantidadSap(String numero) {
        ResultSet rs;
        ArrayList equipos = new ArrayList();
        try {
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();                       
            String sql = " select ";
            sql += " vw.MVD_Cant as Cantidad_Requerida, ";
            sql += " vw.MVD_CAntAsignada as Cantidad_Asignada "; 
            sql += " from SiaSchaffner.dbo.VW_sap vw "; 
            sql += " where vw.MVD_Codprod='1061201005' ";
            //sql += " where vw.MVD_Codprod='"+numero+"'";                                    
            rs = st.executeQuery(sql);
            while (rs.next()) {
                Equipo equipo = new Equipo();
                equipo.setCantidadRequerida(rs.getInt("Cantidad_Requerida"));
                equipo.setCantidadAsignada(rs.getInt("Cantidad_Asignada"));
                equipos.add(equipo);
            }
            rs.close();
            rs = null;
            
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," DetalleCantidadSap() " +  e.getMessage(),0);
        }
        return equipos;
    }*/
     
                        
        public Vector DetalleCantidadSap(long numero){
        Vector comprador = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
          //  String query = " SELECT Usuario FROM MAC.dbo.LG_permiso_a_link WHERE Nombre = 'comprador'";            
            
             String sql = " select MVE_FolioFisico,MVE_FechaDoc,MVE_CentroCosto, ";
                    sql += " UsCreacion,MVD_Cant,MVD_CantAsignada,MVD_ComentarioLinea,MVE_GlosaDoc ";
                    sql += " from  SiaSchaffner.dbo.VW_sap ";
                    sql += " WHERE MVE_FolioFisico='"+numero+"'";
                    sql += " AND MVD_Cant > MVD_CantAsignada ";                                                                           
            rs = st.executeQuery(sql);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setNumDoc(rs.getInt(1));
                estructura.setFechaIn(rs.getString(2));
                estructura.setFamilia(rs.getString(3));                
                estructura.setComprador(rs.getString(4));                
                estructura.setCantidadRequerida(rs.getInt(5));                
                estructura.setCantidadAsignada(rs.getInt(6));                
                estructura.setDescripcion(rs.getString(7));                
                estructura.setObservacion(rs.getString(8));                
                comprador.addElement(estructura);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," DetalleCantidadSap() " +  e.getMessage(),0);
        }
        return comprador;
        
        
        
    }
     
        
                          
        public Vector numeroSap(String numero){
        Vector comprador = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
          //  String query = " SELECT Usuario FROM MAC.dbo.LG_permiso_a_link WHERE Nombre = 'comprador'";            
            
             String sql = " select distinct (MVE_FolioFisico) ";
                    sql += " from  SiaSchaffner.dbo.VW_sap ";
                    sql += " WHERE MVD_CodProd='"+numero+"'";
                    sql += " AND MVD_Cant > MVD_CantAsignada ";                                                                           
            rs = st.executeQuery(sql);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setNumDoc(rs.getInt(1));                     
                comprador.addElement(estructura);
            }
            rs.close();
            st.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("MrpBean.class: "," numeroSap() " +  e.getMessage(),0);
        }
        return comprador;
        
        
        
    }
        
        
    
}// fin
