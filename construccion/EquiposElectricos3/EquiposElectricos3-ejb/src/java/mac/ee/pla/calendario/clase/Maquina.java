/*
 * Maquina.java
 *
 * Created on 13 de febrero de 2008, 9:11
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.calendario.clase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 *
 * @author dbarra
 */
public class Maquina {
    private short id;
    private String nombreMaquina;
    private Conexion mConexion= new Conexion();
    private Connection conMAC=null;
    private Statement st;
    private ResultSet rs;
    
    
    /** Creates a new instance of Maquina */
    public Maquina() {
    }

    public short getId() {
        return id;
    }

    public void setId(short id) {
        this.id = id;
    }

    public String getNombreMaquina() {
        return nombreMaquina;
    }

    public void setNombreMaquina(String nombreMaquina) {
        this.nombreMaquina = nombreMaquina;
    }
    
    public Vector getMaquinas(){
        Vector maquinas = new Vector();
        try{
       conMAC = mConexion.getMACConnector();
       st = conMAC.createStatement();
       String query = "SELECT id,nombre FROM MAC.dbo.Maquina"; 
       rs = st.executeQuery(query);
       while(rs.next()){
           Maquina maquina = new Maquina();
           maquina.setId(rs.getShort(1));
           maquina.setNombreMaquina(rs.getString(2));
           maquinas.addElement(maquina);  
       }
       rs.close();
       st.close();
       conMAC.close();
       }catch(Exception e){
           new Loger().logger("CalendarioWeb.class " , "ERROR: traerHorasSeteadas() " + e.getMessage(),0);
           
       }
        return maquinas;
    }
}
