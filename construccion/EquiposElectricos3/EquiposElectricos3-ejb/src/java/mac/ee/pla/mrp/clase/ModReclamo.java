/*
 * ModReclamo.java
 *
 * Created on 20 de agosto de 2014, 15:52
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.mrp.clase;

import java.util.Date;
import mac.ee.pla.equipo.clase.Equipo;

public class ModReclamo {

    private long numero;
    private String usuario;
    private String usuSolution;
    private String observacion;
    private String obserSolution;
    private String proceso;
    private Date fechaIngreso;
    private Date fechaAjuste;
    private byte estado;
    private int cantidad;
    private int atraso;
    private double cantOc;
    private double cantParte;
    private ModMaterial material;
    private Equipo equipo;
    private String razon;
    private String proyectista;
    private String detencion;
    private String numPlano;
    private String tipoPlano;

    public ModReclamo() {
        usuario = "";
        tipoPlano = "";
        obserSolution = "";
        usuSolution = "";
        numPlano = "";
        detencion = "";
        razon = "";
        setProceso("");
        observacion = "";
        proyectista = "";
        material = new ModMaterial();
        setEquipo(new Equipo());
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public Date getFechaAjuste() {
        return fechaAjuste;
    }

    public void setFechaAjuste(Date fechaAjuste) {
        this.fechaAjuste = fechaAjuste;
    }

    public ModMaterial getMaterial() {
        return material;
    }

    public void setMaterial(ModMaterial material) {
        this.material = material;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public byte getEstado() {
        return estado;
    }

    public void setEstado(byte estado) {
        this.estado = estado;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public int getAtraso() {
        return atraso;
    }

    public void setAtraso(int atraso) {
        this.atraso = atraso;
    }

    public double getCantOc() {
        return cantOc;
    }

    public void setCantOc(double cantOc) {
        this.cantOc = cantOc;
    }

    public double getCantParte() {
        return cantParte;
    }

    public void setCantParte(double cantParte) {
        this.cantParte = cantParte;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public String getRazon() {
        return razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public String getProyectista() {
        return proyectista;
    }

    public void setProyectista(String proyectista) {
        this.proyectista = proyectista;
    }

    public String getDetencion() {
        return detencion;
    }

    public void setDetencion(String detencion) {
        this.detencion = detencion;
    }

    public String getNumPlano() {
        return numPlano;
    }

    public void setNumPlano(String numPlano) {
        this.numPlano = numPlano;
    }

    public String getUsuSolution() {
        return usuSolution;
    }

    public void setUsuSolution(String usuSolution) {
        this.usuSolution = usuSolution;
    }

    public String getObserSolution() {
        return obserSolution;
    }

    public void setObserSolution(String obserSolution) {
        this.obserSolution = obserSolution;
    }

    public String getTipoPlano() {
        return tipoPlano;
    }

    public void setTipoPlano(String tipoPlano) {
        this.tipoPlano = tipoPlano;
    }
}
