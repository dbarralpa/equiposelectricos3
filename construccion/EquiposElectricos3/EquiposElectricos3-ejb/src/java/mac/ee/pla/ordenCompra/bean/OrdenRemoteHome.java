
package mac.ee.pla.ordenCompra.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for Orden enterprise bean.
 */
public interface OrdenRemoteHome extends EJBHome {
    
    OrdenRemote create()  throws CreateException, RemoteException;
    
    
}
