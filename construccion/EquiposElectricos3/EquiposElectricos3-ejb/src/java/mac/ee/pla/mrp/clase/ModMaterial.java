/*
 * ModMaterial.java
 *
 * Created on 20 de agosto de 2014, 15:52
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.mrp.clase;

public class ModMaterial implements Cloneable {

    private int id;
    private int linea;
    private int razon;
    private int idFila;
    private int leadTime;
    private String proceso;
    private String materiaPrima;
    private String materiaPrimaR;
    private String avance;
    private String estado;
    private String descripcion;
    private String um;
    private String centroCosto;
    private String checked;
    private String selected;
    private String ubicacion;
    private String bodega;
    private String usuNombreRetirar;
    private String usuCodigoRetirar;
    private String fecha;
    private String comprador;
    private double cantidad;
    private double cantidadR;
    private double cantidadAsignada;
    private double necesidades;
    private double stock;
    private double reservado;
    private double piso;
    private double planta;
    private double cantRequerida;
    private double entregado;
    private double solicitado;
    private double solicitado1;
    private double solicitado2;
    private double stockMin;
    private double stockMax;
    private double saldo;
    private double oc;
    private byte tipoProd;
    private long numeroDoc;
    private long numero;
    private boolean desReservar;
    private boolean reservar;
    private boolean pedir;
    private boolean grabar;
    private boolean eliminar;
    private boolean existe;

    public ModMaterial() {
        id = 0;
        razon = 0;
        idFila = 0;
        um = "";
        descripcion = "";
        comprador = "";
        proceso = "";
        materiaPrima = "";
        materiaPrimaR = "";
        usuCodigoRetirar = "";
        usuNombreRetirar = "";
        fecha = "";
        avance = "";
        ubicacion = "";
        bodega = "";
        cantidad = 0d;
        piso = 0d;
        planta = 0d;
        cantidadR = 0d;
        stock = 0d;
        cantidadAsignada = 0d;
        necesidades = 0d;
        cantRequerida = 0d;
        reservado = 0d;
        solicitado = 0d;
        solicitado1 = 0d;
        solicitado2 = 0d;
        entregado = 0d;
        stockMin = 0d;
        stockMax = 0d;
        saldo = 0d;
        oc = 0d;
        estado = "";
        desReservar = false;
        reservar = true;
        pedir = false;
        grabar = false;
        eliminar = false;
        tipoProd = 0;
        numeroDoc = 0l;
        numero = 0l;
        linea = 0;
        centroCosto = "";
        checked = "";
        selected = "";
        existe = true;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public int getIdFila() {
        return idFila;
    }

    public void setIdFila(int idFila) {
        this.idFila = idFila;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getUsuNombreRetirar() {
        return usuNombreRetirar;
    }

    public void setUsuNombreRetirar(String usuNombreRetirar) {
        this.usuNombreRetirar = usuNombreRetirar;
    }

    public String getUsuCodigoRetirar() {
        return usuCodigoRetirar;
    }

    public void setUsuCodigoRetirar(String usuCodigoRetirar) {
        this.usuCodigoRetirar = usuCodigoRetirar;
    }
    
    public String getBodega() {
        return bodega;
    }

    public void setBodega(String bodega) {
        this.bodega = bodega;
    }

    public double getOc() {
        return oc;
    }

    public void setOc(double oc) {
        this.oc = oc;
    }

    public int getRazon() {
        return razon;
    }

    public void setRazon(int razon) {
        this.razon = razon;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public double getSolicitado2() {
        return solicitado2;
    }

    public void setSolicitado2(double solicitado2) {
        this.solicitado2 = solicitado2;
    }

    public double getSolicitado1() {
        return solicitado1;
    }

    public void setSolicitado1(double solicitado1) {
        this.solicitado1 = solicitado1;
    }

    public boolean isExiste() {
        return existe;
    }

    public void setExiste(boolean existe) {
        this.existe = existe;
    }

    public String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        this.selected = selected;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public String getMateriaPrima() {
        return materiaPrima;
    }

    public void setMateriaPrima(String materiaPrima) {
        this.materiaPrima = materiaPrima;
    }

    public String getMateriaPrimaR() {
        return materiaPrimaR;
    }

    public void setMateriaPrimaR(String materiaPrimaR) {
        this.materiaPrimaR = materiaPrimaR;
    }

    public String getAvance() {
        return avance;
    }

    public void setAvance(String avance) {
        this.avance = avance;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getCentroCosto() {
        return centroCosto;
    }

    public void setCentroCosto(String centroCosto) {
        this.centroCosto = centroCosto;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public double getCantidadR() {
        return cantidadR;
    }

    public void setCantidadR(double cantidadR) {
        this.cantidadR = cantidadR;
    }

    public double getCantidadAsignada() {
        return cantidadAsignada;
    }

    public void setCantidadAsignada(double cantidadAsignada) {
        this.cantidadAsignada = cantidadAsignada;
    }

    public double getNecesidades() {
        return necesidades;
    }

    public void setNecesidades(double necesidades) {
        this.necesidades = necesidades;
    }

    public double getStock() {
        return stock;
    }

    public void setStock(double stock) {
        this.stock = stock;
    }

    public double getReservado() {
        return reservado;
    }

    public void setReservado(double reservado) {
        this.reservado = reservado;
    }

    public double getPiso() {
        return piso;
    }

    public void setPiso(double piso) {
        this.piso = piso;
    }

    public double getPlanta() {
        return planta;
    }

    public void setPlanta(double planta) {
        this.planta = planta;
    }

    public double getCantRequerida() {
        return cantRequerida;
    }

    public void setCantRequerida(double cantRequerida) {
        this.cantRequerida = cantRequerida;
    }

    public double getEntregado() {
        return entregado;
    }

    public void setEntregado(double entregado) {
        this.entregado = entregado;
    }

    public double getSolicitado() {
        return solicitado;
    }

    public void setSolicitado(double solicitado) {
        this.solicitado = solicitado;
    }

    public double getStockMin() {
        return stockMin;
    }

    public void setStockMin(double stockMin) {
        this.stockMin = stockMin;
    }

    public double getStockMax() {
        return stockMax;
    }

    public void setStockMax(double stockMax) {
        this.stockMax = stockMax;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public byte getTipoProd() {
        return tipoProd;
    }

    public void setTipoProd(byte tipoProd) {
        this.tipoProd = tipoProd;
    }

    public long getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(long numeroDoc) {
        this.numeroDoc = numeroDoc;
    }

    public boolean isDesReservar() {
        return desReservar;
    }

    public void setDesReservar(boolean desReservar) {
        this.desReservar = desReservar;
    }

    public boolean isReservar() {
        return reservar;
    }

    public void setReservar(boolean reservar) {
        this.reservar = reservar;
    }

    public boolean isPedir() {
        return pedir;
    }

    public void setPedir(boolean pedir) {
        this.pedir = pedir;
    }

    public boolean isGrabar() {
        return grabar;
    }

    public void setGrabar(boolean grabar) {
        this.grabar = grabar;
    }

    public boolean isEliminar() {
        return eliminar;
    }

    public void setEliminar(boolean eliminar) {
        this.eliminar = eliminar;
    }

    public ModMaterial copia() {
        Object obj = null;
        try {
            obj = super.clone();
        } catch (CloneNotSupportedException ex) {
        }
        return (ModMaterial) obj;
    }

    public int getLeadTime() {
        return leadTime;
    }

    public void setLeadTime(int leadTime) {
        this.leadTime = leadTime;
    }
}
