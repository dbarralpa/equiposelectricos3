package mac.ee.pla.mrp.clase;

import java.util.ArrayList;

public class ModSesion {
    
    private ArrayList datos;
    private String descripcion;
    
    /** Creates a new instance of ModSesion */
    public ModSesion() {        
        datos = new ArrayList();
        descripcion = "";
    }

    public ArrayList getDatos() {
        return datos;
    }

    public void setDatos(ArrayList datos) {
        this.datos = datos;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
}
