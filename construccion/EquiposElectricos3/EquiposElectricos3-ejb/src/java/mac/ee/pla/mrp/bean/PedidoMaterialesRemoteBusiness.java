
package mac.ee.pla.mrp.bean;


/**
 * This is the business interface for PedidoMateriales enterprise bean.
 */
public interface PedidoMaterialesRemoteBusiness {
    void materialesPorEquipo(int np, int linea, String proceso, String familia) throws java.rmi.RemoteException;

    java.util.Vector retornarMaterialesPorProcesos() throws java.rmi.RemoteException;
    
}
