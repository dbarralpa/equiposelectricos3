/*
 * BitacoraVales.java
 *
 * Created on 13 de agosto de 2010, 12:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.mrp.clase;

import java.util.Date;

/**
 *
 * @author dbarra
 */
public class BitacoraVales {
    private String codigo;
    private String usuario;
    private String observacion;
    private byte estado;
    private Date fecha;
    private int pedido;
    
    
    /** Creates a new instance of BitacoraVales */
    public BitacoraVales() {
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public byte getEstado() {
        return estado;
    }

    public void setEstado(byte estado) {
        this.estado = estado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getPedido() {
        return pedido;
    }

    public void setPedido(int pedido) {
        this.pedido = pedido;
    }
    
}
