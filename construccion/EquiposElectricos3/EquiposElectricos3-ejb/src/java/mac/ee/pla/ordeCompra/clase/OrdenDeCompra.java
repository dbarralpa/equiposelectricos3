/*
 * OrdenDeCompra.java
 *
 * Created on 12 de noviembre de 2007, 17:04
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.ordeCompra.clase;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author dbarra
 */
public class OrdenDeCompra implements Serializable{
    
    private long num_orden_compra;
    private int linea;
    private float rut;
    private Date fecha_pedido;
    private String cod_producto;
    private float can_pedida;
    private float can_pendiente;
    private Date fecha_entrega;
    private Date fecha_nueva;
    private String descripcion;
    private String proveedor;
    private int idOrden;
    private String nombreOrden;
    private float precio;
    private float moneda;
    private String descMoneda;
    private String um;
    private float saldo;
    private String comprador;
    private Date fechaEntrada;
    private float numeroDocOrigen;
    private long numDocOrigen;
    private String aprobacion;
    private String accion;
    private String persona;
    private Timestamp fechaIni;
    
    
    private String comentario;
    private String fechaConfirmada;   
    
    
    public OrdenDeCompra(int num_orden_compra,
                         Date fecha_pedido,
                         String cod_producto,
                         float can_pedida,
                         float can_pendiente,
                         Date fecha_entrega
                         ) {
        this.setNum_orden_compra(num_orden_compra);
        this.setFecha_pedido(fecha_pedido);
        this.setCod_producto(cod_producto);
        this.setCan_pedida(can_pedida);
        this.setCan_pendiente(can_pendiente);
        this.setFecha_entrega(fecha_entrega);
    }
    
    public OrdenDeCompra(){
        
    }
    public OrdenDeCompra(int num_orden_compra,int linea,String cod_producto, float can_pedida,Date fecha_entrega){
        this.setNum_orden_compra(num_orden_compra);
        this.setLinea(linea);
        this.setCod_producto(cod_producto);
        this.setCan_pedida(can_pedida);
        this.setFecha_entrega(fecha_entrega);
        
    }
   
  public void imprimirNumero(){
       System.out.println(getNum_orden_compra() + " " +
                          getLinea() + " " +
                          getCod_producto() + " " +
                          getFecha_entrega() + " " +
                          getCan_pedida());
   }
  

    public long getNum_orden_compra() {
        return num_orden_compra;
    }

    public void setNum_orden_compra(long num_orden_compra) {
        this.num_orden_compra = num_orden_compra;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public float getRut() {
        return rut;
    }

    public void setRut(float rut) {
        this.rut = rut;
    }

    public Date getFecha_pedido() {
        return fecha_pedido;
    }

    public void setFecha_pedido(Date fecha_pedido) {
        this.fecha_pedido = fecha_pedido;
    }

    public String getCod_producto() {
        return cod_producto;
    }

    public void setCod_producto(String cod_producto) {
        this.cod_producto = cod_producto;
    }

    public float getCan_pedida() {
        return can_pedida;
    }

    public void setCan_pedida(float can_pedida) {
        this.can_pedida = can_pedida;
    }

    public float getCan_pendiente() {
        return can_pendiente;
    }

    public void setCan_pendiente(float can_pendiente) {
        this.can_pendiente = can_pendiente;
    }

    public Date getFecha_entrega() {
        return fecha_entrega;
    }

    public void setFecha_entrega(Date fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }

    public Date getFecha_nueva() {
        return fecha_nueva;
    }

    public void setFecha_nueva(Date fecha_nueva) {
        this.fecha_nueva = fecha_nueva;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public int getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(int idOrden) {
        this.idOrden = idOrden;
    }

    public String getNombreOrden() {
        return nombreOrden;
    }

    public void setNombreOrden(String nombreOrden) {
        this.nombreOrden = nombreOrden;
    }
     public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    public float getMoneda() {
        return moneda;
    }

    public void setMoneda(float moneda) {
        this.moneda = moneda;
    }
    public void setDescMoneda(String descMoneda){
        this.descMoneda = descMoneda;
    }
    public String getDescMoneda(){
        return descMoneda;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public float getSaldo() {
        return saldo;
    }

    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public float getNumeroDocOrigen() {
        return numeroDocOrigen;
    }

    public void setNumeroDocOrigen(float numeroDocOrigen) {
        this.numeroDocOrigen = numeroDocOrigen;
    }

    public String getAprobacion() {
        return aprobacion;
    }

    public void setAprobacion(String aprobacion) {
        this.aprobacion = aprobacion;
    }

    public String getAccion() {
        return accion;
    }

    public void setAccion(String accion) {
        this.accion = accion;
    }

    public String getPersona() {
        return persona;
    }

    public void setPersona(String persona) {
        this.persona = persona;
    }

    public Timestamp getFechaIni() {
        return fechaIni;
    }

    public void setFechaIni(Timestamp fechaIni) {
        this.fechaIni = fechaIni;
    }

    public long getNumDocOrigen() {
        return numDocOrigen;
    }

    public void setNumDocOrigen(long numDocOrigen) {
        this.numDocOrigen = numDocOrigen;
    }
    
    //juan hernandez
      public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
    
    public String getFechaConfirmada() {
        return fechaConfirmada;
    }

    public void setFechaConfirmada(String fechaConfirmada) {
        this.fechaConfirmada = fechaConfirmada;
    }
    
    
    
}
