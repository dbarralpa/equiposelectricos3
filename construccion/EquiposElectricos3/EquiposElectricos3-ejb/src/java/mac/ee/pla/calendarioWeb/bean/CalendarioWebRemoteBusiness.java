
package mac.ee.pla.calendarioWeb.bean;

import java.rmi.RemoteException;
import java.util.Vector;


/**
 * This is the business interface for CalendarioWeb enterprise bean.
 */
public interface CalendarioWebRemoteBusiness {
    Vector calendarioMes(int month) throws RemoteException;
    int transformarFechaAhora(String fech, short identificador,short idMaquina) throws java.rmi.RemoteException;
    void setIdentificador(String identi) throws java.rmi.RemoteException;
    String getIdentificador() throws java.rmi.RemoteException;    
    void setearArregloCalendario() throws java.rmi.RemoteException;
    short[][] retornarArregloCalendario() throws java.rmi.RemoteException;
    Vector getMaquinas() throws java.rmi.RemoteException;
    int largoCalendarioDeMaquina() throws java.rmi.RemoteException;
}
