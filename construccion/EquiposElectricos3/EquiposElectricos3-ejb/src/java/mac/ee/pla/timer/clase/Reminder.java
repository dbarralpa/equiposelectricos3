/*
 * Reminder.java
 *
 * Created on 11 de septiembre de 2008, 9:12
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.timer.clase;

import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JOptionPane;
import mac.ee.pla.mrp.bean.*;
/**
 *
 * @author dbarra
 */
public class Reminder {
    Timer timer;
     MrpBean mrp = new MrpBean();
     private int valor;
    // private Vector canti = new Vector();
    public Reminder(int seconds,int numero) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000);
        setValor(numero);
        //devolverCanti(pedidos);
        //ejecutar programa del carlos aqu�
	}
    public void setValor(int valor){
        this.valor = valor;
    }
    
    public int getValor(){
        return valor;
    }
    
    /*public Vector devolverCanti (Vector canti){
        this.canti = canti;
    }*/

    class RemindTask extends TimerTask {
        public void run() {
            int devolver = mrp.cantidadGrabarFlex(getValor());
            if(devolver == 0){
               mrp.actualizarEstado(4,getValor(),"Entregado","");
               JOptionPane.showMessageDialog(null,"Transacci�n exitosa");
            }else{
              
               JOptionPane.showMessageDialog(null,"Fall� la transacci�n");    
                mrp.deleteAFlex(getValor());
            }
            
            timer.cancel(); //Terminate the timer thread
        }
    }

   /* public static void main(String args[]) {
        new Reminder(10);
        System.out.format("Task scheduled.%n");
    }*/
}