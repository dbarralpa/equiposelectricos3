
package mac.ee.pla.mrp.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for PedidoMateriales enterprise bean.
 */
public interface PedidoMaterialesRemote extends EJBObject, PedidoMaterialesRemoteBusiness {
    
    
}
