
package mac.ee.pla.ordenCompra.bean;

import java.rmi.RemoteException;
import java.util.Vector;


/**
 * This is the business interface for Orden enterprise bean.
 */
public interface OrdenRemoteBusiness {
    //Vector ordenDeCompraInvalidas() throws RemoteException;
   // float sumarOrdenDeCompraValidas(String codPro) throws RemoteException;
    float stock(String codPro) throws RemoteException;
    float buscarStockPLanta(String codigo) throws RemoteException;
    int insertarOrdenDeCompra(int numOrden , int linea , String codMaterial , String fleje , String fechaLlegada , float cantidad) throws RemoteException;
    int borrarOrdenDeCompra(int numOrden , int linea) throws RemoteException;
    Vector cambiarFechaLlegada(int numero) throws RemoteException;
    Vector ordenDeCompraInvalidas(String material , int top , String nada) throws RemoteException;
    Vector ordenDeCompraInvalidas(String material) throws RemoteException;
    Vector ordenDeCompraValidas(String codPro, String dat) throws RemoteException;
    Vector ordenDeCompraValidas(String codPro) throws RemoteException;
    Vector buscarPorRutCliente(float rut) throws RemoteException;
    Vector buscarPorNumeroDeOrden() throws RemoteException;
    Vector buscarPorRutCliente() throws RemoteException;
    Vector buscarPorProveedor() throws RemoteException;
    Vector buscarPorDescripcion() throws RemoteException;
    Vector buscarPorCodigo() throws RemoteException;
    Vector mostrarDatosPorProveedor(String proveedor) throws RemoteException;
    Vector mostrarDatosPorCodigo(String codigo) throws RemoteException;
    Vector mostrarDatosPorDescripcionMateriaPrima(String descripcion) throws RemoteException;
    Vector listarOrdenes() throws RemoteException;
    Vector listarMotorDeBusqueda() throws RemoteException;
    Vector listarTodasLasOrdenes() throws RemoteException;
    Vector ordenDeCompraInvalidas(String fechaActual, int identificador) throws RemoteException;
    Vector ordenDeCompraValidas(String fechaActual, int identificador) throws RemoteException;  
    Vector ordenDeCompraValidasComprasRequeridas(String codPro) throws RemoteException;
    float ordenDeCompraPendientes(String codPro) throws java.rmi.RemoteException;
}
