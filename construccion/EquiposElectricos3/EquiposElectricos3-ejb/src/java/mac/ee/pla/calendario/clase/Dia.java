/*
 * Dia.java
 *
 * Created on 13 de febrero de 2008, 17:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.calendario.clase;

import java.util.Calendar;

/**
 *
 * @author dbarra
 */
public class Dia {
      private String lunes;
      private String martes;
      private String miercoles;
      private String jueves;
      private String viernes;
      private String sabado;
      private String domingo;
      Calendar fecha = Calendar.getInstance();
      String date = fecha.get(java.util.Calendar.YEAR) + "-"
                + (fecha.get(java.util.Calendar.MONTH)+1) + "-"  
                + (fecha.get(java.util.Calendar.DATE));
    
    /** Creates a new instance of Dia */
    public Dia() {
    }
    
    public String diaActual(){
        return date;
    }
//----------------------------------------------------------------------------------------------------------------------------        
   
    public String getLunes() {
        return lunes;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setLunes(String lunes) {
        this.lunes = lunes;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public String getMartes() {
        return martes;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setMartes(String martes) {
        this.martes = martes;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public String getMiercoles() {
        return miercoles;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setMiercoles(String miercoles) {
        this.miercoles = miercoles;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public String getJueves() {
        return jueves;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setJueves(String jueves) {
        this.jueves = jueves;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public String getViernes() {
        return viernes;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setViernes(String viernes) {
        this.viernes = viernes;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public String getSabado() {
        return sabado;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setSabado(String sabado) {
        this.sabado = sabado;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public String getDomingo() {
        return domingo;
    }
//----------------------------------------------------------------------------------------------------------------------------    
    public void setDomingo(String domingo) {
        this.domingo = domingo;
    }
}
