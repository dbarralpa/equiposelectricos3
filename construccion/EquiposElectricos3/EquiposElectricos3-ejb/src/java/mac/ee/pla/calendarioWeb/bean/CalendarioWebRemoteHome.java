
package mac.ee.pla.calendarioWeb.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for CalendarioWeb enterprise bean.
 */
public interface CalendarioWebRemoteHome extends EJBHome {
    
    CalendarioWebRemote create()  throws CreateException, RemoteException;
    
    
}
