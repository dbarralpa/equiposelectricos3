
package mac.ee.pla.mrp.bean;

import java.rmi.RemoteException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Vector;
import mac.ee.pla.mrp.clase.ModReclamo;


/**
 * This is the business interface for Mrp enterprise bean.
 */
public interface MrpRemoteBusiness {
    

    Vector controlDeMateriales() throws RemoteException;
    void materialesProyectados(String codigo,String descripcion,int compara,String fechaMenor,String fechaMayor,int validar,String uMedida) throws RemoteException;
    void setRetornarValor(boolean valor) throws RemoteException;
    boolean getRetornarValor() throws RemoteException;
    Timestamp getFecha() throws RemoteException;
    void eliminarOcargarMaterial(String codigo, String descripcion , int identificador , String uMedida) throws RemoteException;
    Vector retornarMaterialesEliminadosOcargados() throws RemoteException;
    Vector controlDeMateriales(Vector controlMaterialesReturn) throws RemoteException;
    //Vector actualizarStockPlanta() throws RemoteException;
    //void actualizarStockPlanta1(String material, float cantidad) throws RemoteException;
    Vector retornarMaterialesEliminadosOcargadosVacio(Vector vacio) throws RemoteException;
    Vector controlDeMateriales1() throws RemoteException;
    Vector controlDeMateriales1(Vector controlMaterialesReturn) throws RemoteException;
    //byte actualizarStockMinMax(String material,String uMedida, float cantidad, byte compara,String descripcion) throws RemoteException;
    void deleteAll() throws RemoteException;
    void deleteOC() throws RemoteException;
    void recalcularEstadoMaterial() throws RemoteException;
    boolean comprasRequeridas(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int verificar,byte estado) throws RemoteException;
    float retornarTotalQuiebre(String codigo) throws RemoteException;
    int retornarCantidadComprasRequeridas(String codigo) throws RemoteException;
    Vector retornarComprasRequeridas(int verificar,String ordenar) throws RemoteException;
    Vector balancePorMaterial() throws RemoteException;
    byte actualizarComprasRequeridas(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int valor) throws RemoteException;
    byte actualizarComprador(String material, String comprador, String fecha) throws RemoteException;
    void agruparPedidosSap(String material, String fecha, int numSap) throws RemoteException;
    void setIncrementar(int incrementar) throws RemoteException;
    int getIncrementar() throws RemoteException;
    int actualizarComprasRequeridas(String material,float cantidad ,String fecha,String fecha1) throws RemoteException;
    int borrarCompra(String material, String fecha) throws RemoteException;
    byte insertarPedidoMateriales (String codigo ,float canti,long numero,String usuario,String um,byte size,byte tipo,String usu) throws RemoteException;
    Vector usuariosDetallePedido() throws RemoteException;
    Vector pedido() throws RemoteException;
   // int cantidadPedidoMateriales() throws RemoteException;
    Vector pedidoPorUsuario(String usuario, String numero) throws RemoteException;
    int actualizarCantidadRequeridas(int numero ,String codigo,float cantidad,int tipo) throws RemoteException;
    int insertarObservacionDeVales(int numero ,String codigo,String observacion,byte estado,String usuario) throws RemoteException;
    int actualizarPedidoMateriales(String usuario, int numero, float cantidadEntregada) throws RemoteException;
    Vector pedidoConDeficit(String usuario, String numero) throws RemoteException;
    int borrarPedido(int np,int linea,String proceso,String usuario) throws java.rmi.RemoteException;
    int mandarPedidoABodega(String usuario, int numero,String estado) throws RemoteException;
    Vector numeroDelPedido(String usuario,int tipo) throws RemoteException;
    Vector usuarios() throws RemoteException;
    int entregarPedidoDesdeBodega(String usuario, int numero, String estado,int veri) throws RemoteException;
    //void materialesPorEquipo(int np, int linea, String proceso, String familia) throws RemoteException;
    //Vector retornarMaterialesPorProcesos() throws RemoteException;
    Vector materiales() throws RemoteException;
    Vector pedidosPorItem(long numero) throws RemoteException;
    float stockBodega(String codigo) throws RemoteException;
    byte actualizarEstado(int estado,long numero,String descri,String usuario) throws RemoteException;
    byte insertarAFlex (int numPedido,int linea,String codigo ,float canti,String proceso) throws RemoteException;
    int cantidadGrabarFlex (int numPedido) throws RemoteException;
    byte deleteAFlex(int numPedido) throws RemoteException;
    byte devolverEstadoCantidadEntregada(Vector cantiEntregada) throws RemoteException;
    int numeroSap() throws RemoteException;
    Vector SapPorUsuario(String comprador) throws RemoteException;
    void descartarCompra(String material, int id) throws RemoteException;
    Vector retornarEstadoMateriales(String codigoIni,String codigoFin,String rango,int valor,byte estado) throws RemoteException;
    int actualizarPrioridadPedido(int numero) throws RemoteException;
    String devolverEstadoGrabacionAflex(char ver) throws RemoteException;
    void actualizarMaterialesTconsumoPromedio() throws RemoteException;
    byte actualizarComprasRequeridasEE(String codigo, float cantRequerida) throws RemoteException;
    Vector familiaPorMaterial(String usuario) throws RemoteException;
    void actualizarMaterialesPorFamilia(String codigo, String familia ,byte estado) throws RemoteException;
    void borrarMaterialesPorFamilia(String codigo, String familia, byte estado) throws RemoteException;
    int comprasRequeridasPorComprador(String comprador) throws RemoteException;
    int comprasRequeridasConSapPorComprador(String comprador) throws RemoteException;
    void cambiarEstadoMaterial(String codigo, byte estado) throws RemoteException;
    Vector detalleConsumoPromedio(String codigo) throws RemoteException;
    Vector generarSap(int verificar, int numeroSap) throws RemoteException;
    Vector fechasConsumoPromedio() throws RemoteException;
    Vector retornarComprasRequeridasPorOrdenCompras(int numero,byte verificar) throws RemoteException;
    void cambiarStokeable(String codigo) throws RemoteException;
    Vector cambiarStockMinMax(String familia, float e,float k1,float k2) throws RemoteException;
    Vector leadTimePorFamilia(String familia) throws RemoteException;
    Vector retornarComprasRequeridasHidroPack() throws RemoteException;
    void insertarObservacionAmaterial(String codigo, String observacion, java.util.Date fecha) throws RemoteException;
    Vector bitacoraMateriales(String codigo) throws RemoteException;
    long insertarPedidoMateriales(Vector necesidades, Vector equipos, String usu, String process, byte grupo,String prioridad,int centroCosto,String supervisor,String ordenMantencion,String tipoMantencion) throws RemoteException;
    StringBuffer bitacoraMaterialesUnidos(String codigo) throws RemoteException;
    byte actualizarStockBodega(Vector pedidos) throws RemoteException;
    boolean validarUsuarioALink(String usuario, String nombre) throws RemoteException;
    Vector buscarPedido(String param,byte entrar, String estado1, String estado2, String estado3, String estado4, String estado5) throws RemoteException;
    String imprimirPedidos(long numero) throws RemoteException;
    void eliminarItem(String eliminar) throws RemoteException;
    int borrarPedido(long numero) throws RemoteException;
    int[] paginacion() throws RemoteException;
    java.util.Vector bitacoraVales(int pedido, String codigo) throws RemoteException;
    boolean estadoObs(int numero, String codigo) throws RemoteException;
    void cambiarPrioridad(String prioridad) throws RemoteException;
    float cantOtrosPedidos(String codigo) throws RemoteException;
    Vector centroDeCosto() throws RemoteException;
    boolean actualizarPedidoMateriales(String codProd, String linea, String nuevoCodProd, String numDoc,String um,String comentario, String usus, String num, String tipo) throws RemoteException;
    Vector historialVales(String supervisor) throws RemoteException;
    Vector detallePorMaterialVales(String codProd) throws RemoteException;
    Vector usuariosVales() throws RemoteException;
    Vector supervisorVales() throws RemoteException;
    boolean descartarMaterialesValeConsumo(String codProd, String supervisor) throws RemoteException;
    Vector materialesPorEquipo(String estructura,String equipo,String linea) throws RemoteException;
    long getNumPedido() throws java.rmi.RemoteException;
    Vector usuAprobarVale(String proceso) throws RemoteException;
    String nombreUsuario(String login) throws RemoteException;
    Vector buscarPorDescripcion(String descri) throws RemoteException;
    String ubicacionBodMaterial(String codigo) throws RemoteException;
    ArrayList materiaPrimaCodigo(String tmp) throws RemoteException;
    ArrayList materiaPrimaDesc(String tmp) throws RemoteException;
    Vector detalleControlPorMaterial(String fechaIni, String fechaFin, String material) throws RemoteException;
    void stockPlanta(String fecha) throws RemoteException;
    String buscarFechaStockPlanta() throws RemoteException;
    boolean consumosActualizados(String fechaIni) throws RemoteException;
    void truncarConsumosPromedio() throws RemoteException;
    Vector consumosPorAno(String fecIni, String fecFin) throws RemoteException;
    boolean insertarConsumosPromedio(float cantidad, int ano, int mes, String codigo) throws RemoteException;
    void conectarMac() throws RemoteException;
    void desconectarMac() throws RemoteException;
    boolean insertarDevolucionesPromedio(float cantidad, int ano, int mes, String codigo) throws RemoteException;
    Vector devolucionesPorAno(String fecIni, String fecFin) throws RemoteException;
    Vector detalleDevolucionesPromedio(String codigo) throws RemoteException;
    boolean asociarMateriales(String codigo, String asociado,byte estado) throws RemoteException;
    Vector retornarComprasRequeridasAsociados(String numero) throws RemoteException;
    void eliminarAsociado(String codigo) throws RemoteException;
    Vector conjuntosAsociados() throws RemoteException;
    float[] stockBodega1(String codigo) throws RemoteException;
    Vector compradores() throws RemoteException;
    Vector pedidoPorNumero(long numero) throws RemoteException;
    Vector pedidoPorFechas(String min, String max) throws RemoteException;
    Vector todosPedidos() throws RemoteException;
    float retornarStockPlanta(String codigo) throws RemoteException;
    int reclamos(String comprador, boolean est) throws RemoteException;
    ModReclamo reclamoPorNumero(int numero) throws java.rmi.RemoteException;
    ArrayList grillaReclamosComprador(String usuario, boolean est) throws java.rmi.RemoteException;
    ArrayList ordenesDeCompra(String codigo, String fechaIni, String fechaFin) throws java.rmi.RemoteException;
    ArrayList partesEntrada(String codigo, String fechaIni, String fechaFin) throws java.rmi.RemoteException;
    byte actualizarCompradorReclamo(String material,String comprador,long numero) throws java.rmi.RemoteException;

    java.util.ArrayList grillaReclamos(String condicion) throws java.rmi.RemoteException;

    java.util.ArrayList equiposPorNumero(long numero) throws java.rmi.RemoteException;

    java.util.Vector numeroSap(String numero) throws java.rmi.RemoteException;

    java.util.Vector DetalleCantidadSap(long numero) throws java.rmi.RemoteException;
}
