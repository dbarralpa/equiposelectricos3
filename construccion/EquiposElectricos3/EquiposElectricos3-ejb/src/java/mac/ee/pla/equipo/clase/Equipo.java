/**
 * <p>T�tulo: Equipo.java<\p>
 * <p>Descripci�n:<\p> 
 * <p>Define equipos en general.<\p>
 * <p>Determina la forma de manejo del mapa de fabricaci�n y estructuras.<\p>
 * <p>Supone que el equipo se descompone en procesos.<\p>
 * <p>Cada proceso tiene su estructura y timming de fabricaci�n.<\p>
 * <p>Copyright: ND<\p>
 * <p>SCHAFFNER S.A.<\p>
 * <p>Historia:<\p>
 * <p>04/06/2007 Se definen los atributos y m�todos de acuerdo al modelo.<\p>
 * @autor JMF
 * @versi�n 1.0.0
 */

package mac.ee.pla.equipo.clase;


import java.sql.Date;
import java.util.Vector;
import sis.conexion.Conexion;


public class Equipo {
    
      
     private int np; // Nota de Pedido
     private int linea; // L�nea o item
     private String codPro;
     private String familia;
     private String kva;
     private String status;
     private int idMapa;
     private Date fechaInicio;
     private Vector vMapa;
     private float cantidad;
     private int fechaMaterial;
     private float ordenEquipo;
     private float stock;
     private Date fechaCliente;
     private Date fechaTermino;
     private Date fecha1;
     private String proceso;
     private Conexion myConexion=null;
     private int identificador;
     private String descripcion;
     private int nodoInicio;
     private int nodoFin;
     private int processTime;
     private int leadTime;
     private Date expeditacion;
     private Date readTime;
     private float stockMaximo;
     private float stockMinimo;
     private String serie;
     private int grupo;
     private long numPedido;
     private int estado;
     private float monto;
     private String nombreCliente;
     private String bitacora;
     private String area;
     private float precioMateriales;
     private float precioReal;
     private float diferencia;
     private String cliente;
     private String modelo;
     private String diseno;
     private byte standBy;
     private float consumo;
     private String reproceso;
     private String hidropack;
     
    

    public Equipo(){
        
    }
    
    /** Creates a new instance of Equipo */
    public Equipo(  String familia, String kva) {

        
    //Varios datos b�sicos
       
        
        this.setFamilia(familia);
        this.setKva(kva);
        hidropack = "";

        
      }
     public Equipo(  int np , int linea) {
         
     }
    public void setPrecioMateriales(float precioMateriales){
        this.precioMateriales = precioMateriales;
    }
    public float getPrecioMateriales(){
        return precioMateriales;
    }
    public void setPrecioReal(float precioReal){
        this.precioReal = precioReal;
    }
    public float getPrecioReal(){
        return precioReal;
    }
    public void setDiferencia(float diferencia){
        this.diferencia = diferencia;
    }
    public float getDiferencia(){
        return diferencia;
    }
    public String getArea(){
        return area;
    }
    public void setArea(String area){
        this.area = area;
    }
    public String getBitacora(){
        return bitacora;
    }
    
    public void setBitacora(String bitacora){
        this.bitacora = bitacora;
    }
    public String getNombreCliente(){
        return nombreCliente;
    }
    
    public void setNombreCliente(String nombreCliente){
        this.nombreCliente = nombreCliente;
    }

    public int getNp() {
        return np;
    }

    public void setNp(int np) {
        this.np = np;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public String getCodPro() {
        return codPro;
    }

    public void setCodPro(String codPro) {
        this.codPro = codPro;
    }

    public String getFamilia() {
        return familia;
    }

    public void setFamilia(String familia) {
        this.familia = familia;
    }

    public String getKva() {
        return kva;
    }

    public void setKva(String kva) {
        this.kva = kva;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getIdMapa() {
        return idMapa;
    }

    public void setIdMapa(int idMapa) {
        this.idMapa = idMapa;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }
    
    public Date getFecha1() {
        return fecha1;
    }

    public void setFecha1(Date fecha1) {
        this.fecha1 = fecha1;
    }

    public Vector getVMapa() {
        return vMapa;
    }

    public void setVMapa(Vector vMapa) {
        this.vMapa = vMapa;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public int getFechaMaterial() {
        return fechaMaterial;
    }

    public void setFechaMaterial(int fechaMaterial) {
        this.fechaMaterial = fechaMaterial;
    }

    public float getOrdenEquipo() {
        return ordenEquipo;
    }

    public void setOrdenEquipo(float ordenEquipo) {
        this.ordenEquipo = ordenEquipo;
    }

    public float getStock() {
        return stock;
    }

    public void setStock(float stock) {
        this.stock = stock;
    }

    public Date getFechaCliente() {
        return fechaCliente;
    }

    public void setFechaCliente(Date fechaCliente) {
        this.fechaCliente = fechaCliente;
    }

    public Date getFechaTermino() {
        return fechaTermino;
    }

    public void setFechaTermino(Date fechaTermino) {
        this.fechaTermino = fechaTermino;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public int getIdentificador() {
        return identificador;
    }

    public void setIdentificador(int identificador) {
        this.identificador = identificador;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getNodoInicio() {
        return nodoInicio;
    }

    public void setNodoInicio(int nodoInicio) {
        this.nodoInicio = nodoInicio;
    }

    public int getNodoFin() {
        return nodoFin;
    }

    public void setNodoFin(int nodoFin) {
        this.nodoFin = nodoFin;
    }

    public int getProcessTime() {
        return processTime;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }

    public int getLeadTime() {
        return leadTime;
    }

    public void setLeadTime(int leadTime) {
        this.leadTime = leadTime;
    }

    public Date getExpeditacion() {
        return expeditacion;
    }

    public void setExpeditacion(Date expeditacion) {
        this.expeditacion = expeditacion;
    }
    
     public float getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(float stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public float getStockMaximo() {
        return stockMaximo;
    }

    public void setStockMaximo(float stockMaximo) {
        this.stockMaximo = stockMaximo;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public long getNumPedido() {
        return numPedido;
    }

    public void setNumPedido(long numPedido) {
        this.numPedido = numPedido;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getDiseno() {
        return diseno;
    }

    public void setDiseno(String diseno) {
        this.diseno = diseno;
    }

    public byte getStandBy() {
        return standBy;
    }

    public void setStandBy(byte standBy) {
        this.standBy = standBy;
    }

    public float getConsumo() {
        return consumo;
    }

    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }

    public Date getReadTime() {
        return readTime;
    }

    public void setReadTime(Date readTime) {
        this.readTime = readTime;
    }

    public String getReproceso() {
        return reproceso;
    }

    public void setReproceso(String reproceso) {
        this.reproceso = reproceso;
    }

    public String getHidropack() {
        return hidropack;
    }

    public void setHidropack(String hidropack) {
        this.hidropack = hidropack;
    }

    
  
}
