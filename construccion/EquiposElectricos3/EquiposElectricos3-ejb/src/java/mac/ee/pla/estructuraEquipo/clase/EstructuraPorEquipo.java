/*
 * EstructuraPorEquipo.java
 *
 * Created on 10 de julio de 2007, 04:39 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.estructuraEquipo.clase;

import com.csvreader.CsvReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Vector;
import mac.ee.pla.equipo.clase.Equipo;
import sis.conexion.Conexion;
import sis.logger.Loger;
import java.util.ArrayList;

/**
 *
 * @author dbarra
 */
public class EstructuraPorEquipo {
    
    private String proceso;
    private String codigoMatPrima;
    private String codigoProducto;
    private float cantidad;
    private String unidadMedida;
    private String descripcion;
    private String fleje;
    private float stockMinimo;
    private float stockMaximo;
    private float stockBodega;
    private float stockBodega1;
    private float stockPlanta;
    private byte compara;
    private Date fechaConsumo;
    private double cantidadConsumo;
    private int fechaEnDias;
    private double desviacionStandartCantidad;
    private double desviacionStandartTiempo;
    private double mediaCantidad;
    private double mediaTiempo;
    private double raiz;
    private int leadTime;
    private float sumCanPen;
    private String estadoOrdenVencida;
    private String estadoOrdenFutura;
    private String fechaEntrega;
    private String prioridad;
    private Conexion myConexion= new Conexion();
    private Conexion mConexion= new Conexion();
    private Connection conMAC=null;
    private Connection conENS=null;
    private Statement st;
    private ResultSet rs;
    private String importancia;
    private String areaNegocio;
    private String estadoArea;
    private String comprador;
    private String usuario;
    private String observacion;
    private String observacionBodega;
    private String descripcionEstado;
    private float cantidadEntregada;
    private long numPedido;
    private int estado;
    private String area;
    private String familia;
    private int mes;
    private int ano3;
    private String bitacoraMaterial;
    private Timestamp fecha;
    private int tipoProducto;
    private long numDoc;
    private int linea;
    private float presupuesto;
    private float presupuesto1;
    private float real;
    private float real1;
    private float diferencia;
    private float diferencia1;
    private float ordenCompra;
    private String usuAprobar;
    private Date fechaCreacion;
    private String fechaIn;
    private String conjunto;
    private int analisis;
    private int anagen;
    private float consumo;
    private boolean eliminar;
    
         //Juan Hernandez - detalle cantidad Sap 
     private int CantidadRequerida;
     private int CantidadAsignada;
     
    
    
    Vector estructuraEquipo = new Vector();
    SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat formate = new SimpleDateFormat("yyyyMMdd");
    String fechaM = formateador.format(new java.util.Date());
    String fechaA = formate.format(new java.util.Date());
    Calendar fecha1 = Calendar.getInstance();
    String ano = fechaM.substring(0,4);
    String ano1 = fechaM.substring(0,4);
    public static Vector desviacionStandart = new Vector();
    Vector desviacion = new Vector();
    Vector stockMinMax = new Vector();
    Vector materialesEE = new Vector();
    /** Creates a new instance of EstructuraPorEquipo */
    public EstructuraPorEquipo()  {
        ano = String.valueOf(Integer.parseInt(fechaM.substring(0,4))-1);
        ano1 = ano+"-"+fechaM.substring(5,7)+"-"+fechaM.substring(8,10);
        ano = ano+fechaM.substring(5,7)+fechaM.substring(8,10);
        eliminar = false;
    }
    public EstructuraPorEquipo(String codigoMatPrima,String descripcion,String unidadMedida,float stockMinimo,float stockMaximo,int leadTime)  {
        this.codigoMatPrima = codigoMatPrima;
        this.descripcion = descripcion;
        this.unidadMedida = unidadMedida;
        this.stockMinimo = stockMinimo;
        this.stockMaximo = stockMaximo;
        this.leadTime = leadTime;
    }
    
    public void setUsuAprobar(String usuAprobar){
        this.usuAprobar = usuAprobar;
    }
    public String getUsuAprobar(){
        return usuAprobar;
    }
    public void setLinea(int linea){
        this.linea = linea;
    }
    public int getLinea(){
        return linea;
    }
    public void setNumDoc(long numDoc){
        this.numDoc = numDoc;
    }
    public long getNumDoc(){
        return numDoc;
    }
    public void setTipoProducto(int tipoProducto){
        this.tipoProducto = tipoProducto;
    }
    public int getTipoProducto(){
        return tipoProducto;
    }
    
    public void setFamilia(String familia){
        this.familia = familia;
    }
    
    public String getFamilia(){
        return familia;
    }
    
    public void setStockPlanta(float stockPlanta){
        this.stockPlanta = stockPlanta;
    }
    
    public float getStockPlanta(){
        return stockPlanta;
    }
    
    public void setArea(String area){
        this.area = area;
    }
    
    public String getArea(){
        return area;
    }
    
    public void setPrioridad(String prioridad){
        this.prioridad = prioridad;
    }
    
    public String getPrioridad(){
        return prioridad;
    }
    
    public void setEstado(int estado){
        this.estado = estado;
    }
    
    public int getEstado(){
        return estado;
    }
    
    public void setNumPedido(long numPedido){
        this.numPedido = numPedido;
    }
    
    public long getNumPedido(){
        return numPedido;
    }
    
    public void setStockBodega(float stockBodega){
        this.stockBodega = stockBodega;
    }
    
    public float getStockBodega(){
        return stockBodega;
    }
    
    public void setCantidadEntregada(float cantidadEntregada){
        this.cantidadEntregada = cantidadEntregada;
    }
    
    public float getCantidadEntregada(){
        return cantidadEntregada;
    }
    
    public void setDescripcionEstado(String descripcionEstado){
        this.descripcionEstado = descripcionEstado;
    }
    
    public String getDescripcionEstado(){
        return descripcionEstado;
    }
    
    public void setObservacion(String observacion){
        this.observacion = observacion;
    }
    
    public String getObservacion(){
        return observacion;
    }
    
    public void setObservacionBodega(String observacionBodega){
        this.observacionBodega = observacionBodega;
    }
    
    public String getObservacionBodega(){
        return observacionBodega;
    }
    
    public void setComprador(String comprador){
        this.comprador = comprador;
    }
    public String getComprador(){
        return comprador;
    }
    public void setEstadoArea(String estadoArea){
        this.estadoArea = estadoArea;
    }
    
    public String getEstadoArea(){
        return estadoArea;
    }
    
    public void setAreaNegocio(String areaNegocio){
        this.areaNegocio = areaNegocio;
    }
    
    public String getAreaNegocio(){
        return areaNegocio;
    }
    
    public void setImportancia(String importancia){
        this.importancia = importancia;
    }
    
    public String getImportancia(){
        return importancia;
    }
    
    public void setEstadoOrdenVencida(String estadoOrdenVencida){
        this.estadoOrdenVencida = estadoOrdenVencida;
    }
    
    public String getEstadoOrdenVencida(){
        return estadoOrdenVencida;
    }
    
    public void setEstadoOrdenFutura(String estadoOrdenFutura){
        this.estadoOrdenFutura = estadoOrdenFutura;
    }
    
    public String getEstadoOrdenFutura(){
        return estadoOrdenFutura;
    }
    
    public void setFechaEntrega(String fechaEntrega){
        this.fechaEntrega = fechaEntrega;
    }
    
    public String getFechaEntrega(){
        return fechaEntrega;
    }
    
    public float getSumarCanPen(){
        return sumCanPen;
    }
    
    public void setSumarCanPen(float sumCanPen){
        this.sumCanPen = sumCanPen;
    }
    
    public void setLeadTime(int leadTime){
        this.leadTime = leadTime;
    }
    
    public int getLeadTime(){
        return leadTime;
    }
    
    public void setMes(int mes){
        this.mes = mes;
    }
    
    public int getMes(){
        return mes;
    }
    
    public double getRaiz(){
        return raiz;
    }
    
    public void setRaiz(double raiz){
        this.raiz = raiz;
    }
    
    public double getMediaCantidad(){
        return mediaCantidad;
    }
    
    public void setMediaCantidad(double mediaCantidad){
        this.mediaCantidad = mediaCantidad;
    }
    
    public double getMediaTiempo(){
        return mediaTiempo;
    }
    
    public void setMediaTiempo(double mediaTiempo){
        this.mediaTiempo = mediaTiempo;
    }
    
    public double getDesviacionStandartCantidad(){
        return desviacionStandartCantidad;
    }
    
    public void setDesviacionStandartCantidad(double desviacionStandartCantidad){
        this.desviacionStandartCantidad = desviacionStandartCantidad;
    }
    
    public double getDesviacionStandartTiempo(){
        return desviacionStandartTiempo;
    }
    
    public void setDesviacionStandartTiempo(double desviacionStandartTiempo){
        this.desviacionStandartTiempo = desviacionStandartTiempo;
    }
    
    public Date getFechaConsumo(){
        return fechaConsumo;
    }
    
    public void setFechaConsumo(Date fechaConsumo){
        this.fechaConsumo = fechaConsumo;
    }
    
    public double getCantidadConsumo(){
        return cantidadConsumo;
    }
    
    public int getFechaEnDias(){
        return fechaEnDias;
    }
    
    public void setFechaEnDias(int fechaEnDias){
        this.fechaEnDias = fechaEnDias;
    }
    
    public void setCantidadConsumo(double cantidadConsumo){
        this.cantidadConsumo = cantidadConsumo;
    }
    
    public String getProceso() {
        return proceso;
    }
    
    public void setProceso(String proceso) {
        this.proceso = proceso;
    }
    
    public String getCodigoMatPrima() {
        return codigoMatPrima;
    }
    
    public void setCodigoMatPrima(String codigoMatPrima) {
        this.codigoMatPrima = codigoMatPrima;
    }
    
    public String getCodigoProducto() {
        return codigoProducto;
    }
    
    public void setCodigoProducto(String codigoProducto) {
        this.codigoProducto = codigoProducto;
    }
    
    public float getCantidad() {
        return cantidad;
    }
    
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    public String getUnidadMedida() {
        return unidadMedida;
    }
    
    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }
    
    public String getDescripcion() {
        return descripcion;
    }
    
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getBitacoraMaterial() {
        return bitacoraMaterial;
    }
    
    public void setBitacoraMaterial(String bitacoraMaterial) {
        this.bitacoraMaterial = bitacoraMaterial;
    }
    
    public Timestamp getFechaBitacora() {
        return fecha;
    }
    
    public void setFechaBitacora(Timestamp fecha) {
        this.fecha = fecha;
    }
    
    public String getFleje() {
        return fleje;
    }
    
    public void setFleje(String fleje) {
        this.fleje = fleje;
    }
    
    public float getStockMinimo() {
        return stockMinimo;
    }
    
    public void setStockMinimo(float stockMinimo) {
        this.stockMinimo = stockMinimo;
    }
    
    public float getStockMaximo() {
        return stockMaximo;
    }
    
    public void setStockMaximo(float stockMaximo) {
        this.stockMaximo = stockMaximo;
    }
    
    public byte getCompara() {
        return compara;
    }
    
    public void setCompara(byte compara) {
        this.compara = compara;
    }
    
    public String getUsuario(){
        return usuario;
    }
    
    public void setUsuario(String usuario){
        this.usuario = usuario;
    }
    
    public int getAno(){
        return ano3;
    }
    
    public void setAno(int ano3){
        this.ano3 = ano3;
    }
    
    public int getCantidadRequerida() {
        return CantidadRequerida;
    }

    public void setCantidadRequerida(int CantidadRequerida) {
        this.CantidadRequerida = CantidadRequerida;
    }

    public int getCantidadAsignada() {
        return CantidadAsignada;
    }

    public void setCantidadAsignada(int CantidadAsignada) {
        this.CantidadAsignada = CantidadAsignada;
    }
    
//------------------------------------------------------------------------------------------------------------------------------
    public Vector estructuraEquipo(String codPro){
        
        
        try{
            
            conENS = myConexion.getENSConnector();
            st = conENS.createStatement();
            String query = " SELECT DISTINCT(ME.CodigoTerminado), ME.CodigoSemiElaborado ,ME.CodigoMatPrima ,ME.CantidadMP"
                    +" FROM  ENSchaffner.dbo.MEstructura ME , ENSchaffner.dbo.TPMP ENS"
                    +" WHERE ME.CodigoTerminado Like 'T%' and  ME.CodigoTerminado = ENS.Codpro AND ME.CodigoMatPrima = '" + codPro + "' and ENS.Status <> 'Bodega' AND ENS.Familia <> 'REP' AND ENS.Status IS NOT NULL AND ENS.Activa != 1";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCodigoProducto(rs.getString(1));
                if(rs.getString(2).equals("BTE")){
                    String proceso = "BAT";
                    estructura.setProceso(proceso);
                } else {estructura.setProceso(rs.getString(2));}
                estructura.setCodigoMatPrima(rs.getString(3));
                estructura.setCantidad(rs.getFloat(4));
                estructuraEquipo.addElement(estructura);
            }
            rs.close();
            st.close();
            conENS.close();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo estructuraEquipo() " +  e.getMessage(),0);
        }
        return estructuraEquipo;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public Vector retornarEstructuraEquipo(){
        return estructuraEquipo;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public boolean saveEstadoMaterial(String codigoMaterial , String descripcion){
        int contador = 0;
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 = "SELECT COUNT(DISTINCT codigoMaterial) FROM MAC.dbo.MaterialSeleccionado where codigoMaterial = '" + codigoMaterial + "'";
            rs = st.executeQuery(query4);
            while(rs.next()){
                
                contador =  rs.getInt(1);
            }
            if(contador == 0){
                String query1 = "insert into MAC.dbo.MaterialSeleccionado (codigoMaterial,descripcion) values ('"+ codigoMaterial +"','"+ descripcion +"')";
                st.execute(query1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: "," m�todo saveEstadoMaterial() " +  e.getMessage(),0);
            return false;
        }
        
        return true;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public boolean deleteMaterialDescartado(String codigoMaterial){
        int contador = 0;
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 = "SELECT COUNT(DISTINCT codigoMaterial) FROM MAC.dbo.MaterialSeleccionado where codigoMaterial = '" + codigoMaterial + "'";
            rs = st.executeQuery(query4);
            while(rs.next()){
                
                contador =  rs.getInt(1);
            }
            if(contador > 0){
                String query1 = "DELETE FROM MAC.dbo.MaterialSeleccionado  WHERE codigoMaterial = '"+ codigoMaterial +"'";
                st.execute(query1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo deleteMaterialDescartado() " +  e.getMessage(),0);
            return false;
        }
        
        return true;
    }
//-------------------------------------------------------------------------------------------------------------------------------
/* public void materialesStockMinMax(){
 
      try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
 
 
         String query = " SELECT t.Codigo,t.descripcion,t.Unimed,t.StockMinimo,t.StockMaximo,l.Cantidad,m.importancia,t.area "
                       +" FROM MAC.dbo.TConsumoPromedio t "
                       +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a "
                       +" ON ( t.Codigo = a.CodigoMatPrima ) "
                       +" LEFT OUTER JOIN MAC.dbo.MArticulo b "
                       +" ON (t.Codigo = b.CODIGO) "
                       +" LEFT OUTER JOIN MAC.dbo.LeadTime l "
                       +" ON (t.Codigo = l.Codigo) "
                       +" LEFT OUTER JOIN MAC.dbo.MaterialesPorImportancia  m "
                       +" ON (t.Codigo = m.codigo) "
                       +" WHERE a.CodigoMatPrima is null and (t.area <> 'EE' or t.area is null) "
                       +" GROUP BY t.Codigo,t.descripcion,t.Unimed,t.StockMinimo,t.StockMaximo,l.Cantidad,m.importancia,t.area";
         rs = st.executeQuery(query);
         int minMax = 0;
         while(rs.next()){
            //String area = "EE";
           // if(!area.equals(rs.getString(8))){
               EstructuraPorEquipo estructura = new EstructuraPorEquipo();
               estructura.setCodigoMatPrima(rs.getString(1));
               estructura.setDescripcion(rs.getString(2));
               estructura.setUnidadMedida(rs.getString(3));
               estructura.setStockMinimo(rs.getFloat(4));
               estructura.setStockMaximo(rs.getFloat(5));
               estructura.setLeadTime(rs.getInt(6));
               estructura.setImportancia(rs.getString(7));
               if(rs.getString(8) != null && !rs.getString(8).equals("NULL")){
               estructura.setAreaNegocio(rs.getString(8));
               }else{
                estructura.setAreaNegocio("nada");
               }
               stockMinMax.addElement(estructura);
           // }
               minMax++;
    }
         new Loger().logger("EstructuraPorEquipo.class"," cantidad de materiales minMax1 " + String.valueOf(minMax),1);
        rs.close();
        st.close();
        conMAC.close();
         }catch(Exception e){
             new Loger().logger("EstructuraPorEquipo.class: ","m�todo materialesStockMinMax1() " +  e.getMessage(),0);
         }
 }*/
    public void materialesStockMinMax1(){
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
         /*String query = " SELECT t.Codigo,t.descripcion,t.Unimed,t.StockMinimo,t.StockMaximo,t.leadTime,t.raiz,t.mediaCantidad "
                       +" FROM MAC.dbo.TConsumoPromedio t "
                       +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a   "
                       +" ON ( t.Codigo = a.CodigoMatPrima ) "
                       +" WHERE a.CodigoMatPrima is null ORDER BY t.Codigo";
          */
            String query = " SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,lt.LeadTime "
                    +" FROM SiaSchaffner.dbo.STO_PRODUCTO  t "
                    +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a   "
                    +" ON ( t.PRO_CODPROD = a.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +" LEFT OUTER JOIN  MAC.dbo.ProductoAnexo lt "
                    +" on(t.PRO_CODPROD = lt.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +" WHERE a.CodigoMatPrima is null "
                    +" and t.PRO_CODTIPO in (1,5,8)"
                    +" ORDER BY t.PRO_CODPROD ";
            rs = st.executeQuery(query);
            int minMax = 0;
            while(rs.next()){
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setDescripcion(rs.getString(2));
                estructura.setUnidadMedida(rs.getString(3));
                estructura.setStockMinimo(rs.getFloat(4));
                estructura.setStockMaximo(rs.getFloat(5));
                estructura.setLeadTime(rs.getInt(6));
                //estructura.setRaiz(rs.getDouble(7));
                //estructura.setMediaCantidad(rs.getDouble(8));
                stockMinMax.addElement(estructura);
                minMax++;
            }
            new Loger().logger("EstructuraPorEquipo.class"," cantidad de materiales minMax " + String.valueOf(minMax),1);
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo materialesStockMinMax() " +  e.getMessage(),0);
        }
    }
    //-----------------------------------------------------------------------------------------------------------------------------
    
    public Vector retornarStockMinMax(){
        return stockMinMax;
    }
    
    public void minMaxVacio(Vector vacio){
        stockMinMax = vacio;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public Vector materialesDescartados(){
        Vector materialesDescartados = new Vector();
        
        try{
            
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query = " SELECT codigoMaterial,descripcion"
                    +" FROM MAC.dbo.MaterialSeleccionado"
                    +" ORDER BY codigoMaterial";
            
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setDescripcion(rs.getString(2));
                materialesDescartados.addElement(estructura);
            }
            
            rs.close();
            st.close();
            conMAC.close();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo materialesDescartados() " +  e.getMessage(),0);
        }
        return materialesDescartados;
    }
    
    //-------------------------------------------------------------------------------------------------------------------------------
    public Vector materiales(){
        Vector materiales = new Vector();
        
        try{
            
            conENS = myConexion.getENSConnector();
            st = conENS.createStatement();
             /*String query = "SELECT DISTINCT (a.CodigoMatPrima), b.DESCRIPCION ,b.UNIMED"
                           +" FROM ENSchaffner.dbo.MEstructura a"
                           +" LEFT OUTER JOIN MAC.dbo.MArticulo b"
                           +" ON (a.CodigoMatPrima = b.CODIGO)"
                           +" LEFT OUTER JOIN MAC.dbo.MaterialSeleccionado c"
                           +" ON (a.CodigoMatPrima =  c.codigoMaterial)"
                           +" WHERE c.codigoMaterial is null "
                        //   +" AND (a.CodigoMatPrima like '2%' or a.CodigoMatPrima like '1%' or a.CodigoMatPrima like '504%')"
                           +" ORDER BY a.CodigoMatPrima";*/
            String query = " SELECT DISTINCT (a.CodigoMatPrima),b.PRO_DESC,b.PRO_UMPRINCIPAL "
                    +" FROM ENSchaffner.dbo.MEstructura a "
                    +" LEFT OUTER JOIN SiaSchaffner.dbo.STO_PRODUCTO b "
                    +" ON (a.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = b.PRO_CODPROD) "
                    +" LEFT OUTER JOIN MAC.dbo.MaterialSeleccionado c "
                    +" ON (a.CodigoMatPrima =  c.codigoMaterial) "
                    +" WHERE c.codigoMaterial is null "
                    +" ORDER BY a.CodigoMatPrima ";
            
            
            /*String query = "SELECT CodigoMatPrima,Descripcion,fleje FROM MAC.dbo.MaterialAS";*/
            
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                String caracter = "";
                if(rs.getString(2) != null){
                    caracter = rs.getString(2);
                    for(int i=0;i<rs.getString(2).length();i++){
                        if(rs.getString(2).charAt(i) == '"'){
                            caracter = rs.getString(2).replace('"',' ');
                        }
                    }
                    estructura.setDescripcion(caracter);
                }else{
                    estructura.setDescripcion("SIN DESCRIPCION");
                }
                estructura.setUnidadMedida(rs.getString(3));
                materiales.addElement(estructura);
            }
            
            rs.close();
            st.close();
            conENS.close();
        } catch(Exception e){
            myConexion.cerrarConENS();
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo materiales() " +  e.getMessage(),0);
        }
        return materiales;
    }
    //--------------------------------------------------------------------------------------------------------------------------------
    public boolean verificarMaterial(String codigo){
        boolean material = false;
        
        try{
            
            conENS = myConexion.getENSConnector();
            st = conENS.createStatement();
            
            String query = "select count(codigoMaterial) from MAC.dbo.MaterialSeleccionado where codigoMaterial = '" + codigo + "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                if(rs.getInt(1)>0){
                    material = true;
                }
                
            }
            
            rs.close();
            st.close();
            conENS.close();
        } catch(Exception e){
            myConexion.cerrarConENS();
            new Loger().logger("EstructuraPorEquipo.class "," verificarMaterial() " +  e.getMessage(),0);
        }
        return material;
    }
    
//---------------------------------------------------------------------------------------------------------------------------------
    // public void materialesEE(){
    
    //try{
    
    //conENS = myConexion.getENSConnector();
    //st = conENS.createStatement();
            /* String query = " SELECT DISTINCT (a.CodigoMatPrima),b.DESCRIPCION ,b.UNIMED,t.StockMinimo ,t.StockMaximo,l.Cantidad,m.importancia "
                           +" FROM ENSchaffner.dbo.MEstructura a "
                           +" LEFT OUTER JOIN MAC.dbo.MArticulo b "
                           +" ON (a.CodigoMatPrima = b.CODIGO) "
                           +" LEFT OUTER JOIN MAC.dbo.TConsumoPromedio t "
                           +" ON (a.CodigoMatPrima = t.Codigo) "
                           +" LEFT OUTER JOIN MAC.dbo.LeadTime l "
                           +" ON (a.CodigoMatPrima = l.Codigo) "
                           +" LEFT OUTER JOIN MAC.dbo.MaterialesPorImportancia  m "
                           +" ON (a.CodigoMatPrima = m.codigo) "
                           +" ORDER BY a.CodigoMatPrima";
             */
    
           /*  String query = " SELECT DISTINCT (a.CodigoMatPrima),b.DESCRIPCION ,b.UNIMED,t.StockMinimo ,t.StockMaximo,l.Cantidad,m.importancia,t.area  "
                           +" FROM ENSchaffner.dbo.MEstructura a  "
                           +" LEFT OUTER JOIN MAC.dbo.MArticulo b "
                           +" ON (a.CodigoMatPrima = b.CODIGO) "
                           +" LEFT OUTER JOIN MAC.dbo.TConsumoPromedio t "
                           +" ON (a.CodigoMatPrima = t.Codigo) "
                           +" LEFT OUTER JOIN MAC.dbo.LeadTime l "
                           +" ON (a.CodigoMatPrima = l.Codigo) "
                           +" LEFT OUTER JOIN MAC.dbo.MaterialesPorImportancia  m "
                           +" ON (a.CodigoMatPrima = m.codigo) ";
                          // +" union "
              int EE = 0;
                            rs = st.executeQuery(query);
                             while(rs.next()){
                             EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                             estructura.setCodigoMatPrima(rs.getString(1));
                             estructura.setDescripcion(rs.getString(2));
                             estructura.setUnidadMedida(rs.getString(3));
                             estructura.setStockMinimo(rs.getFloat(4));
                             estructura.setStockMaximo(rs.getFloat(5));
                             estructura.setLeadTime(rs.getInt(6));
                             estructura.setImportancia(rs.getString(7));
                             estructura.setAreaNegocio("EE");
                             estructura.setEstadoArea("no");
                             materialesEE.addElement(estructura);
                             EE++;
                            }
                            rs.close();
            
            String query1 = " SELECT t.Codigo,t.descripcion,t.Unimed,t.StockMinimo,t.StockMaximo,l.Cantidad,m.importancia,t.area "
                           +" FROM MAC.dbo.TConsumoPromedio t "
                           +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a  "
                           +" ON ( t.Codigo = a.CodigoMatPrima ) "
                           +" LEFT OUTER JOIN MAC.dbo.MArticulo b "
                           +" ON (t.Codigo = b.CODIGO) "
                           +" LEFT OUTER JOIN MAC.dbo.LeadTime l   "
                           +" ON (t.Codigo = l.Codigo) "
                           +" LEFT OUTER JOIN MAC.dbo.MaterialesPorImportancia  m "
                           +" ON (t.Codigo = m.codigo) "
                           +" WHERE a.CodigoMatPrima is null and (t.area = 'EE') "
                           +" GROUP BY t.Codigo,t.descripcion,t.Unimed,t.StockMinimo,t.StockMaximo,l.Cantidad,m.importancia,t.area";
             rs = st.executeQuery(query1);
            
             while(rs.next()){
             EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
             estructura.setCodigoMatPrima(rs.getString(1));
             estructura.setDescripcion(rs.getString(2));
             estructura.setUnidadMedida(rs.getString(3));
             estructura.setStockMinimo(rs.getFloat(4));
             estructura.setStockMaximo(rs.getFloat(5));
             estructura.setLeadTime(rs.getInt(6));
             estructura.setImportancia(rs.getString(7));
             estructura.setAreaNegocio("EE");
             estructura.setEstadoArea("si");
             materialesEE.addElement(estructura);
             EE++;
            }
             rs.close();
             st.close();
             conENS.close();
              new Loger().logger("TareaPlanificada.class"," cantidad de materiales EE " + String.valueOf(EE),1);
         } catch(Exception e){
           new Loger().logger("EstructuraPorEquipo.class: ","m�todo materiales() " +  e.getMessage(),0);
       }
     }
    public void materialesEE1(){
            
              try{
            
             conENS = myConexion.getENSConnector();
             st = conENS.createStatement();
             String query = " SELECT DISTINCT (a.CodigoMatPrima),b.DESCRIPCION ,b.UNIMED,t.StockMinimo ,t.StockMaximo,t.leadTime,t.raiz,t.mediaCantidad "
                           +" FROM ENSchaffner.dbo.MEstructura a  "
                           +" LEFT OUTER JOIN MAC.dbo.MArticulo b "
                           +" ON (a.CodigoMatPrima = b.CODIGO) "
                           +" LEFT OUTER JOIN MAC.dbo.TConsumoPromedio t  "
                           +" ON (a.CodigoMatPrima = t.Codigo) "
                           +" WHERE a.CodigoMatPrima NOT LIKE '9%' ORDER BY a.CodigoMatPrima";
                           int EE = 0;
                            rs = st.executeQuery(query);
                             while(rs.next()){
                             EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                             estructura.setCodigoMatPrima(rs.getString(1));
                             if(rs.getString(2) == null){
                             estructura.setDescripcion("Sin descripci�n");
                             }else{
                             estructura.setDescripcion(rs.getString(2));
                             }
                             estructura.setUnidadMedida(rs.getString(3));
                             estructura.setStockMinimo(rs.getFloat(4));
                             estructura.setStockMaximo(rs.getFloat(5));
                             estructura.setLeadTime(rs.getInt(6));
                             estructura.setRaiz(rs.getDouble(7));
                             estructura.setMediaCantidad(rs.getDouble(8));
                             materialesEE.addElement(estructura);
                             EE++;
                            }
             rs.close();
             st.close();
             conENS.close();
              new Loger().logger("TareaPlanificada.class"," cantidad de materiales EE " + String.valueOf(EE),1);
         } catch(Exception e){
           new Loger().logger("EstructuraPorEquipo.class: ","m�todo materiales() " +  e.getMessage(),0);
       }
     }
//-------------------------------------------------------------------------------------------------------------------------------
            
   public Vector retornarMaterialesEE(){
       return materialesEE;
   }
   public void  materialesEEvacio(Vector vacio){
       materialesEE = vacio;
   }*/
    
//---------------------------------------------------------------------------------------------------------------------------------
    public float retornarStockMin(String material){
        float stockMinMax = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = "SELECT StockMinimo FROM MAC.dbo.TConsumoPromedio WHERE Codigo = '" + material +  "'";
            String query = "SELECT PRO_INVMIN FROM SiaSchaffner.dbo.STO_PRODUCTO WHERE PRO_CODPROD = '" + material +  "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                stockMinMax = rs.getFloat(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo retornarStockMin() " +  e.getMessage(),0);
        }
        return stockMinMax;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public float retornarStockMax(String material){
        float stockMinMax = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = "SELECT StockMaximo FROM MAC.dbo.TConsumoPromedio WHERE Codigo = '" + material +  "'";
            String query = "SELECT PRO_INVMAX FROM SiaSchaffner.dbo.STO_PRODUCTO WHERE PRO_CODPROD = '" + material +  "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                stockMinMax = rs.getFloat(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo retornarStockMax() " +  e.getMessage(),0);
        }
        return stockMinMax;
    }
//---------------------------------------------------------------------------------------------------------------------------------
    public short leadTime(String material){
        short leadTime = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query = "SELECT leadTime FROM MAC.dbo.TConsumoPromedio WHERE Codigo = '" + material + "'";
            String query = "SELECT LeadTime FROM MAC.dbo.ProductoAnexo WHERE Codigo = '" + material + "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                leadTime = rs.getShort(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo leadTime() " +  e.getMessage(),0);
        }
        return leadTime;
    }
//---------------------------------------------------------------------------------------------------------------------------------
    public short diferenciaDiasLeadTime(String inicio , short leadTime){
        long lead = 0;
        long result =0;
        Date nuevaFecha = new Date(0);
        try{
            inicio = inicio.substring(6,10) +"-"+ inicio.substring(3,5) +"-"+ inicio.substring(0,2);
            nuevaFecha = nuevaFecha.valueOf(inicio);
            result = nuevaFecha.getTime() - new Date(0).valueOf(fechaM).getTime();
            lead = result / 86400000;
            
         /*System.out.println(leadTime);
         System.out.println(lead);
         System.out.println(lead - leadTime);*/
            lead -= leadTime;
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo diferenciaDiasLeadTime() " +  e.getMessage(),0);
        }
        return (short)lead;
    }
//--------------------------------------------------------------------------------------------------------------------
 /* public static void main(String []args){
      //new EstructuraPorEquipo().diferenciaDiasLeadTime(new Date(0).valueOf("2008-05-13"),(short)70);
     EstructuraPorEquipo estru =  new EstructuraPorEquipo();
     estru.desviacionStandartPorMaterial("1091510035");
     if(estru.datosParaDesviacionStandard().size()>0){
         estru.calcularEnDiasConsumo();
         estru.desviacionStandard();
     }
  
  
  
  }*/
//00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  /*public void desviacionStandartPorMaterial(String material){
      try{
        conMAC = mConexion.getMACConnector();
        st = conMAC.createStatement();*/
    //String query = "select FECHA,SUM(CANT) FROM MAC.dbo.STOMOV WHERE CODIGO = '" + material + "' AND TIPO = 4 AND FECHA BETWEEN '" + formate.format(new Date(0).valueOf(date1)) + "' AND '" + formate.format(new Date(0).valueOf(date)) + "'  group by FECHA order by FECHA DESC";
        /*String query =   "SELECT b.FechaTermino,sum(d.CantidadMP) "
                        +"FROM ENSchaffner.dbo.TPMP a "
                        +"LEFT OUTER JOIN ENSchaffner.dbo.TAvance b "
                        +"ON (a.Pedido = b.Pedido and a.Linea = b.Linea) "
                        +"LEFT OUTER JOIN  ENSchaffner.dbo.MEstructura d "
                        +"ON (d.CodigoTerminado = a.Codpro and d.CodigoSemiElaborado = b.Semielaborado) "
                        +"WHERE b.FechaTermino between '" + formate.format(new Date(0).valueOf(date1)) + "'  AND  '" + formate.format(new Date(0).valueOf(date)) + "' AND d.CodigoMatPrima is not null and CodigoMatPrima = '" + material + "'"
                        +"GROUP BY b.FechaTermino ORDER BY b.FechaTermino";
         */
      /*  String query =  " SELECT fecha,sum(cantidad) "
                       +" FROM MAC.dbo.consumosPorMaterial "
                       +" WHERE fecha between '" + ano + "'  AND  '" + formate.format(new Date(0).valueOf(fechaM)) + "' and codigo = '" + material + "' "
                       +" GROUP BY fecha ORDER BY fecha";
        // new Loger().logger("EstructuraPorEquipo.class: ",query,1);
        rs = st.executeQuery(query);
        while(rs.next()){
               EstructuraPorEquipo estru = new EstructuraPorEquipo();
               estru.setFechaConsumo(rs.getDate(1));
               estru.setCantidadConsumo(rs.getFloat(2));
               desviacionStandart.addElement(estru);
           }
        rs.close();
        st.close();
        conMAC.close();
         }catch(Exception e){
             new Loger().logger("EstructuraPorEquipo.class: ","m�todo desviacionStandart() " +  e.getMessage(),0);
         }
  }
       
  public void calcularEnDiasConsumo(){
      long fecha=0;
      long fecha1=0;
       
       
      fecha1 = ( new Date(0).valueOf(fechaM).getTime() - ((EstructuraPorEquipo)desviacionStandart.elementAt(desviacionStandart.size()-1)).getFechaConsumo().getTime())/86400000;
      fecha = (((EstructuraPorEquipo)desviacionStandart.elementAt(0)).getFechaConsumo().getTime() - new Date(0).valueOf(ano1).getTime())/86400000;
      ((EstructuraPorEquipo)desviacionStandart.elementAt(0)).setFechaEnDias((int)fecha + (int)fecha1);
       
       
      /*System.out.println(((EstructuraPorEquipo)desviacionStandart.elementAt(0)).getFechaConsumo());
      System.out.println(((EstructuraPorEquipo)desviacionStandart.elementAt(desviacionStandart.size()-1)).getFechaConsumo());
      System.out.println(fecha1);
      System.out.println(fecha);
      System.out.println(new Date(0).valueOf(date).getTime());
      System.out.println(new Date(0).valueOf(date1).getTime());
       */
    
 /*    for(int i=0;i<desviacionStandart.size();i++){
          if(i < desviacionStandart.size()-1){
             fecha = (((EstructuraPorEquipo)desviacionStandart.elementAt(i+1)).getFechaConsumo().getTime() - ((EstructuraPorEquipo)desviacionStandart.elementAt(i)).getFechaConsumo().getTime())/86400000;
             ((EstructuraPorEquipo)desviacionStandart.elementAt(i+1)).setFechaEnDias((int)fecha);
          }
      }
  }
  
  public Vector datosParaDesviacionStandard(){
      return desviacionStandart;
  }
  
  public void desviacionStandard(){
      double cantidad = 0;
      double tiempo = 0;
      double sumMedia = 0;
      double sumMedia1 = 0;
      double result = 0;
      double raiz = 0;
  
     /* double sumarCantidad = sumarCantidad(desviacionStandart);
                        System.out.println(" sumar Cantidad : " + sumarCantidad);*/
  /*    EstructuraPorEquipo estru = new EstructuraPorEquipo();
      double mediaCantidad = mediaCantidad(desviacionStandart);
                        //System.out.println(" media Cantidad : " + mediaCantidad);
      estru.setMediaCantidad(mediaCantidad);
      double mediaTiempo = mediaTiempo(desviacionStandart);
                        //System.out.println(" media Tiempo : " + mediaTiempo);
      estru.setMediaTiempo(mediaTiempo);
   
   
       for ( int i=0; i<desviacionStandart.size(); i++ ){
       final double v = ((EstructuraPorEquipo)desviacionStandart.elementAt(i)).getCantidadConsumo()/sumarCantidad(desviacionStandart) - mediaCantidad;
       sumMedia += v * v;
       }
   
       cantidad = Math.sqrt( sumMedia / desviacionStandart.size() );
       //System.out.println("Desviaci�n standard de la cantidad " + cantidad);
       estru.setDesviacionStandartCantidad(cantidad);
   
   
   
       for ( int i=0; i<desviacionStandart.size(); i++ ){
       final double v = ((EstructuraPorEquipo)desviacionStandart.elementAt(i)).getFechaEnDias()/sumarTiempo(desviacionStandart) - mediaTiempo;
       sumMedia1 += v * v;
             }
   
       tiempo = Math.sqrt( sumMedia1 / desviacionStandart.size() );
       //System.out.println("Desviaci�n standard del tiempo " + tiempo);
       estru.setDesviacionStandartTiempo(tiempo);
       cantidad = cantidad * cantidad;
       tiempo = tiempo * tiempo;
       result = cantidad + tiempo;
       raiz = Math.sqrt(result/desviacionStandart.size());
       //System.out.println("raiz: " + raiz);
       estru.setRaiz(raiz);
       estru.setCantidadConsumo(desviacionStandart.size());
      // System.out.println("veces que se consum�o " + desviacionStandart.size());
       desviacion.addElement(estru);
   
  }
   
  public Vector retornarDesviacionStandart(){
      return desviacion;
  }
   
  private double sumarCantidad(Vector arr){
      double sum = 0.0;
          for(int i=0;i<arr.size();i++){
               sum +=((EstructuraPorEquipo)arr.elementAt(i)).getCantidadConsumo();
             }
      return sum;
  }
   
  private double sumarTiempo(Vector arr){
      double sum = 0.0;
          for(int i=0;i<arr.size();i++){
               sum +=((EstructuraPorEquipo)arr.elementAt(i)).getFechaEnDias();
             }
      return sum;
  }
   
  private  double mediaCantidad(Vector arr){
      int a=0;
      double sum = 0.0;
   
                 for(int i=0;i<arr.size();i++){
                     sum +=((EstructuraPorEquipo)arr.elementAt(i)).getCantidadConsumo()/sumarCantidad(arr);
                     //sum +=((Equipo1)arr.elementAt(i)).getNp();
                     //System.out.println(((EstructuraPorEquipo)arr.elementAt(i)).getCantidadConsumo()/sumarCantidad(arr));
                  }
                //System.out.println(sum / arr.size());
                return sum / arr.size();
        }
  private  double mediaTiempo(Vector arr){
      int a=0;
      double sum = 0.0;
   
                 for(int i=0;i<arr.size();i++){
                     sum +=((EstructuraPorEquipo)arr.elementAt(i)).getFechaEnDias()/sumarTiempo(arr);
                     //System.out.println(((EstructuraPorEquipo)arr.elementAt(i)).getFechaEnDias()/sumarTiempo(arr));
                  }
   
                return sum / arr.size();
        }*/
//00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
    
//----------------------------------------------------------------------------------------------------------------------------
    public byte actualizarLeadTime(String material , short leadTime){
        int count =0;
        byte verificador=0;
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            
            String query3 = "UPDATE MAC.dbo.ProductoAnexo SET LeadTime = " + leadTime + " WHERE Codigo = '" + material + "'";
            st.execute(query3);
            String query4 = "UPDATE MAC.dbo.ComprasRequeridas SET leadTime = " + leadTime + " WHERE Codigo = '" + material + "'";
            st.execute(query4);
            conMAC.commit();
            conMAC.setAutoCommit(true);
            verificador = 1;
            
            
            st.close();
            conMAC.close();
        }catch(Exception e){
            verificador = 0;
            new Loger().logger("MrpBean.class " , " ERROR: actualizarLeadTime(): "+e.getMessage(),0);
        }
        return verificador;
    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    
    public byte actualizarStockMinMax(String material,float stockMinimo,float stockMaximo){
        byte verificador = (byte)0;
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            st = conMAC.createStatement();
            //String query3 = "UPDATE MAC.dbo.TConsumoPromedio SET StockMaximo = " + stockMaximo + " , StockMinimo = " + stockMinimo + " WHERE Codigo = '" + material + "'";
            String query3 = "UPDATE SiaSchaffner.dbo.STO_PRODUCTO SET PRO_INVMAX = " + stockMaximo + " , PRO_INVMIN = " + stockMinimo + " WHERE PRO_CODPROD = '" + material + "'";
            st.execute(query3);
            String query4 = "UPDATE MAC.dbo.ComprasRequeridas SET stockMax = " + stockMaximo + " , stockMin = " + stockMinimo + " WHERE Codigo = '" + material + "'";
            st.execute(query4);
            conMAC.commit();
            conMAC.setAutoCommit(true);
            verificador = (byte)1;
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: actualizarStockMinMax (): "+e.getMessage(),0);
        }
        return verificador;
    }
//---------------------------------------------------------------------------------------------------------------------------------
    public Vector materialesNoMrp(){
        Vector materialesNoMrp  = new Vector();
        int veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
            String query1 = "   SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX, "
                    +"   ( "
                    +"   (select sum(b.PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL b where t.PRO_CODPROD  = b.PSL_CodProd and b.PSL_CodBodega <> 7) "
                    +"   ) as Stock "
                    +"   ,proa.LeadTime,sum(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN ,proa.Comprador "
                    +"   FROM SiaSchaffner.dbo.STO_PRODUCTO t "
                    +"   LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a "
                    +"   ON ( t.PRO_CODPROD = a.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +"   LEFT OUTER JOIN SiaSchaffner.dbo.STO_MOVDET tc "
                    +"   ON (t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = tc.MVD_CodProd "
                    +"   and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1) "
                    +"   LEFT OUTER JOIN MAC.dbo.ProductoAnexo proa "
                    +"   on(t.PRO_CODPROD  = proa.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) "
                    +"   WHERE a.CodigoMatPrima is NULL AND t.PRO_INVMIN > 0 and proa.Estado = 0  "
                    +"   and t.PRO_CODTIPO in (1,5,8) "
                    +"   GROUP BY t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,proa.LeadTime,proa.Comprador ";
            // new Loger().logger("EstructuraPorEquipo.class " , "materialesNoMrp : " + query1,1);
            rs = st.executeQuery(query1);
            while(rs.next()){
                float cantidad = 0;
                if((rs.getFloat(6) + rs.getFloat(8)) < rs.getFloat(4)){
                    cantidad = rs.getFloat(5) - (rs.getFloat(6) + rs.getFloat(8));
                    EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                    materiales.setCodigoMatPrima(rs.getString(1));
                    materiales.setDescripcion(rs.getString(2));
                    materiales.setCantidad(cantidad);
                    materiales.setUnidadMedida(rs.getString(3));
                    materiales.setLeadTime(rs.getInt(7));
                    materiales.setStockMinimo(rs.getFloat(4));
                    materiales.setStockMaximo(rs.getFloat(5));
                    materiales.setComprador(rs.getString(9));
                    materialesNoMrp.addElement(materiales);
                    veri++;
                }
            }
            rs.close();
            st.close();
            conMAC.close();
            new Loger().logger("EstructuraPorEquipo.class " , "Cantidad de materialesNoMrp: " + String.valueOf(veri),1);
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class " , "ERROR materialesNoMrp(): " + e.getMessage(),0);
        }
        return materialesNoMrp;
    }
//---------------------------------------------------------------------------------------------------------------------------------
    public Vector necesidadDeCompra(){
        Vector materialesNoMrp  = new Vector();
        Vector analisisMateriales  = new Vector();
        int veri = 0;
        Statement state = null;
        try{
            conMAC = mConexion.getMACConnector();
            conMAC.setAutoCommit(false);
            state = conMAC.createStatement();
            

            /*String query1 = " SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,isnull(t.PRO_INVMIN,0) as StockMax,isnull(t.PRO_INVMAX,0) as StockMin,b.Saldo";
            query1 += " ,proa.LeadTime,sum(isnull(tc.MVD_Cant,0)-isnull(tc.MVD_CantAsignada,0)) as CAN_PEN ,proa.Comprador, ";
            query1 += " proa.analisis,proa.anaGen, ";
            query1 += " isnull((SELECT sum(me.CantidadMP) ";
            query1 += " FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
            query1 += " WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + formate.format(new java.util.Date()) + "' ";
            query1 += " AND a.proceso = CodigoSemiElaborado ";
            query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0 ) ";
            query1 += " + ";
            query1 += " isnull((SELECT SUM(me.CantidadMP) ";
            query1 += " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma ";
            query1 += " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
            query1 += " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
            query1 += " AND ma.readTime >= '" + formate.format(new java.util.Date()) + "' ";
            query1 += " AND ma.vigencia != 1 ";
            query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0) as NecesidadPorEquipo, ";
            query1 += " (select count(CodigoMatPrima) from ENSchaffner.dbo.MEstructura where CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = t.PRO_CODPROD) as estructura";
            query1 += " ,ml9.Saldo as Stock9,";
            query1 += " (select isnull(sum(mr.Cantidad_mat_prima),0) from ENSchaffner.dbo.TPMP tp ";
            query1 += " inner join MAC2BETA.dbo.VW_EE_avance_rep ar";
            query1 += " on(tp.Pedido = ar.Pedido and tp.Linea = ar.Linea)";
            query1 += " inner join ENSchaffner.dbo.EE_estructura_rep mr";
            query1 += " on(tp.Pedido = mr.Pedido_rep and tp.Linea = mr.Linea_rep and ar.Reproceso = mr.Reproceso)";
            query1 += " where Status = 'Reproceso' and Activa <> 1 and ar.FechaTermino is null and mr.Codigo_mat_prima COLLATE SQL_Latin1_General_CP850_CS_AS = t.PRO_CODPROD and ar.Proceso = mr.Codigo_semielaborado) as MaterialReproceso ";
            query1 += " ,(select isnull(sum(pm.MVD_Cant-pm.MVD_CantAsignada),0) from MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
            query1 += " inner join MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det";
            query1 += " on(enc.nb_numero = det.nb_numero)";
            query1 += " inner join MAC2BETA.dbo.VW_pedido_de_materiales pm";
            query1 += " on(pm.MVD_NumeroDoc = nb_numero_doc_original and pm.MVD_Linea = nb_numero_linea_original)";
            query1 += " where ch_empresa = 'H' and pm.MVD_CodProd = t.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS";
            query1 += " and (pm.MVD_Cant - pm.MVD_CantAsignada) > 0 and enc.nb_estado < 5) as cantHidropack,                        ";
            query1 += " isnull(msf.nb_cantidad,0) as CantidadSolicitudFabricacion ";
            query1 += " FROM SiaSchaffner.dbo.STO_PRODUCTO t ";
            query1 += " LEFT OUTER JOIN SiaSchaffner.dbo.STO_MOVDET tc ";
            query1 += " ON (t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = tc.MVD_CodProd ";
            query1 += " and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1) ";
            query1 += " LEFT OUTER JOIN MAC.dbo.ProductoAnexo proa ";
            query1 += " on(t.PRO_CODPROD  = proa.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) ";
            query1 += " LEFT OUTER JOIN SiaSchaffner.dbo.VW_stock_materiales b";
            query1 += " on(t.PRO_CODPROD = b.PSL_CodProd)";
            query1 += " left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 ";
            query1 += " on(ml9.PSL_CodProd = t.PRO_CODPROD) ";
            query1 += " left outer join  MAC2BETA.dbo.VW_material_solicitud_fabricacion msf ";
            query1 += " on(msf.vc_codigo = t.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS) ";
            query1 += " WHERE (t.PRO_CODTIPO in (1,5,8) or t.PRO_CODPROD like '5041%') and t.PRO_CODPROD <> '' and proa.Estado <> 1 and PRO_VIGENCIA = 'S' ";
            query1 += " and (isnull(t.PRO_INVMIN,0) > 0 or ";
            query1 += " (isnull((SELECT sum(me.CantidadMP) ";
            query1 += " FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
            query1 += " WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + formate.format(new java.util.Date()) + "' ";
            query1 += " AND a.proceso = CodigoSemiElaborado ";
            query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0 ) ";
            query1 += " + ";
            query1 += " isnull((SELECT SUM(me.CantidadMP) ";
            query1 += " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma ";
            query1 += " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
            query1 += " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
            query1 += " AND ma.readTime >= '" + formate.format(new java.util.Date()) + "' ";
            query1 += " AND ma.vigencia != 1 ";
            query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0)";
            query1 += " +";
            query1 += " (select isnull(sum(pm.MVD_Cant-pm.MVD_CantAsignada),0) from MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
            query1 += " inner join MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det";
            query1 += " on(enc.nb_numero = det.nb_numero)";
            query1 += " inner join MAC2BETA.dbo.VW_pedido_de_materiales pm";
            query1 += " on(pm.MVD_NumeroDoc = nb_numero_doc_original and pm.MVD_Linea = nb_numero_linea_original)";
            query1 += " where ch_empresa = 'H' and pm.MVD_CodProd = t.PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS";
            query1 += " and (pm.MVD_Cant - pm.MVD_CantAsignada) > 0 and enc.nb_estado < 5) +";
            query1 += " isnull(msf.nb_cantidad,0)) > 0)";
            query1 += " and t.PRO_CODPROD = '5037212305'";
            query1 += " GROUP BY t.PRO_CODPROD,ml9.Saldo,isnull(msf.nb_cantidad,0) ,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,proa.LeadTime,proa.Comprador,proa.analisis,proa.anaGen,b.Saldo";
             */
            String query1 = " SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,isnull(t.PRO_INVMIN,0) as StockMax,isnull(t.PRO_INVMAX,0) as StockMin,b.Saldo";
               query1 += " ,proa.LeadTime,sum(isnull(tc.MVD_Cant,0)-isnull(tc.MVD_CantAsignada,0)) as CAN_PEN ,proa.Comprador, ";
               query1 += " proa.analisis,proa.anaGen, ";
               query1 += " isnull((SELECT sum(me.CantidadMP) ";
               query1 += " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
               query1 += " WHERE a.codProducto = me.CodigoTerminado";
               query1 += " AND a.proceso = CodigoSemiElaborado ";
               query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0 ) ";
               query1 += " + ";
               query1 += " isnull((SELECT SUM(me.CantidadMP) ";
               query1 += " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma ";
               query1 += " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
               query1 += " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
               query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0) as NecesidadPorEquipo, ";
               query1 += " (select count(CodigoMatPrima) from ENSchaffner.dbo.MEstructura where CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = t.PRO_CODPROD) as estructura";
               query1 += " ,ml9.Saldo as Stock9,";
               query1 += " (select isnull(sum(mr.Cantidad_mat_prima),0) from ENSchaffner.dbo.TPMP tp ";
               query1 += " inner join MAC2BETA.dbo.VW_EE_avance_rep ar";
               query1 += " on(tp.Pedido = ar.Pedido and tp.Linea = ar.Linea)";
               query1 += " inner join ENSchaffner.dbo.EE_estructura_rep mr";
               query1 += " on(tp.Pedido = mr.Pedido_rep and tp.Linea = mr.Linea_rep and ar.Reproceso = mr.Reproceso)";
               query1 += " where Status = 'Reproceso' and Activa <> 1 and ar.FechaTermino is null and mr.Codigo_mat_prima COLLATE SQL_Latin1_General_CP850_CS_AS = t.PRO_CODPROD and ar.Proceso = mr.Codigo_semielaborado) as MaterialReproceso ";
               query1 += " FROM SiaSchaffner.dbo.STO_PRODUCTO t ";
               query1 += " LEFT OUTER JOIN SiaSchaffner.dbo.STO_MOVDET tc ";
               query1 += " ON (t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = tc.MVD_CodProd ";
               query1 += " and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1) ";
               query1 += " LEFT OUTER JOIN MAC.dbo.ProductoAnexo proa ";
               query1 += " on(t.PRO_CODPROD  = proa.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS) ";
               query1 += " LEFT OUTER JOIN SiaSchaffner.dbo.VW_stock_materiales b";
               query1 += " on(t.PRO_CODPROD = b.PSL_CodProd)";
               query1 += " left outer join SiaSchaffner.dbo.VW_stock_materiales_bodega_9 ml9 ";
               query1 += " on(ml9.PSL_CodProd = t.PRO_CODPROD) ";
               query1 += " WHERE (t.PRO_CODTIPO in (1,5,8) or t.PRO_CODPROD like '5041%') and t.PRO_CODPROD <> '' and proa.Estado <> 1 and PRO_VIGENCIA = 'S' ";
               query1 += " and (isnull(t.PRO_INVMIN,0) > 0 or (isnull((SELECT sum(me.CantidadMP) ";
               query1 += " FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me ";
               query1 += " WHERE a.codProducto = me.CodigoTerminado";
               query1 += " AND a.proceso = CodigoSemiElaborado ";
               query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0 )";
               query1 += " + ";
               query1 += " isnull((SELECT SUM(me.CantidadMP) ";
               query1 += " FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma ";
               query1 += " WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea ";
               query1 += " AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' ";
               query1 += " AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0)";
               query1 += " +";
               query1 += " (select isnull(sum(mr.Cantidad_mat_prima),0) from ENSchaffner.dbo.TPMP tp ";
               query1 += " inner join MAC2BETA.dbo.VW_EE_avance_rep ar";
               query1 += " on(tp.Pedido = ar.Pedido and tp.Linea = ar.Linea)";
               query1 += " inner join ENSchaffner.dbo.EE_estructura_rep mr";
               query1 += " on(tp.Pedido = mr.Pedido_rep and tp.Linea = mr.Linea_rep and ar.Reproceso = mr.Reproceso)";
               query1 += " where Status = 'Reproceso' and Activa <> 1 and ar.FechaTermino is null and mr.Codigo_mat_prima COLLATE SQL_Latin1_General_CP850_CS_AS = t.PRO_CODPROD and ar.Proceso = mr.Codigo_semielaborado)";
               query1 += " )>0)";
                //--and t.PRO_CODPROD = '5037212305'
               query1 += " GROUP BY t.PRO_CODPROD,ml9.Saldo,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,proa.LeadTime,proa.Comprador,proa.analisis,proa.anaGen,b.Saldo";

                
            rs = state.executeQuery(query1);
            
            while(rs.next()){
                EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                materiales.setCodigoMatPrima(rs.getString(1));
                materiales.setDescripcion(rs.getString(2));
                materiales.setUnidadMedida(rs.getString(3));
                materiales.setStockMinimo(rs.getFloat(4));
                materiales.setStockMaximo(rs.getFloat(5));
                materiales.setStockBodega(rs.getFloat(6)+rs.getFloat(14));
                materiales.setLeadTime(rs.getInt(7));
                materiales.setOrdenCompra(rs.getFloat(8));
                materiales.setComprador(rs.getString(9));
                materiales.setAnalisis(rs.getInt(10));
                materiales.setAnagen(rs.getInt(11));
                //materiales.setReal(rs.getFloat(12) + rs.getFloat(15));
                materiales.setReal(rs.getFloat(12));
                materiales.setEstado(rs.getInt(13));
                analisisMateriales.addElement(materiales);
            }
            rs.close();
            
            for(int a=0;a<analisisMateriales.size();a++){
                EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                materiales.setCodigoMatPrima(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getCodigoMatPrima());
                materiales.setDescripcion(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getDescripcion().trim());
                materiales.setCantidad(0);
                materiales.setUnidadMedida(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getUnidadMedida());
                materiales.setLeadTime(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getLeadTime());
                materiales.setStockMinimo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMinimo());
                materiales.setStockMaximo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMaximo());
                materiales.setComprador(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getComprador());
                materiales.setEstado(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getEstado());
                if((((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockBodega() + ((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getOrdenCompra()) < (((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMinimo() + ((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getReal())){
                    materiales.setEliminar(true);
                }else{
                    materiales.setEliminar(false);
                }
                materialesNoMrp.addElement(materiales);
            }
            
            
            
            
            
            
            
            
            
        /* for(int a=0;a<analisisMateriales.size();a++){
             float cantidad = 0;
             if(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getAnalisis() == 1){
                 float can1 = devolverNecesidadesSinConsumo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getCodigoMatPrima(),state);
             if((((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockBodega() + ((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getOrdenCompra()) < ((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMinimo()){
                cantidad = ((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMaximo() - (((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockBodega() + ((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getOrdenCompra());
                EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                materiales.setCodigoMatPrima(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getCodigoMatPrima());
                materiales.setDescripcion(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getDescripcion());
                materiales.setCantidad(cantidad);
                materiales.setUnidadMedida(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getUnidadMedida());
                materiales.setLeadTime(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getLeadTime());
                materiales.setStockMinimo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMinimo());
                materiales.setStockMaximo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMaximo());
                materiales.setComprador(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getComprador());
                materiales.setEstado(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getEstado());
                materialesNoMrp.addElement(materiales);
                veri++;
                //System.out.println("Codigo: " + rs.getString(1) + "----" + " decri: " + rs.getString(2)  + "----" + " nec de compra: " + cantidad);
             }
           }
             if(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getAnagen() == 1){
                 //if(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getCodigoMatPrima().equals("1031401037")){
                 float can1 = devolverNecesidadesSinConsumo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getCodigoMatPrima(),state);
                 cantidad =  (2*((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getReal()+((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMinimo()+((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMaximo())/2-(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockBodega()+((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getOrdenCompra());
                 //}
         
                    if(cantidad > 0){
                     EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                     materiales.setCodigoMatPrima(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getCodigoMatPrima());
                     materiales.setDescripcion(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getDescripcion());
                     materiales.setCantidad(cantidad);
                     materiales.setUnidadMedida(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getUnidadMedida());
                     materiales.setLeadTime(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getLeadTime());
                     materiales.setStockMinimo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMinimo());
                     materiales.setStockMaximo(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getStockMaximo());
                     materiales.setComprador(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getComprador());
                     materiales.setEstado(((EstructuraPorEquipo)analisisMateriales.elementAt(a)).getEstado());
                     materialesNoMrp.addElement(materiales);
                     veri++;
                    }
               }
             //System.out.println("Codigo: " + rs.getString(1) + "----" + " decri: " + rs.getString(2)  + "----" + " nec de compra: " + cantidad);
            }*/
            conMAC.commit();
            state.close();
            conMAC.close();
            new Loger().logger("EstructuraPorEquipo.class " , " Cantidad de necesidadDeCompra: " + String.valueOf(veri),1);
        }catch(Exception e){
            try{
                conMAC.rollback();
                state.close();
                conMAC.close();
            }catch(Exception ex){
                new Loger().logger("EstructuraPorEquipo.class " , " necesidadDeCompra no se insert�() " + e.getMessage(),0);
            }
            
            new Loger().logger("EstructuraPorEquipo.class " , " necesidadDeCompra() " + e.getMessage(),0);
        }
        return materialesNoMrp;
    }
    
 /*public Vector necesidadDeCompra(){
    Vector materialesNoMrp  = new Vector();
    int veri = 0;
    try{
        conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
  
          String query1 = "   SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX, "
                         +"    (   "
                         +"    (select sum(b.PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL b where t.PRO_CODPROD  = b.PSL_CodProd and b.PSL_CodBodega <> 7) "
                         +"    ) as Stock  "
                         +"    ,proa.LeadTime,sum(tc.MVD_Cant-tc.MVD_CantAsignada) as CAN_PEN ,proa.Comprador, "
                         +"   proa.analisis,proa.anaGen, "
                         +"   isnull((SELECT sum(me.CantidadMP) "
                         +"   FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                         +"   WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaA + "' "
                         +"   AND a.proceso = CodigoSemiElaborado "
                         +"   AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0 ) "
                         +"   + "
                         +"   isnull((SELECT SUM(me.CantidadMP) "
                         +"   FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma "
                         +"   WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                         +"   AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                         +"   AND ma.readTime >= '" + fechaA + "' "
                         +"   AND ma.vigencia != 1 "
                         +"   AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in(t.PRO_CODPROD)),0) as NecesidadPorEquipo, "
                         +"   (select count(CodigoMatPrima) from ENSchaffner.dbo.MEstructura where CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = t.PRO_CODPROD) as estructura"
                         +"   FROM SiaSchaffner.dbo.STO_PRODUCTO t "
                         +"   LEFT OUTER JOIN SiaSchaffner.dbo.STO_MOVDET tc "
                         +"   ON (t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = tc.MVD_CodProd "
                         +"   and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc = 1) "
                         +"   LEFT OUTER JOIN MAC.dbo.ProductoAnexo proa "
                         +"   on(t.PRO_CODPROD  = proa.Codigo COLLATE SQL_Latin1_General_CP850_CS_AS)  "
                         +"   WHERE (t.PRO_CODTIPO in (1,5,8) or t.PRO_CODPROD like '5041%') and t.PRO_CODPROD <> '' and proa.Estado <> 1 "
                         +"   GROUP BY t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,proa.LeadTime,proa.Comprador,proa.analisis,proa.anaGen ";
         rs = st.executeQuery(query1);
         while(rs.next()){
             float cantidad = 0;
             if(rs.getByte(10) == 1){
             if((rs.getFloat(6) + rs.getFloat(8)) < rs.getFloat(4)){
                cantidad = rs.getFloat(5) - (rs.getFloat(6) + rs.getFloat(8));
                EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                materiales.setCodigoMatPrima(rs.getString(1));
                materiales.setDescripcion(rs.getString(2));
                materiales.setCantidad(cantidad);
                materiales.setUnidadMedida(rs.getString(3));
                materiales.setLeadTime(rs.getInt(7));
                materiales.setStockMinimo(rs.getFloat(4));
                materiales.setStockMaximo(rs.getFloat(5));
                materiales.setComprador(rs.getString(9));
                materiales.setEstado(rs.getInt(13));
                materialesNoMrp.addElement(materiales);
                veri++;
                //System.out.println("Codigo: " + rs.getString(1) + "----" + " decri: " + rs.getString(2)  + "----" + " nec de compra: " + cantidad);
             }
           }
             if(rs.getByte(11) == 1){
                cantidad =  (2*rs.getFloat(12)+rs.getFloat(4)+rs.getFloat(5))/2-(rs.getFloat(6)+rs.getFloat(8));
  
  
                    if(cantidad > 0){
                     EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                     materiales.setCodigoMatPrima(rs.getString(1));
                     materiales.setDescripcion(rs.getString(2));
                     materiales.setCantidad(cantidad);
                     materiales.setUnidadMedida(rs.getString(3));
                     materiales.setLeadTime(rs.getInt(7));
                     materiales.setStockMinimo(rs.getFloat(4));
                     materiales.setStockMaximo(rs.getFloat(5));
                     materiales.setComprador(rs.getString(9));
                     materiales.setEstado(rs.getInt(13));
                     materialesNoMrp.addElement(materiales);
                     veri++;
                    }
               }
             //System.out.println("Codigo: " + rs.getString(1) + "----" + " decri: " + rs.getString(2)  + "----" + " nec de compra: " + cantidad);
            }
         rs.close();
         st.close();
         conMAC.close();
         new Loger().logger("EstructuraPorEquipo.class " , " Cantidad de necesidadDeCompra: " + String.valueOf(veri),1);
    }catch(Exception e){
      this.cerrarCOneccion();
      new Loger().logger("EstructuraPorEquipo.class " , " necesidadDeCompra() " + e.getMessage(),0);
    }
    return materialesNoMrp;
}*/
    
//--------------------------------------------------------------------------------------------------------------
    public float devolverNecesidadesSinConsumo(String codigo, Statement state) throws Exception{
        float nece = 0;
        float consu = 0;
        Vector equipos = new Vector();
        ResultSet resul = null;
        // try{
        
        String query1 = " SELECT np,linea,me.CantidadMP,CodigoSemiElaborado,Familia "
                +" FROM MAC.dbo.VW_MapaAsBuild a "
                +" inner join ENSchaffner.dbo.MEstructura me "
                +" on(a.codProducto = me.CodigoTerminado) "
                +" inner join ENSchaffner.dbo.TPMP "
                +" on(np = Pedido and linea = Linea ) "
                +" WHERE a.proceso = CodigoSemiElaborado "
                +" AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in('"+codigo+"') "
                +" union all "
                +" SELECT np,linea,me.CantidadMP,CodigoSemiElaborado,Familia "
                +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                +" AND me.CodigoMatPrima = '"+codigo+"' ";
        
        resul = state.executeQuery(query1);
        while(resul.next()){
            // nece += resul.getInt(1);
            Equipo equipo = new Equipo();
            equipo.setNp(resul.getInt(1));
            equipo.setLinea(resul.getInt(2));
            equipo.setCantidad(resul.getFloat(3));
            equipo.setCodPro(resul.getString(4));
            equipo.setFamilia(resul.getString(5));
            equipo.setConsumo(0);
            equipos.addElement(equipo);
        }
        resul.close();
        for(int a=0;a<equipos.size();a++){
            float consumo = consumos(codigo,state,((Equipo)equipos.elementAt(a)).getNp(),((Equipo)equipos.elementAt(a)).getLinea(),((Equipo)equipos.elementAt(a)).getFamilia(),((Equipo)equipos.elementAt(a)).getCodPro());
            ((Equipo)equipos.elementAt(a)).setCantidad(((Equipo)equipos.elementAt(a)).getCantidad()-consumo);
            ((Equipo)equipos.elementAt(a)).setConsumo(consumo);
        }
        
        for(int a=0;a<equipos.size();a++){
            nece  += ((Equipo)equipos.elementAt(a)).getCantidad();
            consu += ((Equipo)equipos.elementAt(a)).getConsumo();
        }
        if(consu > 0){
            insertarConsumo(codigo,consu,state);
        }
        
    /* }catch(Exception e){
      new Loger().logger("EstructuraPorEquipo.class " , " devolverNecesidadesSinConsumo() " + e.getMessage(),0);
    }*/
        return nece;
    }
    
    
    public Vector detalleConsumos(String codigo){
        float nece = 0;
        float consu = 0;
        Vector equipos = new Vector();
        ResultSet resul = null;
        Statement state = null;
        try{
            conMAC = myConexion.getMACConnector();
            state = conMAC.createStatement();
          /*String query1 = " SELECT np,linea,me.CantidadMP,CodigoSemiElaborado,Familia, "
                         +" (select sum(MVD_CantAsignada) "
                         +" from SiaSchaffner.dbo.STO_MOVDET, SiaSchaffner.dbo.STO_MOVENC, MAC.dbo.totalEquiposPorPedido ped "
                         +" where MVD_CodSistema=8 and "
                         +" MVD_CodClase =1 AND "
                         +" MVD_TipoDoc = 8 and "
                         +" MVD_NumeroDoc = MVE_NumeroDoc and "
                         +" MVE_CodSistema= 8 and  "
                         +" MVE_CodClase = 1 and "
                         +" MVE_TipoDoc = 8 and "
                         +" MVE_FolioFisico = numero "
                         +" and ped.np = a.np and ped.linea = a.linea "
                         +" and MVD_CodProd = '"+codigo+"' "
                         +" AND area = a.proceso "
                         +" and status = 5) as valeConsumo "
                         +" FROM MAC.dbo.MapaAsBuild a "
                         +" inner join ENSchaffner.dbo.MEstructura me "
                         +" on(a.codProducto = me.CodigoTerminado) "
                         +" inner join ENSchaffner.dbo.TPMP "
                         +" on(np = Pedido and linea = Linea ) "
                         +" WHERE a.vigencia != 1 AND a.readTime >= '"+formate.format(new java.util.Date())+"' "
                         +" AND a.proceso = CodigoSemiElaborado  "
                         +" AND me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS in('"+codigo+"') "
                         +" union all "
                         +" SELECT np,linea,me.CantidadMP,CodigoSemiElaborado,Familia, "
                         +" (select sum(MVD_CantAsignada) "
                         +" from SiaSchaffner.dbo.STO_MOVDET, SiaSchaffner.dbo.STO_MOVENC, MAC.dbo.totalEquiposPorPedido ped "
                         +" where MVD_CodSistema=8 and "
                         +" MVD_CodClase =1 AND "
                         +" MVD_TipoDoc = 8 and "
                         +" MVD_NumeroDoc = MVE_NumeroDoc and "
                         +" MVE_CodSistema= 8 and "
                         +" MVE_CodClase = 1 and "
                         +" MVE_TipoDoc = 8 and "
                         +" MVE_FolioFisico = numero "
                         +" and ped.np = ma.np and ped.linea = ma.linea "
                         +" and MVD_CodProd = '"+codigo+"' "
                         +" and status = 5) as valeConsumo "
                         +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.MapaAsBuild ma "
                         +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                         +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                         +" AND ma.readTime >= '"+formate.format(new java.util.Date())+"' "
                         +" AND ma.vigencia != 1 "
                         +" AND me.CodigoMatPrima = '"+codigo+"'  ";*/
          /*String query1 = " select nb_np,nb_linea,MVD_CodProd,vc_proceso,MVD_CantAsignada-nb_asignado_a_piso";
                query1 += " from MAC2BETA.dbo.VW_equipos_pedidos";
                query1 += " left outer join  ENSchaffner.dbo.TPMP tp";
                query1 += " on(nb_np = tp.Pedido and nb_linea = tp.Linea)";
                query1 += " left outer join  MAC.dbo.TAvance ta";
                query1 += " on(nb_np = ta.Pedido and nb_linea = ta.Linea and ta.FechaTermino is not null and Semielaborado COLLATE SQL_Latin1_General_CP850_CS_AS = case when tp.Familia ='CELDA' then 'MON' when tp.Familia ='REP' then 'LTX' else vc_proceso end)";
                query1 += " where  ta.FechaTermino is null and MVD_Cant > 0 and MVD_CantAsignada > 0 and MVD_CodProd = '"+codigo+"'";
           */
            String query1  = " select nb_np,nb_linea,MVD_CodProd,vc_proceso,MVD_CantAsignada ";
            query1  += " from MAC2BETA.dbo.VW_detalle_pedidos_planta  where MVD_CodProd = '"+codigo+"'";
            resul = state.executeQuery(query1);
            while(resul.next()){
                // nece += resul.getInt(1);
                Equipo equipo = new Equipo();
                equipo.setNp(resul.getInt(1));
                equipo.setLinea(resul.getInt(2));
                equipo.setCodPro(resul.getString(3));
                equipo.setProceso(resul.getString(4));
                equipo.setConsumo(resul.getFloat(5));
                equipos.addElement(equipo);
            }
            resul.close();
            state.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class " , " detalleConsumos() " + e.getMessage(),0);
        }
        return equipos;
    }
    
    public int insertarConsumo(String codigo ,float canti, Statement state) throws Exception{
        int verifi = 0;
        String query4 = "";
        ResultSet result = null;
        int count = 0;
        //try{
        String query =  "select count(codMaterial) from MAC.dbo.stockPlanta where codMaterial = '"+codigo+"'";
        result = state.executeQuery(query);
        while(result.next()){
            verifi = result.getInt(1);
        }
        result.close();
        if(verifi > 0){
            query4 =  "UPDATE MAC.dbo.stockPlanta set cantidad = "+canti+" where codMaterial = '"+codigo+"'";
        }else{
            query4 =  "INSERT INTO MAC.dbo.stockPlanta(codMaterial,cantidad) values ('"+ codigo +"',"+ canti +")";
        }
        count = state.executeUpdate(query4);
        
        
        /*}catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: insertarAFlex(): "+e.getMessage(),0);
        }*/
        return count;
    }
    
    public float consumos(String codigo, Statement state, int np, int linea, String familia, String proceso) throws Exception{
        float nece = 0;
        ResultSet resul = null;
        // try{
        
        String query1 = " select sum(MVD_CantAsignada) "
                +" from SiaSchaffner.dbo.STO_MOVDET, SiaSchaffner.dbo.STO_MOVENC, MAC.dbo.totalEquiposPorPedido "
                +" where MVD_CodSistema=8 and "
                +" MVD_CodClase =1 AND "
                +" MVD_TipoDoc = 8 and "
                +" MVD_NumeroDoc = MVE_NumeroDoc and "
                +" MVE_CodSistema= 8 and "
                +" MVE_CodClase = 1 and "
                +" MVE_TipoDoc = 8 and "
                +" MVE_FolioFisico = numero "
                +" and np = "+np+" and linea = "+linea
                +" and MVD_CodProd = '"+codigo+"' ";
        if(!familia.equals("CELDA")){
            query1  +=" AND area = '"+proceso+"'" ;
        }
        query1  +=" and status = 5 ";
        
        resul = state.executeQuery(query1);
        while(resul.next()){
            nece = resul.getFloat(1);
        }
        resul.close();
        
        
     /*}catch(Exception e){
      new Loger().logger("EstructuraPorEquipo.class " , " consumos() " + e.getMessage(),0);
    }*/
        return nece;
    }
    
    
//--------------------------------------------------------------------------------------------------------------------------------------------------------------
    public Vector materialesMrp(){
        Vector materialesMrp  = new Vector();
        int veri = 0;
        String fechab = formate.format(new java.util.Date());
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            
         /*String query1 = " SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX, "
                        +"    ( "
                        +"    (select sum(b.PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL b where t.PRO_CODPROD  = b.PSL_CodProd and b.PSL_CodBodega <> 7) "
                        +"    ) as Stock, "
                        +"    proa.LeadTime, "
                        +"    (select sum(tc.MVD_Cant-tc.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tc where  tc.MVD_CodProd  = t.PRO_CODPROD  and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc =1) as CAN_PEN "
                        +"    ,proa.Comprador "
                        +"    FROM SiaSchaffner.dbo.STO_PRODUCTO t "
                        +"    LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a  "
                        +"    ON ( t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = a.CodigoMatPrima ) "
                        +"    LEFT OUTER JOIN MAC.dbo.ProductoAnexo proa "
                        +"    on(t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = proa.Codigo) "
                        +"    WHERE a.CodigoMatPrima IS NOT NULL AND t.PRO_INVMIN > 0 and proa.Estado = 0  "
                        +"    and t.PRO_CODTIPO in (1,5,8) "
                        +"    GROUP BY t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,proa.LeadTime,proa.Comprador ";*/
            
            String query1 = " SELECT t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX, "
                    +" ( "
                    +" (select sum(b.PSL_Saldo) from SiaSchaffner.dbo.STO_PRODSAL b where t.PRO_CODPROD  = b.PSL_CodProd and b.PSL_CodBodega <> 7) "
                    +" ) as Stock, "
                    +" proa.LeadTime, "
                    +" (select sum(tc.MVD_Cant-tc.MVD_CantAsignada) from SiaSchaffner.dbo.STO_MOVDET tc where  tc.MVD_CodProd  = t.PRO_CODPROD  and tc.MVD_CodSistema=7 and tc.MVD_CodClase =1 AND tc.MVD_TipoDoc =1) as CAN_PEN "
                    +" ,proa.Comprador, "
                    +" ( "
                    +" SELECT SUM(me.CantidadMP) "
                    +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    +" WHERE a.codProducto = me.CodigoTerminado "
                    +" AND a.proceso = CodigoSemiElaborado  "
                    +" AND me.CodigoMatPrima in(t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI)) as NecesidadPorEquipo_no_Celda, "
                    +" ( "
                    +" SELECT SUM(me.CantidadMP) "
                    +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    +" AND me.CodigoMatPrima in(t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI)) as NecesidadPorEquipo_si_Celda "
                    +" FROM SiaSchaffner.dbo.STO_PRODUCTO t "
                    +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura a  "
                    +" ON ( t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = a.CodigoMatPrima ) "
                    +" LEFT OUTER JOIN MAC.dbo.ProductoAnexo proa "
                    +" on(t.PRO_CODPROD COLLATE Modern_Spanish_CI_AI = proa.Codigo) "
                    +" WHERE a.CodigoMatPrima IS NOT NULL and proa.Estado = 0  "
                    +" and t.PRO_CODTIPO in (1,5,8) "
                    +" GROUP BY t.PRO_CODPROD,t.PRO_DESC,t.PRO_UMPRINCIPAL,t.PRO_INVMIN,t.PRO_INVMAX,proa.LeadTime,proa.Comprador "
                    +" order by t.PRO_CODPROD ";
            rs = st.executeQuery(query1);
            while(rs.next()){
                float cantidad = 0;
                if((rs.getFloat(6) + rs.getFloat(8)) < (rs.getFloat(4)+rs.getFloat(10)+rs.getFloat(11))){
                    cantidad = (rs.getFloat(5) + rs.getFloat(10) + rs.getFloat(11)) - (rs.getFloat(6) + rs.getFloat(8));
                    //System.out.println("stock+oc"+rs.getFloat(6) + rs.getFloat(8) + "---" + "stockMinimo+necesidades " + rs.getFloat(4)+rs.getFloat(10)+rs.getFloat(11));
                    //System.out.println("stockMaximo+necesidades "+rs.getFloat(5) + rs.getFloat(10) + rs.getFloat(11) + "---" + "stock+oc"+rs.getFloat(6) + rs.getFloat(8));
                    EstructuraPorEquipo materiales = new EstructuraPorEquipo();
                    materiales.setCodigoMatPrima(rs.getString(1));
                    materiales.setDescripcion(rs.getString(2));
                    materiales.setCantidad(cantidad);
                    materiales.setUnidadMedida(rs.getString(3));
                    materiales.setLeadTime(rs.getInt(7));
                    materiales.setStockMinimo(rs.getFloat(4));
                    materiales.setStockMaximo(rs.getFloat(5));
                    materiales.setComprador(rs.getString(9));
                    materialesMrp.addElement(materiales);
                    veri++;
                }
            }
            rs.close();
            st.close();
            conMAC.close();
            new Loger().logger("EstructuraPorEquipo.class " , "Cantidad de materialesMrp: " + String.valueOf(veri),1);
        }catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class " , "ERROR materialesMrp(): " + e.getMessage(),0);
        }
        return materialesMrp;
    }
    
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    public int comprasRequeridasMaterialesNoMrp(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int verificar,byte estado,byte identificador){
        int count = 0;
        int veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 = "select count(*) FROM MAC.dbo.ComprasRequeridas  WHERE Codigo = '"+codigo+"' AND verificar <> 3";
            rs = st.executeQuery(query4);
            while(rs.next()){
                count = rs.getInt(1);
            }
            if(count == 0){
                String query1 = "insert into MAC.dbo.ComprasRequeridas(Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',"+ verificar +","+ estado +")";
                st.execute(query1);
                veri = 1;
            }
        }catch(Exception e){
            
        }
        return veri;
    }
    
     public int actualizarComprasRequeridas(String codigo){
        int count = 0;
        int veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query1 = "UPDATE MAC.dbo.ComprasRequeridas SET verificar = 3 WHERE Codigo = '"+codigo+"' AND verificar <> 3 ";
            st.execute(query1);
            veri = 1;
           
        }catch(Exception e){
            
        }
        return veri;
    }
    
    
    public int comprasRequeridasMaterialesNoMrp1(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int verificar,byte estado,byte identificador){
        int count = 0;
        float cantReq = 0;
        float numSap = 0;
        float numSap4 = 0;
        int veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            if(identificador == 1){
                String query4 = " SELECT COUNT(Codigo),(select sum(cantRequerida) FROM MAC.dbo.ComprasRequeridas "
                        +" WHERE Codigo = '" + codigo + "' AND verificar in (1,4) AND numeroSap IS NOT NULL), "
                        +" (SELECT sum(cantRequerida) FROM MAC.dbo.ComprasRequeridas  "
                        +" WHERE Codigo = '" + codigo + "' AND verificar = 1 AND numeroSap IS NULL), "
                        +" (SELECT sum(cantRequerida) FROM MAC.dbo.ComprasRequeridas  "
                        +" WHERE Codigo = '" + codigo + "' AND verificar = 4 AND numeroSap IS NULL) "
                        +" FROM MAC.dbo.ComprasRequeridas "
                        +" WHERE Codigo = '" + codigo + "' AND verificar in(1,4)";
                rs = st.executeQuery(query4);
                while(rs.next()){
                    count = rs.getInt(1);
                    cantReq = rs.getFloat(2);
                    numSap = rs.getFloat(3);
                    numSap4 = rs.getFloat(4);
                }
                if(count == 0){
                    String query1 = "insert into MAC.dbo.ComprasRequeridas  (Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',"+ verificar +","+ estado +")";
                    st.execute(query1);
                    veri = 1;
                }else{
                    if((cantReq + numSap + numSap4) < cantRequerida){
                        if(numSap4 > 0){
                            cantRequerida -=  (cantReq + numSap);
                            String query = "UPDATE MAC.dbo.ComprasRequeridas SET cantRequerida = '" + cantRequerida + "' ,fechaEntrega = '" + fechaEntrega + "' WHERE Codigo = '" + codigo + "' AND verificar = 4 AND numeroSap IS NULL" ;
                            st.execute(query);
                            veri = 1;
                        }
                        if(numSap4 == 0){
                            cantRequerida -= (cantReq + numSap);
                            String query = "insert into MAC.dbo.ComprasRequeridas(Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',"+ verificar +","+ estado +")";
                            st.execute(query);
                            veri = 1;
                        }
                    }
                }
                rs.close();
            }else{
                String query4 = " SELECT COUNT(Codigo),(select sum(cantRequerida) FROM MAC.dbo.ComprasRequeridas "
                        +" WHERE Codigo = '" + codigo + "' AND verificar <> 3 AND numeroSap IS NOT NULL), "
                        +" (select sum(cantRequerida) FROM MAC.dbo.ComprasRequeridas  "
                        +" WHERE Codigo = '" + codigo + "' AND verificar <> 3 AND numeroSap IS NULL) "
                        +" FROM MAC.dbo.ComprasRequeridas "
                        +" WHERE Codigo = '" + codigo + "' AND verificar <> 3";
                rs = st.executeQuery(query4);
                while(rs.next()){
                    count = rs.getInt(1);
                    cantReq = rs.getFloat(2);
                    numSap = rs.getFloat(3);
                }
                if(count == 0){
                    String query1 = "insert into MAC.dbo.ComprasRequeridas  (Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',"+ verificar +","+ estado +")";
                    st.execute(query1);
                    veri = 1;
                }else{
                    if((cantReq + numSap) < cantRequerida){
                        if(numSap > 0){
                            cantRequerida -=   cantReq;
                            String query = "UPDATE MAC.dbo.ComprasRequeridas SET cantRequerida = '" + cantRequerida + "' ,fechaEntrega = '" + fechaEntrega + "' WHERE Codigo = '" + codigo + "' AND verificar <> 3 AND numeroSap IS NULL" ;
                            st.execute(query);
                            veri = 1;
                        }
                        if(numSap == 0){
                            cantRequerida -= cantReq;
                            String query = "insert into MAC.dbo.ComprasRequeridas(Codigo,Descripcion,cantRequerida,UniMed,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado) values ('"+ codigo +"','"+ descripcion +"',"+ cantRequerida +",'"+ uni +"','"+ comprador +"',"+ leadTime +",'"+ contador +"',"+ stockMin +","+ stockMax +",'"+ fechaEntrega +"',"+ verificar +","+ estado +")";
                            st.execute(query);
                            veri = 1;
                        }
                    }
                }
                rs.close();
            }
            st.close();
            conMAC.close();
        }catch(Exception e){
            this.cerrarCOneccion();
            new Loger().logger("EstructuraPorEquipo.class " , "ERROR comprasRequeridasMaterialesNoMrp(): " + e.getMessage() + " " + codigo,0);
        }
        return veri;
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public Vector eee(){
        Vector dd = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " select mf.Codigo,tc.descripcion,tc.Unimed from MAC.dbo.materialesPorFamilia mf ,MAC.dbo.TConsumoPromedio tc "
                    +" where mf.Codigo = tc.Codigo ";
            
            
            
            
            //String query = " SELECT Codigo FROM MAC.dbo.TConsumoPromedio"; //David
            // String query = " SELECT Codigo FROM MAC.dbo.TConsumoPromedio where raiz is not null"; Carlos
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setDescripcion(rs.getString(2));
                estructura.setUnidadMedida(rs.getString(3));
                dd.addElement(estructura);
            }
            rs.close();
            // st.close();
            // conMAC.close();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo eee() " +  e.getMessage(),0);
        }
        return dd;
    }
    
    public Vector rrr(String codPro,String descri,String um){
        try{
            //conMAC = myConexion.getMACConnector();
            //st = conMAC.createStatement();
            String query = " UPDATE MAC.dbo.materialesPorFamilia SET Descripcion = '" + descri + "',Unimed = '" + um + "' where Codigo = '" + codPro + "'";
            //System.out.println("Raiz : " +raiz+  "  Codigo : " +codPro);
            st.execute(query);
            //st.close();
            //conMAC.close();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo rrr() " +  e.getMessage(),0);
        }
        return estructuraEquipo;
    }
    
    public void cerrarCOneccion(){
        try{
            st.close();
            myConexion.cerrarConMAC();
        }catch(Exception e){}
    }
    
//---------------------------------------------------------------------------------------------------------------------
    // M�todo para actualizar los stock minimo y maximo de los materiales
    public void actualizarTConsumoPromedio(String codigo ,float stockMin , float stockMax){
        //public void actualizarTConsumoPromedio(String codigo ,float leadTime){
        try{
            //conMAC = myConexion.getMACConnector();
            //st = conMAC.createStatement();
            //String query = " UPDATE MAC.dbo.TConsumoPromedio SET mediaCantidad = " + leadTime + " where Codigo = '" + codigo + "'";
            String query = " UPDATE SiaSchaffner.dbo.STO_PRODUCTO SET PRO_INVMIN = " + stockMin + " , PRO_INVMAX = " + stockMax + " WHERE PRO_CODPROD = '" + codigo + "'";
            st.execute(query);
            //st.close();
            //conMAC.close();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo actualizarTConsumoPromedio() " +  e.getMessage(),0);
        }
    }
    public Vector  valoresMinMax(){
        Vector minMax = new Vector();
        String  query = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            query = " select Codigo,StockMinimo,StockMaximo from MAC.dbo.TConsumoPromedio ";
            //String query = " SELECT codigo,SUM(cantidad)/12 FROM MAC.dbo.consumosPorMaterial  GROUP BY codigo";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setStockMinimo(rs.getFloat(2));
                estructura.setStockMaximo(rs.getFloat(3));
                minMax.addElement(estructura);
            }
            rs.close();
            //st.close();
            //conMAC.close();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo valoresMinMax() " +  e.getMessage(),0);
        }
        return minMax;
    }
    
//--------------------------------------------------------------------------------------------------------------------------------
    public String  buscarComprador(String codigo){
        String comprador = "";
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query = " SELECT Comprador FROM MAC.dbo.ProductoAnexo WHERE Codigo = '"  + codigo + "'";
            rs = st.executeQuery(query);
            while(rs.next()){
                comprador = rs.getString(1);
            }
            rs.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            myConexion.cerrarConMAC();
            new Loger().logger("EstructuraPorEquipo.class: ","m�todo buscarComprador() " +  e.getMessage(),0);
        }
        return comprador;
    }
//-------------------------------------------------------------------------------------------------------------------------------
 /*public List stockMinMax(){
      List usar = new ArrayList();
       try{
             conMAC = myConexion.getMACConnector();
             st = conMAC.createStatement();
             String query = " SELECT t.Codigo,t.descripcion,t.Unimed,t.StockMinimo,t.StockMaximo,l.Cantidad "
                           +" FROM MAC.dbo.TConsumoPromedio t "
                           +" LEFT OUTER JOIN MAC.dbo.LeadTime l "
                           +" ON (t.Codigo = l.Codigo) ";
                           //+" ORDER BY t.descripcion";
  
             rs = st.executeQuery(query);
             while(rs.next()){
             EstructuraPorEquipo estructura = new  EstructuraPorEquipo(rs.getString(1),rs.getString(2),rs.getString(3),rs.getFloat(4),rs.getFloat(5),rs.getInt(6));
             usar.add(estructura);
             }
             rs.close();
             st.close();
             conMAC.close();
         } catch(Exception e){
           new Loger().logger("EstructuraPorEquipo.class: ","stockMinMax() " +  e.getMessage(),0);
         }
  
      return usar;
  }
  
    public int compareTo(Object o) {
        return 1;
    }*/
    
//----------------------------------------------------------------------------------------------------------------------------
    // m�todos para actualizar flex con los pedidos de materiales
    private void consumo03() throws IOException{
        DataOutputStream inicial=null;
        try {
            inicial = new DataOutputStream(new FileOutputStream("c:/inflex/conn.csv"));
            inicial.writeBytes("OPCION,");
            inicial.writeChar('\n');
            inicial.writeBytes("kk,");
            inicial.close();
            
        } catch (FileNotFoundException ex) {
            new Loger().logger("EstructuraPorEquipo.class: ","Consumo03() " +  ex.getMessage(),0);
        }
    }
    private void consumo2(){
        String sql=null;
        
        try {
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            sql="Select * from grabarAFlex";
            rs =st.executeQuery(sql);
            DataOutputStream salida=null;
            try {
                salida = new DataOutputStream(new FileOutputStream("c:/inflex/cons.csv"));
                while (rs.next()){
                    String Var=rs.getString("numPedido");
                    Var=Var + ",";
                    Var=Var + rs.getString("linea");
                    Var=Var + ",";
                    Var=Var + rs.getString("codigo");
                    Var=Var + ",";
                    Var=Var + rs.getFloat("cantEntregada");
                    Var=Var + ",";
                    Var=Var + rs.getString("proceso");
                    salida.writeBytes(Var);
                    salida.writeChar('\n');
                }
                salida.close();
                rs.close();
                st.close();
                conMAC.close();
            } catch (FileNotFoundException ex) {
                new Loger().logger("EstructuraPorEquipo.class: ","Consumo02() " +  ex.getMessage(),0);
            }
            
        } catch (Exception e) {
            new Loger().logger("EstructuraPorEquipo.class: ","Consumo02() " +  e.getMessage(),0);
        }
    }
    private void ejecutaFLX(){
        
        Runtime obj = Runtime.getRuntime();
        
        try {
            obj.exec("C:/dfw305e/bin/dfrun.exe //sch06/flexline/inflex/mac_02.flx" );
            // obj.exec("C:/dfw305e/bin/dfrun.exe d:/probar/borrajav.flx" );
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
        
    }
    private String consumo01() throws IOException{
        String  valor = "kk";
        CsvReader reader = null;
        try {
            reader = new CsvReader("c:/inflex/conn.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                valor = reader.get("OPCION");
            }
            reader.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return valor;
    }
    
    public char grabarAFlex() throws IOException {
        // TODO code application logic here
        this.consumo03();
        this.consumo2();
        this.ejecutaFLX();
        String  Resp="kk";
        char modo;
        while (Resp.equals("kk")){
            Resp=  this.consumo01();
        }
        modo= Resp.substring(0).charAt(0);
        return modo;
    }
// fin de los m�todos para actualizar flex de los pedidos de materiales
    public Vector cambioMoneda(){
        Vector usar = new Vector();
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query =" select PRO_CODPROD,PRO_MONEDACOMPRA from SiaSchaffner.dbo.STO_PRODUCTO "
                    +" inner join MAC.dbo.ProductoAnexo "
                    +" on(PRO_CODPROD COLLATE SQL_Latin1_General_CP850_CS_AS = Codigo) "
                    +" where PRO_MONEDACOMPRA <> MonedaCompra ";
            rs = st.executeQuery(query);
            while(rs.next()){
                EstructuraPorEquipo estructura = new  EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setCompara(rs.getByte(2));
                usar.addElement(estructura);
            }
            rs.close();
            //st.close();
            //conMAC.close();
        } catch(Exception e){
            this.cerrarCOneccion();
            new Loger().logger("EstructuraPorEquipo.class: "," cambioMoneda() " +  e.getMessage(),0);
        }
        
        return usar;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public float necesidadesDeMateriales(String codigo){
        float cant = 0f;
        try{
            
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query =" SELECT sum(me.CantidadMP) "
                    +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                    +" WHERE a.codProducto = me.CodigoTerminado "
                    +" AND a.proceso = CodigoSemiElaborado "
                    +" AND me.CodigoMatPrima in('" + codigo + "') ";
            rs = st.executeQuery(query);
            while(rs.next()){
                cant = rs.getFloat(1);
            }
            rs.close();
            
            String query1 =" SELECT SUM(me.CantidadMP) "
                    +" FROM ENSchaffner.dbo.MEstructura me,ENSchaffner.dbo.TPMP tp,MAC.dbo.VW_MapaAsBuild ma "
                    +" WHERE me.CodigoTerminado = tp.Codpro AND ma.np = tp.Pedido and ma.linea = tp.Linea "
                    +" AND ma.proceso = 'MON' and me.CodigoTerminado like 'B%' "
                    +" AND me.CodigoMatPrima in('" + codigo + "') ";
            rs = st.executeQuery(query1);
            while(rs.next()){
                cant += rs.getFloat(1);
            }
            rs.close();
            
            st.close();
            conMAC.close();
            myConexion.cerrarConMAC();
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: "," necesidadesDeMateriales() " +  e.getMessage(),0);
        }
        
        return cant;
    }
    
    public void actualizarMoneda(String codPro,byte moneda){
        try{
            String query = " update MAC.dbo.ProductoAnexo set MonedaCompra = " + moneda + " where Codigo = '" + codPro + "'";
            st.execute(query);
        } catch(Exception e){
            new Loger().logger("EstructuraPorEquipo.class: "," actualizarMoneda " +  e.getMessage(),0);
        }
        
    }
    
    public float getPresupuesto() {
        return presupuesto;
    }
    
    public void setPresupuesto(float presupuesto) {
        this.presupuesto = presupuesto;
    }
    
    public float getPresupuesto1() {
        return presupuesto1;
    }
    
    public void setPresupuesto1(float presupuesto1) {
        this.presupuesto1 = presupuesto1;
    }
    
    public float getReal() {
        return real;
    }
    
    public void setReal(float real) {
        this.real = real;
    }
    
    public float getReal1() {
        return real1;
    }
    
    public void setReal1(float real1) {
        this.real1 = real1;
    }
    
    public float getDiferencia() {
        return diferencia;
    }
    
    public void setDiferencia(float diferencia) {
        this.diferencia = diferencia;
    }
    
    public float getDiferencia1() {
        return diferencia1;
    }
    
    public void setDiferencia1(float diferencia1) {
        this.diferencia1 = diferencia1;
    }
    
    public Date getFechaCreacion() {
        return fechaCreacion;
    }
    
    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    public String getFechaIn() {
        return fechaIn;
    }
    
    public void setFechaIn(String fechaIn) {
        this.fechaIn = fechaIn;
    }
    
    public String getConjunto() {
        return conjunto;
    }
    
    public void setConjunto(String conjunto) {
        this.conjunto = conjunto;
    }
    
    public float getOrdenCompra() {
        return ordenCompra;
    }
    
    public void setOrdenCompra(float ordenCompra) {
        this.ordenCompra = ordenCompra;
    }
    
    public int getAnalisis() {
        return analisis;
    }
    
    public void setAnalisis(int analisis) {
        this.analisis = analisis;
    }
    
    public int getAnagen() {
        return anagen;
    }
    
    public void setAnagen(int anagen) {
        this.anagen = anagen;
    }
    
    public float getConsumo() {
        return consumo;
    }
    
    public void setConsumo(float consumo) {
        this.consumo = consumo;
    }
    
    public float getStockBodega1() {
        return stockBodega1;
    }
    
    public void setStockBodega1(float stockBodega1) {
        this.stockBodega1 = stockBodega1;
    }
    
    public boolean isEliminar() {
        return eliminar;
    }
    
    public void setEliminar(boolean eliminar) {
        this.eliminar = eliminar;
    }
}
