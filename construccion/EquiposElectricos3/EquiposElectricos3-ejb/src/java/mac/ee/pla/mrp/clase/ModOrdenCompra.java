/*
 * ModOrdenCompra.java
 *
 * Created on 20 de agosto de 2014, 16:43
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.mrp.clase;

public class ModOrdenCompra {

    private long numero;
    private int linea;
    private double cantidad;
    private String fechaIngreso;
    private String fechaEntrega;
    private String fechaConfirmada;
    private String nombreProveedor;

    public ModOrdenCompra() {
        fechaIngreso = "";
        fechaEntrega = "";
        fechaConfirmada = "";
        nombreProveedor = "";
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public double getCantidad() {
        return cantidad;
    }

    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(String fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getFechaConfirmada() {
        return fechaConfirmada;
    }

    public void setFechaConfirmada(String fechaConfirmada) {
        this.fechaConfirmada = fechaConfirmada;
    }

    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }
}
