/*
 * Usuario.java
 *
 * Created on 4 de septiembre de 2008, 15:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.mrp.clase;

/**
 *
 * @author dbarra
 */
public class Usuario {
    private String nombre;
    private int codigo;
    
    /** Creates a new instance of Usuario */
    public Usuario() {
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }
    
}
