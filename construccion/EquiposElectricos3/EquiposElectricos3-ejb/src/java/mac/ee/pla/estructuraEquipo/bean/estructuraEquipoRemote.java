
package mac.ee.pla.estructuraEquipo.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for estructuraEquipo enterprise bean.
 */
public interface estructuraEquipoRemote extends EJBObject, estructuraEquipoRemoteBusiness {
    
    
}
