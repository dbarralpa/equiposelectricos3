
package mac.ee.pla.estructuraEquipo.bean;

import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.Date;
import java.util.List;
import java.util.Vector;
import java.util.ArrayList;


/**
 * This is the business interface for estructuraEquipo enterprise bean.
 */
public interface estructuraEquipoRemoteBusiness {
   
    float retornarStockMin(String material) throws RemoteException;
    float retornarStockMax(String material) throws RemoteException;
    byte actualizarLeadTime(String material, short leadTime) throws RemoteException;
    short leadTime(String material) throws RemoteException;
    short diferenciaDiasLeadTime(String inicio, short leadTime) throws RemoteException;
    void datosParaDesviacionStandard(String material) throws RemoteException;
    Vector datosParaDesviacionStandard() throws RemoteException;
    Vector retornarDesviacionStandard() throws RemoteException;
    void setIncrementar(int incrementar,int verificar) throws RemoteException;
    int getIncrementar() throws RemoteException;
    void setCodigoMaterial(String material) throws RemoteException;
    String getCodigoMaterial() throws RemoteException;
    void setIncrementar(int incrementar) throws RemoteException;
    void setFechaEntrega(String fechaEntrega) throws RemoteException;
    String getFechaEntrega() throws RemoteException;
    void materialesStockMinMax() throws RemoteException;
    Vector retornarStockMinMax() throws RemoteException;
    byte actualizarStockMinMax(String material, String uMedida, float cantidad, byte compara, String descripcion,String area) throws RemoteException;
    void materialesEE() throws RemoteException;
    Vector retornarMaterialesEE() throws RemoteException;
    int areaNegocio(String material, String area, String areaAntigua, String descripcion, String um,int iden) throws java.rmi.RemoteException;
    void setAreaDeNegocio(String areaDeNegocio) throws RemoteException;
    String getAreaDeNegocio() throws RemoteException;
    void agregarAvectorEE(String codigo, String descri, String um, float stockMinimo, float stockMaximo, int leadTime, String importancia, String area) throws RemoteException;
    void setearAreaNegocioEE(String area, String area1, int vector, String cambiar, String agregar, int position) throws RemoteException;
    void setearAreaNegocioHP(String area, String area1, int vector, String cambiar, String agregar, int position) throws RemoteException;
    void setearAreaNegocioLG(String area, String area1, int vector, String cambiar, String agregar, int position) throws RemoteException;
    void setearAreaNegocioMM(String area, String area1, int vector, String cambiar, String agregar, int position) throws RemoteException;
    Vector materialesNoMrp() throws RemoteException;
    int comprasRequeridasMaterialesNoMrp(String codigo, String descripcion, float cantRequerida, String uni, String comprador, short leadTime, String contador, float stockMin, float stockMax, String fechaEntrega, int verificar,byte estado,byte identificador) throws RemoteException;
    void minMaxVacio(Vector vacio) throws RemoteException;
    void materialesEEvacio(Vector vacio) throws RemoteException;
    String  buscarComprador(String codigo)throws RemoteException;
    //List stockMinMax() throws RemoteException;
    byte actualizarStockMinMaxLeadTime(String material,float stocMinimo,float stocMaximo,int leadTime) throws RemoteException;
    byte actualizarStockMinMax1(String material, float stocMinimo, float stocMaximo) throws RemoteException;
    char grabarAFlex() throws IOException, RemoteException;
    Vector materialesMrp() throws RemoteException;
    void cerrarCOneccion() throws RemoteException;
    Vector cambioMoneda() throws RemoteException;
    void actualizarMoneda(String codPro, byte moneda) throws RemoteException;
   
}
