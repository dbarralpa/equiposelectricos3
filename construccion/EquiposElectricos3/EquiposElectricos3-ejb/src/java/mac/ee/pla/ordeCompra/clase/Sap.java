/*
 * Sap.java
 *
 * Created on 26 de marzo de 2009, 15:29
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.ordeCompra.clase;

import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author mrojas
 */
public class Sap {
    private String descripcion;
    private String codigo;
    private String um;
    private float cantidad;
    private String usuario;
    private String area;
    private Date fechaEntrada;
    private Date fechaEntrega;
    private Date fechaValSgc;
    private Date fechaVal;
    private int np;
    private int linea;
    private long numSap;
    private String observacion;
    private byte validar;
    private int id;
    private byte anular;
    private int diferenciaDias;
    private String comprador;
    private String observacionGerenteArea;
    private String observacionGerenteLogi;
    private int carroDeCompra;
    private String nameCarroDeCompra;
    private String centroCosto;
    private float cantAsignada;
    private float precio;
    private String persona;
    private String nomPersona;
    private long numOrden;
    private int estado;
    private int estadoParte;
    private int estadoFactura;
    private int estadoFacturaFA;
    private int estadoParteFA;
    private int recepcionDeCompra;
    private float stock;
    private float stockMin;
    private float stockMax;
    private float neceEquipo;
    private float ordenFutura;
    private float ordenVencida;
    private long numDocOrigen;
    private float saldo;
    private String aprobacion;
    private Timestamp fechaIni;
    private String fechaIngreso;
    private Date fechaAnulacion;
    private String estadoOc;
    private String estadoOc1;
    private String estadoOc2;
    private String estadoOc3;
    private String usuValLogi;
    private String usuValArea;
    private String usuAnular;
    private String accion;
    private int tipo;
    private String itemGasto;
    private String numeroDoc;
    /** Creates a new instance of Sap */
    public Sap() {
        descripcion = "";
        codigo = "";
        um = "";
        stock = 0f;
        stockMin = 0f;
        stockMax = 0f;
        neceEquipo = 0f;
        ordenFutura = 0f;
        ordenVencida = 0f;
        cantidad = 0f;
        numeroDoc = "0";
    }
    
    public void setTipo(int tipo){
        this.tipo = tipo;
    }
    public int getTipo(){
        return tipo;
    }
    
    public String getAccion(){
        return accion;
    }
    
    public void setAccion(String accion){
        this.accion = accion;
    }
    
    public String getUsuValLogi(){
        return usuValLogi;
    }
    
    public void setUsuValLogi(String usuValLogi){
        this.usuValLogi = usuValLogi;
    }
    
    public String getUsuValArea(){
        return usuValArea;
    }
    
    public void setUsuValArea(String usuValArea){
        this.usuValArea = usuValArea;
    }
    
     public String getUsuAnular(){
        return usuAnular;
    }
    
    public void setUsuAnular(String usuAnular){
        this.usuAnular = usuAnular;
    }
    
    public String getEstadoOc(){
        return estadoOc;
    }
    public void setEstadoOc(String estadoOc){
        this.estadoOc = estadoOc;
    }
    public String getEstadoOc1(){
        return estadoOc1;
    }
    public void setEstadoOc1(String estadoOc1){
        this.estadoOc1 = estadoOc1;
    }
    public String getEstadoOc2(){
        return estadoOc2;
    }
    public void setEstadoOc2(String estadoOc2){
        this.estadoOc2 = estadoOc2;
    }
    public void setFechaIngreso(String fechaIngreso){
        this.fechaIngreso = fechaIngreso;
    }
    
    public String getFechaIngreso(){
        return fechaIngreso;
    }
    public void setFechaIni(Timestamp fechaIni){
        this.fechaIni = fechaIni;
    }
    
    public Timestamp getFechaIni(){
        return fechaIni;
    }
    public void setOrdenFutura(float ordenFutura){
        this.ordenFutura = ordenFutura;
    }
    
    public float getOrdenFutura(){
        return ordenFutura;
    }
    
    public void setOrdenVencida(float ordenVencida){
        this.ordenVencida = ordenVencida;
    }
    
    public float getOrdenVencida(){
        return ordenVencida;
    }
    
    public void setNeceEquipo(float neceEquipo){
        this.neceEquipo = neceEquipo;
    }
    
    public float getNeceEquipo(){
        return neceEquipo;
    }
    
    public void setStock(float stock){
        this.stock = stock;
    }
    
    public float getStock(){
        return stock;
    }
    
    public void setStockMin(float stockMin){
        this.stockMin = stockMin;
    }
    
    public float getStockMin(){
        return stockMin;
    }
    
    public void setStockMax(float stockMax){
        this.stockMax = stockMax;
    }
    
    public float getStockMax(){
        return stockMax;
    }
    
    public void setEstadoFacturaFA(int estadoFacturaFA){
        this.estadoFacturaFA = estadoParteFA;
    }
    
    public int getEstadoFacturaFA(){
        return estadoFacturaFA;
    }
    
    public void setRecepcionDeCompra(int recepcionDeCompra){
        this.recepcionDeCompra = recepcionDeCompra;
    }
    
    public int getRecepcionDeCompra(){
        return recepcionDeCompra;
    }
    
    public void setEstadoParteFA(int estadoParteFA){
        this.estadoParteFA = estadoParteFA;
    }
    
    public int getEstadoParteFA(){
        return estadoParteFA;
    }
    
    public void setEstado(int estado){
        this.estado = estado;
    }
    
    public int getEstado(){
        return estado;
    }
    
    public void setEstadoFactura(int estadoFactura){
        this.estadoFactura = estadoFactura;
    }
    
    public int getEstadoFactura(){
        return estadoFactura;
    }
    
    public void setEstadoParte(int estadoParte){
        this.estadoParte = estadoParte;
    }
    
    public int getEstadoParte(){
        return estadoParte;
    }
    
    public void setNumOrden(long numOrden){
        this.numOrden = numOrden;
    }
    
    public long getNumOrden(){
        return numOrden;
    }
    
    public void setNomPersona(String nomPersona){
        this.nomPersona = nomPersona;
    }
    
    public String getNomPersona(){
        return nomPersona;
    }
    
    public void setPersona(String persona){
        this.persona = persona;
    }
    
    public String getPersona(){
        return persona;
    }
    
    public void setPrecio(float precio){
        this.precio = precio;
    }
    
    public float getPrecio(){
        return precio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechaValSgc() {
        return fechaValSgc;
    }

    public void setFechaValSgc(Date fechaValSgc) {
        this.fechaValSgc = fechaValSgc;
    }

    public Date getFechaVal() {
        return fechaVal;
    }

    public void setFechaVal(Date fechaVal) {
        this.fechaVal = fechaVal;
    }

    public int getNp() {
        return np;
    }

    public void setNp(int np) {
        this.np = np;
    }

    public int getLinea() {
        return linea;
    }

    public void setLinea(int linea) {
        this.linea = linea;
    }

    public long getNumSap() {
        return numSap;
    }

    public void setNumSap(long numSap) {
        this.numSap = numSap;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public byte getValidar() {
        return validar;
    }

    public void setValidar(byte validar) {
        this.validar = validar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public byte getAnular() {
        return anular;
    }

    public void setAnular(byte anular) {
        this.anular = anular;
    }

    public int getDiferenciaDias() {
        return diferenciaDias;
    }

    public void setDiferenciaDias(int diferenciaDias) {
        this.diferenciaDias = diferenciaDias;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getObservacionGerenteArea() {
        return observacionGerenteArea;
    }

    public void setObservacionGerenteArea(String observacionGerenteArea) {
        this.observacionGerenteArea = observacionGerenteArea;
    }

    public String getObservacionGerenteLogi() {
        return observacionGerenteLogi;
    }

    public void setObservacionGerenteLogi(String observacionGerenteLogi) {
        this.observacionGerenteLogi = observacionGerenteLogi;
    }

    public int getCarroDeCompra() {
        return carroDeCompra;
    }

    public void setCarroDeCompra(int carroDeCompra) {
        this.carroDeCompra = carroDeCompra;
    }

    public String getNameCarroDeCompra() {
        return nameCarroDeCompra;
    }

    public void setNameCarroDeCompra(String nameCarroDeCompra) {
        this.nameCarroDeCompra = nameCarroDeCompra;
    }
    
    public int devolverPosicion(String valor){
        int devolver = 0;
        for(int i=0;i<valor.length();i++){
            if(valor.charAt(i) == '-'){
              devolver = i;
            }
        }
        
        return devolver;
    }

    public static String devolverObservacion(String observacion){
     String devolver = "";   
        if(observacion == null || observacion.trim().length() == 0){
            observacion = "Sin observación";
        }
     devolver = observacion.replace("\n"," ");
     devolver = devolver.replace("\r"," ");
     devolver = devolver.replace("\t"," ");
     devolver = devolver.replace("/"," ");
     devolver = devolver.replace('"',' ');
     devolver = devolver.replace('”',' ');
     devolver = devolver.replace("'"," ");
     devolver = devolver.replace("#"," ");
     //devolver = devolver.replace("$"," ");
     return devolver;
    }
    
    
    public String devolverDescripcion(String descripcion){
     String devolver = "";   
        if(descripcion == null || descripcion.length() == 0){
            descripcion = "Sin descripcion";
        }
     devolver = descripcion.replace("\n"," ");
     devolver = devolver.replace("\r"," ");
     devolver = devolver.replace("\t"," ");
     devolver = devolver.replace("/"," ");
     devolver = devolver.replace('"',' ');
     devolver = devolver.replace('”',' ');
     devolver = devolver.replace("'"," ");
     //devolver = devolver.replace("$"," ");
     return devolver;
    }

   public String getCentroCosto(){
       return centroCosto;
   }
   
   public void setCentroCosto(String centroCosto){
       this.centroCosto = centroCosto;
   }
   
   public void setCantAsignada(float cantAsignada){
       this.cantAsignada = cantAsignada;
   }
   
   public float getCantAsignada(){
       return cantAsignada;
   }
   
   public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }
    
    public void setNumDocOrigen(long numDocOrigen){
        this.numDocOrigen = numDocOrigen;
    }
    public long getNumDocOrigen(){
        return numDocOrigen;
    }
    
    public void setSaldo(float saldo){
        this.saldo = saldo;
    }
    public float getSaldo(){
        return saldo;
    }
    
    public String getAprobacion(){
        return aprobacion;
    }
    public void setAprobacion(String aprobacion){
        this.aprobacion = aprobacion;
    }
    
    public Date getFechaAnulacion(){
        return fechaAnulacion;
    }
   public void setFechaAnulacion(Date fechaAnulacion){
       this.fechaAnulacion = fechaAnulacion;
   }
   
   public void limpSap(){
        descripcion = "";
        codigo = "";
        um = "";
        stock = 0f;
        stockMin = 0f;
        stockMax = 0f;
        neceEquipo = 0f;
        ordenFutura = 0f;
        ordenVencida = 0f;
        cantidad = 0f;
   }

    public String getItemGasto() {
        return itemGasto;
    }

    public void setItemGasto(String itemGasto) {
        this.itemGasto = itemGasto;
    }

    public String getEstadoOc3() {
        return estadoOc3;
    }

    public void setEstadoOc3(String estadoOc3) {
        this.estadoOc3 = estadoOc3;
    }

    public String getNumeroDoc() {
        return numeroDoc;
    }

    public void setNumeroDoc(String numeroDoc) {
        this.numeroDoc = numeroDoc;
    }
    
}
