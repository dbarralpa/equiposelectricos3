
package mac.ee.pla.mrp.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for PedidoMateriales enterprise bean.
 */
public interface PedidoMaterialesRemoteHome extends EJBHome {
    
    PedidoMaterialesRemote create()  throws CreateException, RemoteException;
    
    
}
