
package mac.ee.pla.crp2.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for Crp2 enterprise bean.
 */
public interface Crp2Remote extends EJBObject, Crp2RemoteBusiness {
    
    
}
