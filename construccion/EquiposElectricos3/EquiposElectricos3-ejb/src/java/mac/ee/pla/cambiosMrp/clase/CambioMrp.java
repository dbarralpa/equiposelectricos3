/*
 * CambioMrp.java
 *
 * Created on 23 de noviembre de 2007, 11:28
 *
 * Autor: David Barra 
 */

package mac.ee.pla.cambiosMrp.clase;


import conexionEJB.ConexionEJB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.rmi.PortableRemoteObject;
import mac.ee.pla.crp2.bean.Crp2Bean;
import mac.ee.pla.equipo.clase.Equipo;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import mac.ee.pla.mrp.bean.MrpRemote;
import mac.ee.pla.mrp.bean.MrpRemoteHome;
import mac.ee.pla.ordenCompra.bean.OrdenRemote;
import mac.ee.pla.ordenCompra.bean.OrdenRemoteHome;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 *
 * @author dbarra
 */
public class CambioMrp {
    private Conexion myConexion= new Conexion();
    private Connection conMAC=null;
    private Statement st;
    private ResultSet rs;
    
    /*
     * M�todo que primero revisa la tabla TMovimiento extrae el material, cantidad y el tipo si es devoluci�n , OC o consumo
     * y va al m�todo stockPlanta y actualiza la base de datos.
     * luego hace la conexion al bean mrp y carga en memoria el vector con los materiales proyectados
     */
    
 public void escucharCambiosBodega(){
     
    try{
                
                ConexionEJB cal2 = new ConexionEJB();
                Object ref2 =cal2.conexionBean("MrpBean");
                MrpRemoteHome mrpHome1 = (MrpRemoteHome) PortableRemoteObject.narrow(ref2, MrpRemoteHome.class);
                MrpRemote mrp = mrpHome1.create();
                Vector vacio = new Vector();
                mrp.actualizarMaterialesTconsumoPromedio();               
                mrp.recalcularEstadoMaterial();
                Vector materialesRetornados = mrp.retornarMaterialesEliminadosOcargados();
                 String descripcion = "";
                 for(int i=0;i<materialesRetornados.size();i++){
                     mrp.materialesProyectados((String)((Vector)materialesRetornados.elementAt(i)).elementAt(0),(String)((Vector)materialesRetornados.elementAt(i)).elementAt(1),1,"","",0,(String)((Vector)materialesRetornados.elementAt(i)).elementAt(3));
                 }
                 mrp.retornarMaterialesEliminadosOcargadosVacio(vacio);
//--------------------------------------------------------------------------------------------------------------------------------                
               Crp2Bean crp = new Crp2Bean();
               Vector equipos = crp.recalcularEstadoMaterialParaActualizar();
                if(equipos.size() > 0){
                  for(int i=0;i<equipos.size();i++){
                     int pedido = ((Equipo)equipos.elementAt(i)).getNp();
                     int linea =  ((Equipo)equipos.elementAt(i)).getLinea();
                     int identificador = ((Equipo)equipos.elementAt(i)).getIdentificador();
                     //System.out.println("np: " + pedido + " linea " + linea + " identificador " + identificador);
                     crp.actualizarEquipos(pedido,linea,identificador);
                     crp.eliminarMaterialesRepetidos(pedido,linea);
                 }

                crp.createMapa(crp.retornarEquipos());
                crp.borrarEquipos();
                crp.retornarEquipos(vacio);
                ConexionEJB cal3 = new ConexionEJB();  
                Object ref3 =cal3.conexionBean("MrpBean");
                MrpRemoteHome mrpHome3 = (MrpRemoteHome) PortableRemoteObject.narrow(ref3, MrpRemoteHome.class);
                MrpRemote mrp3 = mrpHome3.create();
                Vector materialesRepetidos = crp.retornarEliminarMaterialesRepetidos();
                /*for(int i=0;i<materialesRepetidos.size();i++){
                      for(int u=0;u<materiales.size();u++){
                          if(((Equipo)materialesRepetidos.elementAt(i)).getCodPro().equals((String)((Vector)materiales.elementAt(u)).elementAt(0))){
                              materiales.remove(u);
                          }
                      }
                  }
                mrp.controlDeMateriales(materiales);*/
                String descri = "";
                for(int i=0;i<materialesRepetidos.size();i++){
                    if(((EstructuraPorEquipo)materialesRepetidos.elementAt(i)).getDescripcion() == null || ((EstructuraPorEquipo)materialesRepetidos.elementAt(i)).getDescripcion().equals("")){
                       descri = "SIN DESCRIPCION"; 
                    }else{
                       descri =  ((EstructuraPorEquipo)materialesRepetidos.elementAt(i)).getDescripcion();
                    }
                    mrp.materialesProyectados(((EstructuraPorEquipo)materialesRepetidos.elementAt(i)).getCodigoMatPrima(),descri,1,"","",0,((EstructuraPorEquipo)materialesRepetidos.elementAt(i)).getUnidadMedida());
                }
                crp.retornarEliminarMaterialesRepetidos(vacio);
                }
                new Loger().logger("CambioMrp.class: "," Hizo su trabajo",1);
        }catch(Exception e){
                new Loger().logger("CambioMrp.class: "," M�todo escucharCambiosBodega() " +  e.getMessage(),0);
                System.exit(0);
                }
        }
}