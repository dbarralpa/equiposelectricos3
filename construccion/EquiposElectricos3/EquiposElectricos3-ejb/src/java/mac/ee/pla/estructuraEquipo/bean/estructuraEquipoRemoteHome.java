
package mac.ee.pla.estructuraEquipo.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for estructuraEquipo enterprise bean.
 */
public interface estructuraEquipoRemoteHome extends EJBHome {
    
    estructuraEquipoRemote create()  throws CreateException, RemoteException;
    
    
}
