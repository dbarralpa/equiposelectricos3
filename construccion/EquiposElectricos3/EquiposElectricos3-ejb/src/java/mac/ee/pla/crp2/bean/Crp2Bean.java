package mac.ee.pla.crp2.bean;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.ejb.*;
import mac.ee.pla.equipo.clase.Equipo;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 * This is the bean class for the Crp2Bean enterprise bean.
 * Created 04-feb-2008 15:47:08
 * @author dbarra
 */
public class Crp2Bean implements SessionBean, Crp2RemoteBusiness {
    private SessionContext context;
    private Connection con;
    private Statement st;
    private ResultSet rs;
    private Statement stm;
    private ResultSet rst;
    private Vector vEquipos = new Vector();
    private Vector procesos = new Vector();
    private Date fechaNueva = new Date(0);
    private long produccion1   = Long.MIN_VALUE;
    private Conexion myConexion= new Conexion();
    private Conexion mConexion= new Conexion();
    private Connection conMAC=null;
    private Connection conENS=null;
    private String familia="";
    private String kva="";
    public final byte SIMPLE =1;
    public final byte RETROACTIVO =2;
    private Vector saveMapa = new Vector();
    private Vector saveMapa1 = new Vector();
    Date fechaMayor = new Date(0);
    Vector agregarEstructuraAProceso = new Vector();
    Vector equiposConMateriales = new Vector();
    java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
    java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("yyyy-MM-dd");
    String fechaM = formateador.format(new java.util.Date());
    String fechaN = formate.format(new java.util.Date());
    Vector equiposTrouble = new Vector();
    Vector equipos = new Vector();
    Vector eliminarMaterialesRepetidos = new Vector();
    Vector recuperarEquipos = new Vector();
    Vector detalle = new Vector();
    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    
    
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")
    
    public void getNotValidTimming(){
        String query = "";
        try{
            
            conENS = myConexion.getENSConnector();
            stm = conENS.createStatement();
            
            query = " SELECT Pedido,Linea,Familia,KVA,Expeditacion,Codpro"
                    +" FROM  ENSchaffner.dbo.TPMP"
                    //+" WHERE Status <> 'Bodega' AND Familia <> 'REP' AND Status IS NOT NULL AND Activa != 1 AND Status <> 'Reproceso'  "
                    +" WHERE Status <> 'Bodega' AND Familia <> 'REP' AND Status IS NOT NULL AND Activa != 1 "
                    +" ORDER BY Pedido,Linea";
            //+" and Linea = 1 and Pedido = 77118  ";
            rst = stm.executeQuery(query);
            while(rst.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(rst.getInt(1));
                equipo.setLinea(rst.getInt(2));
                equipo.setFamilia(rst.getString(3));
                equipo.setKva(rst.getString(4));
                equipo.setFechaInicio(rst.getDate(5));
                equipo.setCodPro(rst.getString(6));
                equipo.setIdentificador(0);
                vEquipos.addElement(equipo);
            }
            rst.close();
            myConexion.cerrarConENS();
        } catch(Exception e){
            myConexion.cerrarConENS();
            new Loger().logger("CrpBean.class "," getNotValidTimming()  "+e.getMessage(),0);
            
        }
    }
    //---------------------------------------------------------------------------------------------------------------------------
    public Vector actualizarEquipos(int np , int linea , int identificador){
        
        try{
            conENS = myConexion.getENSConnector();
            stm = conENS.createStatement();
            String query = "SELECT Pedido,Linea,Familia,KVA,Expeditacion,Codpro FROM ENSchaffner.dbo.TPMP  WHERE Pedido = " + np + " and Linea = " + linea +  " " ;
            rst = stm.executeQuery(query);
            while(rst.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(rst.getInt(1));
                equipo.setLinea(rst.getInt(2));
                equipo.setFamilia(rst.getString(3));
                equipo.setKva(rst.getString(4));
                equipo.setFechaInicio(rst.getDate(5));
                //equipo.setFechaInicio(new Date(0).valueOf("2008-12-15"));
                equipo.setCodPro(rst.getString(6));
                equipo.setIdentificador(identificador);
                vEquipos.addElement(equipo);
            }
            rst.close();
            stm.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class ","ERROR actualizarEquipos() abriendo conENS "+e.getMessage(),0);
            try{
                rst.close();
                stm.close();
                conENS.close();
            }catch(Exception ex){
                new Loger().logger("CrpBean.class ","ERROR actualizarEquipos() volvi� a fallar abriendo conENS "+e.getMessage(),0);
            }
        }
        return vEquipos;
    }
    //--------------------------------------------------------------------------------------------------------------------------
    public Vector retornarEquipos(){
        return vEquipos;
    }
    public Vector retornarEquipos(Vector vacio){
        vEquipos = vacio;
        return vEquipos;
    }
    
    //----------------------------------------------------------------------------------------------------------------------------
    public void createMapa(Vector vEquiposParaMapear){
        String query = "";
        int npParaMapear=-1;
        int lineaParaMapear=-1;
        Date date = new Date(0);
        String codPro ="";
        String kva = "";
        int identificador = -1;
        //Equipo equipoParaMapear=  new Equipo();
        Vector equiposVec1 = new Vector();
        Vector vMapa = new Vector();
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
        }catch(Exception e){
            new Loger().logger("CrpBean.class ","ERROR createMapa() abriendo conMAC "+e.getMessage(),0);
        }
        
        for(int i=0; i< vEquiposParaMapear.size();i++){
            npParaMapear = ((Equipo)vEquiposParaMapear.elementAt(i)).getNp();
            lineaParaMapear = ((Equipo)vEquiposParaMapear.elementAt(i)).getLinea();
            date =            ((Equipo)vEquiposParaMapear.elementAt(i)).getFechaInicio();
            codPro = ((Equipo)vEquiposParaMapear.elementAt(i)).getCodPro();
            familia = ((Equipo)vEquiposParaMapear.elementAt(i)).getFamilia();
            kva = ((Equipo)vEquiposParaMapear.elementAt(i)).getKva();
            identificador = ((Equipo)vEquiposParaMapear.elementAt(i)).getIdentificador();
            //equipoParaMapear= this.getEquipo(npParaMapear,lineaParaMapear);
            //System.out.println(npParaMapear + " " + lineaParaMapear + " " + date + " " + codPro + " " + familia + " " + kva + " " + identificador);
            
            try{
                if (  familia.equals("BIFA") ||  familia.equals("CELDA") ||  familia.equals("ECM") ||  familia.equals("OTROS") ||
                        familia.equals("ESTANQUE") ||  familia.equals("BRIDA") ||  familia.equals("CHASIS") ||  familia.equals("FLANGUE") ||
                        familia.equals("PASADAMURO") ||  familia.equals("COLADORFLUIDO") ||  familia.equals("CURVA") ||  familia.equals("SOPORTE") ||
                        familia.equals("PUNTERA") ||  familia.equals("ESCUADRA") ||  familia.equals("PIEZAESPECIAL") ||  familia.equals("VARILLABRONCE") ||
                        familia.equals("MANIFOLD") ||  familia.equals("TABLERO") ||  familia.equals("BANDEJASBPC")){
                    query = "select SecuenciaPorFamilia.proceso,SecuenciaPorFamilia.nodoInicio, SecuenciaPorFamilia.nodoFin,timmingPorFamilia.familia,timmingPorFamilia.processTime,timmingPorFamilia.proceso  from SecuenciaPorFamilia,timmingPorFamilia where timmingPorFamilia.familia = SecuenciaPorFamilia.familia and SecuenciaPorFamilia.familia ='"+ familia +"' AND timmingPorFamilia.proceso = SecuenciaPorFamilia.proceso";
                    rs = st.executeQuery(query);
                }else{
                    
                    for(int p=0;p < kva.length();p++){
                        if(kva.charAt(p) == '/'){
                            kva = "1";
                        }
                    }
                    query = "select SecuenciaPorFamilia.proceso,SecuenciaPorFamilia.nodoInicio, SecuenciaPorFamilia.nodoFin,timmingPorFamilia.familia,timmingPorFamilia.processTime,timmingPorFamilia.proceso  from SecuenciaPorFamilia,timmingPorFamilia where timmingPorFamilia.familia = SecuenciaPorFamilia.familia and SecuenciaPorFamilia.familia ='"+ familia +"' AND timmingPorFamilia.proceso = SecuenciaPorFamilia.proceso and " + kva + " between timmingPorFamilia.kvaMinimo and timmingPorFamilia.kvaMaximo";
                    rs = st.executeQuery(query);
                }
                vMapa.clear();
                int s = 0;
                while(rs.next()){
                    Vector vLineaMapa=new Vector();
                    vLineaMapa.addElement(rs.getString(1)); // Proceso
                    vLineaMapa.addElement(new Integer(rs.getInt(2))); // NodoInicio
                    vLineaMapa.addElement(new Integer(rs.getInt(3))); // NodoFin
                    vLineaMapa.addElement(new Integer(rs.getInt(5))); // PT
                    vLineaMapa.addElement(new Date(0)); // RT
                    vLineaMapa.addElement(new Date(0)); // CT
                    vLineaMapa.addElement(new Integer(0)); // TTB
                    vLineaMapa.addElement(new Integer(0)); // TTL
                    vLineaMapa.addElement(new Integer(npParaMapear));
                    vLineaMapa.addElement(new Integer(lineaParaMapear));
                    vLineaMapa.addElement(codPro);
                    vLineaMapa.addElement(familia);
                    vMapa.addElement(vLineaMapa);
                }
                setMapa(vMapa , npParaMapear , lineaParaMapear);
            } catch(Exception e){
                Equipo equipo = new Equipo();
                equipo.setNp(npParaMapear);
                equipo.setLinea(lineaParaMapear);
                equipo.setKva(kva);
                equipo.setFamilia(familia);
                //equiposTrouble.addElement(equipo);
                new Loger().logger("Equipos con problemas: " , " np " + String.valueOf(npParaMapear) + " linea " + lineaParaMapear , 0 );
                new Loger().logger("CrpBean.class "," ERROR: createMapa() " + e.getMessage() ,0);
            }
            if(vMapa.size() > 0){
                if(identificador == 8){
                    // this.calculateMapa(this.getMapa(npParaMapear,lineaParaMapear),date,2  );
                    this.procesosMapaAsBuild(this.mapaAsBuild(npParaMapear,lineaParaMapear),date,identificador);
                } else if(identificador == 7){
                    this.calculateMapa(this.getMapa(npParaMapear,lineaParaMapear),date,2  );
                    this.procesosMapaAsBuild(this.mapaAsBuild(npParaMapear,lineaParaMapear),date,identificador);
                } else{
                    this.calculateMapa(this.getMapa(npParaMapear,lineaParaMapear),date,2);
                    
                    this.procesosMapaAsBuild(this.mapaAsBuild(npParaMapear,lineaParaMapear),date,0);
                }
            }
            //this.procesosMapaAsBuild(this.mapaAsBuild(npParaMapear,lineaParaMapear),date,0);
        }//Fin ciclo for
        
        
        try{
            rs.close();
            mConexion.cerrarConMAC();
        } catch(Exception e){
            mConexion.cerrarConMAC();
            new Loger().logger("CrpBean.class "," createMapa() " + e.getMessage(),1);
        }
        
    }
//------------------------------------------------------------------------------------------------------------------------
    public Vector equiposConProblemas(){
        return equiposTrouble;
    }
//------------------------------------------------------------------------------------------------------------------------
    
    public boolean setEquipo(Equipo myEquipo, int npParaMapear , int lineaParaMapear){
        
        for(int j=0; j< vEquipos.size();j++){
            
            if(    (myEquipo.getNp()   == npParaMapear     ) &&
                    (myEquipo.getLinea()== lineaParaMapear  )      ) {
                vEquipos.setElementAt(myEquipo ,j);
                return true; // Lo encontr�
            }
        }
        
        return false;
    }
    
//-------------------------------------------------------------------------------------------------------------------------
    
    public Equipo getEquipo(int npParaMapear , int lineaParaMapear){
        
        for(int j=0; j< vEquipos.size();j++){
            if(    (((Equipo)vEquipos.elementAt(j)).getNp()   == npParaMapear     ) &&
                    (((Equipo)vEquipos.elementAt(j)).getLinea()== lineaParaMapear  )      ) {
                return (Equipo)vEquipos.elementAt(j); // Lo encontr�
            }
        }
        
        return null;
        
    }
    //-------------------------------------------------------------------------------------------
    
    public boolean setMapa(Vector myMapa, int npParaMapear , int lineaParaMapear){
        
        for(int j=0; j< vEquipos.size();j++){
            
            if(    (((Equipo)vEquipos.elementAt(j)).getNp()   == npParaMapear     ) &&
                    (((Equipo)vEquipos.elementAt(j)).getLinea()== lineaParaMapear  )      ) {
                ((Equipo)vEquipos.elementAt(j)).setVMapa(myMapa);
                return true; // Lo encontr�
            }
        }
        
        return false;
    }
    //----------------------------------------------------------------------------------------------------------------
    public Vector getMapa(int npParaMapear , int lineaParaMapear){
        for(int j=0; j< vEquipos.size();j++){
            if(    (((Equipo)vEquipos.elementAt(j)).getNp()     == npParaMapear     ) &&
                    (((Equipo)vEquipos.elementAt(j)).getLinea() == lineaParaMapear  )      ) {
                
                return ((Equipo)vEquipos.elementAt(j)).getVMapa();
            }
        }
        
        return null;
        
    }
    //-------------------------------------------------------------------------------------------------------------------
    
    private Vector buildMapaSimple(Vector mapa , Date fechaInicio){
        
        try{
            int nodoFinal=-1;
            int procesoEnObservacion=-1;
            for(int i = 0;i < mapa.size();i++){    // Borra todas las fechas
                
                ((Vector)mapa.elementAt(i)).setElementAt( new Date(0), 4 );
                ((Vector)mapa.elementAt(i)).setElementAt( new Date(0), 5 );
                
            }
            
            // PASO 1 Poner fechas a los que parten en Nodo 0
            for(int i = 0;i < mapa.size();i++){
                if(((Integer)((Vector)mapa.elementAt(i)).elementAt(1)).intValue() == 0){
                    ((Vector)mapa.elementAt(i)).setElementAt(fechaInicio,4)  ;
                    ((Date)((Vector)mapa.elementAt(i)).elementAt(5)).setTime   (   fechaInicio.getTime()+ ((Integer)((Vector)mapa.elementAt(i)).elementAt(3)).intValue() * 86400000);
                }
            }// for
            
            // PASO 2 Buscar� el nodo final
            
            nodoFinal = this.findNodoFinal(mapa);
            
            // PASO 3 Espera que al nodo final le llegue un proceso con Completion Time v�lido
            
            while( (procesoEnObservacion=analizaNodoFinal(mapa,nodoFinal)) >= 0 ){
                
                if(procesoEnObservacion==-1) {
                    //System.out.println(" Termin�...");
                }
                
                
                while((procesoEnObservacion=analizaProceso(mapa,procesoEnObservacion  )) >= 0) {
                    //  El proceso en Observaci�n no tiene Fecha de T�rmino,
                    //  por lo tanto, debe analizar el nodo anterior a �l para
                    //  conseguir su timming
                }//fin while analizaProceso
            }//fin while analizaNodoFinal
            //this.desplegarMapa(mapa);
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: buildMapaSimple() " + e.getMessage(),0);
        }
        return mapa;
    }
    
//-------------------------------------------------------------------------------------------------------
    // M�todo que retorna la posici�n del nodo final
    private int  analizaNodoFinal(Vector mapa , int nodoFinal){
        // Dado que conoce el nodo final, analiza los procesos que
        // llega a �l. Si todos tienen la fecha det�rmino v�lida,
        // retorna -1
        // En caso contrario, retorna el primer proceso que no tiene
        // fecha v�lida
        try{
            for(int i = 0;i < mapa.size();i++){
                
                if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == nodoFinal) {
                    if( ((Date)((Vector)mapa.elementAt(i)).elementAt(5)).hashCode()== new Date(0).hashCode() ){
                        return i;
                    }
                    return -1;
                }
            }
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR analizaNodoFinal() "  + e.getMessage(),0);
        }
        return 0; // Nunca puede ocurrir
    }
    
    //-----------------------------------------------------------------------------------------------------------------------
    
    private int analizaProceso(Vector mapa , int procesoEnObservacion ){
        // Analiza el Nodo del cual sale el procesoEnObservacion
        // Si al nodo en cuesti�n llegan n procesos devuelve el valor
        // del primero de ellos que NO tiene definida la fecha de t�rmino.
        // De esta manera, el que llam� a este m�todo puede iterar en busca
        // del siguiente nodo
        // Por el contrario, si todos los procesos que llegan tienen fechas
        // v�lidas, calcular� el timming del procesoEnObservacion
        // y retornar� -1.
        try{
            int nodoIni =((Integer)((Vector)mapa.elementAt(procesoEnObservacion)).elementAt(1)).intValue();
            for(int i = 0;i < mapa.size();i++){
                if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == nodoIni) {
                    
                    if( ((Date)((Vector)mapa.elementAt(i)).elementAt(5)).hashCode()== (new Date(0)).hashCode() ){
                        return i;
                    }
                }
            }
            // El proceso anterior tiene todas las fechas
            
            Date fechaMayor = new Date(0);
            
            for(int i = 0;i < mapa.size();i++){
                if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == nodoIni) {
                    if(  ((Date)((Vector)mapa.elementAt(i)).elementAt(5)).hashCode()!= (new Date(0)).hashCode() ){
                        if(   fechaMayor.compareTo( (Date)((Vector)mapa.elementAt(i)).elementAt(5)) < 0 )
                            fechaMayor= (Date)((Vector)mapa.elementAt(i)).elementAt(5);
                    }
                }
                
            }
            
            
            ((Vector)mapa.elementAt( procesoEnObservacion  )).setElementAt(  fechaMayor,4);
            
            long x =  fechaMayor.getTime();
            long y=((Integer)((Vector)mapa.elementAt(procesoEnObservacion)).elementAt(3)).intValue() * 86400000;
            ((Vector)mapa.elementAt( procesoEnObservacion  )).setElementAt(  new Date(x+y)   ,5);
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: analizaProceso() "  + e.getMessage() , 0);
        }
        return -1;//procesoEnObservacion;
        
    }
    
    //------------------------------------------------------------------------------------------------------------------------
    
    public long tiempoProduccion(Vector mapa,Date fecha){
        int nodoFinal= findNodoFinal(mapa);
        Date fechaMenor= (Date)((Vector)mapa.elementAt(0)).elementAt(4);
        Date fechaMayor= new Date(0);
        for(int i = 0;i < mapa.size();i++){
            if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == nodoFinal) {
                fechaMayor = ((Date)((Vector)mapa.elementAt(i)).elementAt(5));
                if(((Date)((Vector)mapa.elementAt(i)).elementAt(4)).compareTo(fechaMenor) < 0){
                    fechaMenor = (Date)((Vector)mapa.elementAt(i)).elementAt(4);
                }
            }
        }
        return  fechaMayor.getTime() - fechaMenor.getTime();
    }
    //-------------------------------------------------------------------------------------------------------------------------
    
    public int findNodoFinal(Vector mapa){
        int nodoFinal=-1;
        for(int i = 0;i < mapa.size();i++){
            
            for(int j = 0;j < mapa.size();j++){
                
                if( ((Integer)((Vector)mapa.elementAt(j)).elementAt(1)).intValue() ==
                        (nodoFinal=((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue())){
                    
                    nodoFinal=-1;
                    break;
                }
                
            }
            if( nodoFinal!=-1){
                
                break;
            }
        }
        
        return nodoFinal;
    }
    
    //-----------------------------------------------------------------------------------------------------------
    
    public Vector calculateMapa(Vector mapa , Date fecha, int algoritmo){
        
        try{
            
            this.buildMapaSimple(mapa,fecha);
            long aux =tiempoProduccion( mapa, fecha);
            this.buildMapaSimple(mapa,new Date( fecha.getTime()-aux ));
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
            
            for(int d = 0; d<mapa.size();d++){
                this.saveMapa(((Integer)((Vector)mapa.elementAt(d)).elementAt(8)).intValue(),
                        ((Integer)((Vector)mapa.elementAt(d)).elementAt(9)).intValue(),
                        (String)((Vector)mapa.elementAt(d)).elementAt(0),
                        ((Integer)((Vector)mapa.elementAt(d)).elementAt(3)).intValue(),
                        formateador.format((Date)((Vector)mapa.elementAt(d)).elementAt(4)),
                        formateador.format((Date)((Vector)mapa.elementAt(d)).elementAt(5)),
                        ((Integer)((Vector)mapa.elementAt(d)).elementAt(6)).intValue(),
                        ((Integer)((Vector)mapa.elementAt(d)).elementAt(7)).intValue(),
                        (String)((Vector)mapa.elementAt(d)).elementAt(10),
                        (String)((Vector)mapa.elementAt(d)).elementAt(11),
                        0);
            }
            mapa.clear();
            
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " calculateMapa() " + e.getMessage(),0);
            
        }
        
        return null;
    }
    
    //------------------------------------------------------------------------------------------------------------
    // M�todo que guarda en la base de datos los equipos con su mapa de fechas de fabricaci�n
    public boolean saveMapa(int pedido , int linea ,  String proceso, int processTime, String readTime ,String completionTime , int timeToBegin , int timeToLead , String codPro , String familia , int vigencia){
        int contador = 0;
        try{
            String query4 = "SELECT COUNT(DISTINCT np) FROM MAC.dbo.MapaAsBuild where np = '" + pedido + "' and linea = '" + linea + "' and proceso = '"+ proceso +"'";
            rs = st.executeQuery(query4);
            while(rs.next()){
                
                contador =  rs.getInt(1);
            }
            if(contador == 0){
                String query1 = "insert into MAC.dbo.MapaAsBuild (np,linea,proceso,processTime,readTime,completionTime,timeToBegin,timeToLead,codProducto,familia,vigencia) values ('"+ pedido +"','"+ linea +"','"+ proceso +"' ,'"+ processTime +"','"+ readTime +"','"+ completionTime +"','"+ timeToBegin +"','"+ timeToLead +"','"+ codPro +"','"+ familia +"','"+ vigencia +"')";
                st.execute(query1);
            }else{
                String query1 ="UPDATE MAC.dbo.MapaAsBuild SET readTime = '" + readTime + "' , completionTime = '" + completionTime + "'"
                        +" WHERE np = " + pedido + " and linea = " + linea + " and proceso = '" + proceso + "'";
                st.execute(query1);
            }
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " M�todo saveMapa " + e.getMessage() ,0 );
            return false;
        }
        
        return true;
    }
//--------------------------------------------------------------------------------------------------------------------------
    public void desplegarMapa(Vector mapa){
        
        Vector vAux = new Vector();
        
        System.out.println( "--------------   MAPA  ---------------------   ");
        
        for(int i=0;i<mapa.size();i++){
            vAux = (Vector)mapa.elementAt(i);
            System.out.println(
                    vAux.elementAt(9) + "---"+
                    vAux.elementAt(10) + "---"+
                    vAux.elementAt(0) + "---"+
                    vAux.elementAt(3) + "---"+
                    vAux.elementAt(4) + "---"+
                    vAux.elementAt(5) + "---"+
                    vAux.elementAt(6) + "---"+
                    vAux.elementAt(7) + "---"+
                    vAux.elementAt(1) + "---"+
                    vAux.elementAt(2) + "---"+
                    vAux.elementAt(8) + "---"+
                    vAux.elementAt(11)// + "---"+
                    //vAux.elementAt(12)
                    );
        }
        System.out.println( "--------------------------------------------   ");
        
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public Vector mapaAsBuild(int pedido , int linea){
        
        String codigoEstrctura = "";
        String proceso = "";
        Vector estructura = new Vector();
        String query4  = " SELECT a.proceso,d.nodoInicio,d.nodoFin,a.processTime,a.readTime,a.completionTime,b.FechaInicio,b.FechaTermino,a.familia,a.np,a.linea,b.Orden,a.vigencia"
                +" FROM MAC.dbo.MapaAsBuild a"
                +" LEFT OUTER JOIN ENSchaffner.dbo.TAvance b"
                +" ON (a.np = b.Pedido and a.linea = b.Linea)"
                +" AND a.proceso=b.Semielaborado "
                +" LEFT OUTER JOIN  MAC.dbo.SecuenciaPorFamilia d"
                +" ON a.familia = d.familia"
                +" AND a.proceso = d.proceso"
                +" WHERE  a.np = '" + pedido + "' and a.linea = '" + linea + "'"
                +" ORDER BY a.np ,a.linea,nodoInicio,nodoFin";
        try{
            //conMAC = mConexion.getMACConnector();
            //st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Vector mapaAsBuild = new Vector();
                mapaAsBuild.addElement(rs.getString(1)); // proceso 0
                mapaAsBuild.addElement(new Integer(rs.getInt(2))); // nodoInicio 1
                mapaAsBuild.addElement(new Integer(rs.getInt(3))); // nodoFin 2
                mapaAsBuild.addElement(new Integer(rs.getInt(4))); // processTime 3
                mapaAsBuild.addElement(rs.getDate(5)); // readTime 4
                mapaAsBuild.addElement(rs.getDate(6)); // completionTime 5
                mapaAsBuild.addElement(rs.getDate(7)); // fechaInicio 6
                mapaAsBuild.addElement(rs.getDate(8)); // fechaTermino 7
                mapaAsBuild.addElement(rs.getString(9)); // familia 8
                mapaAsBuild.addElement(new Integer(rs.getInt(10))); // np 9
                mapaAsBuild.addElement(new Integer(rs.getInt(11))); // linea 10
                mapaAsBuild.addElement(new Integer(rs.getInt(12))); // orden 11
                mapaAsBuild.addElement(new Integer(rs.getInt(13))); // vigencia 12
                procesos.addElement(mapaAsBuild);
                
            }
            //this.desplegarMapa(equipo);
            
        }catch(Exception e){
            new Loger().logger("CrpBean.class " ,"ERROR: mapaAsBuild (): "+e.getMessage(),0);
        }
        return procesos;
    }
    
//------------------------------------------------------------------------------------------------------------------------------
    public Vector mapaAsBuildConMateriales(){
        Vector mapaAsBuildConMateriales = new Vector();
        for(int i=0;i<procesos.size();i++){
            if (((Integer)((Vector)procesos.elementAt(i)).elementAt(12)).intValue() != 1){
                if(((String)((Vector)procesos.elementAt(i)).elementAt(0)).compareTo("HNO") !=0){
                    if(((String)((Vector)procesos.elementAt(i)).elementAt(0)).compareTo("TER") !=0){
                        if(((String)((Vector)procesos.elementAt(i)).elementAt(0)).compareTo("RF") !=0){
                            Vector mapaAsBuildConMaterial = new Vector();
                            mapaAsBuildConMaterial.addElement( (String)((Vector)procesos.elementAt(i)).elementAt(0));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(1));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(2));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(3));
                            mapaAsBuildConMaterial.addElement((Date)((Vector)procesos.elementAt(i)).elementAt(4));
                            mapaAsBuildConMaterial.addElement((Date)((Vector)procesos.elementAt(i)).elementAt(5));
                            mapaAsBuildConMaterial.addElement((Date)((Vector)procesos.elementAt(i)).elementAt(6));
                            mapaAsBuildConMaterial.addElement((Date)((Vector)procesos.elementAt(i)).elementAt(7));
                            mapaAsBuildConMaterial.addElement((String)((Vector)procesos.elementAt(i)).elementAt(8));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(9));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(10));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(11));
                            mapaAsBuildConMaterial.addElement((Integer)((Vector)procesos.elementAt(i)).elementAt(12));
                            mapaAsBuildConMateriales.addElement(mapaAsBuildConMaterial);
                        }
                    }
                    
                }
                
            }
        }
        
        return mapaAsBuildConMateriales;
        
    }
    
//----------------------------------------------------------------------------------------------------------------------------
    
    public Vector mapaAsBuildConEstructura(String material,String fechaMenor,String fechaMayor,int compara){
        String query4 = "";
        if(compara == 0){
           /*vista VW_MapaAsBuild
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE a.vigencia != 1 AND  a.readTime >= '" + fechaM + "' and tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY tp.Expeditacion  ";
            */
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.VW_MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY tp.Expeditacion  ";
        }else{
           /*vista VW_MapaAsBuild
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE a.vigencia != 1 AND  tp.Expeditacion BETWEEN '" + fechaMenor + "' AND '" + fechaMayor + "' and tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY tp.Expeditacion  ";
            */
            
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.VW_MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE tp.Expeditacion BETWEEN '" + fechaMenor + "' AND '" + fechaMayor + "' and tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY tp.Expeditacion  ";
        }
               
        /*vista VW_MapaAsBuild
        String query  = " SELECT ma.readTime,tp.Pedido,tp.Linea,me.CodigoSemiElaborado,me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
        query += " FROM ENSchaffner.dbo.MEstructura me ";
        query += " inner join ENSchaffner.dbo.TPMP tp";
        query += " on(me.CodigoTerminado = tp.Codpro )";
        query += " inner join MAC.dbo.MapaAsBuild ma";
        query += " on(ma.np = tp.Pedido and ma.linea = tp.Linea)";
        query += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
        query += " on(ep.nb_np = ma.np and ep.nb_linea = ma.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS)";
        query += " WHERE  ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA'";
        query += " AND ma.readTime >= '" + fechaM + "'";
        query += " AND ma.vigencia != 1";
        query += " AND me.CodigoMatPrima in('" + material + "')";
        query += " ORDER BY tp.Expeditacion";
         */
        
        String query  = " SELECT ma.readTime,tp.Pedido,tp.Linea,me.CodigoSemiElaborado,me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
        query += " FROM ENSchaffner.dbo.MEstructura me ";
        query += " inner join ENSchaffner.dbo.TPMP tp";
        query += " on(me.CodigoTerminado = tp.Codpro )";
        query += " inner join MAC.dbo.VW_MapaAsBuild ma";
        query += " on(ma.np = tp.Pedido and ma.linea = tp.Linea)";
        query += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
        query += " on(ep.nb_np = ma.np and ep.nb_linea = ma.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS)";
        query += " WHERE  ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA'";
        query += " AND me.CodigoMatPrima in('" + material + "')";
        query += " ORDER BY tp.Expeditacion";
        
        
        /*String queryMarcaje = " select ar.FechaProgramada ,tp.Pedido , tp.Linea , mr.Proceso , mr.Cantidad, mr.Material,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie";
        queryMarcaje += " ,isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'SI' ";
        queryMarcaje += " from ENPrueba.dbo.EE_materiales_rep mr";
        queryMarcaje += " inner join ENPrueba.dbo.TPMP tp";
        queryMarcaje += " on(tp.Pedido = mr.Pedido and tp.Linea = mr.Linea)";
        queryMarcaje += " inner join ENPrueba.dbo.EE_avance_rep ar";
        queryMarcaje += " on(tp.Pedido = mr.Pedido and tp.Linea = mr.Linea and ar.Proceso = mr.Proceso)";
        queryMarcaje += " inner join MAC.dbo.MapaAsBuild a ";
        queryMarcaje += " on(a.np = mr.Pedido AND a.linea = mr.Linea and a.proceso = mr.Proceso)";
        queryMarcaje += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
        queryMarcaje += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  mr.Material COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
        queryMarcaje += " where Status = 'Reproceso' and Activa <> 1 and ar.FechaTermino is null and mr.Material in('" + material + "') ";*/
        
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
           /* rs = st.executeQuery(queryMarcaje);
            
            while(rs.next()){
                Vector mapaAsBuild = new Vector();
                mapaAsBuild.addElement(rs.getDate(1)); // readTime 0
                mapaAsBuild.addElement(new Integer(rs.getInt(2))); // np 1
                mapaAsBuild.addElement(new Integer(rs.getInt(3))); // linea 2
                mapaAsBuild.addElement(rs.getString(6)); // material 3
                mapaAsBuild.addElement(new Float(rs.getFloat(5))); // cantidad de material 4
                mapaAsBuild.addElement(rs.getString(4)); // proceso 5
                mapaAsBuild.addElement(rs.getString(7)); // cliente 6
                mapaAsBuild.addElement(rs.getDate(8)); // expeditaci�n 7
                mapaAsBuild.addElement(rs.getString(9)); //descripcion 8
                mapaAsBuild.addElement(rs.getString(10)); //dise�o 9
                mapaAsBuild.addElement(rs.getString(11)); // serie 10
                mapaAsBuild.addElement(new Float(rs.getFloat(12))); // Cantidad entregada de bodega 11
                mapaAsBuild.addElement(rs.getString(13)); // Reproceso 12
                
                equiposConMateriales.addElement(mapaAsBuild);
            }
            
            rs.close();*/
            
            
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Vector mapaAsBuild = new Vector();
                mapaAsBuild.addElement(rs.getDate(1)); // readTime 0
                mapaAsBuild.addElement(new Integer(rs.getInt(2))); // np 1
                mapaAsBuild.addElement(new Integer(rs.getInt(3))); // linea 2
                mapaAsBuild.addElement(rs.getString(6)); // material 3
                mapaAsBuild.addElement(new Float(rs.getFloat(5))); // cantidad de material 4
                mapaAsBuild.addElement(rs.getString(4)); // proceso 5
                mapaAsBuild.addElement(rs.getString(7)); // cliente 6
                mapaAsBuild.addElement(rs.getDate(8)); // expeditaci�n 7
                mapaAsBuild.addElement(rs.getString(9)); //descripcion 8
                mapaAsBuild.addElement(rs.getString(10)); //dise�o 9
                mapaAsBuild.addElement(rs.getString(11)); // serie 10
                mapaAsBuild.addElement(new Float(rs.getFloat(12))); // Cantidad entregada de bodega 11
                mapaAsBuild.addElement(rs.getString(13)); // Reproceso 12
                equiposConMateriales.addElement(mapaAsBuild);
            }
            
            rs.close();
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                Vector mapaAsBuild = new Vector();
                mapaAsBuild.addElement(rs.getDate(1)); // readTime 0
                mapaAsBuild.addElement(new Integer(rs.getInt(2))); // np 1
                mapaAsBuild.addElement(new Integer(rs.getInt(3))); // linea 2
                mapaAsBuild.addElement(rs.getString(6)); // material 3
                mapaAsBuild.addElement(new Float(rs.getFloat(5))); // cantidad de material 4
                mapaAsBuild.addElement(rs.getString(4)); // proceso 5
                mapaAsBuild.addElement(rs.getString(7)); // cliente 6
                mapaAsBuild.addElement(rs.getDate(8)); // expeditaci�n 7
                mapaAsBuild.addElement(rs.getString(9)); //descripcion 8
                mapaAsBuild.addElement(rs.getString(10)); //dise�o 9
                mapaAsBuild.addElement(rs.getString(11)); // serie 10
                mapaAsBuild.addElement(new Float(rs.getFloat(12))); // Cantidad entregada de bodega 11
                mapaAsBuild.addElement(rs.getString(13)); // Reproceso 12
                equiposConMateriales.addElement(mapaAsBuild);
            }
            
            rs.close();
            this.desconectarEnschaffner();
            
            
        }catch(Exception e){
            this.desconectarEnschaffner();
            new Loger().logger("CrpBean.class " , "ERROR: mapaAsBuildConEstructura (): "+e.getMessage(),0);
        }
        return equiposConMateriales;
    }
//----------------------------------------------------------------------------------------------------------------------------
    public Vector retornarMapaAsBuildConMateriales(){
        return equiposConMateriales;
    }
    
//----------------------------------------------------------------------------------------------------------------------------
    public Vector getFamilia(Vector proceso, String process){
        Vector cantidadDiaria = new Vector();
        int contador = 0;
        Date fecha = new Date(0);
        long fechaIni = 0;
        
        
        Date fechaMayor = ((Date)((Vector)procesos.elementAt(0)).elementAt(4));
        for(int i = 0;i<procesos.size();i++){
            if((((Integer)((Vector)procesos.elementAt(i)).elementAt(12))).intValue() != 1){
                if(((Date)((Vector)procesos.elementAt(i)).elementAt(4)).compareTo(fechaMayor) > 0 && ((String)((Vector)procesos.elementAt(i)).elementAt(0)).equals(process)  ){
                    fechaMayor = ((Date)((Vector)procesos.elementAt(i)).elementAt(4));
                }
            }
        }
        Date fechaMenor = new Date(0).valueOf(fechaN);
        long fechaInicio = fechaMenor.getTime()/86400000;
        long fechaFin = fechaMayor.getTime()/86400000;
        
        for(int f = (int)fechaInicio;f <= (int)fechaFin;f++){
            for(int i=0;i<procesos.size();i++){
                //procesos = new Vector();
                if( (((Integer)((Vector)procesos.elementAt(i)).elementAt(12))).intValue() != 1 && ((String)((Vector)procesos.elementAt(i)).elementAt(0)).equals(process)){
                    if(((Date)((Vector)procesos.elementAt(i)).elementAt(4)).getTime() / 86400000 <= f  && ((Date)((Vector)procesos.elementAt(i)).elementAt(5)).getTime() / 86400000 >= f){
                        
                        fechaIni = (((long)(f+1)) * 86400000);
                        contador++;
                        
                    }
                }
            }
            if(contador != 0){
                Date fec = new Date(0);
                fec.setTime(fechaIni);
                Vector cantidadPorProceso = new Vector();
                cantidadPorProceso.addElement(process);
                cantidadPorProceso.addElement(fec);
                cantidadPorProceso.addElement(new Integer(contador));
                cantidadDiaria.addElement(cantidadPorProceso);
                contador =0;
            }
        }
        
        
        
        return cantidadDiaria;
    }
//----------------------------------------------------------------------------------------------------------------------------
    public Vector agregarEstructuraAProceso(Vector estructura , String codPro){
        
        float cantidad = 0;
        
        // una variante
        for(int t=0;t < estructura.size();t++){
            if((((Integer)((Vector)estructura.elementAt(t)).elementAt(12))).intValue() != 1){
                if(((Date)((Vector)estructura.elementAt(t)).elementAt(4)).compareTo(new Date(0).valueOf(fechaN)) >=0){
                    for(int i=0;i< ((Vector)((Vector)estructura.elementAt(t)).elementAt(13)).size();i++){
                        if (i%2==0 && ((Vector)((Vector)estructura.elementAt(t)).elementAt(13)).elementAt(i).equals(codPro)){
                            //if (i%2==0  && (((Integer)((Vector)estructura.elementAt(t)).elementAt(12))).intValue() != 1){
                            Vector estructuraPorProceso = new  Vector();
                            estructuraPorProceso.addElement(((Integer)((Vector)estructura.elementAt(t)).elementAt(9))); // np 0
                            estructuraPorProceso.addElement(((Integer)((Vector)estructura.elementAt(t)).elementAt(10))); // linea 1
                            estructuraPorProceso.addElement(((String)((Vector)estructura.elementAt(t)).elementAt(0))); // proceso 2
                            estructuraPorProceso.addElement(((Vector)((Vector)estructura.elementAt(t)).elementAt(13)).elementAt(i+1)); // cantidad de material 3
                            estructuraPorProceso.addElement(((Date)((Vector)estructura.elementAt(t)).elementAt(4))); // readTime 4
                            estructuraPorProceso.addElement(((Vector)((Vector)estructura.elementAt(t)).elementAt(13)).elementAt(i)); // codPro
                            agregarEstructuraAProceso.addElement(estructuraPorProceso);
                            //cantidad += ((Float)((Vector)((Vector)estructura.elementAt(t)).elementAt(13)).elementAt(i+1)).floatValue();
                        }
                    }
                }
            }
        }
        return agregarEstructuraAProceso;
    }
//----------------------------------------------------------------------------------------------------------------------------
    public Vector returnAgregarEstructuraAProceso(){
        return agregarEstructuraAProceso;
    }
//----------------------------------------------------------------------------------------------------------------------------
// AGREGA UN VECTOR A LA COLA DEL VECTOR PROCESOS CON LOS MATERIALES POR PROCESOS
    public Vector agregarEstructuraAequipo(String codigoMaterial , float cantidad){
        Vector vec = new Vector();
        vec.addElement(codigoMaterial);
        vec.addElement(new Float(cantidad));
        
        return vec;
    }
    
    
//----------------------------------------------------------------------------------------------------------------------------
    // Revisa todos los equipos con mapa y coloca las fechas validas a los procesos sin hacer
    public void procesosMapaAsBuild(Vector ultimoProceso, Date fecha , int verificador){
        try{
            //Vector ultimoProceso = new Vector();
            //ultimoProceso = mapaAsBuild (pedido , line);
            
            
            int orden = (((Integer)((Vector)ultimoProceso.elementAt(0)).elementAt(11)).intValue());
            for(int i=0;i < ultimoProceso.size();i++){
                
                if((((Integer)((Vector)ultimoProceso.elementAt(i)).elementAt(11)).intValue())>orden && (Date)((Vector)ultimoProceso.elementAt(i)).elementAt(7) != null){
                    orden = (((Integer)((Vector)ultimoProceso.elementAt(i)).elementAt(11)).intValue());
                }
            }
            
            Date readyTime = new Date(0);
            for(int m = 0;m<ultimoProceso.size();m++){
                
                if(verificador == 8 || verificador == 7){
                     /*if(fecha.compareTo(new Date(0).valueOf(fechaN)) < 0){
                        fecha.setTime(new Date(0).valueOf(fechaN).getTime());
                     }*/
                    if(orden == 0){
                        
                        orden = -2;
                        this.cambiarFechaAmapa(ultimoProceso,fecha,orden);
                        break;
                    }
                    
                    if(((Integer)((Vector)ultimoProceso.elementAt(m)).elementAt(11)).intValue() == orden){
                        if((Date)((Vector)ultimoProceso.elementAt(m)).elementAt(7) == null &&
                                (((String)((Vector)ultimoProceso.elementAt(m)).elementAt(0)).equals("CAB")
                                || ((String)((Vector)ultimoProceso.elementAt(m)).elementAt(0)).equals("CAJ")
                                )){
                            orden = -2;
                            
                            this.cambiarFechaAmapa(ultimoProceso,fecha,orden);
                        }
                    }
                    
                    if(orden > 0){
                        this.updateMapaSimple(ultimoProceso,fecha,1);
                    }
                    
                }else{
                    
                    if(orden == 0 && ((Date)((Vector)ultimoProceso.elementAt(m)).elementAt(4)).compareTo(new Date(0).valueOf(fechaN)) < 0){
                        readyTime.setTime(new Date(0).valueOf(fechaN).getTime());
                        orden = -1;
                        this.cambiarFechaAmapa(ultimoProceso,readyTime,orden);
                        break;
                    }
                    if(orden == 0) {
                        break;
                    }
                    if(((Integer)((Vector)ultimoProceso.elementAt(m)).elementAt(11)).intValue() == orden){
                        if((Date)((Vector)ultimoProceso.elementAt(m)).elementAt(7) == null &&
                                (((String)((Vector)ultimoProceso.elementAt(m)).elementAt(0)).equals("CAB")
                                || ((String)((Vector)ultimoProceso.elementAt(m)).elementAt(0)).equals("CAJ")
                                )){
                            readyTime.setTime(new Date(0).valueOf(fechaN).getTime());
                            orden = -1;
                            
                            this.cambiarFechaAmapa(ultimoProceso,readyTime,orden);
                        }
                    }
                    
                    if(orden > 0){
                        this.updateMapaSimple(ultimoProceso,readyTime,2);
                    }
                }
                
                
            }
            ultimoProceso.clear();
            
            
            
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: procesosMapaAsBuild(): "+e.getMessage(),0);
        }
        
    }
    
    
    //-----------------------------------------------------------------------------------------------------------------------------
    private void cambiarFechaAmapa(Vector mapa , Date fechaInicio , int orden){
        int procesoEnObservacion=-1;
        int nodoFinal=-1;
        try{
            //if( orden == -1 ){
            this.buildMapaSimple(mapa,fechaInicio);
            if( orden == -2){
                long aux =tiempoProduccion( mapa, fechaInicio);
                if(new Date( fechaInicio.getTime()-aux ).compareTo(new Date(0).valueOf(fechaN)) < 0){
                    this.buildMapaSimple(mapa,new Date( new Date(0).valueOf(fechaN).getTime()));
                }else{
                    this.buildMapaSimple(mapa,new Date( fechaInicio.getTime()-aux ));
                }
            }
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
            for(int i = 0;i < mapa.size();i++){
                ((Vector)mapa.elementAt(i)).setElementAt(new Integer(2),12);
                updateMapaAsBuild(((Integer)((Vector)mapa.elementAt(i)).elementAt(9)).intValue()
                ,((Integer)((Vector)mapa.elementAt(i)).elementAt(10)).intValue()
                ,(String)((Vector)mapa.elementAt(i)).elementAt(0)
                ,formateador.format((Date)((Vector)mapa.elementAt(i)).elementAt(4))
                ,formateador.format((Date)((Vector)mapa.elementAt(i)).elementAt(5))
                ,((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue()
                );
            }
            //}
            //this.desplegarMapa(mapa);
            
            //mapa.clear();
            
        } catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: cambiarFechaAmapa() " + e.getMessage(),0);
        }
    }
//--------------------------------------------------------------------------------------------------------------------------------
// pone fecha a los procesos que est�n terminados y no tienen fechas v�lidas
    private Vector updateMapaSimple(Vector mapa , Date fechaInicio ,  int verificador){
        int nodoFinal=-1;
        int procesoEnObservacion=-1;
        Date dateTime = new Date(0);
        dateTime = fechaInicio;
        try{
            for(int i = 0;i < mapa.size();i++){
                if((Date)((Vector)mapa.elementAt(i)).elementAt(7) == null){
                    ((Vector)mapa.elementAt(i)).setElementAt( new Date(0) ,6 );
                    ((Vector)mapa.elementAt(i)).setElementAt( new Date(0) ,7 );
                }
                
            }
            //this.desplegarMapa(mapa);
            
            nodoFinal = this.findNodoFinal(mapa);
            procesoEnObservacion = this.analizadorNodoFinal(mapa,nodoFinal);
            int nodito = 0;
            for(int i = procesoEnObservacion;i >= 0;i--){
                for(int p = procesoEnObservacion;p >= 0;p--){
                    if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(1)).intValue() == ((Integer)((Vector)mapa.elementAt(p)).elementAt(2)).intValue() ){
                        if( ((Date)((Vector)mapa.elementAt(p)).elementAt(7)).hashCode() != new Date(0).hashCode()){
                            fechaInicio = (Date)((Vector)mapa.elementAt(p)).elementAt(6);
                            nodito = ((Integer)((Vector)mapa.elementAt(p)).elementAt(1)).intValue();
                        }
                    }
                    
                    
                    
                    for(int y = procesoEnObservacion;y >= 0;y--){
                        if(((Integer)((Vector)mapa.elementAt(y)).elementAt(2)).intValue() == nodito){
                            if(((Date)((Vector)mapa.elementAt(y)).elementAt(7)).hashCode() == new Date(0).hashCode()){
                                ((Vector)mapa.elementAt(y)).setElementAt( fechaInicio ,7 );
                                ((Date)((Vector)mapa.elementAt(y)).elementAt(6)).setTime   (fechaInicio.getTime() - ((Integer)((Vector)mapa.elementAt(y)).elementAt(3)).intValue() * 86400000);
                            }
                        }
                    }
                }
            }
            
            for(int i = 0;i < mapa.size();i++){
                if(((Date)((Vector)mapa.elementAt(i)).elementAt(7)).hashCode() != new Date(0).hashCode( )){
                    ((Vector)mapa.elementAt(i)).setElementAt( (Date)((Vector)mapa.elementAt(i)).elementAt(6) ,4 );
                    ((Vector)mapa.elementAt(i)).setElementAt( (Date)((Vector)mapa.elementAt(i)).elementAt(7) ,5 );
                    ((Vector)mapa.elementAt(i)).setElementAt(new Integer(1),12);
                }
            }
            
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
            for(int i = 0;i < mapa.size() ;i++){
                
                updateMapaAsBuild(((Integer)((Vector)mapa.elementAt(i)).elementAt(9)).intValue()
                ,((Integer)((Vector)mapa.elementAt(i)).elementAt(10)).intValue()
                ,(String)((Vector)mapa.elementAt(i)).elementAt(0)
                ,formateador.format((Date)((Vector)mapa.elementAt(i)).elementAt(4))
                ,formateador.format((Date)((Vector)mapa.elementAt(i)).elementAt(5))
                ,((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue()
                );
                
            }
            // this.desplegarMapa(mapa);
            if(verificador == 1){
                this.analizaProcesoSinMarcaje(mapa,dateTime);
            }else{
                this.analizaProcesoSinMarcaje(mapa,new Date(0).valueOf(fechaN));
            }
            mapa.clear();
        }catch(Exception e){
            new Loger().logger("CrepBean.class " , "ERROR: " + "updateMapaSimple(): " + e.getMessage(),0);
            
        }
        
        //mapa.clear();
        return mapa;
        
    }
//---------------------------------------------------------------------------------------------------------------------------------
    // pone nuevas fechas a los procesos que todav�a no se hacen
    public void procesosSinMarcaje(Vector mapa , Date fechita){
        int nodoInicio = -1;
        int procesoEnObservacion = -1;
        Date fechaInicio = new Date(0);
        
        
        
        try{
            
            int nodoFinal = this.findNodoFinal(mapa);
            procesoEnObservacion = this.analizadorNodoFinal(mapa,nodoFinal);
            for(int i = 0;i < mapa.size() ;i++){
                if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                    if(((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == nodoFinal ) {
                        ((Vector)mapa.elementAt(i)).setElementAt(fechita,7);
                        ((Date)((Vector)mapa.elementAt(i)).elementAt(6)).setTime(fechita.getTime() - ((Integer)((Vector)mapa.elementAt(i)).elementAt(3)).intValue() * 86400000);
                    }
                }
            }
            
            int nodito = 0;
            for(int i = procesoEnObservacion;i >= 0;i--){
                for(int p = procesoEnObservacion;p >= 0;p--){
                    if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                        if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == ((Integer)((Vector)mapa.elementAt(p)).elementAt(1)).intValue() ){
                            if( ((Date)((Vector)mapa.elementAt(p)).elementAt(7)).hashCode() != new Date(0).hashCode()){
                                
                                fechaInicio = (Date)((Vector)mapa.elementAt(p)).elementAt(6);
                                
                                nodito = ((Integer)((Vector)mapa.elementAt(p)).elementAt(1)).intValue();
                                
                            }
                        }
                    }
                    
                    //ac� le coloca la nueva fecha
                    for(int y = procesoEnObservacion;y >= 0;y--){
                        if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                            if(((Integer)((Vector)mapa.elementAt(y)).elementAt(2)).intValue() == nodito){
                                if(((Date)((Vector)mapa.elementAt(y)).elementAt(7)).hashCode() == new Date(0).hashCode()){
                                    ((Vector)mapa.elementAt(y)).setElementAt( fechaInicio ,7 );
                                    ((Date)((Vector)mapa.elementAt(y)).elementAt(6)).setTime   (fechaInicio.getTime() - ((Integer)((Vector)mapa.elementAt(y)).elementAt(3)).intValue() * 86400000);
                                    
                                }
                            }
                        }
                    }
                }
            }
            
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: procesosSinMarcaje(): " + e.getMessage(),0);
            
        }
    }
    
//---------------------------------------------------------------------------------------------------------------------------------
    
    public Date fechaMenor(Vector mapa){
        Date fechaMenor = new Date(0).valueOf(fechaN);
        for(int i = 0;i < mapa.size() ;i++){
            if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                if(((Date)((Vector)mapa.elementAt(i)).elementAt(6)).compareTo(fechaMenor) < 0){
                    fechaMenor = (Date)((Vector)mapa.elementAt(i)).elementAt(6);
                }
            }
        }
        
        return fechaMenor;
    }
//---------------------------------------------------------------------------------------------------------------------------------
    private void analizaProcesoSinMarcaje(Vector mapa,Date fechita){
        int contador = 0;
        this.procesosSinMarcaje(mapa,fechita);
        //this.desplegarMapa(mapa);
        Date fechaMenor = this.fechaMenor(mapa);
        //System.out.println("fecha Menor: " + fechaMenor);
        long diferencia = this.tiempoProduccionProcesosSinMarcaje(fechaMenor);
        for(int i = 0;i < mapa.size() ;i++){
            if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                ((Vector)mapa.elementAt(i)).setElementAt( new Date(0) ,6 );
                ((Vector)mapa.elementAt(i)).setElementAt( new Date(0) ,7 );
            }
        }
        this.procesosSinMarcaje(mapa,new Date(fechita.getTime() + diferencia));
        for(int i = 0;i < mapa.size() ;i++){
            /*if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                //System.out.println("elemento 6: " + ((Date)((Vector)mapa.elementAt(i)).elementAt(6)) +" fechita: " + fechita);
               if(((Date)((Vector)mapa.elementAt(i)).elementAt(6)).compareTo(fechita) == 0){
                   if(((Date)((Vector)mapa.elementAt(i)).elementAt(4)).compareTo(fechita) < 0){
                      contador = 1;
              }
            }
               if(((Integer)((Vector)mapa.elementAt(i)).elementAt(1)).intValue() == 0 && ((Date)((Vector)mapa.elementAt(i)).elementAt(4)).compareTo(fechita) < 0){
                   contador = 1;
               }
           }*/
        }
        
        for(int i = 0;i < mapa.size() ;i++){
            if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                //if(contador==1){
                ((Vector)mapa.elementAt(i)).setElementAt( (Date)((Vector)mapa.elementAt(i)).elementAt(6) ,4 );
                ((Vector)mapa.elementAt(i)).setElementAt( (Date)((Vector)mapa.elementAt(i)).elementAt(7) ,5 );
                //}
            }
        }
        java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
        for(int i = 0;i < mapa.size() ;i++){
            if(((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue() == 0) {
                updateMapaAsBuild(((Integer)((Vector)mapa.elementAt(i)).elementAt(9)).intValue()
                ,((Integer)((Vector)mapa.elementAt(i)).elementAt(10)).intValue()
                ,(String)((Vector)mapa.elementAt(i)).elementAt(0)
                ,formateador.format((Date)((Vector)mapa.elementAt(i)).elementAt(4))
                ,formateador.format((Date)((Vector)mapa.elementAt(i)).elementAt(5))
                ,((Integer)((Vector)mapa.elementAt(i)).elementAt(12)).intValue()
                );
            }
        }
        
        // this.desplegarMapa(mapa);
        
        
        
    }
    
//------------------------------------------------------------------------------------------------------------------------------
    
    public long tiempoProduccionProcesosSinMarcaje(Date fecha){
        Date fechaMayor= new Date(0).valueOf(fechaN);
        Date fechaMenor= fecha;
        
        return  fechaMayor.getTime() - fechaMenor.getTime();
    }
    
//------------------------------------------------------------------------------------------------------------------------------
    private int  analizadorNodoFinal(Vector mapa , int nodoFinal ){
        
        for(int i = 0;i < mapa.size();i++){
            
            if( ((Integer)((Vector)mapa.elementAt(i)).elementAt(2)).intValue() == nodoFinal) {
                if( ((Date)((Vector)mapa.elementAt(i)).elementAt(7)).hashCode() == new Date(0).hashCode() ){
                    
                    return i;
                    
                }
            }
        }
        
        return 0;  // Nunca puede ocurrir
    }
    
    
//------------------------------------------------------------------------------------------------------------------------------
    public void updateMapaAsBuild(int np,int linea,String proceso,String readTime,String completionTime,int vigencia){
        try{
            /*conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();*/
            String query1 = "UPDATE MAC.dbo.MapaAsBuild  SET readTime = '"+ readTime +"' , completionTime = '"+ completionTime +"' , vigencia = '"+ vigencia +"' WHERE np = " + np + " and linea = " + linea + " and proceso = '"+ proceso +"'";
            st.execute(query1);
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: updateMapaSimple(): " + e.getMessage(),0);
        }
        
    }
    
    
//------------------------------------------------------------------------------------------------------------------------------
    public Vector procesosSinTerminar(String proceso){
        Vector procesosSinTerminar = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            /*vista VW_MapaAsBuild
            String query = " SELECT en.Semielaborado,ma.np,ma.linea,en.FechaInicio,tp.FechaCli,ma.completionTime"
                    +" FROM MAC.dbo.MapaAsBuild ma,ENSchaffner.dbo.TAvance en,ENSchaffner.dbo.TPMP tp"
                    +" WHERE vigencia != 1 and proceso = 'RF'"
                    +" AND ma.np = en.Pedido"
                    +" AND ma.np = tp.Pedido"
                    +" AND ma.linea = en.Linea"
                    +" AND ma.linea = tp.Linea"
                    +" AND en.Semielaborado= '" + proceso.toUpperCase() + "'"
                    +" AND en.FechaTermino is NULL";
             */
            String query = " SELECT en.Semielaborado,ma.np,ma.linea,en.FechaInicio,tp.FechaCli,ma.completionTime"
                    +" FROM MAC.dbo.VW_MapaAsBuild ma,ENSchaffner.dbo.TAvance en,ENSchaffner.dbo.TPMP tp"
                    +" WHERE proceso = 'RF'"
                    +" AND ma.np = en.Pedido"
                    +" AND ma.np = tp.Pedido"
                    +" AND ma.linea = en.Linea"
                    +" AND ma.linea = tp.Linea"
                    +" AND en.Semielaborado= '" + proceso.toUpperCase() + "'"
                    +" AND en.FechaTermino is NULL";
            rs = st.executeQuery(query);
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setProceso(rs.getString(1));
                equipo.setNp(rs.getInt(2));
                equipo.setLinea(rs.getInt(3));
                equipo.setFechaInicio(rs.getDate(4));
                equipo.setFechaCliente(rs.getDate(5));
                equipo.setFechaTermino(rs.getDate(6));
                procesosSinTerminar.addElement(equipo);
            }
        }catch(Exception e){
            System.out.println("ERROR: procesosSinTerminar()");
        }
        return procesosSinTerminar;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public void borrarEquipos(){
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query ="DELETE FROM ENSchaffner.dbo.TMovimientos WHERE Tipo in(7,8)";
            st.execute(query);
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR borrarEquipos(): " + e.getMessage(),0);
        }
    }
//------------------------------------------------------------------------------------------------------------------------------
    public void actualizarStockPlanta(){ // despues borrar
        try{
            conENS = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String query ="DELETE FROM ENSchaffner.dbo.TMovimientos WHERE Tipo in(7,8)";
            st.execute(query);
            st.close();
            conMAC.close();
        }catch(Exception e){
            System.out.println("ERROR actualizarStockPlanta(): " + e.getMessage());
        }
    }
//-------------------------------------------------------------------------------------------------------------------------------
    public void   eliminarMaterialesRepetidos(int np , int linea){
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
         /*String query4 = "SELECT distinct(d.CodigoMatPrima),d.DescriMP, d.UnimedMP"
                         +" FROM ENSchaffner.dbo.TPMP a , ENSchaffner.dbo.MEstructura d"
                         +" WHERE a.Codpro = d.CodigoTerminado"
                         +" and d.CodigoMatPrima is not null"
                         +" and a.Pedido =" + np + " and a.Linea = " + linea;
          */
            String  query4  = " SELECT DISTINCT(d.CodigoMatPrima),d.DescriMP,d.UnimedMP "
                    +" FROM ENSchaffner.dbo.MEstructura d "
                    +" LEFT OUTER JOIN ENSchaffner.dbo.TPMP a "
                    +" ON d.CodigoTerminado = a.Codpro "
                    +" LEFT OUTER JOIN MAC.dbo.MaterialSeleccionado c "
                    +" ON (d.CodigoMatPrima =  c.codigoMaterial) "
                    +" WHERE  c.codigoMaterial IS NULL "
                    +" AND a.Pedido =" + np + " and a.Linea = " + linea;
            rs = st.executeQuery(query4);
            while(rs.next()){
                
                for(int u=0;u<eliminarMaterialesRepetidos.size();u++){
                    if( ((EstructuraPorEquipo)eliminarMaterialesRepetidos.elementAt(u)).getCodigoMatPrima().equals(rs.getString(1))){
                        eliminarMaterialesRepetidos.remove(u);
                    }
                }
                EstructuraPorEquipo estructura = new EstructuraPorEquipo();
                estructura.setCodigoMatPrima(rs.getString(1));
                estructura.setDescripcion(rs.getString(2));
                estructura.setUnidadMedida(rs.getString(3));
                eliminarMaterialesRepetidos.addElement(estructura);
                
            }
            
            rs.close();
            st.close();
            conMAC.close();
            
            
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: eliminarMaterialesRepetidos (): "+e.getMessage(),0);
        }
        
    }
    public Vector retornarEliminarMaterialesRepetidos(){
        return eliminarMaterialesRepetidos;
    }
    public Vector retornarEliminarMaterialesRepetidos(Vector vacio){
        eliminarMaterialesRepetidos = vacio;
        return eliminarMaterialesRepetidos;
    }
    
//------------------------------------------------------------------------------------------------------------------------------
    public void cambiarFechaAProcesos(Vector cambiarFechas){
        
        for(int i=0;i<cambiarFechas.size();i++){
            
        }
    }
//------------------------------------------------------------------------------------------------------------------------------
    public Vector traerEquipo(int np , int linea){
        Vector traerEquipo = new Vector();
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 = " SELECT a.proceso,a.readTime,a.completionTime,a.vigencia,d.nodoInicio,d.nodoFin,a.np,a.linea,a.processTime"
                    +" FROM MAC.dbo.VW_MapaAsBuild a"
                    +" LEFT OUTER JOIN  MAC.dbo.SecuenciaPorFamilia d"
                    +" ON a.familia = d.familia"
                    +" AND a.proceso = d.proceso"
                    +" WHERE  a.np = " + np + " AND a.linea = " + linea
                    //+" AND a.vigencia != 1"
                    +" ORDER BY a.np ,a.linea,nodoInicio,nodoFin";
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setProceso(rs.getString(1));
                equipo.setFechaInicio(rs.getDate(2));
                equipo.setFechaTermino(rs.getDate(3));
                equipo.setIdentificador(rs.getInt(4));
                equipo.setNodoInicio(rs.getInt(5));
                equipo.setNodoFin(rs.getInt(6));
                equipo.setNp(rs.getInt(7));
                equipo.setLinea(rs.getInt(8));
                equipo.setProcessTime(rs.getInt(9));
                traerEquipo.addElement(equipo);
            }
            rs.close();
            st.close();
            conMAC.close();
            
        }catch(Exception e){
            
        }
        return traerEquipo;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public void recuperarEquipo(Vector recuperarEquipos){
        this.recuperarEquipos = recuperarEquipos;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public Vector recuperarEquipos(){
        return recuperarEquipos;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public String updateTeam(int np , int linea ,Date fechaInicio , String date, int nodo , int identificador ,String  proceso){
        String estado = "";
        String estado1= "";
        long resultado = 0;
        Date fechaTermino = new Date(0);
        Date fechaIni = new Date(0);
        long diferencia = 0;
        int nodo1 = -1;
        String nodito = "";
        try{
            
            
            Vector equipos = this.traerEquipo(np,linea);
            for(int i=0;i<equipos.size();i++){
                if(identificador == 1){
                    if(((Equipo)equipos.elementAt(i)).getNodoFin() == nodo){
                        if(date.equals("")){
                            estado = "la fecha es nula";
                            estado1 = "no";
                        }else{
                            if(((Equipo)equipos.elementAt(i)).getFechaTermino().compareTo(new Date(0).valueOf(date)) > 0){
                                //  fechaInicio.getTime()+ ((Integer)((Vector)mapa.elementAt(i)).elementAt(3)).intValue() * 86400000
                                estado  = "no se puede grabar porque la fecha es menor a la anterior";
                                estado1 = "no";
                                
                            }else{
                                estado1 = "si";
                                diferencia = this.diferenciaFecha(new Date(0).valueOf(date) , fechaInicio);
                                
                            }
                        }
                    }
                    
                    if(nodo == 0 && ((Equipo)equipos.elementAt(i)).getProceso().equals(proceso)){
                        if(date.equals("")){
                            estado = "la fecha es nula";
                            estado1 = "no";
                        }else{
                            estado1 = "si";
                            diferencia = this.diferenciaFecha(new Date(0).valueOf(date) , fechaInicio);
                        }
                    }
                }
                
                if(identificador == 2){
                    if(((Equipo)equipos.elementAt(i)).getNodoFin() == nodo){
                        nodo1 = ((Equipo)equipos.elementAt(i)).getNodoInicio();
                        nodo = nodo1;
                        nodito = "1";
                    }
                    if(nodito.equals("1")){
                        for(int r=0;r<equipos.size();r++){
                            if(((Equipo)equipos.elementAt(i)).getNodoFin() == nodo1){
                                if(date.equals("")){
                                    estado = "la fecha es nula";
                                    estado1 = "no";
                                }else{
                                    if(((Equipo)equipos.elementAt(i)).getFechaTermino().compareTo(new Date(0).valueOf(date)) > 0){
                                        //  fechaInicio.getTime()+ ((Integer)((Vector)mapa.elementAt(i)).elementAt(3)).intValue() * 86400000
                                        estado  = "no se puede grabar porque la fecha es menor a la anterior";
                                        estado1 = "no";
                                        
                                    }else{
                                        estado1 = "si";
                                        diferencia = this.diferenciaFecha(new Date(0).valueOf(date) , fechaInicio);
                                        
                                    }
                                }
                            }
                            if(nodo1 == 0 && ((Equipo)equipos.elementAt(r)).getProceso().equals(proceso)){
                                if(date.equals("")){
                                    estado = "la fecha es nula";
                                    estado1 = "no";
                                }else{
                                    estado1 = "si";
                                    diferencia = this.diferenciaFecha(new Date(0).valueOf(date) , fechaInicio);
                                }
                            }
                            
                        }
                        nodito = "";
                    }
                }
                
                
                
                
                if(estado1.equals("si")){
                    for(int y=0;y<equipos.size();y++){
                        if(((Equipo)equipos.elementAt(y)).getNodoInicio() >= nodo){
                            
                            fechaTermino.setTime(((Equipo)equipos.elementAt(y)).getFechaTermino().getTime() + diferencia);
                            fechaIni.setTime(((Equipo)equipos.elementAt(y)).getFechaInicio().getTime() + diferencia);
//estado = String.valueOf(date).concat(String.valueOf(dde));
                            //estado = String.valueOf(resultado);
                            int est =  this.grabarUpdateTeam(((Equipo)equipos.elementAt(y)).getNp(),((Equipo)equipos.elementAt(y)).getLinea(),((Equipo)equipos.elementAt(y)).getProceso(),fechaIni,fechaTermino);
                            if(est == 1){
                                estado = "actualiz� la base de datos";
                            }else{
                                estado = "se fue a la cresta";
                            }
                            estado1 = "no";
                        }
                    }
                }
            }
            
            
        }catch(Exception e){
            estado = "hubo un error al grabar";
        }
        return estado;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public int grabarUpdateTeam(int np,int linea,String proceso,Date fechaInicio , Date fechaTermino){
        int estado = 0;
        java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyyMMdd");
        try{
            conMAC = myConexion.getMACConnector();
            st = conMAC.createStatement();
            String  query1 =  "UPDATE MAC.dbo.MapaAsBuild SET readTime = '" + formateador.format(fechaInicio) + "' , completionTime = '" + formateador.format(fechaTermino) + "' WHERE  proceso = '" + proceso +"' AND np = " + np + " AND linea = " + linea;
            st.execute(query1);
            estado=1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            estado = 0;
            new Loger().logger("MarpBean.class","grabarUpdateTeam() "  + e.getMessage() + " " + new Date(0).valueOf(fechaN),0);
        }
        return estado;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public long diferenciaFecha(Date fechaInicio , Date fechaTermino){
        long diferencia = 0;
        diferencia = fechaInicio.getTime() - fechaTermino.getTime();
        return diferencia;
    }
//--------------------------------------------------------------------------------------------------------------------------------
    public Vector retornarEquiposConProblemas(String material , String fecha){
        Vector equiposConMaterial = new Vector();
        
       
        String query4 = " SELECT a.readTime , a.np , a.linea , me.CantidadMP, me.CodigoMatPrima , me.CodigoSemiElaborado ,le.LeadTime,tp.Expeditacion,tp.Descri,tc.PRO_INVMIN,tc.PRO_INVMAX,tp.Serie "
                +" FROM MAC.dbo.VW_MapaAsBuild a  "
                +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura me "
                +" ON a.codProducto = me.CodigoTerminado "
                +" AND a.proceso = CodigoSemiElaborado "
                +" LEFT OUTER JOIN  MAC.dbo.ProductoAnexo le "
                +" ON me.CodigoMatPrima = le.Codigo "
                +" LEFT OUTER JOIN  ENSchaffner.dbo.TPMP tp "
                +" ON a.np = tp.Pedido AND a.linea = tp.Linea "
                +" LEFT OUTER JOIN  SiaSchaffner.dbo.STO_PRODUCTO tc "
                +" ON (me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD) "
                +" WHERE  me.CodigoMatPrima in('" + material + "') "
                +" AND tp.Status <> 'Bodega'  AND tp.Familia <> 'REP' "
                +" AND tp.Status IS NOT NULL AND tp.Activa != 1  "
                +" AND tp.Status <> 'Reproceso' "
                +" ORDER BY tp.Expeditacion,a.readTime,a.np,a.linea";
        
       
        String query = " SELECT a.readTime,tp.Pedido,tp.Linea,me.CantidadMP, me.CodigoMatPrima,me.CodigoSemiElaborado,le.LeadTime,tp.Expeditacion,tp.Descri,tc.PRO_INVMIN,tc.PRO_INVMAX,tp.Serie "
                +" FROM MAC.dbo.VW_MapaAsBuild a "
                +" LEFT OUTER JOIN ENSchaffner.dbo.MEstructura me "
                +" ON a.codProducto = me.CodigoTerminado "
                +" LEFT OUTER JOIN ENSchaffner.dbo.TPMP tp "
                +" ON  me.CodigoTerminado = tp.Codpro AND a.np = tp.Pedido and a.linea = tp.Linea "
                +" LEFT OUTER JOIN  MAC.dbo.ProductoAnexo le "
                +" ON me.CodigoMatPrima = le.Codigo "
                +" LEFT OUTER JOIN  SiaSchaffner.dbo.STO_PRODUCTO tc "
                +" ON (me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS = tc.PRO_CODPROD ) "
                +" where a.proceso = 'MON' "
                +" and me.CodigoTerminado like 'B%' "
                +" and me.CodigoMatPrima in('" + material + "')  "
                +" AND tp.Status <> 'Bodega'  AND tp.Familia <> 'REP' "
                +" AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                +" AND tp.Status <> 'Reproceso' "
                +" order by tp.Expeditacion,a.readTime,a.np,a.linea";
        
        try{
            conMAC = myConexion.getENSConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setFechaInicio(rs.getDate(1)); // readTime 0
                equipo.setNp(rs.getInt(2)); // np 1
                equipo.setLinea(rs.getInt(3)); // linea 2
                equipo.setCantidad(rs.getFloat(4)); // cantidad de material 3
                equipo.setCodPro(rs.getString(5)); // material 4
                equipo.setProceso(rs.getString(6));
                equipo.setLeadTime(rs.getInt(7));
                equipo.setExpeditacion(rs.getDate(8));
                equipo.setDescripcion(rs.getString(9));
                equipo.setStockMinimo(rs.getFloat(10));
                equipo.setStockMaximo(rs.getFloat(11));
                equipo.setSerie(rs.getString(12));
                equiposConMaterial.addElement(equipo);
            }
            //System.out.println("equiposConMateriales: "  + equiposConMateriales);
            rs.close();
            st.close();
            conMAC.close();
            
            conMAC = myConexion.getENSConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query);
            
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setFechaInicio(rs.getDate(1)); // readTime 0
                equipo.setNp(rs.getInt(2)); // np 1
                equipo.setLinea(rs.getInt(3)); // linea 2
                equipo.setCantidad(rs.getFloat(4)); // cantidad de material 3
                equipo.setCodPro(rs.getString(5)); // material 4
                equipo.setProceso(rs.getString(6));
                equipo.setLeadTime(rs.getInt(7));
                equipo.setExpeditacion(rs.getDate(8));
                equipo.setDescripcion(rs.getString(9));
                equipo.setStockMinimo(rs.getFloat(10));
                equipo.setStockMaximo(rs.getFloat(11));
                equipo.setSerie(rs.getString(12));
                equiposConMaterial.addElement(equipo);
            }
            //System.out.println("equiposConMateriales: "  + equiposConMateriales);
            rs.close();
            st.close();
            conMAC.close();
            
            
        }catch(Exception e){
            new Loger().logger("MrpBean.class " , " ERROR: retornarEquiposConProblemas(): "+e.getMessage(),0);
        }
        return equiposConMaterial;
    }
//----------------------------------------------------------------------------------------------------------------------------
    
     /* public Vector mapaAsBuildConEstructura (String proceso,String np,String linea,String fecha,String fecha1){
        String query4 = "";
        String query2 = "";
        String nada = "nada";
        String line = "aaaa";
        String pedi = "0";
        if(linea != null){
        line = linea;
        }
      
         Vector mapaAsBuildConEstructura = new Vector();
         if(!pedi.equals(np) && nada.equals(linea) && fecha.length() == 0 && fecha1.length() == 0){
                query4 = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP"
                        +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me"
                        +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "'"
                        +" AND a.proceso = CodigoSemiElaborado"
                        +" AND a.proceso = '" + proceso + "' AND a.np = '" + np + "'"
                        +" ORDER BY a.readTime";
      
                query2 =  " SELECT  me.CodigoMatPrima,ma.DESCRIPCION,sum(me.CantidadMP), me.UnimedMP  "
                         +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me,MAC.dbo.MArticulo ma "
                         +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                         +" AND a.proceso = CodigoSemiElaborado "
                         +" AND me.CodigoMatPrima = ma.CODIGO "
                         +" AND a.np = '" + np + "' AND a.proceso = '" + proceso + "' "
                         +" GROUP by me.CodigoMatPrima,ma.DESCRIPCION, me.UnimedMP ";
      
         }else if(!pedi.equals(np) && line.length() < 4 && fecha.length() == 0 && fecha1.length() == 0){
             query4 = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP"
                        +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me"
                        +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "'"
                        +" AND a.proceso = CodigoSemiElaborado"
                        +" AND a.proceso = '" + proceso + "' AND a.np = '" + np + "' AND a.linea ='" + linea + "' "
                        +" ORDER BY a.readTime";
      
             query2 =     " SELECT  me.CodigoMatPrima,ma.DESCRIPCION,sum(me.CantidadMP), me.UnimedMP  "
                         +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me,MAC.dbo.MArticulo ma "
                         +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                         +" AND a.proceso = CodigoSemiElaborado "
                         +" AND me.CodigoMatPrima = ma.CODIGO "
                         +" AND a.proceso = '" + proceso + "' AND a.np = '" + np + "' AND a.linea ='" + linea + "' "
                         +" GROUP by me.CodigoMatPrima,ma.DESCRIPCION, me.UnimedMP ";
      
         }else if(fecha.length() > 0){
             query4 = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP"
                        +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me"
                        +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "'"
                        +" AND a.proceso = CodigoSemiElaborado"
                        +" AND a.proceso = '" + proceso + "' and readTime between '" + fecha + "' and '" + fecha1 + "' "
                        +" ORDER BY a.readTime";
      
             query2 =     " SELECT  me.CodigoMatPrima,ma.DESCRIPCION,sum(me.CantidadMP), me.UnimedMP  "
                         +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me,MAC.dbo.MArticulo ma "
                         +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                         +" AND a.proceso = CodigoSemiElaborado "
                         +" AND me.CodigoMatPrima = ma.CODIGO "
                         +" AND a.proceso = '" + proceso + "' and readTime between '" + fecha + "' and '" + fecha1 + "' "
                         +" GROUP by me.CodigoMatPrima,ma.DESCRIPCION, me.UnimedMP ";
      
         }else{
      
                query4 = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP"
                        +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me"
                        +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "'"
                        +" AND a.proceso = CodigoSemiElaborado"
                        +" AND a.proceso = '" + proceso + "'"
                        +" ORDER BY a.readTime";
      
      
                query2 =  " SELECT  me.CodigoMatPrima,ma.DESCRIPCION,sum(me.CantidadMP), me.UnimedMP  "
                         +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me,MAC.dbo.MArticulo ma "
                         +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "' "
                         +" AND a.proceso = CodigoSemiElaborado "
                         +" AND me.CodigoMatPrima = ma.CODIGO "
                         +" AND a.proceso = '" + proceso + "' "
                         +" GROUP by me.CodigoMatPrima,ma.DESCRIPCION, me.UnimedMP ";
         }
      
     try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         rs = st.executeQuery(query4);
         while(rs.next()){
         Vector mapaAsBuild = new Vector();
         mapaAsBuild.addElement(rs.getDate(1)); // readTime 0
         mapaAsBuild.addElement(new Integer(rs.getInt(2))); // np 1
         mapaAsBuild.addElement(new Integer(rs.getInt(3))); // linea 2
         mapaAsBuild.addElement(rs.getString(4)); // proceso 3
         mapaAsBuild.addElement(new Float(rs.getFloat(5))); // cantidad de material 4
         mapaAsBuild.addElement(rs.getString(6)); // unidad medida 5
         mapaAsBuild.addElement(rs.getString(7)); // material 6
         mapaAsBuild.addElement(rs.getString(8)); // descripci�n 7
         mapaAsBuildConEstructura.addElement(mapaAsBuild);
         }
         rs.close();
         rs = st.executeQuery(query2);
         while(rs.next()){
         Vector detalles = new Vector();
         detalles.addElement(rs.getString(1)); // readTime 0
         detalles.addElement(rs.getString(2)); // np 1
         detalles.addElement(new Float(rs.getFloat(3))); // linea 2
         detalle.addElement(detalles);
         }
         rs.close();
         st.close();
         conMAC.close();
     }catch(Exception e){
         new Loger().logger("CrpBean.class " , "ERROR: mapaAsBuildConEstructura (): "+e.getMessage(),0);
     }
        return mapaAsBuildConEstructura;
  }
      
   public Vector retornarVectorDetalles(){
       return detalle;
   }*/
//----------------------------------------------------------------------------------------------------------------------------
    
    public Vector mapaAsBuildConEstructura(String proceso,String np,String linea){
        String query4 = "";
        String query2 = "";
        String nada = "nada";
        String line = "aaaa";
        String pedi = "0";
        if(linea != null){
            line = linea;
        }
        
        Vector mapaAsBuildConEstructura = new Vector();
        query4 = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.UnimedMP, me.CodigoMatPrima,me.DescriMP"
                +" FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me"
                +" WHERE a.codProducto = me.CodigoTerminado"
                +" AND a.proceso = CodigoSemiElaborado"
                +" AND a.proceso = '" + proceso + "' AND a.np = '" + np + "' AND a.linea ='" + linea + "' "
                +" ORDER BY a.readTime";
        
        
        //new Loger().logger("CrpBean.class " , query4,1);
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            //System.out.println(query4);
            while(rs.next()){
                Vector mapaAsBuild = new Vector();
                mapaAsBuild.addElement(rs.getDate(1)); // readTime 0
                mapaAsBuild.addElement(new Integer(rs.getInt(2))); // np 1
                mapaAsBuild.addElement(new Integer(rs.getInt(3))); // linea 2
                mapaAsBuild.addElement(rs.getString(4)); // proceso 3
                mapaAsBuild.addElement(new Float(rs.getFloat(5))); // cantidad de material 4
                mapaAsBuild.addElement(rs.getString(6)); // unidad medida 5
                mapaAsBuild.addElement(rs.getString(7)); // material 6
                mapaAsBuild.addElement(rs.getString(8)); // descripci�n 7
                mapaAsBuildConEstructura.addElement(mapaAsBuild);
            }
            rs.close();
            rs = st.executeQuery(query2);
            while(rs.next()){
                Vector detalles = new Vector();
                detalles.addElement(rs.getString(1)); // readTime 0
                detalles.addElement(rs.getString(2)); // np 1
                detalles.addElement(new Float(rs.getFloat(3))); // linea 2
                detalle.addElement(detalles);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " ERROR: mapaAsBuildConEstructura (): "+e.getMessage(),0);
        }
        return mapaAsBuildConEstructura;
    }
    
    public Vector retornarVectorDetalles(){
        return detalle;
    }
//----------------------------------------------------------------------------------------------------------------------------
    
    public int contarEquipos(String proceso){
        int valor = 0;
        Vector mapaAsBuildConEstructura = new Vector();
        String query4 =  " SELECT count(np) FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                +" WHERE a.codProducto = me.CodigoTerminado"
                +" AND a.proceso = CodigoSemiElaborado "
                +" AND a.proceso = '" + proceso + "'";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            while(rs.next()){
                valor = rs.getInt(1);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: contarEquipos(): "+e.getMessage(),0);
        }
        return valor;
    }
//------------------------------------------------------------------------------------------------------------------------------
    // para vale de consumo
      /*   public Vector equiposPorProcesos (String proceso){
       
         Vector equiposPorProcesos = new Vector();
         String query4 = " SELECT distinct(a.np)"
                        +" FROM MAC.dbo.MapaAsBuild a ,ENSchaffner.dbo.MEstructura me"
                        +" WHERE a.vigencia != 1 AND a.codProducto = me.CodigoTerminado AND a.readTime >= '" + fechaM + "'"
                        +" AND a.proceso = CodigoSemiElaborado"
                        +" AND a.proceso = '" + proceso + "'";
     try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         rs = st.executeQuery(query4);
         while(rs.next()){
         equiposPorProcesos.addElement(new Integer(rs.getInt(1))); // np
         }
         rs.close();
         st.close();
         conMAC.close();
     }catch(Exception e){
         new Loger().logger("CrpBean.class " , "ERROR: equiposPorProcesos (): "+e.getMessage(),0);
     }
        return equiposPorProcesos;
  } */
    
    public Vector equiposPorProcesos(String proceso,String equipo1,byte value){
        long dia = new java.util.Date().getTime() + (5 * 86400000);
        java.util.Date nuevaFecha = new java.util.Date();
        nuevaFecha.setTime(dia);
        String fechaNu = formateador.format(nuevaFecha);
        String query4 = "";
        Vector equiposPorProcesos = new Vector();
        if(value == 1){
            
            query4 = " SELECT a.readTime , a.np , a.linea , a.proceso ,tp.Serie,a.familia,   "
                    +"   (SELECT COUNT(te.numero) from MAC.dbo.totalEquiposPorPedido te "
                    +"   where  a.np = te.np AND a.linea = te.linea AND te.status < 5  and te.area='" + proceso + "' "
                    +"   ) as cant,(SELECT max(te.grupo) from MAC.dbo.totalEquiposPorPedido te "
                    +"   where  a.np = te.np AND a.linea = te.linea AND te.status < 5 "
                    +"    and te.area='" + proceso + "' "
                    +"   )  as grupo,(SELECT max(te.numero) from MAC.dbo.totalEquiposPorPedido te "
                    +"   where  a.np = te.np AND a.linea = te.linea AND te.status < 5 "
                    +"    and te.area='" + proceso + "'   "
                    +"   )  as numPedido "
                    +"   FROM MAC.dbo.VW_MapaAsBuild a "
                    +"   LEFT OUTER JOIN ENSchaffner.dbo.TPMP tp "
                    +"   ON (a.np = tp.Pedido AND a.linea = tp.Linea ) "
                    +"   WHERE a.proceso = '" + proceso + "' "
                    +"   ORDER BY a.readTime ";
            
            
        } else if(value == 2){
            
            query4 = " SELECT a.readTime , a.np , a.linea , a.proceso ,tp.Serie,a.familia, "
                    +"   (SELECT COUNT(te.numero) from MAC.dbo.totalEquiposPorPedido te "
                    +"      where  a.np = te.np AND a.linea = te.linea AND te.status < 5 "
                    +"    and te.area='" + proceso + "' "
                    +"    ) as cant,(SELECT max(te.grupo) from MAC.dbo.totalEquiposPorPedido te "
                    +"    where  a.np = te.np AND a.linea = te.linea AND te.status < 5 "
                    +"    and te.area='" + proceso + "' "
                    +"    )  as grupo,(SELECT max(te.numero) from MAC.dbo.totalEquiposPorPedido te "
                    +"    where  a.np = te.np AND a.linea = te.linea AND te.status < 5 "
                    +"   and te.area='" + proceso + "'  "
                    +"    )  as numPedido  "
                    +"    FROM MAC.dbo.VW_MapaAsBuild a "
                    +"     LEFT OUTER JOIN ENSchaffner.dbo.TPMP tp "
                    +"     ON (a.np = tp.Pedido AND a.linea = tp.Linea ) "
                    +"     WHERE a.np = '" + equipo1 + "' and a.proceso = '" + proceso + "' "
                    +"     ORDER BY a.readTime ";
        } else if(value == 3){
            
            query4 = " select fecha,np,linea,area,0,'general',numero,grupo,numero from MAC.dbo.totalEquiposPorPedido where area = '" + proceso + "' and status < 5 order by numero";
            
        }else{}
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setFechaInicio(rs.getDate(1));
                equipo.setNp(rs.getInt(2));
                equipo.setLinea(rs.getInt(3));
                equipo.setProceso(rs.getString(4));
                equipo.setSerie(rs.getString(5));
                equipo.setFamilia(rs.getString(6));
                equipo.setGrupo(rs.getInt(7));
                equipo.setEstado(rs.getInt(8));
                equipo.setNumPedido(rs.getLong(9));
                equiposPorProcesos.addElement(equipo);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " equiposPorProcesos (): "+e.getMessage(),0);
        }
        return equiposPorProcesos;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public Vector lineaPorEquipo(String proceso , int np){
        
        Vector lineasPorProcesos = new Vector();
        String query4 =  " SELECT distinct(linea) FROM MAC.dbo.VW_MapaAsBuild a ,ENSchaffner.dbo.MEstructura me "
                + " WHERE a.codProducto = me.CodigoTerminado AND a.proceso = CodigoSemiElaborado "
                + " AND a.proceso = '" + proceso + "' and np = " + np + "";
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            rs = st.executeQuery(query4);
            while(rs.next()){
                lineasPorProcesos.addElement(new Integer(rs.getInt(1))); // linea
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: lineaPorEquipo (): "+e.getMessage(),0);
        }
        return lineasPorProcesos;
    }
//------------------------------------------------------------------------------------------------------------------------------
    public Vector equiposPorPedido(int np,int linea,String proceso,String numero){
        
        Vector equiposPorPedido = new Vector();
        String query4 = "";
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            //String query4 =  " SELECT np,linea,numero,grupo,status,descriEstado FROM MAC.dbo.totalEquiposPorPedido WHERE  np = " + np + " AND linea = " + linea + " AND status < 4";
         /*String query4 = " SELECT distinct(te.np),te.linea,te.numero,te.grupo,te.status,te.descriEstado,dp.prioridad FROM MAC.dbo.totalEquiposPorPedido te ,MAC.dbo.detallePedido dp "
                        +" WHERE  te.np = " + np + " AND te.linea = " + linea + " AND te.status < 5 and te.numero = dp.numero and dp.area = '" + proceso + "' ";
          */
            if(numero.equals("SI")){
                query4 = " SELECT distinct(np),linea,numero,grupo,status,descriEstado,prioridad FROM MAC.dbo.totalEquiposPorPedido "
                        +" WHERE  np =  " + np + "  AND linea =  " + linea + "  AND status < 5 and area = '" + proceso + "' ";
            }else{
                query4 = " SELECT distinct(np),linea,numero,grupo,status,descriEstado,prioridad FROM MAC.dbo.totalEquiposPorPedido "
                        +" WHERE  np =  " + np + "  AND linea =  " + linea + "  AND status < 5 and area = '" + proceso + "' and numero = '" + numero + "'";
            }
            
            rs = st.executeQuery(query4);
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(rs.getInt(1));
                equipo.setLinea(rs.getInt(2));
                equipo.setNumPedido(rs.getLong(3));
                equipo.setGrupo(rs.getInt(4));
                equipo.setEstado(rs.getInt(5));
                equipo.setDescripcion(rs.getString(6));
                equipo.setStatus(rs.getString(7));
                equiposPorPedido.addElement(equipo);
            }
            rs.close();
            this.desconectarEnschaffner();
        }catch(Exception e){
            this.desconectarEnschaffner();
            new Loger().logger("CrpBean.class " , " equiposPorPedido(): "+e.getMessage(),0);
        }
        return equiposPorPedido;
    }
    
    
    
//------------------------------------------------------------------------------------------------------------------------------
    public Vector equiposPorItem(long numero){
        
        Vector equiposPorNumero = new Vector();
        
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            //String query4 =  " SELECT np,linea FROM MAC.dbo.totalEquiposPorPedido WHERE  numero = " + numero + " AND status < 5 ";
         /*String query4 = " SELECT np,linea,Serie,Familia FROM MAC.dbo.totalEquiposPorPedido,ENSchaffner.dbo.TPMP "
                        +" WHERE  numero = " + numero + " AND status < 5 and Pedido = np and Linea = linea ";*/
            String query4 = " SELECT np,linea, "
                    +" isnull((select Serie from ENSchaffner.dbo.TPMP where  Pedido = np and Linea = linea ),0) as Serie, "
                    +" isnull((select isnull(Familia,'general') from ENSchaffner.dbo.TPMP where  Pedido = np and Linea = linea ),'general') as Familia "
                    +" FROM MAC.dbo.totalEquiposPorPedido "
                    +" WHERE  numero = " + numero + "   ";
            rs = st.executeQuery(query4);
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(rs.getInt(1));
                equipo.setLinea(rs.getInt(2));
                equipo.setSerie(rs.getString(3));
                equipo.setFamilia(rs.getString(4));
                equiposPorNumero.addElement(equipo);
            }
            rs.close();
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " equiposPorNumero(): "+e.getMessage(),0);
        }
        return equiposPorNumero;
    }
//-----------------------------------------------------------------------------------------------------------------------------
    public byte totalEquiposPorPedido(int np,int linea,long numero,byte grupo,byte estado,String descriEstado){
        int verifi = 0;
        byte veri = 0;
        try{
            conMAC = mConexion.getMACConnector();
            st = conMAC.createStatement();
            String query4 =  "INSERT INTO MAC.dbo.totalEquiposPorPedido (np,linea,numero,grupo,status,descriEstado) values ("+ np +","+ linea +","+ numero +","+ grupo +","+ estado +",'" + descriEstado + "')";
            st.execute(query4);
            veri = 1;
            st.close();
            conMAC.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: totalEquiposPorPedido(): "+e.getMessage(),0);
        }
        return veri;
    }
    
//-------------------------------------------------------------------------------------------------------------------------------
    public Vector recalcularEstadoMaterialParaActualizar(){
        
        Vector equiposParaActualizar = new Vector();
        int identificador = 0;
        try{
            
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            //String query3 = "SELECT Numero,Linea,Tipo FROM ENSchaffner.dbo.TMovimientos WHERE Tipo in(7,8) ";
            String query3 = " SELECT DISTINCT(ma.np),tm.Numero,tm.Linea "
                    +" FROM ENSchaffner.dbo.TMovimientos tm "
                    +" LEFT OUTER JOIN MAC.dbo.VW_MapaAsBuild ma "
                    +" ON tm.Numero = ma.np and tm.Linea = ma.linea "
                    +" LEFT OUTER JOIN ENSchaffner.dbo.TPMP tp "
                    +" ON tm.Numero = tp.Pedido AND tm.Linea = tp.Linea "
                    +" WHERE tm.Tipo IN (7,8)";
            rs = st.executeQuery(query3);
            
            while(rs.next()){
                if(rs.getInt(1) == 0){
                    identificador = 7;
                }else{
                    identificador = 8;
                }
                Equipo equipo = new Equipo();
                equipo.setNp(rs.getInt(2));
                equipo.setLinea(rs.getInt(3));
                equipo.setIdentificador(identificador);
                equiposParaActualizar.addElement(equipo);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , "ERROR: recalcularEstadoMaterialParaActualizar(): "+e.getMessage(),0);
        }
        return equiposParaActualizar;
    }
//--------------------------------------------------------------------------------------------------------------------------------------
    public boolean aprobarPedidos(String pedido,String prioridad,String usuario){
        boolean verificar = false;
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            conENS.setAutoCommit(false);
         /*String query = "update MAC.dbo.detallePedido set prioridad = '" + prioridad + "',status = 2, descripcionStatus = 'Aprobado', usuAprobacion = '" + usuario + "' "
                       +" where numero = '" + pedido + "' ";*/
            //st.execute(query);
            String query1 = " update MAC.dbo.totalEquiposPorPedido set status = 2,descriEstado = 'Aprobado',usuAprobacion = '" + usuario + "' where numero = '" + pedido + "' ";
            st.execute(query1);
            String query = "     update SiaSchaffner.dbo.STO_MOVENC  "
                    +"  set  MVE_Aprobacion = 'S',MVE_EstadoImpresion = 'S' "
                    +"  where MVE_CodSistema=8 and "
                    +"  MVE_CodClase =1 AND "
                    +"  MVE_TipoDoc = 8 and "
                    +"  MVE_FolioFisico =  " + pedido + " ";
            st.execute(query);
            conENS.commit();
            conENS.setAutoCommit(true);
            verificar = true;
            this.desconectarEnschaffner();
        }catch(Exception e){
            this.desconectarEnschaffner();
            new Loger().logger("CrpBean.class " , " aprobarPedidos(): "+e.getMessage(),0);
        }
        return verificar;
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public Vector analisisMaterialesPorPedido(String familia,String avance,String fechaIni,String fechaFin){
        Vector aux = new Vector();
        try{
            
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
            String query3 = " select distinct(Pedido),Linea,Status,Cliente,Familia,tp.Codpro "
                    +" ,(select sum(CantidadMP * Costo)  from ENSchaffner.dbo.MEstructura,ENSchaffner.dbo.SCHCup "
                    +" where CodigoTerminado =  tp.Codpro and Codpro = CodigoMatPrima) as PrecioTotal "
                    +" ,(select sum(MVD_CantAsignada * Costo) "
                    +"   from SiaSchaffner.dbo.STO_MOVDET ,SiaSchaffner.dbo.STO_MOVENC,ENSchaffner.dbo.SCHCup "
                    +"        where MVD_CodSistema=8 and "
                    +"       MVD_CodClase =1 AND "
                    +"       MVD_TipoDoc = 8 and "
                    +"       MVD_NumeroDoc = MVE_NumeroDoc  "
                    +"      and MVE_CodSistema=8 and "
                    +"       MVE_CodClase =1 AND "
                    +"       MVD_CantAsignada > 0 and "
                    +"       MVD_CodProd = Codpro COLLATE SQL_Latin1_General_CP850_CS_AS  and "
                    +"       MVE_TipoDoc = 8 and MVE_FolioFisico in (select distinct(numero) from MAC.dbo.totalEquiposPorPedido where Pedido = np and Linea = linea)) as PrecioReal "
                    +" from ENSchaffner.dbo.TPMP tp "
                    +" inner join MAC.dbo.totalEquiposPorPedido "
                    +" on(Pedido = np and Linea = linea  ) ";
            if(avance.equals("nada")){
                query3  +="  where Familia = '" + familia + "' ";
            }
            if(familia.equals("nada")){
                query3  +="  where Status = '" + avance + "' ";
            }
            query3  +="  and fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "' ";
            
         /*   String query3 = " select Pedido,Linea,Status,Cliente,Familia,Codpro "
                           +" ,(select sum(PrecioCosto*CantidadMP) from ENSchaffner.dbo.MEstructura where CodigoTerminado = Codpro) as precioTotal "
                           +"  ,sum(PrecioCosto*MVD_CantAsignada) as precioReal "
                           +"  ,((select sum(PrecioCosto*CantidadMP) from ENSchaffner.dbo.MEstructura where CodigoTerminado = Codpro) - sum(PrecioCosto*MVD_CantAsignada)) as diferencia "
                           +"  from ENSchaffner.dbo.TPMP "
                           +"  inner join MAC.dbo.totalEquiposPorPedido "
                           +"  on(Pedido = np and Linea = linea  ) "
                           +"  inner join ENSchaffner.dbo.MEstructura "
                           +"  on(CodigoTerminado = Codpro and area = CodigoSemiElaborado) "
                           +"  left outer join SiaSchaffner.dbo.STO_MOVDET "
                           +"  on(MVD_CodSistema=8 and "
                           +"  MVD_CodClase =1 AND "
                           +"  MVD_TipoDoc = 8 and "
                           +"  MVD_NumeroDoc in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC  "
                           +"  where MVE_CodSistema=8 and "
                           +"  MVE_CodClase =1 AND "
                           +"  MVE_TipoDoc = 8 and MVE_FolioFisico in (select numero from MAC.dbo.totalEquiposPorPedido where Pedido = np and Linea = linea  and CodigoSemiElaborado = area) and "
                           +"  MVD_CodProd = CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS "
                           +"  and MVD_CantAsignada > 0)) ";
                           if(avance.equals("nada")){
                   query3  +="  where Familia = '" + familia + "' ";
                           }
                           if(familia.equals("nada")){
                   query3  +="  where Status = '" + avance + "' ";
                           }
                   query3  +="  and fechaEntregado between '" + fechaIni + "' and '" + fechaFin + "' "
                           +"  group by Pedido,Linea,Status,Cliente,Familia,Codpro";*/
            rs = st.executeQuery(query3);
            
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(rs.getInt(1));
                equipo.setLinea(rs.getInt(2));
                equipo.setStatus(rs.getString(3));
                equipo.setNombreCliente(rs.getString(4));
                equipo.setFamilia(rs.getString(5));
                equipo.setCodPro(rs.getString(6));
                equipo.setPrecioMateriales(rs.getFloat(7));
                equipo.setPrecioReal(rs.getFloat(8));
                equipo.setDiferencia(rs.getFloat(7)-rs.getFloat(8));
                aux.addElement(equipo);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " analisisMaterialesPorPedido(): "+e.getMessage(),0);
        }
        return aux;
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public Vector controlMaterialesPorPedido(String pedido,String linea){
        
        Vector controlMaterialesPorPedido = new Vector();
        
        try{
            
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
            String query3 = " select Familia,Cliente,Status,Codpro from ENSchaffner.dbo.TPMP where Pedido = '" + pedido + "' and Linea = '" + linea + "'";
            rs = st.executeQuery(query3);
            
            while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(Integer.parseInt(pedido));
                equipo.setFamilia(rs.getString(1));
                equipo.setNombreCliente(rs.getString(2));
                equipo.setStatus(rs.getString(3));
                equipo.setCodPro(rs.getString(4));
                controlMaterialesPorPedido.addElement(equipo);
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " controlMaterialesPorPedido(): "+e.getMessage(),0);
        }
        return controlMaterialesPorPedido;
    }
    //-------------------------------------------------------------------------------------------------------------------------------
    public boolean equipoIfExist(String pedido,String linea){
        
        boolean ifExist = false;
        
        try{
            
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
            //String query3 = " select count(Pedido) from ENSchaffner.dbo.TPMP where Pedido = '" + pedido + "' and Linea = '" + linea + "'";
            String query3 = "select count(*) from "
                    +"  SiaSchaffner.dbo.STO_MOVDET   "
                    +" where MVD_CodSistema=6 and "
                    +" MVD_CodClase =1 AND "
                    +" MVD_TipoDoc = 20 and "
                    +" MVD_NumeroDoc in (select MVE_NumeroDoc from SiaSchaffner.dbo.STO_MOVENC  "
                    +" where MVE_CodSistema=6 and "
                    +" MVE_CodClase =1 AND "
                    +" MVE_TipoDoc = 20 and MVE_FolioFisico = '" + pedido + "') and MVD_Linea = '" + linea + "' ";
            rs = st.executeQuery(query3);
            
            while(rs.next()){
                if(rs.getInt(1) > 0){
                    ifExist = true;
                }
            }
            rs.close();
            st.close();
            conENS.close();
        }catch(Exception e){
            new Loger().logger("CrpBean.class " , " equipoIfExist(): "+e.getMessage(),0);
        }
        return ifExist;
    }
    public Vector equiposProduccion(){
        Vector equipos = new Vector();
        try{
            
            conENS = myConexion.getENSConnector();
            stm = conENS.createStatement();
            
            String query = " select Pedido,Linea,isnull(Descri,'Sin Descripci�n') AS Descripcion,FechaCli,isnull(Status,'Sin estado') "
                    +" ,isnull(Expeditacion,'19000101') as Expeditacion,isnull(Cliente,'Sin nombre') as Cliente,Serie,Modelo,Diseno,Monto,KVA,StandBy "
                    +"  from ENSchaffner.dbo.TPMP "
                    +"  where Estado <> 1 and Activa = 0 ";
            rst = stm.executeQuery(query);
            while(rst.next()){
                Equipo equipo = new Equipo();
                equipo.setNp(rst.getInt(1));
                equipo.setLinea(rst.getInt(2));
                equipo.setDescripcion(rst.getString(3).trim());
                equipo.setFechaCliente(rst.getDate(4));
                equipo.setStatus(rst.getString(5).trim());
                equipo.setExpeditacion(rst.getDate(6));
                equipo.setCliente(rst.getString(7).trim());
                equipo.setSerie(rst.getString(8));
                equipo.setModelo(rst.getString(9));
                equipo.setDiseno(rst.getString(10));
                equipo.setMonto(rst.getFloat(11));
                equipo.setKva(rst.getString(12));
                equipo.setStandBy(rst.getByte(13));
                equipos.addElement(equipo);
            }
            
            rst.close();
            myConexion.cerrarConENS();
        } catch(Exception e){
            myConexion.cerrarConENS();
            new Loger().logger("CrpBean.class "," equiposProduccion()  "+e.getMessage(),0);
            
        }
        return equipos;
    }
    //--------------------------------------------------------------------------------------------------------------------
    public void desconectarEnschaffner(){
        try{
            st.close();
            conENS.close();
        }catch(Exception e){}
    }
    
    public Vector mapaAsBuildConEstructura1(String material,String fechaMenor,String fechaMayor,int compara){
        String query4 = "";
        if(compara == 0){
            /*vista VW_MapaAsBuild
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE a.vigencia != 1 AND  a.readTime >= '" + fechaM + "' and tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY a.readTime  ";
             */
            
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.VW_MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY a.readTime  ";
        }else{
            /*vista VW_MapaAsBuild
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE a.vigencia != 1 AND  tp.Expeditacion BETWEEN '" + fechaMenor + "' AND '" + fechaMayor + "' and tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY a.readTime  ";
        }
             */
            query4  = " SELECT a.readTime , a.np , a.linea , a.proceso , me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
            query4 += " FROM MAC.dbo.VW_MapaAsBuild a ";
            query4 += " inner join ENSchaffner.dbo.TPMP tp";
            query4 += " on(a.np = tp.Pedido AND a.linea = tp.Linea)";
            query4 += " inner join ENSchaffner.dbo.MEstructura me";
            query4 += " on(a.codProducto = me.CodigoTerminado AND a.proceso = me.CodigoSemiElaborado)";
            query4 += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
            query4 += " on(ep.nb_np = a.np and ep.nb_linea = a.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS and a.proceso = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
            query4 += " WHERE tp.Expeditacion BETWEEN '" + fechaMenor + "' AND '" + fechaMayor + "' and tp.Familia <> 'CELDA'";
            query4 += " AND me.CodigoMatPrima in('" + material + "') ";
            query4 += " ORDER BY a.readTime  ";
        }
        
        /*vista VW_MapaAsBuild
        String query  = " SELECT ma.readTime,tp.Pedido,tp.Linea,me.CodigoSemiElaborado,me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
        query += " FROM ENSchaffner.dbo.MEstructura me ";
        query += " inner join ENSchaffner.dbo.TPMP tp";
        query += " on(me.CodigoTerminado = tp.Codpro )";
        query += " inner join MAC.dbo.MapaAsBuild ma";
        query += " on(ma.np = tp.Pedido and ma.linea = tp.Linea)";
        query += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
        query += " on(ep.nb_np = ma.np and ep.nb_linea = ma.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS)";
        query += " WHERE  ma.proceso = 'MON' and me.CodigoTerminado like 'B%' and tp.Familia = 'CELDA'";
        query += " AND ma.readTime >= '" + fechaM + "'";
        query += " AND ma.vigencia != 1";
        query += " AND me.CodigoMatPrima in('" + material + "')";
        query += " ORDER BY ma.readTime";
         */
        
        String query  = " SELECT ma.readTime,tp.Pedido,tp.Linea,me.CodigoSemiElaborado,me.CantidadMP, me.CodigoMatPrima,tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
        query += " FROM ENSchaffner.dbo.MEstructura me ";
        query += " inner join ENSchaffner.dbo.TPMP tp";
        query += " on(me.CodigoTerminado = tp.Codpro )";
        query += " inner join MAC.dbo.VW_MapaAsBuild ma";
        query += " on(ma.np = tp.Pedido and ma.linea = tp.Linea)";
        query += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
        query += " on(ep.nb_np = ma.np and ep.nb_linea = ma.linea and ep.MVD_CodProd =  me.CodigoMatPrima COLLATE SQL_Latin1_General_CP850_CS_AS)";
        query += " WHERE  ma.proceso = 'MON' and tp.Familia = 'CELDA'";
        query += " AND me.CodigoMatPrima in('" + material + "')";
        query += " ORDER BY ma.readTime";
        
        
       
        String queryMarcaje = "select rpr_fecha,rpr_pedido,rpr_linea,Codigo_semielaborado,Cantidad_mat_prima,Codigo_mat_prima,";
        queryMarcaje += " tp.Cliente,tp.Expeditacion,tp.Descri,tp.Diseno,tp.Serie, isnull(ep.MVD_CantAsignada,0) as MVD_CantAsignada,'NO'";
        queryMarcaje += " from  ENSchaffner.dbo.EE_repro_rechazo ";
        queryMarcaje += " INNER JOIN ENSchaffner.dbo.EE_repro_avance";
        queryMarcaje += " ON(rpr_pedido = av_pedido  and rpr_linea = av_linea)";
        queryMarcaje += " inner join ENSchaffner.dbo.EE_repro_falla";
        queryMarcaje += " on(rep_codigo = rpr_codigo_falla)";
        queryMarcaje += " inner join ENSchaffner.dbo.EE_repro_est ";
        queryMarcaje += " on(Pedido_rep = rpr_pedido and Linea_rep = rpr_linea)";
        queryMarcaje += " inner join ENSchaffner.dbo.TPMP tp";
        queryMarcaje += " on(rpr_pedido = tp.Pedido AND rpr_linea = tp.Linea)";
        queryMarcaje += " left outer join MAC2BETA.dbo.VW_equipos_pedidos_sumados ep";
        queryMarcaje += " on(ep.nb_np = rpr_pedido and ep.nb_linea = rpr_linea and ep.MVD_CodProd =  Codigo_mat_prima COLLATE SQL_Latin1_General_CP850_CS_AS and Codigo_semielaborado = ep.vc_proceso COLLATE SQL_Latin1_General_CP850_CS_AS)";
        queryMarcaje += " where av_fecha_termino is null and rep_analisis =1 ";
        queryMarcaje += " and Codigo_mat_prima = '" + material + "' ";
        /*
        String queryObraHidropack = "select convert(datetime,TAB_Fecha1),nb_np,nb_linea,'SIN',(nb_solicitado-MVD_CantAsignada) as nb_solicitado,vc_codigo,PER_NOMBRE,convert(datetime,TAB_Fecha1),'SIN','SIN','SIN', MVD_CantAsignada,'NO'";
        queryObraHidropack += " from MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_ENC enc";
        queryObraHidropack += " inner join MAC2BETA.dbo.TBL_SIS_PEDIDO_MATERIAL_A_BODEGA_DET det";
        queryObraHidropack += " on(enc.nb_numero = det.nb_numero)";
        queryObraHidropack += " inner join MAC2BETA.dbo.VW_pedido_de_materiales pm";
        queryObraHidropack += " on(pm.MVD_NumeroDoc = nb_numero_doc_original and pm.MVD_Linea = nb_numero_linea_original)";
        queryObraHidropack += " inner join SiaHidropack.dbo.SYS_TABLAS ";
        queryObraHidropack += " on(TAB_CodTabla = 500 AND TAB_Codigo = nb_np)";
        queryObraHidropack += " INNER JOIN SiaHidropack.dbo.sys_persona ";
        queryObraHidropack += " ON (TAB_Texto3 = PER_CODIGO AND PER_CLIENTE = 'S')";
        queryObraHidropack += " where ch_empresa = 'H' and pm.MVD_CodProd = '" + material + "'";
        queryObraHidropack += " and (pm.MVD_Cant - pm.MVD_CantAsignada) > 0 and enc.nb_estado < 5 ";
        
        String queryHidropack  = " select (SELECT DATEADD (day, -(select isnull(sum(nb_tiempo_produccion),0) from MAC2BETA.dbo.TBL_HID_FAMILIA_RUTA_PROCESO hfrp";
           queryHidropack += " where hfrp.TBL_HID_TIPO_FAMILIA_nb_id = htf.nb_id), det.dt_fecha_entrega)),det.nb_numero_solicitud,det.nb_item,hrp.vc_nombre_corto,hm.nb_cantidad * det.nb_cantidad,PRO_CODPROD,PER_NOMBRE,det.dt_fecha_entrega,'SIN','SIN','SIN','0','NO'";
           queryHidropack += " from MAC2BETA.dbo.TBL_HID_MATERIAL hm ";
           queryHidropack += " inner join SiaSchaffner.dbo.STO_PRODUCTO";
           queryHidropack += " on(ID_FILA = hm.STO_PRODUCTO_ID_FILA)";
           queryHidropack += " inner join MAC2BETA.dbo.TBL_HID_RUTA_DE_PROCESO hrp";
           queryHidropack += " on(hm.TBL_HID_RUTA_DE_PROCESO_nb_id = hrp.nb_id)";
           queryHidropack += " inner join MAC2BETA.dbo.TBL_HID_PRODUCTO hp";
           queryHidropack += " on(hm.TBL_HID_PRODUCTO_nb_id = hp.nb_id) ";
           queryHidropack += " inner join MAC2BETA.dbo.TBL_HID_TIPO_FAMILIA htf";
           queryHidropack += " on(hp.TBL_HID_TIPO_FAMILIA_nb_id = htf.nb_id)";
           queryHidropack += " inner join MAC2BETA.dbo.TBL_HID_FAMILIA_RUTA_PROCESO hfrp";
           queryHidropack += " on(hfrp.TBL_HID_TIPO_FAMILIA_nb_id = htf.nb_id and hfrp.TBL_HID_RUTA_PROCESO_nb_id = hm.TBL_HID_RUTA_DE_PROCESO_nb_id)";
           queryHidropack += " inner join MAC2BETA.dbo.TBL_HID_SOLICITUD_DE_FABRICACION_DET det";
           queryHidropack += " on(hp.nb_id = det.TBL_HID_PRODUCTO_nb_id)";
           queryHidropack += " inner join MAC2BETA.dbo.TBL_HID_SOLICITUD_DE_FABRICACION_ENC enc";
           queryHidropack += " on(det.nb_numero_solicitud = enc.nb_numero_solicitud)";
           queryHidropack += " left outer join MAC2BETA.dbo.TBL_HID_MARCAJE hmj";
           queryHidropack += " on(hmj.TBL_HID_FAMILIA_RUTA_DE_PROCESO_nb_id = hfrp.nb_id and hmj.TBL_HID_SOLICITUD_DE_FABRICACION_ENC_nb_numero_solicitud = det.nb_numero_solicitud)";
           queryHidropack += " INNER JOIN SiaHidropack.dbo.SYS_TABLAS ";
           queryHidropack += " ON(TAB_CodTabla = 500 AND convert(bigint,TAB_Codigo) = enc.nb_id_obra)";
           queryHidropack += " INNER JOIN SiaHidropack.dbo.sys_persona ";
           queryHidropack += " ON (convert(bigint,TAB_Texto3) = PER_CODIGO AND PER_CLIENTE = 'S') ";
           queryHidropack += " where PRO_CODPROD = '" + material + "'  and (hmj.dt_fecha_marcaje_fin is null or hmj.dt_fecha_marcaje_fin = '01/01/1900')";
        */
        
        try{
            conENS = mConexion.getENSConnector();
            st = conENS.createStatement();
            
         /*  rs = st.executeQuery(queryHidropack);
            
            while(rs.next()){
                Equipo equi = new Equipo();
                equi.setReadTime(rs.getDate(1));
                equi.setNp(rs.getInt(2));
                equi.setLinea(rs.getInt(3));
                equi.setCodPro(rs.getString(6));
                equi.setCantidad(rs.getFloat(5));
                equi.setProceso(rs.getString(4));
                equi.setCliente(rs.getString(7));
                equi.setExpeditacion(rs.getDate(8));
                equi.setDescripcion(rs.getString(9));
                equi.setDiseno(rs.getString(10));
                equi.setSerie(rs.getString(11));
                equi.setConsumo(rs.getFloat(12));
                equi.setReproceso(rs.getString(13));
                equi.setHidropack("SI");
                equiposConMateriales.addElement(equi);
            }
            
            rs.close();*/
           
            
         /*   rs = st.executeQuery(queryObraHidropack);
            
            while(rs.next()){
                Equipo equi = new Equipo();
                equi.setReadTime(rs.getDate(1));
                equi.setNp(rs.getInt(2));
                equi.setLinea(rs.getInt(3));
                equi.setCodPro(rs.getString(6));
                equi.setCantidad(rs.getFloat(5));
                equi.setProceso(rs.getString(4));
                equi.setCliente(rs.getString(7));
                equi.setExpeditacion(rs.getDate(8));
                equi.setDescripcion(rs.getString(9));
                equi.setDiseno(rs.getString(10));
                equi.setSerie(rs.getString(11));
                equi.setConsumo(rs.getFloat(12));
                equi.setReproceso(rs.getString(13));
                equi.setHidropack("SI");
                equiposConMateriales.addElement(equi);
            }
            
            rs.close();*/
          
            
            
            rs = st.executeQuery(queryMarcaje);
            
            while(rs.next()){
                Equipo equi = new Equipo();
                equi.setReadTime(rs.getDate(1));
                equi.setNp(rs.getInt(2));
                equi.setLinea(rs.getInt(3));
                equi.setCodPro(rs.getString(6));
                equi.setCantidad(rs.getFloat(5));
                equi.setProceso(rs.getString(4));
                equi.setCliente(rs.getString(7));
                equi.setExpeditacion(rs.getDate(8));
                equi.setDescripcion(rs.getString(9));
                equi.setDiseno(rs.getString(10));
                equi.setSerie(rs.getString(11));
                equi.setConsumo(rs.getFloat(12));
                equi.setReproceso(rs.getString(13));
                equi.setHidropack("NO");                
                equiposConMateriales.addElement(equi);
            }
            
            rs.close();
            
            
            rs = st.executeQuery(query4);
            
            while(rs.next()){
                Equipo equi = new Equipo();
                equi.setReadTime(rs.getDate(1));
                equi.setNp(rs.getInt(2));
                equi.setLinea(rs.getInt(3));
                equi.setCodPro(rs.getString(6));
                equi.setCantidad(rs.getFloat(5));
                equi.setProceso(rs.getString(4));
                equi.setCliente(rs.getString(7));
                equi.setExpeditacion(rs.getDate(8));
                equi.setDescripcion(rs.getString(9));
                equi.setDiseno(rs.getString(10));
                equi.setSerie(rs.getString(11));
                equi.setConsumo(rs.getFloat(12));
                equi.setReproceso(rs.getString(13));
                equi.setHidropack("NO");
                equiposConMateriales.addElement(equi);
            }
            
            rs.close();
            
            rs = st.executeQuery(query);
            
            while(rs.next()){
                Equipo equi = new Equipo();
                equi.setReadTime(rs.getDate(1));
                equi.setNp(rs.getInt(2));
                equi.setLinea(rs.getInt(3));
                equi.setCodPro(rs.getString(6));
                equi.setCantidad(rs.getFloat(5));
                equi.setProceso(rs.getString(4));
                equi.setCliente(rs.getString(7));
                equi.setExpeditacion(rs.getDate(8));
                equi.setDescripcion(rs.getString(9));
                equi.setDiseno(rs.getString(10));
                equi.setSerie(rs.getString(11));
                equi.setConsumo(rs.getFloat(12));
                equi.setReproceso(rs.getString(13));
                equi.setHidropack("NO");
                /*Vector mapaAsBuild = new Vector();
                mapaAsBuild.addElement(rs.getDate(1)); // readTime 0
                mapaAsBuild.addElement(new Integer(rs.getInt(2))); // np 1
                mapaAsBuild.addElement(new Integer(rs.getInt(3))); // linea 2
                mapaAsBuild.addElement(rs.getString(6)); // material 3
                mapaAsBuild.addElement(new Float(rs.getFloat(5))); // cantidad de material 4
                mapaAsBuild.addElement(rs.getString(4)); // proceso 5
                mapaAsBuild.addElement(rs.getString(7)); // cliente 6
                mapaAsBuild.addElement(rs.getDate(8)); // expeditaci�n 7
                mapaAsBuild.addElement(rs.getString(9)); //descripcion 8
                mapaAsBuild.addElement(rs.getString(10)); //dise�o 9
                mapaAsBuild.addElement(rs.getString(11)); // serie 10
                mapaAsBuild.addElement(new Float(rs.getFloat(12))); // Cantidad entregada de bodega 11
                mapaAsBuild.addElement(rs.getString(13)); // Reproceso 12*/
                equiposConMateriales.addElement(equi);
            }
            
            rs.close();
            this.desconectarEnschaffner();
            
            
        }catch(Exception e){
            this.desconectarEnschaffner();
            new Loger().logger("CrpBean.class " , "ERROR: mapaAsBuildConEstructura (): "+e.getMessage(),0);
        }
        return equiposConMateriales;
    }
    
}//fin