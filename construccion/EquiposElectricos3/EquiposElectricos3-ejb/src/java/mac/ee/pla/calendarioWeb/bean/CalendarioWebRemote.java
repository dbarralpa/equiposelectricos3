
package mac.ee.pla.calendarioWeb.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for CalendarioWeb enterprise bean.
 */
public interface CalendarioWebRemote extends EJBObject, CalendarioWebRemoteBusiness {
    
    
}
