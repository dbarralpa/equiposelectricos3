
package mac.ee.pla.ordenCompra.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for Orden enterprise bean.
 */
public interface OrdenRemote extends EJBObject, OrdenRemoteBusiness {
    
    
}
