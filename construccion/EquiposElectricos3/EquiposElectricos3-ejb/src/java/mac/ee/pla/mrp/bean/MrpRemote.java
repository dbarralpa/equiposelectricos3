
package mac.ee.pla.mrp.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for Mrp enterprise bean.
 */
public interface MrpRemote extends EJBObject, MrpRemoteBusiness {
    
    
}
