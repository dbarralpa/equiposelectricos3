package mac.ee.pla.estructuraEquipo.bean;

import java.io.IOException;
import java.sql.Date;
import java.util.List;
import java.util.Vector;
import javax.ejb.*;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import java.util.ArrayList;

/**
 * This is the bean class for the estructuraEquipoBean enterprise bean.
 * Created 11-mar-2008 15:52:26
 * @author dbarra
 */
public class estructuraEquipoBean implements SessionBean, estructuraEquipoRemoteBusiness {
    private SessionContext context;
    Vector materialModificarStockMinMax = new Vector();
    EstructuraPorEquipo estructuraEquipo = new EstructuraPorEquipo();
    int incrementar;
    String areaDeNegocio = "";
    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    
    
    // Add business logic below. (Right-click in editor and choose
    // "EJB Methods > Add Business Method" or "Web Service > Add Operation")

    
    public float retornarStockMin(String material){
        return estructuraEquipo.retornarStockMin(material);
    }
    
    public float retornarStockMax(String material){
         return estructuraEquipo.retornarStockMax(material);
    }
     public byte actualizarLeadTime(String material , short leadTime){
         return estructuraEquipo.actualizarLeadTime(material,leadTime);
     }
     public short leadTime(String material){
      return  estructuraEquipo.leadTime(material);
     }
     
     public short diferenciaDiasLeadTime(String inicio , short leadTime){
         return estructuraEquipo.diferenciaDiasLeadTime(inicio,leadTime);
     }
     
     public void datosParaDesviacionStandard(String material){
       /* estructuraEquipo.desviacionStandartPorMaterial(material);
         if(estructuraEquipo.datosParaDesviacionStandard().size()>0){
           estructuraEquipo.calcularEnDiasConsumo();
           estructuraEquipo.desviacionStandard();  
         }*/   
     }
     public Vector datosParaDesviacionStandard(){
         return  new Vector();//estructuraEquipo.datosParaDesviacionStandard();
     }
     
     public Vector retornarDesviacionStandard(){
         return new Vector();//estructuraEquipo.retornarDesviacionStandart();
     }
     
     public void setIncrementar(int incrementar ,int verificar){
         if(verificar==1){
           this.incrementar += incrementar;    
         }else{
             this.incrementar -= incrementar;
         }
     }
      public void setIncrementar(int incrementar){
         this.incrementar = incrementar;
         
     }
     public int getIncrementar(){
         return incrementar;
     }
     public void setCodigoMaterial(String material){
         estructuraEquipo.setCodigoMatPrima(material);
     }
     public String getCodigoMaterial(){
         return estructuraEquipo.getCodigoMatPrima();
     }
     
     public void setFechaEntrega(String fechaEntrega){
         estructuraEquipo.setFechaEntrega(fechaEntrega);
     }
     
     public String getFechaEntrega(){
         return estructuraEquipo.getFechaEntrega();
     }
     
     public void materialesStockMinMax(){
          estructuraEquipo.materialesStockMinMax1();
     }
     
     public Vector retornarStockMinMax(){
         return estructuraEquipo.retornarStockMinMax();
     }
    public byte actualizarStockMinMax (String material,String uMedida,float cantidad,byte compara,String descripcion,String area){
         byte count = 0;
         //count = estructuraEquipo.actualizarStockMinMax(material,uMedida,cantidad,compara,descripcion,area);
         return count;
     }
     public void materialesEE(){
       //  estructuraEquipo.materialesEE1();
     }
     
     public Vector retornarMaterialesEE(){
         return new Vector();//estructuraEquipo.retornarMaterialesEE();
     }
     
     public int areaNegocio(String material,String area,String areaAntigua,String descripcion,String um,int iden){
         //int veri = estructuraEquipo.areaNegocio(material,area,areaAntigua,descripcion,um,iden);
         return 0;
     }
     
     public void setAreaDeNegocio(String areaDeNegocio){
         this.areaDeNegocio = areaDeNegocio;
     }
     public String getAreaDeNegocio(){
         return areaDeNegocio;
     }
     
     public void agregarAvectorEE(String codigo,String descri ,String um,float stockMinimo,float stockMaximo,int leadTime,String importancia,String area){
         //estructuraEquipo.agregarAvectorEE(codigo,descri,um,stockMinimo,stockMaximo,leadTime,importancia,area);
     }
     
     public void setearAreaNegocioEE(String area,String area1,int vector,String cambiar,String agregar,int position){
         //estructuraEquipo.setearAreaNegocioEE(area,area1,vector,cambiar,agregar,position);
         }
     
     public void setearAreaNegocioHP(String area,String area1,int vector,String cambiar,String agregar,int position){
         //estructuraEquipo.setearAreaNegocioHP(area,area1,vector,cambiar,agregar,position);
         }
     public void setearAreaNegocioLG(String area,String area1,int vector,String cambiar,String agregar,int position){
         //estructuraEquipo.setearAreaNegocioLG(area,area1,vector,cambiar,agregar,position);
         }
     public void setearAreaNegocioMM(String area,String area1,int vector,String cambiar,String agregar,int position){
         //estructuraEquipo.setearAreaNegocioMM(area,area1,vector,cambiar,agregar,position);
         }
     public Vector materialesNoMrp(){
         return estructuraEquipo.materialesNoMrp();
     }
     public Vector materialesMrp(){
         return estructuraEquipo.materialesMrp();
     }
     public int comprasRequeridasMaterialesNoMrp(String codigo,String descripcion,float cantRequerida,String uni,String comprador,short leadTime,String contador,float stockMin,float stockMax,String fechaEntrega,int verificar,byte estado,byte identificador){
         return estructuraEquipo.comprasRequeridasMaterialesNoMrp(codigo,descripcion,cantRequerida,uni,comprador,leadTime,contador,stockMin,stockMax,fechaEntrega,verificar,estado,identificador);
     }
      public void minMaxVacio(Vector vacio){
      estructuraEquipo.minMaxVacio(vacio);
     }
      public void  materialesEEvacio(Vector vacio){
       //   estructuraEquipo.materialesEEvacio(vacio);
      }
      public String  buscarComprador(String codigo){
          return estructuraEquipo.buscarComprador(codigo);
      }
    /* public List stockMinMax(){
           return ;//estructuraEquipo.stockMinMax();
       }*/
     public byte actualizarStockMinMaxLeadTime(String material,float stocMinimo,float stocMaximo,int leadTime){
         //byte veri = estructuraEquipo.actualizarStockMinMaxLeadTime(material,stocMinimo,stocMaximo,leadTime);
         return 0;
     }
     
      public byte actualizarStockMinMax1(String material,float stocMinimo,float stocMaximo){
         byte veri = estructuraEquipo.actualizarStockMinMax(material,stocMinimo,stocMaximo);
         return veri;
     }
       public char grabarAFlex()  throws IOException{
          char veri = estructuraEquipo.grabarAFlex();
          return veri;
       }
      
          public void cerrarCOneccion(){
              estructuraEquipo.cerrarCOneccion();
          }
      public Vector cambioMoneda(){
         return estructuraEquipo.cambioMoneda();
      }
     
      public void actualizarMoneda(String codPro,byte moneda){ 
         estructuraEquipo.actualizarMoneda(codPro,moneda); 
      }
     
   }
