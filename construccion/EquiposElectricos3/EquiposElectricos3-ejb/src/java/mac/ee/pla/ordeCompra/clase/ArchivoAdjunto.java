/*
 * ArchivoAdjunto.java
 *
 * Created on 3 de abril de 2009, 10:16
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package mac.ee.pla.ordeCompra.clase;

import java.util.Date;

/**
 *
 * @author mrojas
 */
public class ArchivoAdjunto {
    private int numSap;
    private Date fechaEntrada;
    private String nombreArchivo;
    /** Creates a new instance of ArchivoAdjunto */
    public ArchivoAdjunto() {
    }

    public int getNumSap() {
        return numSap;
    }

    public void setNumSap(int numSap) {
        this.numSap = numSap;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    public void setNombreArchivo(String nombreArchivo) {
        this.nombreArchivo = nombreArchivo;
    }
    
    public String contenType(String nombre){
        String extencion =nombre;
        int ultimaPosi=0;
        for (int i = 0; i < extencion.length(); i++) {
            if (extencion.charAt(i) == '.'){
                ultimaPosi=i;
            }
        }
        extencion = extencion.substring(ultimaPosi);
        boolean valido = false;
        if (extencion.toLowerCase().equals(".jpg")){
            extencion = "image/gif";
            valido = true;}
        if (extencion.toLowerCase().equals(".png")){
            extencion = "image/png";
            valido = true;} 
        if (extencion.toLowerCase().equals(".bmp")){
            extencion = "image/bmp";
            valido = true;}
        if (extencion.toLowerCase().equals(".gif")){
            extencion = "image/gif";
            valido = true;}
        if (extencion.toLowerCase().equals(".doc") || extencion.toLowerCase().equals(".docx") || extencion.toLowerCase().equals(".rtf")){
            extencion = "application/msword";
            valido = true;}
        if (extencion.toLowerCase().equals(".pdf")){
            extencion = "application/pdf";
            valido = true;}
        if (extencion.toLowerCase().equals(".xls") || extencion.toLowerCase().equals("xlsx")){
            extencion = "application/octet-stream";
            valido = true;}
        if (extencion.toLowerCase().equals(".txt")){
            extencion = "text/plain";
            valido = true;}
         if (extencion.toLowerCase().equals(".zip")){
            extencion = "application/zip";
            valido = true;}
        if (extencion.toLowerCase().equals(".rar")){
            extencion = "application/rar";
            valido = true;}
        if (extencion.toLowerCase().equals(".xml")){
            extencion = "text/xml";
            valido = true;}
        if (extencion.toLowerCase().equals(".mp3")){
            extencion = "audio/mpeg";
            valido = true;}
        if (extencion.toLowerCase().equals(".html")){
            extencion = "text/html";
            valido = true;}
        if(valido == false)
        {
            extencion ="<span style=\"background-color:#FFFF99\"><strong>Error, tipo de dato desconocido</strong></span>";
        }
        return extencion;



    }
    
}
