package mac.ee.pro.calidadProgramacion.bean;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Vector;
import javax.ejb.*;
import javax.swing.JLabel;
import mac.ee.pla.equipo.clase.Equipo;
import sis.conexion.Conexion;
import sis.logger.Loger;

/**
 * This is the bean class for the CalidadDeProgramacionBean enterprise bean.
 * Created 14-ene-2009 13:51:01
 * @author dbarra
 */
public class CalidadDeProgramacionBean implements SessionBean, CalidadDeProgramacionRemoteBusiness {
    private SessionContext context;
    private int diainicial; //Primer dia del mes
    private int diafinal;   //Ultimo dia del mes
    private int mes;        //Mes actual 
    private int ano;        //A�o actual       
    private GregorianCalendar calendario;
    private JLabel dias[][];
    private String prueba[][];
    private String[] diasDeLaSemana ={"Lu","Ma","Mi","Ju","Vi","Sa","Do"};
    private float[] porcentaje ={100,90,80,70,60,50,40,30,20,10,0};
    String[] dia = new String[5];
    int day = 0;
    private Conexion mConexion= new Conexion();
    private Connection conMAC=null;
    private Statement st;
    private ResultSet rs;
    SimpleDateFormat fecha4 = new SimpleDateFormat("yyyy-MM-dd");
    
    // <editor-fold defaultstate="collapsed" desc="EJB infrastructure methods. Click the + sign on the left to edit the code.">
    // TODO Add code to acquire and use other enterprise resources (DataSource, JMS, enterprise bean, Web services)
    // TODO Add business methods or web service operations
    /**
     * @see javax.ejb.SessionBean#setSessionContext(javax.ejb.SessionContext)
     */
    public void setSessionContext(SessionContext aContext) {
        context = aContext;
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbActivate()
     */
    public void ejbActivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbPassivate()
     */
    public void ejbPassivate() {
        
    }
    
    /**
     * @see javax.ejb.SessionBean#ejbRemove()
     */
    public void ejbRemove() {
        
    }
    // </editor-fold>
    
    /**
     * See section 7.10.3 of the EJB 2.0 specification
     * See section 7.11.3 of the EJB 2.1 specification
     */
    public void ejbCreate() {
        // TODO implement ejbCreate if necessary, acquire resources
        // This method has access to the JNDI context so resource aquisition
        // spanning all methods can be performed here such as home interfaces
        // and data sources.
    }
    
    
    
   public String[][] calendarioMes(int month,int year){
       Vector months = new Vector();
       int[] meses={month};
       dias = new JLabel[7][7];
       prueba = new String[6][14];
       //diasDeLaSemana = new String[7];
       calendario = new GregorianCalendar();
       //mes = calendario.get(Calendar.MONTH);
       ano = year;
       
       
       
      
       //for(int u=0;u<meses.length;u++){
           mes = month;
           update();
       
        for(int i=0; i<6; i++){
	  // for(int j=0; j<7; j++){
           /* if(!dias[i][0].getText().equals(" ") || 
               !dias[i][1].getText().equals(" ") ||
               !dias[i][2].getText().equals(" ") ||
               !dias[i][3].getText().equals(" ") ||
               !dias[i][4].getText().equals(" ") ||
               !dias[i][5].getText().equals(" ") ||
               !dias[i][6].getText().equals(" ")){
*/
                /*Dia da = new Dia();
                da.setDomingo(dias[i][0].getText());
                da.setLunes(dias[i][1].getText());
                da.setMartes(dias[i][2].getText());
                da.setMiercoles(dias[i][3].getText());
                da.setJueves(dias[i][4].getText());
                da.setViernes(dias[i][5].getText());
                da.setSabado(dias[i][6].getText());
                months.addElement(da);*/
               /*prueba[i][0].setText(dias[i][0].getText());
               prueba[i][1].setText("Domingo");
               prueba[i][2].setText(dias[i][1].getText());
               prueba[i][3].setText("Lunes");
               prueba[i][4].setText(dias[i][2].getText());
               prueba[i][5].setText("Martes");
               prueba[i][6].setText(dias[i][3].getText());
               prueba[i][7].setText("Miercoles");
               prueba[i][8].setText(dias[i][4].getText());
               prueba[i][9].setText("Jueves");
               prueba[i][10].setText(dias[i][5].getText());
               prueba[i][11].setText("Viernes");
               prueba[i][12].setText(dias[i][6].getText());
               prueba[i][13].setText("Sabado");*/
               
              /*System.out.print(prueba[i][1].getText() + " " + prueba[i][0].getText() + " ");
              System.out.print(prueba[i][3].getText() + " " + prueba[i][2].getText()+ " ");
              System.out.print(prueba[i][5].getText() + " " + prueba[i][4].getText()+ " ");
              System.out.print(prueba[i][7].getText() + " " + prueba[i][6].getText()+ " ");
              System.out.print(prueba[i][9].getText() + " " + prueba[i][8].getText()+ " ");
              System.out.print(prueba[i][11].getText() + " " + prueba[i][10].getText()+ " ");
              System.out.print(prueba[i][13].getText() + " " + prueba[i][12].getText() + "\n");*/
               //System.out.println("prueba1: " + dias[i][1].getText());
              System.out.print(prueba[i][7]  + " " + prueba[i][0]  + " ");
              System.out.print(prueba[i][8]  + " " + prueba[i][1]  + " ");
              System.out.print(prueba[i][9]  + " " + prueba[i][2]  + " ");
              System.out.print(prueba[i][10]  + " " + prueba[i][3]  + " ");
              System.out.print(prueba[i][11]  + " " + prueba[i][4]  + " ");
              System.out.print(prueba[i][12]  + " " + prueba[i][5]  + " ");
              System.out.print(prueba[i][13]  + " " + prueba[i][6]  + "\n");
            //break;
            }
          //}
     // }
  //}
       return prueba;
    }
    private void update(){
   	

     
	       	
                 calendario.set( Calendar.DATE, 1 );
                 
                // System.out.println( "Calendar.DATE: " + calendario.get( Calendar.DATE));
                 
                 calendario.set( Calendar.MONTH, mes+Calendar.JANUARY );
                 
               //  System.out.println( "Calendar.MONTH: " + calendario.get( Calendar.MONTH));
                 
                 calendario.set( Calendar.YEAR, ano+1900 ); //1900 a�o inicial
                 
                // System.out.println( "Calendar.YEAR: " + calendario.get( Calendar.YEAR));
                 
                 diainicial = calendario.get(Calendar.DAY_OF_WEEK)-Calendar.SUNDAY-1;
                 
                 //System.out.println("Calendar.DAY_OF_WEEK: " + calendario.get(Calendar.DAY_OF_WEEK));
                 //System.out.println("Calendar.SUNDAY-1: " + Calendar.SUNDAY);
               //  System.out.println("diaInicial: " + diainicial);
                 if(diainicial==-1){
                     diainicial=diainicial+7;
                 }
                 if(diainicial == 0){
                    diainicial  = 7;
                 }
                 diafinal = calendario.getActualMaximum(Calendar.DATE);
                 int diaSemana = 7;
                // System.out.println("diafinal: " + diafinal);                
                 for(int i=0; i<6; i++){
                 for(int y=0; y<diafinal; y++)
                 {	  	
                  //dias[(i+diainicial)/7+1][(i+diainicial)%7].setText( String.valueOf(i+1));
                  //System.out.println("i+diainicial)/7+1: " + (i+diainicial)/7+1 + " (i+diainicial)%7 " + (i+diainicial)%7);
                     prueba[i][diainicial-1] = String.valueOf(y+1);
                     prueba[i][diaSemana+diainicial-1] = diasDeLaSemana[diainicial-1];
                     
                     if(diainicial-1 == 6){
                        i++;
                        diainicial = 1;
                     }else{
                      diainicial++;
   
                     }
                     if(y-diafinal == -1){
                         i++;
                     }
                     
                 }
                  
               }//fin for
          ///////////////////////////////////////////////////////////////////       
                int contador = 0;
                for(int i=0; i<7; i++){ 
                 if(prueba[0][i] == null){
                     contador++;
                 }
             }
               //System.out.println("contador: " + contador);
              calendario.set( Calendar.MONTH, (mes+Calendar.JANUARY)-1);  
              diafinal = calendario.getActualMaximum(Calendar.DATE);
              for(int i=contador-1; i>=0; i--){ 
                 prueba[0][i] = String.valueOf(diafinal);
                 prueba[0][diaSemana+i] = diasDeLaSemana[i];
                 diafinal--;
             }
          ////////////////////////////////////////////////////////////////////////////
              contador =0;
                for(int i=0; i<7; i++){ 
                 if(prueba[4][i] == null){
                     contador++;
                 }
             }
            //  System.out.println("contador: " + contador);
              calendario.set( Calendar.MONTH, mes+Calendar.JANUARY);  
              diafinal = calendario.getActualMaximum(Calendar.DATE);
              if(contador == 0){
                if(!String.valueOf(diafinal).equals(prueba[4][6])){
                   for(int i=0; i<7; i++){ 
                       if(prueba[5][i] == null){
                       contador++;
                    }
                 }      
              }
                //System.out.println("contador: " + contador);
                int diferencia = 7-contador;
                for(int i=1; i<=contador; i++){ 
                 prueba[5][diferencia] = String.valueOf(i);
                 prueba[5][diaSemana+diferencia] = diasDeLaSemana[diferencia];
                 diferencia++;
             }
           }else{
                int diferencia = 7-contador;
                for(int i=1; i<=contador; i++){ 
                prueba[4][diferencia] = String.valueOf(i);
                prueba[4][diaSemana+diferencia] = diasDeLaSemana[diferencia];
                diferencia++;
             }  
                  
           }
              
              
    }
    public static void main(String[] aerfd){
        new CalidadDeProgramacionBean().calendarioMes(0,2010);
    }
    public int grabarCalendario(short horaIni , short horaFin , short identificador){
        int result = 0;
       try{
       conMAC = mConexion.getMACConnector();
       st = conMAC.createStatement();
       String query = "insert into MAC.dbo.Calendario (horaInicial,horaFinal,identificador) values (" + horaIni + "," +  horaFin+ "," + identificador + ")"; 
       st.executeUpdate(query);
       result = 1;
       st.close();
       conMAC.close();
       }catch(Exception e){
           new Loger().logger("CalidadDeProgramacionBean.class " , "ERROR: grabarCalendario() " + e.getMessage(),0);
           result = 0;
       }
        return result;
   }

    
    public void transformarFechaAhora(String fech ,short identificador){
        Timestamp fecha1 = null;
        Timestamp fecha = null;
        long hour = 0; 
        long hour1 = 0;
        long fechaHora1 = fecha1.valueOf(fech+" 00:00:00.0").getTime();
        long fechaHora  = fecha.valueOf("2008-01-01 00:00:00.0").getTime();
        hour = (fechaHora1 - fechaHora) /3600000;
       // System.out.println(hour);
        
        if(identificador == 1){
          hour1 = hour + 16;     
        }
        if(identificador == 2){
          hour1 = hour + 24;
        }
        if(identificador == 8){
          hour1 = hour + 24;
        }
      //  System.out.println(hour1);
        this.grabarCalendario((short)hour,(short)hour1,identificador);
    }
     public Vector cantidadEquipo(){
        Vector cantidadEquipos = new Vector();
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         /* consulta para traer los equipos que est�n en producci�n ecxepto los equipos que tienen reprogramaci�n
          * 2,3,4,5,6,7,8
          */ 
         String query = " select tp.Familia,tp.Expeditacion,count(tp.Expeditacion),sum(tp.Monto) from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea ) ) "
                       +" where "
                       +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) " 
                       +" Group by tp.Expeditacion,tp.Familia "
                       +" order by tp.Familia ";

         rs = st.executeQuery(query);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 
                 equipo.setFamilia(rs.getString(1));
                 equipo.setFechaInicio(rs.getDate(2));
                 equipo.setGrupo(rs.getInt(3));
                 equipo.setMonto(rs.getFloat(4));
                 cantidadEquipos.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR cantidadEquipo() " +ex.getMessage(),0);   
         }
          return cantidadEquipos;
     }
    public Vector cantidadEquiposRezagados(String area,String fecha){
        Vector cantidadEquipos = new Vector();
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " SELECT COUNT(Expeditacion),sum(Monto) FROM ENSchaffner.dbo.TPMP WHERE Familia = '" + area + "' AND Expeditacion < '" + fecha + "' "
                       +" AND Status <> 'Bodega' AND Familia <> 'REP' AND Status IS NOT NULL AND Activa != 1 AND Status <> 'Reproceso' ";
         rs = st.executeQuery(query);
             while(rs.next()){
                Equipo equipo = new Equipo();
                equipo.setGrupo(rs.getInt(1));
                equipo.setMonto(rs.getFloat(2));
                cantidadEquipos.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR cantidadEquipo() " +ex.getMessage(),0);
           }
         
          return cantidadEquipos;
     }
public Vector equipos(String fecha,String area){
        Vector cantidadEquipos = new Vector();
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         /*String query = " select  tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tp.Status,tp.Descri  from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea ) ) "
                       +" where "
                       +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) "
                       +" and tp.Expeditacion = '" + fecha + "' and tp.Familia = '" + area + "' ";
          */
         String query = " select  tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tp.Status,tp.Descri,tb.Observacion  "
                       +" from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea )) "
                       +" left outer join ENSchaffner.dbo.TBitacora tb "
                       +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea "
                       +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea)) "
                       +" where "
                       +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) "
                       +" and tp.Expeditacion = '" + fecha + "' and tp.Familia = '" + area + "' ";
         rs = st.executeQuery(query);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setExpeditacion(rs.getDate(4));
                 equipo.setMonto(rs.getFloat(5));
                 equipo.setSerie(rs.getString(6));
                 equipo.setKva(rs.getString(7));
                 equipo.setFamilia(rs.getString(8));
                 equipo.setNombreCliente(rs.getString(9));
                 equipo.setStatus(rs.getString(10));
                 equipo.setDescripcion(rs.getString(11));
                 equipo.setBitacora(rs.getString(12));
                 cantidadEquipos.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR equipos() " +ex.getMessage(),0);   
         }
          return cantidadEquipos;
     } 

public Vector razonReprograma(int ano2,int mes2,String fecha2,String razon){
        Vector razonReprograma = new Vector();
        String query = "";
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         if(razon.equals("12")){
             query =      " select tr.Pedido,tr.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tb.Observacion,tp.Status "
                         +" from ENSchaffner.dbo.TReprograma tr "
                         +" left outer join ENSchaffner.dbo.TPMP tp "  
                         +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                         +" and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                         +" ) " 
                         +" left outer join ENSchaffner.dbo.TBitacora tb "
                         +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea " 
                         +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea))  "
                         +" where tr.FechaModificacion <= '" + fecha2 + "' and tp.Pedido is not null "
                         +" and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                         +"  "
                         +" and tr.Cod_Razon = '4' "
                         +" union "
                         +" select tr.Pedido,tr.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tb.Observacion,tp.Status "
                         +" from ENSchaffner.dbo.TReprograma tr "
                         +" left outer join ENSchaffner.dbo.TPMP tp "  
                         +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                         +" and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                         +" ) " 
                         +" left outer join ENSchaffner.dbo.TBitacora tb "
                         +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea " 
                         +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea))  "
                         +" where tr.FechaModificacion <= '" + fecha2 + "' and tp.Pedido is not null "
                         +" and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                         +"  "
                         +" and tr.Cod_Razon = '8' ";
         }else if(razon.equals("5")){
                query = " select tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tb.Observacion,tp.Status "
                        +" from ENSchaffner.dbo.TPMP tp "
                        +" left outer join ENSchaffner.dbo.TBitacora tb "
                        +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea "
                        +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea))  "
                        +" where Status = 'RevisionFinal' "
                        +" and Activa = 0 "
                        +" and Familia <> 'REP' "
                        +" and year(Expeditacion) <=  " + ano2 + "  and month(Expeditacion) <= '" + mes2 + "' ";
         }else if(razon.equals("6")){
                /*query =   " select count(distinct(convert(varchar(7),Pedido)+ convert(varchar(3),Linea))) from ENSchaffner.dbo.TPMP "
                         +" where Status = 'Reproceso' "
                         +" and Activa = 0 "
                         +" and Familia <> 'REP' "
                         +" and year(Expeditacion) <=  " + ano2 + "  and month(Expeditacion) <= '" + mes2 + "' ";
                 */
                 query = " select tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tb.Observacion,tp.Status "
                        +" from ENSchaffner.dbo.TPMP tp "
                        +" left outer join ENSchaffner.dbo.TBitacora tb "
                        +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea "
                        +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea))  "
                        +" where Status = 'Reproceso' "
                        +" and Activa = 0 "
                        +" and Familia <> 'REP' "
                        +" and year(Expeditacion) <=  " + ano2 + "  and month(Expeditacion) <= '" + mes2 + "' ";
                
         }else{
                /*query = " select  tr.Pedido,tr.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente "
                       +" from ENSchaffner.dbo.TReprograma tr "
                       +" left outer join ENSchaffner.dbo.TPMP tp "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +" )"
                       +" where tr.FechaModificacion <= '" + fecha2 + "' and tp.Pedido is not null "
                       +" and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea )"
                       +" and tr.Cod_Razon = '" + razon + "'";*/
                  query = " select tr.Pedido,tr.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tb.Observacion,tp.Status "
                         +" from ENSchaffner.dbo.TReprograma tr "
                         +" left outer join ENSchaffner.dbo.TPMP tp "  
                         +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                         +" and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                         +" ) " 
                         +" left outer join ENSchaffner.dbo.TBitacora tb "
                         +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea" 
                         +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea))  "
                         +" where tr.FechaModificacion <= '" + fecha2 + "' and tp.Pedido is not null "
                         +" and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                         +"  "
                         +" and tr.Cod_Razon = '" + razon + "' ";
                  
                  
         }
         rs = st.executeQuery(query);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setExpeditacion(rs.getDate(4));
                 equipo.setMonto(rs.getFloat(5));
                 equipo.setSerie(rs.getString(6));
                 equipo.setKva(rs.getString(7));
                 equipo.setFamilia(rs.getString(8));
                 equipo.setNombreCliente(rs.getString(9));
                 equipo.setBitacora(rs.getString(10));
                 equipo.setStatus(rs.getString(11));
                 razonReprograma.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR razonReprograma() " +ex.getMessage(),0);   
         }
          return razonReprograma;
     }    
public Vector equiposIndiceCarga(String fecha1,String fecha2){
        Vector razonReprograma = new Vector();
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " select tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tp.Status,tp.Descri from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea ) ) "
                       +" where "
                       +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) "
                       +" and tp.Expeditacion between '" + fecha1 + "' and '" + fecha2 + "'";
         rs = st.executeQuery(query);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setExpeditacion(rs.getDate(4));
                 equipo.setMonto(rs.getFloat(5));
                 equipo.setSerie(rs.getString(6));
                 equipo.setKva(rs.getString(7));
                 equipo.setFamilia(rs.getString(8));
                 equipo.setNombreCliente(rs.getString(9));
                 equipo.setStatus(rs.getString(10));
                 razonReprograma.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR razonReprograma() " +ex.getMessage(),0);   
         }
          return razonReprograma;
     }    
public float promedioDeManufactura(String familia,int mes1,int ano1){
        float dias = 0;
        int contador =0;
        float kva = 0;
        java.util.Date date = new java.util.Date();
        //SimpleDateFormat ano = new SimpleDateFormat("yyyy");
        //SimpleDateFormat mes = new SimpleDateFormat("MM");
        //SimpleDateFormat dia = new SimpleDateFormat("dd");
        //GregorianCalendar cal = new GregorianCalendar();
   try{   
       if(!familia.equals("BIFA") && !familia.equals("CELDA") && !familia.equals("ECM")){ 
        //int ano1 = Integer.parseInt(ano.format(date));
        //int mes1 = Integer.parseInt(mes.format(date));
        //cal.set( Calendar.MONTH,mes1);
        //int diafinal = cal.getActualMaximum(Calendar.DATE);   
        //mes1 -=1;
        if(mes1 == 0){
            ano1 -=1;
            mes1 = 12;
        }
        String fecha = "";
        String mesleng = String.valueOf(mes1);
        if(mesleng.length() == 1){
          mesleng = "0".concat(mesleng);
        }
        fecha = String.valueOf(ano1)+mesleng+"01";
        //System.out.println(fecha);
         
           
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " SELECT FechaPMP,FechaBodega,KVA from ENSchaffner.dbo.TPMP WHERE "
                       +" Status = 'Bodega' "
                       +" AND FechaBodega > '" + fecha + "' AND Familia = '" + familia + "'";
                        
         rs = st.executeQuery(query);
             while(rs.next()){
             String str=rs.getString(3).trim();
	     int divi=str.indexOf("/");
             int esp =str.indexOf(" ");
             int t =str.indexOf("T");
             if(rs.getDate(1).getTime() < rs.getDate(2).getTime() && divi < 0 && esp < 0 && t < 0){
                 contador++;
                 long fechaIni = rs.getDate(1).getTime();
                 long fechaFin = rs.getDate(2).getTime();
                 dias += (fechaFin-fechaIni)/86400000;
                 kva  +=Integer.parseInt(rs.getString(3).trim());
             }
         }
         if(dias >0){
            //dias /=contador;
            // kva /= dias;
             dias /= kva;
         }
         rs.close();
         st.close();
         conMAC.close();
         }
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR promedioDeManufatura " +ex.getMessage(),0);   
         }
          return dias;
     }    

public Vector equiposTodos(String fecha){
        Vector cantidadEquipos = new Vector();
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         /*String query = " select  tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tp.Status,tp.Descri  from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea ) ) "
                       +" where "
                       +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) "
                       +" and tp.Expeditacion = '" + fecha + "'";
          */
         String query = " select  tp.Pedido,tp.Linea,tp.FechaCli,tp.Expeditacion,tp.Monto,tp.Serie,tp.KVA,tp.Familia,tp.Cliente,tp.Status,tp.Descri,tb.Observacion  "
                       +" from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea )) "
                       +" left outer join ENSchaffner.dbo.TBitacora tb "
                       +" on(tp.Pedido = tb.Pedido and tp.Linea = tb.Linea "
                       +" and tb.ID = (select max(tt.ID) from ENSchaffner.dbo.TBitacora tt where tp.Pedido = tt.Pedido and tp.Linea = tt.Linea)) "
                       +" where  "
                       +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) "
                       +" and tp.Expeditacion = '" + fecha + "'";
         rs = st.executeQuery(query);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setExpeditacion(rs.getDate(4));
                 equipo.setMonto(rs.getFloat(5));
                 equipo.setSerie(rs.getString(6));
                 equipo.setKva(rs.getString(7));
                 equipo.setFamilia(rs.getString(8));
                 equipo.setNombreCliente(rs.getString(9));
                 equipo.setStatus(rs.getString(10));
                 equipo.setDescripcion(rs.getString(11));
                 equipo.setBitacora(rs.getString(12));
                 cantidadEquipos.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR equiposTodos() " +ex.getMessage(),0);   
         }
          return cantidadEquipos;
     }

public int kvaPorMesYfamilia(String familia,String ano,String mes){
        int kvaPorMesYfamilia = 0;
        GregorianCalendar calendari = new GregorianCalendar();
        calendario.set( Calendar.MONTH,Integer.parseInt(mes));
        int mesFin = calendario.getActualMaximum(Calendar.DATE);
        if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
          mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
        }else{
         mes = String.valueOf(Integer.parseInt(mes)+1);   
         }
         try{
         if(!familia.equals("BIFA") && !familia.equals("CELDA") && !familia.equals("ECM")){
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " select sum(convert(int,KVA)) from ENSchaffner.dbo.TPMP where "
                       +" Status <> 'Bodega' AND Familia <> 'REP' AND Status IS NOT NULL AND Activa != 1 AND Status <> 'Reproceso' "
                       +" and Familia = '" + familia + "' and Expeditacion between '" + ano+mes.concat("01") +"' and '" + ano+mes.concat(String.valueOf(mesFin)) +"' "
                       +" and KVA not like '%/%' "
                       +" group by Familia ";
         rs = st.executeQuery(query);
             while(rs.next()){
                 kvaPorMesYfamilia = rs.getInt(1);
             }
         rs.close();
         st.close();
         conMAC.close();
         } 
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR kvaPorMesYfamilia() " +ex.getMessage(),0);   
         }
          return kvaPorMesYfamilia;
     } 




public float precisionDeProgramacion(String dia,int ano ,String mes,int y,String dia1){
    String fecha  = "";
    String fecha1 = "";
    String fecha2 = "";
    String mes1   = mes;
    int ano1      = ano;
    int valor     = 0;
    float valor1     = 0;
    float valor2     = 0;
    long date     = 0;
    int incrementar = 0;
    Vector reprograma = new Vector();
    String pasa = "";
  try{
    if(y==0){
        if(Integer.parseInt(dia) > 7){
               mes = String.valueOf(Integer.parseInt(mes)-1);
               if(Integer.parseInt(mes)==-1){
                  ano =ano - 1;
                  mes = "11";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia); 
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;
    fecha2 = String.valueOf(ano1)+"-"+mes1+"-"+dia1;
    conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " SELECT tp.FechaCli,tr.FechaReprog,tr.FechaModificacion,tp.Pedido,tp.Linea "
                       +" FROM ENSchaffner.dbo.TPMP tp,ENSchaffner.dbo.TReprograma tr "
                       +" WHERE tr.Cod_Razon in(0,1) AND  tp.Pedido = tr.Pedido AND tp.Linea = tr.Linea AND "
                       //+" tp.Status <> 'Bodega' and "
                       +" tr.FechaModificacion between '" + fecha + "' AND '" + fecha1 + "' ORDER BY tp.Pedido,tp.Linea  ";
         rs = st.executeQuery(query);
             while(rs.next()){
                 
                 if(rs.getDate(1).compareTo(rs.getDate(2))<0){
                     incrementar++;
                     date = (rs.getDate(2).getTime() - rs.getDate(1).getTime());
                     date /= 86400000;
                     if(incrementar == 1){
                        date += 2;    
                     }
                     if(date <10){
                      valor = (int)date;
                      valor1 += porcentaje[valor];    
                     }else{
                      valor1 += porcentaje[10];       
                     }  
                 }
                  if(rs.getDate(2).compareTo(rs.getDate(1))<=0){
                     date = 1;
                     incrementar++;
                     if(new java.sql.Date(0).valueOf(fecha2).compareTo(new java.sql.Date(0).valueOf(fecha4.format(new java.util.Date())))>=0){  
                     date = (rs.getDate(2).getTime() - new java.sql.Date(0).valueOf(fecha4.format(new java.util.Date())).getTime());
                     }else{
                     date = (rs.getDate(2).getTime() - rs.getDate(3).getTime());  
                     }
                     date /= 86400000;
                     if(date >= 0 && date <=7){
                         date =1 + 5;
                     }
                     else if(date >= 8 && date <=14){
                         date = 1 + 3;
                     }
                     else if(date >= 15 && date <=21){
                         date = 1 + 1;
                     }
                     else{
                         date = 1;
                     }
                     valor = (int)date;
                     valor1 += porcentaje[valor];
                  }
                 pasa = "A";
                 
             }
         if(valor1 > 0){
            valor2 = valor1 / incrementar;  
         }else if(pasa.equals("A")){
          valor2 = porcentaje[10];
         }else{
          valor2 = porcentaje[0]; 
         }
         
         rs.close();
         st.close();
         conMAC.close();
  }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR precisionDeProgramacion() " +ex.getMessage(),0);
  }
   return valor2;
}




public float indiceDeCumplimiento(String dia,int ano ,String mes,int y,String dia1){
    String fecha  = "";
    String fecha1 = "";
    String mes1   = mes;
    int ano1      = ano;
    int cantidadEquipos  = 0;
    int conFechaCliMenor = 0; 
    float valor = 0;
    float valor1 = 0;
    float valor2 = 0;
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  try{
    
    if(y==0){
         if(Integer.parseInt(dia) > 7){
               mes = String.valueOf(Integer.parseInt(mes)-1);
               if(Integer.parseInt(mes)==-1){
                  ano =ano - 1;
                  mes = "11";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    //int mesLenght = mes.length();
    if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia); 
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;
    
   // if(new java.sql.Date(0).valueOf(format.format(new java.util.Date())).compareTo(new java.sql.Date(0).valueOf(String.valueOf(ano1)+"-"+mes1+"-"+dia1))>0){
    conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " SELECT count(*) FROM ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "  
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.Cod_Razon = 5) "
                       +" WHERE  tp.FechaBodega between '" + fecha + "' AND '" + fecha1 + "' "
                       +" and tp.Activa <> 1 and tr.Pedido is null "
                       +" union all"
                       +" SELECT count(*) "
                       +" FROM ENSchaffner.dbo.TReprograma tr "
                       +" left outer join ENSchaffner.dbo.TReprograma tp "
                       +" on(tr.Pedido = tp.Pedido and tr.Linea = tp.Linea and tp.FechaModificacion < '" + fecha + "' and tp.Cod_Razon = 5) "
                       +" WHERE tr.FechaModificacion between '" + fecha + "' AND '" + fecha1 + "' "   
                       +" and tr.Cod_Razon = 5 "
                       +" and tp.Pedido is null    ";
         rs = st.executeQuery(query);
             while(rs.next()){
               cantidadEquipos += rs.getInt(1);
             }
         rs.close();
         String query1 = " SELECT count(*)  FROM ENSchaffner.dbo.TPMP tp "
                        +" left outer join ENSchaffner.dbo.TReprograma tr  " 
                        +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.Cod_Razon = 5) "
                        +" WHERE  tp.FechaBodega between '" + fecha + "' AND '" + fecha1 + "' "
                        +" and tp.Activa <> 1 and tr.Pedido is null "
                        +" and tp.FechaCli < tp.FechaBodega "
                        +" union all "
                        +" SELECT count(*) "
                        +" FROM ENSchaffner.dbo.TReprograma tr "
                        +" left outer join ENSchaffner.dbo.TReprograma tp "
                        +" on(tr.Pedido = tp.Pedido and tr.Linea = tp.Linea and tp.FechaModificacion < '" + fecha + "' and tp.Cod_Razon = 5) "
                        +" left outer join ENSchaffner.dbo.TPMP pm "
                        +" on(tr.Pedido = pm.Pedido and tr.Linea = pm.Linea) "
                        +" WHERE tr.FechaModificacion between '" + fecha + "' AND '" + fecha1 + "'    "
                        +" and tr.Cod_Razon = 5 "
                        +" and tp.Pedido is null "
                        +" and pm.FechaCli < tr.FechaModificacion  ";
         rs = st.executeQuery(query1);
             while(rs.next()){
               conFechaCliMenor += rs.getInt(1);
             }
         if(cantidadEquipos == 0 && conFechaCliMenor == 0){
            valor2 = 100; 
         }else{
         valor = cantidadEquipos-conFechaCliMenor;
         valor1 = valor/ cantidadEquipos;
         valor2 = valor1 *100;
         }
         rs.close();
         st.close();
         conMAC.close();
    //}
  }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR indiceDeCumplimiento() " +ex.getMessage(),0);
         }
    return valor2;
}

public String regresarFecha(String dia,int ano ,String mes,int y,String dia1){
    String fecha  = "";
        String fecha1 = "";
        String mes1   = mes;
        int ano1      = ano;
        
         try{
          if(y==0){
         if(Integer.parseInt(dia) > 7){
               mes = String.valueOf(Integer.parseInt(mes)-1);
               if(Integer.parseInt(mes)==-1){
                  ano =ano - 1;
                  mes = "11";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    //int mesLenght = mes.length();
    if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia); 
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;    
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR regresarFecha() " +ex.getMessage(),0);   
         }
        return fecha+fecha1;
}
//----------------------------------------------------------------------------------------------------------------
  public Vector pedidosEnBodega(String fechaIni,String fechaFin){
        Vector cantidadEquipos = new Vector();
        try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = " SELECT tp.Pedido,tp.Linea,tp.FechaCli,tp.FechaBodega,tp.Cliente,tp.Monto,tp.Serie,tp.KVA,'Bodega'  FROM ENSchaffner.dbo.TPMP tp "
                         +" left outer join ENSchaffner.dbo.TReprograma tr   "
                         +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.Cod_Razon = 5) "
                         +" WHERE  tp.FechaBodega between '" + fechaIni + "' AND '" + fechaFin + "' "
                         +" and tp.Activa <> 1 and tr.Pedido is null "
                         +" and tp.FechaCli >= tp.FechaBodega "
                         +" union all "
                         +" SELECT pm.Pedido,pm.Linea,pm.FechaCli,tr.FechaModificacion,pm.Cliente,pm.Monto,pm.Serie,pm.KVA,'Revisi�n final' "
                         +" FROM ENSchaffner.dbo.TReprograma tr "
                         +" left outer join ENSchaffner.dbo.TReprograma tp "
                         +" on(tr.Pedido = tp.Pedido and tr.Linea = tp.Linea and tp.FechaModificacion < '" + fechaIni + "' and tp.Cod_Razon = 5) "
                         +" left outer join ENSchaffner.dbo.TPMP pm "
                         +" on(tr.Pedido = pm.Pedido and tr.Linea = pm.Linea) "
                         +" WHERE tr.FechaModificacion between '" + fechaIni + "' AND '" + fechaFin + "'  "  
                         +" and tr.Cod_Razon = 5 "
                         +" and tp.Pedido is null "
                         +" and pm.FechaCli >= tr.FechaModificacion  ";
         rs = st.executeQuery(query);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setFechaTermino(rs.getDate(4));
                 equipo.setNombreCliente(rs.getString(5));
                 equipo.setMonto(rs.getFloat(6));
                 equipo.setSerie(rs.getString(7));
                 equipo.setKva(rs.getString(8));
                 equipo.setEstado(0); 
                 equipo.setDescripcion(rs.getString(9));
                 cantidadEquipos.addElement(equipo);
             }
          rs.close();
          String query1 = " SELECT tp.Pedido,tp.Linea,tp.FechaCli,tp.FechaBodega,tp.Cliente,tp.Monto,tp.Serie,tp.KVA,'Bodega'  FROM ENSchaffner.dbo.TPMP tp "
                         +" left outer join ENSchaffner.dbo.TReprograma tr   "
                         +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.Cod_Razon = 5) "
                         +" WHERE  tp.FechaBodega between '" + fechaIni + "' AND '" + fechaFin + "' "
                         +" and tp.Activa <> 1 and tr.Pedido is null "
                         +" and tp.FechaCli < tp.FechaBodega "
                         +" union all "
                         +" SELECT pm.Pedido,pm.Linea,pm.FechaCli,tr.FechaModificacion,pm.Cliente,pm.Monto,pm.Serie,pm.KVA,'Revisi�n final' "
                         +" FROM ENSchaffner.dbo.TReprograma tr "
                         +" left outer join ENSchaffner.dbo.TReprograma tp "
                         +" on(tr.Pedido = tp.Pedido and tr.Linea = tp.Linea and tp.FechaModificacion < '" + fechaIni + "' and tp.Cod_Razon = 5) "
                         +" left outer join ENSchaffner.dbo.TPMP pm "
                         +" on(tr.Pedido = pm.Pedido and tr.Linea = pm.Linea) "
                         +" WHERE tr.FechaModificacion between '" + fechaIni + "' AND '" + fechaFin + "'  "  
                         +" and tr.Cod_Razon = 5 "
                         +" and tp.Pedido is null "
                         +" and pm.FechaCli < tr.FechaModificacion  ";
          rs = st.executeQuery(query1);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setFechaTermino(rs.getDate(4));
                 equipo.setNombreCliente(rs.getString(5));
                 equipo.setMonto(rs.getFloat(6));
                 equipo.setSerie(rs.getString(7));
                 equipo.setKva(rs.getString(8));
                 equipo.setEstado(1);   
                 equipo.setDescripcion(rs.getString(9));
                 cantidadEquipos.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR pedidosEnBodega() " +ex.getMessage(),0);   
         }
          return cantidadEquipos;
     }
//----------------------------------------------------------------------------------------------------------------
public Vector reprograma(String dia,int ano ,String mes,int y,String dia1){
    String fecha = "";
    String fecha1 = "";
    String mes1  = mes;
    int ano1     = ano;
    int mes2 = Integer.parseInt(mes)+1;
    int ano2 = ano;
    Vector reprogramas = new Vector();
  try{
    if(y==0){
         if(Integer.parseInt(dia) > 7){
               mes = String.valueOf(Integer.parseInt(mes)-1);
               if(Integer.parseInt(mes)==-1){
                  ano =ano - 1;
                  mes = "11";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
   if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia);
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;
    conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
       
         
         String query = " select sum(tp.Monto) as FaltanPlanos, "
                       +"  (select sum(tp.Monto) "
                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp  "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 3) as FaltaAbastecimiento, "



                     /*  +"  (select count(distinct(convert(varchar(7),tr.Pedido)+ convert(varchar(3),tr.Linea))) "
                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 5) as FaltaRevisionFinal, "
                      */
                     
                       +" (select sum(Monto) from ENSchaffner.dbo.TPMP "
                       +" where Status = 'RevisionFinal' "
                       +" and Activa = 0 "
                       +" and Familia <> 'REP' "
                       +" and year(Expeditacion) <=  " + ano2 + "  and month(Expeditacion) <= '" + mes2 + "') as FaltaRevisionFinal, "


                       /*+"  (select count(distinct(convert(varchar(7),tr.Pedido)+ convert(varchar(3),tr.Linea))) "
                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null  "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 6) as EquiposEnReproceso, "
                        */
                 
                       +" (select sum(Monto) from ENSchaffner.dbo.TPMP "
                       +" where Status = 'Reproceso' "
                       +" and Activa = 0 "
                       +" and Familia <> 'REP' "
                       +" and year(Expeditacion) <=  " + ano2 + "  and month(Expeditacion) <= '" + mes2 + "') as EquiposEnReproceso, "



                       +"  (select sum(tp.Monto) "
                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 7) as FaltaAprobacionPlanos, "



                       +"  (select sum(tp.Monto) "
                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 8) + "
                       +" (select sum(tp.Monto) "
                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null  "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 4) as Otros,2,3,5,6,7,12 "



                       +"  from ENSchaffner.dbo.TReprograma tr "
                       +"  left outer join ENSchaffner.dbo.TPMP tp "
                       +"  on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                       +"  and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                       +"  ) "
                       +"  where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                       +"  and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                       +"  and tr.Cod_Razon = 2";
          //new Loger().logger("CalidadDeProgramacionBean.class ",query,1);         
         rs = st.executeQuery(query);
             while(rs.next()){
                 Vector reprograma = new Vector();
                 reprograma.addElement(new Float(rs.getFloat(1)));
                 reprograma.addElement(new Float(rs.getFloat(2)));
                 reprograma.addElement(new Float(rs.getFloat(3)));
                 reprograma.addElement(new Float(rs.getFloat(4)));
                 reprograma.addElement(new Float(rs.getFloat(5)));
                 reprograma.addElement(new Float(rs.getFloat(6)));
                 //reprograma.addElement(new Integer(rs.getInt(7)));
                 reprograma.addElement(fecha);
                 reprograma.addElement(fecha1);
                 reprograma.addElement(new Integer(rs.getInt(7)));
                 reprograma.addElement(new Integer(rs.getInt(8)));
                 reprograma.addElement(new Integer(rs.getInt(9)));
                 reprograma.addElement(new Integer(rs.getInt(10)));
                 reprograma.addElement(new Integer(rs.getInt(11)));
                 reprograma.addElement(new Integer(rs.getInt(12)));
                 reprogramas.addElement(reprograma);
             }
         rs.close();
         st.close();
         conMAC.close();
  }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR reprograma() " +ex.getMessage(),0);   
         }
    return reprogramas;
}

public Vector indiceDeCarga(String dia,int ano ,String mes,int y,String dia1){
    String fecha = "";
    String fecha1 = "";
    String mes1  = mes;
    int ano1     = ano;
    int mes2 = Integer.parseInt(mes)+1;
    int ano2 = ano;
    Vector indiceDeCargas = new Vector();
  try{
    if(y==0){
         if(Integer.parseInt(dia) > 7){
               mes = String.valueOf(Integer.parseInt(mes)-1);
               if(Integer.parseInt(mes)==-1){
                  ano =ano - 1;
                  mes = "11";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
               mes1 = String.valueOf(Integer.parseInt(mes1)+1);
               if(Integer.parseInt(mes1)==12){
                  ano1 =ano1 + 1;
                  mes1 = "0";
                  //System.out.println("a�o: " + ano + "---"+"mes: " + mes);
               }
            }
         }
   if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia); 
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;
    float sumMonto = 0;
    float sumMonto1 = 0;
    float sumMonto2 = 0;
    conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         
         
        /* String query =   " select count(Pedido),sum(Monto) from ENSchaffner.dbo.TPMP where "
                         +" Status <> 'Bodega' AND Familia <> 'REP' AND Status IS NOT NULL AND Activa != 1 AND Status <> 'Reproceso' "
                         +" and Expeditacion between '" + fecha + "' and '" + fecha1 + "'  ";
         */
        /* String query = " select sum(tp.Monto) from ENSchaffner.dbo.TPMP tp "
                       +" left outer join ENSchaffner.dbo.TReprograma tr "
                       +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.Cod_Razon = 5) "
                       +" where "
                       +" tp.Status <> 'Bodega' AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 AND tp.Status <> 'Reproceso' "
                       +" and tp.Expeditacion between '" + fecha + "' and '" + fecha1 + "' "
                       +" and tr.Cod_Razon is  null ";
          */
         
         String query ="select sum(tp.Monto) from ENSchaffner.dbo.TPMP tp "
                     +" left outer join ENSchaffner.dbo.TReprograma tr "
                     +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tp.Pedido = tm.Pedido and tp.Linea = tm.Linea ) ) "
                     +" where "
                     +" tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                     +" AND tp.Status <> 'Reproceso'  and (tr.Cod_Razon is null or tr.Cod_Razon in(0,1)) "
                     +" and tp.Expeditacion between '" + fecha + "' and '" + fecha1 + "'";
         
         rs = st.executeQuery(query);
             while(rs.next()){
                 Vector indiceDeCarga = new Vector();
                 sumMonto += rs.getFloat(1); 
             }
         rs.close();
         
         String query8 = "  select sum(tp.Monto) "
                        +" from ENSchaffner.dbo.TReprograma tr "
                        +" left outer join ENSchaffner.dbo.TPMP tp " 
                        +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                        +" and year(tp.Expeditacion) <= " + ano2 + " and month(tp.Expeditacion) <= '" + mes2 + "'"
                        +" ) "
                        +" where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                        +" and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                        +" and tr.Cod_Razon not in(0,1)";
         rs = st.executeQuery(query8);
             while(rs.next()){
                 sumMonto1 += rs.getFloat(1);
             }
         rs.close();
         Vector indiceDeCarga = new Vector();
         indiceDeCarga.addElement(new Float(sumMonto));
         indiceDeCarga.addElement(new Float(sumMonto1));
         indiceDeCarga.addElement(new Float(sumMonto + sumMonto1));
         indiceDeCarga.addElement(fecha);
         indiceDeCarga.addElement(fecha1);
         indiceDeCargas.addElement(indiceDeCarga);
         st.close();
         conMAC.close();
  }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR indiceDeCarga() " +ex.getMessage(),0);   
         }
    return indiceDeCargas;
}
//------------------------------------------------------------------------------------------------------------------------
 public byte modificarFechaExpeditacion(String np,String linea,String fechaReprog,String usuario,String fechaModificacion,String serie,String cod_razon,String comentario){
     int count = 0; 
     byte valor = (byte)0;
       try{
       conMAC = mConexion.getMACConnector();
       conMAC.setAutoCommit(false);
       st = conMAC.createStatement();
      
      
          String query = "insert into ENSchaffner.dbo.TReprograma (Pedido,Linea,FechaReprog,Usuario,FechaModificacion,Serie,Cod_Razon,Comentario) values ('" + np + "','" + linea + "','" + fechaReprog + "','" + usuario + "','" + fechaModificacion + "','" + serie + "','" + cod_razon + "','" + comentario + "')"; 
          st.executeUpdate(query); 
          String query1 = "update ENSchaffner.dbo.TPMP set Expeditacion = '" + fechaReprog + "' where Pedido = '" + np + "' and Linea = '" + linea + "'"; 
          st.executeUpdate(query1);
          String query2 = "update ENSchaffner.dbo.TComercial set ExpeditacionPedido = '" + fechaReprog + "' where Pedido = '" + np + "' and Linea = '" + linea + "'"; 
          st.executeUpdate(query2);

       conMAC.commit();
       conMAC.setAutoCommit(true);
       valor = (byte)1;
       st.close();
       conMAC.close();
       }catch(Exception e){
           new Loger().logger("CalidadDeProgramacionBean.class " , "ERROR: modificarFechaExpeditacion() " + e.getMessage(),0);
            try{
           conMAC.rollback();
           st.close();
           conMAC.close();
           }catch(Exception ex){
           new Loger().logger("CalidadDeProgramacionBean.class " , "ERROR: rollback() " + ex.getMessage(),0);    
           }
       }
     return valor;
   }
 public float montoBodega(String dia,int ano ,String mes,int y,String dia1){
    String fecha = "";
    String fecha1 = "";
    String mes1  = mes;
    int ano1     = ano;
    calendario = new GregorianCalendar();
    float sumMonto = 0;
  try{
    if(y==0){
         if(Integer.parseInt(dia) > 7){
            dia = "1";
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               calendario.set( Calendar.MONTH, Integer.parseInt(mes));  
               dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE)); 
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
                calendario.set( Calendar.MONTH, Integer.parseInt(mes));  
                dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
            }
         }
   if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia); 
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;
    
    
    conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query8 = "  SELECT sum(tp.Monto) "
                        +"  FROM ENSchaffner.dbo.TPMP tp "
                        +"  WHERE  tp.FechaBodega between '" + fecha + "' and '" + fecha1 + "'"
                        +"  and tp.Activa = 0 ";
         rs = st.executeQuery(query8);
             while(rs.next()){
                 sumMonto += rs.getFloat(1);
             }
         rs.close();
         st.close();
         conMAC.close();
  }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class "," ERROR montoBodega() " +ex.getMessage(),0);   
         }
    return sumMonto;
}
 public Vector cargaPlanta(int ano ,String mes,float conProblemas){
    String fecha = "";
    String fecha1 = "";
    String mes1  = mes;
    int ano1     = ano;
    Vector cargasDePlanta = new Vector();
    calendario = new GregorianCalendar();
    float produccion = 0f;
    float revFinal = 0f;
  try{
    
    
               calendario.set( Calendar.MONTH, Integer.parseInt(mes));  
               String dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE)); 
           
   if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+"01";
    fecha1 = String.valueOf(ano1)+mes1+dia1;
    
    
    conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         
           String query =" select  sum(tp.Monto) "
                        +" from ENSchaffner.dbo.TReprograma tr "
                        +" left outer join ENSchaffner.dbo.TPMP tp "
                        +" on(tp.Pedido = tr.Pedido and tp.Linea = tr.Linea and tp.Status not in ('Bodega') AND tp.Familia <> 'REP' AND tp.Status IS NOT NULL AND tp.Activa != 1 "
                        +" ) "
                        +" where tr.FechaModificacion <= '" + fecha1 + "' and tp.Pedido is not null "
                        +" and tr.FechaDeAlta = (select max(tm.FechaDeAlta) from ENSchaffner.dbo.TReprograma tm  where tr.Pedido = tm.Pedido and tr.Linea = tm.Linea ) "
                        +" and tr.Cod_Razon = '5' ";
         
         rs = st.executeQuery(query);
             while(rs.next()){
                 revFinal += rs.getFloat(1);
             }
         rs.close();
         
         String query8 = "  SELECT sum(Monto) FROM ENSchaffner.dbo.TPMP where Expeditacion <=  '" + fecha1 + "'   and Estado <> 1 and Activa = 0 and Division = 0";
         rs = st.executeQuery(query8);
             while(rs.next()){
              produccion += rs.getFloat(1);   
             }
         rs.close();
         String query9 = "  SELECT sum(Monto) FROM ENSchaffner.dbo.TPMP where Estado = 1 and  FechaBodega  Between '" + fecha + "' and '" + fecha1 + "' and Activa = 0 And Division = 0";
         rs = st.executeQuery(query9);
             while(rs.next()){
              produccion += rs.getFloat(1);   
             }
         
         Vector cargaPlanta = new Vector();
         cargaPlanta.addElement(new Float(produccion));
         cargaPlanta.addElement(new Float(conProblemas - revFinal));
         cargaPlanta.addElement(new Float(produccion - (conProblemas - revFinal)));
         cargasDePlanta.addElement(cargaPlanta);
         rs.close();
         st.close();
         conMAC.close();
  }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class "," ERROR cargaPlanta() " +ex.getMessage(),0); 
         }
    return cargasDePlanta;
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------------
  public Vector degloceMontoBodega(String fechaIni,String fechaFin){
        Vector cantidadEquipos = new Vector();
        try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
        
          String query1 = " SELECT tp.Pedido,tp.Linea,tp.FechaCli,tp.FechaBodega,tp.Cliente,tp.Monto,tp.Serie,tp.KVA "
                         +" FROM ENSchaffner.dbo.TPMP tp "
                         +" WHERE  tp.FechaBodega between '" + fechaIni + "' AND '" + fechaFin + "' "
                         +" and tp.Activa = 0  ";
          rs = st.executeQuery(query1);
             while(rs.next()){
                 Equipo equipo = new Equipo();
                 equipo.setNp(rs.getInt(1));
                 equipo.setLinea(rs.getInt(2));
                 equipo.setFechaCliente(rs.getDate(3));
                 equipo.setFecha1(rs.getDate(4));
                 equipo.setNombreCliente(rs.getString(5));
                 equipo.setMonto(rs.getFloat(6));
                 equipo.setSerie(rs.getString(7));
                 equipo.setKva(rs.getString(8));
                 cantidadEquipos.addElement(equipo);
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR degloceMontoBodega() " +ex.getMessage(),0);   
         }
          return cantidadEquipos;
     }
//---------------------------------------------------------------------------------------------------------------------------------------------------------
  public String regresarFechaMontoBodega(String dia,int ano ,String mes,int y,String dia1){
    String fecha  = "";
        String fecha1 = "";
        String mes1   = mes;
        int ano1      = ano;
        calendario = new GregorianCalendar();
         try{
           if(y==0){
         if(Integer.parseInt(dia) > 7){
            dia = "1";
            }
    }
    if(y==4){
            if(Integer.parseInt(dia1) < 7){
               calendario.set( Calendar.MONTH, Integer.parseInt(mes));  
               dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE)); 
            }
         }
    if(y==5){
            if(Integer.parseInt(dia1) < 7){
                calendario.set( Calendar.MONTH, Integer.parseInt(mes));  
                dia1 = String.valueOf(calendario.getActualMaximum(Calendar.DATE));
            }
         }
    if(String.valueOf(Integer.parseInt(mes)+1).length() == 1){
    mes = "0".concat(String.valueOf(Integer.parseInt(mes)+1));
    }else{
     mes = String.valueOf(Integer.parseInt(mes)+1);   
    }
    if(String.valueOf(Integer.parseInt(mes1)+1).length() == 1){
    mes1 = "0".concat(String.valueOf(Integer.parseInt(mes1)+1));
    }else{
    mes1 = String.valueOf(Integer.parseInt(mes1)+1);    
    }
    if(dia.length() == 1){
    dia = "0".concat(dia); 
    }
    if(dia1.length() == 1){
    dia1 = "0".concat(dia1); 
    }
    fecha  = String.valueOf(ano)+mes+dia;
    fecha1 = String.valueOf(ano1)+mes1+dia1;    
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR regresarFecha() " +ex.getMessage(),0);   
         }
        return fecha+fecha1;
   }
//-----------------------------------------------------------------------------------------------------------------------------------------  
  public String observacionRepreoceso(int np, int linea){
        String observacionRepreoceso = "";
         try{
         conMAC = mConexion.getMACConnector();
         st = conMAC.createStatement();
         String query = "select tlp.Observacion "
                       +" from ENSchaffner.dbo.TLaboratorio tlp "
                       +" where NumeroDocumento = (select max(tl.NumeroDocumento) "
                       +" from ENSchaffner.dbo.TLaboratorio tl "
                       +" where tl.Pedido = " + np + " and tl.Linea = " + linea + ") " 
                       +" and tlp.Pedido = " + np + " and tlp.Linea = " + linea + " ";
         rs = st.executeQuery(query);
             while(rs.next()){
                 observacionRepreoceso = rs.getString(1);
                 if(observacionRepreoceso == null || observacionRepreoceso.equals("")){
                    observacionRepreoceso = "Sin observaci�n"; 
                 }
                 
             }
         rs.close();
         st.close();
         conMAC.close();
         }catch(Exception ex){
               new Loger().logger("CalidadDeProgramacionBean.class ","ERROR observacionRepreoceso() " +ex.getMessage(),0);   
         }
          return observacionRepreoceso;
     }
}