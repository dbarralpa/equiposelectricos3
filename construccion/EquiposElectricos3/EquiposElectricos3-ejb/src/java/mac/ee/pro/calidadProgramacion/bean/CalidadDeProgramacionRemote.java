
package mac.ee.pro.calidadProgramacion.bean;

import javax.ejb.EJBObject;


/**
 * This is the remote interface for CalidadDeProgramacion enterprise bean.
 */
public interface CalidadDeProgramacionRemote extends EJBObject, CalidadDeProgramacionRemoteBusiness {
    
    
}
