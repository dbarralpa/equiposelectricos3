
package mac.ee.pro.calidadProgramacion.bean;

import java.rmi.RemoteException;
import javax.ejb.CreateException;
import javax.ejb.EJBHome;


/**
 * This is the home interface for CalidadDeProgramacion enterprise bean.
 */
public interface CalidadDeProgramacionRemoteHome extends EJBHome {
    
    CalidadDeProgramacionRemote create()  throws CreateException, RemoteException;
    
    
}
