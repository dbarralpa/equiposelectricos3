<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.mrp.clase.BitacoraVales,mac.ee.pla.mrp.bean.*,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css" />
        <title>Manufactura asistida por computadora</title>
        <script>
               function form_click() {
                 var myForm = document.form1;
                 myForm.observacion.focus();
            }
            function vali(){
            if(document.form1.observacion.value == ""){
              alert('Debe agregar una observaci�n');
              document.form1.observacion.focus();
              return false;
            }
         }
        </script>
    </head>
    <body onload="form_click()">
        <form action="observacionesPedidoMateriales.jsp" method="post" name="form1">
            <%              
            try{
                HttpSession sesion    = request.getSession();
                ConexionEJB cal = new ConexionEJB();
                Object ref =cal.conexionBean("MrpBean");
                MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
                MrpRemote mrp= mrpHome.create();%>
            <%if(request.getParameter("guardar") == null){
            sesion.setAttribute("material",request.getParameter("material"));
            sesion.setAttribute("numero",request.getParameter("numero"));
            sesion.setAttribute("estatu",request.getParameter("status"));
            sesion.setAttribute("type1",request.getParameter("type1"));
            sesion.setAttribute("descri",request.getParameter("descripcion"));
            sesion.setAttribute("usu",request.getParameter("usuario"));
            }
            Vector vales = new Vector();
            if(request.getParameter("guardar") != null){
            if(((String)sesion.getAttribute("estatu")).equals("2")){
            int veri = mrp.insertarObservacionDeVales(Integer.parseInt((String)(sesion.getAttribute("numero"))),
            (String)sesion.getAttribute("material"),
            request.getParameter("observacion"),
            Byte.parseByte((String)sesion.getAttribute("type1")),
            (String)sesion.getAttribute("usu"));%> 
            <%if(veri == 1){%>
            <center><h5><font class="Estilo10">Grab&oacute; los datos en la base de datos</h5></center>
            <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center>
            <%}else{%>
            <center><h5><font class="Estilo10">No grab� los datos en la base de datos</h5></center>
            <%}}else{   
            for(int i=0;i<((Vector)sesion.getAttribute("necesidades")).size();i++){
            if(((EstructuraPorEquipo)((Vector)sesion.getAttribute("necesidades")).elementAt(i)).getCodigoMatPrima().equals((String)sesion.getAttribute("material"))){
            ((EstructuraPorEquipo)((Vector)sesion.getAttribute("necesidades")).elementAt(i)).setObservacion(request.getParameter("observacion"));
            }
            }%>
            <br>
            <center><h5><font class="Estilo10">Dato Actualizado</h5></center>       
            <%}}%>
            
            <%if(((String)sesion.getAttribute("estatu")).equals("2")){
            vales = mrp.bitacoraVales(Integer.parseInt((String)sesion.getAttribute("numero")),(String)sesion.getAttribute("material"));
            }
            
            %>
            
            <br>
            <center><h5><font class="Estilo10">Bitacora de vales</h5></center>
            <br>
            <center class="Estilo10"><h5>Material:&nbsp;&nbsp;<%=(String)sesion.getAttribute("material")%>&nbsp;&nbsp;&nbsp;&nbsp;Descripci&oacute;n:&nbsp;&nbsp;<%=(String)sesion.getAttribute("descri")%></h5></center>
            <br>
            <table cellpadding="0" cellspacing="0" border="0" align="center">
                <tr>
                    <td> 
                        <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center" width="90%">
                            
                            <tr>
                                <td class="Estilo10" width="80">Pedido</td>
                                <td class="Estilo10" width="80">Fecha</td>
                                <td class="Estilo10" width="80">Usuario</td>
                                <td class="Estilo10">Obs. usuario</td>
                            </tr>
                            <%for(int i=0;i<vales.size();i++){%>
                            <%--if(((BitacoraVales)vales.elementAt(i)).getEstado() == (byte)1){--%>
                            <tr bgcolor="#FFFFCC">
                                <td class="Estilo10" width="80"><%=sesion.getAttribute("numero")%></td>
                                <td class="Estilo10" width="80"><%=((BitacoraVales)vales.elementAt(i)).getFecha()%></td>
                                <td class="Estilo10" width="80"><%=((BitacoraVales)vales.elementAt(i)).getUsuario()%></td>
                                <td class="Estilo10"><div align="justify"><%=((BitacoraVales)vales.elementAt(i)).getObservacion()%></div></td>
                            </tr>
                            <%--}--%><%}%>
                            
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" height="40">&nbsp;</td>
                </tr>
                <!--tr>
                    <td>
                        <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc  align="center" width="90%">
                            <tr>
                                <td class="Estilo10" width="80">Pedido</td>
                                <td class="Estilo10" width="80">Fecha</td>
                                <td class="Estilo10" width="80">Usuario</td>
                                <td class="Estilo10">Obs. bodega</td>
                            </tr>
                            <%--for(int i=0;i<vales.size();i++){%>
                            <%if(((BitacoraVales)vales.elementAt(i)).getEstado() == (byte)2){%>
                            <tr bgcolor="#FFFFCC">
                                <td class="Estilo10" width="80"><%=((BitacoraVales)vales.elementAt(i)).getPedido()%></td>
                                <td class="Estilo10" width="80"><%=((BitacoraVales)vales.elementAt(i)).getFecha()%></td>
                                <td class="Estilo10" width="80"><%=((BitacoraVales)vales.elementAt(i)).getUsuario()%></td>
                                <td class="Estilo10"><%=((BitacoraVales)vales.elementAt(i)).getObservacion()%></td>
                            </tr>
                            <%}}--%>
                            
                        </table>
                    </td>
                </tr-->
                <tr>
                    <td colspan="4" height="60">&nbsp;</td>
                </tr>
                <tr>
                    <td> 
                        <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                            <tr bgcolor="#B9DDFB">
                                <td class="Estilo10" colspan="2" align="center">Observaci&oacute;n</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><textarea name="observacion" cols="60" rows="2"></textarea></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br><br>
            <center>
               <%if(((String)sesion.getAttribute("type1")).equals("1")){%>
               <input type="submit" name="guardar" class="Estilo10" onclick="return vali();" value="Enviar"> 
                <!--input type="image" name="guardar"  onclick="return vali();" onmouseover="this.src = '../../Images/enviarObs1.jpg'" onmouseout="this.src = '../../Images/enviarObs.jpg'"   src="../../Images/enviarObs.jpg" width="140" height="20" alt="Enviar observaci&oacute;n" border="0"/-->
                <%}else{%>
                <input type="submit" name="guardar" disabled class="Estilo10"  value="Enviar"> 
                <!--input type="image" name="guardar"  disabled   src="../../Images/enviarObs.jpg" width="140" height="20" alt="Enviar observaci&oacute;n" border="0"/-->
                <%}%>
            </center>
        </form><br>
        
        
        <%}catch(Exception e){e.printStackTrace();}%>
    </body>
</html>