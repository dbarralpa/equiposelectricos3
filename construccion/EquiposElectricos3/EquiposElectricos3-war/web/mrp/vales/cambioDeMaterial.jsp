<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%try{
    HttpSession sesion    = request.getSession(); 
    ConexionEJB cal = new ConexionEJB();
    Object ref =cal.conexionBean("MrpBean");
    MrpRemoteHome mrpBeanHome = (MrpRemoteHome)PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
    MrpRemote mrp = mrpBeanHome.create();
    if(request.getParameter("estado") == null){
    //sesion.setAttribute("mate",mrp.materiaPrima());
    sesion.setAttribute("material",request.getParameter("material")); 
    sesion.setAttribute("numDoc",request.getParameter("numDoc")); 
    sesion.setAttribute("linea",request.getParameter("linea"));
    sesion.setAttribute("usus",request.getParameter("usuario"));
    sesion.setAttribute("num",request.getParameter("numPedido"));
    sesion.setAttribute("cantPedi",request.getParameter("cantidad"));
    }   
    
    String descripcion = "";
    String codigo = "";
    String um = "";
    float stock = 0f;
    int tipo = 0;
    int contador = 0;%>
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <link rel="stylesheet" type="text/css" href="../../CSS/search.css"/>
        <script type="text/javascript" src="../../js/ajax_search.js"></script> 
        <script>
           function actuali(){
          if(confirm('�Seguro que desea proseguir con la operaci�n?')){
            form1.submit();
          }else{
          return false;
          }
       }
       function validar(){
             form1.submit();
            }
        </script>
    </head>
    <body  leftmargin="0" marginheight="0" marginwidth="0" onload="asignaVariables();">
        
        
        <form name="form1" method="post">
            <br>
            <%if(request.getParameter("mater") != null && !request.getParameter("mater").equals("")){
            Vector buscar = mrp.buscarPorDescripcion(request.getParameter("mater"));     
            descripcion = ((EstructuraPorEquipo)buscar.elementAt(0)).getDescripcion();
            codigo = ((EstructuraPorEquipo)buscar.elementAt(0)).getCodigoMatPrima();
            um = ((EstructuraPorEquipo)buscar.elementAt(0)).getUnidadMedida();
            tipo = ((EstructuraPorEquipo)buscar.elementAt(0)).getTipoProducto();
            stock = mrp.stockBodega(codigo);
            }
            
            %>
            <center>
                <center><h5><font class="Estilo10">Buscar por descripci&oacute;n</font></h5></center>
            </center>
            <center>
                <div id="demo">
                    <div id="demoDer">
                        <input type="text" id="input_2" class="input" size="45" name="mater"
                               onfocus="if(document.getElementById('lista').childNodes[0]!=null && this.value!='') {
                               document.getElementById('lista').style.display='block'; }"
                               onblur="if(v==1) document.getElementById('lista').style.display='none';"
                               onkeyup="if(navegaTeclado(event)==1) {setTimeout('rellenaLista()', 500);}">
                        <div id="lista" onmouseout="v=1;" onmouseover="v=0;"></div>
                    </div>
                    <div class="mensaje" id="error"></div>
                </div>
            </center>
            <center>
                <input type="button" name="enviar" value="Agregar" onclick="validar();" class="Estilo10">         
            </center>
            <br>
            <table border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc" align="center">
                <tr bgcolor="#B9DDFB">
                    <td colspan="4" height="30" class="Estilo10" align="center">Cambiar Material</td>
                </tr>
                <tr class="Estilo10">
                    <td>codigo:&nbsp;<input type="text" name="codigo" size="12" class="Estilo10" value="<%=codigo%>" readonly></td>
                    <td>descri:&nbsp;<input type="text" name="descri" size="30" class="Estilo10" value="<%=descripcion%>" readonly></td>
                    <td>um:&nbsp;<input type="text" name="um" size="8" class="Estilo10" value="<%=um%>" readonly></td>
                    <td>stock:&nbsp;<input type="text" name="stock" size="8" class="Estilo10" value="<%=stock%>" readonly></td>
                </tr>
                <tr>
                    <td height="60" colspan="4" align="center" bgcolor="white"> 
                        <%if(request.getParameter("mater") != null){%>
                        <input type="submit" name="actualizar" class="Estilo10" onclick="return actuali();" value="Agregar">  
                        <%}else{%>
                        <input type="submit" name="actualizar" class="Estilo10" disabled  value="Agregar">
                        <%}%>
                    </td>
                </tr>   
            </table>
            <input type="hidden" name="tipo" value="<%=tipo%>">
            <%if(request.getParameter("actualizar") != null){
            if(Float.parseFloat(request.getParameter("stock")) >= Float.parseFloat(((String)sesion.getAttribute("cantPedi")))){
            boolean estado = mrp.actualizarPedidoMateriales((String)sesion.getAttribute("material"),(String)sesion.getAttribute("linea"),request.getParameter("codigo"),(String)sesion.getAttribute("numDoc"),request.getParameter("um"),request.getParameter("descri"),(String)sesion.getAttribute("usus"),(String)sesion.getAttribute("num"),request.getParameter("tipo"));    
            if(estado){%>
            <center><h5><font class="Estilo10">Actauliz&oacute; el material</font></h5></center>
            <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center>
            <%}else{%>
            <center><h5><font class="Estilo10">No se pudo actualizar el dato</font></h5></center>
            <%}}else{%>
            <script>
                 alert('No se puede agregar porque no hay stock suficiente');
            </script>   
            <%}}%>
            
            
            
            <%--if(request.getParameter("actualizar") != null){
         if(((String)sesion.getAttribute("status")).equals("2")){
         float stock = mrp.stockBodega(request.getParameter("codigo"));
         byte very = mrp.insertarPedidoMateriales(request.getParameter("codigo"),
                                                  Float.parseFloat(request.getParameter("cantidad")),
                                                  Integer.parseInt((String)sesion.getAttribute("numero")),
                                                  (String)sesion.getAttribute("usuario"),
                                                  request.getParameter("um"),
                                                  Byte.parseByte((String)sesion.getAttribute("size")),
                                                  (byte)tipo
                                                  );%>
          <%if(very == 1){%>
                  <center><h5><font class="Estilo10">Grab&oacute; los datos en la base de datos</font></h5></center>
                  <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center>
                  <%}else{%>
                    <center><h5><font class="Estilo10">No grab� los datos en la base de datos</font></h5></center>
                  <%}%>                 
          
         <%}else{
         float stock = mrp.stockBodega(request.getParameter("codigo"));
         EstructuraPorEquipo estructura = new EstructuraPorEquipo();
         estructura.setCantidad(Float.parseFloat(request.getParameter("cantidad")));
         estructura.setUnidadMedida(request.getParameter("um"));
         estructura.setCodigoMatPrima(request.getParameter("codigo"));
         estructura.setDescripcion(request.getParameter("descri"));
         estructura.setStockBodega(stock);
         estructura.setObservacion("");
         estructura.setStockPlanta(0f);
         ((Vector)sesion.getAttribute("necesidades")).addElement(estructura);%>
         <center><h5><font class="Estilo10">Dato Actualizado</font></h5></center>     
         <!--center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center-->
         <%}}--%>   
            <input type="hidden" name="estado">
        </form>
    </body>
    <%}catch(Exception e){}%>
</html>