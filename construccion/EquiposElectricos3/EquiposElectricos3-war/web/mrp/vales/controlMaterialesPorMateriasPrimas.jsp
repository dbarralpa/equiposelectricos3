<%@page contentType="text/html" import="mac.ee.pla.mrp.bean.MrpBean,java.text.*,java.util.Vector,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/tablas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
    </head>
    <body>
        <%
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        MrpBean mrp = new MrpBean();
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        NumberFormat number = NumberFormat.getInstance();
        number.setMaximumFractionDigits(2);
        Vector pedidos = mrp.controlDeCostoPorMateriales(request.getParameter("fechaIni"),request.getParameter("fechaFin"));
        sesion.setAttribute("toExcel",pedidos);
        float cantReal = 0f;
        float cantPtdo = 0f;
        float costoReal = 0f;
        float ptdoReal = 0f;
        if(idSesion.equals(sesion.getId())){
        %>
        <form method="post" action="controlMaterialesPorMateriasPrimas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">
            <table width="654" border="0" align="center" cellpadding="0" cellspacing="0" class="Estilo10">
                <tr>
                    <td height="53" colspan="7" align="center" valign="middle">MATERIALES PEDIDOS A BODEGA </td>
                </tr>
                <tr>
                    <td height="72" colspan="7" valign="top"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <!--DWLayoutTable-->
                            <tr>
                                <td width="306"  align="right" valign="middle">Fecha inicio:&nbsp;</td>
                                <td width="97" valign="middle">
                                    <DIV id="popCal" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                    <IFRAME name="popFrame" src="../../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                                    <SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
                                    <TABLE align="LEFT" CELLPADDING="0" CELLSPACING="0">
                                        <TR>
                                            <TD ALIGN="center">
                                                <input class="Estilo10" readOnly name="fechaIni" id="fechaIni" size="12"  <%if(request.getParameter("fechaIni") != null && !request.getParameter("fechaIni").equals("")){%>value="<%=request.getParameter("fechaIni")%>"<%}%>>
                                                <a onclick="popFrame.fPopCalendar(fechaIni,fechaIni,popCal);return false">
                                                <img src="../../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                            </td>
                                        </tr>
                                    </table>     
                                </td>
                                <td width="87" rowspan="2" align="center" valign="middle">
                                    <input type="submit" name="Submit" value="Enviar" class="Estilo10" />       
                                </td>
                                <td width="160" rowspan="2" valign="top">&nbsp;</td>
                            </tr>
                            <tr>
                                <td height="43" align="right" valign="middle">Fecha fin:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td valign="middle">
                                    <DIV id="popCal1" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                    <IFRAME name="popFrame1" src="../../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                                    <SCRIPT event=onclick() for=document>popCal1.style.visibility = "hidden";</SCRIPT>
                                    <TABLE align="LEFT" CELLPADDING="0" CELLSPACING="0">
                                        <TR>
                                            <TD ALIGN="center">
                                                <input class="Estilo10" readOnly name="fechaFin" id="fechaFin" size="12"  <%if(request.getParameter("fechaFin") != null && !request.getParameter("fechaFin").equals("")){%>value="<%=request.getParameter("fechaFin")%>"<%}%>>
                                                <a onclick="popFrame1.fPopCalendar(fechaFin,fechaFin,popCal1);return false">
                                                <img src="../../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                            </td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            
                            
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <table class="tableone" >
                <thead>
                    <tr>
                        <th width="114">C&oacute;digo</th>
                        <th width="189" >Descripci&oacute;n</th>
                        <th width="47" >Um</th>
                        <th width="103" >Cantidad real </th>
                        <th width="100" >Cantidad Ppda </th>
                        <th width="101" >Costo real </th>
                        <th width="96" >Costo Ppda </th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td colspan="8">
                            <div class="innerb">
                                <table class="tabletwo">
                                  <%for(int i=0;i<pedidos.size();i++){%>
                                    <tr class="dk">
                                        <td  width="114"><a href="javascript:Abrir_ventana5('detalleCostoPorMaterial.jsp?material=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getCodigoMatPrima()%>&fechaIni=<%=request.getParameter("fechaIni")%>&fechaFin=<%=request.getParameter("fechaFin")%>&costo=<%=nf.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getCantidad())%>&cantReal=<%=number.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getReal())%>&cantPpta=<%=number.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getPresupuesto())%>')"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getCodigoMatPrima()%></a></td>
                                        <td  width="189"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getDescripcion()%></td>
                                        <td  width="47"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getUnidadMedida()%></td>
                                        <td  width="103"><%=number.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getReal())%></td>
                                        <td  width="100"><%=number.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getPresupuesto())%></td>
                                        <td  width="101"><%=nf.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getReal1())%></td>
                                        <td  width="86"><%=nf.format(((EstructuraPorEquipo)pedidos.elementAt(i)).getPresupuesto1())%></td>
                                    </tr>
                                    <%
                                    cantReal += ((EstructuraPorEquipo)pedidos.elementAt(i)).getReal();
                                    cantPtdo += ((EstructuraPorEquipo)pedidos.elementAt(i)).getPresupuesto();
                                    costoReal += ((EstructuraPorEquipo)pedidos.elementAt(i)).getReal1();
                                    ptdoReal += ((EstructuraPorEquipo)pedidos.elementAt(i)).getPresupuesto1();
                                    }%>  
                                </table>
                        </div></td>
                    </tr>
                </tbody>
            </table>
            
            <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" >
                <tr>
                    <td height="22" width="350"  align="right" valign="middle">Totales</td>
                    <td width="103"><%=number.format(cantReal)%></td>
                    <td width="100"><%=number.format(cantPtdo)%></td>
                    <td width="101"><%=nf.format(costoReal)%></td>
                    <td width="86"><%=nf.format(ptdoReal)%></td>
                </tr>
            </table>
         <%
            sesion.setAttribute("real",number.format(cantReal));
            sesion.setAttribute("presupuesto",number.format(cantPtdo));
            sesion.setAttribute("real1",nf.format(costoReal));
            sesion.setAttribute("presupuesto1",nf.format(ptdoReal));
            %>    
        </form>
        <form method="post" name="form10" action="FormExcel.jsp?estado=1">
          <table align="center" border="0">
              <tr>      
                  <td><input type="image" src="../../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></td>
                  <td align="center"><a href="ControlDeCosto.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../../Images/volver2.jpg" border="0" width="89" height="18"></a></td>
              </tr>
          </table>
        </form>    
        <%}else{response.sendRedirect("../mac");}%>  
    </body>
</html>
