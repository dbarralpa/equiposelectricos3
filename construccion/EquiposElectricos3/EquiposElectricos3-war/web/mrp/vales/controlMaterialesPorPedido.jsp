<%@page contentType="text/html" import="java.text.*,java.util.Date,java.text.SimpleDateFormat,conexionEJB.ConexionEJB,java.util.Vector,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.crp2.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,javax.rmi.PortableRemoteObject"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Control de materiales por pedido</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
    </head>
    <body>
        <%
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        NumberFormat number = NumberFormat.getInstance();
        number.setMaximumFractionDigits(0);
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        if(idSesion.equals(sesion.getId())){
        %>
        
        <%
        try{
            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
            ConexionEJB cal = new ConexionEJB();
            Object ref1 =cal.conexionBean("Crp2Bean");
            Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
            Crp2Remote crp = crpHome.create();
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            float cantidad = 0f;
            float presupuesto = 0f;
            float real = 0f;
            float diferencia = 0f;
            float presupuesto1 = 0f;
            float real1 = 0f;
            float diferencia1 = 0f;
            //Vector pedidos = crp.controlMaterialesPorPedido(request.getParameter("pedido"),request.getParameter("linea"));
            Vector materialesPorEquipo = mrp.materialesPorEquipo(request.getParameter("estructura"),request.getParameter("pedido"),request.getParameter("linea"));
            sesion.setAttribute("toExcel",materialesPorEquipo);
        %>
        
        
        <form method="post" name="form1" action="FormExcel.jsp?estado=0">
            
            <table width="810" border="0" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td height="26" colspan="7" valign="middle" class="Estilo10" align="center"> 
                        
                        <h5>Control de Materiales por Nota de Pedido</h5>
                    </td>
                </tr>
                
                <tr>
                    <td height="35" colspan="3" valign="top" align="right"><input type="image" src="../../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></td>
                    <td colspan="3" align="right" valign="middle" class="Estilo10">Fecha informe: </td>
                    <td width="118" align="right" valign="middle" class="Estilo10"><%=format.format(new Date())%></td>
                </tr>
                <tr>
                    <td width="106" height="24" align="left" valign="middle" class="Estilo10">Nota de pedido: </td>
                    <td colspan="6" valign="middle" class="Estilo10"><%=request.getParameter("pedido")%>/<%=request.getParameter("linea")%></td>
                </tr>
                <tr>
                    <td height="24" valign="middle" class="Estilo10">Cliente:</td>
                    <td width="290" valign="middle" class="Estilo10"><%=request.getParameter("cliente")%></td>
                    <td width="37" valign="middle" class="Estilo10">&nbsp;</td>
                    <td width="63" valign="middle" class="Estilo10">Familia:&nbsp;&nbsp;<%=request.getParameter("familia")%></td>
                    <td colspan="2" valign="middle" class="Estilo10">&nbsp;</td>
                </tr>
                <tr>
                    <td height="24" valign="middle" class="Estilo10">Avance:</td>
                    <td colspan="6" valign="middle" class="Estilo10"><%=request.getParameter("status")%></td>
                </tr>
                <tr>
                    <td height="19" colspan="7" valign="top">&nbsp;</td>
                </tr>
                <tr>
                    <td height="247" colspan="7" valign="top">
                        <table width="810" border="1" cellpadding="0" cellspacing="0" class="Estilo10">
                            <tr>
                                <td width="85" rowspan="2" valign="middle" class="Estilo10" align="center">Material</td>
                                <td width="130" rowspan="2" valign="middle" class="Estilo10" align="center">Descripci&oacute;n</td>
                                <td width="50" rowspan="2" valign="middle" class="Estilo10" align="center">UM</td>
                                <td width="89" rowspan="2" valign="middle" class="Estilo10" align="center">P.Unitario</td>
                                <td height="19" colspan="3" align="center" valign="middle" class="Estilo10">Cantidad</td>
                                <td colspan="3" align="center" valign="middle" class="Estilo10">Monto $ </td>
                            </tr>
                            <tr>
                                <td width="76" height="21" align="center" valign="middle" class="Estilo10">Ppto.</td>
                                <td width="70" align="center" valign="middle" class="Estilo10">Real</td>
                                <td width="79" align="center" valign="middle" class="Estilo10">Diferencia</td>
                                <td width="76" valign="middle" class="Estilo10" align="center">Ppto.</td>
                                <td width="70" valign="middle" class="Estilo10" align="center">Real</td>
                                <td width="85" valign="middle" class="Estilo10" align="center">Diferencia</td>
                            </tr>
                            <%for(int i=0;i<materialesPorEquipo.size();i++){%>
                            <tr>
                                <td valign="middle" align="center"  class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getCodigoMatPrima()%></td>
                                <%if(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDescripcion().length() > 18){%>
                                <td valign="middle"  align="center" title="<%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDescripcion()%>"  class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDescripcion().substring(0,17)%></td>
                                <%}else{%>
                                <td valign="middle"  align="center" class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDescripcion()%></td>
                                <%}%>
                                <td valign="middle" align="center" class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getUnidadMedida()%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=number.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getCantidad())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getPresupuesto())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getReal())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDiferencia())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=number.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getPresupuesto1())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=number.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getReal1())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=number.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDiferencia1())%></td>
                            </tr>
                            <%
                            cantidad +=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getCantidad();
                            presupuesto += ((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getPresupuesto();
                            real += ((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getReal();
                            diferencia += ((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDiferencia();
                            presupuesto1 += ((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getPresupuesto1();
                            real1 += ((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getReal1();
                            diferencia1 += ((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getDiferencia1();
                            }%>
                            <tr>
                                <td height="26" valign="middle" class="Estilo10">Totales</td>
                                <td valign="middle">&nbsp;</td>
                                <td valign="middle">&nbsp;</td>
                                <td valign="middle"><%=number.format(cantidad)%></td>
                                <td valign="middle"><%=nf.format(presupuesto)%></td>
                                <td valign="middle"><%=nf.format(real)%></td>
                                <td valign="middle"><%=nf.format(diferencia)%></td>
                                <td valign="middle"><%=number.format(presupuesto1)%></td>
                                <td valign="middle"><%=number.format(real1)%></td>
                                <td valign="middle"><%=number.format(diferencia1)%></td>
                            </tr>
                        </table> 
                    </td>
                </tr>
                <tr>
                    <td height="1"></td>
                    <td></td>
                    <td></td>
                    <td width="233"></td>
                    <td></td>
                </tr>
            </table>
            <%
            sesion.setAttribute("cantidad",number.format(cantidad));
            sesion.setAttribute("presupuesto",nf.format(presupuesto));
            sesion.setAttribute("real",nf.format(real));
            sesion.setAttribute("diferencia",nf.format(diferencia));
            sesion.setAttribute("presupuesto1",number.format(presupuesto1));
            sesion.setAttribute("real1",number.format(real1));
            sesion.setAttribute("diferencia1",number.format(diferencia1));
            %>
            
        </form>
        <%}catch(Exception e){
        new sis.logger.Loger().logger("controlMaterialesPorPedido.jsp " , " controlMaterialesPorPedido() "+e.getMessage(),0);
        }%>
        <%}else{response.sendRedirect("../mac");}%>  
    </body>
</html>
