<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.crp2.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
    HttpSession sesion    = request.getSession();
    String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    if(idSesion.equals(sesion.getId())){
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <meta http-equiv="refresh" content="180;URL=javascript:window.close()">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
        <script>
           function actualiza(valor){
            if(confirm('�Seguro que desea proseguir con la operaci�n?')){
              form1.submit();
         }else{
           valor.checked = false; 
         }
            
      }
      
      function eliminarMaterial(valor,nombre){
      alert(nombre);
            var i;
            var a = 0;
            for(i=0; i<valor; i++){
            var eliminar = "eliminar"+i;
               if(document.getElementById(eliminar).checked == true){
                  a = 1;
               }
            }
               if(a==0){
               alert('Debe seleccionar un material');
               return false;
               }else{
               form1.submit();
               }
           }
           function actuali(valor){
              if(confirm('�Seguro que desea proseguir con la operaci�n?')){
                //alert(valor.value);
                document.form1.estado1.value = valor.value; 
                form1.submit();
              }else{
              valor.checked = false; 
              }
   }
   function actuali1(valor){
              if(confirm('�Seguro que desea proseguir con la operaci�n?')){
                //alert(valor.value);
                document.form1.estado2.value = valor.value; 
                form1.submit();
              }else{
              valor.checked = false; 
              }
   }
 
      
   </script>
   <style type="text/css" media="all">
            td.uno {
            border:hidden;
            }
            table.dos {
            border: 1px solid #999999;
            }
        </style>
    </head>
    <body>
        <form name="form1" method="post" action="pedidos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">
            <%try{
            
            ConexionEJB cal = new ConexionEJB();  
            Object ref1 =cal.conexionBean("Crp2Bean");
            Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
            Crp2Remote crp = crpHome.create();
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            if(request.getParameter("estado1") == null){
            sesion.setAttribute("equipos",crp.equiposPorPedido(Integer.parseInt(request.getParameter("np")),Integer.parseInt(request.getParameter("linea")),request.getParameter("proceso"),request.getParameter("numero")));
            sesion.setAttribute("np",request.getParameter("np"));    
            sesion.setAttribute("line",request.getParameter("linea"));    
            sesion.setAttribute("proce",request.getParameter("proceso"));    
            sesion.setAttribute("numero",request.getParameter("numero"));    
            }
            if(request.getParameter("prioridad1") != null && !request.getParameter("prioridad1").equals("nada")){
            mrp.cambiarPrioridad(request.getParameter("prioridad1")); 
            sesion.setAttribute("equipos",crp.equiposPorPedido(Integer.parseInt((String)sesion.getAttribute("np")),Integer.parseInt((String)sesion.getAttribute("line")),(String)sesion.getAttribute("proce"),(String)sesion.getAttribute("numero")));    
            }
            if(request.getParameter("borrar") != null){
            int valor = mrp.borrarPedido(Integer.parseInt(request.getParameter("borrar")));
            if(valor == 1){
            sesion.setAttribute("equipos",crp.equiposPorPedido(Integer.parseInt((String)sesion.getAttribute("np")),Integer.parseInt((String)sesion.getAttribute("line")),(String)sesion.getAttribute("proce"),(String)sesion.getAttribute("numero")));    
            }
            }
            Vector equipos = (Vector)sesion.getAttribute("equipos");
            
            if(request.getParameter("estado1") != null && request.getParameter("estado1").length() > 0){
            mrp.eliminarItem(request.getParameter("estado1"));
            }
            if(request.getParameter("estado2") != null && request.getParameter("estado2").length()>0){
            boolean verificar = crp.aprobarPedidos(request.getParameter("aprobar"),request.getParameter("prioridad"),usuario); 
            if(verificar){
            equipos = new Vector();    
            %>
            <script>
            alert('Pedido aprobado');
            </script>
            <%}}
            %>
            <br>
            <center><h5><font class="Estilo10">SISTEMA DE ADMINISTRACI&Oacute;N DE PEDIDO DE MATERIALES</font></h5></center>
            
            
            <br>
            
            <%
            if(equipos.size() > 0){
                for(int i=0;i<equipos.size();i++){%>
            
            <%Vector pedidosPorItem = mrp.pedidosPorItem(((Equipo)equipos.elementAt(i)).getNumPedido());%>
            <%Vector equiposPorItem = crp.equiposPorItem(((Equipo)equipos.elementAt(i)).getNumPedido());%>
            
            <br>
            <table width="90%" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td>    
                        <table border=0 cellspacing=0 cellpadding=0 align="left">
                            <tr align="center">
                                <td align="center" class="Estilo10" width="80" height="20">Np</td>
                                <td align="center" class="Estilo10" width="80" height="20">Linea</td>
                                <td class="Estilo10" align="center" width="80" height="20">Serie</td>
                                <td class="Estilo10" align="center" width="80" height="20">Familia</td>
                                <td align="center" class="Estilo10" width="80" height="20">N� Pedido</td>
                            </tr>
                            <tr>
                                <td colspan="5" valign="top">
                                    <table  class="dos" cellpadding="0" cellspacing="0">
                                        <%for(int h=0;h<equiposPorItem.size();h++){%>
                                        <tr><!--"#d51f2c"color rojo-->
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getNp()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getLinea()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getSerie()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getFamilia()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equipos.elementAt(i)).getNumPedido()%></td>
                                        </tr>
                                        
                                        <%}%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" align="right" class="Estilo10">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="Estilo10" align="center" width="80" height="20">Usuario:</td>
                                <td class="Estilo10" align="center" width="80" height="20">&Aacute;rea:</td>
                                <td class="Estilo10" align="center" width="80" height="20">Prioridad:</td>
                                <td class="Estilo10" align="center" width="140" height="20">Estado:</td>
                            </tr>
                            <tr>
                                <td colspan="4" valign="top">
                                    <table  class="dos">
                                        <tr>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario()%></td>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getProceso()%></td>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getDescripcionEstado()%></td>
                                            <td class="Estilo10" align="center" width="140"><%=((Equipo)equipos.elementAt(i)).getStatus()%></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>    
            <br><br>
            
            <table border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc" align="center" width="90%">
                <tr bgcolor="#B9DDFB" align="center">
                    <td align="center" class="Estilo10" width="80">Codigo</td>
                    <td align="center" class="Estilo10" width="80">Descripcion</td>
                    <td align="center" class="Estilo10" width="80">Um</td>
                    <td align="center" class="Estilo10" width="80">Cantidad Pedida</td>
                    <td align="center" class="Estilo10" width="80">Stock bodega</td>
                    <td align="center" class="Estilo10" width="80">Cantidad Entregada</td>
                    <td align="center" class="Estilo10" width="80">Observaci&oacute;nes</td>
                    <td align="center" class="Estilo10" width="80">Eliminar</td>
                </tr>
                <%for(int u=0;u<pedidosPorItem.size();u++){%>
                <%if((((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getStockBodega() + ((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getStockPlanta()) < ((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()){%>
                <tr bgcolor="#FF0000"><!--"#d51f2c"color rojo-->
                <%}else{%>
                <tr bgcolor="#FFFFCC"><!--"#d51f2c"color rojo-->
                    <%}%>
                    <%if(((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getEstado() < 3 && (((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario().equals(usuario) || mrp.validarUsuarioALink(usuario,"supervisorVales") || mrp.validarUsuarioALink(usuario,"ingVales"))){%>
                    <%if(mrp.validarUsuarioALink(usuario,"ingVales")){%>
                    <td align="center" onclick=""><font class="Estilo9"><a href="javascript:Abrir_ventana('cambioDeMaterial.jsp?material=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>&numDoc=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getNumDoc()%>&linea=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getLeadTime()%>&numPedido=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getNumPedido()%>&usuario=<%=usuario%>&cantidad=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()%>')"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%></a></font></td>
                    <%}else{%>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%></font></td>
                    <%}%>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getUnidadMedida()%></font></td>
                    <!--td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana('cantidadPedidoMateriales.jsp?material=<%--=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>&numero=<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>&status=2&type=1')"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()--%></a></font></td-->
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getStockBodega()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidadEntregada()%></font></td>
                    <%if(!((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getObservacion().equals("")){%>
                    <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana('observacionesPedidoMateriales.jsp?material=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>&numero=<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>&status=2&type1=1&descripcion=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%>&usuario=<%=usuario%>')"><img src="../../Images/observacion.jpg" width="18" height="16" border="0"></a></font></td>
                    <%}else{%>
                    <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana('observacionesPedidoMateriales.jsp?material=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>&numero=<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>&status=2&type1=1&descripcion=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%>&usuario=<%=usuario%>')"><img src="../../Images/observacionVacia.jpg" width="18" height="16" border="0"></a></font></td>         
                    <%}%>
                    
                    <%if(pedidosPorItem.size() > 1){%>
                    <td align="center" class="Estilo10" width="80"><input type="checkbox" name="eliminar<%=i%>" value="<%=((Equipo)equipos.elementAt(i)).getNumPedido()+"-"+((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>" onclick="actuali(this);"></td>
                    <%}else{%>
                    <td align="center" class="Estilo10" width="80"><input type="checkbox" name="eliminar<%=i%>" disabled></td>
                    <%}%>
                </tr>
                <%}else{%>
                <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%></font></td>
                <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%></font></td>
                <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getUnidadMedida()%></font></td>
                <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()%></font></td>
                <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getStockBodega()%></font></td>
                <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidadEntregada()%></font></td>
                
                
                <td align="center"><font class="Estilo9"><img src="../../Images/observacionVacia.jpg" width="18" height="16" border="0"></font></td>         
                
                <td align="center" class="Estilo10" width="80"><input type="checkbox" name="eliminar<%=i%>" disabled></td>
                
                <%}}%>
                
            </table>
            <br>
            <%if(((Equipo)equipos.elementAt(i)).getEstado() == 1){%>
            <%if(((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario().equals(usuario) || mrp.validarUsuarioALink(usuario,"supervisorVales") || mrp.validarUsuarioALink(usuario,"ingVales")){%>
            <%if(mrp.validarUsuarioALink(usuario,"supervisorVales")){%>
            <center  class="Estilo10">Aprobar pedido:&nbsp;<input type="checkbox" name="aprobar" onclick="actuali1(this);" value="<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>" class="Estilo10">
            &nbsp;&nbsp;&nbsp;Elija prioridad:&nbsp;<select class="Estilo10" name="prioridad">
                <option value="normal" <%if(request.getParameter("prioridad") != null && request.getParameter("prioridad").equals("normal")){%> selected<%}%>>Normal</option>
                <option value="urgente" <%if(request.getParameter("prioridad") != null && request.getParameter("prioridad").equals("urgente")){%> selected<%}%>>Urgente</option>
            </select>
            <%}else{%>
            <center class="Estilo10">Aprobar pedido:&nbsp;<input type="checkbox" name="aprobar"  class="Estilo10" disabled>&nbsp;&nbsp;&nbsp;
                                                                                                                               &nbsp;&nbsp;&nbsp;<select disabled class="Estilo10">
                    <option>Elija prioridad</option>
                </select>
                <%}%>
                &nbsp;&nbsp;&nbsp;<font class="Estilo10">Eliminar&nbsp;pedido&nbsp;&nbsp;&nbsp;</font><input type="checkbox" value="<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>" name="borrar" onclick="actualiza(this);">
                <%if(mrp.validarUsuarioALink(usuario,"ingVales")){%>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:Abrir_ventana('agregarMaterial.jsp?size=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(pedidosPorItem.size()-1)).getLeadTime()%>&proceso=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getProceso()%>&numero=<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>&status=2&usuario=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario()%>&usu=<%=usuario%>')">Agregar Material</a>                      
                <%}else{%>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar Material
                <%}%>
            </center>
            <%}}else if(((Equipo)equipos.elementAt(i)).getEstado() == 2){
            if(mrp.validarUsuarioALink(usuario,"supervisorVales")){%>
            <center class="Estilo10">Aprobar pedido:&nbsp;<input type="checkbox" name="aprobar"  class="Estilo10" disabled>&nbsp;&nbsp;&nbsp;
                                                                                                                           &nbsp;&nbsp;&nbsp;<select class="Estilo10" name="prioridad1" onchange="form1.submit()">
                <option value="nada">Elija prioridad</option>        
                <option value="normal-<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>" <%if(request.getParameter("prioridad1") != null && request.getParameter("prioridad1").equals("normal-"+((Equipo)equipos.elementAt(i)).getNumPedido())){%> selected<%}%>>Normal</option>
                <option value="urgente-<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>" <%if(request.getParameter("prioridad1") != null && request.getParameter("prioridad1").equals("urgente-"+((Equipo)equipos.elementAt(i)).getNumPedido())){%> selected<%}%>>Urgente</option>
            </select>
            &nbsp;&nbsp;&nbsp;<font class="Estilo10">Eliminar&nbsp;pedido&nbsp;&nbsp;&nbsp;</font><input type="checkbox" value="<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>" name="borrar" onclick="actualiza(this);">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar Material
            <%}else{%>
            <center class="Estilo10">Aprobar pedido:&nbsp;<input type="checkbox" name="aprobar"  class="Estilo10" disabled>
                    &nbsp;&nbsp;&nbsp;<select disabled class="Estilo10">
                    <option>Elija prioridad</option>
                </select>  
                &nbsp;&nbsp;&nbsp;Eliminar&nbsp;pedido&nbsp;&nbsp;&nbsp;<input type="checkbox" value="" name="borrar" disabled> 
                    <%if(mrp.validarUsuarioALink(usuario,"ingVales")){%>    
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:Abrir_ventana('agregarMaterial.jsp?size=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(pedidosPorItem.size()-1)).getLeadTime()%>&proceso=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getProceso()%>&numero=<%=((Equipo)equipos.elementAt(i)).getNumPedido()%>&status=2&usuario=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario()%>&usu=<%=usuario%>')">Agregar Material</a>                      
                <%}else{%>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar Material
                <%}%>
            </center>
            <%}%>
            <%}else{%>
            <center class="Estilo10">Aprobar pedido:&nbsp;<input type="checkbox" name="aprobar"  class="Estilo10" disabled>
                    &nbsp;&nbsp;&nbsp;<select disabled class="Estilo10">
                    <option>Elija prioridad</option>
                </select>  
                &nbsp;&nbsp;&nbsp;Eliminar&nbsp;pedido&nbsp;&nbsp;&nbsp;<input type="checkbox" value="" name="borrar" disabled> 
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Agregar Material
            </center>
            <%}%>  
            
            <br><br>
            <%}}else{%>
            <!--center><h5><font class="Estilo10">No hay pedidos para el proceso de este equipo</font></h5></center-->
            <br><br><br>
            <center><a href="javascript:window.opener.document.location.reload();self.close()">CERRAR VENTANA</a></center>
            <%}%>
            
            <input type="hidden" name="estado1" value=""> 
            <input type="hidden" name="estado2" value=""> 
            <%sesion.setAttribute("val",new Byte((byte)1));%>
            <%}catch(Exception e){
            new sis.logger.Loger().logger("pedidos.jsp " , " pedidos(): "+e.getMessage(),0);
            }%>
        </form>
    </body>
    <%}else{response.sendRedirect("../mac");}%>  
</html>