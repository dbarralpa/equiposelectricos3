<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.crp2.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.estructuraEquipo.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB,javax.swing.JOptionPane;"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
    HttpSession sesion    = request.getSession();
    String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    String on = "on";
    String entre = "1";
    if(idSesion.equals(sesion.getId())){
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
        <script>
            function actualiza(valor){
            if(confirm('�Seguro que desea proseguir con la operaci�n?')){
            form1.submit(); 
            }else{
            valor.checked = false; 
            }
      }
      function imprimirPagina() {
        this.location.href="javascript:Abrir_ventana2('jaspeReport.jsp')";  
     }

function entregado(){
            if(confirm('�Seguro que desea proseguir con la operaci�n?')){
            form1.submit(); 
            }
      }

        </script>
        <style type="text/css" media="all">
            td.uno {
            border:hidden;
            }
            table.dos {
            border: 1px solid #999999;
            }
        </style>
    </head>
    <body>
        <form name="form1" method="post">
            <%try{
            
            ConexionEJB cal = new ConexionEJB();  
            Object ref1 =cal.conexionBean("Crp2Bean");
            Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
            Crp2Remote crp = crpHome.create();
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            Object ref2 =cal.conexionBean("estructuraEquipoBean");
            estructuraEquipoRemoteHome estructuraEquipo = (estructuraEquipoRemoteHome) PortableRemoteObject.narrow(ref2, estructuraEquipoRemoteHome.class);
            estructuraEquipoRemote estructura = estructuraEquipo.create();
            java.text.SimpleDateFormat formateador = new java.text.SimpleDateFormat("yyyy-MM-dd");
            byte valor = 1;
            if(request.getParameter("estado1") == null){
            sesion.setAttribute("numero",request.getParameter("numero"));
            sesion.setAttribute("usua",usuario);
            }
            if(request.getParameter("enProceso") != null){
            mrp.actualizarEstado(3,Long.parseLong((String)sesion.getAttribute("numero")),"En proceso",usuario);
            }
            else if(request.getParameter("listo") != null){
            mrp.actualizarEstado(4,Long.parseLong((String)sesion.getAttribute("numero")),"Listo para entregar",usuario);
            }
            else if(request.getParameter("entregado") != null){
            mrp.actualizarEstado(5,Long.parseLong((String)sesion.getAttribute("numero")),"Entregado",usuario);   
            String parametrosJspOrigen = request.getQueryString(); 
            String urlDestino = "bodega.jsp?"+parametrosJspOrigen; 
            response.sendRedirect(urlDestino); 
            }
            
            if(request.getParameter("borrar") != null){
            int valor1 = mrp.borrarPedido(Long.parseLong((String)sesion.getAttribute("numero")));
            if(valor1 == 1){
            String parametrosJspOrigen = request.getQueryString(); 
            String urlDestino = "bodega.jsp?"+parametrosJspOrigen; 
            response.sendRedirect(urlDestino); 
               }else{%>
               <script>
                   alert("No se elimin"+'\u00f3'+"el pedido");
               </script> 
               <%}
            }
            %>
            
            
            <%Vector pedidosPorItem = mrp.pedidosPorItem(Long.parseLong((String)sesion.getAttribute("numero")));
            Vector equiposPorItem = crp.equiposPorItem(Long.parseLong((String)sesion.getAttribute("numero")));
            
            %>
            
            
            
            <br>
            <center><h4><font class="Estilo10">Detalle del pedido</font></h4></center>
            <center><a href="bodega.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../../Images/volver2.jpg" border="0"></a></center>
            <br>
            <table width="70%" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td>    
                        <table border=0 cellspacing=0 cellpadding=0 align="left">
                            <tr align="center">
                                <td align="center" class="Estilo10" width="80" height="20">Np</td>
                                <td align="center" class="Estilo10" width="80" height="20">Linea</td>
                                <td class="Estilo10" align="center" width="80" height="20">Serie</td>
                                <td class="Estilo10" align="center" width="80" height="20">Familia</td>
                                <td align="center" class="Estilo10" width="80" height="20">N� Pedido</td>
                            </tr> 
                            <tr>
                                <td colspan="5" valign="top">
                                    <table  class="dos">
                                        <%for(int h=0;h<equiposPorItem.size();h++){%>
                                        <tr><!--"#d51f2c"color rojo-->
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getNp()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getLinea()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getSerie()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getFamilia()%></td>
                                            <td align="center" class="Estilo10" width="80"><%=Long.parseLong((String)sesion.getAttribute("numero"))%></td>
                                        </tr>
                                        
                                        <%}%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" align="right" class="Estilo10">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="Estilo10" align="center" width="80" height="20">Usuario:</td>
                                <td class="Estilo10" align="center" width="80" height="20">&Aacute;rea:</td>
                                <td class="Estilo10" align="center" width="140" height="20">Estado:</td>
                                <td class="Estilo10" align="center" width="80" height="20">Prioridad:</td>
                            </tr>
                            <tr>
                                <td colspan="5" valign="top">
                                    <table  class="dos">
                                        <tr>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario()%></td>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getProceso()%></td>
                                            <td class="Estilo10" align="center" width="140"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getDescripcionEstado()%></td>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getPrioridad()%></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>    
            <br><br>
            <table border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc" align="center" width="70%">
                <tr bgcolor="#B9DDFB" align="center">
                    <td align="center" class="Estilo10" width="80">C&oacute;digo</td>
                    <td align="center" class="Estilo10" width="80">Descripci&oacute;n</td>
                    <td align="center" class="Estilo10" width="80">Um</td>
                    <td align="center" class="Estilo10" width="80">Pedida</td>
                    <td align="center" class="Estilo10" width="80">Stock</td>
                    <td align="center" class="Estilo10" width="80">Observaci&oacute;n</td>
                </tr>
                <%for(int u=0;u<pedidosPorItem.size();u++){%>
                <tr bgcolor="#FFFFCC"><!--"#d51f2c"color rojo-->
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getUnidadMedida()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getStockBodega()%></font></td>
                    <%if(!((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getObservacion().equals("")){%>
                    <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana6('observacionesPedidoMateriales.jsp?usuario=<%=usuario%>&descripcion=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%>&material=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>&numero=<%=(String)sesion.getAttribute("numero")%>&status=2&type1=2')"><img src="../../Images/observacion.jpg" width="18" height="16" border="0"></a></font></td>
                    <%}else{%>
                    <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana6('observacionesPedidoMateriales.jsp?usuario=<%=usuario%>&descripcion=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%>&material=<%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%>&numero=<%=(String)sesion.getAttribute("numero")%>&status=2&type1=2')"><img src="../../Images/observacionVacia.jpg" width="18" height="16" border="0"></a></font></td>
                    <%}%>
                </tr>
                <%}%>
                <tr class="Estilo10">
                    <td colspan="8" align="center"><input type="button" name="renueva" value="Imprimir" onclick="imprimirPagina();" class="Estilo10">
                        <%if(((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getEstado() == 2){%>
                        &nbsp;En proceso&nbsp;<input name="enProceso" type="checkbox" onclick="actualiza(this);">
                        &nbsp;Listo para retirar&nbsp;<input name="listo" type="checkbox" onclick="actualiza(this);" disabled>
                            &nbsp;Entregado&nbsp;<input name="entregado" type="checkbox" onclick="actualiza(this);" disabled>
                            <%}else if(((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getEstado() == 3){%>
                            &nbsp;En proceso&nbsp;<input name="enProceso" type="checkbox" onclick="actualiza(this);" disabled>
                            &nbsp;Listo para retirar&nbsp;<input name="listo" type="checkbox" onclick="actualiza(this);">
                        &nbsp;Entregado&nbsp;<input name="entregado" type="checkbox" onclick="actualiza(this);" disabled>
                            <%}else if(((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getEstado() == 4){%>
                            &nbsp;En proceso&nbsp;<input name="enProceso" type="checkbox" onclick="actualiza(this);" disabled>
                            &nbsp;Listo para retirar&nbsp;<input name="listo" type="checkbox" onclick="actualiza(this);" disabled>
                            &nbsp;Entregado&nbsp;<input name="entregado" type="checkbox" onclick="actualiza(this);">
                        <%}%>
                        &nbsp;Eliminar pedido&nbsp;<input name="borrar" type="checkbox" onclick="actualiza(this);">
                    </td>
                </tr>
            </table>
            <br><br>
            <%//}%>
            
            <input type="hidden" name="estado1">
            <%sesion.setAttribute("entre","1");%>
            <%sesion.setAttribute("pedidosPorItem",pedidosPorItem);%>
            <%
            
            
            
            //new imprimir.JasperReport2().jasper(pedidosPorItem,(String)sesion.getAttribute("numero"),((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario());%>
            <%}catch(Exception e){
            new sis.logger.Loger().logger("pedidos.jsp " , "ERROR: pedidos(): "+e.getMessage(),0);
            }%>
        </form>
    </body>
    <%}else{response.sendRedirect("../mac");}%>  
</html>