<%@page import="mac.contador.Contador,java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.crp2.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.equipo.clase.Equipo,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
    HttpSession sesion    = request.getSession();
    String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    /*String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    sesion.setAttribute("idSesion",idSesion);
    out.println(idSesion);
    out.println(usuario);
    out.println(tipou);*/
    if(idSesion.equals(sesion.getId())){
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
        <script type="text/javascript">
            
           
            var nav4 = window.Event ? true : false;
            function acceptNum(evt){ 
            var key = nav4 ? evt.which : evt.keyCode; 
            return (key >= 48 && key <= 57);
            }
          
         function send(){
           if(myform.proceso.options[myform.proceso.selectedIndex].value != 'nada'  ){
             myform.equipo.value = '';
           }
           myform.submit();
         }
         function send2(){
           //if(myform.proceso1.value == 'Ver pedidos globales'  ){
             myform.equipo.value = '';
             myform.proceso.options[myform.proceso.selectedIndex].value = 'nada'
           //}
           myform.submit();
         }
         
         function send1(){
          if(myform.equipo.value != '' && myform.proceso.options[myform.proceso.selectedIndex].value != 'nada'){
             myform.submit();
           }else{
             alert('Debe agregar proceso y equipo');
             return false;
           }
             
         }
         
      function refrescar(){
       document.location.reload();
      }
      
       function ajustarCeldas(){
                
                var ancho1,ancho2,i;
                var columnas=8; //CANTIDAD DE COLUMNAS//
                for(i=0;i<columnas;i++){
                ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
                ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
                if(ancho1>ancho2){
                document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-6;
                }else{
                document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-6;
                }
                }
                }
        </script>
    </head>
    <body onload="ajustarCeldas()">
        <script>
            function Abrir_ventana1 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=980, height=500, top=0, left=20"; 
                window.open(pagina,"",opciones);
            }
        </script>
        <br>
        <center><h5><font class="Estilo10">SISTEMA DE ADMINISTRACI&Oacute;N DE PEDIDO DE MATERIALES</font></h5></center>
        <br>
        <%if(tipou.equals("usuAdm") || tipou.equals("adm")){%>
        <form action="/mac/jsp/sistemas/EquiposElectricosAdm.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <input type="image" src="../../Images/volver2.jpg" border="0" width="89" height="18">
        </form>
        <%}if(tipou.equals("usu")){%>
        <form action="/mac/jsp/sistemas/EquiposElectricos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <input type="image" src="../../Images/volver2.jpg" border="0" width="89" height="18">
        </form>
        <%}%>
        <form name="myform" method="post" >
            <%try{
            if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("")){
             //Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
            }    
            ConexionEJB cal = new ConexionEJB();  
            Object ref1 =cal.conexionBean("Crp2Bean");
            Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
            Crp2Remote crp = crpHome.create();
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            int valor = 0;
            String actua = "1";
            String proceso = "";
            String numero = "";
            //Vector equipo = new Vector();
            java.text.NumberFormat number = new java.text.DecimalFormat(" ");
            java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
            sesion.setAttribute("equipos",null);
            sesion.setAttribute("necesidades",null);
            if(request.getParameter("es") == null){
            sesion.setAttribute("equipo",new Vector());
            }
            if(request.getParameter("eliminarMarcados.x") != null){
            for(int i=0;i<((Vector)sesion.getAttribute("equipo")).size();i++){
            if(request.getParameter("eliminar"+i) != null){
            mrp.borrarPedido(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp(),((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getLinea(),((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getProceso(),usuario);
            
            }
            }     
            }   
            if(request.getParameter("proceso") != null && !request.getParameter("proceso").equals("nada") &&  request.getParameter("equipo").equals("")){
            proceso = request.getParameter("proceso"); 
            //numero = "SI";
            sesion.setAttribute("equipo",crp.equiposPorProcesos(request.getParameter("proceso"),request.getParameter("equipo"),(byte)1));
            }else if(request.getParameter("buscar.x") != null ){
            if(request.getParameter("equipo") != null && request.getParameter("proceso") != null && !request.getParameter("equipo").equals("") && !request.getParameter("proceso").equals("nada")){
            proceso = request.getParameter("proceso");  
            //numero = "SI";
            sesion.setAttribute("equipo",crp.equiposPorProcesos(request.getParameter("proceso"),request.getParameter("equipo"),(byte)2));        
            } 
            
            }else if(request.getParameter("proceso1.x") != null ){
            proceso = "SIN";
            
            sesion.setAttribute("equipo",crp.equiposPorProcesos("SIN",request.getParameter("equipo"),(byte)3));
            
            }else{}
            
            
            %>
            
            
            <!--center><a href="../index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" target="_parent"><img src="../../Images/volver2.jpg" border="0"></center></a-->
            <br>
            <table cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>   
                    <td>
                        <table border=0 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                            <tr bgcolor="#B9DDFB">
                                <td align="center" class="Estilo10" height="20" colspan="2">Por &Aacute;rea</td>
                            </tr>
                            <tr class="Estilo10">
                                <td class="Estilo10" width="145" height="30" align="center">
                                    <select name="proceso" class="Estilo10" onchange="send();">
                                        <option value="nada">Elija un proceso</option>
                                        <option value="APA" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("APA")){%>selected<%}%>>APA</option>
                                        <option value="BAT" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("BAT")){%>selected<%}%>>BAT</option>
                                        <option value="BBT" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("BBT")){%>selected<%}%>>BBT</option>
                                        <option value="CAB" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("CAB")){%>selected<%}%>>CAB</option>
                                        <option value="CAJ" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("CAJ")){%>selected<%}%>>CAJ</option>
                                        <option value="ENV" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("ENV")){%>selected<%}%>>ENV</option>
                                        <option value="EST" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("EST")){%>selected<%}%>>EST</option>
                                        <option value="GAB" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("GAB")){%>selected<%}%>>GAB</option>
                                        <option value="HNO" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("HNO")){%>selected<%}%>>HNO</option>
                                        <option value="LCD" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("LCD")){%>selected<%}%>>LCD</option>
                                        <option value="LTX" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("LTX")){%>selected<%}%>>LTX</option>
                                        <option value="MON" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("MON")){%>selected<%}%>>MON</option>
                                        <option value="NET" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("NET")){%>selected<%}%>>NET</option>
                                        <option value="NUC" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("NUC")){%>selected<%}%>>NUC</option>
                                        <option value="OP"  <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("OP")){%>selected<%}%>>OP</option>
                                        <option value="RF"  <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("RF")){%>selected<%}%>>RF</option>
                                        <option value="TAP" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("TAP")){%>selected<%}%>>TAP</option>
                                        <option value="TER" <%if(request.getParameter("proceso") != null && request.getParameter("proceso").equals("TER")){%>selected<%}%>>TER</option>
                                    </select>
                                </td>
                                <td align="center" class="Estilo10">NP:&nbsp;<input type="text" name="equipo" size="10" onKeyPress="return acceptNum(event)" <%if(request.getParameter("equipo") != null && !request.getParameter("equipo").equals("")){%>value="<%=request.getParameter("equipo")%>"<%}else{%>value=""<%}%>>&nbsp;<input type="image" onmouseover="this.src = '../../Images/buscarEquipo_.jpg'" onmouseout="this.src = '../../Images/ingresar.jpg'"   src="../../Images/ingresar.jpg" width="100" height="16" name="buscar"></td>
                            </tr>
                            
                        </table>
                    </td>
                    <td width="60">&nbsp;</td>
                    <td align="center"><!--input type="submit" name="proceso1" value="Ver pedidos globales" onclick="send2();" class="Estilo10"></td-->
                    <input type="image" onclick="send2();" onmouseover="this.src = '../../Images/pedidosGlobales_.jpg'" onmouseout="this.src = '../../Images/pedidosGlobales.jpg'"   src="../../Images/pedidosGlobales.jpg" width="100" height="16" name="proceso1">
                    <td>
                        <table cellspacing="0" cellpadding="0" border="0">
                            <tr>
                                <td width="70">&nbsp;</td>
                                <td align="left" class="Estilo10"><a href="javascript:Abrir_ventana1('historialPedido.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&valor=1')">Historial pedidos</a></td> 
                            </tr>       
                            <tr>
                                <td width="70">&nbsp;</td>
                                <%if(mrp.validarUsuarioALink(usuario,"supervisorVales")){%>
                                <td align="left" class="Estilo10"><a href="javascript:Abrir_ventana1('historialVales.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>')">Ver saldo a mi cuenta</a></td> 
                                <%}else{%>
                                <td align="left" class="Estilo10">Ver saldo a mi cuenta</td> 
                                <%}%>
                            </tr>
                            <tr>
                                <td width="70">&nbsp;</td>
                                <%if(mrp.validarUsuarioALink(usuario,"pedidosGlobales")){%>
                                <td align="left" class="Estilo10"><a href="pedidosGlobales.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">Hacer Pedido global</a></td> 
                                <%}else{%>
                                <td align="left" class="Estilo10">Hacer Pedido global</td> 
                                <%}%>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            
            
            <!--br>
    <center><input type="submit" name="actu" value="Actualizar" class="Estilo10" onclick=" return enviar();">&nbsp;&nbsp;&nbsp;<font class="Estilo10">Agrupar&nbsp;&nbsp;&nbsp;<input type="checkbox" name="agruparGeneral" disabled></font></center-->
            <br>
            <table border=0 bgcolor=scrollbar align=center>
                <tr>
                    <td>
                        <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc" >
                            <tr bgcolor="#B9DDFB" align="center">
                                <td align="center" class="Estilo10" width="80">Proyectado</td>
                                <td align="center" class="Estilo10" width="80">Pedido</td>
                                <td align="center" class="Estilo10" width="80">L&iacute;nea</td>
                                <td align="center" class="Estilo10" width="80">Serie</td>
                                <td align="center" class="Estilo10" width="80">Familia</td>
                                <td align="center" class="Estilo10" width="80">Pedidos</td>       
                                <td align="center" class="Estilo10" width="80">Agrupar/Marcar</td>
                                <td align="center" class="Estilo10" width="80">Eliminar</td>
                            </tr>
                        </table>
                        <div style="overflow:auto; height:280px; padding:0">
                            <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD >     
                                <%
                                for(int i=0;i<((Vector)sesion.getAttribute("equipo")).size();i++){%>
                                
                                <tr bgcolor="#FFFFCC"><!--"#d51f2c"color rojo-->
                                    <td align="center" class="Estilo10" width="80"><%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getFechaInicio()%></td>
                                    <!--td align="center" class="Estilo10" width="80"><a href="javascript:Abrir_ventana1('materialesPorEquipo.jsp?idSesion=<%--=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&np=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp()%>&linea=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getLinea()%>&proceso=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getProceso()%>&familia=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getFamilia()%>')"><%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp()--%></a></td-->
                                    <td align="center" class="Estilo10" width="80"><%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp()%></td>
                                    <td align="center" class="Estilo10" width="80"><%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getLinea()%></td>
                                    <!--td align="center"><font class="Estilo9"><%--=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getProceso()--%></font></td-->
                                    <td align="center" class="Estilo10" width="80"><%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getSerie()%></td>
                                    <td align="center" class="Estilo10" width="80"><%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getFamilia()%></td>
                                    <%if(proceso.equals("SIN")){%>
                                    <%numero = String.valueOf(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNumPedido());%>
                                    <%}else{%>
                                    <%numero = "SI";%>
                                    <%}%>
                                    <%if(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getGrupo() > 0){%>
                                    <%if(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getEstado() == 1){%>
                                    
                                    <td align="center" class="Estilo10" width="80" bgcolor="#009966"><a href="javascript:Abrir_ventana1('pedidos.jsp?numero=<%=numero%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&equipo=<%=request.getParameter("equipo")%>&np=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp()%>&linea=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getLinea()%>&proceso=<%=proceso%>')">SI</td>
                                    <%}else{%>
                                    <td align="center" class="Estilo10" width="80" bgcolor="#FF9900"><a href="javascript:Abrir_ventana1('pedidos.jsp?numero=<%=numero%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&equipo=<%=request.getParameter("equipo")%>&np=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp()%>&linea=<%=((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getLinea()%>&proceso=<%=proceso%>')">SI</td>
                                    <%}}else{%>
                                    <td align="center" class="Estilo10" width="80">NO</td>
                                    <%}%>
                                    <td align="center" class="Estilo10" width="80"><input type="checkbox" name="agrupar<%=i%>"></td>
                                    <td align="center" class="Estilo10" width="80"><input type="checkbox" name="eliminar<%=i%>" ></td>
                                </tr>
                                <%}%>
                                <%if(((Vector)sesion.getAttribute("equipo")).size() < 15){%>
                                <%for(int i=0;i<20;i++){%>
                                <tr>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                </tr>
                                <%}}%>
                            </table>    
                        </div>
                        <tr>
                            <table border=0 cellspacing=0 cellpadding=2  bgcolor="#FFFFFF" align="center" >
                                <tr>
                                    <td height="50" width="520" style="border-width: 0px; border-right-color:#DDDDDD">&nbsp;</td>
                                    
                                    
                                    <td style="border-width: 0px; border-right-color:#FFFFFF" align="right"><input type="image" onmouseover="this.src = '../../Images/agrupar_.jpg'" onmouseout="this.src = '../../Images/agrupar.jpg'"  name="agruparmarcados" src="../../Images/agrupar.jpg" width="100" height="16" border="0"/></td>
                                    <td style="border-width: 0px; border-right-color:#FFFFFF" align="right"><input type="image" name="eliminarMarcados" src="../../Images/eliminar.jpg" onmouseover="this.src = '../../Images/eliminar_.jpg'" onmouseout="this.src = '../../Images/eliminar.jpg'" width="100" height="16" alt="Eliminar marcados" border="0"/></td>
                                    <td style="border-width: 0px; border-right-color:#FFFFFF" align="right">&nbsp;</td>
                                </tr>
                            </table>     
                        </tr>
                    </td>  
                </tr>
                
            </table>
            <br><br>
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="left" width="25%">
                <tr align="right">
                    <td class="Estilo10">NP agrupada:&nbsp;</td>
                    <td bgcolor="#FF9900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr align="right">
                    <td height="10">&nbsp;</td>
                </tr>
                <tr align="right">
                    <td class="Estilo10">NP sola:&nbsp;</td>
                    <td bgcolor="#009966">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr align="right">
                    <td height="10">&nbsp;</td>
                </tr>
                <tr align="right">
                    <td class="Estilo10">Sin pedido:&nbsp;</td>
                    <td bgcolor="#FFFFCC">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>                   
            </table>      
            
            
            <%if(request.getParameter("agruparmarcados.x") != null && actua.equals((String)sesion.getAttribute("actua"))){
            Vector equipos = new Vector();
            // if(request.getParameter("agruparGeneral") != null){
            for(int i=0;i<((Vector)sesion.getAttribute("equipo")).size();i++){
            if(request.getParameter("agrupar"+i) != null){
            //out.println("pedido: " + ((Equipo)equipo.elementAt(i)).getNp() + " linea: " + ((Equipo)equipo.elementAt(i)).getLinea());
            Equipo team = new Equipo();
            team.setNp(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getNp());
            team.setLinea(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getLinea());
            team.setProceso(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getProceso());
            team.setFamilia(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getFamilia());
            team.setSerie(((Equipo)((Vector)sesion.getAttribute("equipo")).elementAt(i)).getSerie());
            equipos.addElement(team);
            }
            }
            sesion.setAttribute("equipos",equipos);
            // }
            /*String parametrosJspOrigen = request.getQueryString(); 
            String urlDestino = "materialesPorEquipo.jsp?idSesion="+idSesion+"&usuario="+usuario+"&tipou="+ tipou; 
            out.println(urlDestino);
            response.sendRedirect(urlDestino); */
            %>
            <!--script>
                          this.location.href="javascript:Abrir_ventana1('materialesPorEquipo.jsp?idSesion=<%--=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou--%>')";
    </script-->
            <script>
                          this.location.href="materialesPorEquipo.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>";
            </script>    
            <%}%>
            <input type="hidden" name="es">          
            <%sesion.setAttribute("actua","1");%>
            <%}catch(Exception e){
            //new sis.logger.Loger().logger("arriba.jsp " , "ERROR: arriba (): "+e.getMessage(),0);
            }%>
        </form>
    </body>
    <%}else{response.sendRedirect("../mac");}%>  
</html>