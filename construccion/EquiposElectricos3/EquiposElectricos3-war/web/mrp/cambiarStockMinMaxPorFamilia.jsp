<%@page import="java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.estructuraEquipo.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cambiar stock min max por familia</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <style>
            .Estilo10 {
    font-size: 10px;
    font-family: Verdana, Arial, Helvetica, sans-serif;
    color: #666666;
}
        </style>
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
        <script>
         function changeLeadTime(){
         if(document.form1.leadTime.value == "" || document.form1.familia.value == "nada"){
            alert('Debe agregar un valor y familia para cambiar lead time');
            return false;
          }
          else{
           form1.submit();
          }
        }    
        function cambiarStockMinMax(){
         if(document.form1.E.value == "" || document.form1.k1.value == "" || document.form1.k2.value == "" || document.form1.familia.value == "nada"){
            alert('Debe agregar todos los valores que se le piden');
            return false;
          }
          else{
           form1.submit();
          }
        }
           
         var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return ((key >= 48 && key <= 57) || key == 46);

            }  
         
        </script>   
    </head>
    <body>
 <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               /*String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               sesion.setAttribute("idSesion",idSesion);
               out.println(idSesion);
               out.println(usuario);
               out.println(tipou);*/
               if(idSesion.equals(sesion.getId())){
              try{
                  ConexionEJB cal = new ConexionEJB();
                   Object ref1 =cal.conexionBean("MrpBean");
                   MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
                   MrpRemote mrp = mrpHome.create();
                   Object ref =cal.conexionBean("estructuraEquipoBean");
                   estructuraEquipoRemoteHome estructuraHome = (estructuraEquipoRemoteHome) PortableRemoteObject.narrow(ref, estructuraEquipoRemoteHome.class);
                   estructuraEquipoRemote estructura= estructuraHome.create();
                   Vector materialesPorFamilia = mrp.familiaPorMaterial(usuario);
                   NumberFormat number = new DecimalFormat(" ");
                   if(request.getParameter("cambiar") != null && sesion.getAttribute("stockMinMaxChange") != null){
                        for(int u=0;u<((Vector)sesion.getAttribute("stockMinMaxChange")).size();u++){
                            estructura.actualizarStockMinMax1(((EstructuraPorEquipo)(((Vector)sesion.getAttribute("stockMinMaxChange")).elementAt(u))).getCodigoMatPrima(),
                                                              ((EstructuraPorEquipo)(((Vector)sesion.getAttribute("stockMinMaxChange")).elementAt(u))).getStockMinimo(),
                                                              ((EstructuraPorEquipo)(((Vector)sesion.getAttribute("stockMinMaxChange")).elementAt(u))).getStockMaximo());
                        }
                       sesion.setAttribute("stockMinMaxChange",null); 
                   }
                   if(request.getParameter("leadTime") != null && !request.getParameter("leadTime").equals("") && request.getParameter("cambiarLeadTime") != null && request.getParameter("familia") != null && !request.getParameter("familia").equals("nada")){
                      Vector leadTimePorFamilia = mrp.leadTimePorFamilia(request.getParameter("familia"));
                      for(int i=0;i<leadTimePorFamilia.size();i++){
                          estructura.actualizarLeadTime(((EstructuraPorEquipo)leadTimePorFamilia.elementAt(i)).getCodigoMatPrima(),Short.parseShort(request.getParameter("leadTime")));
                      }
                   }
                   
    %>              
<form  action="cambiarStockMinMaxPorFamilia.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
    <br>
<center><h4><font class="Estilo10">CAMBIAR STOCK MIN MAX POR FAMILIA</font></h4></center>
<center><a href="retornarEstadoMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></center>
<br>
<table width="812" border="0" cellpadding="0" cellspacing="0" class="Estilo10">
  <!--DWLayoutTable-->
  <tr>
    <td width="74" rowspan="3" align="center" valign="middle">Min &nbsp;=&nbsp; </td>
    <td width="738" height="22" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Consumo promedio &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    *&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(
    Lead time
&nbsp;&nbsp;+&nbsp;&nbsp;   
<input name="E" type="text" size="10"  onKeyPress="return acceptNum(event)"/>
)&nbsp;&nbsp;&nbsp;&nbsp;*&nbsp;&nbsp;&nbsp;&nbsp;
    <input name="k1" type="text" size="10" onKeyPress="return acceptNum(event)" /></td>
    </tr>
  <tr>
    <td height="5" valign="top"><img name="" src="../Images/linea.GIF" width="200" height="5" alt="" /></td>
    </tr>
  
  <tr>
    <td height="22" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;30</td>
    </tr>
  <tr>
    <td height="2"></td>
    <td></td>
    </tr>
</table>
<br><br>
<table width="685" border="0" cellpadding="0" cellspacing="0" class="Estilo10">
  <!--DWLayoutTable-->
  <tr>
    <td width="74" height="32" align="center" valign="middle">Max &nbsp;=&nbsp; </td>
    <td width="611" valign="middle">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
	&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;Minimo&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      Consumo promedio
&nbsp;&nbsp;*&nbsp;&nbsp;   
<input name="k2" type="text" size="10" onKeyPress="return acceptNum(event)" />
&nbsp;&nbsp;&nbsp;&nbsp;</td>
    </tr>
</table>
<br><br>
    <table cellpadding="0" cellspacing="0" class="Estilo10" align="center" width="80%">
        <tr>
        <td align="center">Elija familia para actualizar minimo y maximo</td>
        <td width="10">&nbsp;</td>
        <td align="left">
           <select name="familia" class="Estilo10">
                                        <option value="nada">Elija familia</option>
                                        <%for(int i=0;i<materialesPorFamilia.size();i++){%>
                                        <option value="<%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%>"><%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%></option>                    
                                        <%}%>
                                    </select> 
            </td>
            <td width="10">&nbsp;</td>
            <td align="left"><input type="submit" class="Estilo10" value="Mostrar cambios" name="mostrar" onclick="return cambiarStockMinMax();"></td>
            <td align="center"><input type="submit" class="Estilo10" value="Actualizar cambios" name="cambiar"></td>
         </tr>                           
    </table>   
    <br>
    <table cellpadding="0" cellspacing="0" class="Estilo10" align="center" width="80%">
        <tr>
        <td align="center">Modificar lead time por familia</td>
        <td width="10">&nbsp;</td>
        <td align="left"><input name="leadTime" type="text" size="10" onKeyPress="return acceptNum(event)" /></td>
        <td width="10">&nbsp;</td>
        <td align="left"><input type="submit" class="Estilo10" value="Cambiar lead time" name="cambiarLeadTime" onclick="return changeLeadTime();"></td>
         </tr>                           
    </table>   
    <br><br>
    <%if(request.getParameter("mostrar") != null){
      Vector cambiarStockMinMax = mrp.cambiarStockMinMax(request.getParameter("familia"),Float.parseFloat(request.getParameter("E")),Float.parseFloat(request.getParameter("k1")),Float.parseFloat(request.getParameter("k2")));
      sesion.setAttribute("stockMinMaxChange",cambiarStockMinMax);%>
      <table cellpadding="0" cellspacing="0" class="Estilo10" align="center" border="1">
          <tr class="Estilo10">
              <td align="center"><font class="Estilo10">C&oacute;digo</font></td>
              <td align="center"><font class="Estilo10">nuevo Stock Min</font></td>                                    
              <td align="center"><font class="Estilo10">nuevo Stock Max</font></td>                                    
              <td align="center"><font class="Estilo10">antiguo Stock Min</font></td>                                   
              <td align="center"><font class="Estilo10">antiguo Stock Max</font></td>   
              <td align="center"><font class="Estilo10">Lead time</font></td>                                    
              <td align="center"><font class="Estilo10">Consumo promedio</font></td>
          </tr>
          <% for(int u=0;u<cambiarStockMinMax.size();u++){%>
          <tr>
              <td align="center"><font class="Estilo10"><%=((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getCodigoMatPrima()%></font></td>
              <td align="center"><font class="Estilo10"><%=((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getStockMinimo()%></font></td>
              <td align="center"><font class="Estilo10"><%=((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getStockMaximo()%></font></td>
              <td align="center"><font class="Estilo10"><%=((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getCantidad()%></font></td>
              <td align="center"><font class="Estilo10"><%=((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getCantidadEntregada()%></font></td>
              <td align="center"><font class="Estilo10"><%=((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getLeadTime()%></font></td>
              <td align="center"><font class="Estilo10"><%=number.format(((EstructuraPorEquipo)cambiarStockMinMax.elementAt(u)).getMediaCantidad())%></font></td>
          </tr>
          <%}%>
      </table>
    <%}%>
    <br>
</form>
<%}catch(Exception e){
                   out.println("ERROR: retornarEstadoMaterial.jsp " + e.getMessage());
                   }        
  }else{response.sendRedirect("../mac");}%>   
    </body>
</html>
