<%@page import="java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%><%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Bitacora Materiales</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <script language="javascript">
          function validar(){
          if(document.form1.observacion.value == ""){
            alert('Tiene que agregar una observaci&oacute;n');
            return false;
          }
          }
        </script>
    </head>
    <body>

        <form action="observacionComprasRequeridas.jsp" method="post" name="form1">
                   <%
                   HttpSession sesion    = request.getSession(); 
                   if(request.getParameter("guardar") == null){
                      sesion.setAttribute("codigo",request.getParameter("codigo"));
                      sesion.setAttribute("descripcion",request.getParameter("descripcion"));
                   }
                   ConexionEJB cal = new ConexionEJB();
                   Object ref1 =cal.conexionBean("MrpBean");
                   MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
                   MrpRemote mrp = mrpHome.create();
                   if(request.getParameter("guardar") != null){
                      mrp.insertarObservacionAmaterial((String)sesion.getAttribute("codigo"),request.getParameter("observacion"),new Date()); 
                   }
                   Vector bitacoraMateriales = mrp.bitacoraMateriales((String)sesion.getAttribute("codigo"));
                   %>
                   <br>
                   <table cellpadding="0" cellspacing="0" width="90%" border="0" align="center" class="Estilo10">
                       <tr>
                           <td align="center" colspan="2">Lista de observaciones de <%=(String)sesion.getAttribute("codigo")%>&nbsp;&nbsp;&nbsp;<%=(String)sesion.getAttribute("descripcion")%></td>
                       </tr>
                       <tr>
                           <td height="20">&nbsp;</td>
                       </tr>
                       <tr>
                           <td width="20%">Fecha de creaci&oacute;n</td>
                           <td>Bitacora</td>
                       </tr>
                       <tr>
                       <%for(int i=0;i<bitacoraMateriales.size();i++){%>
                       <tr>
                           <td><%=((EstructuraPorEquipo)bitacoraMateriales.elementAt(i)).getFechaBitacora()%></td>
                           <td><%=((EstructuraPorEquipo)bitacoraMateriales.elementAt(i)).getBitacoraMaterial()%></td>
                       </tr>     
                       <%}%>
                       </tr>
                   </table>   
                   <br><br>
                   
                   <table cellpadding="0" cellspacing="0" border="0" width="90%" align="center" class="Estilo10">
                       <tr>
                           <td align="center">Agregar una Observaci&oacute;n a la bitacora</td>
                       </tr>
                       <tr>
                           <td height="20">&nbsp;</td>
                       </tr>
                       <tr>
                           <td align="center"><textarea  name="observacion"  rows="3" cols="60"></textarea></td>
                       </tr>
                       <tr>
                           <td height="20">&nbsp;</td>
                       </tr>
                       <tr>
                           <td align="center"><input type="submit" name="guardar" value="Guardar" class="Estilo10" onclick="return validar()"></td>
                       </tr>     
                   </table>
        </form>    
    </body>
</html>
