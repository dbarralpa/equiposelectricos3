<%@ page import="java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura Asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
        
        <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               /*String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               sesion.setAttribute("idSesion",idSesion);
               out.println(idSesion);
               out.println(usuario);
               out.println(tipou);*/
               if(idSesion.equals(sesion.getId())){
        %>
        <FORM action="materialesAanalizar1.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method=post>
            <br>
            <center><h5><font class="Estilo10">ELEGIR MATERIALES PARA EL ANALISIS</font></h5></center>
        <%out.println("<center><font class='Estilo10'>Coloque un ticket en la casilla ESTADO para descartar ese Material</font>");%> <br>
        <%out.println("<center><font class='Estilo10'>Click en botón Aceptar para hacer los cambios</font>");%> 
        <br><br>
        <center><input type='submit' value='Aceptar' NAME=OK style='font-size: 10px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #330066;'></center><br>
        <center><a href="index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></center><br>
                <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
                 <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
                    <td align="center"><font class="Estilo9">CODIGO MATERIAL</font></td>
                    <td align="center"><font class="Estilo9">DESCRIPCION</font></td>
                    <td align="center"><font class="Estilo9">ESTADO</font></td>
                    
                 </tr>
                <%
               
                mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo estructuraEquipo = new mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo();
                try{
                Vector materiales = new Vector();
                materiales = estructuraEquipo.materiales();
                sesion.setAttribute("materiales",materiales);
                for(int i=0;i<materiales.size();i++){
                
                %>
                <tr class="Estilo9">
                    <td><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)materiales.elementAt(i)).getCodigoMatPrima()%></td>
                    <td><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)materiales.elementAt(i)).getDescripcion()%></td>
                    <%if(usuario.equals("jbriones") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                    <td align="center"><input type=checkbox name="M<%=i%>" ></td>
                    <%}else{%>
                    <td align="center"><input type=checkbox name="M<%=i%>" disabled></td>
                    <%}%>
                </tr>
                
                <%}%>
                 </table>
               
               <%
                
       }catch(Exception e){
                out.println("ERROR: index2.jsp " + e.getMessage());
                }         
                %>
            
        </form>
    <%}else{response.sendRedirect("../mac");}%>
    </body>
</html>
