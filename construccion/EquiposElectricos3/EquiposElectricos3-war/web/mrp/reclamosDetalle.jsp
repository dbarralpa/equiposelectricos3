<%@page import="java.text.SimpleDateFormat,java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.mrp.clase.*,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.mrp.bean.*" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Manufactura asistida por computadora</title>
    </head>
    <body>
        <%try{
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat fechap = new SimpleDateFormat("yyyyMMdd");
        ConexionEJB cal = new ConexionEJB();
        Object ref1 =cal.conexionBean("MrpBean");
        MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
        MrpRemote mrp = mrpHome.create();
        
        %>
        <br>
        <center><h4><font class="Estilo10">RECLAMOS POR NUMERO</font></h4></center>
        <center><a href="javascript:self.history.back('reclamos.jsp')" target="_parent"><img src="../Images/volver2.jpg" border="0"/></a>
        <br>
        <table cellpadding='0' cellspacing='0' align="center">
            <tr>
                <td style='padding-top: 20px' align='center'>
                    <table border='1' cellpadding='0' cellspacing='0'>
                        <tr>
                            <td class='Estilo10' colspan='10' align='center'>RECLAMO</td>
                        </tr>
                        <tr bgcolor='#FAE8D8'>
                            <td class='Estilo10' width='80'>Nº</td>
                            <td class='Estilo10' width='80'>Código</td>
                            <td class='Estilo10' width='200'>Descripción</td>
                            <td class='Estilo10' width='40'>UM</td>
                            <td class='Estilo10' width='80'>Cantidad</td>
                            <td class='Estilo10' width='80'>Usuario</td>
                            <td class='Estilo10' width='80'>Comprador</td>
                            <td class='Estilo10' width='200'>Observación</td>
                            <td class='Estilo10' width='80'>Fecha ingreso</td>
                            <td class='Estilo10' width='80'>Fecha parte</td>
                        </tr>
                        <%ModReclamo pla = mrp.reclamoPorNumero(Integer.parseInt(request.getParameter("reclamoUni")));%>
                        <tr>
                            <td class='Estilo10' width='80'><%=pla.getNumero()%></td>
                            <td class='Estilo10' width='80'><%=pla.getMaterial().getMateriaPrima()%></td>
                            <td class='Estilo10' width='200'><%=pla.getMaterial().getDescripcion()%></td>
                            <td class='Estilo10' width='40'><%=pla.getMaterial().getUm()%></td>
                            <td class='Estilo10' width='80'><%=pla.getMaterial().getCantidad()%></td>
                            <td class='Estilo10' width='80'><%=pla.getUsuario()%></td>
                            <td class='Estilo10' width='80'><%=pla.getMaterial().getComprador()%></td>
                            <td class='Estilo10' width='200'><%=pla.getObservacion()%></td>
                            <td class='Estilo10' width='80'><%=fecha.format(pla.getFechaIngreso())%></td>
                            <%if (pla.getFechaAjuste() != null) {%>
                            <td class='Estilo10' width='80'><%=fecha.format(pla.getFechaAjuste())%></td>
                            <%} else {%>
                            <td class='Estilo10' width='80'>&nbsp;</td>
                            <%}%>
                        </tr>
                    </table>
                </td>
            </tr>
            
            
            <tr>
                <td style='padding-top: 20px' align='center'>
                    <table border='1' cellpadding='0' cellspacing='0'>
                        <tr>
                            <td class='Estilo10' colspan='10' align='center'>EQUIPOS</td>
                        </tr>
                        <tr bgcolor='#FAE8D8'>
                            <td class='Estilo10' width='80'>Pedido</td>
                            <td class='Estilo10' width='40'>Línea</td>
                            <td class='Estilo10' width='60'>Serie</td>
                            <td class='Estilo10' width='200'>Cliente</td>
                            <td class='Estilo10' width='80'>Cliente</td>
                            <td class='Estilo10' width='80'>Expeditación</td>
                            <td class='Estilo10' width='80'>Familia</td>
                            <td class='Estilo10' width='60'>KVA</td>
                            <td class='Estilo10' width='80'>Diseño</td>
                            <td class='Estilo10' width='80'>Status</td>
                        </tr>
                        
                        <% ArrayList equipos = mrp.equiposPorNumero(pla.getNumero());
                        for (int i = 0; i < equipos.size(); i++) {
                            Equipo equipo = (Equipo) equipos.get(i);%>
                        <tr>
                            <td class='Estilo10' width='80'><%=equipo.getNp()%></td>
                            <td class='Estilo10' width='40'><%=equipo.getLinea()%></td>
                            <td class='Estilo10' width='60'><%=equipo.getSerie()%> "</td>
                            <td class='Estilo10' width='200'><%=equipo.getNombreCliente()%></td>
                            <td class='Estilo10' width='80'><%=fecha.format(equipo.getFechaCliente())%></td>
                            <td class='Estilo10' width='80'><%=fecha.format(equipo.getExpeditacion())%></td>
                            <td class='Estilo10' width='80'><%=equipo.getFamilia()%></td>
                            <td class='Estilo10' width='60'><%=equipo.getKva()%></td>
                            <td class='Estilo10' width='80'><%=equipo.getDiseno()%></td>
                            <td class='Estilo10' width='80'><%=equipo.getStatus()%></td>                
                        </tr>
                        <% }%>
                    </table>
                </td>
            </tr>
            
            
            
            <tr>
                <td style='padding-top: 30px' align='center'>
                    <table border='1' cellpadding='0' cellspacing='0' align="center">
                        <tr>
                            <td class='Estilo10' colspan='7' align='center'>ORDENES DE COMPRA</td>
                        </tr>
                        <tr bgcolor='#FAE8D8'>
                            <td class='Estilo10' width='80'>Nº Orden</td>
                            <td class='Estilo10' width='40'>Línea</td>
                            <td class='Estilo10' width='60'>Cantidad</td>
                            <td class='Estilo10' width='80'>Ingreso</td>
                            <td class='Estilo10' width='80'>Entrega</td>
                            <td class='Estilo10' width='80'>Confirmada</td>
                            <td class='Estilo10' width='200'>Proveedor</td>
                        </tr>
                        <%String fecha1 = "";
                        if (pla.getFechaAjuste() != null) {
                        fecha1 = fecha.format(pla.getFechaAjuste());
                        } else {
                        fecha1 = "";
                        }
                        ArrayList ordenes = mrp.ordenesDeCompra(pla.getMaterial().getMateriaPrima(), fechap.format(pla.getFechaIngreso()), fecha1);
                        for (int i = 0; i < ordenes.size(); i++) {
                        ModOrdenCompra orden = (ModOrdenCompra) ordenes.get(i);%>
                        <tr>
                            <td class='Estilo10' width='80'><%=orden.getNumero()%></td>
                            <td class='Estilo10' width='40'><%=orden.getLinea()%></td>
                            <td class='Estilo10' width='60'><%=orden.getCantidad()%></td>
                            <td class='Estilo10' width='80'><%=orden.getFechaIngreso()%></td>
                            <td class='Estilo10' width='80'><%=orden.getFechaEntrega()%></td>
                            <%if (!orden.getFechaConfirmada().trim().equals("")) {%>
                            <td class='Estilo10' width='80'><%=orden.getFechaConfirmada()%></td>
                            <%} else {%>
                            <td class='Estilo10' width='80'>&nbsp;</td>
                            <%}%>
                            <td class='Estilo10' width='200'><%=orden.getNombreProveedor()%></td>
                        </tr>
                        <%}%>
                    </table>
                </td>
            </tr>
            
            
            <tr>
                <td style='padding-top: 30px' align='center'>
                    <table border='1' cellpadding='0' cellspacing='0' align="center">
                        <tr>
                            <td class='Estilo10' colspan='7' align='center'>PARTES DE ENTRADA</td>
                        </tr>
                        <tr bgcolor='#FAE8D8'>
                            <td class='Estilo10' width='80'>Nº parte</td>
                            <td class='Estilo10' width='40'>Línea</td>
                            <td class='Estilo10' width='60'>Cantidad</td>
                            <td class='Estilo10' width='80'>Ingreso</td>
                            <td class='Estilo10' width='200'>Proveedor</td>
                        </tr>
                        <%String fecha2 = "";
                        if (pla.getFechaAjuste() != null) {
                        fecha2 = fecha.format(pla.getFechaAjuste());
                        } else {
                        fecha2 = "";
                        }
                        ArrayList partes = mrp.partesEntrada(pla.getMaterial().getMateriaPrima(), fechap.format(pla.getFechaIngreso()), fecha2);
                        for (int i = 0; i < partes.size(); i++) {
                        ModParteEntrada parte = (ModParteEntrada) partes.get(i);%>
                        <tr>
                            <td class='Estilo10' width='80'><%=parte.getNumero()%></td>
                            <td class='Estilo10' width='40'><%=parte.getLinea()%></td>
                            <td class='Estilo10' width='60'><%=parte.getCantidad()%></td>
                            <td class='Estilo10' width='80'><%=parte.getFechaIngreso()%></td>
                            <td class='Estilo10' width='200'><%=parte.getNombreProveedor()%></td>
                        </tr>
                        <%}%>
                    </table>
                </td>
            </tr>
        </table>
        
        
        
        
        <%}catch(Exception e){e.printStackTrace();}%> 
    </body>
</html>