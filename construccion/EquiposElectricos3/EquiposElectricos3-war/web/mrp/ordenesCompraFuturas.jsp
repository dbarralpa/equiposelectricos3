<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<  <html>
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
               <script language="javascript" type="text/javascript" src="../js/javascript.js"></script>
           <title>Manufactura asistida por computadora</title>
       </head>
       <body>
           <script>
               function Abrir_ventana10 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1200, height=800, top=0, left=30"; 
                window.open(pagina,"",opciones);
               }
           </script>
           <%
               
            HttpSession sesion    = request.getSession();
            String idSesion       = request.getParameter("idSesion");
            String usuario        = request.getParameter("usuario");
            String tipou          = request.getParameter("tipou");
            if(idSesion.equals(sesion.getId())){    
           try{
               ConexionEJB cal = new ConexionEJB();
               Object ref =cal.conexionBean("OrdenBean");
               OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
               OrdenRemote ordenCompra= ordenHome.create();
               java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
               java.text.NumberFormat number = new java.text.DecimalFormat(" ");
               java.text.NumberFormat nf = new java.text.DecimalFormat("#########.##");
               java.text.NumberFormat nf1 = java.text.NumberFormat.getInstance();
               nf1.setMaximumFractionDigits(0);
           %>
           <br>
           <center><h5><font class="Estilo10">Ordenes de Compra pendientes</font></h5></center>
           <br>
           <center><h5><font class="Estilo10">Material&nbsp;&nbsp;<%=request.getParameter("codpro")%>&nbsp;&nbsp;Descripci&oacute;n&nbsp;&nbsp;<%=request.getParameter("descri")%></font></h5></center>
           <br>

    <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
               <tr bgcolor="#B9DDFB" align="center">
                   <td><font class="Estilo9">numero Orden</font> </td>
                   <td><font class="Estilo9">Linea </font></td>
                   <td><font class="Estilo9">Proveedor</font></td>
                   <td><font class="Estilo9">rut Proveedor</font></td>
                   <td><font class="Estilo9">fecha Pedido</font> </td>
                   <td><font class="Estilo9">fecha Entrega</font></td>
                   <td><font class="Estilo9">Cantidad Pedida</font> </td>
                   <td><font class="Estilo9">Cantidad Pendiente</font></td>
                   <td><font class="Estilo9">Moneda</font></td>
                   <td><font class="Estilo9">Precio Unitario</font></td>
               </tr>
               
               <%
               Vector ordenesValidas = ordenCompra.ordenDeCompraValidasComprasRequeridas(request.getParameter("codpro"));
               int top = 0;
               Enumeration enu = ordenesValidas.elements();
               Enumeration enum3 = ordenesValidas.elements();
               while(enu.hasMoreElements()){
                 mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enu.nextElement();
               top++;
               }
               Vector ordenesInValidas = ordenCompra.ordenDeCompraInvalidas(request.getParameter("codpro"),20,"");
               Enumeration enum2 = ordenesInValidas.elements();
               while(enum2.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enum2.nextElement();
               %>
               <tr>
                   <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana10('buscarOrden.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&parametros=<%=orden.getNum_orden_compra()%>')"><%=orden.getNum_orden_compra()%></a></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getLinea()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getProveedor()%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getRut())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_pedido())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_entrega())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getCan_pedida())%></font></td>
                   <td align="center"><font class="Estilo9"><%=nf1.format(orden.getCan_pendiente())%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getDescMoneda()%></font></td>
                   <td align="center"><font class="Estilo9"><%=nf.format(orden.getPrecio())%></font></td>
                   <%--if(orden.getDescMoneda().equals("PESO")){%>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getPrecio())%></font></td>
                   <%}else{%>
                   <td align="center"><font class="Estilo9"><%=nf.format(orden.getPrecio())%></font></td>
                   <%}--%>
                   
               </tr>
               <%}  
               while(enum3.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enum3.nextElement();
               %>
               <tr>
                   <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana10('buscarOrden.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&parametros=<%=orden.getNum_orden_compra()%>')"><%=orden.getNum_orden_compra()%></a></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getLinea()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getProveedor()%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getRut())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_pedido())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_entrega())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getCan_pedida())%></font></td>
                   <td align="center"><font class="Estilo9"><%=nf1.format(orden.getCan_pendiente())%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getDescMoneda()%></font></td>
                   <td align="center"><font class="Estilo9"><%=nf.format(orden.getPrecio())%></font></td>
                   <%--if(orden.getDescMoneda().equals("PESO")){%>
                   <td align="center"><font class="Estilo9"><%=nf.format(orden.getPrecio())%></font></td>
                   <%}else{%>
                   <td align="center"><font class="Estilo9"><%=nf.format(orden.getPrecio())%></font></td>
                   <%}--%>
               </tr>
               <%}%>  
              
               
           </table>
     <%}catch(Exception e){e.printStackTrace();}%> 
      <%}else{response.sendRedirect("../mac");}%>
    </body>
</html>
