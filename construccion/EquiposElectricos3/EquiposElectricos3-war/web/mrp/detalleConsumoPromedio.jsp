<%@ page import="java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   <%
   NumberFormat nf1 = NumberFormat.getInstance();
   nf1.setMaximumFractionDigits(0);
   nf1.setMinimumFractionDigits(0);%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
        <%try{
             ConexionEJB cal = new ConexionEJB();
                   Object ref1 =cal.conexionBean("MrpBean");
                   MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
                   MrpRemote mrp = mrpHome.create(); 
                   //String[] mes = {"","ENERO","FEBRERO","MARZO","ABRIL","MAYO","JUNIO","JULIO","AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
                   //String codigo = request.getParameter("codigo");
                   float sumaPromedio = 0;
            %>
          
        <table cellpadding="0" cellspacing="0" border="0" class="Estilo10">
            <tr>
                <td><font size="3">Detalle consumos promedios por mes de <%=request.getParameter("codigo")%>&nbsp;<%=request.getParameter("descri")%></font></td>
            </tr>          
        </table>
      
        <br>
        <table border="1" cellpadding="0" cellspacing="0" class="Estilo10" width="30%" align="center">
            <tr  bgcolor="#B9DDFB">
                <td>Año</td>
                <td>Mes</td>
                <td>Consumo</td>
                <td>Devoluciones</td>
                <td>Diferencia</td>
            </tr>
            <%Vector consumos = mrp.detalleConsumoPromedio(request.getParameter("codigo"));
              Vector devoluciones = mrp.detalleDevolucionesPromedio(request.getParameter("codigo"));
              Vector fecha = mrp.fechasConsumoPromedio();
              float consumo = 0f;
              float devolucion = 0f;
              float promedio = 0f;
            for(int i=0;i<fecha.size();i++){%>
            <tr bgcolor="#DDDDDD">
               <%for(int b=0;b<consumos.size();b++){ 
                 if(((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)fecha.elementAt(i)).getAno() == ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumos.elementAt(b)).getAno()
                && ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)fecha.elementAt(i)).getMes() == ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumos.elementAt(b)).getMes()){%>  
                <td align="center"><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumos.elementAt(b)).getAno()%></td>
                <td align="center"><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumos.elementAt(b)).getMes()%></td>
                <td align="center"><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumos.elementAt(b)).getCantidad()%></td>
                <%consumo = ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumos.elementAt(b)).getCantidad();%>
                <%}}%>
                <%if(consumo == 0){%>
                <td align="center"><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)fecha.elementAt(i)).getAno()%></td>
                <td align="center"><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)fecha.elementAt(i)).getMes()%></td>
                <td align="center">0.0</td>
                <%}%>
                
                <%for(int a=0;a<devoluciones.size();a++){
                if(((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)fecha.elementAt(i)).getAno() == ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)devoluciones.elementAt(a)).getAno()
                && ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)fecha.elementAt(i)).getMes() == ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)devoluciones.elementAt(a)).getMes()){%>
                <td align="center"><%=((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)devoluciones.elementAt(a)).getCantidad()%></td>
                <%devolucion = ((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)devoluciones.elementAt(a)).getCantidad();%>
                <%}}%>
                
                <%if(devolucion == 0){
                   promedio += consumo;%>
                <td align="center">0</td>
                <%}else{
                promedio += (consumo-devolucion);
                }%>
                <%--//sumaPromedio += mrp.detalleConsumoPromedio(codigo,((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumo.elementAt(i)).getAno(),((mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo)consumo.elementAt(i)).getMes());--%>
                <td align="center"><%=nf1.format(consumo-devolucion)%></td>
                <%
                  consumo = 0;
                  devolucion = 0;%>
            </tr>
            <%}%>

        </table>  
        <br>
            <center><font size="2" class="Estilo10">Consumo Promedio&nbsp;&nbsp;&nbsp;<%=nf1.format(promedio/12)%></font></center>
                        
            <td bgcolor="#DDDDDD"></td>
          <%}catch(Exception e){
        out.println("ERROR: equiposConNecesidad.jsp " + e.getMessage());
        }%>
    </body>
</html>
