<%@page import="java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
        <script>
      
      function agregarMaterial(valor){
            var i;
            var a = 0;
            for(i=0; i<valor; i++){
            var material = "material"+i;
               if(document.getElementById(material).checked == true){
                  a = 1;
               }
           }
           if(a == 0){
            alert('debe agregar un material');
            return false;
            }else if(a == 1 && (document.form1.noAnalizar.checked == true || 
                                document.form1.stokeable.checked == true  ||
                                document.form1.minMax.checked == true     ||
                                document.form1.actualizarFamilia.checked == true  ||
                                document.form1.borrarMaterial.checked == true  ||
                                document.form1.mater.checked == true ||
                                document.form1.anaGen.checked == true || 
                                document.form1.borrarConjunto.checked == true ||
                                document.form1.actualizarConjunto.checked == true ||
                                document.form1.nombreConjunto1.checked == true)){ 
            form1.submit();
            }else{
            alert('debe agregar una categoria');
            return false;
            }
      }
      
       function ajustarCeldas(){
                
                var ancho1,ancho2,i;
                var columnas=17; //CANTIDAD DE COLUMNAS//
                for(i=0;i<columnas;i++){
                ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
                ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
                if(ancho1>ancho2){
                document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-6;
                }else{
                document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-6;
                }
                }
                }
  
                        </script>
    </head>
    <body onload="ajustarCeldas()">
         <script>
            function Abrir_ventana5(pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, top=0, left=50"; 
                window.open(pagina,"",opciones);
            }
            function Abrir_ventana (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=500, top=240, left=200"; 
                window.open(pagina,"",opciones);
             }
             function Abrir_ventana2 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=960, height=600, top=0, left=50"; 
                window.open(pagina,"",opciones);
             }
             function Abrir_ventana4 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=450, height=200, top=200, left=100"; 
                window.open(pagina,"",opciones);
             }
        </script>
        <%
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        /*String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        sesion.setAttribute("idSesion",idSesion);
        out.println(idSesion);
        out.println(usuario);
        out.println(tipou);*/
        if(idSesion.equals(sesion.getId())){
        %>               
        <form action="retornarEstadoMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">    
            
            <center><h4><font class="Estilo10">ESTADO DE MATERIALES</font></h4></center>
            <%
            try{      
            SimpleDateFormat formate = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            NumberFormat number = new DecimalFormat(" ");
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(3);
            String estado = "";
            ConexionEJB cal = new ConexionEJB();
            Object ref1 =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
            MrpRemote mrp = mrpHome.create();
            Vector compradores = mrp.compradores();
            int size = 0;
            String familia = "nada";
            if(request.getParameter("agregar") != null && sesion.getAttribute("compras") != null && request.getParameter("mater") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.actualizarMaterialesPorFamilia((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),request.getParameter("nombreFamilia"),(byte)1);
                    }
                }
                
            } else  if(request.getParameter("agregar") != null && sesion.getAttribute("compras") != null && request.getParameter("borrarMaterial") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.borrarMaterialesPorFamilia((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),request.getParameter("borrarMaterialFamilia"),(byte)1);
                        
                    }
                }
            } else  if(request.getParameter("agregar") != null && sesion.getAttribute("compras") != null && request.getParameter("actualizarFamilia") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.actualizarMaterialesPorFamilia((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),request.getParameter("familiaActualizar"),(byte)1);
                    }
                }
            }
            
            else if(request.getParameter("agregar") != null && sesion.getAttribute("compras") != null && request.getParameter("noAnalizar") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.cambiarEstadoMaterial((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),(byte)1);
                        //out.println(((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0));
                    }
                }
            } else if(sesion.getAttribute("compras") != null && request.getParameter("agregar") != null && request.getParameter("stokeable") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.cambiarStokeable((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0));
                    }
                }
            } else if(sesion.getAttribute("compras") != null && request.getParameter("agregar") != null && request.getParameter("minMax") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.cambiarEstadoMaterial((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),(byte)2);
                    }
                }
            }else if(sesion.getAttribute("compras") != null && request.getParameter("agregar") != null && request.getParameter("anaGen") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.cambiarEstadoMaterial((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),(byte)3);
                    }
                }
            }else if(sesion.getAttribute("compras") != null && request.getParameter("agregar") != null && request.getParameter("nombreConjunto1") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.asociarMateriales((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),request.getParameter("nombreConjunto"),(byte)1);
                    }
                }  
            }else if(sesion.getAttribute("compras") != null && request.getParameter("agregar") != null && request.getParameter("actualizarConjunto") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.asociarMateriales((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),request.getParameter("conjuntoActualizar"),(byte)1);
                        //mrp.eliminarAsociado((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0));
                    }
                }  
            }else if(sesion.getAttribute("compras") != null && request.getParameter("agregar") != null && request.getParameter("borrarConjunto") != null){
                for(int y=0;y<((Vector)sesion.getAttribute("compras")).size();y++){
                    if(request.getParameter("material"+y) != null){
                        mrp.asociarMateriales((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0),null,(byte)2);
                        //mrp.eliminarAsociado((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(y)).elementAt(0));
                    }
                }  
            }else{}
            
            if(request.getParameter("actualizarComprador") != null){
                for(int u=0;u<((Vector)sesion.getAttribute("compras")).size();u++){
                    if(request.getParameter("comprador"+u) != null && !request.getParameter("comprador"+u).equals("0")){
                        mrp.actualizarComprador((String)((Vector)((Vector)sesion.getAttribute("compras")).elementAt(u)).elementAt(0),request.getParameter("comprador"+u),null);
                    }
                }
            }
            
            
            ///////////////////////////////////////////////////////////////
            
            if(request.getParameter("material") != null && !request.getParameter("material").equals("00000000000000000000")){
                sesion.setAttribute("material",request.getParameter("material"));
                sesion.setAttribute("rango",null);
                sesion.setAttribute("familia",null);
                sesion.setAttribute("equipo4",null);
                sesion.setAttribute("asociar",null);
                sesion.setAttribute("sap",null);
                sesion.setAttribute("codigo15",null);
            }
            if(request.getParameter("rango") != null && !request.getParameter("rango").equals("")){
                sesion.setAttribute("rango",request.getParameter("rango"));
                sesion.setAttribute("material",null);
                sesion.setAttribute("familia",null);
                sesion.setAttribute("equipo4",null);
                sesion.setAttribute("asociar",null);
                sesion.setAttribute("sap",null);
                sesion.setAttribute("codigo15",null);
            }
            if(request.getParameter("familia") != null && !request.getParameter("familia").equals("nada")){
                sesion.setAttribute("familia",request.getParameter("familia"));
                sesion.setAttribute("material",null);
                sesion.setAttribute("rango",null);
                sesion.setAttribute("equipo4",null);
                sesion.setAttribute("asociar",null);
                sesion.setAttribute("sap",null);
                sesion.setAttribute("codigo15",null);
            }
            if(request.getParameter("equipo4") != null && !request.getParameter("equipo4").equals("")){
                sesion.setAttribute("equipo4",request.getParameter("equipo4"));
                sesion.setAttribute("material",null);
                sesion.setAttribute("rango",null);
                sesion.setAttribute("familia",null);
                sesion.setAttribute("asociar",null);
                sesion.setAttribute("sap",null);
                sesion.setAttribute("codigo15",null);
            }
            if(request.getParameter("asociar") != null && !request.getParameter("asociar").equals("nada")){
                sesion.setAttribute("asociar",request.getParameter("asociar"));
                sesion.setAttribute("material",null);
                sesion.setAttribute("rango",null);
                sesion.setAttribute("familia",null);
                sesion.setAttribute("equipo4",null);
                sesion.setAttribute("sap",null);
                sesion.setAttribute("codigo15",null);
            }
            
            if(request.getParameter("sap") != null && !request.getParameter("sap").equals("")){
                sesion.setAttribute("sap",request.getParameter("sap"));
                sesion.setAttribute("material",null);
                sesion.setAttribute("rango",null);
                sesion.setAttribute("familia",null);
                sesion.setAttribute("equipo4",null);
                sesion.setAttribute("asociar",null);
                sesion.setAttribute("codigo15",null);
            }
            
            if(request.getParameter("codigo15") != null && !request.getParameter("codigo15").equals("")){
                sesion.setAttribute("codigo15",request.getParameter("codigo15"));
                sesion.setAttribute("sap",null);
                sesion.setAttribute("material",null);
                sesion.setAttribute("rango",null);
                sesion.setAttribute("familia",null);
                sesion.setAttribute("equipo4",null);
                sesion.setAttribute("asociar",null);
            }
            
            ///////////////////////////////////////////////////////////////
            
            Vector materialesPorFamilia = mrp.familiaPorMaterial(usuario);
            Vector conjuntoActualizar = mrp.conjuntosAsociados();
            if( sesion.getAttribute("material") != null){
                if(((String)sesion.getAttribute("material")).equals("8011001147")){
                    sesion.setAttribute("compras",mrp.retornarEstadoMateriales((String)sesion.getAttribute("material"),"","",1,(byte)1));
            %> 
            <center><h6><font class="Estilo10">Material:&nbsp;&nbsp;<%=(String)sesion.getAttribute("material")%></font></h6></center> 
            <%}
            else{
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales(((String)sesion.getAttribute("material")).substring(0,10),((String)sesion.getAttribute("material")).substring(10,20),"",1,(byte)1));    
            %>
            <center><h6><font class="Estilo10">Material:&nbsp;&nbsp;entre&nbsp;&nbsp;<%=((String)sesion.getAttribute("material")).substring(0,10)%>&nbsp;&nbsp;y&nbsp;&nbsp;<%=((String)sesion.getAttribute("material")).substring(10,20)%></font></h6></center>
            
            <%}}   
            if(sesion.getAttribute("rango") != null){
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales("","",(String)sesion.getAttribute("rango"),0,(byte)1));
            %>
            <center><h6><font class="Estilo10">Rango:&nbsp;&nbsp;<%=(String)sesion.getAttribute("rango")%></font></h6></center> 
            <%}     
            if(sesion.getAttribute("familia") != null){
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales("","",(String)sesion.getAttribute("familia"),2,(byte)1));   
            %>
            <center><h6><font class="Estilo10">Familia:&nbsp;&nbsp;<%=(String)sesion.getAttribute("familia")%></font></h6></center> 
            <%}
            if(sesion.getAttribute("equipo4") != null){
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales("","",(String)sesion.getAttribute("equipo4"),3,(byte)1));   
            %>
            <center><h6><font class="Estilo10">Equipo&nbsp;&nbsp;<%=(String)sesion.getAttribute("equipo4")%></font></h6></center> 
            <%}
            if(sesion.getAttribute("asociar") != null){
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales("","",(String)sesion.getAttribute("asociar"),4,(byte)1));   
            %>
            <center><h6><font class="Estilo10">Conjunto:&nbsp;&nbsp;<%=(String)sesion.getAttribute("asociar")%></font></h6></center> 
            <%}
            if(sesion.getAttribute("sap") != null){
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales("","",(String)sesion.getAttribute("sap"),5,(byte)1));   
            %>
            <center><h6><font class="Estilo10">Sap&nbsp;&nbsp;<%=(String)sesion.getAttribute("sap")%></font></h6></center> 
            <%}
            if(sesion.getAttribute("codigo15") != null){
            sesion.setAttribute("compras",mrp.retornarEstadoMateriales("","",(String)sesion.getAttribute("codigo15"),6,(byte)1));   
            %>
            <center><h6><font class="Estilo10">Código&nbsp;&nbsp;<%=(String)sesion.getAttribute("codigo15")%></font></h6></center> 
            <%}
            Vector compras = new Vector();
            if(sesion.getAttribute("compras") != null){
            compras = (Vector)sesion.getAttribute("compras");
            }
            %>
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                <tr>
                    <td colspan="12" align="center"><a href="analisisReclamos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">Reclamos</a>&nbsp;&nbsp;</td>
                </tr>
                <tr class="Estilo10" style="padding-top: 30px">   
                    <td >Texto de b&uacute;squeda&nbsp;&nbsp;<input type="text" name="rango"></td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10">Buscar por Np&nbsp;&nbsp;<input type="text" name="equipo4" size="10"></td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10">Buscar por Sap&nbsp;&nbsp;<input type="text" name="sap" size="10"></td>
                    <td width="10">&nbsp;</td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10">Buscar por Código&nbsp;&nbsp;<input type="text" name="codigo15" size="10"></td>
                    <td><input type="submit" value="Enviar" name="aceptar" class="Estilo9"></td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10"><a href="../EstadoMateriales"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a></td>
                    <td class="Estilo10">&nbsp;&nbsp;&nbsp;Necesidades Mes<a href="../ExcelComprasMes"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a></td>
                </tr>
            </table>
            <br>
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                <tr class="Estilo10">
                    <td align="center">
                    <a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></td>
                    <td align="center">
                        <select name="material" class="Estilo10" onchange="form1.submit()">
                            <option value="00000000000000000000" >Elija material</option>
                            <option value="10110990011051105003" >Entre 1011099001 y 1051105003</option>
                            <option value="10511050041061401114" >Entre 1051105004 y 1061401114</option>
                            <option value="10614070011131006015" >Entre 1061407001 y 1131006015</option>
                            <option value="11310060161131396001" >Entre 1131006016 y 1131396001</option>
                            <option value="11313990011151001390" >Entre 1131399001 y 1151001390</option>
                            <option value="11510013911151015043" >Entre 1151001391 y 1151015043</option>
                            <option value="11510150441991099014" >Entre 1151015044 y 1991099014</option>
                            <option value="19910990152031003153" >Entre 1991099015 y 2031003153</option>
                            <option value="20310031542031008049" >Entre 2031003154 y 2031008049</option>
                            <option value="20310080502031019058" >Entre 2031008050 y 2031019058</option>
                            <option value="20310190592031054008" >Entre 2031019059 y 2031054008</option>
                            <option value="20310540092031190547" >Entre 2031054009 y 2031190547</option>
                            <option value="20311905522031927261" >Entre 2031190552 y 2031927261</option>
                            <option value="20319272622031951052" >Entre 2031927262 y 2031951052</option>
                            <option value="20319510532031999210" >Entre 2031951053 y 2031999210</option>
                            <option value="20319992112041001008" >Entre 2031999211 y 2041001008</option>
                            <option value="20410020012041099025" >Entre 2041002001 y 2041099025</option>
                            <option value="20410990267111009001" >Entre 2041099026 y 7111009001</option>
                            <option value="71110090028011001146" >Entre 7111009002 y 8011001146</option>
                            <option value="8011001147">Desde 8011001147</option>
                        </select> 
                    </td>
                    <td width="20">&nbsp;</td>
                    <td align="center">
                        <select name="familia" class="Estilo10" onchange="form1.submit()">
                            <option value="nada">Elija familia</option>
                            <%for(int i=0;i<materialesPorFamilia.size();i++){%>
                            <option value="<%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%>"><%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%></option>                    
                            <%}%>
                        </select> 
                    </td>
                    <td width="20">&nbsp;</td>
                    <td align="center">
                        <select name="asociar" class="Estilo10" onchange="form1.submit()">
                            <option value="nada">Elija conjunto</option>
                            <%for(int i=0;i<conjuntoActualizar.size();i++){%>
                            <option value="<%=((EstructuraPorEquipo)conjuntoActualizar.elementAt(i)).getConjunto()%>"><%=((EstructuraPorEquipo)conjuntoActualizar.elementAt(i)).getConjunto()%></option>                    
                            <%}%>
                        </select> 
                    </td>
                    <td width="20">&nbsp;</td>
                    <%if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                    <td class="Estilo10"><a href="cambiarStockMinMaxPorFamilia.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">Cambiar Stock Min Max por familia</a></td>
                    <%}else{%>
                    <td class="Estilo10" > Cambiar Stock Min Max por familia</td>
                    <%}%>
                </tr>
            </table>
            <br>
            <!--table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                <tr>
                    <td class="Estilo10">Analisis general:&nbsp;</td>
                    <td bgcolor="#0099FF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td class="Estilo10">An&aacute;lisis s&oacute;lo MinMax:&nbsp;</td>
                    <td bgcolor="#FF3300">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td class="Estilo10">No analizar:&nbsp;</td>
                    <td bgcolor="#339966">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td>&nbsp;&nbsp;</td>
                    <td class="Estilo10">Con observaci&oacute;n:&nbsp;</td>
                    <td bgcolor="#FFCC00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table-->      
            <br>
            <table border=0 bgcolor=scrollbar align=center>
                <td>
                    <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc">
                        <tr bgcolor="#B9DDFB">
                            <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                            <td align="center"><font class="Estilo9">C&oacute;digo</font></td>
                            <td align="center"><font class="Estilo9">Descripci&oacute;n</font></td>                                    
                            <td align="center"><font class="Estilo9" title="Necesidad por material">Nec mat</font></td>
                            <td align="center"><font class="Estilo9" title="Consumo promedio">Con Pro</font></td>
                            <td align="center"><font class="Estilo9" title="Stock Minimo">Stock Min</font></td>
                            <td align="center"><font class="Estilo9" title="Stock Maximo">Stock Max</font></td>
                            <td align="center"><font class="Estilo9">S.Planta</font></td>
                            <td align="center"><font class="Estilo9" title="Bodeba de materias primas">BMP</font></td>
                            <td align="center"><font class="Estilo9" title="Bodega de productos terminados">BPT</font></td>
                            <td align="center"><font class="Estilo9" title="Bodega de materias primas en espera de validación">BMP 9</font></td>
                            <td align="center"><font class="Estilo9" title="Unidad de medida">um</font></td>    
                            <td align="center"><font class="Estilo9" title="Lead time">Lead</font></td>
                            <td align="center"><font class="Estilo9" title="&oacute;rdenes de compra pendientes">Oc p</font></td>
                            <td align="center"><font class="Estilo9" title="Sap pendientes">Sap</font></td>
                            <td align="center"><font class="Estilo9" title="Comprador">Co</font></td>
                            <td align="center"><font class="Estilo9" title="Estado material stokeable">ES</font></td>
                            <td align="center"><font class="Estilo9" title="Elegir un material">M</font></td>
                            
                            <td align="center">&nbsp;&nbsp;&nbsp;</td>
                            
                        </tr>
                    </table>
                    <div style="overflow:auto; height:290px; padding:0">
                        <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                            <% for(int u=0;u<compras.size();u++){%>
                            <%--if(((Float)((Vector)compras.elementAt(u)).elementAt(4)).floatValue() > 0 && ((Byte)((Vector)compras.elementAt(u)).elementAt(12)).byteValue() == (byte)0){%>
                            <tr bgcolor="#FFFFCC" class="Estilo10">
                           <%}else{%>
                            <tr class="Estilo10">
                                <%}--%>
                                <%if(((Byte)((Vector)compras.elementAt(u)).elementAt(12)).byteValue() == 1){%>
                                <tr class="Estilo10">
                                    <%}else{%>
                                  <tr class="Estilo10" bgcolor="#339966">  
                                    <%}%>
                                <%--if(((Byte)((Vector)compras.elementAt(u)).elementAt(15)).byteValue() == 1){%>
                                
                                <td align="center" bgcolor="#0099FF"><%=((Vector)compras.elementAt(u)).elementAt(0)%></td>
                                <%}%>
                                <%if(((Byte)((Vector)compras.elementAt(u)).elementAt(14)).byteValue() == 1){%>
                                
                                <td align="center" bgcolor="#FF3300"><%=((Vector)compras.elementAt(u)).elementAt(0)%></td>
                                <%}%>
                                <%if(((Byte)((Vector)compras.elementAt(u)).elementAt(12)).byteValue() == 1){%>
                                
                                <td align="center" bgcolor="#339966"><%=((Vector)compras.elementAt(u)).elementAt(0)%></td>
                                <%}--%>
                                 <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(0)%></td>
                                
                                <%if(usuario.equals("jgonzalez") || usuario.equals("dbarra") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <%if(((Integer)((Vector)compras.elementAt(u)).elementAt(18)).intValue() > 0 ){%>
                                <td bgcolor="#FFCC00" align="center" onclick="javascript:Abrir_ventana5('observacionComprasRequeridas.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>&descripcion=<%=((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase()%></td>
                                <%}else{%>
                                <td align="center" onclick="javascript:Abrir_ventana5('observacionComprasRequeridas.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>&descripcion=<%=((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase()%></td>
                                <%}%>
                                <%}else{%>
                                <%if(((Integer)((Vector)compras.elementAt(u)).elementAt(18)).intValue() > 0 ){%>
                                <td bgcolor="#FFCC00" align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase()%></td>
                                <%}else{%>
                                <td align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase()%></td>
                                <%}}%>
                                
                                <%--}--%>

                                <td align="center"><a href="javascript:Abrir_ventana('equiposConNecesidad.jsp?ordenVencida=<%=((Float)((Vector)compras.elementAt(u)).elementAt(9)).floatValue()%>&ordenFutura=<%=((Float)((Vector)compras.elementAt(u)).elementAt(10)).floatValue()%>&stock=<%=((Float)((Vector)compras.elementAt(u)).elementAt(6)).floatValue()%>&stockMinimo=<%=((Float)((Vector)compras.elementAt(u)).elementAt(4)).floatValue()%>&stockMaximo=<%=((Float)((Vector)compras.elementAt(u)).elementAt(5)).floatValue()%>&codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(2)).floatValue())%></a></td>
                                
                                <td align="center"><a href="javascript:Abrir_ventana5('detalleConsumoPromedio.jsp?codigo=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>&media=<%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(3)).floatValue())%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(3)).floatValue())%></a></td>
                                <%if(usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <td align="center"><a href="javascript:Abrir_ventana4('setearStockMinMax.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(4)).floatValue())%></a></td>
                                <td align="center"><a href="javascript:Abrir_ventana4('setearStockMinMax.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(5)).floatValue())%></a></td>
                                <%}else{%>
                                <td align="center"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(4)).floatValue())%></td>
                                <td align="center"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(5)).floatValue())%></td>
                                <%}%>
                                
                                <td align="center"><a href="javascript:Abrir_ventana5('setearStockPlanta_1.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>')"><%=nf.format(((Float)((Vector)compras.elementAt(u)).elementAt(11)).floatValue())%></a></td>
                                <td align="center"><a href="javascript:Abrir_ventana2('evolucionStock.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(6)).floatValue())%></a></td>
                                <td align="center"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(19)).floatValue())%></td>
                                <td align="center"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(21)).floatValue())%></td>
                                <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(7)%></td>
                                <%if(usuario.equals("jgonzalez") || usuario.equals("vhernandez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <td align="center"><a href="javascript:Abrir_ventana4('leadTimes.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=((Vector)compras.elementAt(u)).elementAt(8)%></a></td>
                                <%}else{%>
                                <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(8)%></td>
                                <%}%>
                                
                                <%if(((Float)((Vector)compras.elementAt(u)).elementAt(9)).floatValue() > 0 || ((Float)((Vector)compras.elementAt(u)).elementAt(10)).floatValue() > 0){%>
                                <td align="center" bgcolor="#FF0000"><a href="javascript:Abrir_ventana5('ordenesCompraFuturas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(9)).floatValue()+((Float)((Vector)compras.elementAt(u)).elementAt(10)).floatValue())%></a></td>
                                <%}else{%>
                                <td align="center"><a href="javascript:Abrir_ventana5('ordenesCompraFuturas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(9)).floatValue()+((Float)((Vector)compras.elementAt(u)).elementAt(10)).floatValue())%></a></td>
                                <%}%>
                                
                                <!--ACA-->
                                 <td align="center"><%=(((Vector)compras.elementAt(u)).elementAt(22)) %><a href="javascript:Abrir_ventana5('DetalleCantidadSap.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(9)).floatValue()+((Float)((Vector)compras.elementAt(u)).elementAt(10)).floatValue())%></a></td>
                                
                                 <%if(!((String)((Vector)compras.elementAt(u)).elementAt(16)).equals("") && !((String)((Vector)compras.elementAt(u)).elementAt(16)).equals("null")){
                                size++;%>
                                <%if(usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <td align="center"><a href="javascript:Abrir_ventana('actualizarComprador.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=((String)((Vector)compras.elementAt(u)).elementAt(16)).substring(0,2)%></td>
                                
                                <%}else{%>
                                <td align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(16)).substring(0,2)%></td>
                                <%}}else{%>
                                <%if(usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <td align="center">
                                    <select name="comprador<%=u%>" class="Estilo10">
                                        <option value="0">Com</option>
                                <%for(int i=0;i<compradores.size();i++){%>
                                <%EstructuraPorEquipo estru = (EstructuraPorEquipo)compradores.elementAt(i);%>
                                <option value="<%=estru.getUsuario()%>"><%=estru.getUsuario()%></option>
                                <%}%>
                                    </select>
                                </td>
                                <%}else{%>
                                <td align="center">
                                    <select name="comprador<%=u%>" class="Estilo10" disabled>
                                        <option value="0">Com</option>
                                <%for(int i=0;i<compradores.size();i++){%>
                                <%EstructuraPorEquipo estru = (EstructuraPorEquipo)compradores.elementAt(i);%>
                                <option value="<%=estru.getUsuario()%>"><%=estru.getUsuario()%></option>
                                <%}%>
                                    </select>
                                </td>
                                <%}}%>
                                <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(12)%></td>
                                <%if(usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <td align="center"><input type=checkbox name="material<%=u%>" class="Estilo10"></td>
                                <%}else{%>
                                <td align="center"><input type=checkbox name="material<%=u%>" class="Estilo10" disabled></td>
                                <%}%>
                               
                               
                                
                            </tr> 
                            <%size = u;}%>                             
                            <%if(size < 26){
                            for(int i=0;i<26-size;i++){%>
                            <tr>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                <td align="center" class="Estilo10">&nbsp;</td>
                                
                            </tr>
                            <%}}%>
                        </table>
                    </div>
                </td>
            </table>
            <br>
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                <tr class="Estilo10">
                    <td class="Estilo10">
                        <fieldset>
                            <legend>Agregar familia:</legend>
                            <input type="checkbox" name="mater" class="Estilo10"> &nbsp;&nbsp;<input type="text" name="nombreFamilia" size="10">
                        </fieldset>
                    </td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center">
                        <fieldset>
                            <legend>Actualizar familia:</legend>
                            <input type="checkbox" name="actualizarFamilia" class="Estilo10" >&nbsp;&nbsp;
                            <select name="familiaActualizar" class="Estilo10">
                                <option value="nada">Elija familia</option>
                                <%for(int i=0;i<materialesPorFamilia.size();i++){%>
                                <option value="<%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%>"><%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%></option>                    
                                <%}%>
                            </select>
                        </fieldset>
                    </td>
                     <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center" colspan="4">
                        <fieldset>
                            <legend>Borrar material:</legend>
                            <input type="checkbox" name="borrarMaterial" class="Estilo10" >&nbsp;&nbsp;
                            <select name="borrarMaterialFamilia" class="Estilo10">
                                <option value="nada">Elija familia</option>
                                <%for(int i=0;i<materialesPorFamilia.size();i++){%>
                                <option value="<%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%>"><%=((EstructuraPorEquipo)materialesPorFamilia.elementAt(i)).getFamilia()%></option>                    
                                <%}%>
                            </select>
                        </fieldset>
                    </td>
                </tr>                
                <tr>
                    <td class="Estilo10">
                        <fieldset>
                            <legend>Agregar conjunto:</legend>
                            <input type="checkbox" name="nombreConjunto1" class="Estilo10"> &nbsp;&nbsp;<input type="text" name="nombreConjunto" size="10">
                        </fieldset>
                    </td>
                     <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center">
                        <fieldset>
                            <legend>Actualizar conjunto:</legend>
                            <input type="checkbox" name="actualizarConjunto" class="Estilo10" >&nbsp;&nbsp;
                            <select name="conjuntoActualizar" class="Estilo10">
                                <option value="nada">Elija Conjunto</option>
                                <%for(int i=0;i<conjuntoActualizar.size();i++){%>
                                <option value="<%=((EstructuraPorEquipo)conjuntoActualizar.elementAt(i)).getConjunto()%>"><%=((EstructuraPorEquipo)conjuntoActualizar.elementAt(i)).getConjunto()%></option>                    
                                <%}%>
                            </select>
                        </fieldset>
                    </td>
                     <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center"  colspan="4">
                        <fieldset>
                            <legend>Borrar Conjunto:</legend>
                            <input type="checkbox" name="borrarConjunto" class="Estilo10" >&nbsp;&nbsp;
                            <select name="borrarMaterialConjunto" class="Estilo10">
                                <option value="nada">Elija conjunto:</option>
                                <%for(int i=0;i<conjuntoActualizar.size();i++){%>
                                <option value="<%=((EstructuraPorEquipo)conjuntoActualizar.elementAt(i)).getConjunto()%>"><%=((EstructuraPorEquipo)conjuntoActualizar.elementAt(i)).getConjunto()%></option>                    
                                <%}%>
                            </select>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td class="Estilo10" align="center">
                        <fieldset>
                            <legend>S&oacute;lo Min Max:</legend>
                            <input type="checkbox" name="minMax" class="Estilo10" >
                        </fieldset>
                    </td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center">
                        <fieldset>
                            <legend>Material stokeable:</legend>
                            <input type="checkbox" name="stokeable" class="Estilo10" >
                        </fieldset>
                    </td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center">
                        <fieldset>
                            <legend>No analizar</legend>
                            <input type="checkbox" name="noAnalizar" class="Estilo10" >
                        </fieldset>
                    </td>
                    <td width="10">&nbsp;</td>
                    <td class="Estilo10" align="center">
                        <fieldset>
                            <legend>An&aacute;lisis general</legend>
                            <input type="checkbox" name="anaGen" class="Estilo10" >
                        </fieldset>
                    </td>
                </tr>
                <tr>
                                        
                    <%if(usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                    <td colspan="5" height="50" align="center"><input type="submit" class="Estilo10" value="Actualizar" name="agregar" onclick="return agregarMaterial(<%=compras.size()%>);"></td>
                    <td colspan="5" height="50" align="center"><input type="submit" class="Estilo10" value="Actualizar Comprador" name="actualizarComprador"></td>
                    <%}else{%>
                    <td colspan="5" height="50" align="center"><input type="submit" class="Estilo10" value="Actualizar" name="agregar" disabled></td>
                    <td colspan="5" height="50" align="center"><input type="submit" class="Estilo10" value="Actualizar Comprador"  disabled></td>
                    <%}%>
                </tr>
            </table>   
            <%sesion.setAttribute("compras2",compras);%>
            <%}catch(Exception e){
            out.println("ERROR: retornarEstadoMaterial.jsp " + e.getMessage());
            }        
            %>
            <%}else{response.sendRedirect("../mac");}%>
        </form>
    </body>
</html>

