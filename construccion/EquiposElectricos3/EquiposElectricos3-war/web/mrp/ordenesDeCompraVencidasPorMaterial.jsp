<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <html>
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
           <title>Manufactura asistida por computadora</title>
       </head>
       <body>
           <%try{
               ConexionEJB cal = new ConexionEJB();  
               Object ref =cal.conexionBean("OrdenBean");
               OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
               OrdenRemote ordenCompra= ordenHome.create();
               java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy"); 
               java.text.NumberFormat number = new java.text.DecimalFormat(" ");
               java.text.NumberFormat nf = java.text.NumberFormat.getInstance();
               nf.setMaximumFractionDigits(2);
               nf.setMinimumFractionDigits(0);
               %>
           <br>
           <center><h5><font class="Estilo10">Material&nbsp;&nbsp;<%=request.getParameter("codpro")%>&nbsp;&nbsp;Descripci&oacute;n&nbsp;&nbsp;<%=request.getParameter("descri")%></font></h5></center>
           <br>
           <center><h5><font class="Estilo10">Ordenes de Compra vencidas</font></h5></center>
           <br>
           <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
               <tr bgcolor="#B9DDFB" align="center">
                   <td><font class="Estilo9">numero Orden</font> </td>
                   <td><font class="Estilo9">Linea </font></td>
                   <td><font class="Estilo9">Proveedor</font></td>
                   <td><font class="Estilo9">rut Proveedor</font></td>
                   <td><font class="Estilo9">fecha Pedido</font> </td>
                   <td><font class="Estilo9">fecha Entrega</font></td>
                   <td><font class="Estilo9">Cantidad Pedida</font> </td>
                   <td><font class="Estilo9">Cantidad Pendiente</font></td>
                   <td><font class="Estilo9">Moneda</font></td>
                   <td><font class="Estilo9">Precio Unitario</font></td>
               </tr>
               
               <%
               Vector ordenesDeCompra = ordenCompra.ordenDeCompraInvalidas(request.getParameter("codpro"),20,"");
               Enumeration enum1 = ordenesDeCompra.elements();
               while(enum1.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enum1.nextElement();%>
               <tr>
                   <td align="center"><font class="Estilo9"><%=orden.getNum_orden_compra()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getLinea()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getProveedor()%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getRut())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_pedido())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_entrega())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getCan_pedida())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getCan_pendiente())%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getDescMoneda()%></font></td>
                   <%--if(orden.getMoneda()==1){%>
                   <td align="center"><font class="Estilo9">Peso</font></td>
                   <%}else if(orden.getMoneda()==2){%>
                   <td align="center"><font class="Estilo9">D&oacute;lar</font></td>
                   <%}else{%>
                   <td align="center"><font class="Estilo9">Euro</font></td>
                   <%}--%>
                   
                   <%if(orden.getDescMoneda().equals("PESO")){%>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getPrecio())%></font></td>
                   <%}else{%>
                   <td align="center"><font class="Estilo9"><%=nf.format(orden.getPrecio())%></font></td>
                   <%}%>
                   
               </tr>
               <%}%>  
           </table>
          
           <%}catch(Exception e){e.printStackTrace();}%> 
       </body>
   </html>