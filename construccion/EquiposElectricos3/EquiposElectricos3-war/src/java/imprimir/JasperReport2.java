/*
 * JasperReport2.java
 *
 * Created on 1 de octubre de 2008, 16:56
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package imprimir;

import java.text.SimpleDateFormat;
import java.util.*;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import sis.logger.Loger;

public class JasperReport2
{

    public JasperReport2()
    {
    }

    public HashMap jasper(Vector datos, String pedido, String usuario)
    {
        net.sf.jasperreports.engine.JasperPrint jasperPrint = null;
        net.sf.jasperreports.engine.JasperReport jasperReport = null;
        HashMap pars = new HashMap();
        try
        {
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
            String cantidad = "";
            String codigo = "";
            String um = "";
            String descripcion = "";
            pars.put("empresa", "Vale de consumo");
            pars.put("npedido", "N\272 pedido: " + pedido);
            pars.put("usuario", "Usuario: " + usuario);
            pars.put("fecha", "Fecha: " + formateador.format(new Date()));
            for(int t = 0; t < datos.size(); t++)
            {
                codigo = codigo + ((EstructuraPorEquipo)datos.elementAt(t)).getCodigoMatPrima() + "\n";
                cantidad = cantidad + String.valueOf(((EstructuraPorEquipo)datos.elementAt(t)).getCantidad()) + "\n";
                um = um + ((EstructuraPorEquipo)datos.elementAt(t)).getUnidadMedida() + "\n";
                descripcion = descripcion + ((EstructuraPorEquipo)datos.elementAt(t)).getDescripcion() + "\n";
            }

            pars.put("codigo", codigo);
            pars.put("cantidad", cantidad);
            pars.put("um", um);
            pars.put("descripcion", descripcion);
        }
        catch(Exception e)
        {
            (new Loger()).logger("JasperReport2.jsp ", "ERROR: JasperReport2(): " + e.getMessage(), 0);
        }
        return pars;
    }
}