/*
 * EquiposEnProduccionIreport.java
 *
 * Created on 14 de diciembre de 2010, 09:37 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package imprimir;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import mac.ee.pla.mrp.bean.MrpBean;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author dbarra
 */
public class EquiposEnProduccionIreport {
    
   public JasperPrint lista(Vector list){
	String reportName = "";
	JRBeanCollectionDataSource dataSource;
	JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map pars = new HashMap();
        
        
    try{
        
           reportName = "E:\\reportes\\equiposEnProduccion";
       
    	//1-Llenar el datasource con la informacion de la base de datos o de donde este, en este caso "hardcode"
    	Collection lista = list;
    	dataSource = new JRBeanCollectionDataSource(lista);
    	
    	//2-Compilamos el archivo XML y lo cargamos en memoria
        jasperReport = JasperCompileManager.compileReport(
        		reportName+".jrxml");
        
        //3-Llenamos el reporte con la información (de la DB) y parámetros necesarios para la consulta
        jasperPrint = JasperFillManager.fillReport(
            jasperReport, pars, dataSource);
        
    }catch (Exception e){
      System.out.println(e);
      e.printStackTrace();
    }
    return jasperPrint;
  }
 
    
}
