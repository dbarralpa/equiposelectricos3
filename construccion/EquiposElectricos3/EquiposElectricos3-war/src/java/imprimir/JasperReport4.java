/*
 * JasperReport4.java
 *
 * Created on 2 de octubre de 2008, 11:40
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package imprimir;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo;
import mac.ee.pla.mrp.bean.MrpBean;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;


public class JasperReport4{
  public JasperPrint lista(Vector list,int valor,String usuario,String numero,String supervisor, String equipo, String fecha){
	String reportName = "";
	JRBeanCollectionDataSource dataSource;
	JasperReport jasperReport;
        JasperPrint jasperPrint = null;
        Map pars = new HashMap();
        pars.put("N_PEDIDO", numero);
        pars.put("USUARIO", usuario);
        pars.put("SUPERVISOR", supervisor);
        pars.put("N_EQUIPO", equipo);
        pars.put("N_FECHA", fecha);
        
    try{
        if(valor == 1){
           reportName = "E:\\reportes\\consumo";
        }else if(valor == 2){
           reportName = "E:\\reportes\\pedidoDeMateriales"; 
        }else{
           reportName = "E:\\reportes\\checkList"; 
        }
    	//1-Llenar el datasource con la informacion de la base de datos o de donde este, en este caso "hardcode"
    	Collection lista = populateData(list,valor);
    	dataSource = new JRBeanCollectionDataSource(lista);
    	
    	//2-Compilamos el archivo XML y lo cargamos en memoria
        jasperReport = JasperCompileManager.compileReport(
        		reportName+".jrxml");
        
        //3-Llenamos el reporte con la información (de la DB) y parámetros necesarios para la consulta
        jasperPrint = JasperFillManager.fillReport(
            jasperReport, pars, dataSource);
        
        //4-Exportamos el reporte a pdf y lo guardamos en disco
        /*JasperExportManager.exportReportToPdfFile(
            jasperPrint, reportName+".pdf");*/
        
    }catch (Exception e){
      new sis.logger.Loger().logger("jasperReport4.java " , " jasperReport(): "+e.getMessage(),0);
    }
    return jasperPrint;
  }
  private Collection populateData(Vector list,int valor){
      MrpBean mrp = new MrpBean();
      NumberFormat number = NumberFormat.getInstance();
      number.setMaximumFractionDigits(2);
	  Collection lista = new ArrayList();
          if(valor == 0 || valor == 2){
            for(int i=0;i<list.size();i++){
              lista.add(new Impresion(((EstructuraPorEquipo)list.elementAt(i)).getCodigoMatPrima(),((EstructuraPorEquipo)list.elementAt(i)).getDescripcion(),((EstructuraPorEquipo)list.elementAt(i)).getUnidadMedida(),number.format(((EstructuraPorEquipo)list.elementAt(i)).getCantidad()),mrp.ubicacionBodMaterial(((EstructuraPorEquipo)list.elementAt(i)).getCodigoMatPrima())));
          }  
        }else{
           for(int i=0;i<list.size();i++){
              lista.add(new Impresion(((EstructuraPorEquipo)list.elementAt(i)).getCodigoMatPrima(),((EstructuraPorEquipo)list.elementAt(i)).getDescripcion(),((EstructuraPorEquipo)list.elementAt(i)).getUnidadMedida(),number.format(((EstructuraPorEquipo)list.elementAt(i)).getCantidad()),number.format(((EstructuraPorEquipo)list.elementAt(i)).getCantidadEntregada())));
          }   
        }
          
	  
	  
	  return lista;
  }
}
