/*
 * Impresion.java
 *
 * Created on 10 de octubre de 2008, 14:28
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package imprimir;

/**
 *
 * @author dbarra
 */
public class Impresion {
    private String codigo;
    private String descripcion;
    private String um;
    private String cantidad;
    private String ubi;
    private String cantidadEntregada;
    
    /** Creates a new instance of Impresion */
   public Impresion(String codigo,String descripcion,String um,String cantidad,String ubi){
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.um = um;
		this.cantidad = cantidad;
                this.ubi = ubi;
	}
   /*public Impresion(String codigo,String descripcion,String um,String cantidad,String cantidadEntregada){
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.um = um;
		this.cantidad = cantidad;
                this.cantidadEntregada = cantidadEntregada;
	}*/

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUm() {
        return um;
    }

    public void setUm(String um) {
        this.um = um;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getUbi() {
        return ubi;
    }

    public void setUbi(String ubi) {
        this.ubi = ubi;
    }

    public String getCantidadEntregada() {
        return cantidadEntregada;
    }

    public void setCantidadEntregada(String cantidadEntregada) {
        this.cantidadEntregada = cantidadEntregada;
    }

   
    
}
