/*
 * Reclamos.java
 *
 * Created on 27 de agosto de 2014, 12:19
 */

package forms;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.mrp.bean.MrpBean;
import mac.ee.pla.mrp.clase.ModReclamo;
import mac.excel.AsExcel;

/**
 *
 * @author dbarra
 * @version
 */
public class Reclamos extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        
        MrpBean mrp = new MrpBean();
        try{
            AsExcel excel = new AsExcel();
            if(request.getParameter("reclamo") != null && request.getParameter("reclamo").trim().equals("codigo")){
                ArrayList equipos = (ArrayList)request.getSession().getAttribute("equipos");
                
                excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/reclamos.xls");
                excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
                
                int indice = 10;
                
                
                for(int i=0; i<equipos.size(); i++){
                    ModReclamo rec = (ModReclamo)equipos.get(i);
                    excel.setValor(0, indice+i, 1, rec.getNumero(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 2, rec.getEquipo().getNp(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 3, rec.getEquipo().getLinea(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 4, rec.getEquipo().getSerie(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 5, rec.getEquipo().getCliente(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 6, rec.getEquipo().getDescripcion(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 7, rec.getEquipo().getFamilia(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 8, rec.getEquipo().getKva(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 9, rec.getEquipo().getMonto(), "DET_TNUMERO");
                    
                    double diesPorCiento = (rec.getEquipo().getMonto()*10)/100;
                    double aa = diesPorCiento/ 30;
                    excel.setValor(0, indice+i, 10, (aa * rec.getAtraso()), "DET_TNUMERO");
                    
                    excel.setValor(0, indice+i, 11, rec.getMaterial().getMateriaPrima(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 12, rec.getMaterial().getDescripcion(), "DET_TTEXTO");
                    
                    
                    excel.setValor(0, indice+i, 13, rec.getMaterial().getCantidad(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 14, rec.getMaterial().getComprador(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 15, format.format(rec.getFechaIngreso()), "DET_TTEXTO");
                    if(rec.getFechaAjuste() != null){
                        excel.setValor(0, indice+i, 16, format.format(rec.getFechaAjuste()), "DET_TTEXTO");
                    }else{
                        excel.setValor(0, indice+i, 16, "", "DET_TTEXTO");
                    }
                    
                    excel.setValor(0, indice+i, 17, rec.getAtraso(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 18, rec.getMaterial().getSolicitado(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 19, rec.getMaterial().getStockMin(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 20, rec.getMaterial().getStockMax(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 21, rec.getMaterial().getSaldo(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 22, rec.getMaterial().getLeadTime(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 23, rec.getMaterial().getOc(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 24, rec.getProceso(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 25, rec.getObservacion(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 26, rec.getRazon(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 27, rec.getUsuario(), "DET_TTEXTO");
                }
            }
            
            
            if(request.getParameter("reclamo") != null && request.getParameter("reclamo").trim().equals("planos")){
                ArrayList equipos = (ArrayList)request.getSession().getAttribute("reclamosPlanos");
                
                excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/reclamosPlanos.xls");
                excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
                
                int indice = 10;
                
                
                for(int i=0; i<equipos.size(); i++){
                    ModReclamo rec = (ModReclamo)equipos.get(i);
                    excel.setValor(0, indice+i, 1, rec.getNumero(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 2, rec.getEquipo().getNp(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 3, rec.getEquipo().getLinea(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 4, rec.getEquipo().getSerie(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 5, rec.getEquipo().getCliente(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 6, rec.getEquipo().getDescripcion(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 7, rec.getEquipo().getFamilia(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 8, rec.getEquipo().getKva(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 9, rec.getEquipo().getMonto(), "DET_TNUMERO");
                    
                    double diesPorCiento = (rec.getEquipo().getMonto()*10)/100;
                    double aa = diesPorCiento/ 30;
                    excel.setValor(0, indice+i, 10, (aa * rec.getAtraso()), "DET_TNUMERO");
                    
                    excel.setValor(0, indice+i, 11, format.format(rec.getFechaIngreso()), "DET_TTEXTO");
                    if(rec.getFechaAjuste() != null){
                        excel.setValor(0, indice+i, 12, format.format(rec.getFechaAjuste()), "DET_TTEXTO");
                    }else{
                        excel.setValor(0, indice+i, 12, "", "DET_TTEXTO");
                    }
                    
                    excel.setValor(0, indice+i, 13, rec.getAtraso(), "DET_TNUMERO");
                    excel.setValor(0, indice+i, 14, rec.getProceso(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 15, rec.getObservacion(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 16, rec.getRazon(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 17, rec.getProyectista(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 18, rec.getDetencion(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 19, rec.getNumPlano(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 20, rec.getUsuSolution(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 21, rec.getObserSolution(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 22, rec.getTipoPlano(), "DET_TTEXTO");
                    excel.setValor(0, indice+i, 23, rec.getUsuario(), "DET_TTEXTO");
                }
            }
            excel.borrarHoja();
            //response.setContentType("application/vnd.ms-excel");
            response.setContentType("application/vnd.oasis.opendocument.spreadsheet-template");
            response.setHeader("Content-Disposition","attachment;filename=\"Reclamos.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            //System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        
    }
    
    private String datoNull(String dato){
        if(dato!=null){
            return dato;
        }else{
            return "Sin dato";
        }
    }
    
    private int datoNull(int dato){
        return dato;
    }
    private String datoNull(Date dato){
        if(dato!=null){
            return dato.toString();
        }else{
            return "Sin fecha";
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
