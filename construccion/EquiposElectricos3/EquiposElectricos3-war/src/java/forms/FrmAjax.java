package forms;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.mrp.bean.MrpBean;
import mac.ee.pla.mrp.clase.ModSesion;

public class FrmAjax extends HttpServlet {
    
     protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {        
        ModSesion Sesion = null;
        try{
            response.setContentType("text/html; charset=iso-8859-1");
            Sesion = (ModSesion)request.getSession().getAttribute("Sesion");            
            MrpBean  mrp = new MrpBean();
            
            PrintWriter out = response.getWriter();
            String html = "";
            
            if (request.getParameter("busqueda") != null ) {            
                if(request.getParameter("stat") != null){
                    if(request.getParameter("stat").equals("codigo")){
                        Sesion.setDatos(mrp.materiaPrimaCodigo(request.getParameter("busqueda").trim()));
                    }else if(request.getParameter("stat").equals("descrip")){
                        Sesion.setDatos(mrp.materiaPrimaDesc(request.getParameter("busqueda").trim()));
                    }else{
                        Sesion.setDatos(mrp.materiaPrimaCodigo(request.getParameter("busqueda").trim()));
                    }
                }else{
                    Sesion.setDatos(mrp.materiaPrimaDesc(request.getParameter("busqueda").trim()));
                }                
                
                if (Sesion.getDatos().isEmpty()) {
                    html += "0&";
                    html += "<div onClick=\"clickLista(this);\" onMouseOver=\"mouseDentro(this);\">" + "vacio" + "</div>";
                } else {
                    html += "1&";
                    for (int i = 0; i < Sesion.getDatos().size(); i++) {
                        html += "<div onClick=\"clickLista(this);\" onMouseOver=\"mouseDentro(this);\">" + ((String)Sesion.getDatos().get(i)).trim() + "</div>";
                    }
                }               
            }
            if (request.getParameter("indiceAjax") != null) {
                int indice = Integer.parseInt(request.getParameter("indiceAjax"));
                if (indice > -1 && !Sesion.getDatos().isEmpty()) {                    
                    String tmp = (String)Sesion.getDatos().get(indice);
                    tmp = tmp.replaceAll("<b>", "");
                    tmp = tmp.replaceAll("</b>", "");
                    Sesion.setDescripcion(tmp);
                    html = Sesion.getDescripcion();
                } else {
                    html = "";
                }
            }
            out.println(html);
            out.close();
            response.setContentType("text/html");
            request.getSession().setAttribute("Sesion", Sesion);
        }catch(Exception ex){
            //EscribeFichero.addError(ex, true);
        }        
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    
    public String getServletInfo() {
        return "Servlet FrmAjax";
    }
    
}
