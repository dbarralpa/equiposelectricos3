/*
 * EquiposProduccion.java
 *
 * Created on 16 de enero de 2015, 12:16
 */

package forms;

import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.equipo.clase.Equipo;
import mac.excel.AsExcel;

/**
 *
 * @author dbarra
 * @version
 */
public class EquiposProduccion extends HttpServlet {
    
     protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH mm:ss");
        try{
            Vector compras = (Vector)request.getSession().getAttribute("teams");
            DecimalFormat formateador = new DecimalFormat("###,###.##");
            //String state =    (String)request.getSession().getAttribute("state");
            //System.out.println(request.getParameter("orden"));
           
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(0);
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/equiposProduccion.xls");
            excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
            excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
            int indice = 0;
            for(int i=0; i<compras.size(); i++){
                excel.setValor(0, 11+indice, 1, ((Equipo)(compras.get(i))).getNp() , "DET_TNUMERO");
                excel.setValor(0, 11+indice, 2,  ((Equipo)(compras.get(i))).getLinea(), "DET_TNUMERO");
                excel.setValor(0, 11+indice, 3,  ((Equipo)(compras.get(i))).getCliente(), "DET_TTEXTO");
                excel.setValor(0, 11+indice, 4,  ((Equipo)(compras.get(i))).getFechaCliente().toString(), "DET_TTEXTO");
                excel.setValor(0, 11+indice, 5,  ((Equipo)(compras.get(i))).getExpeditacion().toString(), "DET_TTEXTO");
                excel.setValor(0, 11+indice, 6,  ((Equipo)(compras.get(i))).getStatus(), "DET_TTEXTO");
                excel.setValor(0, 11+indice, 7,  ((Equipo)(compras.get(i))).getDescripcion(), "DET_TTEXTO");
                excel.setValor(0, 11+indice, 8, formateador.format(((Equipo)(compras.get(i))).getMonto()) , "DET_TNUMERO");
                indice++;     
            }
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition","attachment;filename=\"Equipos en producción.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            //System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        
    }
    
    private String datoNull(String dato){
        if(dato!=null){
            return dato;
        }else{
            return "Sin dato";
        }
    }
    
    private int datoNull(int dato){
        return dato;
    }
    private String datoNull(Date dato){
        if(dato!=null){
            return dato.toString();
        }else{
            return "Sin fecha";
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
