/*
 * informeEquiposDeficit.java
 *
 * Created on 27 de septiembre de 2012, 15:54
 */

package forms;

import conexionEJB.ConexionEJB;
import java.io.*;
import java.net.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import javax.rmi.PortableRemoteObject;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.crp2.bean.Crp2Bean;
import mac.ee.pla.mrp.bean.MrpRemote;
import mac.ee.pla.mrp.bean.MrpRemoteHome;
import mac.excel.AsExcel;

/**
 *
 * @author dbarra
 * @version
 */
public class informeEquiposDeficit extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH mm:ss");
        NumberFormat number = new DecimalFormat(" ");
        SimpleDateFormat formate = new SimpleDateFormat("dd-MM-yyyy");
        Crp2Bean crp = new Crp2Bean();
        try{
            
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/informeEquiposConDeficitMaterial.xls");    
            excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
            excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
            
            ConexionEJB cal = new ConexionEJB();
            Object ref1 =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
            MrpRemote mrp = mrpHome.create();
            
            
            
            excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
            excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
            int indice = 0;
            Vector equipos = new Vector();
            Vector ordenesCompra = new Vector();
            Vector materiales = mrp.controlDeMateriales();
            for(int y=0;y<materiales.size();y++){
                equipos = crp.retornarEquiposConProblemas((String)((Vector)materiales.elementAt(y)).elementAt(0),formate.format(new java.util.Date()));
                float total = 0;
                for(int u=0;u<equipos.size();u++){                    
                    excel.setValor(0, 11 + indice, 1,  (String)((Vector)materiales.elementAt(y)).elementAt(0), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 2,  (String)((Vector)materiales.elementAt(y)).elementAt(1), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 3,  formate.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getExpeditacion()), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 4,  ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getNp(), "DET_TNUMERO");
                    excel.setValor(0, 11 + indice, 5,  ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getLinea(), "DET_TNUMERO");
                    excel.setValor(0, 11 + indice, 6,  ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getSerie(), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 7,  ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getDescripcion(), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 8,  number.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getCantidad()), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 9,  ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getProceso(), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 10,  ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getLeadTime(), "DET_TNUMERO");
                    excel.setValor(0, 11 + indice, 11,  (String)((Vector)materiales.elementAt(y)).elementAt(2), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 12,  number.format(((Float)((Vector)materiales.elementAt(y)).elementAt(7)).floatValue()), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 13,  number.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getStockMinimo()), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 14,  number.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getStockMaximo()), "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 15,  number.format((Float)((Vector)materiales.elementAt(y)).elementAt(3)), "DET_TTEXTO");
                    total += ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getCantidad();
                    indice++;
                }
                
                excel.setValor(0, 11 + indice, 1,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 2,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 3,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 4,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 5,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 6,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 7,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 8,  total, "DET_TNUMERO");
                excel.setValor(0, 11 + indice, 9,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 10,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 11,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 12,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 13,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 14,  "", "DET_TTEXTO");
                excel.setValor(0, 11 + indice, 15,  "", "DET_TTEXTO");
                indice++;
                Vector ordenesVencidas = new mac.ee.pla.ordenCompra.bean.OrdenBean().ordenDeCompraInvalidas((String)((Vector)materiales.elementAt(y)).elementAt(0));
                if(ordenesVencidas.size()>0){
                    
                    excel.setValor(0, 11 + indice, 1,  "N� Orden Vencida", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 2,  "Fecha Entrega", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 3,  "Cantidad Pendiente", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 4,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 5,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 6,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 7,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 8,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 9,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 10,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 11,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 12,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 13,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 14,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 15,  "", "DET_TTEXTO");
                    indice++;
                    for(int p=0;p<ordenesVencidas.size();p++){
                        
                        
                        excel.setValor(0, 11 + indice, 1,  ((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesVencidas.elementAt(p)).getNum_orden_compra(), "DET_TNUMERO");
                        excel.setValor(0, 11 + indice, 2,  ((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesVencidas.elementAt(p)).getFecha_entrega().toString(), "DET_TFECHA");
                        excel.setValor(0, 11 + indice, 3,  ((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesVencidas.elementAt(p)).getCan_pendiente(), "DET_TNUMERO");
                        excel.setValor(0, 11 + indice, 4,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 5,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 6,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 7,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 8,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 9,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 10,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 11,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 12,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 13,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 14,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 15,  "", "DET_TTEXTO");
                        indice++;
                    }
                }
                Vector ordenesFuturas = new mac.ee.pla.ordenCompra.bean.OrdenBean().ordenDeCompraValidas((String)((Vector)materiales.elementAt(y)).elementAt(0));
                if(ordenesFuturas.size()>0){
                    
                    excel.setValor(0, 11 + indice, 1,  "N� Orden Futura", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 2,  "Fecha Entrega", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 3,  "Cantidad Pendiente", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 4,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 5,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 6,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 7,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 8,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 9,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 10,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 11,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 12,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 13,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 14,  "", "DET_TTEXTO");
                    excel.setValor(0, 11 + indice, 15,  "", "DET_TTEXTO");
                    indice++;
                    for(int p=0;p<ordenesFuturas.size();p++){
                        
                        excel.setValor(0, 11 + indice, 1,  ((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesFuturas.elementAt(p)).getNum_orden_compra(), "DET_TNUMERO");
                        excel.setValor(0, 11 + indice, 2,  ((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesFuturas.elementAt(p)).getFecha_entrega().toString(), "DET_TFECHA");
                        excel.setValor(0, 11 + indice, 3,  ((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesFuturas.elementAt(p)).getCan_pendiente(), "DET_TNUMERO");
                        excel.setValor(0, 11 + indice, 4,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 5,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 6,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 7,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 8,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 9,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 10,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 11,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 12,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 13,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 14,  "", "DET_TTEXTO");
                        excel.setValor(0, 11 + indice, 15,  "", "DET_TTEXTO");
                        indice++;
                    }
                }
                
            }
            
            
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition","attachment;filename=\"Deficit_material.xls\"");       
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception e){
            
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
