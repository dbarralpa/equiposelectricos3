/*
 * ExcelCompras.java
 *
 * Created on 10 de febrero de 2011, 10:33 AM
 */

package forms;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.mrp.bean.MrpBean;
import mac.excel.AsExcel;

/**
 *
 * @author dbarra
 * @version
 */
public class ExcelComprasMes extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH mm:ss");
        MrpBean mrp = new MrpBean();
        try{
            String[] meses = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            Vector compras = mrp.retornarNenecidadesMes();
            //String state =    (String)request.getSession().getAttribute("state");
            //System.out.println(request.getParameter("orden"));
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/necesidadesMaterialMes.xls");
            excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
            excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
            int indice = 0;
            
            
            SimpleDateFormat fo = new SimpleDateFormat("yyyy-MM-dd");
            String[] fec = fo.format(new java.util.Date()).split("-");
            Calendar c1 = Calendar.getInstance();
            c1.set(Integer.parseInt(fec[0]), Integer.parseInt(fec[1])-1, 1);
            excel.setValor(0, 10, 22, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 29, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            c1.set(Integer.parseInt(fec[0]), c1.get(Calendar.MONTH)+1, 1);
            excel.setValor(0, 10, 23, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 30, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            c1.set(Integer.parseInt(fec[0]), c1.get(Calendar.MONTH)+1, 1);
            excel.setValor(0, 10, 24, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 31, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            c1.set(Integer.parseInt(fec[0]), c1.get(Calendar.MONTH)+1, 1);
            excel.setValor(0, 10, 25, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 32, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            c1.set(Integer.parseInt(fec[0]), c1.get(Calendar.MONTH)+1, 1);
            excel.setValor(0, 10, 26, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 33, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            c1.set(Integer.parseInt(fec[0]), c1.get(Calendar.MONTH)+1, 1);
            excel.setValor(0, 10, 27, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 34, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            c1.set(Integer.parseInt(fec[0]), c1.get(Calendar.MONTH)+1, 1);
            excel.setValor(0, 10, 28, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            excel.setValor(0, 10, 35, meses[c1.get(Calendar.MONTH)], "DET_TTEXTO");
            
            for(int i=0; i<compras.size(); i++){
                float necesidades = 0f;
                float saldo = 0f;
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue();
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue();
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue();
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue();
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(12)).floatValue();
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(13)).floatValue();
                necesidades += ((Float)((Vector)compras.elementAt(i)).elementAt(14)).floatValue();
                excel.setValor(0, 11+i, 0, ((String)((Vector)compras.elementAt(i)).elementAt(17)), "DET_TTEXTO");//comprador
                excel.setValor(0, 11+i, 1, ((String)((Vector)compras.elementAt(i)).elementAt(0)), "DET_TTEXTO");//codigo
                excel.setValor(0, 11+i, 2, ((String)((Vector)compras.elementAt(i)).elementAt(1)), "DET_TTEXTO");//descripcion
                excel.setValor(0, 11+i, 3, ((String)((Vector)compras.elementAt(i)).elementAt(7)), "DET_TTEXTO");//um
                excel.setValor(0, 11+i, 4, ((Float)((Vector)compras.elementAt(i)).elementAt(3)).floatValue(), "DET_TNUMERO");//consumo promedio
                
                excel.setValor(0, 11+i, 5, ((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue(), "DET_TNUMERO");//stock minimo
                
                
                excel.setValor(0, 11+i, 6, ((Float)((Vector)compras.elementAt(i)).elementAt(5)).floatValue(), "DET_TNUMERO");//stock maximo
                excel.setValor(0, 11+i, 7, ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue(), "DET_TNUMERO");//BMP
                excel.setValor(0, 11+i, 8, ((Float)((Vector)compras.elementAt(i)).elementAt(16)).floatValue(), "DET_TNUMERO");//BPT
                excel.setValor(0, 11+i, 9, ((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue(), "DET_TNUMERO");//BMP 9
                excel.setValor(0, 11+i, 10, ((Float)((Vector)compras.elementAt(i)).elementAt(15)).floatValue(), "DET_TNUMERO");//stock planta
                excel.setValor(0, 11+i, 11, ((Integer)((Vector)compras.elementAt(i)).elementAt(18)).intValue(), "DET_TNUMERO");//leadTime
                if((((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue()) >= necesidades){
                    excel.setValor(0, 11+i, 12, "SI", "DET_TTEXTO");//necesidad cubierta * BMP
                }else{
                    excel.setValor(0, 11+i, 12, "NO", "DET_TTEXTO");//necesidad cubierta * BMP
                }
                
                if((((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue()) >= ((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue() ){
                    excel.setValor(0, 11+i, 13, "SI", "DET_TTEXTO");//stock minimo cubierta * BMP
                }else{
                    excel.setValor(0, 11+i, 13, "NO", "DET_TTEXTO");//stock maximo cubierta * BMP
                }
                
                float oc = ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue();
                float bmp = ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue();
                if((oc+bmp) >= (necesidades + ((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue())){
                    excel.setValor(0, 11+i, 14, "SI", "DET_TTEXTO");//oc + bmp cubre necesidades + stock minimo
                }else{
                    excel.setValor(0, 11+i, 14, "NO", "DET_TTEXTO");//oc + bmp cubre necesidades + stock minimo
                }
                
                
                
                excel.setValor(0, 11+i, 15, ((Float)((Vector)compras.elementAt(i)).elementAt(19)).floatValue(), "DET_TNUMERO");//puc
                excel.setValor(0, 11+i, 16, (String)((Vector)compras.elementAt(i)).elementAt(24), "DET_TNUMERO");//moneda
                excel.setValor(0, 11+i, 17, ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue(), "DET_TNUMERO");//oc
                
                excel.setValor(0, 11+i, 18, ((Long)((Vector)compras.elementAt(i)).elementAt(20)).longValue(), "DET_TNUMERO");//folio oc atrasada
                excel.setValor(0, 11+i, 19, (String)((Vector)compras.elementAt(i)).elementAt(21), "DET_TTEXTO");//fecha oc atrasada
                excel.setValor(0, 11+i, 20, (String)((Vector)compras.elementAt(i)).elementAt(25), "DET_TTEXTO");//fecha oc confirmada
                excel.setValor(0, 11+i, 21, (String)((Vector)compras.elementAt(i)).elementAt(22), "DET_TTEXTO");//proveedor oc atrasada
                
                
                saldo = ((Float)((Vector)compras.elementAt(i)).elementAt(16)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue();
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 22, saldo, "CELL_MENOR_A_CERO");//mes 1
                }else{
                    excel.setValor(0, 11+i, 22, saldo, "DET_TNUMERO");//mes 1
                }
                
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 23, saldo, "CELL_MENOR_A_CERO");//mes 2
                }else{
                    excel.setValor(0, 11+i, 23, saldo, "DET_TNUMERO");//mes 2
                }
                
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 24, saldo, "CELL_MENOR_A_CERO");//mes 3
                }else{
                    excel.setValor(0, 11+i, 24, saldo, "DET_TNUMERO");//mes 3
                }
                
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 25, saldo, "CELL_MENOR_A_CERO");//mes 4
                }else{
                    excel.setValor(0, 11+i, 25, saldo, "DET_TNUMERO");//mes 4
                }
                
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(12)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 26, saldo, "CELL_MENOR_A_CERO");//mes 5
                }else{
                    excel.setValor(0, 11+i, 26, saldo, "DET_TNUMERO");//mes 5
                }
                
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(13)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 27, saldo, "CELL_MENOR_A_CERO");//mes 6
                }else{
                    excel.setValor(0, 11+i, 27, saldo, "DET_TNUMERO");//mes 6
                }
                
                
                saldo -= ((Float)((Vector)compras.elementAt(i)).elementAt(14)).floatValue();
                if(saldo < 0){
                    excel.setValor(0, 11+i, 28, saldo, "CELL_MENOR_A_CERO");//mes 7
                }else{
                    excel.setValor(0, 11+i, 28, saldo, "DET_TNUMERO");//mes 7
                }
                
                
                excel.setValor(0, 11+i, 29, ((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue(), "DET_TNUMERO");//mes 1
                excel.setValor(0, 11+i, 30, ((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue(), "DET_TNUMERO");//mes 2
                excel.setValor(0, 11+i, 31, ((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue(), "DET_TNUMERO");//mes 3
                excel.setValor(0, 11+i, 32, ((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue(), "DET_TNUMERO");//mes 4
                excel.setValor(0, 11+i, 33, ((Float)((Vector)compras.elementAt(i)).elementAt(12)).floatValue(), "DET_TNUMERO");//mes 5
                excel.setValor(0, 11+i, 34, ((Float)((Vector)compras.elementAt(i)).elementAt(13)).floatValue(), "DET_TNUMERO");//mes 6
                excel.setValor(0, 11+i, 35, ((Float)((Vector)compras.elementAt(i)).elementAt(14)).floatValue(), "DET_TNUMERO");//mes 7
                
                
                
                
                
                /*if(((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue() > 1){
                    float stockBodega = ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(16)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue();
                    if(((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue()  - (stockBodega + ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue()) > 0){
                        excel.setValor(0, 11+i, 36, ((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue()  - (stockBodega + ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue()), "DET_TNUMERO");
                    }else{
                        excel.setValor(0, 11+i, 36, 0, "DET_TNUMERO");
                    }
                }else{
                    float stockBodega = ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(16)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue();
                    if((stockBodega + ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue()) < (((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue() + ((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue())){
                        excel.setValor(0, 11+i, 36, (((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue() + ((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue()) - (stockBodega + ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue()), "DET_TNUMERO");
                    }else{
                        excel.setValor(0, 11+i, 36, 0, "DET_TNUMERO");
                    }
                }*/
                float stockBodega = ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(16)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(23)).floatValue();
                if((stockBodega + ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue()) < (((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue() + ((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue())){
                       
                        excel.setValor(0, 11+i, 36, (((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue() + ((Float)((Vector)compras.elementAt(i)).elementAt(26)).floatValue())-(stockBodega + ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue()), "DET_TNUMERO");
                    }else{
                        excel.setValor(0, 11+i, 36, 0, "DET_TNUMERO");
                    }
                
                
                
            }
            excel.borrarHoja();
            //response.setContentType("application/vnd.ms-excel");
            response.setContentType("application/vnd.oasis.opendocument.spreadsheet-template");
            response.setHeader("Content-Disposition","attachment;filename=\"Necesidades_Material_Mes.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            //System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        
    }
    
    private String datoNull(String dato){
        if(dato!=null){
            return dato;
        }else{
            return "Sin dato";
        }
    }
    
    private int datoNull(int dato){
        return dato;
    }
    private String datoNull(Date dato){
        if(dato!=null){
            return dato.toString();
        }else{
            return "Sin fecha";
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
