/*
 * EstadoMateriales.java
 *
 * Created on 23 de febrero de 2011, 09:17 AM
 */

package forms;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.excel.AsExcel;

/**
 *
 * @author dbarra
 * @version
 */
public class EstadoMateriales extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
         SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
         SimpleDateFormat formatHora = new SimpleDateFormat("HH mm:ss");
        try{
            Vector compras = (Vector)request.getSession().getAttribute("compras2");
            //String state =    (String)request.getSession().getAttribute("state");
            //System.out.println(request.getParameter("orden"));
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/EstadoMateriales.xls");    
            excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
            excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
            int indice = 0;
             for(int i=0; i<compras.size(); i++){   
                
                excel.setValor(0, 11+i, 1, ((String)((Vector)compras.elementAt(i)).elementAt(0)), "DET_TTEXTO");
                excel.setValor(0, 11+i, 2, ((String)((Vector)compras.elementAt(i)).elementAt(1)), "DET_TTEXTO");
                excel.setValor(0, 11+i, 3, ((String)((Vector)compras.elementAt(i)).elementAt(7)), "DET_TTEXTO");
                excel.setValor(0, 11+i, 4, ((Float)((Vector)compras.elementAt(i)).elementAt(3)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 5, ((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 6, ((Float)((Vector)compras.elementAt(i)).elementAt(5)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 7, ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 8, ((Float)((Vector)compras.elementAt(i)).elementAt(19)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 9, ((Float)((Vector)compras.elementAt(i)).elementAt(21)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 10, ((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue(), "DET_TNUMERO");
                excel.setValor(0, 11+i, 11, ((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue(), "DET_TNUMERO");    
                excel.setValor(0, 11+i, 12, ((((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(21)).floatValue())+
                                                 (((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue()))-
                                                 (((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue()+((Float)((Vector)compras.elementAt(i)).elementAt(4)).floatValue()), "DET_TNUMERO");
                
              
             }
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition","attachment;filename=\"Estado_de_materiales.xls\"");       
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            //System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }        
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
