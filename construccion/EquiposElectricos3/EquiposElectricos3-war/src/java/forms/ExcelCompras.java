/*
 * ExcelCompras.java
 *
 * Created on 10 de febrero de 2011, 10:33 AM
 */

package forms;

import java.io.*;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.mrp.bean.MrpBean;
import mac.excel.AsExcel;

/**
 *
 * @author dbarra
 * @version
 */
public class ExcelCompras extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        //response.setContentType("text/html;charset=UTF-8");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH mm:ss");
        try{
            Vector compras = (Vector)request.getSession().getAttribute("compras1");
            //String state =    (String)request.getSession().getAttribute("state");
            //System.out.println(request.getParameter("orden"));
            MrpBean mrp = new MrpBean();
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            nf.setMinimumFractionDigits(0);
            AsExcel excel = new AsExcel();
            excel.crearExcel(request.getSession().getServletContext().getRealPath("/") + "templates/excel/ComprasRequeridas.xls");
            excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
            excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
            int indice = 0;
            for(int i=0; i<compras.size(); i++){
                if(!request.getParameter("comprador").equals("todos")){
                    if(((String)((Vector)compras.elementAt(i)).elementAt(4)).equals(request.getParameter("comprador"))){
                        excel.setValor(0, 11+indice, 1, ((String)((Vector)compras.elementAt(i)).elementAt(0)), "DET_TTEXTO");
                        excel.setValor(0, 11+indice, 2, ((String)((Vector)compras.elementAt(i)).elementAt(1)), "DET_TTEXTO");
                        excel.setValor(0, 11+indice, 3, ((String)((Vector)compras.elementAt(i)).elementAt(3)), "DET_TTEXTO");
                        excel.setValor(0, 11+indice, 4, ((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 5, ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 6, ((Float)((Vector)compras.elementAt(i)).elementAt(7)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 7, ((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 8, ((Float)((Vector)compras.elementAt(i)).elementAt(25)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 9, ((Float)((Vector)compras.elementAt(i)).elementAt(27)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 10, ((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 11, ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 12, ((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 13, (((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue()+
                                ((Float)((Vector)compras.elementAt(i)).elementAt(27)).floatValue()+
                                ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue())-
                                (((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue()+
                                ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()), "DET_TNUMERO");
                        if(mrp.validarUsuarioALink(request.getParameter("usuario"),"compras")){
                            Object[] val = mrp.ultimoPrecioCompra(((String)((Vector)compras.elementAt(i)).elementAt(0)));
                            excel.setValor(0, 11+indice, 14, ((Double)val[0]).doubleValue(), "DET_TNUMERO");
                            excel.setValor(0, 11+indice, 15, ((String)val[1]), "DET_TTEXTO");
                        }else{
                            excel.setValor(0, 11+indice, 14, 0, "DET_TNUMERO");
                            excel.setValor(0, 11+indice, 15, "", "DET_TTEXTO");
                        }
                        indice++;
                    }
                }else{
                    excel.setValor(0, 11+indice, 1, ((String)((Vector)compras.elementAt(i)).elementAt(0)), "DET_TTEXTO");
                    excel.setValor(0, 11+indice, 2, ((String)((Vector)compras.elementAt(i)).elementAt(1)), "DET_TTEXTO");
                    excel.setValor(0, 11+indice, 3, ((String)((Vector)compras.elementAt(i)).elementAt(3)), "DET_TTEXTO");
                    excel.setValor(0, 11+indice, 4, ((Float)((Vector)compras.elementAt(i)).elementAt(10)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 5, ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 6, ((Float)((Vector)compras.elementAt(i)).elementAt(7)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 7, ((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 8, ((Float)((Vector)compras.elementAt(i)).elementAt(25)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 9, ((Float)((Vector)compras.elementAt(i)).elementAt(27)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 10, ((Float)((Vector)compras.elementAt(i)).elementAt(2)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 11, ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 12, ((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue(), "DET_TNUMERO");
                    excel.setValor(0, 11+indice, 13, (((Float)((Vector)compras.elementAt(i)).elementAt(11)).floatValue()+
                            ((Float)((Vector)compras.elementAt(i)).elementAt(27)).floatValue()+
                            ((Float)((Vector)compras.elementAt(i)).elementAt(8)).floatValue())-
                            (((Float)((Vector)compras.elementAt(i)).elementAt(9)).floatValue()+
                            ((Float)((Vector)compras.elementAt(i)).elementAt(6)).floatValue()), "DET_TNUMERO");
                    if(mrp.validarUsuarioALink(request.getParameter("usuario"),"compras")){
                        Object[] val = mrp.ultimoPrecioCompra(((String)((Vector)compras.elementAt(i)).elementAt(0)));
                        excel.setValor(0, 11+indice, 14, ((Double)val[0]).doubleValue(), "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 15, ((String)val[1]), "DET_TTEXTO");
                    }else{
                        excel.setValor(0, 11+indice, 14, 0, "DET_TNUMERO");
                        excel.setValor(0, 11+indice, 15, "", "DET_TTEXTO");
                    }
                    indice++;
                    
                }
            }
            excel.borrarHoja();
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition","attachment;filename=\"Compras_Requeridas.xls\"");
            excel.getWorkBook().write(response.getOutputStream());
            excel.cerrarPlantilla();
        }catch(Exception ex){
            //System.out.println(ex.getMessage());
            ex.printStackTrace();
            
        }
        
    }
    
    private String datoNull(String dato){
        if(dato!=null){
            return dato;
        }else{
            return "Sin dato";
        }
    }
    
    private int datoNull(int dato){
        return dato;
    }
    private String datoNull(Date dato){
        if(dato!=null){
            return dato.toString();
        }else{
            return "Sin fecha";
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
