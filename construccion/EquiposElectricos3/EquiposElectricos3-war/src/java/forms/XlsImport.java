/*
 * XlsImport.java
 *
 * Created on 11 de febrero de 2011, 02:50 PM
 */

package forms;

import java.io.*;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.*;
import mac.ee.pla.mrp.bean.MrpBean;
import mac.excel.AsExcel;
import mac.excel.ModAuxiliar;

/**
 *
 * @author dbarra
 * @version
 */
public class XlsImport extends HttpServlet {
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
     AsExcel excel = new AsExcel();
     MrpBean mrp = new MrpBean();
     int cant = 0;
     String devolver = "No fueron actualizados todos los datos";
     String usuario = (String)request.getSession().getAttribute("user");
      try{
        
            excel.crearExcelSinEstilos(request.getParameter("subir"));
            ArrayList datos = excel.leerDatosHoja(0);
            excel.cerrarPlantilla();
            String html = "";
            /*for (int i = 0; i < datos.size(); i++) {
                String codigo = "";
                int valor = 0;
                ModAuxiliar fila = (ModAuxiliar) datos.get(i);
                NumberFormat nf = new DecimalFormat(" ");
                if (fila.getNombre0().equals("")) {
                    codigo = nf.format(ModAuxiliar.redondear(fila.getValor0(), 0));
                } else {
                    codigo = fila.getNombre0();
                }
                valor = Integer.parseInt(nf.format(ModAuxiliar.redondear(fila.getValor1(), 0)).trim());
                boolean estado = mrp.buscarCodigoStockPlanta(codigo.trim(),valor);
                if(estado){
                    cant++;
                }
                nf = null;
            }
            if(cant == datos.size()){
                mrp.updateFechaStockPlantaConn();
                devolver = "Se actualizaron todos los registros";
            }
            mrp = null;
            excel = null;
            System.out.println(html+"\n");*/
            devolver = mrp.buscarCodigoStockPlanta(datos,request.getParameter("todos"),usuario);
            request.getSession().setAttribute("user",null);
            response.sendRedirect("mrp/subirArchivoStockPlanta.jsp?idSesion="+request.getParameter("idSesion")+"&usuario="+request.getParameter("usuario")+"&tipou="+request.getParameter("tipou")+"&devolver="+devolver);
    }catch (Exception ex) {
           mrp = null;
           excel = null;
          //  PrmApp.addError(ex, true);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    // </editor-fold>
}
