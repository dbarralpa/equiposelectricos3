<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pro.calidadProgramacion.bean.*,mac.ee.pla.equipo.clase.Equipo,java.text.*"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
    <%try{
                
                java.text.NumberFormat number = new java.text.DecimalFormat(" ");
                java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
                NumberFormat format = NumberFormat.getInstance();
                format.setMaximumFractionDigits(1);
                format.setMinimumFractionDigits(0);
                Vector equipos = new Vector(); 
                String fecha = "";
                float monto = 0;
                CalidadDeProgramacionBean programacion = new CalidadDeProgramacionBean();  
                equipos =  programacion.pedidosEnBodega(request.getParameter("fecha").substring(0,8),request.getParameter("fecha").substring(8,16));
        %>
        <br><br>
        
        <center><img src="../Images/formula.jpg" border="1" width="710px" height="218px"/></center>
        <br>
        <center><h5><font class="Estilo10">Equipos entregados atrasados a bodega o revisión final</font></h5></center>
       <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
         
         
          <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
                <td align="center"><font class="Estilo9">N°</font></td>
                <td align="center"><font class="Estilo9">Np</font></td>
                <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                <td align="center"><font class="Estilo9">Fecha Cliente</font></td>
                <td align="center"><font class="Estilo9">Fecha Bodega</font></td>
                <td align="center"><font class="Estilo9">Cliente</font></td>
                <td align="center"><font class="Estilo9">Monto</font></td>
                <td align="center"><font class="Estilo9">Serie</font></td>
                <td align="center"><font class="Estilo9">Kva</font></td>
                <td align="center"><font class="Estilo9">Descripci&oacute;n</font></td>
            </tr>
            
           <%int contador = 0;
            for(int u=0;u<equipos.size();u++){%>
            <%if(((Equipo)equipos.elementAt(u)).getEstado() == 1){%> 
            <tr><!--"#CCFFFF"color rojo-->
            
                <td align="center"><font class="Estilo9"><%=contador+1%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getNp()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)equipos.elementAt(u)).getFechaCliente())%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)equipos.elementAt(u)).getFechaTermino())%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getNombreCliente()%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(((Equipo)equipos.elementAt(u)).getMonto())%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getSerie()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getKva()%>&nbsp;</font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getDescripcion()%>&nbsp;</font></td>
                
            </tr>
            
            <%monto += ((Equipo)equipos.elementAt(u)).getMonto();
            contador++;%>
            <%}}%>
        </table>
        <center><h5><font class="Estilo10">Monto:&nbsp;<%=format.format(monto)%></font></h5></center>
        <br>
        <center><h5><font class="Estilo10">Equipos entregados a tiempo a bodega o revisión final</font></h5></center>
        <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
         
         
          <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
                <td align="center"><font class="Estilo9">N°</font></td>
                <td align="center"><font class="Estilo9">Np</font></td>
                <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                <td align="center"><font class="Estilo9">Fecha Cliente</font></td>
                <td align="center"><font class="Estilo9">Fecha Bodega</font></td>
                <td align="center"><font class="Estilo9">Cliente</font></td>
                <td align="center"><font class="Estilo9">Monto</font></td>
                <td align="center"><font class="Estilo9">Serie</font></td>
                <td align="center"><font class="Estilo9">Kva</font></td>
                <td align="center"><font class="Estilo9">Descripci&oacute;n</font></td>
            </tr>
            
           <%
            monto = 0;
            contador = 0;
            for(int u=0;u<equipos.size();u++){%>
            <%if(((Equipo)equipos.elementAt(u)).getEstado() == 0){%> 
            <tr><!--"#CCFFFF"color rojo-->
            
                <td align="center"><font class="Estilo9"><%=contador+1%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getNp()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)equipos.elementAt(u)).getFechaCliente())%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)equipos.elementAt(u)).getFechaTermino())%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getNombreCliente()%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(((Equipo)equipos.elementAt(u)).getMonto())%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getSerie()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getKva()%>&nbsp;</font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getDescripcion()%>&nbsp;</font></td>
                
            </tr>
            <%monto += ((Equipo)equipos.elementAt(u)).getMonto();
            contador++;%>
            <%}}%>
        </table>
<center><h5><font class="Estilo10">Monto:&nbsp;<%=format.format(monto)%></font></h5></center>
<br>
    <%}catch(Exception e){
        out.println("ERROR: IndiceCumplimiento.jsp " + e.getMessage());
        }%>
    </body>
</html>
