<%@page import="mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.equipo.clase.Equipo,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Manufactura asistida por computadora</title>
       
        
    </head>
    <body>
        <%  
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        if(idSesion.equals(sesion.getId())){
        %>
        <form action="setearStockPlanta_1.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
            <%              
            try{
                EstructuraPorEquipo estruc = new EstructuraPorEquipo();
               
                Vector consumos = estruc.detalleConsumos(request.getParameter("codpro"));
            %>
            <br>
            <table align="center" width="100%">
                <tr>
                    <td align="center" class="Estilo10">C&oacute;digo:&nbsp;<%=request.getParameter("codpro")%></td>
                </tr>
                <tr>
                    <td>
                        <table width="50%" class="Estilo10" align="center" border="1">
                            
                            <tr>
                                <td align="left">Pedido</td>
                                <td align="left">Linea</td>
                                <td align="left">C&oacute;digo</td>
                                <td align="left">Um</td>
                                <td align="left">Consumo</td>
                            </tr>
                            
                            <!--tr>
                                <td colspan="5" height="15">&nbsp;</td>
                            </tr-->
                            
                            <%for(int i=0;i<consumos.size();i++){%>
                            <tr>
                                <td align="left" width="100"><%=((Equipo)consumos.elementAt(i)).getNp()%></td>
                                <td align="left" width="100"><%=((Equipo)consumos.elementAt(i)).getLinea()%></td>
                                <td align="left" width="150"><%=((Equipo)consumos.elementAt(i)).getCodPro()%></td>
                                <td align="left" width="150"><%=((Equipo)consumos.elementAt(i)).getProceso()%></td>
                                <td align="left" width="150"><%=((Equipo)consumos.elementAt(i)).getConsumo()%></td>
                            </tr>
                            
                            <%}%>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
        </form><br>
        
        <%}catch(Exception e){e.printStackTrace();}%>
        <%}%>
    </body>
</html>