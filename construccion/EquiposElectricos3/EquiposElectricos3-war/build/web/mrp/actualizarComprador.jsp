<%@page import="java.io.*,java.util.*,javax.naming.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Manufactura asistida por computadora</title>
    </head>
    <body>
        <form action="actualizarComprador.jsp" method="post" name="form1">
            <%
            HttpSession sesion    = request.getSession();
            try{
                
                ConexionEJB cal = new ConexionEJB();
                Object ref =cal.conexionBean("estructuraEquipoBean");
                estructuraEquipoRemoteHome estructuraHome = (estructuraEquipoRemoteHome) PortableRemoteObject.narrow(ref, estructuraEquipoRemoteHome.class);
                estructuraEquipoRemote estructura= estructuraHome.create();
                MrpBean mrp = new MrpBean();
                if(request.getParameter("comprador") == null){
                    estructura.setCodigoMaterial(request.getParameter("codigo"));
                    estructura.setFechaEntrega(request.getParameter("fechaEntrega"));
                    sesion.setAttribute("compradores",mrp.compradores());
                }
            
            %>
            
            <br>
            <center><h5><font class="Estilo10">Cambiar Comprador</font></h5></center>
            <br>
            <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                <tr bgcolor="#B9DDFB">
                    <td class="Estilo10" colspan="2" align="center">Comprador a modificar</td>
                </tr>
                <tr>
                    <td><font class="Estilo9">Material</font></td>
                    <td><font class="Estilo9">Comprador</font></td>
                </tr>
                <tr>
                    <td align="center"><font class="Estilo9"><%=estructura.getCodigoMaterial()%></font></td>
                    <td align="center"><font class="Estilo9">
                            <select name="comprador" onchange="form1.submit()" class="Estilo10">
                                <option value="no">Elija un Comprador</option>
                                <%for(int i=0;i<((Vector)sesion.getAttribute("compradores")).size();i++){%>    
                                <%EstructuraPorEquipo es = (EstructuraPorEquipo)((Vector)sesion.getAttribute("compradores")).elementAt(i);%>
                                <option value="<%=es.getUsuario()%>"><%=es.getUsuario()%></option>
                                <%}%>
                            </select>
                        </font>
                    </td>
                </tr>
            </table>
        </form><br><br>
        
        <%
        if(request.getParameter("comprador") != null && !request.getParameter("comprador").equals("no")){
                    byte veri = mrp.actualizarComprador(estructura.getCodigoMaterial(),request.getParameter("comprador"),null);
                    if(veri == (byte)1){
        
        
        %>
        <center><h5><font class="Estilo10">Actualiz&oacute; Comprador</font></h5></center>
        <center><a href="javascript:window.opener.document.location.reload();self.close()">CERRAR</a></center>
        <%}else{%>
        <center><h5><font class="Estilo10">No actualiz&oacute; comprador</font></h5></center>
        <%}}
        }catch(Exception e){e.printStackTrace();}%>
        
        
    </body>
</html>