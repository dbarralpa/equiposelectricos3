<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
        <title>Manufactura asistida por computador</title>
    </head>
    <body onload=ajustaCeldas()>
        <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               /*String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               sesion.setAttribute("idSesion",idSesion);
               out.println(idSesion);
               out.println(usuario);
               out.println(tipou);*/
               if(idSesion.equals(sesion.getId())){
        %>
    <br>
    <center><h4><font class="Estilo10">BALANCE POR MATERIAL</font></h4></center>
    <br>
    <center><a href="materialesProyectados.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>#<%=request.getParameter("codPro")%>"><img src="../Images/volver2.jpg" border="0"/></a></center>
    <br>
    
    <%
         try{
            ConexionEJB cal = new ConexionEJB();
            Object ref1 =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
            MrpRemote mrp = mrpHome.create();    
            Vector balance = mrp.balancePorMaterial();
            java.text.NumberFormat number = new java.text.DecimalFormat(" ");
            %>
            
            <table border=0 bgcolor=scrollbar align=center>
            <td>
            <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor=#cccccc>
                <tr bgcolor="#B9DDFB">
                    <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                    <td align="center"><font class="Estilo9">MATERIAL</font></td>
                    <td align="center"><font class="Estilo9">DESCRIPCION</font></td>
                    <td align="center"><font class="Estilo9">FECHA</font></td>
                    <td align="center"><font class="Estilo9">STOCK</font></td>
                    <td align="center"><font class="Estilo9">OC</font></td>
                    <td align="center"><font class="Estilo9">CANTIDAD</font></td>
                    <td align="center"><font class="Estilo9">BALANCE</font></td>
                    <td align="center"><font class="Estilo9">&nbsp;&nbsp;&nbsp;</font></td>
                </tr>
            </table>
            <div style="overflow:auto; height:380px; padding:0">
            <%if(balance.size() > 0){%>
            <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                <%for(int u=0;u<balance.size();u++){
                if(((String)((Vector)balance.elementAt(u)).elementAt(0)).equals(request.getParameter("codPro"))){
                %>
                <tr class="Estilo10">
                    <td align="center"><%=request.getParameter("codPro")%></td>
                    <td align="center"><%=request.getParameter("descri")%></td>
                    <td align="center"><%=(String)((Vector)balance.elementAt(u)).elementAt(5)%></td>
                    <td align="center"><%=number.format(((Float)((Vector)balance.elementAt(u)).elementAt(1)))%></td>
                    <td align="center"><%=number.format(((Float)((Vector)balance.elementAt(u)).elementAt(2)))%></td>
                    <td align="center"><%=number.format(((Float)((Vector)balance.elementAt(u)).elementAt(3)))%></td>
                    <td align="center"><%=number.format(((Float)((Vector)balance.elementAt(u)).elementAt(4)))%></td>
                </tr>
                <%}}%>
            </table>
            <%}%>
            </div>
            </td>
           </table>
        <%}catch(Exception e){
           out.println("ERROR: balancePorMaterial " + e.getMessage());
        }
    %>
    <%}else{response.sendRedirect("../mac");}%>
    </body>
</html>