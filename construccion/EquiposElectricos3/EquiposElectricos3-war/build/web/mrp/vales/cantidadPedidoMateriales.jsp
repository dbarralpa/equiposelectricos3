<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <html>
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
           <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css" />
           <title>Manufactura asistida por computadora</title>
           <SCRIPT LANGUAGE="Javascript">

            var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return ((key >= 48 && key <= 57) || key == 46);
            
            }
            
            function vali(){
            if(document.form1.cantidad.value == ""){
              alert('Debe agregar una cantidad');
              document.form1.cantidad.focus();
              return false;
            }
         }
         function form_click() {
                 var myForm = document.form1;
                 myForm.cantidad.focus();
            }
         </SCRIPT>      

       </head>
       <body onload="form_click();">
           <form action="cantidadPedidoMateriales.jsp" method="post" name="form1">
           <%              
               try{
                 HttpSession sesion    = request.getSession();  
                 ConexionEJB cal = new ConexionEJB();
                 Object ref =cal.conexionBean("MrpBean");
                 MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
                 MrpRemote mrp= mrpHome.create();
               if(request.getParameter("guardar") == null){
                  sesion.setAttribute("material",request.getParameter("material"));
                  sesion.setAttribute("numero",request.getParameter("numero"));
                  sesion.setAttribute("estatu",request.getParameter("status"));
                  sesion.setAttribute("type",request.getParameter("type"));
                  sesion.setAttribute("estadoCantidad",request.getParameter("estadoCantidad"));
                 }
             
               %>
               
                   <br>
                   <center><h5><font class="Estilo10">Cambiar cantidad</font></h5></center>
                   <br>
                   <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                       <tr bgcolor="#B9DDFB">
                           <td class="Estilo10" colspan="2" align="center">Cantidad a cambiar</td>
                       </tr>
                       <tr>
                           <td><font class="Estilo9">Material</font></td>
                           <td><font class="Estilo9">Cantidad</font></td>
                       </tr>
                       <tr>
                           <td><font class="Estilo9"><%=sesion.getAttribute("material")%></font></td>
                           <td><font class="Estilo9"><input name="cantidad" type="text" onKeyPress="return acceptNum(event)" autocomplete="off"></font></td>
                       </tr>
                   </table>
                   <br><br>
                   <center><input type="submit" value="Guardar" name="guardar" class="Estilo10" onclick="return vali();"></center>
               </form><br>
               
               <%
               if(request.getParameter("guardar") != null){
                  //int verificador = mrp.actualizarCantidadRequeridas(Integer.parseInt((String)sesion.getAttribute("numero")),(String)sesion.getAttribute("material"),Integer.parseInt(request.getParameter("cantidad")),Integer.parseInt((String)sesion.getAttribute("np")),Integer.parseInt((String)sesion.getAttribute("linea")),((Integer)sesion.getAttribute("tipo")).intValue());
                  if(((String)sesion.getAttribute("estatu")).equals("2")){
                     int veri = mrp.actualizarCantidadRequeridas(Integer.parseInt((String)(sesion.getAttribute("numero"))),
                                                     (String)sesion.getAttribute("material"),
                                                     Float.parseFloat(request.getParameter("cantidad")),
                                                     Integer.parseInt((String)sesion.getAttribute("type")));%> 
                 <%if(veri == 1){%>
                  <center><h5><font class="Estilo10">Grab&oacute; los datos en la base de datos</font></h5></center>
                  <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center>
                  <%}else if(veri == -1){%>
                  <center><h5><font class="Estilo10">No hay stock suficiente para esa cantidad</font></h5></center>
                  <%}else{%>
                    <center><h5><font class="Estilo10">No grab� los datos en la base de datos</font></h5></center>
                  <%}%>                 
          
                 
                  <%}else{
                  for(int i=0;i<((Vector)sesion.getAttribute("necesidades")).size();i++){
                      if(((EstructuraPorEquipo)((Vector)sesion.getAttribute("necesidades")).elementAt(i)).getCodigoMatPrima().equals((String)sesion.getAttribute("material"))){
                         if(((String)sesion.getAttribute("estadoCantidad")).equals("1")){
                           ((EstructuraPorEquipo)((Vector)sesion.getAttribute("necesidades")).elementAt(i)).setCantidad(Float.parseFloat(request.getParameter("cantidad")));     
                         } 
                          if(((String)sesion.getAttribute("estadoCantidad")).equals("2")){
                            ((EstructuraPorEquipo)((Vector)sesion.getAttribute("necesidades")).elementAt(i)).setStockPlanta(Float.parseFloat(request.getParameter("cantidad"))); 
                          }
                        
                      }
                  }%>
              <center><h5><font class="Estilo10">Dato Actualizado</font></h5></center>     
             <%}%>
             <%sesion.setAttribute("entre","2");%>
                  <!--center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center-->

           <%}}catch(Exception e){e.printStackTrace();}%>
       </body>
   </html>