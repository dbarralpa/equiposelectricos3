<%@page import="mac.contador.Contador,java.text.NumberFormat,java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.equipo.clase.Equipo,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
    HttpSession sesion    = request.getSession();
    String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    /*String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    sesion.setAttribute("idSesion",idSesion);
    out.println(idSesion);
    out.println(usuario);
    out.println(tipou);*/
    if(idSesion.equals(sesion.getId())){
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ver saldo a la cuenta</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
        
    </head>
    <body onload="ajustarCeldas(5)">
        <form name="myform" method="post" action="saldarMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">
            <%try{
            if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("")){
               //Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
            }    
            ConexionEJB cal = new ConexionEJB();  
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            Vector historial = new Vector();
            Vector usuarios = mrp.supervisorVales();
            //if(request.getParameter("usuarios") != null && !request.getParameter("usuarios").equals("nada")){
            //historial = mrp.historialVales("20101230");    
            mrp.stockPlanta("20101230");
            //}
            if(request.getParameter("actualizar") != null){
            for(int i=0;i<historial.size();i++){
            if(request.getParameter("saldar"+i) != null){   
            mrp.descartarMaterialesValeConsumo(((EstructuraPorEquipo)historial.elementAt(i)).getCodigoMatPrima(),request.getParameter("usuarios"));    
            }
            historial = mrp.historialVales(request.getParameter("usuarios"));        
            }
            }
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            
            
            %>
            <br>
            <center><h5><font class="Estilo10">SISTEMA DE ADMINISTRACI&Oacute;N DE PEDIDO DE MATERIALES</font></h5></center>
            <br><br>
            <center><h5><font class="Estilo10">Saldar materiales de vales de consumo</font></h5></center>
            <br>
            <table cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    
                    <td align="center" class="Estilo10"><a href="javascript:Abrir_ventana1('historialPedido.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>')">Historial pedidos</a></td> 
                    <td width="100">&nbsp;</td>
                    <td align="center">
                        <select name="usuarios" class="Estilo10" onchange="myform.submit();">
                            <option value="nada" >Elija un supervisor</option>    
                            <%for(int a=0;a<usuarios.size();a++){%>
                            <option value="<%=((EstructuraPorEquipo)usuarios.elementAt(a)).getUsuario()%>" <%if(request.getParameter("usuarios") != null && request.getParameter("usuarios").equals(((EstructuraPorEquipo)usuarios.elementAt(a)).getUsuario())){%>selected<%}%>><%=mrp.nombreUsuario(((EstructuraPorEquipo)usuarios.elementAt(a)).getUsuario())%></option>    
                            <%}%>
                        </select>
                    </td>
                </tr>     
            </table>   
            <br>
            <table border=0 bgcolor=scrollbar align=center>
                <tr>
                    <td>
                        <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc">
                            <tr bgcolor="#B9DDFB" align="center">
                                <td align="center" class="Estilo10" width="100">C&oacute;digo</td>
                                <td align="center" class="Estilo10" width="200">Descripci&oacute;n</td>
                                <td align="center" class="Estilo10">Cantidad</td>
                                <td align="center" class="Estilo10">Saldar</td>
                                <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                                
                            </tr>
                        </table>
                        <div style="overflow:auto; height:280px; padding:0">
                            <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>     
                                <%
                                for(int i=0;i<historial.size();i++){%>
                                
                                <tr bgcolor="#FFFFCC" onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFCC"'>
                                    <td align="center" class="Estilo10" width="150"><a href="javascript:Abrir_ventana1('detallePorMaterialVales.jsp?codigo=<%=((EstructuraPorEquipo)historial.elementAt(i)).getCodigoMatPrima()%>&descripcion=<%=((EstructuraPorEquipo)historial.elementAt(i)).getDescripcion()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>')"><%=((EstructuraPorEquipo)historial.elementAt(i)).getCodigoMatPrima()%></a></td>
                                    <td align="center" class="Estilo10" width="200"><%=((EstructuraPorEquipo)historial.elementAt(i)).getDescripcion()%></td>
                                    <td align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)historial.elementAt(i)).getCantidad())%></td>
                                    <td align="center" class="Estilo10"><input type="checkbox" name="saldar<%=i%>" class="Estilo10" ></td>
                                </tr>
                                <%}%>
                                <%if(historial.size() < 15){%>
                                <%for(int i=0;i<20;i++){%>
                                <tr>
                                    <td align="center" class="Estilo10" width="150">&nbsp;</td>
                                    <td align="center" class="Estilo10" width="200">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                    <td align="center" class="Estilo10">&nbsp;</td>
                                </tr>
                                <%}}%>
                            </table>    
                        </div>
                        
                    </td>  
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFFFFF" colspan="4"> <input type="submit" name="actualizar"  value="Eliminar"/> </td>
                </tr>
            </table>
            <br>
            
            
            <%}catch(Exception e){
            new sis.logger.Loger().logger("saldarMaterial.jsp " , " saldarMaterial() " + e.getMessage(),0);
            }%>
        </form>
    </body>
    <%}else{response.sendRedirect("../mac");}%>  
</html>
