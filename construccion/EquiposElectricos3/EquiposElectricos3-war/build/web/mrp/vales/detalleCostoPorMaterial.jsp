<%@page contentType="text/html" import="java.text.*,java.util.Date,java.text.SimpleDateFormat,conexionEJB.ConexionEJB,java.util.Vector,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.crp2.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,javax.rmi.PortableRemoteObject"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Control de materiales por pedido</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
    </head>
    <body>
        <%
        
        NumberFormat number = NumberFormat.getInstance();
        number.setMaximumFractionDigits(0);
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        %>
        
        <%
        try{
            
            ConexionEJB cal = new ConexionEJB();
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            Vector materialesPorEquipo = mrp.detalleControlPorMaterial(request.getParameter("fechaIni"),request.getParameter("fechaFin"),request.getParameter("material"));
        %>
        
        
        <form method="post" name="form1" action="detalleCostoPorMaterial.jsp">
            
                        <table width="700" border="0" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td height="26" colspan="11" valign="middle" class="Estilo10" align="center"> 
                                    
                                    <h5>Detalle de costo por Materiales</h5>
                                </td>
                            </tr>
                            
                            
                            <tr>
                                <td class="Estilo10">Material:</td>
                                <td class="Estilo10"><%=request.getParameter("material")%></td>
                                <td width="10">&nbsp;</td>
                                <td class="Estilo10">Costo:</td>
                                <td class="Estilo10"><%=request.getParameter("costo")%></td>
                                <td width="10">&nbsp;</td>
                                <td class="Estilo10">Cant real:</td>
                                <td class="Estilo10"><%=request.getParameter("cantReal")%></td>
                                <td width="10">&nbsp;</td>
                                <td class="Estilo10">Cant Ppta:</td>
                                <td class="Estilo10"><%=request.getParameter("cantPpta")%></td>
                            </tr>
                        </table>
              
                        
                        
                        <table width="700" border="1" align="center" cellpadding="0" cellspacing="0" class="Estilo10">
                           
                            <tr>
                                <td  height="21" align="center"  class="Estilo10">N&uacute;mero pedido</td>
                                <td  align="center" class="Estilo10">Np</td>
                                <td  align="center" class="Estilo10">Linea</td>
                                <td   class="Estilo10" align="center">Cant entregada</td>
                                <td   class="Estilo10" align="center">Grupo</td>                                
                            </tr>
                            <%for(int i=0;i<materialesPorEquipo.size();i++){%>
                            <tr>
                                
                                <td valign="middle" align="center" class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getNumPedido()%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getNumDoc()%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getLinea()%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getReal())%></td>
                                <td valign="middle" align="center" class="Estilo10"><%=((EstructuraPorEquipo)materialesPorEquipo.elementAt(i)).getProceso()%></td>
                                
                            </tr>
                            <%}%>
                           
                        </table> 
                  
            
        </form>
        <%}catch(Exception e){
        new sis.logger.Loger().logger("detalleCostoPorMaterial.jsp " , " detalleCostoPorMaterial "+e.getMessage(),0);
        }%> 
    </body>
</html>