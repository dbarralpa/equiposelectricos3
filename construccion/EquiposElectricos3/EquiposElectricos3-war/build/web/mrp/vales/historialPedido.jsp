<%@page contentType="text/html" import="mac.ee.pla.mrp.bean.*, mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,java.util.Vector" session="true"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Historial de pedidos</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
        <script type="text/javascript" src="../../js/overlib.js"></script>
        <script language="JavaScript">
             function validar(){ 
               
              var indice = document.form1.usuarios.selectedIndex; 
             if(document.form1.pedido.value != ""){
                 document.form1.estado.value = 'Buscar por pedido';       
                 form1.submit();
              }else if(document.form1.np.value != ''){
                 document.form1.estado.value = 'Buscar np';       
                 form1.submit();
              }else if(document.form1.fechaIni.value != "" || document.form1.fechaFin.value != ""){
                 if(document.form1.fechaIni.value != "" && document.form1.fechaFin.value != ""){
                 document.form1.estado.value = 'Rango de fechas';       
                 form1.submit();
                 }else{
                 alert('Debe ingresar las dos fechas');
                 return false;
                 }
              }else if(document.form1.usuarios.options[indice].value != 'nada'){
                 document.form1.estado.value = 'Usuario';       
                 form1.submit();
              }else if(document.form1.material.value != ''){
                 document.form1.estado.value = 'Buscar material';       
                 form1.submit();
              }else{
              alert('Debe ingresar alguna categoria');
              }
    }   
        </script>     
        <style type="text/css" media="all">
            td.uno {
            border:hidden;
            }
            table.dos {
            border: 1px solid #999999;
            }
        </style>
    </head>
    <body>
        <%
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        /*String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        sesion.setAttribute("idSesion",idSesion);
        out.println(idSesion);
        out.println(usuario);
        out.println(tipou);*/
        if(idSesion.equals(sesion.getId())){
            Vector pedidos = new Vector();
            MrpBean mrp = new MrpBean();
            Vector usuVales = mrp.usuariosVales();
            String eleccion = "";
            String parame = "";
            Vector usuarios = mrp.usuariosVales();
            if(request.getParameter("pedido") != null && !request.getParameter("pedido").equals("")){
                pedidos =  mrp.buscarPedido(request.getParameter("pedido"),(byte)1,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = request.getParameter("estado");
                parame = request.getParameter("pedido");
            }else if(request.getParameter("vigentes") != null){
                pedidos =  mrp.buscarPedido(request.getParameter("vigentes"),(byte)4,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = request.getParameter("estado");
                parame = "";
            }else if(request.getParameter("usuarios") != null && !request.getParameter("usuarios").equals("nada")){
                pedidos =  mrp.buscarPedido(request.getParameter("usuarios"),(byte)2,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = request.getParameter("estado");
                parame = request.getParameter("usuarios");
            }else if(request.getParameter("fechaIni") != null && !request.getParameter("fechaIni").equals("") && request.getParameter("fechaFin") != null && !request.getParameter("fechaFin").equals("")){
                pedidos =  mrp.buscarPedido(request.getParameter("fechaIni")+" "+request.getParameter("fechaFin"),(byte)3,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = request.getParameter("estado");
                parame = request.getParameter("fechaIni")+" "+request.getParameter("fechaFin");
            }else if(request.getParameter("valor") != null){
                pedidos =  mrp.buscarPedido(usuario,(byte)5,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = "Usuario";
                parame = usuario;
            }else if(request.getParameter("np") != null && !request.getParameter("np").equals("")){
                pedidos =  mrp.buscarPedido(request.getParameter("np"),(byte)6,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = request.getParameter("estado");
                parame   = request.getParameter("np");
            }else if(request.getParameter("material") != null && !request.getParameter("material").equals("")){
                pedidos =  mrp.buscarPedido(request.getParameter("material"),(byte)7,request.getParameter("pedir"),request.getParameter("listo"),request.getParameter("enproceso"),request.getParameter("entregado"),request.getParameter("aprobado"));
                eleccion = request.getParameter("estado");
                parame   = request.getParameter("material");
            }else{}   
          //  out.println(request.getParameter("pedir") + "---" + request.getParameter("listo") + "---" + request.getParameter("enproceso") + "---" + request.getParameter("entregado") + "---" + request.getParameter("aprobado"));
        %>
        <form action="historialPedido.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
            <center><h4><font class="Estilo10">HISTORIAL DE PEDIDOS</font></h4></center>
            <br>
            <table cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                            <tr>
                                <td><input type="checkbox" name="aprobado" class="Estilo10" value="Aprobado" <%if(request.getParameter("estado") == null || request.getParameter("aprobado") != null){%>checked<%}%>>&nbsp;Aprobado</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="pedir" class="Estilo10" value="Pedir autorizaci�n" <%if(request.getParameter("estado") == null || request.getParameter("pedir") != null){%>checked<%}%>>&nbsp;Pedir autorizaci&oacute;n</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="listo" class="Estilo10" value="Listo para entregar"  <%if(request.getParameter("estado") == null || request.getParameter("listo")  != null){%>checked<%}%>>&nbsp;Listo para retirar</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="enproceso" class="Estilo10" value="En proceso"  <%if(request.getParameter("estado") == null || request.getParameter("enproceso")  != null){%>checked<%}%>>&nbsp;En proceso</td>
                            </tr>
                            <tr>
                                <td><input type="checkbox" name="entregado" value="Entregado" class="Estilo10"  <%if(request.getParameter("entregado") != null){%>checked<%}%>>&nbsp;Entregado</td>
                            </tr>
                        </table>    
                    </td>
                    <td width="20">&nbsp;</td>
                    <td>
                        <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                            
                            <tr class="Estilo10">
                                <td class="Estilo10" align="center">
                                    <fieldset>
                                        <legend>Por n� de Pedido</legend>
                                        <input type="text" name="pedido" class="Estilo10" >
                                        &nbsp;&nbsp;
                                        <input type="button" value="ir" name="pedido1" class="Estilo10" onclick="validar();">
                                    </fieldset>
                                </td>
                                <td width="20">&nbsp;</td>
                                <td class="Estilo10" align="center">
                                    <fieldset>
                                        <legend>NP</legend>
                                        <input type="text" name="np" class="Estilo10" >
                                        &nbsp;&nbsp;
                                        <input type="button" value="ir" name="np1" class="Estilo10" onclick="validar();">
                                    </fieldset>
                                </td>
                                <td width="20">&nbsp;</td>
                                <td class="Estilo10" align="center">
                                    <fieldset>
                                        <legend>Buscar por usuario</legend>
                                        <select name="usuarios" class="Estilo10" onchange="validar();">
                                            <option value="nada" >Elija un usuario</option>    
                                            <%for(int a=0;a<usuarios.size();a++){%>
                                            <option value="<%=((EstructuraPorEquipo)usuVales.elementAt(a)).getUsuario()%>"><%=((EstructuraPorEquipo)usuVales.elementAt(a)).getUsuario()%></option>    
                                            <%}%>
                                        </select>
                                    </fieldset>
                                </td>
                            </tr>
                            <tr>    
                                <td class="Estilo10" align="left" >
                                    <fieldset>
                                        <legend>Pedido por fecha</legend>
                                        <table cellpadding="0" cellspacing="0" border="0">
                                            <tr>
                                                <td>
                                                    <DIV id="popCal" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                                    <IFRAME name="popFrame" src="../../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                                                    <SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
                                                    <TABLE align="center" CELLPADDING="0" CELLSPACING="0">
                                                        <TR>
                                                            <TD ALIGN="center">
                                                                Desde:&nbsp;<input class="Estilo10" readOnly name="fechaIni" id="fechaIni" size="12" >
                                                                <a onclick="popFrame.fPopCalendar(fechaIni,fechaIni,popCal);return false">
                                                                <img src="../../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                                            </td>
                                                            <td width="28">&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <DIV id="popCal1" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                                    <IFRAME name="popFrame1" src="../../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                                                    <SCRIPT event=onclick() for=document>popCal1.style.visibility = "hidden";</SCRIPT>
                                                    <TABLE align="center" CELLPADDING="0" CELLSPACING="0">
                                                        <TR>
                                                            <TD ALIGN="center">
                                                                Hasta:&nbsp;<input class="Estilo10" readOnly name="fechaFin" id="fechaFin" size="12" onchange="validar()">
                                                                <a onclick="popFrame1.fPopCalendar(fechaFin,fechaFin,popCal1);return false">
                                                                <img src="../../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                                            </td>
                                                            <td width="10">&nbsp;</td>
                                                            <td align="center"><input type="button" value="ir" name="enviar" class="Estilo10" onclick="validar();"> </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset>
                                </td>  
                                <td width="20">&nbsp;</td>
                                <td class="Estilo10" align="center">
                                    <fieldset>
                                        <legend>Material</legend>
                                        <input type="text" name="material" class="Estilo10" >
                                        &nbsp;&nbsp;
                                        <input type="button" value="ir" name="material1" class="Estilo10" onclick="validar();">
                                    </fieldset>
                                </td>
                                
                            </tr>
                            
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <center class="Estilo10"><font size="2"><%=eleccion%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=parame%></font></center>
            <br>
            
            
            
            
            
            <table border=1 bgcolor="#cccccc" cellspacing=0 cellpadding=2 align="center">
                
                <%
                if(pedidos.size() > 0){%>
                <tr bgcolor="#B9DDFB">
                    <td class="Estilo10" align="center" >N� pedido</td>
                    <td class="Estilo10" align="center" width="100">Area</td>
                    <td class="Estilo10" align="center" width="100">Usuario</td>
                    <td class="Estilo10" align="center" width="100">Supervisor</td>
                    <td class="Estilo10" align="center" >Estado</td>
                    <td class="Estilo10" align="center" width="100">Prioridad</td>
                    
                </tr>
                <%for(int i=0;i<pedidos.size();i++){%>
                
                <tr bgcolor="#FFFF00">
                    <td align="center" valign="middle" class="Estilo10"> <a href="javascript:Abrir_ventana2('imprimirVales.jsp?usuario=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getUsuario()%>&numero=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido()%>&estado10=1&supervisor=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getUsuAprobar()%>&equipo=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getEstado()%>&fecha=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getFechaIn()%>')"    
                                                                                onmouseover="return overlib('<%=mrp.imprimirPedidos(((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido())%>', STICKY, CAPTION, 'N� pedido&nbsp;&nbsp;&nbsp;<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido()%>', WIDTH, 600,ABOVE,OFFSETX,50,FGCOLOR,'#FFFFFF');"  
                                                                                onmouseout="return nd();"> 
                        <%=((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido()%></a>
                    </td>
                    <!--td class="Estilo10" align="center" ><--%=((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido()--%></td-->        
                    <td class="Estilo10" align="center" width="100"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getProceso()%></td>
                    <td class="Estilo10" align="center" width="100"><%=mrp.nombreUsuario(((EstructuraPorEquipo)pedidos.elementAt(i)).getUsuario())%></td>
                    <td class="Estilo10" align="center" width="100"><%=mrp.nombreUsuario(((EstructuraPorEquipo)pedidos.elementAt(i)).getUsuAprobar())%></td>
                    <td class="Estilo10" align="center" ><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getDescripcionEstado()%></td>
                    <td class="Estilo10" align="center" width="100"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getPrioridad()%></td>
                </tr>
                <%}%>
                <tr class="Estilo10">
                    <td align="center" class="Estilo10" colspan="5">Pedidos de materiles</td>
                </tr>
            </table>
            <%}else{%>
            <center><h5><font class="Estilo10">No hay pedidos con estos par&aacute;metros</font></h5></center>
            <%}%>
            <table border="0" cellspacing=0 cellpadding=2 align="right" width="40%">
                <tr>
                    <td align="center" height="60">
                        <!--input type="button"  value="Cerrar esta ventana" onclick="window.close();" class="Estilo10"-->
                        <input type="image" onclick="window.close();" onmouseover="this.src = '../../Images/cerrarVentana_.jpg'" onmouseout="this.src = '../../Images/cerrarVentana.jpg'"  name="agruparmarcados" src="../../Images/cerrarVentana.jpg" width="100" height="16" border="0"/>
                    </td>
                </tr>
            </table>
            
            <input type="hidden" name="estado">
        </form>
        <%}else{response.sendRedirect("../mac");}%>  
    </body>
</html>
