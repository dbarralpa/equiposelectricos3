<%@page contentType="text/html" import="mac.excel.AsExcel,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,java.util.Vector,java.util.Date,java.text.SimpleDateFormat,java.io.*" session="true"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title></title>
    </head>
    <body> 
        <% 
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat formatHora = new SimpleDateFormat("HH mm:ss");
        HttpSession sesion    = request.getSession();
        try{
        Vector toExcel = (Vector)sesion.getAttribute("toExcel");
        AsExcel excel = new AsExcel();
        if(request.getParameter("estado").equals("0")){
        excel.crearExcel(sesion.getServletContext().getRealPath("/") + "templates/excel/controDeCosto.xls");    
        }if(request.getParameter("estado").equals("1")){
        excel.crearExcel(sesion.getServletContext().getRealPath("/") + "templates/excel/controDeCostoPorMaterial.xls");    
        }        
        excel.setValor(0, 4, 4, format.format(new Date()), "DET_TTEXTO");
        excel.setValor(0, 5, 4, formatHora.format(new Date()), "DET_TTEXTO");
        
        if(request.getParameter("estado").equals("0")){
        int fila = 11;
        
        EstructuraPorEquipo estruc = null;
        for(int i=0; i<toExcel.size(); i++){
        estruc = (EstructuraPorEquipo)toExcel.get(i);                
        excel.setValor(0, 11+i, 1, estruc.getCodigoMatPrima(), "DET_TTEXTO");
        excel.setValor(0, 11+i, 2, estruc.getDescripcion(), "DET_TTEXTO");
        excel.setValor(0, 11+i, 3, estruc.getUnidadMedida(), "DET_TTEXTO");
        excel.setValor(0, 11+i, 4, estruc.getCantidad(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 5, estruc.getPresupuesto(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 6, estruc.getReal(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 7, estruc.getDiferencia(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 8, estruc.getPresupuesto1(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 9, estruc.getReal1(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 10,estruc.getDiferencia1(), "DET_TNUMERO");      
        estruc = null;
        fila += 1;
        }
        
        //fila = fila +2;
        excel.setValor(0, fila, 3,"Totales: ", "PIE_TTEXTO");      
        excel.setValor(0, fila, 4,(String)sesion.getAttribute("cantidad"), "DET_TTEXTO");
        excel.setValor(0, fila, 5,(String)sesion.getAttribute("presupuesto"), "DET_TTEXTO");
        excel.setValor(0, fila, 6,(String)sesion.getAttribute("real"), "DET_TTEXTO");
        excel.setValor(0, fila, 7,(String)sesion.getAttribute("diferencia"), "DET_TTEXTO");
        excel.setValor(0, fila, 8,(String)sesion.getAttribute("presupuesto1"), "DET_TTEXTO");
        excel.setValor(0, fila, 9,(String)sesion.getAttribute("real1"), "DET_TTEXTO");
        excel.setValor(0, fila, 10,(String)sesion.getAttribute("diferencia1"), "DET_TTEXTO");
        
        excel.borrarHoja();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment;filename=\"contro_De_Costo.xls\"");   
        }
        if(request.getParameter("estado").equals("1")){
         int fila = 11;
        
        EstructuraPorEquipo estruc = null;
        for(int i=0; i<toExcel.size(); i++){
        estruc = (EstructuraPorEquipo)toExcel.get(i);                
        excel.setValor(0, 11+i, 1, estruc.getCodigoMatPrima(), "DET_TTEXTO");
        excel.setValor(0, 11+i, 2, estruc.getDescripcion(), "DET_TTEXTO");
        excel.setValor(0, 11+i, 3, estruc.getUnidadMedida(), "DET_TTEXTO");
        excel.setValor(0, 11+i, 4, estruc.getReal(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 5, estruc.getPresupuesto(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 6, estruc.getReal1(), "DET_TNUMERO");
        excel.setValor(0, 11+i, 7, estruc.getPresupuesto1(), "DET_TNUMERO");   
        estruc = null;
        fila += 1;
        }
        
        //fila = fila +2;
        excel.setValor(0, fila, 3,"Totales: ", "PIE_TTEXTO");      
        excel.setValor(0, fila, 4,(String)sesion.getAttribute("real"), "DET_TTEXTO");        
        excel.setValor(0, fila, 5,(String)sesion.getAttribute("presupuesto"), "DET_TTEXTO");
        excel.setValor(0, fila, 6,(String)sesion.getAttribute("real1"), "DET_TTEXTO");
        excel.setValor(0, fila, 7,(String)sesion.getAttribute("presupuesto1"), "DET_TTEXTO");
        excel.borrarHoja();
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-Disposition","attachment;filename=\"controDeCostoPorMaterial.xls\"");    
        }
        excel.getWorkBook().write(response.getOutputStream());
        excel.cerrarPlantilla();
        
        }catch(Exception e){out.println(e.getMessage());}        
        %>
    </body>
</html>
