<%@page import="java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Imprimir compras</title>
    </head>
    <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
     <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    <body onkeypress="printPage()">
<%
try{
    
HttpSession sesion    = request.getSession(); 
SimpleDateFormat formate = new SimpleDateFormat("dd-MM-yyyy"); 
SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
NumberFormat number = new DecimalFormat(" ");
NumberFormat nf = NumberFormat.getInstance();
nf.setMaximumFractionDigits(1);
nf.setMinimumFractionDigits(0);
long fecha = 0;
long fecha1 = 0;
Vector compras = (Vector)sesion.getAttribute("buy");%>
 <br><br>
 <div id="divButtons">
   <center><h4><font class="Estilo10">Clic en Aceptar para imprimir</font></h4></center>  
 </div>
 <center><h4><font class="Estilo10">COMPRAS REQUERIDAS</font></h4></center>
 
 <!--center><input type="button" onclick="printPage" class="Estilo10" value="Imprimir" /></center-->
  <br>
                            <table class="Estilo10" border="1" cellspacing="0" cellpadding="2" bgcolor="#cccccc" align="center" onkeypress="printPage()">
                                <tr bgcolor="#B9DDFB">
                                    <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                                    <td align="center"><font class="Estilo10">C&oacute;digo</font></td>
                                    <td align="center"><font class="Estilo10">Descripci&oacute;n</font></td>
                                    <td align="center"><font class="Estilo10" title="cantidad a pedir">Can Req</font></td>
                                    <td align="center"><font class="Estilo10" title="Necesidad por material">Nec mat</font></td>
                                    <td align="center"><font class="Estilo10" title="Consumo promedio">Con Pro</font></td>
                                    <td align="center"><font class="Estilo10" title="Stock bodega">Stock</font></td>
                                    <td align="center"><font class="Estilo10" title="Unidad de medida">um</font></td>
                                    <td align="center"><font class="Estilo10">Com</font></td>
                                    <td align="center"><font class="Estilo10" title="&oacute;rdenes de compra vencidas">Oc v</font></td>
                                    <td align="center"><font class="Estilo10" title="&oacute;rdenes de compra futuras">Oc f</font></td>
                                    <td align="center" title="d&iacute;as ocupados para comprar"><font class="Estilo10">D&iacute;as</font></td>
                                    <td align="center"><font class="Estilo10" title="agrupar pedidos para sap">Sap</font></</td>
                                    <td align="center" class="Estilo10">Saldo</td>
                                    <td align="center" class="Estilo10" title="Cobertura">Cob</td>
                                    <td align="center">&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                          
                            <% for(int u=0;u<compras.size();u++){%>
                            <tr class="Estilo10">
                            
                            <td align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(0)).substring(0,5)%>&nbsp;-&nbsp;<%=((String)((Vector)compras.elementAt(u)).elementAt(0)).substring(5,10)%></td>
             
                            <td align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase()%></td>

                            
                            
                            <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(2))%></td>
                           
                            
                            
                            
                            <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(13))%></td>
                             <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(14))%></td>
                             
                            <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(15))%></td>
                            <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(3)%></td>
                            
                            <td align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(4)).substring(0,2)%></td>
                            
                        </td>
                       
                        
                        <td align="center"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(11)).floatValue())%></td>
                    
                       
                        <td align="center"><%=number.format(((Float)((Vector)compras.elementAt(u)).elementAt(12)).floatValue())%></td>
                    
                        <%fecha = ((java.sql.Date)((Vector)compras.elementAt(u)).elementAt(6)).getTime() / 86400000;
                        fecha1= new java.sql.Date(0).valueOf(format.format(new java.util.Date())).getTime() / 86400000;  
                        %>
                        <td align="center"><%=fecha1-fecha%></td>
   
                        
                        <td align="center"><%=(Integer)((Vector)compras.elementAt(u)).elementAt(17)%></td>
                       
                      
                        
                        <%float saldo = (((Float)((Vector)compras.elementAt(u)).elementAt(15)).floatValue()+((Float)((Vector)compras.elementAt(u)).elementAt(11)).floatValue()+((Float)((Vector)compras.elementAt(u)).elementAt(12)).floatValue())-((Float)((Vector)compras.elementAt(u)).elementAt(13)).floatValue();
                          float cobertura = ((Float)((Vector)compras.elementAt(u)).elementAt(14)).floatValue();%>
                        
                        <td align="center"><%=number.format(saldo)%></td>  
                        <%if(cobertura == 0){%>
                        <td align="center">0,0</td>  
                        <%}else{%>
                        <td align="center"><%=nf.format(saldo/cobertura)%></td>  
                        <%}%>
                        <td align="center">&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <%
                        }
                        %>
                   
                    </table>
                    <br><br>
    <%}catch(Exception e){
                   out.println("ERROR: imprimir.jsp " + e.getMessage());
                   }        
                   %>
    </body>
</html>
