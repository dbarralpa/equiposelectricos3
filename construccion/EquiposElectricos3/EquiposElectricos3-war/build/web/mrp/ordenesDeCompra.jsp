<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <html>
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
           <title>Manufactura asistida por computadora</title>
       </head>
       <body>
           <%try{
               ConexionEJB cal = new ConexionEJB();  
               Object ref =cal.conexionBean("OrdenBean");
               OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
               OrdenRemote ordenCompra= ordenHome.create();      
               java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy"); 
               java.text.NumberFormat number = new java.text.DecimalFormat(" ");
               %>
           <center><h5><font class="Estilo10">Material&nbsp;&nbsp;<%=request.getParameter("codpro")%>&nbsp;&nbsp;Descripci&oacute;n&nbsp;&nbsp;<%=request.getParameter("descri")%></font></h5></center>
           <center><h5><font class="Estilo10">Ordenes de Compra vencidas</font></h5></center>
           <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
               <tr bgcolor="#B9DDFB" align="center">
                   <td><font class="Estilo9">numero Orden</font> </td>
                   <td><font class="Estilo9">Linea </font></td>
                   <td><font class="Estilo9">Proveedor</font></td>
                   <td><font class="Estilo9">rut Proveedor</font></td>
                   <td><font class="Estilo9">fecha Pedido</font> </td>
                   <td><font class="Estilo9">fecha Entrega</font></td>
                   <td><font class="Estilo9">Cantidad Pedida</font> </td>
                   <td><font class="Estilo9">Cantidad Pendiente</font></td>
               </tr>
               
               <%
               Vector ordenesDeCompra = ordenCompra.ordenDeCompraInvalidas(request.getParameter("codpro"));
               Enumeration enum1 = ordenesDeCompra.elements();
               while(enum1.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enum1.nextElement();
               // out.println("<br>"+orden.getCan_pedida());
               
               %>
               <tr>
                   <td align="center"><font class="Estilo9"><%=orden.getNum_orden_compra()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getLinea()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden.getProveedor()%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getRut())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_pedido())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden.getFecha_entrega())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getCan_pedida())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden.getCan_pendiente())%></font></td>
               </tr>
               <%}%>  
           </table>
           <br>
           <center><h5><font class="Estilo10">Ordenes de Compra futuras</font></h5></center>
           <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
               <tr bgcolor="#B9DDFB" align="center">
                   <td><font class="Estilo9">numero Orden</font> </td>
                   <td><font class="Estilo9">Linea </font></td>
                   <td><font class="Estilo9">Proveedor</font></td>
                   <td><font class="Estilo9">rut Proveedor</font></td>
                   <td><font class="Estilo9">fecha Pedido</font> </td>
                   <td><font class="Estilo9">fecha Entrega</font></td>
                   <td><font class="Estilo9">Cantidad Pedida</font> </td>
                   <td><font class="Estilo9">Cantidad Pendiente</font></td>
               </tr>
               <%
               Vector ordenesDeCompraFuturas = ordenCompra.ordenDeCompraValidas(request.getParameter("codpro"));
               Enumeration enu = ordenesDeCompraFuturas.elements();
               while(enu.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden1 =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enu.nextElement();
               // out.println("<br>"+orden.getCan_pedida());
               
               %>
              <tr>
                   <td align="center"><font class="Estilo9"><%=orden1.getNum_orden_compra()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden1.getLinea()%></font></td>
                   <td align="center"><font class="Estilo9"><%=orden1.getProveedor()%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden1.getRut())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden1.getFecha_pedido())%></font></td>
                   <td align="center"><font class="Estilo9"><%=formate.format(orden1.getFecha_entrega())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden1.getCan_pedida())%></font></td>
                   <td align="center"><font class="Estilo9"><%=number.format(orden1.getCan_pendiente())%></font></td>
               </tr>
               <%}%>  
           </table>
           <%}catch(Exception e){e.printStackTrace();}%> 
       </body>
   </html>