<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <html>
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
           <title>Manufactura asistida por computadora</title>
           <SCRIPT LANGUAGE="Javascript">

            var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return (key >= 48 && key <= 57);

            }
            
            function validarCantidad(){
            if(document.form1.stockMin.value == "" && document.form1.stockMax.value == ""){
               alert('Debe agregar minimo , maximo');
               return false;
            }else if(document.form1.stockMin.value == "" && document.form1.stockMax.value != ""){
               alert('Debe agregar minimo , maximo');
               return false;
            }else if(document.form1.stockMin.value != "" && document.form1.stockMax.value == ""){
               alert('Debe agregar minimo , maximo');
               return false;           
            }else{
             return true;
            }
          }

         </SCRIPT>      

       </head>
       <body>
           <form action="setearStockMinMax.jsp" method="post" name="form1">
           <%              
               try{
                 HttpSession sesion    = request.getSession();  
                 ConexionEJB cal = new ConexionEJB();
                 Object ref =cal.conexionBean("estructuraEquipoBean");
                 estructuraEquipoRemoteHome estructuraHome = (estructuraEquipoRemoteHome) PortableRemoteObject.narrow(ref, estructuraEquipoRemoteHome.class);
                 estructuraEquipoRemote estructura= estructuraHome.create();
                 Object ref1 =cal.conexionBean("MrpBean");
                 MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
                 MrpRemote mrp = mrpHome.create();    
               if(request.getParameter("guardar") == null){
                  sesion.setAttribute("cod",request.getParameter("codpro"));
                  //sesion.setAttribute("indicador",request.getParameter("indicador"));
                 }
             
               %>
               
                   <br>
                     <%
               if(request.getParameter("guardar") != null){
                  byte verificador = estructura.actualizarStockMinMax1((String)sesion.getAttribute("cod"),Float.parseFloat(request.getParameter("stockMin")),Float.parseFloat(request.getParameter("stockMax")));                        
                  if(verificador == 1){
                    /*if(((String)sesion.getAttribute("indicador")).equals("1")){
                       for(int i=0;i<mrp.controlDeMateriales().size();i++){
                           if(((String)sesion.getAttribute("cod")).equals((String)((Vector)mrp.controlDeMateriales().elementAt(i)).elementAt(0))){
                              ((Vector)mrp.controlDeMateriales().elementAt(i)).setElementAt(Float.valueOf(request.getParameter("stockMin")),9);
                              ((Vector)mrp.controlDeMateriales().elementAt(i)).setElementAt(Float.valueOf(request.getParameter("stockMax")),10);
              }
            }
        }*/%>
                  <center><h5><font class="Estilo10">Grab&oacute; los datos en la base de datos</font></h5></center>
                  <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center>
                  <%}else{%>
                    <center><h5><font class="Estilo10">No grabó los datos en la base de datos</font></h5></center>
               <%}}else{%>
                   <center><h5><font class="Estilo10">Cambiar stock minimo o maximo</font></h5></center>
                   <br>
                   <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                       <tr bgcolor="#B9DDFB">
                           <td class="Estilo10" colspan="3" align="center">Cantidad a cambiar</td>
                       </tr>
                       <tr>
                           <td><font class="Estilo9">Material</font></td>
                           <td><font class="Estilo9">Stock Minimo</font></td>
                           <td><font class="Estilo9">Stock Maximo</font></td>
                       </tr>
                       <tr>
                           <td><font class="Estilo9"><%=sesion.getAttribute("cod")%></font></td>
                           <td><font class="Estilo9"><input name="stockMin" type="text" onKeyPress="return acceptNum(event)"></font></td>
                           <td><font class="Estilo9"><input name="stockMax" type="text" onKeyPress="return acceptNum(event)"></font></td>
                       </tr>
                   </table>
                   <br><br>
                   <center><input type="submit" value="Guardar" name="guardar" class="Estilo10" onclick="return validarCantidad()"></center>
               </form><br>
               <%}%>
             
           <%}catch(Exception e){e.printStackTrace();}%>
       </body>
   </html>