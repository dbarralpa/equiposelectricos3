<%@page contentType="text/html; charset=iso-8859-1" import="mac.ee.pla.ordeCompra.clase.*,java.util.Vector,java.util.Date,mac.ee.pla.ordenCompra.bean.OrdenBean" session="true"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Ver Sap</title>
        <link href="../CSS/estiloPaginas.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="../js/javascript.js"></script>
        <script>
            function actualiza(valor){
            if(confirm('¿Seguro que desea proseguir con la operación?')){
               form1.submit();  
            }else{
              valor.checked = false;
            }
          }
          
          function informe(){
            if(confirm('¿Seguro que desea proseguir con la operación?')){
            }else{
              return false;
            }
          }
          function observacion(){
           
              if(document.getElementById('observacionGerenteLogistica').value == ""){
                 alert('debe agregar observacion');
                 document.form1.comprador.selectedIndex = 0;
                 return false;
               }else{
                 form1.submit(); 
               }
          }     
          function Abrir_ventana8(pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=500, height=300, top=240, left=200"; 
                window.open(pagina,"",opciones);} 
          function Abrir_ventana1 (pagina) {
                var opciones="toolbar=no,titlebar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=980, height=700, top=0, left=20"; 
                window.open(pagina,"",opciones);}
                
          function Abrir_ventana10 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1000, top=0, left=0"; 
                window.open(pagina,"",opciones);}      
                
          
        </script>
    </head>
    <body>
        <%
               
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               OrdenBean gestion = new OrdenBean();
               if(idSesion.equals(sesion.getId())){ %>
     <form  name="form1" method="post" action="soloVerSap.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">
             <%try{      
               
               //ConexionDB conexionDB =  new ConexionDB();
               //gestion.conectarDataBase();
               Vector traerArchivosAdjuntosPorSap = new Vector();
               Sap sap = new Sap();
               sesion.setAttribute("traerSapPorNumero",gestion.traerSapPorNumero(Long.parseLong(request.getParameter("numSap"))));
               traerArchivosAdjuntosPorSap = gestion.traerArchivosAdjuntosPorSap(Long.parseLong(request.getParameter("numSap")));

               
        %>
        
            <center><h4><font class="Estilo10">SAP</font></h4></center>
            <br><br>
        <table border="0" cellpadding="0" cellspacing="0" class="Estilo10" align="center" width="60%">
            <tr>
                <td height="30" align="left">N&uacute;mero de sap:&nbsp;&nbsp;&nbsp;<%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getNumSap()%></td>
                <td height="30" align="left">Fecha de emisi&oacute;n&nbsp;&nbsp;&nbsp;<%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getFechaEntrada()%></td>
            </tr>
             <tr>
                <td height="30" align="left" title="Validado por Gerente de &aacute;rea">VGA:&nbsp;&nbsp;&nbsp;<%=gestion.devolverObject(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getUsuValArea())%>&nbsp;&nbsp;&nbsp;<%=gestion.devolverObject(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getFechaVal())%></td>
                <td align="left" height="30" title="Validado por Gerente de log&iacutestica">VGL:&nbsp;&nbsp;&nbsp;<%=gestion.devolverObject(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getUsuValLogi())%>&nbsp;&nbsp;&nbsp;<%=gestion.devolverObject(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getFechaValSgc())%></td>
            </tr>
            <tr>
                
                <td align="left" height="30">Usuario del requerimiento:&nbsp;&nbsp;&nbsp;<%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getNomPersona()%></td>
                <td align="left" height="30">Area:&nbsp;&nbsp;&nbsp;<%=gestion.nombreArea(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getArea())%></td>
            </tr>
            <tr>
                
                <td align="left" height="30">CentroCosto:&nbsp;&nbsp;&nbsp;<%=gestion.nombreCentroDeCosto(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getCentroCosto())%></td>
                <td align="left" height="30">Comprador:&nbsp;&nbsp;&nbsp;<%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getComprador()%></td>
            </tr>
             <tr>
                <td align="left" height="30">Np asociada&nbsp;&nbsp;&nbsp;<%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getNp()%></td>
            </tr>
            <%if(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getUsuario().equals(gestion.usuarioSia(usuario))){%>
            <tr>
                <td height="30">&nbsp;</td>               
            </tr>
            <%}%>
        </table>
        <%--if(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getUsuario().equals(gestion.usuarioSia(usuario)) && Integer.parseInt(((String)sesion.getAttribute("validar")))==0 && Integer.parseInt(((String)sesion.getAttribute("anular")))==0){--%>
        <br>
        <% for(int i=0;i<traerArchivosAdjuntosPorSap.size();i++){%>
        <table border="0" cellpadding="0" cellspacing="0" class="Estilo10" align="center" width="60%">
             <tr>
                <td align="left"><a href="../OpenFile?fileName=<%=((ArchivoAdjunto)traerArchivosAdjuntosPorSap.elementAt(i)).getNombreArchivo()%>">Archivo adjunto:&nbsp;&nbsp;&nbsp;<%=((ArchivoAdjunto)traerArchivosAdjuntosPorSap.elementAt(i)).getNombreArchivo()%></a></td>
                
            </tr>
        </table>       
        <%}//}%>
        <br>
       
            <table width="50%" border="0" cellpadding="0" cellspacing="0" align="center" >
                <tr>
                    <td class="Estilo10" align="center"><a href="javascript:Abrir_ventana10('ordenDeCompra.jsp?numSap=<%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getNumSap()%>')">Ver trazabilidad de la SAP</a></td>
               </tr>
                </tr>
            </table> 
        <br>
        <table width="917" border="1" cellpadding="0" cellspacing="0" align="center" class="tablaCuadro">
              <tr>
                <td align="center" valign="middle" class="Estilo10">Descri Usuario</td>
                <td align="center" valign="middle" class="Estilo10">Descri Producto</td>
                <td align="center" valign="middle" class="Estilo10">Codigo</td>
                <td align="center" valign="middle" class="Estilo10">Um</td>
                <td align="center" valign="middle" class="Estilo10">Cantidad</td>
                <td align="center" valign="middle" class="Estilo10" title="Cantidad asignada a una &oacute;rden de compra">CantAsignada a OC</td>
                <!--td align="center" valign="middle" class="Estilo10">Fec_Req</td-->
                <td align="center" valign="middle" class="Estilo10" title="Eliminar de la solicitud">Eli</td>
              </tr>
              <%if(sesion.getAttribute("traerSapPorNumero") != null){
                   
                 Vector traerSap =   (Vector)sesion.getAttribute("traerSapPorNumero");
                 for(int i=0;i<((Vector)sesion.getAttribute("traerSapPorNumero")).size();i++){%>  
                 <%String caracter = ((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getDescripcion().replace('"',' ');%>
               
                <tr>
                   
                
                <td align="center" valign="middle" class="Estilo10"><%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getDescripcion()%></td>
             
                <td align="center" valign="middle" class="Estilo10"><%=gestion.descripcionPorCodigo(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getCodigo())%></td>
                <td align="center" valign="middle" class="Estilo10"><%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getCodigo()%></td>
                <td align="center" valign="middle" class="Estilo10"><%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getUm()%></td>
                <td align="center" valign="middle" class="Estilo10"><%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getCantidad()%></td>
                <td align="center" valign="middle" class="Estilo10"><%=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getCantAsignada()%></td>
                <!--td align="center" valign="middle" class="Estilo10"><%--=((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(i))).getFechaReq()--%></td-->
                
                <td align="center" valign="middle" class="Estilo10"><input type="checkbox" name="eliminar<%=i%>" disabled></td>
                
              </tr>
              <%}%>
              <%}%>
          </table>
        <br/>
        <table width="90%" class="Estilo10" align="center">
                <tr>
                    <td align="left" width="120">Observaci&oacute;n usuario:</td>
                    <td align="left" colspan="4"><%=sap.devolverObservacion(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getObservacion())%></td>

                </tr>
                <%Vector observa = gestion.observacionAsap(((Sap)(((Vector)sesion.getAttribute("traerSapPorNumero")).elementAt(0))).getNumSap());%>
                <%if(observa.size() > 0){%>
                <tr>
                    <td colspan="4" height="30">&nbsp;</td>
                </tr>
                <tr>
                    <!--td align="left">Tipo</td-->
                    <td align="left">Fecha</td>
                    <td align="left">Usuario</td>
                    <td align="left">Accion</td>
                    <td align="left">Observaci&oacute;n</td>
                </tr>
                <tr>
                    <td colspan="5" height="15">&nbsp;</td>
                </tr>
                
                <%for(int i=0;i<observa.size();i++){%>
                <tr>
                    <td align="left" width="150"><%=((Sap)observa.elementAt(i)).getFechaIni()%></td>
                    <td align="left" width="150"><%=gestion.devolverObject(gestion.nombreUsuario1(((Sap)observa.elementAt(i)).getUsuario()))%></td>
                    <td align="left" width="150"><%=gestion.devolverObject(((Sap)observa.elementAt(i)).getAccion())%></td>
                    <td align="left"><%=((Sap)observa.elementAt(i)).getObservacion()%></td>
                </tr>
                <%}%>
             <%}%>
        </table>
        <br/>
          <%}catch(Exception e){
          }%>
          
          </form>
          <%}else{response.sendRedirect("../mac");}%>
        
    </body>
</html>
