<%@page import="mac.datos.PrmDatos,java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.ordenCompra.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="ISO-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%
HttpSession sesion    = request.getSession();
String idSesion       = request.getParameter("idSesion");
String usuario        = request.getParameter("usuario");
String tipou          = request.getParameter("tipou");
/*String idSesion       = request.getParameter("idSesion");
String usuario        = request.getParameter("usuario");
String tipou          = request.getParameter("tipou");
sesion.setAttribute("idSesion",idSesion);
out.println(idSesion);
out.println(usuario);
out.println(tipou);*/
if(idSesion.equals(sesion.getId())){
%>
<html>
      <%
        try{
            SimpleDateFormat formate = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            NumberFormat number = new DecimalFormat(" ");
            NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(0);
            //nf.setMinimumFractionDigits(0);
            
        /*if(request.getParameter("boton") != null){
        sesion.setAttribute("boton",request.getParameter("boton"));
        }*/
            int iden = 17;
            int size = 0;
            String todos = "todos";
            if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez") ){
                iden =   18;
            }
            byte estado = 0;
            //mac.ee.pla.mrp.bean.MrpBean mrp = new mac.ee.pla.mrp.bean.MrpBean();
            ConexionEJB cal = new ConexionEJB();
            Object ref1 =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
            MrpRemote mrp = mrpHome.create();
            long fecha = 0;
            long fecha1 = 0;
            Vector compradores = mrp.compradores();
        /*if(request.getParameter("actualizar") != null){
        if(sesion.getAttribute("buy") != null){
        int sap = mrp.numeroSap();
        for(int u=0;u<((Vector)sesion.getAttribute("buy")).size();u++){
        if(request.getParameter("T"+u) != null){
        mrp.agruparPedidosSap((String)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(0),formate.format((java.sql.Date)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(16)),sap+1);
        }
        }
        }
        }*/
            if(request.getParameter("actualizar") != null){
                if(sesion.getAttribute("buy") != null){
                    if(request.getParameter("validaroc") != null && request.getParameter("validaroc").equals("validaroc")){
                        OrdenBean orden = new OrdenBean();
                        orden.actualizarEstadoOC(Long.parseLong(request.getParameter("ordenC")),usuario);
                        for(int u=0;u<((Vector)sesion.getAttribute("buy")).size();u++){
                            mrp.descartarCompra((String)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(0),((Integer)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(17)).intValue());
                        }
                    }
                    //int sap = mrp.numeroSap();
                    int sap = 0;
                    for(int u=0;u<((Vector)sesion.getAttribute("buy")).size();u++){
                        if(request.getParameter("descartar"+u) != null){
                            mrp.descartarCompra((String)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(0),((Integer)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(17)).intValue());
                        } else if(request.getParameter("comprador"+u) != null && !request.getParameter("comprador"+u).equals("0")){
                            mrp.actualizarComprador((String)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(0),request.getParameter("comprador"+u),formate.format((java.sql.Date)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(16)));
                        } else if(request.getParameter("T"+u) != null){
                            mrp.agruparPedidosSap((String)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(0),formate.format((java.sql.Date)((Vector)((Vector)sesion.getAttribute("buy")).elementAt(u)).elementAt(16)),sap+1);
                        }
                    }
                }
            }
            
            
        /*if(request.getParameter("code") != null && request.getParameter("code").length() > 3){
 
        mrp.actualizarComprasRequeridasEE(request.getParameter("code").substring(0,10),Float.parseFloat(request.getParameter("valor")));
        }*/
            Vector compras = new Vector();
            if(request.getParameter("ordenCompra") != null && !request.getParameter("ordenCompra").equals("")){
                compras =  mrp.retornarComprasRequeridasPorOrdenCompras(Integer.parseInt(request.getParameter("ordenCompra")),(byte)0);
            }else if(request.getParameter("sap") != null && !request.getParameter("sap").equals("")){
                compras =  mrp.retornarComprasRequeridasPorOrdenCompras(Integer.parseInt(request.getParameter("sap")),(byte)1);
            }else if(request.getParameter("asociar") != null){
                compras =  mrp.retornarComprasRequeridasAsociados(request.getParameter("asociar"));
            }else{
                if(request.getParameter("ordenar") != null && !request.getParameter("ordenar").equals("")){
                    compras =  mrp.retornarComprasRequeridas(1,request.getParameter("ordenar"));
                } else{
                    compras =  mrp.retornarComprasRequeridas(1,"cr.Codigo");
                }
                
            }
            if(request.getParameter("buyer") != null && !request.getParameter("buyer").equals("nada")){
                todos =  request.getParameter("buyer");
           // } else if(usuario.equals("miguelangel") || usuario.equals("lcaceres") || usuario.equals("jaceituno") || usuario.equals("jcduran")){
                }else if(mrp.validarUsuarioALink(usuario,"comprador")){
                todos =  usuario;
            }else{}
        %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Compras Requeridas</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <style>
            .Estilo10 {
            font-size: 9px;
            font-family: Verdana, Arial, Helvetica, sans-serif;
            color: #330066;
            }
        </style>
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
        
        <script>
            
          function validar(){
           if(document.form1.ordenCompra.value == "" && document.form1.sap.value == ""){
           alert('Debe ingresar un numero');
             return false;
           }else{
            form1.submit();
           }
          }
        function ajustarCeldas(valor){
                
                var ancho1,ancho2,i;
                var columnas=valor; //CANTIDAD DE COLUMNAS//
                for(i=0;i<columnas;i++){
                ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
                ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
                if(ancho1>ancho2){
                document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-6;
                }else{
                document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-6;
                }
                }
                }
          
        </script>
      
        
    </head>
    <body onload="ajustarCeldas(<%=iden+3%>)">
        <!--div id="dhtmltooltip"></div-->
        <script>
            function Abrir_ventana5(pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, top=0, left=50"; 
                window.open(pagina,"",opciones);
            }
            function Abrir_ventana (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=800, height=500, top=240, left=200"; 
                window.open(pagina,"",opciones);
             }
             function Abrir_ventana2 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=960, height=600, top=0, left=50"; 
                window.open(pagina,"",opciones);
             }
             function Abrir_ventana4 (pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, width=450, height=200, top=200, left=100"; 
                window.open(pagina,"",opciones);
             }
        </script>


        <form action="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
            <center><h4><font class="Estilo10">NECESIDADES DE COMPRA</font></h4></center>
            <div id="divButtons">
                <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                    <tr>
                        <td><a href="materialesProyectados.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></td>
                        <td width="100"><input type="submit" value="Actualizar" class="Estilo10" name="actualizar"></td>
                        <%--if(usuario.equals("bsantana") || usuario.equals("marsanchez") || usuario.equals("jbriones") || usuario.equals("arondon") || usuario.equals("dbarra") || usuario.equals("vhernandez") || usuario.equals("lcaceres") || usuario.equals("jaceituno") || usuario.equals("jcduran") || usuario.equals("miguelangel")
                        || usuario.equals("gvillaroel") || usuario.equals("ccarvalho") || usuario.equals("cpizarro") || usuario.equals("esantander") || usuario.equals("msch") || usuario.equals("evarela") || usuario.equals("alvergara") || usuario.equals("evillar")  || usuario.equals("mariogonzalez")){--%>
                        <%if(mrp.validarUsuarioALink(usuario,"comprasRequeridas")){%>
                        <td width="150"><a href="retornarEstadoMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">Estado de materiales</a></td>
                        <%}else{%>
                        <td width="150">Estado de materiales</td>
                        <%}%>
                        <!--td class="Estilo10"><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&materialesHidroPack=si">Materiales Hidropack&nbsp;&nbsp;&nbsp;</a></td-->
                        <!--td class="Estilo10">Materiales Hidropack</td>
                        <td width="50">&nbsp;</td>
                        <td>Buscar por &oacute;rden de compra:&nbsp;&nbsp; </td>
                        <td><input type="text" name="ordenCompra" size="8" onKeyPress="return acceptNum(event)" onblur="return validar();"  autocomplete="off">&nbsp;&nbsp;</td>
                        <td>Buscar por Sap:&nbsp;&nbsp; </td>
                        <td><input type="text"  size="8" name="sap" onKeyPress="return acceptNum(event)" onblur="return validar();"  autocomplete="off">&nbsp;&nbsp;</td>
                        <td><input type="button" onclick="javascript:Abrir_ventana5('imprimir.jsp')" class="Estilo10" value="Imprimir"/></td-->
                        <!--td><input type="submit" name="ordenDeCompra1" value="Buscar" onclick="return validar();" class="Estilo10"></td-->
                    </tr>
                    <tr>
                        <td colspan="3" align="center" style="padding-top: 30px;font-size: 16px;"><a href="javascript:Abrir_ventana5('reclamos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>')" style="padding-top: 30px;font-size: 16px;"><font color="#FF3300" >RECLAMOS:</font></a>&nbsp;<font color="#FF3300" ><%=mrp.reclamos(usuario,mrp.validarUsuarioALink(usuario,"comprador"))%></font></td>
                    </tr>
                </table>
                <br>
                <table cellpadding="0" cellspacing="0" class="Estilo10" align="center" width="60%">
                    <tr class="Estilo10">
                        <td class="Estilo10" align="left"> Elija una opci&oacute;n:&nbsp;&nbsp;
                            <%if(usuario.equals("gvillaroel") || usuario.equals("jgonzalez") || usuario.equals("jbriones") || usuario.equals("miguelangel") || usuario.equals("lcaceres") || usuario.equals("jaceituno") || usuario.equals("jcduran") || usuario.equals("mariogonzalez")){%>
                            <select name="buyer" class="Estilo10" onchange="form1.submit();">
                                <option value="nada">Elija comprador</option>
                                <%for(int i=0;i<compradores.size();i++){%>
                                <%EstructuraPorEquipo estru = (EstructuraPorEquipo)compradores.elementAt(i);%>
                                <option value="<%=estru.getUsuario()%>" <%if(todos.equals(estru.getUsuario())){%>selected<%}%>><%=estru.getUsuario()%></option>
                                
                                <!--option value="miguelangel" <%if(todos.equals("miguelangel")){%>selected<%}%>>mi</option>
                                <option value="jaceituno" <%if(todos.equals("jaceituno")){%>selected<%}%>>ja</option>
                                <option value="lcaceres" <%if(todos.equals("lcaceres")){%>selected<%}%>>lc</option-->
                                <%}%>
                                <option value="null" <%if(todos.equals("null")){%>selected<%}%>>sin comprador</option>
                                <option value="todos" <%if(todos.equals("todos")){%>selected<%}%>>todos</option>
                            </select>
                            <%}else{%>
                            <select name="buyer" class="Estilo10" onchange="form1.submit();" disabled>
                                <option value="0">Comprador</option>
                            </select>
                            <%}%>
                            
                        </td>
                        <td class="Estilo10">Compras requeridas:&nbsp;&nbsp;<%=mrp.comprasRequeridasPorComprador(todos)%></td>
                        <td class="Estilo10">Compras requeridas con sap:&nbsp;&nbsp;<%=mrp.comprasRequeridasConSapPorComprador(todos)%></td>
                        <td class="Estilo10">&nbsp;&nbsp;&nbsp;Compras requeridas<a href="../ExcelCompras?usuario=<%=usuario%>&comprador=<%=todos%>"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a></td>
                    </tr>
                    <!--tr>
                        <td colspan="4">
                            <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                                <tr>
                                    <td class="Estilo10">Analisis general:&nbsp;</td>
                                    <td bgcolor="#0099FF">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td class="Estilo10">An&aacute;lisis s&oacute;lo MinMax:&nbsp;</td>
                                    <td bgcolor="#FF3300">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td class="Estilo10">No analizar:&nbsp;</td>
                                    <td bgcolor="#339966">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;</td>
                                    <td class="Estilo10">Con observaci&oacute;n:&nbsp;</td>
                                    <td bgcolor="#FFCC00">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                            </table> 
                        </td>
                    </tr-->
                    <%--if(request.getParameter("ordenCompra") != null && !request.getParameter("ordenCompra").equals("") && (usuario.equals("jbriones") || usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel"))){%>
                    <tr>
                        <td colspan="4" align="center" height="40">&Oacute;rden de compra n�&nbsp;&nbsp;&nbsp;<%=request.getParameter("ordenCompra")%>&nbsp;&nbsp;&nbsp;Validar &oacute;rden:&nbsp;&nbsp;&nbsp;<input type="checkbox" name="validaroc" value="validaroc"></td>
                    </tr>
                    <%}%>
                    <%if(request.getParameter("materialesHidroPack") != null && request.getParameter("materialesHidroPack").equals("si")){%>
                    <tr>
                        <td colspan="4" align="center" height="40">Materiales Hidropack</td>
                    </tr>
                    <%}--%>
                </table>
            </div>
            <br>
            
            <%//int [] valors = mrp.paginacion();%>
            <table border=0 bgcolor=scrollbar align=center>
                
                <tr> 
                    <td>
                        <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc">
                            <tr bgcolor="#B9DDFB">
                                <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                                <td align="center"><font class="Estilo10"><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&ordenar=cr.Codigo&buyer=<%=todos%>">C&oacute;digo</a></font></td>
                                <td align="center"><font class="Estilo10"><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&ordenar=Descripcion&buyer=<%=todos%>">Descripci&oacute;n</a></font></td>
                                <td align="center"><font class="Estilo10" title="cantidad sugerida"><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&ordenar=cantRequerida,cr.Codigo&buyer=<%=todos%>">Sugerido</a></font></td-->
                                <td align="center"><font class="Estilo10" title="Necesidad de equipos">Nec mat</font></td>
                                <td align="center"><font class="Estilo10" title="Consumo promedio">Con Pro</font></td>
                                <td align="center"><font class="Estilo10" title="Stock Minimo">Stock Min</font></td>
                                <td align="center"><font class="Estilo10" title="Stock Maximo">Stock Max</font></td>
                                <td align="center"><font class="Estilo10" title="Bodega de materias primas">BMP</font></td>
                                <td align="center"><font class="Estilo10" title="Bodega de productos terminados">BPT</font></td>
                                <td align="center"><font class="Estilo10" title="Bodega de materias primas esperando revisi�n">BMP 9</font></td>
                                <td align="center"><font class="Estilo10" title="Stock planta">S. planta</font></td>
                                <td align="center"><font class="Estilo10" title="Unidad de medida">um</font></td>
                                <td align="center"><font class="Estilo10"><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&ordenar=Comprador&buyer=<%=todos%>">Com</a></font></td>
                                <td align="center"><font class="Estilo10" title="Lead time">Lead</font></td>
                                <td align="center"><font class="Estilo10" title="&oacute;rdenes de compra pendientes de llegada">Oc Pen</font></td>
                                <!--td align="center"><font class="Estilo10" title="agrupar pedidos para sap"><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&ordenar=numeroSap,cr.Codigo&buyer=<%=todos%>">Sap</a></font></</td-->
                                <%if(usuario.equals("jbriones") || usuario.equals("jgonzalez") || usuario.equals("lcaceres") || usuario.equals("gvillaroel")){%>
                                <td align="center" title="Eliminar el pedido del carro de compras"><font class="Estilo10">Eli</font></td>
                                <%}%>
                                <!--td align="center" class="Estilo10">Saldo</td>
                                    <td align="center" class="Estilo10" title="Cobertura">Cob</td-->
                                <td align="center" class="Estilo10" title="procedencia del material N\I">Pro</td>
                                <td align="center" class="Estilo10" title="Stockeable">S</td>
                                <td align="center">&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                        </table>
                        <div style="overflow:auto; height:380px; padding:0">
                        <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                        <% for(int u=0;u<compras.size();u++){
                if((usuario.equals((String)((Vector)compras.elementAt(u)).elementAt(4)))
                || (usuario.equals("dbarra"))
                || (usuario.equals("esantander"))       
                || (usuario.equals("msch"))
                || (usuario.equals("evarela"))
                || (usuario.equals("btrujillo"))
                || (usuario.equals("jmaldonado"))
                || (usuario.equals("ccarvalho"))
                || (usuario.equals("alvergara"))
                || (usuario.equals("evillar"))
                || ((usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("jgonzalez") || usuario.equals("gvillaroel")) && todos.equals("todos"))
                || ((usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("jgonzalez") || usuario.equals("gvillaroel")) && ((String)((Vector)compras.elementAt(u)).elementAt(4)).equals(request.getParameter("buyer")))
                || (usuario.equals("sreyes"))
                || (usuario.equals("jgonzalez"))
                || (usuario.equals("pyanez"))
                || (usuario.equals("sarenas"))
                || (usuario.equals("mjorquera"))
                || (usuario.equals("jcortes"))
                || (usuario.equals("dreyes"))
                || (usuario.equals("rrojas"))
                || ((String)((Vector)compras.elementAt(u)).elementAt(4)).equals(request.getParameter("buyer"))){
                        %>
                        <%--if(((Byte)((Vector)compras.elementAt(u)).elementAt(12)).byteValue() == 1 && ((Byte)((Vector)compras.elementAt(u)).elementAt(5)).byteValue() != 4){%>
                        <tr class="Estilo10" bgcolor="#CCFF00">
                        <%}else if(((Byte)((Vector)compras.elementAt(u)).elementAt(5)).byteValue() == 4){%>
                        <tr class="Estilo10" bgcolor="#33CC66">
                        <%}else{%>
                        <tr class="Estilo10">
                        <%}--%>   
                        <tr class="Estilo10">
                        <!--td align="center"><%--=((String)((Vector)compras.elementAt(u)).elementAt(0)).substring(0,5)%>-<%=((String)((Vector)compras.elementAt(u)).elementAt(0)).substring(5,10)--%></td-->
                        <%--if(((Byte)((Vector)compras.elementAt(u)).elementAt(22)).byteValue() == 1){%>
                        
                        <td align="center" bgcolor="#0099FF"><%=((String)((Vector)compras.elementAt(u)).elementAt(0))%></td>
                        <%}%>
                        <%if(((Byte)((Vector)compras.elementAt(u)).elementAt(20)).byteValue() == 1){%>
                        
                                                <td align="center" bgcolor="#FF3300"><%=((String)((Vector)compras.elementAt(u)).elementAt(0))%></td>
                        <%}%>
                        <%if(((Byte)((Vector)compras.elementAt(u)).elementAt(21)).byteValue() == 1){%>
                        
                        <td align="center" bgcolor="#339966"><%=((String)((Vector)compras.elementAt(u)).elementAt(0))%></td>
                        <%}--%>
                         <td><%=((String)((Vector)compras.elementAt(u)).elementAt(0))%></td>
                        <%String color = "";%>
                       
                        <%if(((Integer)((Vector)compras.elementAt(u)).elementAt(24)).intValue() > 0){%>
                        <td bgcolor="#FFCC00" align="center" ondblclick="javascript:Abrir_ventana5('observacionComprasRequeridas.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>&descripcion=<%=((Vector)compras.elementAt(u)).elementAt(1)%>')" title="<%=((String)((Vector)compras.elementAt(u)).elementAt(1))%>"><%=PrmDatos.devolverString(((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase())%></td>
                        <%}else{%>
                        <td align="center" ondblclick="javascript:Abrir_ventana5('observacionComprasRequeridas.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>&descripcion=<%=((Vector)compras.elementAt(u)).elementAt(1)%>')" title="<%=((String)((Vector)compras.elementAt(u)).elementAt(1))%>"><%=PrmDatos.devolverString(((String)((Vector)compras.elementAt(u)).elementAt(1)).toLowerCase())%></td>
                        <%}%>
                        
                        
                        <!--td align="center">--</td-->
                        
                        
                        
                        <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(2))%></td>
                        <td align="center"><a href="javascript:Abrir_ventana('equiposConNecesidad.jsp?stock=<%=((Vector)compras.elementAt(u)).elementAt(11)%>&codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(9))%></a></td>
                        <td align="center"><a href="javascript:Abrir_ventana5('detalleConsumoPromedio.jsp?media=<%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(10))%>&codigo=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(10))%></a></td>
                        <%if(usuario.equals("jbriones") ||  usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                        <td align="center"><a href="javascript:Abrir_ventana4('setearStockMinMax.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(6))%></a></td>
                        <%}else{%>
                        <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(6))%></td>
                        <%}%>
                        <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(7))%></td>
                        <td align="center"><a href="javascript:Abrir_ventana2('evolucionStock.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(11))%></a></td>
                        <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(25))%></td>
                        <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(27))%></td>
                        <%if(usuario.equals("jbriones") || usuario.equals("dbarra") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                        <td align="center"><a href="javascript:Abrir_ventana5('setearStockPlanta_1.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>')"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(19))%></a></td>
                        <%}else{%>
                        <td align="center"><%=number.format((Float)((Vector)compras.elementAt(u)).elementAt(19))%></td>
                        <%}%>
                        <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(3)%></td>
                        <%if(!((String)((Vector)compras.elementAt(u)).elementAt(4)).equals("") && !((String)((Vector)compras.elementAt(u)).elementAt(4)).equals("null")){
                        size++;%>
                        <%if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                        <td align="center"><a href="javascript:Abrir_ventana('actualizarComprador.jsp?codigo=<%=((Vector)compras.elementAt(u)).elementAt(0)%>&fechaEntrega=<%=formate.format(((Vector)compras.elementAt(u)).elementAt(16))%>')"><%=((String)((Vector)compras.elementAt(u)).elementAt(4)).substring(0,2)%></td>
                        
                        <%}else{%>
                        <td align="center"><%=((String)((Vector)compras.elementAt(u)).elementAt(4)).substring(0,2)%></td>
                        <%}}else{%>
                        <%if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                        <td align="center">
                            <select name="comprador<%=u%>" class="Estilo10">
                                <option value="0">Com</option>
                                <%for(int i=0;i<compradores.size();i++){%>
                                <%EstructuraPorEquipo estru = (EstructuraPorEquipo)compradores.elementAt(i);%>
                                <option value="<%=estru.getUsuario()%>" <%if(todos.equals(estru.getUsuario())){%>selected<%}%>><%=estru.getUsuario()%></option>
                                <%}%>
                            </select>
                        </td>
                        <%}else{%>
                        <td align="center">
                            <select name="comprador<%=u%>" class="Estilo10" disabled>
                                <option value="0">Com</option>
                            </select>
                        </td>
                        <%}}%>
                    </td>
                    <%if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                    <td align="center"><a href="javascript:Abrir_ventana4('leadTimes.jsp?codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>')"><%=((Vector)compras.elementAt(u)).elementAt(18)%></a></td>
                    <%}else{%>
                    <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(18)%></td>
                    <%}%>
                    <td align="center"><a href="javascript:Abrir_ventana5('ordenesCompraFuturas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&codpro=<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>&descri=<%=(String)((Vector)compras.elementAt(u)).elementAt(1)%>')"><%=nf.format((Float)((Vector)compras.elementAt(u)).elementAt(8))%></a></td>
                    <%--if(usuario.equals((String)((Vector)compras.elementAt(u)).elementAt(4))){%>
                    <%if(((Integer)((Vector)compras.elementAt(u)).elementAt(13)).intValue() == 0){%>
                    <td align="center"><input disabled type=checkbox name="T<%=u%>" class="Estilo10"></td>
                    <%}else{%>
                    <td align="center"><%=(Integer)((Vector)compras.elementAt(u)).elementAt(13)%></td>
                    <%}%>
                    <%}else{%>
                    <td align="center"><%=(Integer)((Vector)compras.elementAt(u)).elementAt(13)%></td>
                    <%}--%>
                    <%if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                    <td align="center"><input type=checkbox name="descartar<%=u%>" class="Estilo10"></td>
                    <%}%>
                    
                    <%if(((Integer)((Vector)compras.elementAt(u)).elementAt(14)).intValue() == 1){%>
                    <td align="center">N</td>  
                    <%}%>
                    <%if(((Integer)((Vector)compras.elementAt(u)).elementAt(14)).intValue() == 2){%>
                    <td align="center">I</td>  
                    <%}%>
                    <td align="center"><%=(Integer)((Vector)compras.elementAt(u)).elementAt(15)%></td>
                </tr>
                <%}
                }
                sesion.setAttribute("buy",compras);
                if(size < 26){
                for(int i=0;i<26-size;i++){  
                %>
                <tr>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <!--td align="center" class="Estilo10">&nbsp;</td-->
                    
                    <%if(usuario.equals("jbriones") || usuario.equals("lcaceres") || usuario.equals("gvillaroel") || usuario.equals("jgonzalez")){%>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <%}%>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                    <td align="center" class="Estilo10">&nbsp;</td>
                </tr>
                <%}}%>
            </table>
            </div>
            </td>
            </tr>
            </table>
            <br><br>
            <input type="hidden" name="valor">
            <input type="hidden" name="code">
            <input type="hidden" name="ordenC" value="<%=request.getParameter("ordenCompra")%>">
            <%
            sesion.setAttribute("familia",null);
            sesion.setAttribute("rango",null);
            sesion.setAttribute("material",null);
            sesion.setAttribute("compras",null);
            sesion.setAttribute("compras1",compras);
            //=request.getParameter("valor") + "---" + request.getParameter("code").substring(0,10)%>
            
            <%}catch(Exception e){
            out.println("ERROR: comprasRequeridas.jsp " + e.getMessage());
            }        
            %>
        </form>
        <%}else{response.sendRedirect("../mac");}%>
    </body>
</html>
