<%@ page import="java.io.*, java.util.*,java.text.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
   <%String sap = request.getParameter("sap");
   String comprador = request.getParameter("comprador");
   //out.println(codigo);%>
   <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
        <center>
            <%try{
                   ConexionEJB cal = new ConexionEJB();
                   Object ref1 =cal.conexionBean("MrpBean");
                   MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
                   MrpRemote mrp = mrpHome.create();%>
 <table width="60%" border="0" cellpadding="0" cellspacing="0" class="Estilo10">
            <tr>
                <td colspan="2"></td>
                <td colspan="2"></td>
            </tr>
            <tr>
                <td colspan="4" align="center"><font size="2">Generar Solicitud De Aprovisionamiento (S.A.P)  </font></td>            
            </tr>
				<tr>
				<td colspan="3">&nbsp;</td>
				</tr>
            <tr>
                <td width="22%" align="right">Numero S.A.P :</td>
                <td width="17%" align="left"><%=sap%></td>                
                <td width="61%" align="right">
                <font class="Estilo10">Santiago&nbsp;</font><script languaje="JavaScript">
                                   var mydate=new Date()
                                   var year=mydate.getYear()
                                   if (year < 1000)
                                   year+=1900
                                   var day=mydate.getDay()
                                   var month=mydate.getMonth()
                                   var daym=mydate.getDate()
                                   if (daym<10)
                                   daym="0"+daym
                                   var dayarray=new Array("Domingo","Lunes","Martes","Miercoles","Jueves","Viernes","Sabado")
                                   var montharray=new Array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre")
                                   document.write('<small><font class="Estilo10">'+dayarray[day]+' '+daym+' de '+montharray[month]+' de '+year+'</font></small>')
                </script></font></td>
            </tr>
            <tr>
                <td align="right">Comprador :</td> <td colspan="4" align="left"><%=comprador%></td>
            </tr>
        </table>
        <table width="60%" border="1" cellpadding="0" cellspacing="0" class="Estilo10">
            <tr bgcolor="#B9DDFB" align="center">
                <td>C&oacute;digo</td>
                <td>Descripci&oacute;n</td>
                <td>Can req</td>
                <td>Stock</td>
            </tr>
            <%Vector vectorSap = new Vector();
            vectorSap = mrp.generarSap(1,Integer.parseInt(sap));
               for(int i=0;i<vectorSap.size();i++){%>
            <tr bgcolor="#DDDDDD" align="center">
                <td><%=((String)((Vector)vectorSap.elementAt(i)).elementAt(0))%></td>
                <td><%=((String)((Vector)vectorSap.elementAt(i)).elementAt(1))%></td>
                <td><%=((Float)((Vector)vectorSap.elementAt(i)).elementAt(2))%></td>
                <td><%=((Float)((Vector)vectorSap.elementAt(i)).elementAt(15))%></td>
            </tr>
            <%}%>
        </table>
        <%}catch(Exception e){
                   out.println("ERROR: generarSap.jsp " + e.getMessage());
                   }%>     
        </center>
    </body>
</html>
