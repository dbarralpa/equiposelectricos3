<%@ page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector" session="true"%>
<%@page contentType="application/vnd.ms-excel"%>
<%response.setHeader("Content-Disposition","attachment; filename=\"ordenesPorRut.xls\"");%>
<%@page pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
 <%HttpSession sesion = request.getSession(); 
  String idSesion    = request.getParameter("idSesion");
  String usuario     = request.getParameter("usuario");
  String tipou       = request.getParameter("tipou");%>

<html>
    <head>         
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">        
    </head>
    <%if(idSesion.equals(sesion.getId())){
               OrdenRemote ordenCompra = null;
               try{
                 ConexionEJB cal = new ConexionEJB();  
                 Object ref =cal.conexionBean("OrdenBean");
                 OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
                 ordenCompra= ordenHome.create();
                 }catch(Exception e){
                    out.println("ERROR...!!!!" + e.getMessage());
                 }%>
    <body>
        <table border="1" cellpadding="0" cellspacing="0">
            <tr bgcolor="#B9DDFB">
                <td><font class="Estilo10">Cantidad</font></td>
                <td><font class="Estilo10">Numero_orden</font> </td>
                <td><font class="Estilo10">Linea</font></td>
                <td><font class="Estilo10">Rut_proveedor</font></td>
                <td><font class="Estilo10">Fecha_pedido</font> </td>
                <td><font class="Estilo10">Fecha_entrega</font></td>
                <td><font class="Estilo10">Cantidad-pedida</font> </td>
                <td><font class="Estilo10">Cantidad_pendiente</font></td>
                <td><font class="Estilo10">C�digo_producto</font> </td>
                <td width="100%"><font class="Estilo10">Descripci�n_producto</font></th>
            </tr>
            <%Vector buscarPorRut = new Vector();
            buscarPorRut = ordenCompra.buscarPorRutCliente(Long.parseLong(request.getParameter("rut")));
            Enumeration eRut = buscarPorRut.elements();
            int i=0;
            while(eRut.hasMoreElements()){
                 mac.ee.pla.ordeCompra.clase.OrdenDeCompra veClientesPorRut =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eRut.nextElement();%>%>  
            <tr bgcolor="#DDDDDD">
                <td align="center"><font class="Estilo9"><%=i%></font></td>    
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getNum_orden_compra()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=(int)veClientesPorRut.getRut()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getFecha_pedido()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getFecha_entrega()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getCan_pedida()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getCan_pendiente()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getCod_producto()%></font></td>
                <td align="center"><font class="Estilo9"><%=veClientesPorRut.getDescripcion()%></font></td>
            </tr><%i++;
            }%>
        </table>
    </body>
      <%}else{response.sendRedirect("../mac");}%>
</html>
