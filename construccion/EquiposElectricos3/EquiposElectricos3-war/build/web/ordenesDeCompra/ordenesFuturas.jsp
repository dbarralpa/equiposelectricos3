<%@ page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<HTML>
<HEAD>
<TITLE>Manufactura asistida por computador</TITLE>
<SCRIPT language= "JavaScript">
var ancho1,ancho2,i;
var columnas=9; //CANTIDAD DE COLUMNAS//

function ajustaCeldas(){
for(i=0;i<columnas;i++){
ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
if(ancho1>ancho2){
document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-6};
else{
document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-6;}
}
}
</SCRIPT>
</HEAD>
<BODY onload=ajustaCeldas()>
  <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               /*String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               sesion.setAttribute("idSesion",idSesion);
               out.println(idSesion);
               out.println(usuario);
               out.println(tipou);*/
               if(idSesion.equals(sesion.getId())){
        %>
<!--jsp:useBean id="bean" scope="page" class="mac.pla.ordenCompra.OrdenBean" /-->
<link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
<!--script language="JavaScript" TYPE="text/javascript" src ="../js/ordenCompra.js"> </script-->
<br>
<center><h5><font class="Estilo10">Ordenes De Compra Futuras</font></h5></center>
<%--<center><a href="/mac/jsp/dependencias/Panificacion.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></center>--%>
<center><a href="../mrp/index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></center>
<br><table border=1 bgcolor=scrollbar align=center>
<td>
<table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor=#cccccc>
<tr bgcolor="#B9DDFB">
    <th><font class="Estilo9">cantidad </font></th>
    <th><font class="Estilo9">numero_orden</font> </th>
    <th><font class="Estilo9">Linea </font></th>
    <th><font class="Estilo9">rut_proveedor</font></th>
    <th><font class="Estilo9">fecha_pedido</font> </th>
    <th><font class="Estilo9">fecha_entrega</font></th>
    <th><font class="Estilo9">Cantidad_pedida</font> </th>
    <th><font class="Estilo9">Cantidad_pendiente</font></th>
    <th><font class="Estilo9">codido_producto</font> </th>
    <th width="100%"><font class="Estilo9">Descripción_producto</font> </th>
    </tr>
</table>

<div style="overflow:auto; height:370px; padding:0">

<table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
    <%
        try{
             ConexionEJB cal = new ConexionEJB();  
             Object ref =cal.conexionBean("OrdenBean");
             OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
             OrdenRemote ordenCompra= ordenHome.create();           
             Vector ordenesDeCompra = ordenCompra.ordenDeCompraValidas();
             Enumeration enum1 = ordenesDeCompra.elements();
             int i=1;
        
         %> 
     <%    while(enum1.hasMoreElements()){
                
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra orden =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enum1.nextElement();          
               %>
<tr>
                    <td align="center"><font class="Estilo9"><%=i%></font></td>    
                    <td align="center"><font class="Estilo9"><%=orden.getNum_orden_compra()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getLinea()%></font></td>
                    <td align="center"><font class="Estilo9"><%=(int)orden.getRut()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getFecha_pedido()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getFecha_entrega()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getCan_pedida()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getCan_pendiente()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getCod_producto()%></font></td>
                    <td align="center"><font class="Estilo9"><%=orden.getDescripcion()%></font></td>
                   
                    
                          
                </tr>
 <%i++;}}catch(Exception e){e.printStackTrace();}%> 
</table>
</div>
</td>
</table>
 <%}else{response.sendRedirect("../mac");}%>
</BODY>
</HTML>