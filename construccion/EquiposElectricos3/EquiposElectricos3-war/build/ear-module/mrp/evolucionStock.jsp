<%@page contentType="text/html" import="java.text.NumberFormat,mac.ee.pla.mrp.bean.MrpBean,java.util.Vector,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo" session="true"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Evoluci&oacute;n Stock</title>
    </head>
    <body>
        
        <%
        HttpSession sesion    = request.getSession();
        MrpBean  mrp = new MrpBean();
        /*Vector partes = new Vector();
        Vector valesConsumo = new Vector();
        Vector devoluciones = new Vector();
        Vector marcajes = new Vector();*/
        Vector evolucion = new Vector();
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        float parte = 0f;
        float valeConsumo = 0f;
        float devolucion = 0f;
        float marcaje = 0f;
        
       
            /*String dias = mrp.fechaEnDias(Integer.parseInt(request.getParameter("stockFecha")));
            float stockBodega = mrp.stockBodega((String)sesion.getAttribute("codigo"));
            partes = mrp.partesEntrada((String)sesion.getAttribute("codigo"),dias);
            valesConsumo = mrp.valesDeConsumo((String)sesion.getAttribute("codigo"),dias);
            devoluciones = mrp.devoluciones((String)sesion.getAttribute("codigo"),dias);
            marcajes = mrp.marcaje((String)sesion.getAttribute("codigo"),dias);*/
            evolucion = mrp.evolucionStock(request.getParameter("codigo"));
           
        
        
        %>
        <form name="form1">
            <br><br>
            <center><h5><font class="Estilo10">EVOLUCI&Oacute;N DEL STOCK</font></h5></center>
            
            
            <!--select name="stockFecha" class="Estilo10" onchange="form1.submit()">
                <option value="0">Elija periodo</option>
                <option value="30">30</option>
                <option value="60">60</option>
                <option value="90">90</option>
                <option value="120">120</option>
                <option value="150">150</option>
                <option value="180">180</option>
                <option value="210">210</option>
                <option value="240">240</option>
                <option value="270">270</option>
                <option value="300">300</option>
                <option value="330">330</option>
                <option value="365">365</option>
            </select-->
            <br>
            <table align="center">
                <tr>
                    <td>
                        <table bgcolor="scrollbar" border=0 align=center class="Estilo10">
                            
                            <tr>
                                <td>
                                    <table border=1 cellspacing=0 cellpadding=2 bgcolor="#FFFFCC">
                                        <tr bgcolor="scrollbar">
                                            <td colspan="10">C&oacute;digo:&nbsp;<%=request.getParameter("codigo")%></td>
                                        </tr>
                                        <tr bgcolor="#B9DDFB" align="center">
                                            <td align="center" class="Estilo10" width="80">N� documento</td>
                                            <td align="center" class="Estilo10" width="80">Fecha Documento</td>
                                            <td align="center" class="Estilo10" width="100">Stock</td>
                                            <td align="center" class="Estilo10" width="100">Tipo</td>
                                            <td align="center" class="Estilo10" width="100">Cantidad</td>
                                            <td align="center" class="Estilo10" width="100">Usu bodega</td>
                                            <td align="center" class="Estilo10" width="100">Usu prod</td>
                                            <td align="center" class="Estilo10" width="100">Np/linea</td>
                                            <td align="center" class="Estilo10" width="100">Proceso</td>
                                            <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                    <div style="overflow:auto; height:400px; padding:0">
                                        <table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF>     
                                            <%
                                            for(int i=0;i<evolucion.size();i++){%>
                                            
                                            <tr onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFFF"'>
                                                <td align="center" class="Estilo10" width="80"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getNumDoc()%></td>
                                                <td align="center" class="Estilo10" width="80"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getFechaIn()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=nf.format(((EstructuraPorEquipo)evolucion.elementAt(i)).getCantidad())%></td>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getDescripcion()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=nf.format(((EstructuraPorEquipo)evolucion.elementAt(i)).getCantidadConsumo())%></td>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getComprador()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getDescripcionEstado()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getNumPedido()%>&nbsp;/<%=((EstructuraPorEquipo)evolucion.elementAt(i)).getLinea()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)evolucion.elementAt(i)).getProceso()%></td>
                                            </tr>
                                            <%}%>
                                        </table>    
                                    </div>
                                </td>  
                                
                            </tr>
                        </table>
                    </td>
                    <!--td>
                        <table bgcolor="scrollbar" border=0 align=center class="Estilo10">
                            
                            <tr>
                                <td>
                                    <table border=1 cellspacing=0 cellpadding=2 bgcolor="#FFFFCC">
                                        <tr bgcolor="scrollbar">
                                            <td colspan="3">Vales de consumo</td>
                                        </tr>
                                        <tr bgcolor="#B9DDFB" align="center">
                                            <td align="center" class="Estilo10" width="100">Fecha creaci&oacute;n</td>
                                            <td align="center" class="Estilo10" width="100">Cantidad</td>
                                            <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                    <div style="overflow:auto; height:200px; padding:0">
                                        <table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF>     
                                            <%--
                                            for(int i=0;i<valesConsumo.size();i++){%>
                                            
                                            <tr onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFFF"'>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)valesConsumo.elementAt(i)).getFechaCreacion()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=nf.format(((EstructuraPorEquipo)valesConsumo.elementAt(i)).getCantidad())%></td>
                                            </tr>
                                            <%
                                            valeConsumo += ((EstructuraPorEquipo)valesConsumo.elementAt(i)).getCantidad();
                                            }%>
                                            
                                        </table>    
                                    </div>
                                    <table>
                                        <tr>
                                            <td class="Estilo10" width="100" align="right">Total:</td>
                                            <td class="Estilo10" width="100"><%=nf.format(valeConsumo)%></td>
                                        </tr>
                                    </table>
                                </td>  
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table bgcolor="scrollbar" border=0 align=center class="Estilo10">
                            
                            <tr>
                                <td>
                                    <table border=1 cellspacing=0 cellpadding=2 bgcolor="#FFFFCC">
                                        <tr bgcolor="scrollbar">
                                            <td colspan="3">Devoluciones</td>
                                        </tr>
                                        <tr bgcolor="#B9DDFB" align="center">
                                            <td align="center" class="Estilo10" width="100">Fecha creaci&oacute;n</td>
                                            <td align="center" class="Estilo10" width="100">Cantidad</td>
                                            <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                    <div style="overflow:auto; height:200px; padding:0">
                                        <table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF>     
                                            <%
                                            for(int i=0;i<devoluciones.size();i++){%>
                                            
                                            <tr onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFFF"'>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)devoluciones.elementAt(i)).getFechaCreacion()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=nf.format(((EstructuraPorEquipo)devoluciones.elementAt(i)).getCantidad())%></td>
                                            </tr>
                                            <%
                                            devolucion += ((EstructuraPorEquipo)devoluciones.elementAt(i)).getCantidad();
                                            }%>
                                            
                                        </table>    
                                    </div>
                                    <table>
                                        <tr>
                                            <td class="Estilo10" width="100" align="right">Total:</td>
                                            <td class="Estilo10" width="100"><%=nf.format(devolucion)%></td>
                                        </tr>
                                    </table>
                                </td>  
                            </tr>
                        </table>
                    </td>
                    <td>
                        <table bgcolor="scrollbar" border=0 align=center class="Estilo10">
                            
                            <tr>
                                <td>
                                    <table border=1 cellspacing=0 cellpadding=2 bgcolor="#FFFFCC">
                                        <tr bgcolor="scrollbar">
                                            <td colspan="3">Marcaje</td>
                                        </tr>
                                        <tr bgcolor="#B9DDFB" align="center">
                                            <td align="center" class="Estilo10" width="100">Fecha creaci&oacute;n</td>
                                            <td align="center" class="Estilo10" width="100">Cantidad</td>
                                            <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </table>
                                    <div style="overflow:auto; height:200px; padding:0">
                                        <table border=1 cellspacing=0 cellpadding=2 bgcolor=#FFFFFF>     
                                            <%
                                            for(int i=0;i<marcajes.size();i++){%>
                                            
                                            <tr onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFFF"'>
                                                <td align="center" class="Estilo10" width="100"><%=((EstructuraPorEquipo)marcajes.elementAt(i)).getFechaCreacion()%></td>
                                                <td align="center" class="Estilo10" width="100"><%=nf.format(((EstructuraPorEquipo)marcajes.elementAt(i)).getCantidad())%></td>
                                            </tr>
                                            <%
                                            marcaje += ((EstructuraPorEquipo)marcajes.elementAt(i)).getCantidad();
                                            }%>
                                            
                                        </table>    
                                    </div>
                                    <table>
                                        <tr>
                                            <td class="Estilo10" width="100" align="right">Total:</td>
                                            <td class="Estilo10" width="100"><%=nf.format(marcaje)--%></td>
                                        </tr>
                                    </table>
                                </td>  
                            </tr>
                        </table>
                    </td-->
                </tr>
            </table>
            <br><br>
        </form>   
        
    </body>
</html>
