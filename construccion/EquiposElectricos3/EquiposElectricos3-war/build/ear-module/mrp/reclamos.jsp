<%@page import="java.text.SimpleDateFormat,java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.mrp.clase.ModReclamo,mac.ee.pla.mrp.bean.*" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Manufactura asistida por computadora</title>
    </head>
    <body>
        <%try{
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        SimpleDateFormat fecha = new SimpleDateFormat("dd/MM/yyyy");
        ConexionEJB cal = new ConexionEJB();
        Object ref1 =cal.conexionBean("MrpBean");
        MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
        MrpRemote mrp = mrpHome.create();
        ArrayList auxiliar = mrp.grillaReclamosComprador(request.getParameter("usuario"),mrp.validarUsuarioALink(usuario,"comprador"));
        if(request.getParameter("actualizarReclamo") != null){
        for(int i=0;i<auxiliar.size();i++){
        if(!request.getParameter("comprador"+i).equals("0")){   
        ModReclamo pla = (ModReclamo) auxiliar.get(i);
        mrp.actualizarCompradorReclamo(pla.getMaterial().getMateriaPrima(),request.getParameter("comprador"+i),pla.getNumero());
        }
        auxiliar = mrp.grillaReclamosComprador(request.getParameter("usuario"),mrp.validarUsuarioALink(usuario,"jefeCompras"));
        }
        }
        Vector compradores = mrp.compradores();
        
        %>
        <form action="reclamos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
            <br>
            <center><h4><font class="Estilo10">RECLAMOS</font></h4></center>
            <br>
            <%if(mrp.validarUsuarioALink(usuario,"jefeCompras")){%>
            <center><input type="submit" name="actualizarReclamo" id="actualizarReclamo" value="Actualizar" class="Estilo10"></center>
            <%}%>
            <br>
            <table border='1' cellpadding='0' cellspacing='0' align="center">
                <tr bgcolor='#FAE8D8'>
                    <td class='Estilo10' width='80'>Nº</td>
                    <td class='Estilo10' width='80'>Código</td>
                    <td class='Estilo10' width='200'>Descripción</td>
                    <td class='Estilo10' width='40'>UM</td>
                    <td class='Estilo10' width='80'>Cantidad</td>
                    <td class='Estilo10' width='80'>Usuario</td>
                    <td class='Estilo10' width='80'>Comprador</td>
                    <td class='Estilo10' width='200'>Observación</td>
                    <td class='Estilo10' width='80'>Fecha Ingreso</td>
                    <td class='Estilo10' width='80'>Fecha parte</td>
                    <td class='Estilo10' width='80'>Oc</td>
                    <td class='Estilo10' width='80'>Parte</td>
                    <td class='Estilo10' width='80'>Días atraso</td>
                </tr>
                <% for (int i = 0; i < auxiliar.size(); i++) {
            ModReclamo pla = (ModReclamo) auxiliar.get(i);%>
                <tr>
                    <td class='Estilo10' width='80'><a href='reclamosDetalle.jsp?reclamoUni=<%=pla.getNumero()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>'><%=pla.getNumero()%></a></td>
                    <td class='Estilo10' width='80'><%=pla.getMaterial().getMateriaPrima()%></td>
                    <td class='Estilo10' width='200'><%=pla.getMaterial().getDescripcion()%></td>
                    <td class='Estilo10' width='40'><%=pla.getMaterial().getUm()%></td>
                    <td class='Estilo10' width='80'><%=pla.getMaterial().getCantidad()%></td>
                    <td class='Estilo10' width='80'><%=pla.getUsuario()%></td>
                    <%if(pla.getMaterial().getComprador() != null && !pla.getMaterial().getComprador().equals("null")){%>
                    <td class='Estilo10' width='80'><%=pla.getMaterial().getComprador()%></td>
                    <%}else{%>
                    <td class='Estilo10' width='80'>
                        <select name="comprador<%=i%>" class="Estilo10">
                            <option value="0">Com</option>
                            <%for(int a=0;a<compradores.size();a++){%>
                            <%EstructuraPorEquipo estru = (EstructuraPorEquipo)compradores.elementAt(a);%>
                            <option value="<%=estru.getUsuario()%>" <%if(pla.getMaterial().getComprador().equals(estru.getUsuario())){%>selected<%}%>><%=estru.getUsuario()%></option>
                            <%}%>
                        </select>
                    </td>
                    <%}%>
                    <%if(pla.getObservacion() != null && !pla.getObservacion().trim().equals("")){%>
                    <td class='Estilo10' width='200'><%=pla.getObservacion()%></td>
                    <%}else{%>
                    <td class='Estilo10' width='200'>&nbsp;</td>
                    <%}%>
                    <td class='Estilo10' width='80'><%=fecha.format(pla.getFechaIngreso())%></td>
                    <%if (pla.getFechaAjuste() != null) {%>
                    <td class='Estilo1' width='80'><%=fecha.format(pla.getFechaAjuste())%></td>
                    <%} else {%>
                    <td class='Estilo1' width='80'>&nbsp;</td>
                    <%}%>
                    <td class='Estilo10' width='80'><%=pla.getCantOc()%></td>
                    <td class='Estilo10' width='80'><%=pla.getCantParte()%></td>
                    <td class='Estilo10' width='80'><%=pla.getAtraso()%></td>
                </tr>
                <%}%>
            </table>
            
        </form>
        
        
        <%}catch(Exception e){e.printStackTrace();}%> 
    </body>
</html>