<%@page contentType="text/html" import="mac.ee.pla.ordeCompra.clase.*,mac.ee.pla.ordenCompra.bean.OrdenBean,java.util.*" session="true"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar &oacute;rden</title>
        <link href="../CSS/estiloPaginas.css" rel="stylesheet" type="text/css" />
        <script language="javascript" type="text/javascript" src="../js/javascript.js"></script>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/tablas.css"/>
        <script type="text/javascript" src="../js/overlib.js"></script>
        <script>
            var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return (key >= 48 && key <= 57);
      }
       function informe(){
            if(confirm('¿Seguro que desea proseguir con la operación?')){
            document.form1.recibido1.value  = "yes";
            form1.submit();
            }else{
              return false;
            }
          }
          function Abrir_ventana5(pagina) {
                var opciones="toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=900, height=500, top=0, left=50"; 
                window.open(pagina,"",opciones);}
        </script>
    </head>
    <body>
        <%
        
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        if(idSesion.equals(sesion.getId())){%>
        
        <%try{      
        OrdenBean orden = new OrdenBean();
        
         String comentario = "";
        
        sesion.setAttribute("parame",orden.buscarOCNumero(request.getParameter("parametros")));
         //comentario = ((OrdenDeCompra)orden.elementAt(0)).getComentario();
         comentario = ((OrdenDeCompra)((Vector)sesion.getAttribute("parame")).get(0)).getComentario();
        %>
        <br>
        <center><h4><font class="Estilo10">ORDEN DE COMPRA</font></h4></center>
        <br>
        <form  name="form1" method="post" action="buscarOrden.jsp"> 
            <center class="Estilo10">
                <input type="submit" name="send" value="Aceptar" class="Estilo10">
                &nbsp;&nbsp;&nbsp;&nbsp;
                Buscar n&uacute;mero de documento <input type="text" name="parametros" onKeyPress="return acceptNum(event)" size="5" <%if(request.getParameter("parametros")!= null && !request.getParameter("parametros").equals("")){%>value="<%=request.getParameter("parametros")%>"<%}%>>
            </center>
            <br><br>
            <table width="999" class="tableone">
                <thead>
                    <tr>
                        <th width="59">Orden</th>
                        <th width="47">Item</th>
                        <th width="87">C&oacute;digo</th>
                        <th width="126">Descripci&oacute;n</th>
                        <th width="39">Um</th>
                        <th width="51">Cant</th>
                        <th width="62">Saldo</th>
                        <th width="67">Precio</th>
                        <th width="33">Co</th>
                        <th width="83">Emisi&oacute;n</th>
                        <th width="82">Entrega</th>                                    
                        <th width="39">Sap</th>
                        <th width="133">Proveedor</th>
                        <th width="133"> Fecha Proveedor</th>                       
                        <!--th width="32" title="Enviada">Check</td-->
                        <th width="20">&nbsp;</th> 
                    </tr>
                </thead>
                <tbody>  
                    <tr>
                        <td colspan="15">
                            <div class="innerb">
                                <table class="tabletwo" width="999">
                                    <%for(int i=0;i<((List)sesion.getAttribute("parame")).size();i++){%>  
                                                                        
                                    <%if(((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getAprobacion() != null && (((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getAprobacion().equals("9")) || (((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getAprobacion().equals("A"))){%>
                                    <tr class="dk1">
                                    <%}else{%>
                                    <tr class="dk">
                                        <%}%>   
                                        <!--td width="59"><a href="javascript:Abrir_ventana5('bitacoraOC.jsp?numOrden=<%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getNum_orden_compra()%>&usuario=<%=usuario%>&par=<%=request.getParameter("parame")%>&validarOrdenDeCompra=enviar')"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getNum_orden_compra()%></a></td-->
                                        <td width="59"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getNum_orden_compra()%></td>
                                        <td width="47"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getLinea()%></td>
                                        <td width="87"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getCod_producto()%></td>
                                        <td width="126">
                                            <a href="javascript:void(0);" onmouseover="return overlib('<%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getDescripcion()%>');" onmouseout="return nd();">
                                                <%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getDescripcion()%>
                                            </a>
                                        </td>
                                        <td width="39"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getUm()%></td>
                                        <td width="51"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getCan_pedida()%></td>
                                        <td width="62"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getSaldo()%></td>
                                        <td width="67"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getPrecio()%></td>
                                        <td width="33" title="<%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getComprador()%>"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getComprador().substring(0,2)%></td>
                                        <td width="83"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getFechaEntrada()%></td>
                                        <td width="82"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getFecha_entrega()%></td>
                                        
                                        
                                        
                                        <td width="39">                                 
                                            <a href="javascript:Abrir_ventana5('soloVerSap.jsp?idSesion=<%=idSesion%>&usuario=<%=usuario%>&tipou=<%=tipou%>&numSap=<%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getNumDocOrigen()%>')">
                                                <%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getNumDocOrigen()%>
                                            </a>
                                        </td> 
                                        <!--td width="39"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getNumDocOrigen()%></td-->  
                                        
                                        
                                        
                                        <td width="133"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getProveedor()%>
                                        </td>
                                         <td width="133"><%=((OrdenDeCompra)(((List)sesion.getAttribute("parame")).get(i))).getFechaConfirmada()%>
                                        </td>
                                        
                                      
                                        
                                        
                                       <!--<textarea readonly rows="4" cols="20"></textarea>-->
                                        
                                        <!--td width="32"><input type="checkbox" name="enviar<%--=i--%>"></td-->
                                    </tr>
                                    <%}%>
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>                       
            <table width="480" align="center" class="tableone">
                <thead>
                    <tr>                        
                        <th width="126">Comentarios</th>
                        <th width="20">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>  
                    <tr>
                        <td colspan="15">
                            <div class="innerb">
                                <table class="tabletwo" align="center" width="480">                                                                                                                                     
                                  <td><textarea readonly rows="8" cols="56"> <%=comentario%></textarea></td>                                                                                                                                                 
                                </table>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>                
            <table width="600px" class="Estilo10" align="center" border="1">
                <tr>
                    <td align="left">Num &oacute;rden</td>
                    <td align="left">Fecha</td>
                    <td align="left">Usuario</td>
                    <td align="left">Acci&oacute;n</td>
                </tr>
                <tr>
                    <td colspan="5" height="15">&nbsp;</td>
                </tr>
                <%Vector observa = orden.devolverEstadosOC(Long.parseLong(request.getParameter("parametros")));%>
                <%for(int i=0;i<observa.size();i++){%>
                <tr>
                    
                    <td align="left" width="150"><%=((OrdenDeCompra)observa.elementAt(i)).getNum_orden_compra()%></td>
                    <td align="left" width="150"><%=((OrdenDeCompra)observa.elementAt(i)).getFechaEntrada()%></td>
                    <td align="left" width="150"><%=((OrdenDeCompra)observa.elementAt(i)).getPersona()%></td>
                    <td align="left" width="150"><%=((OrdenDeCompra)observa.elementAt(i)).getDescripcion()%></td>
                    
                    
                </tr>
                <%}%>
        </table>
         <br>
            <table border="1" cellspacing="0" cellpadding="0" bgcolor="#cccccc" align="center" width="600">
                <tr bgcolor="#B9DDFB">
                    <td class="Estilo10" align="center">Datos anexos</td>
                    <td class="Estilo10" align="center">Fecha</td>
                    <td class="Estilo10" align="center">Usuario</td>
                </tr>
                <%Vector bitacoraOrden = orden.bitacoraOc(Long.parseLong(request.getParameter("parametros")));
                 Enumeration enum2 = bitacoraOrden.elements();%>
                <% while(enum2.hasMoreElements()){
                int increment = 0;
                OrdenDeCompra orden1 =  (OrdenDeCompra)enum2.nextElement();%>
                <tr>
                    <td align="left" class="Estilo10" title="<%=orden1.getDescripcion()%>"><%=orden1.getDescripcion()%></td>
                    <td align="center" class="Estilo10" width="150"><%=orden1.getFechaIni()%></td>
                    <td align="center" class="Estilo10" width="150"><%=orden.nombreUsuario1(orden1.getPersona())%></td>
                </tr>
                <%}%>  
                
            </table> 
        <!--br><br>
            <table border="0" width="570" align="center" class="Estilo10">
                <tr align="right">  
                    <td colspan="5" width="500">&nbsp;</td>
                    <td>Sin acci&oacute;n&nbsp;<input type="radio" name="vali" value="nada" checked/></td>
                </tr>
                <tr align="right">  
                    <td colspan="5" width="500">&nbsp;</td>
                    <td>Enviar&nbsp;<input type="radio" name="vali" value="valida" /></td>
                </tr>
                <tr align="right">  
                    <td colspan="5" width="500">&nbsp;</td>
                    <td>Anular&nbsp;<input type="radio" name="vali" value="anular" /></td>    
                </tr>
                <tr align="right">  
                    <td colspan="5" width="500">&nbsp;</td>
                    <td>Pendiente&nbsp;<input type="radio" name="vali" value="pendiente" /></td>    
                </tr>
            </table>
            <br-->    
        <%}catch(Exception e){out.println(e.getMessage());   
            }%>
        <%}else{response.sendRedirect("../mac");}%>
        
    </body>
</html>
