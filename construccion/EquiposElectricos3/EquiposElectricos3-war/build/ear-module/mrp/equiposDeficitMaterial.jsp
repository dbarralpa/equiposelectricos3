<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,conexionEJB.ConexionEJB" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <script language="JavaScript">
        
          <!--
            ns=document.layers
            ie=document.all
            function esconde() {
                if (ie) precarga.style.visibility="hidden";
                if(ns) document.pregarga.visibility="hide";
                document.getElementById('precarga').style.display ='none';
            }
            // -->
        </script>
    </head>
    <body onload="esconde();">
        <%
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        java.util.Calendar fecha = java.util.Calendar.getInstance();
        String date = fecha.get(java.util.Calendar.DATE) +"-"
                + (fecha.get(java.util.Calendar.MONTH)+1) + "-"
                + (fecha.get(java.util.Calendar.YEAR));
        /*String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        sesion.setAttribute("idSesion",idSesion);
        out.println(idSesion);
        out.println(usuario);
        out.println(tipou);*/
        if(idSesion.equals(sesion.getId())){
        %>
        <form action="equiposDeficitMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
        <%try{
        mac.ee.pla.crp2.bean.Crp2Bean crp = new mac.ee.pla.crp2.bean.Crp2Bean();
        //mac.ee.pla.mrp.bean.MrpBean mrp = new mac.ee.pla.mrp.bean.MrpBean();
        ConexionEJB cal = new ConexionEJB();
        Object ref1 =cal.conexionBean("MrpBean");
        MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
        MrpRemote mrp = mrpHome.create();    
        java.text.NumberFormat number = new java.text.DecimalFormat(" ");
        java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
        mrp.controlDeMateriales(new Vector());
        mrp.materialesProyectados("all","",0,"","",0,"");
        
        %>
         <center><h5><font class="Estilo10">EQUIPOS CON MATERIALES NEGATIVOS</font></h5></center>
        <br>
        <center><a href="../informeEquiposDeficit"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a></center>
        <br>
        <!--table width="90%" cellpadding="0" cellspacing="0">
            <tr>
                <td>
                    <script language="JavaScript">
                             if(ie || ns) document.write('<div id="precarga" align="center"><img src="../Images/espera.gif" width="200" height="40" alt="cargando2"/></div>');
                    </script>  
                </td>
            </tr>
        </table>
        <br><br>
        <center><h5><font class="Estilo10">EQUIPOS CON MATERIALES NEGATIVOS</font></h5></center>
        <br>
        <center><a href="../informeEquiposDeficit"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a></center>
        <br>
        <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc" width="1200">
            <tr bgcolor="#B9DDFB">
                <td align="center"><font class="Estilo9">C&oacute;digo</font></td>
                <td align="center"><font class="Estilo9">Descripci&oacute;n</font></td>
                <td align="center"><font class="Estilo9">Expeditaci&oacute;n</font></td>
                <td align="center"><font class="Estilo9">Np</font></td>
                <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                <td align="center"><font class="Estilo9">Serie</font></td>
                <td align="center"><font class="Estilo9">Descripci&oacute;n Equipo</font></td>
                <td align="center"><font class="Estilo9">Cantidad</font></td>
                <td align="center"><font class="Estilo9">Proceso</font></td>
                <td align="center"><font class="Estilo9">Lead Time</font></td>
                <td align="center"><font class="Estilo9">Fecha Quiebre</font></td>
                <td align="center"><font class="Estilo9">Stock</font></td>
                <td align="center"><font class="Estilo9" title="stock minimo">Stock Minimo</font></td>
                <td align="center"><font class="Estilo9" title="stock maximo">Stock Maximo</font></td>
                <td align="center"><font class="Estilo9" title="total">Total</font></td>
                
                
                
                
                
                
                
            </tr>
            
            <%--
            Vector equipos = new Vector();
            Vector ordenesCompra = new Vector();
            
            Vector materiales = mrp.controlDeMateriales();
            for(int y=0;y<materiales.size();y++){
                equipos       = crp.retornarEquiposConProblemas((String)((Vector)materiales.elementAt(y)).elementAt(0),formate.format(new java.util.Date()));
                float total = 0;
                //ordenesCompra = orden.ordenDeCompraValidas(request.getParameter("codpro"),request.getParameter("fecha"));
                for(int u=0;u<equipos.size();u++){
            %>
            
            <tr>
                <td align="center"><font class="Estilo9"><%=(String)((Vector)materiales.elementAt(y)).elementAt(0)%></font></td>
                <td align="center"><font class="Estilo9"><%=(String)((Vector)materiales.elementAt(y)).elementAt(1)%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getExpeditacion())%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getNp()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getSerie()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getDescripcion()%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getCantidad())%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getProceso()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getLeadTime()%></font></td>
                <td align="center"><font class="Estilo9"><%=(String)((Vector)materiales.elementAt(y)).elementAt(2)%></font></td>
                
                <td align="center"><font class="Estilo9"><%=number.format(((Float)((Vector)materiales.elementAt(y)).elementAt(7)).floatValue())%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getStockMinimo())%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getStockMaximo())%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format((Float)((Vector)materiales.elementAt(y)).elementAt(3))%></font></td>         
                
            </tr>
            <%
            total += ((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getCantidad();
                }%> 
            <tr>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td class="Estilo10"><%=total%></td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
            </tr>
            <%Vector ordenesVencidas = new mac.ee.pla.ordenCompra.bean.OrdenBean().ordenDeCompraInvalidas((String)((Vector)materiales.elementAt(y)).elementAt(0));%>
            <%if(ordenesVencidas.size()>0){%>
            <tr>
                <td align="center"><font class="Estilo9">Nº Orden Vencida</font></td>
                <td align="center"><font class="Estilo9">Fecha Entrega</font></td>
                <td align="center"><font class="Estilo9">Cantidad Pendiente</font></td>
            </tr>
            <% for(int p=0;p<ordenesVencidas.size();p++){
            %>
            <tr class="Estilo10">
                <td><%=((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesVencidas.elementAt(p)).getNum_orden_compra()%></td>
                <td><%=((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesVencidas.elementAt(p)).getFecha_entrega()%></td>
                <td><%=((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesVencidas.elementAt(p)).getCan_pendiente()%></td>
            </tr>
            <%}}%>
            <%Vector ordenesFuturas = new mac.ee.pla.ordenCompra.bean.OrdenBean().ordenDeCompraValidas((String)((Vector)materiales.elementAt(y)).elementAt(0));%>
            <%if(ordenesFuturas.size()>0){%>
            <tr>
                <td align="center"><font class="Estilo9">Nº Orden Futura</font></td>
                <td align="center"><font class="Estilo9">Fecha Entrega</font></td>
                <td align="center"><font class="Estilo9">Cantidad Pendiente</font></td>
            </tr>
            <% for(int p=0;p<ordenesFuturas.size();p++){
            %>
            <tr class="Estilo10">
                <td><%=((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesFuturas.elementAt(p)).getNum_orden_compra()%></td>
                <td><%=((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesFuturas.elementAt(p)).getFecha_entrega()%></td>
                <td><%=((mac.ee.pla.ordeCompra.clase.OrdenDeCompra)ordenesFuturas.elementAt(p)).getCan_pendiente()%></td>
            </tr>
            <%}}%>
            <tr>
                <td colspan="12">&nbsp;</td>
            </tr>
            <%}--%>
        </table-->
        
        <%}catch(Exception e){
        out.println("ERROR: equiposConProblemas.jsp " + e.getMessage());
        }%>
        
        <%}else{response.sendRedirect("../mac");}%>
    </body>
</html>