<%@page contentType="text/html" import="mac.ee.pla.mrp.clase.ModReclamo,mac.ee.pla.mrp.bean.*,java.util.*" session="true"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Buscar &oacute;rden</title>
        <link href="../CSS/estiloPaginas.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../themes/base/jquery.ui.all.css"/>
            <script type="text/javascript" src="../js/jquery-1.3.2.js"></script>
            <script language="javascript" type="text/javascript" src="../js/jquery.ui.core.js"></script>
            <script language="javascript" type="text/javascript" src="../js/jquery.ui.widget.js"></script>
            <script language="javascript" type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
        <script language="javascript" type="text/javascript" src="../js/javascript.js"></script>
        <script type="text/javascript">
            function validarFecha(){
              if(document.form1.fechaRechazoIni.value == '' || document.form1.fechaRechazoFin.value == ''){
                alert('Debe ingresar ambas fechas');
                return false;
              }
            }
            
            function validarNp(){
              if(document.form1.np.value == '' || document.form1.linea.value == ''){
                alert('Debe ingresar np e item');
                return false;
              }
            }
            
            function fecha(){
            if(document.form1.fechaRechazoIni.value == '' || document.form1.fechaRechazoFin.value == ''){
            alert('Debe ingresar las dos fechas');
            return false;
            }
            }
        </script>
         <script>
            $(document).ready(function(){
            $( "#fechaRechazoIni" ).datepicker();
            $( "#fechaRechazoFin" ).datepicker();
        });
</script>
    </head>
    <body>
        <%
        
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        MrpBean mrp = new MrpBean();
        sesion.setAttribute("equipos",new ArrayList());
        sesion.setAttribute("reclamosPlanos",new ArrayList());
        if(idSesion.equals(sesion.getId())){%>
        
        <%try{      
        if(request.getParameter("rReclamo") != null && request.getParameter("rReclamo").trim().equals("codigo")){    
        if(request.getParameter("buscarPorFecha") != null){
        if(request.getParameter("fechaRechazoIni") != null && !request.getParameter("fechaRechazoIni").trim().equals("") && !request.getParameter("fechaRechazoFin").trim().equals("")){
        
        String[] fechaIni = request.getParameter("fechaRechazoIni").split("/");
        String[] fechaFin = request.getParameter("fechaRechazoFin").split("/");
        if(request.getParameter("reclamosVigentes") == null){
        sesion.setAttribute("equipos",mrp.grillaReclamos("re.dt_fecha_ingreso between '"+(fechaIni[2]+fechaIni[1]+fechaIni[0])+"' and '"+(fechaFin[2]+fechaFin[1]+fechaFin[0])+"'"));
        }else{
        sesion.setAttribute("equipos",mrp.grillaReclamos("re.dt_fecha_ingreso between '"+(fechaIni[2]+fechaIni[1]+fechaIni[0])+"' and '"+(fechaFin[2]+fechaFin[1]+fechaFin[0])+"' and re.nb_estado = 1"));    
        }
        }
        }
        
        if(request.getParameter("buscarNp") != null){
        if(request.getParameter("np") != null && !request.getParameter("np").trim().equals("") && !request.getParameter("linea").trim().equals("")){
        sesion.setAttribute("equipos",mrp.grillaReclamos("ree.nb_np = "+Integer.parseInt(request.getParameter("np").trim())+" and ree.nb_linea = "+Integer.parseInt(request.getParameter("linea").trim())));
        }
        }
        }
        
        if(request.getParameter("rReclamo") != null && request.getParameter("rReclamo").trim().equals("planos")){    
        if(request.getParameter("buscarPorFecha") != null){
        if(request.getParameter("fechaRechazoIni") != null && !request.getParameter("fechaRechazoIni").trim().equals("") && !request.getParameter("fechaRechazoFin").trim().equals("")){
        
        String[] fechaIni = request.getParameter("fechaRechazoIni").split("/");
        String[] fechaFin = request.getParameter("fechaRechazoFin").split("/");
        if(request.getParameter("reclamosVigentes") == null){
        sesion.setAttribute("reclamosPlanos",mrp.grillaReclamosPorPlanos("re.dt_fecha_creacion between '"+(fechaIni[2]+fechaIni[1]+fechaIni[0])+"' and '"+(fechaFin[2]+fechaFin[1]+fechaFin[0])+"'"));
        }else{
        sesion.setAttribute("reclamosPlanos",mrp.grillaReclamosPorPlanos("re.dt_fecha_creacion between '"+(fechaIni[2]+fechaIni[1]+fechaIni[0])+"' and '"+(fechaFin[2]+fechaFin[1]+fechaFin[0])+"' and re.nb_id_estado = 1"));    
        }
        }
        }
        
        if(request.getParameter("buscarNp") != null){
        if(request.getParameter("np") != null && !request.getParameter("np").trim().equals("") && !request.getParameter("linea").trim().equals("")){
        sesion.setAttribute("reclamosPlanos",mrp.grillaReclamosPorPlanos("re.nb_np = "+Integer.parseInt(request.getParameter("np").trim())+" and re.nb_linea = "+Integer.parseInt(request.getParameter("linea").trim())));
        }
        }
        }
        
        
        %>
        <br>
        <center><h4><font class="Estilo10">RECLAMOS</font></h4></center>
        <br>
        <form  name="form1" method="post" action="analisisReclamos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"> 
            
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                <tr class="Estilo10">
                    <td>
                        <fieldset>
                            <legend>Fecha de Inicio:</legend>
                            <!--DIV id="popCal1" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                            <IFRAME name="popFrame1" src="../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                            <SCRIPT event=onclick() for=document>popCal1.style.visibility = "hidden";</SCRIPT>
                            <TABLE align="center" CELLPADDING="0" CELLSPACING="0">
                                <TR>
                                    <TD ALIGN="right">
                                        <input class="Estilo10" readOnly name="fechaRechazoIni" size="12" value="<%--if(request.getParameter("fechaRechazoIni") != null && !request.getParameter("fechaRechazoIni").trim().equals("")){%><%=request.getParameter("fechaRechazoIni")%><%}--%>">
                                        <a onclick="popFrame1.fPopCalendar(fechaRechazoIni,fechaRechazoIni,popCal1);return false">
                                        <img src="../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                    </td>
                                </tr>
                            </table-->
                             <input id="fechaRechazoIni" name="fechaRechazoIni" type="text" size="15" readonly="yes" class="txtFecha"  />
                        </fieldset>
                    </td>    
                    <td>
                        <fieldset>
                            <legend>Fecha de fin:</legend>
                            <!--DIV id="popCal2" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                            <IFRAME name="popFrame2" src="../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                            <SCRIPT event=onclick() for=document>popCal2.style.visibility = "hidden";</SCRIPT>
                            <TABLE align="center" CELLPADDING="0" CELLSPACING="0">
                                <TR>
                                    <TD ALIGN="right">
                                        <input class="Estilo10" readOnly name="fechaRechazoFin" size="12" value="<%--if(request.getParameter("fechaRechazoFin") != null && !request.getParameter("fechaRechazoFin").trim().equals("")){%><%=request.getParameter("fechaRechazoFin")%><%}--%>">
                                        <a onclick="popFrame2.fPopCalendar(fechaRechazoFin,fechaRechazoFin,popCal2);return false">
                                        <img src="../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                    </td>
                                </tr>
                            </table-->
                            <input id="fechaRechazoFin" name="fechaRechazoFin" type="text" size="15" readonly="yes" class="txtFecha"  />
                        </fieldset>
                    </td>
                    <td>
                        <input class="Estilo10" type="submit" name="buscarPorFecha" id="buscarPorFecha" value="Buscar" onclick="return validarFecha();">
                    </td>
                    <td width="20">&nbsp;</td>
                    <td>
                        <fieldset>
                            <legend>Np/Item</legend>
                            Np:&nbsp;<input type="text" size="5" name="np" id="np">&nbsp;Linea:&nbsp;<input type="text" size="5" name="linea" id="linea">
                            <input class="Estilo10" type="submit"  name="buscarNp" id="buscarNp" value="Buscar" onclick="return validarNp();">
                        </fieldset>
                    </td>
                </tr>
                
            </table>
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                <tr>
                    <td height="20">&nbsp;</td>
                </tr>
                <tr>
                    <!--td>
                        Reclamos vigentes: &nbsp;<input type="checkbox" name="reclamosVigentes" id="reclamosVigentes">
                    </td-->
                    <td class="Estilo10">
                        Reclamo por código:<input type="radio" name="rReclamo" id="rReclamo" value="codigo">
                        &nbsp;Reclamo por planos:<input type="radio" name="rReclamo" id="rReclamo" value="planos">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td height="60">&nbsp;</td>
                    <td>
                        Reclamos por código:&nbsp;
                    </td>
                    <td>
                        <a href="../Reclamos?reclamo=codigo"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a>    
                    </td>               
                    <td width="30">&nbsp;</td>
                    <td>
                        Reclamos por planos:&nbsp;
                    </td>
                    <td>
                        <a href="../Reclamos?reclamo=planos"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a>    
                    </td>               
                </tr>
            </table>
             <br><br>
        <center><a href="retornarEstadoMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" target="_parent"><img src="../Images/volver2.jpg" border="0"/></a></center>
        </form>
        <%}catch(Exception e){out.println(e.getMessage());   
        }%>
        <%}else{response.sendRedirect("../mac");}%>
        
    </body>
</html>
