<%@page import="java.io.*, java.util.*,javax.naming.*,mac.contador.Contador,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,sc.validarLink.ValidarLink,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

 
   <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
    </head>
    <body onload=ajustaCeldas()>
    <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               if(idSesion.equals(sesion.getId())){
        %>
    <form action="materialesProyectados.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
        <%! String m; %>    
        <%try{
            ConexionEJB cal = new ConexionEJB();
            Object ref1 =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
            MrpRemote mrp = mrpHome.create();  
            if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("")){
               //Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
            }
            java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
            java.text.NumberFormat number = new java.text.DecimalFormat(" ");
            List materiales = (List)mrp.controlDeMateriales();
            ValidarLink validaLink = new ValidarLink();
             if(request.getParameter("valor") != null){
                Collections.sort(materiales,new ordenamiento.Ordenamiento(Byte.parseByte(request.getParameter("valor")),(byte)1));    
              }
           %>
            <center><h4><font class="Estilo10">MATERIALES PROYECTADOS</font></h4></center>
            <center><h4><font class="Estilo10">Ultima actualizaci&oacute;n&nbsp;&nbsp;<%=mrp.getFecha()%></font></h4></center>
            <br>
             <table cellpadding="0" cellspacing="0" class="Estilo10" align="center" >
                <tr class="Estilo10">
                    <td><a href="index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></td>
                    <td><input type="submit" value="Actualizar" name="actualizar" class="Estilo10"></td>
                    <!--td  width="10"></td>
                    <td><a href="EquipoSeleccionado.jsp?idSesion=<%--=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou--%>" class="Estilo10">Seleccionar Equipos por fecha</a></td-->
                    <td  width="10"></td>
                    <%--if(validaLink.validarUsuarioALink(usuario).equals("actualizarComprasRequeridas")){--%>
                    <%if(usuario.equals("vhernandez") || usuario.equals("gvillaroel") || usuario.equals("jmaldonado") || usuario.equals("dbarra") || usuario.equals("jgonzalez")){%>
                    <td><a href="http://mac:12080/EquiposDeficitMateriales/index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" class="Estilo10">Informe equipos con d&eacute;ficit</a></td>
                    <%}else{%>
                    <td>Actualizar Pedidos</td>
                    <%}%>
                    <td  width="10"></td>
                    <%if(mrp.validarUsuarioALink(usuario,"comprasRequeridas")){%>
                    <%--if(usuario.equals("bsantana") || usuario.equals("marsanchez") || usuario.equals("dbarra") || usuario.equals("vhernandez") || usuario.equals("miguelangel") || usuario.equals("jaceituno") || usuario.equals("lcaceres") || usuario.equals("jmaldonado") || usuario.equals("arondon")|| usuario.equals("jcduran") || usuario.equals("ccarvalho") || usuario.equals("jbriones")
                          || usuario.equals("gvillaroel") || usuario.equals("cpizarro") || usuario.equals("esantander") || usuario.equals("msch") || usuario.equals("evarela")  || usuario.equals("rcea")  || usuario.equals("evillar")){--%>
                    <td><a href="comprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" class="Estilo10" accesskey="C">Necesidades de compra</a></td>
                    <%}else{%>
                    <td>Compras requeridas</td>
                    <%}%>
                    <td  width="10"></td>
                    <td><a href="equiposDeficitMaterial.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" class="Estilo10">Equipos con d&eacute;ficit de materiales</a></td>
                    </td>
                </tr>
           </table>
            <br>
            
                <!--table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center" class="sortable" id="unique_id"-->
                <table border=0 bgcolor=scrollbar align=center>
                <td>
                <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc">
                    <tr bgcolor="#B9DDFB">
                        <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                        <td align="center"><font class="Estilo9"><a href="materialesProyectados.jsp?valor=1&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">CODIGO</a></font></td>
                        <td align="center"><font class="Estilo9"><a href="materialesProyectados.jsp?valor=2&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">DESCRIPCION</a></font></td>
                        <td align="center"><font class="Estilo9" title="fecha de quiebre de un material">QUIEBRE</font></td>
                        <td align="center"><font class="Estilo9">TOTAL</font></td>
                        <td align="center"><font class="Estilo9" title="d&iacute;as que dura quiebre">DIAS</font></td>
                        <td align="center"><font class="Estilo9">BODEGA</font></td>
                        <td align="center"><font class="Estilo9" title="unidad de medida">UM</font></td>
                        <td align="center"><font class="Estilo9">STOCK MIN</font></td>
                        <td align="center"><font class="Estilo9">STOCK MAX</font></td>
                        <td align="center"><font class="Estilo9" title="&oacute;rdenes de compra">OC</font></td>
                        <td align="center"><font class="Estilo9" title="lead time">LT</font></td>
                        <td align="center"><font class="Estilo9" title="Compras Requeridas">CR</font></td>
                        <td align="center"><font class="Estilo9" title="balance">B</font></td>
                        <td align="center"><font class="Estilo9">&nbsp;&nbsp;&nbsp;</font></td>
                        
                    </tr>
                </table>
                <div style="overflow:auto; height:350px; padding:0">
                <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                <%
                
                for(int u=0;u<materiales.size();u++){%>
                <tr class="Estilo9">
                    
                   <%if(request.getParameter("T"+u) != null){%>

                                  <%             
                                               mrp.comprasRequeridas((String)((List)materiales.get(u)).get(0),
                                               (String)((List)materiales.get(u)).get(1),
                                               Float.valueOf("0").floatValue(),
                                               (String)((List)materiales.get(u)).get(8),                   
                                               (String)((List)materiales.get(u)).get(15),
                                               ((Short)((List)materiales.get(u)).get(12)).shortValue(),
                                               formate.format(new java.sql.Date(0)),
                                               ((Float)((List)materiales.get(u)).get(9)).floatValue(),
                                               ((Float)((List)materiales.get(u)).get(10)).floatValue(),
                                               formate.format(new java.sql.Date(0)),
                                               0,
                                               (byte)1
                                               );
                                               ((List)materiales.get(u)).set(14,new Integer(1));
                      }
                                   %>
                    
                    <%if(((Integer)((List)materiales.get(u)).get(14)).intValue() > 0){%>
                    <td align="center" bgcolor="#FFFF00"><a name="<%=(String)((List)materiales.get(u)).get(0)%>"><%=(String)((List)materiales.get(u)).get(0)%></a></td>
                    <%}else{%>
                    <td align="center"><a name="<%=(String)((List)materiales.get(u)).get(0)%>"><%=(String)((List)materiales.get(u)).get(0)%></a></td>
                    <%}%>
                    <%if(request.getParameter("mater") != null && !request.getParameter("mater").equals("")){%>
                    <%if(request.getParameter("mater").equals((String)((List)materiales.get(u)).get(0))){%>
                        <td align="center" bgcolor="#FF0000"><%=(String)((List)materiales.get(u)).get(1)%></td>
                    <%}else{%>
                       <td align="center"><%=(String)((List)materiales.get(u)).get(1)%></td>
                   <%}}else{%>
                    <td align="center"><%=(String)((List)materiales.get(u)).get(1)%></td>
                    <%}%>
                    <td align="center"><%=(String)((List)materiales.get(u)).get(2)%>
                        <%if(((List)((List)materiales.get(u)).get(6)).size() > 0){%>
                        <br>
                        <%for(int t=0;t< ((Vector)((List)materiales.get(u)).get(6)).size();t++){
                        if (t==0 || (t%3==0 && t > 2)){%>   
                        <%=((List)((List)materiales.get(u)).get(6)).get(t)%><br>
                        <%}}%>
                    </td>
                    <%}else{%>
                    </td>
                    <%}%>
                    <td align="center"><%=number.format((Float)((List)materiales.get(u)).get(3))%>         
                        <%if(((List)((List)materiales.get(u)).get(6)).size() > 0){%>
                        <br>
                        <%for(int t=0;t< ((List)((List)materiales.get(u)).get(6)).size();t++){
                        if (t==1){%> 
                        <%=number.format((Float)(((List)((List)materiales.get(u)).get(6)).get(t)))%><br>
                        <%}
                        if((t%3==0 && t > 2)){%>
                        <%=number.format((Float)(((List)((List)materiales.get(u)).get(6)).get(t+1)))%><br>
                        <%}   
                        }%>
                    </td>
                    <%}else{%>
                    </td>
                    <%}%>       
                    
                    
                    <%if(((Integer)((List)materiales.get(u)).get(5)).intValue() == 2){%>
                    <td align="center" onclick='this.style.background="#FF0000"' ondblclick='this.style.background="#cccccc"'><a href="javascript:Abrir_ventana('equiposConProblemas.jsp?codpro=<%=(String)((List)materiales.get(u)).get(0)%>&descri=<%=(String)((List)materiales.get(u)).get(1)%>&fecha=<%=(String)((List)materiales.get(u)).get(2)%>')"><%=(Integer)((List)materiales.get(u)).get(4)%></a>
                        <%}else{%>
                        <td align="center" onclick='this.style.background="#FF0000"' ondblclick='this.style.background="#cccccc"'><a href="javascript:Abrir_ventana('equiposConProblemas.jsp?codpro=<%=(String)((List)materiales.get(u)).get(0)%>&descri=<%=(String)((List)materiales.get(u)).get(1)%>&fecha=<%=(String)((List)materiales.get(u)).get(2)%>')">>></a>
                            <%}%>
                            <%if(((List)((List)materiales.get(u)).get(6)).size() > 0){%>
                            <br>
                            <%for(int t=0;t< ((List)((List)materiales.get(u)).get(6)).size();t++){
                            if (t==2){%>  
                            <a href="javascript:Abrir_ventana('equiposConProblemas.jsp?codpro=<%=(String)((List)materiales.get(u)).get(0)%>&descri=<%=(String)((List)materiales.get(u)).get(1)%>&fecha=<%=((List)((List)materiales.get(u)).get(6)).get(t-2)%>')"><%=((List)((List)materiales.get(u)).get(6)).get(t)%></a><br>
                            <%}
                            
                            if(t%3==0 && t > 2){%>
                            <a href="javascript:Abrir_ventana('equiposConProblemas.jsp?codpro=<%=(String)((List)materiales.get(u)).get(0)%>&descri=<%=(String)((List)materiales.get(u)).get(1)%>&fecha=<%=((List)((List)materiales.get(u)).get(6)).get(t)%>')"><%=((List)((List)materiales.get(u)).get(6)).get(t+2)%></a><br>
                            <%}      
                            }%>
                        </td>
                        <%}else{%>
                    </td>
                    <%}%>       
                    <td align="center"><%=number.format(((Float)((List)materiales.get(u)).get(7)))%></td>
                    <td align="center"><%=(String)((List)materiales.get(u)).get(8)%></td>
                    <td align="center"><%=number.format(((Float)((List)materiales.get(u)).get(9)))%></td>
                    <td align="center"><%=number.format(((Float)((List)materiales.get(u)).get(10)))%></td>
                    <%//if(((String)((List)materiales.get(u)).get(11)).equals("SI")){%>
                    <td align="center"><a href="javascript:Abrir_ventana5('ordenesDeCompra.jsp?codpro=<%=(String)((List)materiales.get(u)).get(0)%>&descri=<%=(String)((List)materiales.get(u)).get(1)%>')"><%=(String)((List)materiales.get(u)).get(11)%></a></td>
                    <%//}else{%>
                    <!--td align="center"><%//=(String)((List)materiales.get(u)).get(11)%></td-->
                    <%//}%>
                   
                    <%if(((Short)((List)materiales.get(u)).get(12)).floatValue() > (short)0){
                         if(((Short)((List)materiales.get(u)).get(13)).floatValue() > (short)7){%>
                         <td align="center" bgcolor="#00FF00"><%=(Short)((List)materiales.get(u)).get(12)%></td>
                         <%}%>
                         <%if(((Short)((List)materiales.get(u)).get(13)).floatValue() >= (short)0 && ((Short)((List)materiales.get(u)).get(13)).floatValue() <= (short)7){%>  
                         <td align="center" bgcolor="#FF9900"><%=(Short)((List)materiales.get(u)).get(12)%></td>
                         <%}%>
                          <%if(((Short)((List)materiales.get(u)).get(13)).floatValue() < (short)0){%>  
                          <td align="center" bgcolor="#FF0000"><%=(Short)((List)materiales.get(u)).get(12)%></td>
                          <%}%>
                    <%}else{%>
                    <td align="center"><%=(Short)((List)materiales.get(u)).get(12)%></td>
                    <%}%>
                    <%if(usuario.equals("vhernandez")){%>
                    <td align="center"><input type=checkbox name="T<%=u%>" class="Estilo10"></td>
                    <%}else{%>
                    <td align="center"><input type=checkbox name="T<%=u%>" class="Estilo10" disabled></td>
                    <%}%>
                    <td align="center" class="Estilo10"><a href="balancePorMaterial.jsp?codPro=<%=(String)((List)materiales.get(u)).get(0)%>&descri=<%=(String)((List)materiales.get(u)).get(1)%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">B</a></td>
                    
                </tr>
                <%}%>
            </table>
            </div>
            </td>
           </table> 
            <%}catch(Exception e){
                   out.println("ERROR: materialesProyectados " + e.getMessage());
            }        
            %>
        </form>
         <%}else{response.sendRedirect("../mac");}%>
       </body>
     </html>
       