<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

   <html>
       <head>
           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
           <title>Manufactura asistida por computadora</title>
            <SCRIPT LANGUAGE="Javascript">

            var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return (key >= 48 && key <= 57);

            }
            
            function form_click() {
                 var myForm = document.form1;
                 myForm.leadTime.focus();
            }
            function validar(){

           if (document.form1.leadTime.value == ""){
              alert("Tiene que ingresar un valor valido");
              return false;
              }
           }   
         </SCRIPT>       

       </head>
       <body onload="form_click()">
            <form action="leadTimes.jsp" method="post" name="form1">
           <%
              HttpSession sesion    = request.getSession(); 
              try{
               
                 ConexionEJB cal = new ConexionEJB();
                 Object ref =cal.conexionBean("estructuraEquipoBean");
                 estructuraEquipoRemoteHome estructuraHome = (estructuraEquipoRemoteHome) PortableRemoteObject.narrow(ref, estructuraEquipoRemoteHome.class);
                 estructuraEquipoRemote estructura= estructuraHome.create();
               if(request.getParameter("guardar") == null){
                   estructura.setCodigoMaterial(request.getParameter("codpro"));
                 }
             
               %>
               
                   <br>
                    <%
               if(request.getParameter("guardar") != null){
                  byte verificador = estructura.actualizarLeadTime(estructura.getCodigoMaterial(),Short.parseShort(request.getParameter("leadTime")));
                  if(verificador == 1){
                     sesion.setAttribute("leadTime",new Byte((byte)1));
                     //sesion.setAttribute("aa",estructura.getCodigoMaterial());
                     //out.println((String)sesion.getAttribute("codpro"));
                      %>
                  <center><h5><font class="Estilo10">Actualiz&oacute; los datos</font></h5></center>
                  <center><a href="javascript:window.opener.document.location.reload();self.close()">CERRAR</a></center>
                  <%}else{%>
                  <center><h5><font class="Estilo10">No Actualiz&oacute; los datos</font></h5></center>
               <%}}else{%>
                   <center><h5><font class="Estilo10">Cambiar LeadTime</font></h5></center>
                   <br>
                   <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                       <tr bgcolor="#B9DDFB">
                           <td class="Estilo10" colspan="2" align="center">Lead Time a agregar</td>
                       </tr>
                       <tr>
                           <td><font class="Estilo9">Material</font></td>
                           <td><font class="Estilo9">LeadTime</font></td>
                       </tr>
                       <tr>
                           <td align="center"><font class="Estilo9"><%=estructura.getCodigoMaterial()%></font></td>
                           <td align="center"><font class="Estilo9"><input name="leadTime" type="text" onKeyPress="return acceptNum(event)"></font></td>
                       </tr>
                   </table>
                   <br><br>
                   <center><input type="submit" value="Guardar" name="guardar" class="Estilo10" onclick="return validar()"></center>
              
               <%}%>
               </form>
               
              
           <%}catch(Exception e){e.printStackTrace();}%>
 
         
       </body>
   </html>