<%@page import="mac.contador.Contador,java.io.*,java.text.NumberFormat,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.mrp.bean.*,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <%
    HttpSession sesion    = request.getSession();
    String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    /*String idSesion       = request.getParameter("idSesion");
    String usuario        = request.getParameter("usuario");
    String tipou          = request.getParameter("tipou");
    sesion.setAttribute("idSesion",idSesion);
    out.println(idSesion);
    out.println(usuario);
    out.println(tipou);*/
    if(idSesion.equals(sesion.getId())){
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--meta http-equiv="refresh" content="30"-->
        <title>Manufactura asistida por computador</title>
        <link  rel="stylesheet" type="text/css"  href="../../themes/base/jquery.ui.all.css"/>
        <script type="text/javascript" src="../../js/jquery-1.3.2.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.ui.core.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.ui.widget.js"></script>
        <script language="javascript" type="text/javascript" src="../../js/jquery.ui.datepicker.js"></script>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script>
        function actualiza(valor){
            if(confirm('�Seguro que desea proseguir con la operaci�n?')){
            form1.submit(); 
            }else{
            valor.checked = false; 
            }
        }
        function eliminar(){
        document.form1.eliminated.value = 2
        form1.submit();
        }
        </script>
        
        <script language="JavaScript">
        
          <!--
            ns=document.layers
            ie=document.all
            function esconde() {
                if (ie) precarga.style.visibility="hidden";
                if(ns) document.pregarga.visibility="hide";
                document.getElementById('precarga').style.display ='none';
            }
            // -->
        </script>
        
    </head>
    <body onload="esconde();">
        <script>
     $(document).ready(function(){
            $( "#fechaMayor" ).datepicker();
            $( "#fechaMenor" ).datepicker();
        });
  
        </script>
        <table width="90%" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td>
                        <script language="JavaScript">
                             if(ie || ns) document.write('<div id="precarga" align="center"><img src="../../Images/espera.gif" width="200" height="40" alt="cargando2"/></div>');
                        </script>  
                    </td>
                </tr>
            </table>
        <center><h5><font class="Estilo10">PEDIDOS DE MATERIALES</font></h5></center>
        <%if(tipou.equals("usuAdm") || tipou.equals("adm")){%>
        <form action="/mac/jsp/sistemas/EquiposElectricosAdm.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <input type="image" src="../../Images/volver2.jpg" border="0" width="89" height="18">
        </form>
        <%}if(tipou.equals("usu")){%>
        <form action="/mac/jsp/sistemas/EquiposElectricos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <input type="image" src="../../Images/volver2.jpg" border="0" width="89" height="18">
        </form>
        <%}%>
        <form name="form1" method="post">
            <%try{
            if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("") && request.getParameter("eliminated") == null){
            //Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
            }    
            Vector necesidades = new Vector();
            Vector pedidoConDeficit = new Vector();
            ConexionEJB cal1 = new ConexionEJB();  
            Object ref =cal1.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref,MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            Vector pedidos = new Vector();
            if(request.getParameter("eliminated") != null && request.getParameter("eliminated").equals("2")){
            for(int i=0;i<((Vector)sesion.getAttribute("eliminar")).size();i++){
            if(request.getParameter("eliminar"+i) != null){
            mrp.borrarPedido(((EstructuraPorEquipo)((Vector)sesion.getAttribute("eliminar")).elementAt(i)).getNumPedido());
            }
            }
            }
            pedidos = mrp.pedido();    
            sesion.setAttribute("eliminar",pedidos);
            if(request.getParameter("enviarPedido") != null && request.getParameter("npedido") != null && !request.getParameter("npedido").trim().equals("")){
            pedidos = mrp.pedidoPorNumero(Long.parseLong(request.getParameter("npedido")));    
            sesion.setAttribute("eliminar",pedidos);    
            }
            if(request.getParameter("enviarFechas") != null && request.getParameter("fechaMayor") != null && !request.getParameter("fechaMayor").trim().equals("") && !request.getParameter("fechaMenor").trim().equals("")){
            pedidos = mrp.pedidoPorFechas(request.getParameter("fechaMenor"), request.getParameter("fechaMayor"));    
            sesion.setAttribute("eliminar",pedidos);    
            }
            if(request.getParameter("todos") != null){
            pedidos = mrp.todosPedidos();    
            sesion.setAttribute("eliminar",pedidos);    
            }
            %>
            
            
            
            
            <!--center><a href="../index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../../Images/volver2.jpg" border="0"></center></a-->
            
            <table cellspacing="0" cellpadding="0" align="center">
                <tr>
                    <td>
                        <table  cellspacing="0" cellpadding="0" align="center">
                            <tr>
                                <td>
                                    <fieldset>
                                        <legend class="Estilo10">Buscar por n� pedido</legend>
                                        <input type="text" name="npedido" id="npedido" class="Estilo10" onkeydown="acceptNum(event);">
                                        &nbsp;<input type="submit" name="enviarPedido" id="enviarPedido" value="ir" class="Estilo10">
                                    </fieldset>
                                </td>
                                <td width="20">&nbsp;</td>
                                <td>
                                    <fieldset class="Estilo10">
                                        <legend class="Estilo10">Buscar por fechas</legend>
                                        Entre&nbsp;<input id="fechaMenor" name="fechaMenor" type="text" size="15" readonly="yes"  />
                                        &nbsp;
                                        y&nbsp;<input id="fechaMayor" name="fechaMayor" type="text" size="15" readonly="yes"  />
                                        &nbsp;<input type="submit" name="enviarFechas" id="enviarFechas" value="ir" class="Estilo10">
                                    </fieldset>
                                </td>
                                <td width="20">&nbsp;</td>
                                <td>
                                    <input class="Estilo10" type="submit" name="todos" id="todos" value="Todos los pedidos">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>                     
            
            <table bgcolor="#cccccc" cellspacing=0 cellpadding=2 align="center">
                <%
                if(pedidos.size() > 0){%>
                
                <tr>
                    <td>
                        <table border=1 bgcolor="#cccccc" cellspacing=0 cellpadding=2 align="center">
                            <tr bgcolor="#B9DDFB">
                                <td class="Estilo10" align="center">N� de pedido</td>
                                <td class="Estilo10" align="center" width="100">Area</td>
                                <td class="Estilo10" align="center" width="100">Usuario</td>
                                <td class="Estilo10" align="center" width="100">Supervisor</td>
                                <td class="Estilo10" align="center">Estado</td>
                                <td class="Estilo10" align="center" width="100">Fecha creaci&oacute;n</td>
                                <td class="Estilo10" align="center" width="100">Eliminar</td>
                                
                            </tr>
                            <%for(int i=0;i<pedidos.size();i++){%>
                            
                            <tr bgcolor="#FFFFCC">
                                
                                <td class="Estilo10" align="center" width="100"><a href="pedidoDetalleBodega.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&numero=<%=((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido()%>"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getNumPedido()%></a></td>
                                <td class="Estilo10" align="center" width="100"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getProceso()%></td>
                                <td class="Estilo10" align="center" ><%=mrp.nombreUsuario(((EstructuraPorEquipo)pedidos.elementAt(i)).getUsuario())%></td>
                                <td class="Estilo10" align="center" ><%=mrp.nombreUsuario(((EstructuraPorEquipo)pedidos.elementAt(i)).getUsuAprobar())%></td>
                                <td class="Estilo10" align="center" width="100"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getDescripcionEstado()%></td>
                                <td class="Estilo10" align="center" width="100"><%=((EstructuraPorEquipo)pedidos.elementAt(i)).getFechaCreacion()%></td>
                                <td class="Estilo10" align="center" bgcolor="#009966"><input type="checkbox" name="eliminar<%=i%>"></td> 
                            </tr>
                            
                            <%}%>
                        </table>
                    </td>
                </tr>
                <tr class="Estilo10">
                    <td align="right" class="Estilo10"><input type="button" value="Eliminar" onclick="eliminar();" class="Estilo10">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
            </table>
            <br><br>
            <table cellpadding="0" cellspacing="0" class="Estilo10" align="left" width="25%">
                <tr align="right">
                    <td class="Estilo10">normal:&nbsp;</td>
                    <td bgcolor="#FF9900">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr align="right">
                    <td height="10">&nbsp;</td>
                </tr>
                <tr align="right">
                    <td class="Estilo10">urgente:&nbsp;</td>
                    <td bgcolor="#009966">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>          
            </table>      
            <input type="hidden" name="eliminated" value="0">
            <%}else{%>
            <center><h5><font class="Estilo10">No hay pedidos en curso</font></h5></center>
            <%}%>
            <%}catch(Exception e){
            new sis.logger.Loger().logger("bodega.jsp " , " bodega(): "+e.getMessage(),0);
            }%>
        </form>
    </body>
    <%}else{response.sendRedirect("../mac");}%>  
</html>