<%@page import="java.text.NumberFormat,java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.equipo.clase.Equipo,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<%
HttpSession sesion    = request.getSession();
String idSesion       = request.getParameter("idSesion");
String usuario        = request.getParameter("usuario");
String tipou          = request.getParameter("tipou");
/*String idSesion       = request.getParameter("idSesion");
String usuario        = request.getParameter("usuario");
String tipou          = request.getParameter("tipou");
sesion.setAttribute("idSesion",idSesion);
out.println(idSesion);
out.println(usuario);
out.println(tipou);*/
if(idSesion.equals(sesion.getId())){
%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Detalle por material</title>
    <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
    <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
  
</head>
<body onload="ajustarCeldas(9)">
<form name="myform" method="post" >
    <%try{
    ConexionEJB cal = new ConexionEJB();  
    Object ref =cal.conexionBean("MrpBean");
    MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
    MrpRemote mrp = mrpBeanHome.create();
    Vector detalle = mrp.detallePorMaterialVales(request.getParameter("codigo"));
    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    
    
    %>
    <br>
    <center><h5><font class="Estilo10">SISTEMA DE ADMINISTRACI&Oacute;N DE PEDIDO DE MATERIALES</font></h5></center>
<br>
<center><h4><font class="Estilo10">Detalle por material</font></h4></center>
<br>
<center><h5><font class="Estilo10">C&oacute;digo&nbsp;<%=request.getParameter("codigo")%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Descripci&oacute;n&nbsp;<%=request.getParameter("descripcion")%></font></h5></center>
    <table border=0 bgcolor=scrollbar align=center>
        <tr>
            <td>
                <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc">
                    <tr bgcolor="#B9DDFB" align="center">
                        <td align="center" class="Estilo10">NumPedido</td>
                        <td align="center" class="Estilo10">Item</td>
                        <td align="center" class="Estilo10" width="200">C&oacute;digo</td>
                        <td align="center" class="Estilo10">Descripci&oacute;n</td>
                        <td align="center" class="Estilo10">Proceso</td>
                        <td align="center" class="Estilo10">Fecha doc</td>
                        <td align="center" class="Estilo10">Cantidad</td>
                        <td align="center" class="Estilo10">Usuario</td>
                        <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                        
                    </tr>
                </table>
                <div style="overflow:auto; height:280px; padding:0">
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>     
                        <%
                        for(int i=0;i<detalle.size();i++){%>
                        
                        <tr bgcolor="#FFFFCC" onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFCC"'>
                            <td align="center" class="Estilo10"><a href="javascript:Abrir_ventana1('detallePedidoPorNumero.jsp?pedido=<%=((EstructuraPorEquipo)detalle.elementAt(i)).getNumPedido()%>')"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getNumPedido()%></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getLinea()%></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getCodigoMatPrima()%></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getDescripcion()%></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getProceso()%></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getFechaConsumo()%></td>
                            <td align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)detalle.elementAt(i)).getCantidad())%></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)detalle.elementAt(i)).getUsuario()%></td>
                        </tr>
                        <%}%>
                        <%if(detalle.size() < 15){%>
                        <%for(int i=0;i<20;i++){%>
                        <tr>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            
                        </tr>
                        <%}}%>
                    </table>    
                </div>
            
            </td>  
        </tr>
        
    </table>
    <br>
    
   
    <%}catch(Exception e){
    new sis.logger.Loger().logger("detallePorMaterialVales.jsp " , " detallePorMaterialVales() "+e.getMessage(),0);
    }%>
</form>
</body>
<%}else{response.sendRedirect("../mac");}%>  
</html>