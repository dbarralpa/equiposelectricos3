<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.crp2.bean.*,mac.ee.pla.mrp.bean.*,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
       
        <style type="text/css" media="all">
            td.uno {
            border:hidden;
            }
            table.dos {
            border: 1px solid #999999;
            }
        </style>
    </head>
    <body>
        <form name="form1" method="post">
            <%try{
            
            ConexionEJB cal = new ConexionEJB();  
            Object ref1 =cal.conexionBean("Crp2Bean");
            Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
            Crp2Remote crp = crpHome.create();
            Object ref =cal.conexionBean("MrpBean");
            MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
            MrpRemote mrp = mrpBeanHome.create();
            %>
            <br>
            <center><h5><font class="Estilo10">SISTEMA DE ADMINISTRACI&Oacute;N DE PEDIDO DE MATERIALES</h5></center>
            
           
            <br>
            <%Vector pedidosPorItem = mrp.pedidosPorItem(Long.parseLong(request.getParameter("pedido")));%>
            <%Vector equiposPorItem = crp.equiposPorItem(Long.parseLong(request.getParameter("pedido")));%>
            
            <br>
            <table width="90%" cellpadding="0" cellspacing="0" align="center">
                <tr>
                    <td>    
                        <table border=0 cellspacing=0 cellpadding=2 align="left">
                            <tr align="center">
                                <td align="center" class="Estilo10" width="80">Np</td>
                                <td align="center" class="Estilo10" width="80">Linea</td>
                                <td class="Estilo10" align="center" width="80" >Serie</td>
                                <td class="Estilo10" align="center" width="80" >Familia</td>
                                <td align="center" class="Estilo10" width="80">N� Pedido</td>
                            </tr>
                            <tr>
                                <td colspan="5" valign="top">
                                    <table  class="dos">
                                        <%for(int h=0;h<equiposPorItem.size();h++){%>
                                        <tr><!--"#d51f2c"color rojo-->
                                            <td align="center" class="Estilo10 uno" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getNp()%></td>
                                            <td align="center" class="Estilo10 uno" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getLinea()%></td>
                                            <td align="center" class="Estilo10 uno" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getSerie()%></td>
                                            <td align="center" class="Estilo10 uno" width="80"><%=((Equipo)equiposPorItem.elementAt(h)).getFamilia()%></td>
                                            <td align="center" class="Estilo10 uno" width="80"><%=request.getParameter("pedido")%></td>
                                        </tr>
                                        
                                        <%}%>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top" align="right" class="Estilo10">
                        <table cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td class="Estilo10" align="center" width="80" height="20">Usuario:</td>
                                <td class="Estilo10" align="center" width="80" height="20">&Aacute;rea:</td>
                                <td class="Estilo10" align="center" width="80" height="20">Prioridad:</td>
                                 <td class="Estilo10" align="center" width="140" height="20">Estado:</td>
                            </tr>
                            <tr>
                                <td colspan="4" valign="top">
                                    <table  class="dos">
                                        <tr>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getUsuario()%></td>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getProceso()%></td>
                                            <td class="Estilo10" align="center" width="80"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getPrioridad()%></td>
                                            <td class="Estilo10" align="center" width="140"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(0)).getDescripcionEstado()%></td>
                                            
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>    
            <br><br>
            
            <table border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc" align="center" width="90%">
                <tr bgcolor="#B9DDFB" align="center">
                    <td align="center" class="Estilo10" width="80">Codigo</td>
                    <td align="center" class="Estilo10" width="80">Descripcion</td>
                    <td align="center" class="Estilo10" width="80">Um</td>
                    <td align="center" class="Estilo10" width="80">Cantidad Pedida</td>
                    <td align="center" class="Estilo10" width="80">Stock bodega</td>
                    <td align="center" class="Estilo10" width="80">Cantidad Entregada</td>
                </tr>
                <%for(int u=0;u<pedidosPorItem.size();u++){%>
                
                <tr bgcolor="#FFFFCC" onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFCC"'>
                   
                    <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCodigoMatPrima()%></td>
                    <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getDescripcion()%></td>
                    <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getUnidadMedida()%></td>
                    <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidad()%></td>
                    <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getStockBodega()%></td>
                    <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)pedidosPorItem.elementAt(u)).getCantidadEntregada()%></td>
                    
                    
                   
                </tr>
                <%}%>
                
            </table>
            <br>
            
        </form>
    </body>
     <%}catch(Exception e){
    new sis.logger.Loger().logger("detallePedidoPorNumero.jsp " , " detallePedidoPorNumero() "+e.getMessage(),0);
    }%>

</html>