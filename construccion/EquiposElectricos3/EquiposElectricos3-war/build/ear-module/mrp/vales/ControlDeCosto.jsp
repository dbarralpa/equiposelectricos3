<%@page contentType="text/html" import="mac.contador.Contador,java.util.Date,java.text.*,conexionEJB.ConexionEJB,java.util.Vector,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.crp2.bean.*,javax.rmi.PortableRemoteObject" session="true"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
        <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
        <script language="JavaScript">
           function validar(){ 
            if(document.form1.fechaIni.value != "" || document.form1.fechaFin.value != ""){
                 if(document.form1.fechaIni.value != "" && document.form1.fechaFin.value != ""){
                 //document.form1.estado.value = 'Rango de fechas';       
                 form1.submit();
                 }else{
                 alert('Debe ingresar las dos fechas');
                 return false;
                 }
              }else{
                 alert('Debe ingresar las dos fechas');
                 return false;
              }
    }   
    function cambiar(){
    if(document.form1.fechaIni.value != "" && document.form1.fechaFin.value != ""){
        form1.familia.options[form1.familia.selectedIndex].value = 'nada'; 
        form1.submit();
    }else{
     form1.familia.options[form1.familia.selectedIndex].value = 'nada'; 
     form1.avance.options[form1.avance.selectedIndex].value = 'nada';
     alert('Debe ingresar las dos fechas');
    }
     
  }
  function cambiar1(){
    if(document.form1.fechaIni.value != "" && document.form1.fechaFin.value != ""){
        form1.avance.options[form1.avance.selectedIndex].value = 'nada';
        form1.submit();
    }else{
     form1.familia.options[form1.familia.selectedIndex].value = 'nada'; 
     form1.avance.options[form1.avance.selectedIndex].value = 'nada';
     alert('Debe ingresar las dos fechas');
    }
     
  }
        </script>     
    </head>
    <body>
        <%
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        float precio = 0f;
        float real = 0f;
        float diferencia = 0f;
        if(idSesion.equals(sesion.getId())){
        %>
        
        <%if(tipou.equals("usuAdm") || tipou.equals("adm")){%>
        <form action="/mac/jsp/sistemas/EquiposElectricosAdm.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <table width="561" border="0" cellpadding="0" cellspacing="0" align="center" class="Estilo10">
                <tr>
                    <td height="28" colspan="4" align="center" valign="middle"  class="Estilo10"><h5>Control de Costo</h5></td>
                    <td align="center"><input type="image" src="../../Images/volver2.jpg" border="0" width="89" height="18"></td>
                </tr>
            </table>       
        </form>  
        <%}if(tipou.equals("usu")){%>
        <form action="/mac/jsp/sistemas/EquiposElectricos.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <table width="561" border="0" cellpadding="0" cellspacing="0" align="center" class="Estilo10">
                <tr>
                    <td height="28" colspan="4" align="center" valign="middle"  class="Estilo10"><h5>Control de Costo</h5></td>
                    <td align="center"><input type="image" src="../../Images/volver2.jpg" border="0" width="89" height="18"></td>
                </tr>
            </table>       
        </form>  
        <%}%>
        
        <form method="post" name="form1" action="ControlDeCosto.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" >
            <%
            try{
                if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("")){
                    //Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
                }
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
                NumberFormat number = new DecimalFormat(" ");
                NumberFormat nf = NumberFormat.getInstance();
                nf.setMaximumFractionDigits(0);
                ConexionEJB cal = new ConexionEJB();
                Object ref1 =cal.conexionBean("Crp2Bean");
                Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
                Crp2Remote crp = crpHome.create();
                Vector pedidos = crp.analisisMaterialesPorPedido(request.getParameter("familia"),
                        request.getParameter("avance"),
                        request.getParameter("fechaIni"),
                        request.getParameter("fechaFin"));
            %>
            <table width="561" border="0" cellpadding="0" cellspacing="0" align="center" class="Estilo10">
                <!--tr>
                    <td height="28" colspan="4" align="center" valign="middle"  class="Estilo10"><h2>Control de Costo</h2></td>
                    <a href="bodega.jsp?idSesion=<%--=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou--%>"><img src="../../Images/volver2.jpg" border="0"></a>
                </tr-->
                <tr>
                    <td width="337" height="1"><a href="controlMaterialesPorMateriasPrimas.jsp?fechaIni=<%=request.getParameter("fechaIni")%>&fechaFin=<%=request.getParameter("fechaFin")%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">Control de costo por materias primas</a></td>
                    <td height="31" colspan="2" align="right" valign="middle">Fecha informe: </td>
                    <td width="88" align="right" valign="middle"><%=format.format(new Date())%></td>
                </tr>
                <tr>
                    <td width="126" rowspan="2" align="left" valign="middle">Fecha inicio: </td>
                    <td width="10" rowspan="2" valign="top">&nbsp;</td>
                    <td width="337" height="1"></td>
                    <td></td>
                </tr>
                <tr>
                    <td height="22" colspan="2" valign="top" class="Estilo10">
                        <DIV id="popCal" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                        <IFRAME name="popFrame" src="../../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                        <SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
                        <TABLE align="LEFT" CELLPADDING="0" CELLSPACING="0">
                            <TR>
                                <TD ALIGN="center">
                                    <input class="Estilo10" readOnly name="fechaIni" id="fechaIni" size="12" <%if(request.getParameter("fechaIni") != null && !request.getParameter("fechaIni").equals("")){%>value="<%=request.getParameter("fechaIni")%>"<%}%>>
                                    <a onclick="popFrame.fPopCalendar(fechaIni,fechaIni,popCal);return false">
                                    <img src="../../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="22" align="left" valign="top" class="Estilo10">Fecha Fin: </td>
                    <td valign="top">&nbsp;</td>
                    <td colspan="2" valign="top" class="Estilo10">
                        <DIV id="popCal1" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                        <IFRAME name="popFrame1" src="../../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                        <SCRIPT event=onclick() for=document>popCal1.style.visibility = "hidden";</SCRIPT>
                        <TABLE align="LEFT" CELLPADDING="0" CELLSPACING="0">
                            <TR>
                                <TD ALIGN="center">
                                    <input class="Estilo10" readOnly name="fechaFin" id="fechaFin" size="12" <%if(request.getParameter("fechaFin") != null && !request.getParameter("fechaFin").equals("")){%>value="<%=request.getParameter("fechaFin")%>"<%}%>>
                                    <a onclick="popFrame1.fPopCalendar(fechaFin,fechaFin,popCal1);return false">
                                    <img src="../../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                </td>
                                <!--td width="10">&nbsp;</td>
                                <td align="center"><input type="button" value="ir" name="enviar" class="Estilo10" onclick="validar();"> </td-->
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr>
                    <td height="19" colspan="4" valign="top" class="Estilo10">&nbsp;</td>
                </tr>
                <tr>
                    <td height="24" valign="top" class="Estilo10">Familia:</td>
                    <td valign="top" class="Estilo10">&nbsp;</td>
                    <td colspan="2" valign="top" class="Estilo10">
                        <select name="familia" class="Estilo10" onchange="cambiar1()">
                            <option value="nada"  <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("nada")){%>selected<%}%>>Elija familia</option>
                            <option value="AEREO" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("AEREO")){%>selected<%}%>>AEREO</option>
                            <option value="SEU" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("SEU")){%>selected<%}%>>SEU</option>
                            <option value="SECO" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("SECO")){%>selected<%}%>>SECO</option>
                            <option value="CELDA" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("CELDA")){%>selected<%}%>>CELDA</option>
                            <option value="BIFA" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("BIFA")){%>selected<%}%>>BIFA</option>
                            <option value="RAD" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("RAD")){%>selected<%}%>>RAD</option>
                            <option value="PM" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("PM")){%>selected<%}%>>PM</option>
                            <option value="NET" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("NET")){%>selected<%}%>>NET</option>
                            <option value="ECM" <%if(request.getParameter("familia") != null && request.getParameter("familia").equals("ECM")){%>selected<%}%>>ECM</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td height="25" valign="top" class="Estilo10">Estado de avance </td>
                    <td valign="top" class="Estilo10">&nbsp;</td>
                    <td colspan="2" valign="top" class="Estilo10">
                        <select name="avance" class="Estilo10" onchange="cambiar()">
                            <option value="nada"  <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("nada")){%>selected<%}%>>Elija estado de avance</option>
                            <option value="Envase" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Envase")){%>selected<%}%>>Envase</option>
                            <option value="Reproceso" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Reproceso")){%>selected<%}%>>Reproceso</option>
                            <option value="APA" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("APA")){%>selected<%}%>>APA</option>
                            <option value="Bodega" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Bodega")){%>selected<%}%>>Bodega</option>
                            <option value="RevisionFinal" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("RevisionFinal")){%>selected<%}%>>RevisionFinal</option>
                            <option value="Terminaciones" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Terminaciones")){%>selected<%}%>>Terminaciones</option>
                            <option value="Bob/Nuc" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Bob/Nuc")){%>selected<%}%>>Bob/Nuc</option>
                            <option value="Montaje" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Montaje")){%>selected<%}%>>Montaje</option>
                            <option value="LAB TX" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("LAB TX")){%>selected<%}%>>LAB TX</option>
                            <option value="Detenido" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Detenido")){%>selected<%}%>>Detenido</option>
                            <option value="Horno" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Horno")){%>selected<%}%>>Horno</option>
                            <option value="Sin Avance" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("Sin Avance")){%>selected<%}%>>Sin Avance</option>
                            <option value="LAB TC" <%if(request.getParameter("avance") != null && request.getParameter("avance").equals("LAB TC")){%>selected<%}%>>LAB TC</option>                            
                        </select>
                        
                    </td>
                </tr>
                <tr>
                    <td height="19" colspan="4" valign="top" class="Estilo10">&nbsp;</td>
                </tr>
                <tr>
                    <td height="221" colspan="4" valign="top" class="Estilo10">
                        <!--table width="561" border="1" cellpadding="0" cellspacing="0" align="center">
                            <tr>
                                <td width="145" rowspan="2" valign="middle" align="center" class="Estilo10">Nota de pedido </td>
                                <td width="140" rowspan="2" valign="middle" align="center" class="Estilo10">Estado avance </td>
                                <td height="21" colspan="3" valign="middle" align="center" class="Estilo10">Monto $</td>
                            </tr>
                            <tr>
                                <td width="90" height="28" valign="middle" align="center" class="Estilo10">Ppto.</td>
                                <td width="89" valign="middle" align="center" class="Estilo10">Real</td>
                                <td width="85" valign="middle" align="center" class="Estilo10">Diferencia</td>
                            </tr>
                            <%--for(int i=0;i<pedidos.size();i++){%>
                            <tr>
                                <td valign="top"><a href="controlMaterialesPorPedido.jsp?pedido=<%=((Equipo)pedidos.elementAt(i)).getNp()%>&estructura<%=((Equipo)pedidos.elementAt(i)).getCodPro()%>"><%=((Equipo)pedidos.elementAt(i)).getNp()%></a></td>
                                <td valign="top"><%=((Equipo)pedidos.elementAt(i)).getStatus()%></td>
                                <td valign="top">&nbsp;</td>
                                <td valign="top">&nbsp;</td>
                                <td valign="top">&nbsp;</td>
                            </tr>
                            <%}--%>
                            <tr>
                                <td height="23" valign="top">&nbsp;</td>
                                <td valign="middle" align="right" class="Estilo10">Totales:</td>
                                <td valign="top">&nbsp;</td>
                                <td valign="top">&nbsp;</td>
                                <td valign="top">&nbsp;</td>
                            </tr>
                        </table--> 
                        
                        
                        <table width="561" border="1" cellpadding="0" cellspacing="0" align="center">
                            
                            <tr>
                                <td width="96" rowspan="2" align="center" valign="middle">Nota de pedido </td>
                                <td width="47" rowspan="2" valign="middle" align="center">Item</td>
                                <td width="140" rowspan="2" valign="middle" align="center">Estado avance </td>
                                <td height="21" colspan="3" align="center" valign="middle">Monto $</td>
                            </tr>
                            <tr>
                                <td width="90" height="28" align="center" valign="middle">Ppto.</td>
                                <td width="89" valign="middle" align="center">Real</td>
                                <td width="85" valign="middle" align="center">Diferencia</td>
                            </tr>
                            
                            <%for(int i=0;i<pedidos.size();i++){%>
                            <tr>
                                
                                <td valign="middle" align="center"><a href="javascript:Abrir_ventana11('controlMaterialesPorPedido.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&pedido=<%=((Equipo)pedidos.elementAt(i)).getNp()%>&linea=<%=((Equipo)pedidos.elementAt(i)).getLinea()%>&estructura=<%=((Equipo)pedidos.elementAt(i)).getCodPro()%>&familia=<%=((Equipo)pedidos.elementAt(i)).getFamilia()%>&status=<%=((Equipo)pedidos.elementAt(i)).getStatus()%>&cliente=<%=((Equipo)pedidos.elementAt(i)).getNombreCliente()%>')"><%=((Equipo)pedidos.elementAt(i)).getNp()%></a></td>
                                <td valign="middle" align="center"><%=((Equipo)pedidos.elementAt(i)).getLinea()%></td>
                                <td valign="middle" align="center"><%=((Equipo)pedidos.elementAt(i)).getStatus()%></td>
                                <td valign="middle" align="center"><%=nf.format(((Equipo)pedidos.elementAt(i)).getPrecioMateriales())%></td>
                                <td valign="middle" align="center"><%=nf.format(((Equipo)pedidos.elementAt(i)).getPrecioReal())%></td>
                                <td valign="middle" align="center"><%=nf.format(((Equipo)pedidos.elementAt(i)).getDiferencia())%></td>
                            </tr>
                            <%
                            precio += ((Equipo)pedidos.elementAt(i)).getPrecioMateriales();
                            real += ((Equipo)pedidos.elementAt(i)).getPrecioReal();
                            diferencia += ((Equipo)pedidos.elementAt(i)).getDiferencia();
                            }%>                      
                            <tr>
                                <td height="23" colspan="2" valign="top">&nbsp;</td>
                                <td valign="middle" align="right">Totales:</td>
                                <td valign="middle" align="center"><%=nf.format(precio)%></td>
                                <td valign="middle" align="center"><%=nf.format(real)%></td>
                                <td valign="middle" align="center"><%=nf.format(diferencia)%></td>
                            </tr>
                        </table>  
                    </td>
                </tr>
            </table>
        </form>
        <%}catch(Exception e){
        new sis.logger.Loger().logger("ControlDeCosto.jsp " , " ControlDeCosto(): "+e.getMessage(),0);
        }%>
        <%}else{response.sendRedirect("../mac");}%>  
    </body>
</html>
