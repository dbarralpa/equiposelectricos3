<%@page import="java.text.NumberFormat,java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,mac.ee.pla.equipo.clase.Equipo,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<%
HttpSession sesion    = request.getSession();
String idSesion       = request.getParameter("idSesion");
String usuario        = request.getParameter("usuario");
String tipou          = request.getParameter("tipou");
/*String idSesion       = request.getParameter("idSesion");
String usuario        = request.getParameter("usuario");
String tipou          = request.getParameter("tipou");
sesion.setAttribute("idSesion",idSesion);
out.println(idSesion);
out.println(usuario);
out.println(tipou);*/
if(idSesion.equals(sesion.getId())){
%>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Ver saldo a la cuenta</title>
    <link REL="stylesheet" TYPE="text/css" HREF="../../CSS/estiloPaginas.css"/>
    <script language="JavaScript" TYPE="text/javascript" src ="../../js/javascript.js"> </script>
  
</head>
<body onload="ajustarCeldas(4)">
<form name="myform" method="post" >
    <%try{
    ConexionEJB cal = new ConexionEJB();  
    Object ref =cal.conexionBean("MrpBean");
    MrpRemoteHome mrpBeanHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref, MrpRemoteHome.class);
    MrpRemote mrp = mrpBeanHome.create();
    Vector historial = mrp.historialVales(usuario);
    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    
    
    %>
    <br>
    <center><h5><font class="Estilo10">SISTEMA DE ADMINISTRACI&Oacute;N DE PEDIDO DE MATERIALES</font></h5></center>
<br><br>
<center><h5><font class="Estilo10">Saldo a mi cuenta</font></h5></center>
<br>
    <table border=0 bgcolor=scrollbar align=center>
        <tr>
            <td>
                <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor="#cccccc">
                    <tr bgcolor="#B9DDFB" align="center">
                        <td align="center" class="Estilo10" width="200">C&oacute;digo</td>
                        <td align="center" class="Estilo10">Descripci&oacute;n</td>
                        <td align="center" class="Estilo10">Cantidad</td>
                        <td align="center" class="Estilo10">&nbsp;&nbsp;&nbsp;</td>
                        
                    </tr>
                </table>
                <div style="overflow:auto; height:280px; padding:0">
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>     
                        <%
                        for(int i=0;i<historial.size();i++){%>
                        
                        <tr bgcolor="#FFFFCC" onMouseOver='this.style.background="#F9B908"' onmouseout='this.style.background="#FFFFCC"'><!--"#d51f2c"color rojo-->
                            <td align="center" class="Estilo10" width="200"><a href="javascript:Abrir_ventana1('detallePorMaterialVales.jsp?codigo=<%=((EstructuraPorEquipo)historial.elementAt(i)).getCodigoMatPrima()%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&descripcion=<%=((EstructuraPorEquipo)historial.elementAt(i)).getDescripcion()%>')"><%=((EstructuraPorEquipo)historial.elementAt(i)).getCodigoMatPrima()%></a></td>
                            <td align="center" class="Estilo10"><%=((EstructuraPorEquipo)historial.elementAt(i)).getDescripcion()%></td>
                            <td align="center" class="Estilo10"><%=nf.format(((EstructuraPorEquipo)historial.elementAt(i)).getCantidad())%></td>
                        </tr>
                        <%}%>
                        <%if(historial.size() < 15){%>
                        <%for(int i=0;i<20;i++){%>
                        <tr onMouseOver='this.style.background="#EDF0F8"' onmouseout='this.style.background="#FFFFCC"'>
                            <td align="center" class="Estilo10" width="200" >&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            <td align="center" class="Estilo10">&nbsp;</td>
                            
                        </tr>
                        <%}}%>
                    </table>    
                </div>
                <!--tr>
                    <table border=0 cellspacing=0 cellpadding=2  bgcolor="#FFFFFF" align="center" width="688">
                        <tr>
                            <td height="50" width="580" style="border-width: 0px; border-right-color:#DDDDDD">&nbsp;</td>
                            
                            
                            <td style="border-width: 0px; border-right-color:#FFFFFF" align="right"><input type="image" onmouseover="this.src = '../../Images/boton.jpg'" onmouseout="this.src = '../../Images/boton1.jpg'"  name="agruparmarcados" src="../../Images/boton1.jpg" width="85" height="50" alt="Agrupar marcados" border="0"/></td>
                            <td style="border-width: 0px; border-right-color:#FFFFFF" align="right"><input type="image" name="eliminarMarcados" src="../../Images/boton2.jpg" onmouseover="this.src = '../../Images/boton3.jpg'" onmouseout="this.src = '../../Images/boton2.jpg'" width="85" height="50" alt="Eliminar marcados" border="0"/></td>
                            <td style="border-width: 0px; border-right-color:#FFFFFF" align="right">&nbsp;</td>
                        </tr>
                    </table>     
                </tr-->
            </td>  
        </tr>
        
    </table>
    <br>
    
   
    <%}catch(Exception e){
    new sis.logger.Loger().logger("historialVales.jsp " , " historialVales() "+e.getMessage(),0);
    }%>
</form>
</body>
<%}else{response.sendRedirect("../mac");}%>  
</html>
