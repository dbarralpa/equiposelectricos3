<%@page contentType="text/html" import="forms.XlsImport"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Subir Excel stock de planta</title>
    </head>
    <body>
        <% HttpSession sesion    = request.getSession();
        /*String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");*/
        String idSesion = sesion.getId();
        String tipou="usu";
        String usuario="dbarra";
        if(idSesion.equals(sesion.getId())){
           if(request.getParameter("todos") == null){ 
           sesion.setAttribute("user",usuario); 
           }
            %>
        
        <form action="../XlsImport?subir=<%=request.getParameter("subir")%>&idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">
            <%if(request.getParameter("devolver") != null){%>
            <script>
               alert("<%=request.getParameter("devolver")%>");
            </script>
            <%}%>
            <!--input name="subir" type="file" />
            <input name="enviar" type="submit" />
            Si desea que los dem&aacute;s valores queden en cero clic en esta casilla<input type="checkbox" name="todos" value="si"-->
            <table width="721" border="0" cellpadding="0" cellspacing="0" align="center" class="Estilo10">
                <!--DWLayoutTable-->
                <tr>
                    <td height="60" colspan="5" align="center" valign="middle">ACTUALIZAR STOCK DE PLANTA</td>
                </tr>
                <tr>
                    <td height="65" colspan="2" align="right" valign="middle">Cargar Excel: </td>
                    <td width="43" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
                    <td colspan="2" align="left" valign="middle">
                        
                    <input type="file" name="subir" />   </td>
                </tr>
                
                <tr>
                    <td width="139" height="86" valign="top"><!--DWLayoutEmptyCell-->&nbsp;</td>
                    <td colspan="3" valign="top">Si desea que los dem&aacute;s valores queden en cero clic en esta casilla</td>
                    <td width="320" valign="top">
                    <input type="checkbox" name="todos" value="si" />    </td>
                </tr>
                <tr>
                    <td height="67" colspan="5" align="center" valign="middle">
                    <input type="submit" name="enviar" value="Enviar solicitud"/>    </td>
                </tr>
                <tr>
                    <td height="1"></td>
                    <td width="89"></td>
                    <td></td>
                    <td width="130"></td>
                    <td></td>
                </tr>
            </table>
        </form>
        <%}%>
    </body>
</html>
