<%@page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.clase.EstructuraPorEquipo,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <title>Manufactura asistida por computadora</title>
        <SCRIPT LANGUAGE="Javascript">

            var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return (key >= 48 && key <= 57);

            }
            
            function validarCantidad(){
            if(document.form1.stockPlanta.value == ""){
               alert('Debe agregar stock planta');
               return false;
            }else{
             return true;
            }
          }

        </SCRIPT>      
        
    </head>
    <body>
        <%  
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        if(idSesion.equals(sesion.getId())){
        %>
        <form action="setearStockPlanta.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
            <%              
            try{
                MrpBean mrp = new MrpBean();
                if(request.getParameter("guardar") == null){
                    sesion.setAttribute("cod",request.getParameter("codpro"));
                }
                Vector bitacora = mrp.bitacoraStockPlanta((String)sesion.getAttribute("cod"));
            %>
            
            <br>
            <%
            if(request.getParameter("guardar") != null){
                    byte verificador = mrp.updateStockPlanta(Float.parseFloat(request.getParameter("stockPlanta")),(String)sesion.getAttribute("cod"),usuario,request.getParameter("observacion"));
                    if(verificador == 1){
            %>
            <center><h5><font class="Estilo10">Grab&oacute; los datos en la base de datos</font></h5></center>
            <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar</a></center>
            <%}else{%>
            <center><h5><font class="Estilo10">No grabó los datos en la base de datos</font></h5></center>
            <%}}else{%>
            <center><h5><font class="Estilo10">Cambiar stock de planta</font></h5></center>
            <br>
            <table border=1 cellspacing=0 cellpadding=0 bgcolor=#cccccc align="center">
                <tr bgcolor="#B9DDFB">
                    <td class="Estilo10" colspan="2" align="center">Cantidad a cambiar</td>
                </tr>
                <tr>
                    <td><font class="Estilo9">Material</font></td>
                    <td><font class="Estilo9">Stock Planta</font></td>
                </tr>
                <tr>
                    <td><font class="Estilo9"><%=sesion.getAttribute("cod")%></font></td>
                    <td><font class="Estilo9"><input name="stockPlanta" type="text" onKeyPress="return acceptNum(event)"></font></td>
                </tr>
                
            </table>
            <br>
            <table align="center" width="100%">
                <tr>
                    <td>
                        <table width="90%" class="Estilo10" align="left">
                            
                            <tr>
                                <td align="left">Usuario</td>
                                <td align="left">Fecha</td>
                                <td align="left">Observaci&oacute;n</td>
                            </tr>
                            <tr>
                                <td colspan="3" height="15">&nbsp;</td>
                            </tr>
                            
                            <%for(int i=0;i<bitacora.size();i++){%>
                            <tr>
                                <td align="left" width="100"><%=((EstructuraPorEquipo)bitacora.elementAt(i)).getUsuario()%></td>
                                <td align="left" width="100"><%=((EstructuraPorEquipo)bitacora.elementAt(i)).getFechaCreacion()%></td>
                                <td align="left" width="150"><%=((EstructuraPorEquipo)bitacora.elementAt(i)).getObservacion()%></td>
                            </tr>
                            <%}%>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td class="Estilo10">Observaci&oacute;n:&nbsp;<textarea name="observacion" id="observacion" cols="60" rows="3"></textarea></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <center><input type="submit" value="Guardar" name="guardar" class="Estilo10" onclick="return validarCantidad()"></center>
        </form><br>
        <%}%>
        
        <%}catch(Exception e){e.printStackTrace();}%>
        <%}%>
    </body>
</html>