<%@page contentType="text/html" import="java.text.*,mac.contador.Contador,mac.ee.pla.crp2.bean.*,mac.ee.pla.equipo.clase.Equipo,java.util.*,ordenamiento.Ordenamiento" session="true"%>
<%@page pageEncoding="iso-8859-1"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Validar orden de compra</title>
        <link href="../CSS/estiloPaginas.css" rel="stylesheet" type="text/css" />
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/tablas.css"/>
        <script language="javascript" type="text/javascript" src="../js/javascript.js"></script>
        <script type="text/javascript" src="../js/overlib.js"></script>
        <script>
            function actualiza(valor){
            if(confirm('�Seguro que desea proseguir con la operaci�n?')){
               form1.submit();  
            }else{
              valor.checked = false;
            }
          }
          
          function informe(){
            if(confirm('�Seguro que desea proseguir con la operaci�n?')){
            form1.submit();
            }else{
              return false;
            }
          }
           
      
        </script>
    </head>
    <body>
        <%
        
        HttpSession sesion    = request.getSession();
        String idSesion       = request.getParameter("idSesion");
        String usuario        = request.getParameter("usuario");
        String tipou          = request.getParameter("tipou");
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        if(idSesion.equals(sesion.getId())){%>
        <form  name="form1" method="post" action="equiposProduccion.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">
        <%try{  
        if(request.getParameter("counter") != null && usuario != null && !usuario.trim().equals("")){
          // Contador.insertarContador(Integer.parseInt(request.getParameter("counter")),usuario);
        }    
        Crp2Bean crp = new Crp2Bean();
        
        
        if(request.getParameter("para1") == null){
        sesion.setAttribute("teams",crp.equiposProduccion());
        }
        if(request.getParameter("ordenar") != null && !request.getParameter("ordenar").equals("0")){
        Collections.sort((List)sesion.getAttribute("teams"),new Ordenamiento(Byte.parseByte(request.getParameter("ordenar")),(byte)2));    
        }
        %>
        <center><h4><font class="Estilo10">EQUIPOS EN PRODUCCION</font></h4></center>
        <center class="Estilo10"><h6>Cantidad de equipos&nbsp;&nbsp;&nbsp;<%=((Vector)sesion.getAttribute("teams")).size()%></h6>
            &nbsp;&nbsp;<a href="/mac/jsp/dependencias/Produccion.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a>
        </center>
        <center>
            <select name="ordenar" class="Estilo10" onchange="form1.submit();"> 
                <option value="0">Ordenar por</option>    
                <option value="1">Fecha Cliente</option>   
                <option value="2">Expeditaci&oacute;n</option>  
                <option value="3">Cliente</option>    
                <option value="4">Estado</option>  
            </select>
        </center>
        &nbsp;&nbsp;
        
        
        
        
        <table width="1045" class="tableone">
            <thead>
                <tr>
                    <th width="55" height="22" align="center" valign="top">Pedido</th>
                    <th width="30" align="center" valign="top">Lin</th>
                    <th width="300" align="center" valign="top">Descripci&oacute;n</th>
                    <th width="228" align="center" valign="top">Cliente</th>
                    <th width="107" align="center" valign="top">Fecha Cliente </th>
                    <th width="107" align="center" valign="top">Expeditaci&oacute;n</th>
                    <th width="85" align="center" valign="top">Estado</th>
                    <th width="100" align="center" valign="top">Monto</th>
                    <th width="53" align="center" valign="top">Obs.</th>
                </tr>
            </thead>
            <tbody>  
                <tr>
                    <td colspan="9">
                        <div class="innerb">
                            <table class="tabletwo" width="1045">
                                <%                            
                                for(int i=0;i<((List)sesion.getAttribute("teams")).size();i++){
            String MuestraEquipoDetenido = "N";
            if(((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getStandBy() == 1){
                MuestraEquipoDetenido  = "S";
            }
                                %>  
                                <tr class="dk">
                                    
                                    <td width="55"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getNp()%></td>
                                    <td width="30"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getLinea()%></td>
                                    <td width="300"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getDescripcion()%></td>
                                    <td width="228"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getCliente()%></td>
                                    <td width="107" align="center"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getFechaCliente()%></td>
                                    <td width="107" align="center"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getExpeditacion()%></td>
                                    <td width="85"><%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getStatus()%></td>
                                    <td width="100"><%=formateador.format(((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getMonto())%></td>
                                    <td width="23"><A href="javascript:Abrir_ventana2('/mac.ee.pro.produccion-war/StatusProduccionCliente/EditarObservacion.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&status=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getStatus()%>&nomCli=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getCliente()%>&Pedido=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getNp()%>&Linea=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getLinea()%>&fechaCliente=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getFechaCliente()%>&MuestraEquipoDetenido=<%=MuestraEquipoDetenido%>&Serie=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getSerie()%>&Modelo=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getModelo()%>&Diseno=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getDiseno()%>&Monto=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getMonto()%>&Potencia=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getKva()%>&Descripcion=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getDescripcion()%>&Expedita=<%=((Equipo)(((List)sesion.getAttribute("teams")).get(i))).getExpeditacion()%>','NoConforme')" style="TEXT-DECORATION: none"><img src="../Images/observacionVacia.jpg" border="0" width="16" height="15" alt="observacionVacia"/></a></td>
                                    
                                </tr>
                                
                                <%}%>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <input type="hidden" name="para1" value="q">
        
        <br><br>
        <table align="center">
            <tr>
                <!--td valign="middle"><input type="button" value="Enviar a pdf" name="enviarPdf" class="Estilo10" onclick="javascript:Abrir_ventana2('jasperReport1.jsp')"></td-->
                <td width="100">&nbsp;</td>
                <td>Exportar a Excel&nbsp;<a href="../EquiposProduccion"><img src="../Images/logo_excel.gif" width="30" height="30" alt="logo_excel" border="0"/></a></td>
            </tr>
        </table>
        <%if(request.getParameter("enviarPdf") != null){%>
        <script>
                          this.location.href="javascript:Abrir_ventana2('jasperReport1.jsp')";
        </script>
        <%}%>
        <%}catch(Exception e){out.println(e.getMessage());
        }%>
        
        <%}else{response.sendRedirect("../mac");}%>
        
    </body>
</html>