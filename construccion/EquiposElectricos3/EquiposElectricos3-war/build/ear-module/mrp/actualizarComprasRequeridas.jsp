<%@ page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.mrp.bean.*,mac.ee.pla.estructuraEquipo.bean.*,conexionEJB.ConexionEJB,sis.logger.Loger" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"></script>
        <SCRIPT LANGUAGE="Javascript">

            var nav4 = window.Event ? true : false;

            function acceptNum(evt){ 

            var key = nav4 ? evt.which : evt.keyCode; 

            return (key >= 48 && key <= 57);

            }

         </SCRIPT>   


    </head>
    <body>

    
    <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               /*String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               sesion.setAttribute("idSesion",idSesion);
               out.println(idSesion);
               out.println(usuario);
               out.println(tipou);*/
               if(idSesion.equals(sesion.getId())){%>
               <form action="actualizarComprasRequeridas.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post" name="form1">
                   <%try{
                   java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");    
                   byte estado = 0;
                   ConexionEJB cal = new ConexionEJB();
                   Object ref1 =cal.conexionBean("MrpBean");
                   MrpRemoteHome mrpHome = (MrpRemoteHome) PortableRemoteObject.narrow(ref1, MrpRemoteHome.class);
                   MrpRemote mrp = mrpHome.create();   
                   String modificar = "aaa";
                   int veri = 0;
                   int cants = 0;
                   int fecs = 0;
                   %>
                  
                   <br>
                   <center><h4><font class="Estilo10">ACTUALIZAR COMPRAS REQUERIDAS</font></h4></center>
                   <br>
                   <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                       <tr class="Estilo10">
                           <td><a href="materialesProyectados.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></td>
                   </tr>
                  </table>
                   <br>
                   <center><h4><font class="Estilo10">Seleccione material para ingresar a compras&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <a href="actualizarComprasRequeridas1.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>">Modificar compras</a></font></h4></center>
                   <br>
                   <%
                    if(request.getParameter("actualizar") != null && request.getParameter("cantidad") != null && request.getParameter("fecha") != null){
                       if(request.getParameter("cantidad").equals("")){%>
                          <center><h5><font class="Estilo10">Debe ingresar una cantidad</font></h5></center> 
                        <%}else if(request.getParameter("fecha").equals("")){%>
                             <center><h5><font class="Estilo10">Debe ingresar una fecha</font></h5></center>  
                        <%}else if(request.getParameter("fecha").equals("") && request.getParameter("cantidad").equals("")){%>
                           <center><h5><font class="Estilo10">Debe ingresar una fecha y una cantidad</font></h5></center>  
                        <%}else{
                        estado = mrp.actualizarComprasRequeridas((String)sesion.getAttribute("material"),(String)sesion.getAttribute("descripcion"),Float.valueOf(request.getParameter("cantidad")).floatValue(),(String)sesion.getAttribute("uni"),(String)sesion.getAttribute("comprador"),((Short)sesion.getAttribute("leadTime")).shortValue(),formate.format(new java.util.Date()),((Float)sesion.getAttribute("stockMin")).floatValue(),((Float)sesion.getAttribute("stockMax")).floatValue(),formate.format(new java.sql.Date(0).valueOf(request.getParameter("fecha"))),mrp.getIncrementar());
                        
                            if(estado == (byte)1){
                               
                            %>
                               <center><h5><font class="Estilo10">Pedido Actualizado</font></h5></center>
                            <%}else{%>
                               <center><h5><font class="Estilo10">No se pudo Actualizar el pedido</font></h5></center>
                   <%}}
                    //mrp.setIncrementar(3,0);
                    }
                    
                 
                   Vector compras = mrp.retornarComprasRequeridas(0,"Codigo");
                   %>
                   
                   
                  <table cellpadding="0" cellspacing="0" class="Estilo10" align="center">
                       <tr class="Estilo10">
                         <td width="150">
                             <select name="materiales" onchange="form1.submit()" class="Estilo10">
                                   <option value="no">Elija un Material</option>
                                    <%for(int u=0;u<compras.size();u++){%>
                                           <option VALUE="<%=(String)((Vector)compras.elementAt(u)).elementAt(0)%>" STYLE="font-size: 1px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #330066;"><%=(String)((Vector)compras.elementAt(u)).elementAt(0)%></option>  
                                    <%}%>
                           </select>        
                       </td>
                       <td><input type="submit" value="Actualizar" class="Estilo10" name="actualizar"></td>
                   </tr>
                  </table>
                   <br>
                  
                   <%
                   if(request.getParameter("materiales") != null && !request.getParameter("materiales").equals("no")){
                          modificar = "si";  
                    }
                   if(request.getParameter("materiales") != null){
                   if(modificar.equals("si") ){
                     mrp.setIncrementar(1);  
                   }         
                   
                   for(int u=0;u<compras.size();u++){
                   if(request.getParameter("materiales").equals((String)((Vector)compras.elementAt(u)).elementAt(0))){
                      sesion.setAttribute("material",request.getParameter("materiales"));
                      sesion.setAttribute("descripcion",(String)((Vector)compras.elementAt(u)).elementAt(1)); 
                      sesion.setAttribute("uni",(String)((Vector)compras.elementAt(u)).elementAt(2)); 
                      sesion.setAttribute("leadTime",(Short)((Vector)compras.elementAt(u)).elementAt(4));
                      sesion.setAttribute("stockMin",(Float)((Vector)compras.elementAt(u)).elementAt(5));
                      sesion.setAttribute("stockMax",(Float)((Vector)compras.elementAt(u)).elementAt(6));
                      sesion.setAttribute("comprador",(String)((Vector)compras.elementAt(u)).elementAt(3)); 
                       %>
                    <table  border="1" align="center" cellspacing=0 cellpadding=2 bgcolor=#cccccc>
                       <tr bgcolor="#B9DDFB">
                           <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                           <td align="center"><font class="Estilo9">CODIGO</font></td>
                           <td align="center"><font class="Estilo9">DESCRIPCION</font></td>
                           <td align="center"><font class="Estilo9">CANTIDAD</font></td>
                           <td align="center"><font class="Estilo9" title="Fecha de entrega del material">FECHA</font></td>
                           <td align="center"><font class="Estilo9" title="Checkear si desea agregar otra cantidad al mismo material">CH</font></td>
                       </tr>
                   <tr class="Estilo10">
                   <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(0)%></td>
                   <td align="center"><%=((Vector)compras.elementAt(u)).elementAt(1)%></td>
                   <td align="center"><input type="text" name="cantidad" class="Estilo10" onKeyPress="return acceptNum(event)"></td>
                   <td><DIV id="popCal" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                   <IFRAME name="popFrame" src="popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                                   <SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
                                   <TABLE align="center" CELLPADDING="0" CELLSPACING="0">
                                       <TR>
                                           <TD ALIGN="right">
                                               <input class="Estilo10" readOnly name=fecha size="12">
                                               <a onclick="popFrame.fPopCalendar(fecha,fecha,popCal);return false">
                                               <img src="../Images/calendar.gif" width="16" height="16" alt="calendario"/></a>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                   <td align="center"><input type=checkbox name="T" class="Estilo10"></td>
                   </tr>
                       <%}%>
                   <%}}%>
                   <%if(request.getParameter("T") != null){%>
                    <table  border="1" align="center" cellspacing=0 cellpadding=2 bgcolor=#cccccc>
                       <tr bgcolor="#B9DDFB">
                           <!--td align="center"><font class="Estilo9">ESTADO</font></td-->
                           <td align="center"><font class="Estilo9">CODIGO</font></td>
                           <td align="center"><font class="Estilo9">DESCRIPCION</font></td>
                           <td align="center"><font class="Estilo9">CANTIDAD</font></td>
                           <td align="center"><font class="Estilo9" title="Fecha de entrega del material">FECHA</font></td>
                           <td align="center"><font class="Estilo9" title="Checkear si desea agregar otra cantidad al mismo material">CH</font></td>   
                   </tr>
                   <tr class="Estilo10">
                   <td align="center"><%=(String)sesion.getAttribute("material")%></td>
                   <td align="center"><%=(String)sesion.getAttribute("descripcion")%></td>
                   <td align="center"><input type="text" name="cantidad" class="Estilo10" onKeyPress="return acceptNum(event)"></td>
                   <td><DIV id="popCal" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                                   <IFRAME name="popFrame" src="popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                                   <SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
                                   <TABLE align="center" CELLPADDING="0" CELLSPACING="0">
                                       <TR>
                                           <TD ALIGN="right">
                                               <input class="Estilo10" readOnly name=fecha size="12">
                                               <a onclick="popFrame.fPopCalendar(fecha,fecha,popCal);return false">
                                               <img src="../Images/calendar.gif" width="16" height="16" alt="calendario"/></a>
                                           </td>
                                       </tr>
                                   </table>
                               </td>
                   <td align="center"><input type=checkbox name="T" class="Estilo10"></td>
                   </tr>
                   <%}%>
                                     
                    </table>
                   
                    <%if(request.getParameter("T") != null){
                    if(request.getParameter("materiales").equals("no")){
                     mrp.setIncrementar(3);  
                   }   
                 }%>
                 <br>
                      <%}catch(Exception e){
                   }        
                   %>
               </form>
       <%}else{response.sendRedirect("../mac");}%>
    </body>
</html>
