<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.crp2.bean.*,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
    <%try{
                ConexionEJB cal = new ConexionEJB();  
                Object ref1 =cal.conexionBean("Crp2Bean");
                Crp2RemoteHome mrpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
                Crp2Remote crp = mrpHome.create();        
               
        %>
        <br><br>
        <center><h5><font class="Estilo10">EQUIPOS CON PROBLEMAS DE ESTRUCTURA</font></h5></center>
        <br>
      <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
            <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
                <td align="center"><font class="Estilo9">Np</font></td>
                <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                <td align="center"><font class="Estilo9">Kva</font></td>
                <td align="center"><font class="Estilo9">Familia</font></td>
            </tr>
            
           <%
            Vector equipos = new Vector(); 
                equipos    = crp.equiposConProblemas();
            for(int u=0;u<equipos.size();u++){
               %>
               
            <tr><!--"#d51f2c"color rojo-->
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getNp()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getKva()%></font></td>
                <td align="center"><font class="Estilo9"><%=((mac.ee.pla.equipo.clase.Equipo)equipos.elementAt(u)).getFamilia()%></font></td>
            </tr>
            <%}%>
            
        </table>

    <%}catch(Exception e){
        out.println("ERROR: equiposTroubles.jsp " + e.getMessage());
        }%>
    </body>
</html>