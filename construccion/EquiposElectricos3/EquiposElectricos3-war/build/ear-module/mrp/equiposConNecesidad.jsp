<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pla.crp2.bean.*,ordenamiento.Ordenamiento,mac.ee.pla.equipo.clase.Equipo,mac.ee.pla.ordenCompra.bean.*,conexionEJB.ConexionEJB"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
        <%try{
        ConexionEJB cal = new ConexionEJB();  
        Object ref1 =cal.conexionBean("Crp2Bean");
        Crp2RemoteHome crpHome = (Crp2RemoteHome) PortableRemoteObject.narrow(ref1, Crp2RemoteHome.class);
        Crp2Remote crp = crpHome.create();
        Object ref =cal.conexionBean("OrdenBean");
        OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
        OrdenRemote orden = ordenHome.create();    
        java.text.NumberFormat number = new java.text.DecimalFormat("#########.####");
        java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
        
        %>
        <br><br>
        <center><h5><font class="Estilo10">EQUIPOS CON NECESIDAD</font></h5></center>
        <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
            <tr>
                <td><img src="../Images/lineaVerde.GIF" border="0"/></td>
                <td class="Estilo10">&nbsp;&nbsp;Equipos cubiertos por stock</td>
            </tr>
            <tr>
                <td><img src="../Images/lineaVerdeOscura.GIF" border="0"/></td>
                <td class="Estilo10">&nbsp;&nbsp;Equipos cubiertos por &Oacute;rdenes de compra</td>
            </tr>
            <!--tr>
                <td bgcolor="#FF0000">&nbsp;</td>
                <td class="Estilo10">&nbsp;&nbsp;Equipo en reproceso</td>
            </tr>
            <tr>
                <td bgcolor="#FFFF00">&nbsp;</td>
                <td class="Estilo10">&nbsp;&nbsp;Hidropack</td>
            </tr-->
        </table>
        <br>
        <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
            
            <tr>
                <td colspan="10" align="center" class="Estilo10">Material&nbsp;&nbsp;<%=request.getParameter("codpro").substring(0,5)%>&nbsp;-&nbsp;<%=request.getParameter("codpro").substring(5,10)%>&nbsp;&nbsp;<%=request.getParameter("descri")%></td>
            </tr>  
            <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
          
                <td align="center"><font class="Estilo9">Expeditaci&oacute;n</font></td>
                <td align="center"><font class="Estilo9">Fecha Necesidad</font></td>
                <td align="center"><font class="Estilo9">Cliente</font></td>
                <td align="center"><font class="Estilo9">Np</font></td>
                <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                <td align="center"><font class="Estilo9">Cantidad</font></td>
                <td align="center"><font class="Estilo9">Planta</font></td>
                <td align="center"><font class="Estilo9">Proceso</font></td>
                <td align="center"><font class="Estilo9">Decripcion</font></td>
                <td align="center"><font class="Estilo9">Diseño</font></td>
                <td align="center"><font class="Estilo9">Serie</font></td>
                
            </tr>
            
            <%
            Vector equipos = new Vector();
            Vector ordenesCompra = new Vector();
            crp.retornarMapaAsBuildConMateriales().clear();
            equipos       = crp.mapaAsBuildConEstructura1(request.getParameter("codpro"),"","",0);
            float stock =  Float.parseFloat(request.getParameter("stock"));
            float ordenFutura = orden.ordenDeCompraPendientes(request.getParameter("codpro"));
            Collections.sort((List)equipos,new Ordenamiento((byte)2,(byte)2));   
            for(int u=0;u<equipos.size();u++){
                Equipo equi = (Equipo)equipos.get(u);
                //stock -= ((Float)((Vector)equipos.elementAt(u)).elementAt(4)).floatValue();
                stock -= equi.getCantidad();%>
            <%if(stock >= 0){%>   
            <tr bgcolor="#CCFF00"><!--"#d51f2c"color rojo-->
            <%}else{
            ordenFutura +=stock;
            stock = 0;
            if(ordenFutura >= 0){%>
            <tr  bgcolor="#33CC66">
            <%}else{%>
            <tr>
                <%}}%>
                <%if(equi.getReproceso().equals("SI")){%>
                <td align="center" bgcolor="#FF0000"><font class="Estilo9"><%=formate.format(equi.getExpeditacion())%></font></td>
                <%}else if(equi.getHidropack().equals("SI")){%>
                <td align="center" bgcolor="#FFFF00"><font class="Estilo9"><%=formate.format(equi.getExpeditacion())%></font></td>
                <%}else{%>
                <td align="center"><font class="Estilo9"><%=formate.format(equi.getExpeditacion())%></font></td>
                <%}%>
                <td align="center"><font class="Estilo9"><%=formate.format(equi.getReadTime())%></font></td>
                <%if(equi.getCliente().length() >=18){%>
                <td align="center"><font class="Estilo9" title="<%=equi.getCliente()%>"><%=equi.getCliente().substring(0,18)%></font></td>
                <%}else{%>
                <td align="center"><font class="Estilo9" title="<%=equi.getCliente()%>"><%=equi.getCliente()%></font></td>
                <%}%>
                <td align="center"><font class="Estilo9"><%=equi.getNp()%></font></td>
                <td align="center"><font class="Estilo9"><%=equi.getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(equi.getCantidad())%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(equi.getConsumo())%></font></td>
                <td align="center"><font class="Estilo9"><%=equi.getProceso()%></font></td>
                <td align="center"><font class="Estilo9"><%=equi.getDescripcion()%></font></td>
                <%if(equi.getDiseno() == null || equi.getDiseno().equals("")){%>
                <td align="center"><font class="Estilo9">&nbsp;</font></td>
                <%}else{%>
                <td align="center"><font class="Estilo9"><%=equi.getDiseno()%></font></td>
                <%}%>
                <td align="center"><font class="Estilo9"><%=equi.getSerie()%></font></td>
            </tr>
            <%}%>
        </table>
        <br>
        <%}catch(Exception e){
        out.println("ERROR: equiposConNecesidad.jsp " + e.getMessage());
        }%>
    </body>
</html>