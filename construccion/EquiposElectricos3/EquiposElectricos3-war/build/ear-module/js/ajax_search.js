function asignaVariables()
{
    // Funcion que asigna variables que se usan a lo largo de las funciones
    v=1;            
    divLista=document.getElementById("lista");
    inputLista=document.getElementById("input_2");
    chkEstado = document.getElementById("hstate");
    inputIdAjax=document.getElementById("indiceAjax");
    txtDescAjax=document.getElementById("txtDesc");
    elementoSeleccionado=0;    
}

function nuevoAjax()
{
    /* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
	lo que se puede copiar tal como esta aqui */
    var xmlhttp=false;
    try
    {
        // Creacion del objeto AJAX para navegadores no IE
        xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
    }
    catch(e)
    {
        try
        {
            // Creacion del objet AJAX para IE
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        catch(E) {
            xmlhttp=false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!="undefined") {
        xmlhttp=new XMLHttpRequest();
    }

    return xmlhttp;
}

function formateaLista(valor)
{
    // Funcion encargada de ir colocando en negrita las palabras y asignarle un ID a los elementos    
    var x=0, verificaExpresion=new RegExp("^("+valor+")", "i");
    while(divLista.childNodes[x]!=null)
    {
        // Asigo el ID para reconocerlo cuando se navega con el teclado                
        divLista.childNodes[x].id=x+1;        
        if(isDefined(divLista.childNodes[x])){            
            // Coloco en cada elemento de la lista en negrita lo que se haya ingresado en el input
            divLista.childNodes[x].innerHTML=divLista.childNodes[x].innerHTML.replace(verificaExpresion, "$1");            
        }        
        x++;
    }
}

function isDefined(variable) {
    return (typeof(window[variable]) == "undefined")?  false: true;
}

function limpiaPalabra(palabra)
{
    // Funcion encargada de sacarle el codigo HTML de la negrita a las palabras
    palabra=palabra.replace(/<b>/i, "");
    palabra=palabra.replace(/<\/b>/i, "");
    return palabra;
}

function reiniciaSeleccion()
{
    mouseFuera();
    elementoSeleccionado=0;   
}

function navegaTeclado(evento)
{
    var teclaPresionada=(document.all) ? evento.keyCode : evento.which;    
    switch(teclaPresionada)
    {
        case 40:
            if(elementoSeleccionado<divLista.childNodes.length)
            {
                mouseDentro(document.getElementById(parseInt(elementoSeleccionado)+1));
            }
            return 0;

        case 38:
            if(elementoSeleccionado>1)
            {
                mouseDentro(document.getElementById(parseInt(elementoSeleccionado)-1));
            }
            return 0;

        case 13:
            if(divLista.style.display=="block" && elementoSeleccionado!=0)
            {
                clickLista(document.getElementById(elementoSeleccionado))
            }
            return 0;

        default:
            return 1;
    }
}

function rellenaLista()
{
    var valor=inputLista.value;  
    var valor1=chkEstado.value;  
    // Valido con una expresion regular el contenido de lo que el usuario ingresa    
    var reg;
    if(chkEstado.value == 'descrip'){
       reg=/(^[a-zA-Z0-9 !"#$%'()*+,./:;<=>?@\^_`{|}~-]{3,100}$)/;
    }else{
       reg=/(^[a-zA-Z0-9 !"#$%'()*+,./:;<=>?@\^_`{|}~-]{8,11}$)/;
    }
    if(!reg.test(valor)) divLista.style.display="none";
    else
    {        
        var ajax=nuevoAjax();
        ajax.open("POST", "../../FrmAjax?", true);
        ajax.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        ajax.send("busqueda="+valor+"&stat="+valor1);

        ajax.onreadystatechange=function()
        {
            if (ajax.readyState==4)
            {
                if(!ajax.responseText) {
                    divLista.style.display="none";
                }
                else
                {
                    var respuesta=new Array(2);
                    respuesta=ajax.responseText.split("&");

                    /* Obtengo un valor que representa si tengo que ir a BD en las proximas
						busquedas con cadena similar */
                    nuevaBusqueda=respuesta[0];

                    divLista.style.display="block";
                    divLista.innerHTML=respuesta[1];
                    // Coloco en negrita las palabras                    
                    reiniciaSeleccion();                    
                    formateaLista(valor);                    
                }
            }
        }
    }
}

function obtenerDescripcion() {
   var rdm=Math.floor(Math.random()*11);
   var url = "../../FrmAjax?indiceAjax="+inputIdAjax.value+"&rdm"+rdm;
  var ajax=nuevoAjax();
  if (ajax) {
     ajax.open("GET", url, false);
     ajax.send(null);
     txtDescAjax.value = ajax.responseText;
  } else {
     txtDescAjax.value = "";

  }

}


function clickLista(elemento)
{
    /* Se ejecuta cuando se hace clic en algun elemento de la lista. Se coloca en el input el
	valor del elemento clickeado */
    v=1;
    valor=limpiaPalabra(elemento.innerHTML);    
    inputLista.value=valor;
    inputIdAjax.value=elemento.id-1;
    divLista.style.display="none";
    elemento.className="normal";    
}

function mouseFuera()
{
    // Des-selecciono el elemento actualmente seleccionado, si es que hay alguno
    if(elementoSeleccionado!=0 && document.getElementById(elementoSeleccionado)) document.getElementById(elementoSeleccionado).className="normal";    
}

function mouseDentro(elemento)
{
    mouseFuera();
    elemento.className="resaltado";
    // Establezco el nuevo elemento seleccionado
    elementoSeleccionado=elemento.id;    
}