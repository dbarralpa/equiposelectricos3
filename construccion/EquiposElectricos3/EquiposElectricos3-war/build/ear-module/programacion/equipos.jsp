<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pro.calidadProgramacion.bean.*,mac.ee.pla.equipo.clase.Equipo,java.text.*" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <link rel="stylesheet" href="../themes/base/jquery.ui.all.css"/>
        <script type="text/javascript" src="../js/jquery-1.3.2.js"></script>
        <script language="javascript" type="text/javascript" src="../js/jquery.ui.core.js"></script>
        <script language="javascript" type="text/javascript" src="../js/jquery.ui.widget.js"></script>
        <script language="javascript" type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
        <script>
            function vali(){
            
            var indice = document.form1.razon.selectedIndex;
            var valor = document.form1.razon.options[indice].value;
            if(valor == 0 || document.form1.fechaMenor.value == "" || document.form1.comentario.value == ""){
              alert('Debe agregar una razon, fecha de expeditacion y comentario');
              return false;
            }else{
            form1.submit();
            }
         }
        </script>
    </head>
    <body>
         <script>
            $(document).ready(function(){
            $( "#fechaMenor" ).datepicker();
            $( "#fechaMenor" ).datepicker();
        });
        </script>
        <form name="form1" method="post">
            <%try{
            HttpSession sesion    = request.getSession();
            java.text.NumberFormat number = new java.text.DecimalFormat(" ");
            java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
            NumberFormat format = NumberFormat.getInstance();
            format.setMaximumFractionDigits(1);
            format.setMinimumFractionDigits(0);
            Vector equipos = new Vector(); 
            String fecha = "";
            CalidadDeProgramacionBean programacion = new CalidadDeProgramacionBean();  
            if(request.getParameter("np") == null){
            if(request.getParameter("fecha").length()==7){
            sesion.setAttribute("fecha",request.getParameter("fecha").substring(0,4)+"0"+request.getParameter("fecha").substring(4,7));
            }else{
            sesion.setAttribute("fecha",request.getParameter("fecha"));
            }
            if(!request.getParameter("area").equals("nada")){
            sesion.setAttribute("equipos",programacion.equipos((String)sesion.getAttribute("fecha"),request.getParameter("area")));    
            }else{
            sesion.setAttribute("equipos",programacion.equiposTodos((String)sesion.getAttribute("fecha")));
            }   
            sesion.setAttribute("area",request.getParameter("area"));
            sesion.setAttribute("np",request.getParameter("np"));            
            sesion.setAttribute("linea",request.getParameter("linea"));            
            }
            
            float monto = 0;
            %>
            <br><br>
            <center><h5><font class="Estilo10">EQUIPOS POR AREA</font></h5></center>
             <center><a href="javascript:window.opener.document.location.reload();self.close()">Cerrar y actualizar</a></center>
            <%if(!((String)sesion.getAttribute("area")).equals("nada")){%>
            <center><h5><font class="Estilo10"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(0)).getFamilia()%></font></h5></center>
            <%}%>
            <br>
            <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
                
                
                <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
                    <td align="center"><font class="Estilo9">N°</font></td>
                    <td align="center"><font class="Estilo9">Np</font></td>
                    <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                    <td align="center"><font class="Estilo9">Cliente</font></td>
                    <td align="center"><font class="Estilo9">Fecha Cliente</font></td>
                    <td align="center"><font class="Estilo9">Fecha Expeditacion</font></td>
                    <td align="center"><font class="Estilo9">Monto</font></td>
                    <td align="center"><font class="Estilo9">Serie</font></td>
                    <td align="center"><font class="Estilo9">Kva</font></td>
                    <td align="center"><font class="Estilo9">Status</font></td>
                    <td align="center"><font class="Estilo9">Descripci&oacute;n</font></td>
                    <%if(((String)sesion.getAttribute("area")).equals("nada")){%>
                    <td align="center"><font class="Estilo9">Familia</font></td>
                    <%}%>
                    <td align="center"><font class="Estilo9">Bit&aacute;cora</font></td>
                </tr>
                
                <%
                for(int u=0;u<((Vector)sesion.getAttribute("equipos")).size();u++){%>
                
                <tr><!--"#d51f2c"color rojo-->
                    <td align="center"><font class="Estilo9"><%=u+1%></font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getNp()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getLinea()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getNombreCliente()%></font></td>
                    <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getFechaCliente())%></font></td>
                    <%if(((String)sesion.getAttribute("usuario")).equals("caravena") || ((String)sesion.getAttribute("usuario")).equals("vhernandez")){%>
                    <td align="center"><font class="Estilo9"><a href="equipos.jsp?np=<%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getNp()%>&linea=<%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getLinea()%>&serie=<%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getSerie()%>"><%=formate.format(((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getExpeditacion())%></a></font></td>
                    <%}else{%>
                    <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getExpeditacion())%></font></td>
                    <%}%>
                    <td align="center"><font class="Estilo9"><%=number.format(((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getMonto())%></font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getSerie()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getKva()%>&nbsp;</font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getStatus()%></font></td>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getDescripcion()%></font></td>
                    <%if(((String)sesion.getAttribute("area")).equals("nada")){%>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getFamilia()%></font></td>
                    <%}%>
                    <td align="center"><font class="Estilo9"><%=((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getBitacora()%></font></td>
                </tr>
                <%monto += ((Equipo)((Vector)sesion.getAttribute("equipos")).elementAt(u)).getMonto();%>
                <%}%>
            </table>
            <center><h5><font class="Estilo10">Monto:&nbsp;<%=format.format(monto)%></font></h5></center>
            
            <br><br>
            
            <%if(request.getParameter("np")!= null){
            sesion.setAttribute("np",request.getParameter("np"));
            sesion.setAttribute("linea",request.getParameter("linea"));
            sesion.setAttribute("serie",request.getParameter("serie"));
            }%>
            <table width="70%" cellpadding="0" cellspacing="0" bgcolor="#D0DBB1" border="0" bordercolordark="#0066FF" bordercolorlight="#FFFF66" align="center">
                
                <tr>
                    <td class="Estilo10" height="30" align="center" colspan="3"> Cambiar fecha expeditaci&oacute;n</td>
                </tr>
                <tr>
                    <%if(sesion.getAttribute("np") != null){%>
                    <td class="Estilo10" height="30" align="center" colspan="3">Pedido&nbsp;&nbsp;<%=sesion.getAttribute("np")%>&nbsp;&nbsp;Linea&nbsp;&nbsp;<%=sesion.getAttribute("linea")%></td>
                    <%}else{%>
                    <td class="Estilo10" height="30" align="center" colspan="3">Pedido&nbsp;&nbsp;0&nbsp;&nbsp;Linea&nbsp;&nbsp;0</td>
                    <%}%>
                </tr>
                <tr class="Estilo10">
                    <td>Raz&oacute;n Expeditaci&oacute;n&nbsp;&nbsp;
                        <select name="razon" class="Estilo10">
                            <option value="0">Elija una raz&oacute;n</option>
                            <option value="1">Producci&oacute;n</option>
                            <option value="2">Faltan planos, Esps, Docs</option>
                            <option value="3">Falta abastecimiento</option>
                            <option value="4">Modificaci&oacute;n al diseño</option>
                            <!--option value="5">Falta revisi&oacute;n final</option-->
                            <option value="5">Falta de inspecci&oacute; externa</option>
                            <option value="6">Equipo en reproceso</option>
                            <option value="7">Falta aprobaci&oacute;n de planos</option>
                            <option value="8">Capacidad de planta</option>
                        </select>
                    </td>
                    <td>Fecha Expeditaci&oacute;n</td>
                    <td>
                        <!--DIV id="popCal" style="BORDER-RIGHT: 2px ridge; BORDER-TOP: 2px ridge; Z-INDEX: 100; VISIBILITY: hidden; BORDER-LEFT: 2px ridge; WIDTH: 10px; BORDER-BOTTOM: 2px ridge; POSITION: absolute" onclick=event.cancelBubble=true>
                        <IFRAME name="popFrame" src="../popcjs.html" frameBorder=0 width=200 scrolling=no height=188></IFRAME></DIV>
                        <SCRIPT event=onclick() for=document>popCal.style.visibility = "hidden";</SCRIPT>
                        <TABLE align="left" CELLPADDING="0" CELLSPACING="0">
                            <TR>
                                <TD ALIGN="left">
                                    <input class="Estilo10" readOnly name=fechaMenor size="12">
                                    <a onclick="popFrame.fPopCalendar(fechaMenor,fechaMenor,popCal);return false">
                                    <img src="../Images/calendar.gif" width="16" height="16" alt="calendario"/>
                                </td>
                            </tr>
                        </table-->
                        fecha:&nbsp;<input id="fechaMenor" name="fechaMenor" type="text" size="15" readonly="yes" <%if(request.getParameter("fechaMenor") != null && !request.getParameter("fechaMenor").equals("")){%>value="<%=request.getParameter("fechaMenor")%>"<%}%> />       
                    </td>
                </tr>
                 <tr>
                    <td class="Estilo10" height="30" align="center" colspan="3">Comentario</td>
                </tr>
                <tr>
                    <td class="Estilo10" height="30" align="center" colspan="3"><textarea name="comentario" cols="60" rows="2"></textarea></td>
                </tr>
                <tr>
                    
                    <%if(sesion.getAttribute("np") != null){%>
                    <td height="60" align="center" colspan="3"><input type="submit" name="actualizar" class="Estilo10" onclick="return vali();"> </td>
                    <%}else{%>
                    <td height="60" align="center" colspan="3"><input type="submit" name="actualizar" class="Estilo10" onclick="return vali();" disabled> </td>
                    <%}%>
                </tr>
                <% byte valor = (byte)0;
                   if(request.getParameter("actualizar") != null){
                     
                      valor = programacion.modificarFechaExpeditacion((String)sesion.getAttribute("np"),
                                                            (String)sesion.getAttribute("linea"),
                                                            formate.format(new java.sql.Date(0).valueOf(request.getParameter("fechaMenor"))),
                                                            (String)sesion.getAttribute("usuario"),
                                                            formate.format(new java.util.Date()),
                                                            (String)sesion.getAttribute("serie"),
                                                            request.getParameter("razon"),
                                                            request.getParameter("comentario"));
                                                            
                 if(valor == 1){%>
                 <tr>
                 <td class="Estilo10" height="30" align="center" colspan="3"><script>alert('Datos actuslizados');</script></td>
                </tr>
                <%}else{%>
                <tr>
                 <td class="Estilo10" height="30" align="center" colspan="3"><script>alert('No se pudo actualizar los datos');</script></td>
                </tr>
                <%}}%>
                
            </table>
            <%}catch(Exception e){
            out.println("ERROR: equipos.jsp " + e.getMessage());
            }%>
        </form>
    </body>
</html>
