<%@page import="java.io.*, java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,mac.ee.pro.calidadProgramacion.bean.*,mac.ee.pla.equipo.clase.Equipo,java.text.*"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%--
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%> 
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Manufactura asistida por computador</title>
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
    </head>
    <body>
    <%try{
                
                java.text.NumberFormat number = new java.text.DecimalFormat(" ");
                java.text.SimpleDateFormat formate = new java.text.SimpleDateFormat("dd-MM-yyyy");
                NumberFormat format = NumberFormat.getInstance();
                format.setMaximumFractionDigits(1);
                format.setMinimumFractionDigits(0);
                String fecha = "";
                CalidadDeProgramacionBean programacion = new CalidadDeProgramacionBean();
                String ano2 = request.getParameter("ano2");
                String mes2 = request.getParameter("mes2");
                String fecha1 = request.getParameter("fecha1");
                String fecha2 = request.getParameter("fecha2");
                String razon  = request.getParameter("razon");
                Vector equipos = new Vector();
                if(request.getParameter("veri").equals("1")){
                   equipos = programacion.equiposIndiceCarga(fecha1,fecha2);
                }
                if(request.getParameter("veri").equals("2")){
                   equipos = programacion.razonReprograma(Integer.parseInt(ano2),Integer.parseInt(mes2)+1,fecha2,razon);
                }
                float monto = 0;
        %>
        <br><br>
        <center><h5><font class="Estilo10">EQUIPOS</font></h5></center>
        <%if(razon != null && razon.equals("12")){%>
        <br>
        <center><h3><font class="Estilo10">Mod diseño &nbsp;&nbsp;&nbsp;&nbsp; Ex cap planta</font></h3></center>
        <br>
        <%}%>
      <table  align="center" border="1" cellpadding="0" cellspacing="0" bgcolor="#cccccc">
         
         
          <tr bgcolor="#B9DDFB"><!--"#d51f2c"color rojo-->
                <td align="center"><font class="Estilo9">N°</font></td>
                <td align="center"><font class="Estilo9">Np</font></td>
                <td align="center"><font class="Estilo9">L&iacute;nea</font></td>
                <td align="center"><font class="Estilo9">Cliente</font></td>
                <td align="center"><font class="Estilo9">Fecha Cliente</font></td>
                <td align="center"><font class="Estilo9">Fecha Expeditacion</font></td>
                <td align="center"><font class="Estilo9">Monto</font></td>
                <td align="center"><font class="Estilo9">Serie</font></td>
                <td align="center"><font class="Estilo9">Kva</font></td>
                <td align="center"><font class="Estilo9">Familia</font></td>
                <td align="center"><font class="Estilo9">Status</font></td>
                <%if(request.getParameter("veri").equals("2") && razon.equals("6")){%>
                <td align="center"><font class="Estilo9">Obs. Laboratorio</font></td>
                <%}else{%>
                <td align="center"><font class="Estilo9">Bitacora</font></td>
                <%}%>
            </tr>
            
           <%
            for(int u=0;u<equipos.size();u++){%>
               
            <tr><!--"#d51f2c"color rojo-->
                <td align="center"><font class="Estilo9"><%=u+1%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getNp()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getNombreCliente()%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)equipos.elementAt(u)).getFechaCliente())%></font></td>
                <td align="center"><font class="Estilo9"><%=formate.format(((Equipo)equipos.elementAt(u)).getExpeditacion())%></font></td>
                <td align="center"><font class="Estilo9"><%=number.format(((Equipo)equipos.elementAt(u)).getMonto())%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getSerie()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getKva()%>&nbsp;</font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getFamilia()%></font></td>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getStatus()%></font></td>
                <%if(request.getParameter("veri").equals("2") && razon.equals("6")){%>
                <td align="center"><font class="Estilo9"><%=programacion.observacionRepreoceso(((Equipo)equipos.elementAt(u)).getNp(),((Equipo)equipos.elementAt(u)).getLinea())%></font></td>
                <%}else{%>
                <td align="center"><font class="Estilo9"><%=((Equipo)equipos.elementAt(u)).getBitacora()%></font></td>
                <%}%>
            </tr>
            <%monto += ((Equipo)equipos.elementAt(u)).getMonto();%>
            <%}%>
        </table>
<center><h5><font class="Estilo10">Monto:&nbsp;<%=format.format(monto)%></font></h5></center>
    <%}catch(Exception e){
        out.println("ERROR: programa.jsp " + e.getMessage());
        }%>
    </body>
</html>
