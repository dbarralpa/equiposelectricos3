<%@ page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector" session="true"%>
<%@page contentType="application/vnd.ms-excel"%>
<%response.setHeader("Content-Disposition","attachment; filename=\"ordenes.xls\""); %>
<%@page pageEncoding="ISO-8859-1"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<%HttpSession sesion = request.getSession(); 
  String idSesion    = request.getParameter("idSesion");
  String usuario     = request.getParameter("usuario");
  String tipou       = request.getParameter("tipou");%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">        
    </head>
     <%if(idSesion.equals(sesion.getId())){
               OrdenRemote ordenCompra = null;
               try{
                 ConexionEJB cal = new ConexionEJB();  
                 Object ref =cal.conexionBean("OrdenBean");
                 OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
                 ordenCompra= ordenHome.create();
                 }catch(Exception e){
                    out.println("ERROR...!!!!" + e.getMessage());
                 }%>
    <body>
        <table border="1" cellpadding="0" cellspacing="0">
            <tr bgcolor="#B9DDFB">
                <td><font class="Estilo10">Cantidad</font></td>
                <td><font class="Estilo10">Numero_orden</font> </td>
                <td><font class="Estilo10">Linea</font></td>
                <td><font class="Estilo10">Rut_proveedor</font></td>
                <td><font class="Estilo10">Fecha_pedido</font> </td>
                <td><font class="Estilo10">Fecha_entrega</font></td>
                <td><font class="Estilo10">Cantidad-pedida</font> </td>
                <td><font class="Estilo10">Cantidad_pendiente</font></td>
                <td><font class="Estilo10">C�digo_producto</font> </td>
                <td width="100%"><font class="Estilo10">Descripci�n_producto</font></th>
            </tr>
            <%if((request.getParameter("id").equals("1"))||(request.getParameter("id").equals("2"))||(request.getParameter("id").equals("3"))||(request.getParameter("id").equals("4"))||(request.getParameter("id").equals("5"))||(request.getParameter("id").equals("6"))){%>
            <%Vector ordenesAtrasadas = new Vector();
            ordenesAtrasadas = ordenCompra.ordenDeCompraInvalidas(request.getParameter("fechaHoy"),Integer.parseInt(request.getParameter("id")));
            Enumeration eAtrasada = ordenesAtrasadas.elements();
            int i = 0;
            while(eAtrasada.hasMoreElements()){
              mac.ee.pla.ordeCompra.clase.OrdenDeCompra ordenInvalidas =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eAtrasada.nextElement();%>
            <tr bgcolor="#DDDDDD">
                <td align="center"><font class="Estilo9"><%=i%></font></td>    
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getNum_orden_compra()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=(int)ordenInvalidas.getRut()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getFecha_pedido()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getFecha_entrega()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getCan_pedida()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getCan_pendiente()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getCod_producto()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenInvalidas.getDescripcion()%></font></td>
            </tr><%i++;%>
            <%}%>
            <%}else if((request.getParameter("id").equals("7"))||(request.getParameter("id").equals("8"))||(request.getParameter("id").equals("9"))||(request.getParameter("id").equals("10"))||(request.getParameter("id").equals("11"))||(request.getParameter("id").equals("12"))){%>
            <%Vector ordenesFuturas = new Vector();
            ordenesFuturas = ordenCompra.ordenDeCompraValidas(request.getParameter("fechaHoy"),Integer.parseInt(request.getParameter("id")));
            Enumeration eFutura = ordenesFuturas.elements();
            int i2 = 0;
            while(eFutura.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra ordenFuturas =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eFutura.nextElement(); %>
             <tr>
                <td align="center"><font class="Estilo9"><%=i2%></font></td>    
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getNum_orden_compra()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=(int)ordenFuturas.getRut()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getFecha_pedido()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getFecha_entrega()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getCan_pedida()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getCan_pendiente()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getCod_producto()%></font></td>
                <td align="center"><font class="Estilo9"><%=ordenFuturas.getDescripcion()%></font></td>
            </tr><%i2++;%>
            <%}
            }else{%>
             <%Vector todasLasOrdenes = new Vector();
            todasLasOrdenes = ordenCompra.listarTodasLasOrdenes();
            Enumeration eTodas = todasLasOrdenes.elements();
            int i3 = 0;
            while(eTodas.hasMoreElements()){
               mac.ee.pla.ordeCompra.clase.OrdenDeCompra todas =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eTodas.nextElement(); %>
             <tr>
                <td align="center"><font class="Estilo9"><%=i3%></font></td>    
                <td align="center"><font class="Estilo9"><%=todas.getNum_orden_compra()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getLinea()%></font></td>
                <td align="center"><font class="Estilo9"><%=(int)todas.getRut()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getFecha_pedido()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getFecha_entrega()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getCan_pedida()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getCan_pendiente()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getCod_producto()%></font></td>
                <td align="center"><font class="Estilo9"><%=todas.getDescripcion()%></font></td>
            </tr><%i3++;}
            }%>
        </table>
    </body>
        <%}else{response.sendRedirect("../mac");}%>
</html>
