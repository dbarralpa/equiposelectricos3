<%@ page import="java.io.*,java.util.*,javax.naming.*,javax.rmi.PortableRemoteObject,conexionEJB.ConexionEJB,mac.ee.pla.ordenCompra.bean.*,java.util.Vector" session="true"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<HTML>
<HEAD>
<TITLE>Manufactura asistida por computador</TITLE>
<script language="JavaScript" TYPE="text/javascript" src ="../js/javascript.js"> </script>
<SCRIPT language= "JavaScript">
    var ancho1,ancho2,i;
    var columnas=9; //CANTIDAD DE COLUMNAS//
    function ajustaCeldas(){
    for(i=0;i<columnas;i++){
    ancho1=document.getElementById("encabezado").rows.item(0).cells.item(i).offsetWidth;
    ancho2=document.getElementById("datos").rows.item(0).cells.item(i).offsetWidth;
    if(ancho1>ancho2){
    document.getElementById("datos").rows.item(0).cells.item(i).width = ancho1-6};
    else{
    document.getElementById("encabezado").rows.item(0).cells.item(i).width = ancho2-6;}
    }
   }
</SCRIPT>
</HEAD>
<BODY onload=ajustaCeldas()>
  <%
               HttpSession sesion    = request.getSession(); 
               String idSesion       = request.getParameter("idSesion");
               String usuario        = request.getParameter("usuario");
               String tipou          = request.getParameter("tipou");
               String fecAno  = null;
               String fecAno2 = null;
               String fecAno3 = null;
               java.util.Date now = new java.util.Date();
               java.text.DateFormat anoDate = new java.text.SimpleDateFormat("yyyyMMdd");
               java.text.DateFormat anoDate2 = new java.text.SimpleDateFormat("yyyy-MM-dd");
           
               fecAno = anoDate.format(now);
               fecAno2 = anoDate2.format(now);
               
               
             //  java.util.Date anoDate2 = new java.util.Date();
             //  long lnMilisegundos = anoDate2.getTime();
             //  java.sql.Date sqlDate = new java.sql.Date(lnMilisegundos);
               //fecAno2 = anoDate2.format(now);
               //out.println("fecha 2 : " + sqlDate);
               int fechaActual            = Integer.parseInt(fecAno);
               String rut                 = request.getParameter("rutClie");
               String proveedor           = request.getParameter("nombreCli");
               String codMateriaPrima     = request.getParameter("nombreCod");
               String descripcionProducto = request.getParameter("nombreDescripcion");
              // String idOrden             = request.getParameter("ordenes");
              // String idBusqueda          = request.getParameter("busqueda");
              
             /*  int identificadorOrden = 0;
               if(idOrden!=null)
                  if(idOrden.equals(""))
                      identificadorOrden = 0;
                      else
                          identificadorOrden = Integer.parseInt(idOrden);
               
               int identificadorBusqueda = 0;
               if(idBusqueda!=null)
                  if(idBusqueda.equals(""))
                      identificadorBusqueda = 0;
                      else
                          identificadorBusqueda = Integer.parseInt(idBusqueda);*/
               
               if(idSesion.equals(sesion.getId())){
               OrdenRemote ordenCompra = null;
               Vector ordenesDeCompraInvalidas = new Vector();
               Vector ordenesDeCompraValidas   = new Vector();
               Vector todasLaOrden             = new Vector();
               Vector buscarPorRut             = new Vector();
               Vector buscarPorProveedor       = new Vector();
               Vector buscarPorCodigo          = new Vector();
               Vector buscarPorCodMatPrima     = new Vector();
               Vector buscarPorDescripcion     = new Vector();%>
             
             
               <%try{
                 ConexionEJB cal = new ConexionEJB();  
                 Object ref =cal.conexionBean("OrdenBean");
                 OrdenRemoteHome ordenHome = (OrdenRemoteHome) PortableRemoteObject.narrow(ref, OrdenRemoteHome.class);
                 ordenCompra= ordenHome.create();
                 }catch(Exception e){
                    out.println("ERROR...!!!!" + e.getMessage());
                 }
                 if(request.getParameter("ordenes")!=null){
                 ordenesDeCompraInvalidas = ordenCompra.ordenDeCompraInvalidas(fecAno,Integer.parseInt(request.getParameter("ordenes")));
                 ordenesDeCompraValidas   = ordenCompra.ordenDeCompraValidas(fecAno,Integer.parseInt(request.getParameter("ordenes")));
                 todasLaOrden             = ordenCompra.listarTodasLasOrdenes();
                 }
                 if(request.getParameter("submitButtonName")!=null){
                 buscarPorRut = ordenCompra.buscarPorRutCliente(Long.parseLong(rut));
                 }
                 if(request.getParameter("submitButtonNomCli")!=null){
                 buscarPorProveedor = ordenCompra.mostrarDatosPorProveedor(proveedor);
                 }
                 if(request.getParameter("submitButtonNomCod")!=null){
                   buscarPorCodMatPrima = ordenCompra.mostrarDatosPorCodigo(codMateriaPrima);
                 }
                 if(request.getParameter("submitButtonNomDesc")!=null){
                   buscarPorDescripcion = ordenCompra.mostrarDatosPorDescripcionMateriaPrima(descripcionProducto);
                 }
                 
                 Enumeration enumInvalidas    = ordenesDeCompraInvalidas.elements();
                 Enumeration enumValidas      = ordenesDeCompraValidas.elements();
                 Enumeration enumRut          = buscarPorRut.elements();
                 Enumeration eProveedor       = buscarPorProveedor.elements();
                 Enumeration eCodigoMatPrimas = buscarPorCodMatPrima.elements();
                 Enumeration eDescMatPrima    = buscarPorDescripcion.elements();
                 Enumeration enumTodas        = todasLaOrden.elements();
                 int i=1;
                 
                 Vector rutPorCliente        = new Vector();
                 Vector ordenPorCliente      = new Vector();
                 Vector nombreProveedor      = new Vector();
                 Vector VecProveedor         = new Vector();
                 Vector codigoMateriaPrima   = new Vector();
                 Vector descripcionMatPrimas = new Vector();
                 
                 rutPorCliente        = ordenCompra.buscarPorRutCliente();
                 ordenPorCliente      = ordenCompra.buscarPorNumeroDeOrden();
                 VecProveedor         = ordenCompra.buscarPorProveedor();
                 codigoMateriaPrima   = ordenCompra.buscarPorCodigo();
                 descripcionMatPrimas = ordenCompra.buscarPorDescripcion();

                 Enumeration eBuscarNombre        = rutPorCliente.elements();
                 Enumeration eOrdenPorCliente     = ordenPorCliente.elements();
                 Enumeration eBuscarProveedor     = VecProveedor.elements();
                 Enumeration eCodMatPrima         = codigoMateriaPrima.elements();
                 Enumeration eDescripcionMatPrima = descripcionMatPrimas.elements();

                 int numAutoCom         = rutPorCliente.size();
                 int numAutoProveedor   = VecProveedor.size(); 
                 int numCodigoMatPrimas = codigoMateriaPrima.size();
                 int numDescMatPrimas   = descripcionMatPrimas.size();

                 int contador  = 0;
                 int contador2 = 0;
                 int contador3 = 0;
                 int contador4 = 0;
                 %>
        <script language="javascript" type="text/javascript" src="actb.js"></script>
        <script language="javascript" type="text/javascript" src="common.js"></script>
        <%if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("1"))){%>
        <script>
                  var customarray = new Array(
                    <%while(eBuscarNombre.hasMoreElements()){
                        contador++;
                        mac.ee.pla.ordeCompra.clase.OrdenDeCompra buscarRut =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eBuscarNombre.nextElement();
                        if(numAutoCom==contador){%>
                          "<%=buscarRut.getRut()%>"
                          <%}else{%>
                          "<%=buscarRut.getRut()%>",
                     <%}
                    }%>
                  );            
        </script>
        <%}if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("2"))){%>
         <script>
                  var customarray2 = new Array(
                    <%while(eBuscarProveedor.hasMoreElements()){
                        contador2++;
                        mac.ee.pla.ordeCompra.clase.OrdenDeCompra buscarProveedor =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eBuscarProveedor.nextElement();
                        if(numAutoProveedor==contador2){%>
                          "<%=buscarProveedor.getProveedor()%>"
                          <%}else{%>
                          "<%=buscarProveedor.getProveedor()%>",
                     <%}
                    }%>
                  );            
        </script>
           <%}if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("3"))){%>
         <script>
                  var customarray3 = new Array(
                    <%while(eCodMatPrima.hasMoreElements()){
                        contador3++;
                        mac.ee.pla.ordeCompra.clase.OrdenDeCompra buscarCodigoMatPrima =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eCodMatPrima.nextElement();
                        if(numCodigoMatPrimas==contador3){%>
                          "<%=buscarCodigoMatPrima.getCod_producto()%>"
                          <%}else{%>
                          "<%=buscarCodigoMatPrima.getCod_producto()%>",
                     <%}
                    }%>
                  );            
        </script> 
        <%}if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("4"))){%>
        <script>
                  var customarray4 = new Array(
                    <%while(eDescripcionMatPrima.hasMoreElements()){
                        contador4++;
                        mac.ee.pla.ordeCompra.clase.OrdenDeCompra buscarDescripcionMatPrima =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eDescripcionMatPrima.nextElement();
                        if(numDescMatPrimas==contador4){%>
                          "<%=buscarDescripcionMatPrima.getDescripcion()%>"
                          <%}else{%>
                          "<%=buscarDescripcionMatPrima.getDescripcion()%>",
                     <%}
                    }%>
                  );            
        </script>
        <%}%>
        <script LANGUAGE="javascript" TYPE="text/javascript">
        function entsub(event,ourform) {
        if (event && event.which == 13)
        ourform.submit();
        else
        return true;}
        </script>
        <style type="text/css">
        .botoncontacto 
        {
        background-image: url(../Images/lupa.JPG); width: 15px; height: 20px; border-width: 0
        }
        </style>               
        <link REL="stylesheet" TYPE="text/css" HREF="../CSS/estiloPaginas.css" />
        <form name="formulario" action="todaslasOrdenes.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>" method="post">
            <center>
                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="Estilo10">
                    <tr>
                        <td colspan="3" align="center"><font size="3" color="#000099">Listar ordenes de compras</font></td>
                    </tr>
                    <tr>
                      <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="center">
                           <select name="busqueda" class="Estilo10"  onchange="formulario.submit()">
                                <option value="0">Escoger opcion de busqueda</option>
                                <%Vector motorBusqueda = new Vector();
                                motorBusqueda = ordenCompra.listarMotorDeBusqueda();
                                Enumeration eMotor = motorBusqueda.elements();
                               while(eMotor.hasMoreElements()){
                                     mac.ee.pla.ordeCompra.clase.OrdenDeCompra motor =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eMotor.nextElement();%>
                                     <option value="<%=motor.getIdOrden()%>"><%=motor.getNombreOrden()%></option>
                                <%}%>
                            </select>
                      </td>
                      <td colspan="2" align="center">
                        <select name="ordenes" class="Estilo10" onchange="formulario.submit()">
                            <option value="0">Escoga una opcion</option>
                            <%Vector orden = new Vector();
                            orden = ordenCompra.listarOrdenes();
                            Enumeration eOrden = orden.elements();
                            while(eOrden.hasMoreElements()){
                                mac.ee.pla.ordeCompra.clase.OrdenDeCompra ordenes =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eOrden.nextElement();%>
                            <option value="<%=ordenes.getIdOrden()%>"><%=ordenes.getNombreOrden()%></option>
                            <%}%>
                        </select>
                      </td>
                    </tr>
                    <tr>
                        <%if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("1"))){%>
                        <td width="34%" align="center"><font class="Estilo10">
                           <input type="text" class="Estilo10" size="50" name="rutClie" type='text' id='tb' value='' onkeypress="return entsub(event,this.form)">
                           <script>var obj = new actb(document.getElementById('tb'),customarray);</script> 
                        </td>                       
                        <td width="14%" align="left"><input class="botoncontacto" type="submit" name="submitButtonName" value="" alt="Buscar"></td>
                        <%}else if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("2"))){%>
                         <td width="34%" align="center"><font class="Estilo10">
                           <input type="text" class="Estilo10" size="50" name="nombreCli" type='text' id='tb' value='' onkeypress="return entsub(event,this.form)">
                           <script>var obj2 = new actb(document.getElementById('tb'),customarray2);</script> 
                           
                        </td>                       
                        <td width="14%" align="left"><input class="botoncontacto" type="submit" name="submitButtonNomCli" value="" alt="Buscar"></td>                                          
                        <%}else if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("3"))){%>
                         <td width="34%" align="center"><font class="Estilo10">
                           <input type="text" class="Estilo10" size="50" name="nombreCod" type='text' id='tb' value='' onkeypress="return entsub(event,this.form)">
                           <script>var obj3 = new actb(document.getElementById('tb'),customarray3);</script> 
                          
                        </td>                       
                        <td width="14%" align="left"><input class="botoncontacto" type="submit" name="submitButtonNomCod" value="" alt="Buscar"></td>                                          
                        <%}else if((request.getParameter("busqueda")!=null)&&(request.getParameter("busqueda").equals("4"))){%>
                         <td width="34%" align="center"><font class="Estilo10">
                           <input type="text" class="Estilo10" size="50" name="nombreDescripcion" type='text' id='tb' value='' onkeypress="return entsub(event,this.form)">
                           <script>var obj4 = new actb(document.getElementById('tb'),customarray4);</script> 
                           
                        </td>                       
                        <td width="14%" align="left"><input class="botoncontacto" type="submit" name="submitButtonNomDesc" value="" alt="Buscar"></td>                                          
                        <%}else{%>   
                        <td colspan="3">&nbsp;</td>
                        <%}%>
                    </tr>
                    <tr>
                        <td colspan="3" align="center">&nbsp;</td>
		    </tr>                    
                    <tr>
                        <td colspan="2">&nbsp;</td>
                        <td align="center"><a href="../mrp/index.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>"><img src="../Images/volver2.jpg" border="0"/></a></td>
                    </tr>
                </table>                
            </center>
            <br><br>
            <%if((request.getParameter("ordenes")!=null)||(request.getParameter("busqueda")!=null)){%>
            <table border=1 bgcolor=scrollbar align=center>
                <td>
                    <table id="encabezado" border=1 cellspacing=0 cellpadding=2 bgcolor=#cccccc>
                        <tr bgcolor="#B9DDFB">
                            <th><font class="Estilo9">Cantidad</font></th>
                            <th><font class="Estilo9">Numero_orden</font></th>
                            <th><font class="Estilo9">Linea</font></th>                          
                            <th><font class="Estilo9">Rut_proveedor</font></th>
                            <th><font class="Estilo9">Fecha_pedido</font> </th>
                            <th><font class="Estilo9">Fecha_entrega</font></th>
                            <th><font class="Estilo9">Cantidad-pedida</font> </th>
                            <th><font class="Estilo9">Cantidad_pendiente</font></th>
                            <th><font class="Estilo9">Código_producto</font> </th>                           
                            <th width="100%"><font class="Estilo9">Descripción_producto</font> </th>
                        </tr>
                    </table>
                    <div style="overflow:auto; height:330px; padding:0">
                    <%if((request.getParameter("ordenes").equals("1"))||(request.getParameter("ordenes").equals("2"))||(request.getParameter("ordenes").equals("3"))||(request.getParameter("ordenes").equals("4"))||(request.getParameter("ordenes").equals("5"))||(request.getParameter("ordenes").equals("6"))){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                        <%while(enumInvalidas.hasMoreElements()){
                            mac.ee.pla.ordeCompra.clase.OrdenDeCompra ordenInvalidas =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enumInvalidas.nextElement();%>
                        <tr>
                            <td align="center"><font class="Estilo9"><%=i%></font></td>    
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getNum_orden_compra()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getLinea()%></font></td>
                            <td align="center"><font class="Estilo9"><%=(int)ordenInvalidas.getRut()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getFecha_pedido()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getFecha_entrega()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getCan_pedida()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getCan_pendiente()%></font></td>
                            <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesDeCompraVencidasPorMaterial.jsp?codpro=<%=ordenInvalidas.getCod_producto()%>&descri=<%=ordenInvalidas.getDescripcion()%>')"><%=ordenInvalidas.getCod_producto()%></a></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenInvalidas.getDescripcion()%></font></td>
                        </tr><%i++;}%> 
                    </table>
                    <%}%>
                    <%if((request.getParameter("ordenes").equals("7"))||(request.getParameter("ordenes").equals("8"))||(request.getParameter("ordenes").equals("9"))||(request.getParameter("ordenes").equals("10"))||(request.getParameter("ordenes").equals("11"))||(request.getParameter("ordenes").equals("12"))){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                        <%while(enumValidas.hasMoreElements()){
                            mac.ee.pla.ordeCompra.clase.OrdenDeCompra ordenValidas =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enumValidas.nextElement();%>
                        <tr>
                            <td align="center"><font class="Estilo9"><%=i%></font></td>    
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getNum_orden_compra()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getLinea()%></font></td>
                            <td align="center"><font class="Estilo9"><%=(int)ordenValidas.getRut()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getFecha_pedido()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getFecha_entrega()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getCan_pedida()%></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getCan_pendiente()%></font></td>
                            <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesCompraFuturas.jsp?codpro=<%=ordenValidas.getCod_producto()%>&descri=<%=ordenValidas.getDescripcion()%>')"><%=ordenValidas.getCod_producto()%></a></font></td>
                            <td align="center"><font class="Estilo9"><%=ordenValidas.getDescripcion()%></font></td>
                        </tr><%i++;}%> 
                    </table>
                    <%}%>
                    <%if(request.getParameter("ordenes").equals("13")){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                        <%while(enumTodas.hasMoreElements()){
                         mac.ee.pla.ordeCompra.clase.OrdenDeCompra todasLasOrdenes = (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enumTodas.nextElement();
                         fecAno3 = anoDate.format(todasLasOrdenes.getFecha_entrega());
                         int x = Integer.parseInt(fecAno3);
                         int y = Integer.parseInt(fecAno);
                         if(x<y){%>
                         <tr bgcolor="#FFE882">         
                         <%}else{%>
                         <tr bgcolor="A1C595">  
                         <%}%>
                            <td align="center" class="Estilo9"><%=i%></td>    
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getNum_orden_compra()%></td>
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getLinea()%></td>  
                            <td align="center" class="Estilo9"><%=(int)todasLasOrdenes.getRut()%></td>
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getFecha_pedido()%></td>
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getFecha_entrega()%></td>
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getCan_pedida()%></td>
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getCan_pendiente()%></td>
                            <%if(x<y){%>
                            <td align="center" class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesDeCompraVencidasPorMaterial.jsp?codpro=<%=todasLasOrdenes.getCod_producto()%>&descri=<%=todasLasOrdenes.getDescripcion()%>')"><%=todasLasOrdenes.getCod_producto()%></a></td> 
                            <%}else{%>
                            <td align="center" class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesCompraFuturas.jsp?codpro=<%=todasLasOrdenes.getCod_producto()%>&descri=<%=todasLasOrdenes.getDescripcion()%>')"><%=todasLasOrdenes.getCod_producto()%></a></td> 
                            <%}%>
                            <td align="center" class="Estilo9"><%=todasLasOrdenes.getDescripcion()%></td>         
                        </tr><%i++;}%> 
                    </table>
                    <%}%>                    
                    <%if(request.getParameter("submitButtonName")!=null){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" bgcolor=#DDDDDD>
                        <%while(enumRut.hasMoreElements()){
                            mac.ee.pla.ordeCompra.clase.OrdenDeCompra veClientesPorRut =  (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) enumRut.nextElement();
                         fecAno3 = anoDate.format(veClientesPorRut.getFecha_entrega());
                         int x = Integer.parseInt(fecAno3);
                         int y = Integer.parseInt(fecAno);
                         if(x<y){%>
                         <tr bgcolor="#FFE882">         
                         <%}else{%>
                         <tr bgcolor="A1C595">  
                         <%}%>                            
                            <td align="center"><font class="Estilo9"><%=i%></font></td>    
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getNum_orden_compra()%></font></td>
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getLinea()%></font></td>
                            <td align="center"><font class="Estilo9"><%=(int)veClientesPorRut.getRut()%></font></td>
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getFecha_pedido()%></font></td>
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getFecha_entrega()%></font></td>
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getCan_pedida()%></font></td>
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getCan_pendiente()%></font></td>
                            <%if(x<y){%>
                            <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesDeCompraVencidasPorMaterial.jsp?codpro=<%=veClientesPorRut.getCod_producto()%>&descri=<%=veClientesPorRut.getDescripcion()%>')"><%=veClientesPorRut.getCod_producto()%></a></font></td>
                            <%}else{%>
                            <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesCompraFuturas.jsp?codpro=<%=veClientesPorRut.getCod_producto()%>&descri=<%=veClientesPorRut.getDescripcion()%>')"><%=veClientesPorRut.getCod_producto()%></a></font></td>
                            <%}%>                            
                            <td align="center"><font class="Estilo9"><%=veClientesPorRut.getDescripcion()%></font></td>
                        </tr><%i++;}%> 
                    </table>
                    <%}%>
                    <%if(request.getParameter("submitButtonNomCli")!=null){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" class="Estilo10">
                        <%while(eProveedor.hasMoreElements()){
                            mac.ee.pla.ordeCompra.clase.OrdenDeCompra verProveedor = (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eProveedor.nextElement();                      
                         fecAno3 = anoDate.format(verProveedor.getFecha_entrega());
                         int x = Integer.parseInt(fecAno3);
                         int y = Integer.parseInt(fecAno);
                         if(x<y){%>
                         <tr bgcolor="#FFE882">         
                         <%}else{%>
                         <tr bgcolor="A1C595">  
                         <%}%>      
                            <td align="center"><font class="Estilo9"><%=i%></td>    
                            <td align="center"><font class="Estilo9"><%=verProveedor.getNum_orden_compra()%></font></td>
                            <td align="center"><font class="Estilo9"><%=verProveedor.getLinea()%></font></td>
                            <td align="center"><font class="Estilo9"><%=(int)verProveedor.getRut()%></font></td>
                            <td align="center"><font class="Estilo9"><%=verProveedor.getFecha_pedido()%></font></td>
                            <td align="center"><font class="Estilo9"><%=verProveedor.getFecha_entrega()%></font></td>
                            <td align="center"><font class="Estilo9"><%=verProveedor.getCan_pedida()%></font></td>
                            <td align="center"><font class="Estilo9"><%=verProveedor.getCan_pendiente()%></font></td>
                            <%if(x<y){%>
                            <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesDeCompraVencidasPorMaterial.jsp?codpro=<%=verProveedor.getCod_producto()%>&descri=<%=verProveedor.getDescripcion()%>')"><%=verProveedor.getCod_producto()%></a></font></td>
                            <%}else{%>
                            <td align="center"><font class="Estilo9"><a href="javascript:Abrir_ventana5('../mrp/ordenesCompraFuturas.jsp?codpro=<%=verProveedor.getCod_producto()%>&descri=<%=verProveedor.getDescripcion()%>')"><%=verProveedor.getCod_producto()%></a></font></td>
                            <%}%>
                            <td align="center"><font class="Estilo9"><%=verProveedor.getDescripcion()%></font></td>                           
                        </tr><%i++;}%> 
                    </table>
                    <%}%>
                    
                    <%if(request.getParameter("submitButtonNomCod")!=null){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" class="Estilo10">
                        <%while(eCodigoMatPrimas.hasMoreElements()){
                            mac.ee.pla.ordeCompra.clase.OrdenDeCompra verCodigoMateriaPrima = (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eCodigoMatPrimas.nextElement();
                             fecAno3 = anoDate.format(verCodigoMateriaPrima.getFecha_entrega());
                         int x = Integer.parseInt(fecAno3);
                         int y = Integer.parseInt(fecAno);
                         if(x<y){%>
                         <tr bgcolor="#FFE882">         
                         <%}else{%>
                         <tr bgcolor="A1C595">  
                         <%}%>                            
                            <td align="center"><%=i%></td>    
                            <td align="center"><%=verCodigoMateriaPrima.getNum_orden_compra()%></td>
                            <td align="center"><%=verCodigoMateriaPrima.getLinea()%></td>
                            <td align="center"><%=(int)verCodigoMateriaPrima.getRut()%></td>
                            <td align="center"><%=verCodigoMateriaPrima.getFecha_pedido()%></td>
                            <td align="center"><%=verCodigoMateriaPrima.getFecha_entrega()%></td>
                            <td align="center"><%=verCodigoMateriaPrima.getCan_pedida()%></td>
                            <td align="center"><%=verCodigoMateriaPrima.getCan_pendiente()%></td>
                            <%if(x<y){%>
                            <td align="center"><a href="javascript:Abrir_ventana5('../mrp/ordenesDeCompraVencidasPorMaterial.jsp?codpro=<%=verCodigoMateriaPrima.getCod_producto()%>&descri=<%=verCodigoMateriaPrima.getDescripcion()%>')"><%=verCodigoMateriaPrima.getCod_producto()%></a></td>
                            <%}else{%>
                            <td align="center"><a href="javascript:Abrir_ventana5('../mrp/ordenesCompraFuturas.jsp?codpro=<%=verCodigoMateriaPrima.getCod_producto()%>&descri=<%=verCodigoMateriaPrima.getDescripcion()%>')"><%=verCodigoMateriaPrima.getCod_producto()%></a></td>
                            <%}%>
                            <td align="center"><%=verCodigoMateriaPrima.getDescripcion()%></td>
                        </tr><%i++;}%> 
                    </table>
                    <%}%>
                    
                    <%if(request.getParameter("submitButtonNomDesc")!=null){%>
                    <table border=1 cellspacing=0 cellpadding=2 id="datos" class="Estilo10">
                        <%while(eDescMatPrima.hasMoreElements()){
                            mac.ee.pla.ordeCompra.clase.OrdenDeCompra verDescripcionMatPrimas = (mac.ee.pla.ordeCompra.clase.OrdenDeCompra) eDescMatPrima.nextElement();
                        fecAno3 = anoDate.format(verDescripcionMatPrimas.getFecha_entrega());
                         int x = Integer.parseInt(fecAno3);
                         int y = Integer.parseInt(fecAno);
                         if(x<y){%>
                         <tr bgcolor="#FFE882">         
                         <%}else{%>
                         <tr bgcolor="A1C595">  
                         <%}%>      
                            <td align="center"><%=i%></td>    
                            <td align="center"><%=verDescripcionMatPrimas.getNum_orden_compra()%></td>
                            <td align="center"><%=verDescripcionMatPrimas.getLinea()%></td>
                            <td align="center"><%=(int)verDescripcionMatPrimas.getRut()%></td>
                            <td align="center"><%=verDescripcionMatPrimas.getFecha_pedido()%></td>
                            <td align="center"><%=verDescripcionMatPrimas.getFecha_entrega()%></td>
                            <td align="center"><%=verDescripcionMatPrimas.getCan_pedida()%></td>
                            <td align="center"><%=verDescripcionMatPrimas.getCan_pendiente()%></td>
                            <%if(x<y){%>
                            <td align="center"><a href="javascript:Abrir_ventana5('../mrp/ordenesDeCompraVencidasPorMaterial.jsp?codpro=<%=verDescripcionMatPrimas.getCod_producto()%>&descri=<%=verDescripcionMatPrimas.getDescripcion()%>')"><%=verDescripcionMatPrimas.getCod_producto()%></a></td>
                            <%}else{%>
                            <td align="center"><a href="javascript:Abrir_ventana5('../mrp/ordenesCompraFuturas.jsp?codpro=<%=verDescripcionMatPrimas.getCod_producto()%>&descri=<%=verDescripcionMatPrimas.getDescripcion()%>')"><%=verDescripcionMatPrimas.getCod_producto()%></a></td>
                            <%}%>
                            <td align="center"><%=verDescripcionMatPrimas.getDescripcion()%></td>
                        </tr><%i++;}%> 
                    </table>
                    <%}%>
                </div>
            </td>
        </table>
        <%}%>
         <table align="right">
            <tr>
                <%if((request.getParameter("ordenes")!=null)&&(!request.getParameter("ordenes").equals("0"))){%>
                <td><font class="Estilo10"><a href="exportarExcel.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&id=<%=request.getParameter("ordenes")%>&fechaHoy=<%=fecAno%>"><img src="../Images/logo_excel.gif" width="18" height="18" border="0" alt="Transformar a Excel"/></a></font></td>
                <%}if(request.getParameter("submitButtonName")!=null){%>
                <td><font class="Estilo10"><a href="exportarExcel2.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&rut=<%=request.getParameter("rutClie")%>"><img src="../Images/logo_excel.gif" width="18" height="18" border="0" alt="Transformar a Excel"/></a></font></td>
                <%}if(request.getParameter("submitButtonNomCli")!=null){%>
                <td><font class="Estilo10"><a href="exportarExcel3.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&proveedor=<%=request.getParameter("nombreCli")%>"><img src="../Images/logo_excel.gif" width="18" height="18" border="0" alt="Transformar a Excel"/></a></font></td>
                <%}if(request.getParameter("submitButtonNomCod")!=null){%>
                <td><font class="Estilo10"><a href="exportarExcel4.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&codigo=<%=request.getParameter("nombreCod")%>"><img src="../Images/logo_excel.gif" width="18" height="18" border="0" alt="Transformar a Excel"/></a></font></td>
                <%}if(request.getParameter("submitButtonNomDesc")!=null){%>
                <td><font class="Estilo10"><a href="exportarExcel5.jsp?idSesion=<%=sesion.getId()%>&usuario=<%=usuario%>&tipou=<%=tipou%>&descripcion=<%=request.getParameter("nombreDescripcion")%>"><img src="../Images/logo_excel.gif" width="18" height="18" border="0" alt="Transformar a Excel"/></a></font></td>
                <%}else{%>
                <td>&nbsp;</td>
                <%}%>
            </tr>
        </table>
        <%}else{response.sendRedirect("../mac");}%>
      </form>
    </BODY>
</HTML>
